<?php
/**
 * 所有的相关的页面
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class JcropAsset extends AssetBundle
{
    public $basePath = '@webroot/resources/jcrop';
    public $baseUrl = '@web/resources/jcrop';
    public $css = [
        'css/jquery.Jcrop.min.css',
    ];
    public $js = [
        'js/jquery-migrate-1.2.1.js',
        'js/jquery.Jcrop.min.js',
    ];
    public $jsOptions = [
        //'position' => \yii\web\View::POS_HEAD       
    ];    
}
