<?php
/**
 * 所有的相关的页面
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot/main';
    public $baseUrl = '@web/main';
    public $css = [
        'css/91.css',
    ];
    public $js = [
        'js/91.js?id=5',
        'layer/layer.js',
        'ajaxForm.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD       
    ];    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
