<?php

namespace backend\components;
use \yii;
class BaseController extends \common\components\BaseController
{
    public function init()
    {
        parent::init();
        $this->user->isLogin();
    }
    public function runAction($id, $params = array())
    {
        $controller_list = yii::$app->params['no_login_controller'];
        $action_list = yii::$app->params['no_login_action'];
        if(in_array(yii::$app->controller->id, $controller_list) || in_array($id, $action_list)){//是否是在这个相应的控制器里面
            return parent::runAction($id, $params);
        }else{
            if(!$this->user->uid){
                return $this->redirect(['public/login']);
                //$this->error($this->user->error,['public/login']);
            }
            if(!$this->rbac($id)){
                $this->error('你没有权限操作',['public/login']);
            }
        }     
        //在这里面的说明是经常过权限的验证等的东西，在这里面各操作处理完了后都必须再调用我的另一个的方法来保存我的相关的操作日志
        register_shutdown_function([$this,"afterOperation"],$id);
        return parent::runAction($id, $params);
    }
    
    //权限检查
    protected function rbac($id)
    {
        $user = $this->user->member_info;
        $controller = $this->id;
        $action = $id;
        return \backend\models\AdminAccessModel::rbac($user, $controller, $action);
    }
    
    //操作完了后的我们的相关的日志的记录
    public function afterOperation($id)
    {
        $model = new \backend\models\AdminLogModel();
        $data = [];
        $data['admin_id'] = $this->user->uid;
        $data['username'] = $this->user->username;
        $data['params'] = json_encode($_GET);
        $data['module'] = $this->module->id;
        $data['controller'] = $this->id;
        $data['action'] = $id;
        if(!empty($_SERVER['HTTP_HOST']) && !empty($_SERVER['REQUEST_URI'])){
            $data['url'] = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $data['post'] = json_encode($_POST);
        }else{
            $data['url'] = '';
        }        
        $data['add_date'] = date('Y-m-d H:i:s');
        $remarks = \backend\models\AdminMenuModel::findOne(['controller' => $data['controller'],'action' => $data['action']]);
        if($remarks){
            $data['remarks'] = $remarks['title'];
        }else{
            $data['remarks'] = '';
        }
        $model->attributes = $data;
        $model->save(false);        
    }    
    
    
    
    public function actionIndex_excel()
    {
        set_time_limit(0);
        $this->layout = false;
        $datas = $this->actionIndex(99999);
        
        //print_r($this->tVar['sum_money']);exit;
        $other_data = [];
        //$other_data[0][0] = "总提现：300万";
        $title = \common\components\GuestInfo::getParam("title","列表");
        $obj = new \common\components\Excel_Download(['title' => $title,"download_data" => $datas,"other_data" => $other_data]);
        $obj->run();
        exit;                 
    }
    
    

    public function is_can_operation($save_uid)
    {
        if(!$this->user->isSuper()){
            if($this->user->frontend_uid == $save_uid){
                return true;
            }
            
            if(!\common\models\hy_designerModel::isParent($this->user->frontend_uid, $save_uid)){
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }        
    }    
    
    
}

