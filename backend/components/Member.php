<?php
/*
 * 后台用户的相关的操作
 */
namespace backend\components;
use Exception;
class Member extends \common\components\BaseMember
{
    const ADMIN_KEY = 'admin_gsdew';
    const SUPER_ADMIN = 1;//超级管理员

    
    //是否是管理员
    public function isAdmin()
    {
        return 1;
    }
    //username password
    public function login($datas)
    {
        try {
            if(empty($datas['username'])){
                throw new Exception('用户名不能为空');
            }
            if(empty($datas['password'])){
                throw new Exception('密码不能为空');
            }
            $datas['password'] = $this->createPassword($datas['password']);
            $member = \backend\models\AdminMemberModel::find()->where(['username' => $datas['username']])->one();
            if(!$member){
                throw new Exception('找不到相关的用户信息');
            }
            if($member['password'] != $datas['password']){
                throw new Exception('用户密码错误');
            }
            return $this->createLoginData($member);
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    
    //登录后需要的操作
    //创建相关的SESSION信息，然后再确定相关的操作。
    public function createLoginData($member_model)
    {
        $session = \yii::$app->session;
        $session->set(self::ADMIN_KEY, \common\components\Encrypt::authcode($member_model['uid'], ''));
        
        //写日志，记录最后一次登录时间
        $member_model->last_login_time = TIMESTAMP;
        $member_model->save();
        return true;
    }
    //是否有登录
    public function isLogin()
    {
        if($this->member_info){
            return $this->uid;
        }
        $session = \yii::$app->session;
        $uid = $session->get(self::ADMIN_KEY);        
        $uid = \common\components\Encrypt::authcode($uid);
        try {
            if(!$uid){
                throw new Exception('你没有登录');
            }
            $member = \backend\models\AdminMemberModel::findOne($uid);
            if(!$member){
                throw new Exception('找不到此用户');
            }
            return $this->afterIsLogin($member);
            
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    
    public function afterIsLogin($member)
    {
        $this->uid = $member['uid'];
        $this->member_info = $member;
        return $this->uid;
    }
    //退出
    public function logout()
    {
        $session = \yii::$app->session;
        $session->remove(self::ADMIN_KEY);
    }
    
    public function register($datas) {
        return true;
    }
    
    //获取用户数据
    public function getMember()
    {
        try {
            if(!$this->uid){
                throw new Exception('用户没有登录');
            }
            if($this->member_info){
                return $this->member_info;
            }
            $member = \backend\models\AdminMemberModel::findOne($this->uid);
            if(!$member){
                $this->logout();
                throw new Exception('找不到相关用户');
            }
            $this->member_info = $member->attributes;;
            return $this->member_info;
        } catch (Exception $exc) {
            return false;
        }
    }
    
    //加密操作
    public function createPassword($password)
    {
        return md5($password);
    }   
    
    /**
     * 是否是超管
     */
    public function isSuper(){
    	if(!$this->member_info){
    		return false;
    	}
    	if($this->member_info['role_id']==1){
    		return true;
    	}
    	return false;
    }     
}
