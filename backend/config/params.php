<?php
return [
    'adminEmail' => 'admin@example.com',
    'no_login_controller' => [
        'public',
    ],
    'no_login_action' => [
        'public',
        'update_mine',
        'public_list_user',
    ],
    
];
