<?php
namespace backend\controllers;
use backend\components\BaseController;
use backend\models\AdminMenuModel;
use backend\models\AdminRoleModel;
/*
 * 角色的权限
 */
class AccessController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \backend\models\AdminAccessModel();
    }

    public function actionIndex($role_id)
    {
        if($_POST){
            //删除原来的，再添加新的
            $state = AdminRoleModel::updateData($role_id, $_POST['menu']);
            if(!$state){
                $this->error(AdminRoleModel::$error);
            }else{
                $this->success('成功处理');
            }
        }
        //获取所有的
        //获取某个角色的拥有的权限
        $menus = AdminMenuModel::getAllForRole($role_id);
        //print_r($menus);exit;
        //print_r($menus);exit;
        $this->assign('menus',$menus);
        return $this->render();
    }
    
    public function actionCreate($parent_id = '')
    {
        $this->model_class->parent_id = $parent_id;
        $this->model_class->status = 1;
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}
