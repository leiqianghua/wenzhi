<?php
namespace backend\controllers;
class Ad_list_wapController extends \backend\components\BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\AdListWapModel();
    }

    public function actionIndex()
    {
    	return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    public function actionFz(){
    	$id=\common\components\GuestInfo::getParam('id');
    	$data=  \common\models\AdListWapModel::findOne($id);
    	if(!$data){
    		$this->error('数据不存在');
    	}
    	$model=new \common\models\AdListWapModel();
    	$model->attributes=$data->attributes;
    	unset($model->id);
    	if(!$model->save()){
    		$this->error($model->getOneError());
    	}
    	$this->success('复制成功');
    }
    

    public function actionIndex_popup()
    {
        $_GET['post_alias'] = "popup";
    	return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate_popup()
    {
        $_GET['post_alias'] = "popup";
        return $this->baseCreate($this->model_class,"index_popup");
    }
    
    public function actionUpdate_popup()
    {
        $_GET['post_alias'] = "popup";
        return $this->baseUpdate(get_class($this->model_class),"index_popup");
    }
    
    public function actionDelete_popup()
    {
        return $this->baseDelete(get_class($this->model_class),"index_popup");
    }   
    
    
    public function actionSelect_key_type()
    {
        $id = \common\components\GuestInfo::getParam("id");
        $key_type = \common\components\GuestInfo::getParam("key_type");
        if($id){
            $model = \common\models\AdListWapModel::findOne($id);
        }
        if(empty($model)){
            $model = new \common\models\AdListWapModel();
        }
        if($key_type){
            $model->post_alias = $key_type;
        }
        $str_model = get_class($model);
        //print_r($model);exit;
        $content = $this->renderPartial("_sel", array('model' => $model,'object' => $str_model));
        $content = preg_replace("/<form.*?>/i", '', $content);
        $content = preg_replace("/<\/form>/i", '', $content);
    
        $this->success(['content' => $content]);
    }
}
