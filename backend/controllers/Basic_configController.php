<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
use common\models\MessageSupportModel;
use common\models\PayConfigModel;
use common\models\DomainModel;
/*
 * 
 */
class Basic_configController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ConfigModel();
    }

   /* 网站基本配置 
    * $ctype配置类型6
    * */
    public function actionIndex()
    {
    	//通过附加条件可以更改查询结果
    	//$this->model_class->type=1
    	$ctype=1;
    	if(Yii::$app->request->isGet){
    		$basic=$this->model_class->getObj($ctype);
    		return $this->render('index',['basic'=>$basic]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    } 
    
    /* 网站基本配置
     * $ctype配置类型6
     * */
    public function actionInfo_type()
    {
        //通过附加条件可以更改查询结果
        //$this->model_class->type=1
        $ctype=17;
        if(Yii::$app->request->isGet){
            $basic=$this->model_class->getObj($ctype);
            return $this->render('info_type',['basic'=>$basic]);
        }else{
            $post=Yii::$app->request->post();
            $res=$this->model_class->saveConfig($post,$ctype);
            if($res==0){
                $this->success();
            }
        }
    }
    
    /* 中奖阈值配置
     * $ctype配置类型16
     * */
    public function actionLottery_out()
    {
        //通过附加条件可以更改查询结果
        //$this->model_class->type=1
        $ctype=16;
        if(Yii::$app->request->isGet){
            $basic=$this->model_class->getObj($ctype);
            return $this->render('lottery_out',['basic'=>$basic]);
        }else{
            $post=Yii::$app->request->post();
            $res=$this->model_class->saveConfig($post,$ctype);
            if($res==0){
                $this->success();
            }
        }
    }
    
    /* 注册短信发送配置
     * $ctype配置类型18
     * */
    public function actionMessage_type()
    {
        //通过附加条件可以更改查询结果
        //$this->model_class->type=1
        $ctype=18;
        if(Yii::$app->request->isGet){
            $basic=$this->model_class->getObj($ctype);
            return $this->render('code_type',['basic'=>$basic]);
        }else{
            $post=Yii::$app->request->post();
            $res=$this->model_class->saveConfig($post,$ctype);
            if($res==0){
                $this->success();
            }
        }
    }
    
    
    /* 兑吧积分配置
     * $ctype配置类型8
     * */
    public function actionDuiba()
    {
    	//通过附加条件可以更改查询结果
    	//$this->model_class->type=1
    	$ctype=8;
    	if(Yii::$app->request->isGet){
    		$duiba=$this->model_class->getObj($ctype);
    		return $this->render('duiba',['duiba'=>$duiba]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    	//  return $this->baseIndex($this->model_class);
    }
    
    /* 开奖网站地址配置
     * $ctype配置类型11
     * */
    public function actionLottery()
    {
    	//通过附加条件可以更改查询结果0
    	//$this->model_class->type=1
    	$ctype=11;
    	if(Yii::$app->request->isGet){
    		$lottery_url=$this->model_class->getObj($ctype);
    		return $this->render('lottery',['lottery'=>$lottery_url]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    	//  return $this->baseIndex($this->model_class);
    }
    
    /* 是否跳转到他人个人中心配置
     * $ctype配置类型12
     * */
    public function actionTo_other()
    {
    	//通过附加条件可以更改查询结果0
    	//$this->model_class->type=1
    	$ctype=12;
    	if(Yii::$app->request->isGet){
    		$to_other=$this->model_class->getObj($ctype);
    		return $this->render('to_other',['to_other'=>$to_other]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    	//  return $this->baseIndex($this->model_class);
    }
    
    public function actionScore_percent()
    {
        //积分比例
        //$this->model_class->type=19
        $ctype=19;
        if(Yii::$app->request->isGet){
            $score=$this->model_class->getObj($ctype);
            return $this->render('score_percent',['score'=>$score]);
        }else{
            $post=Yii::$app->request->post();
            $res=$this->model_class->saveConfig($post,$ctype);
            if($res==0){
                $this->success();
            }
        }
    }
    /* 
     * $ctype配置类型1
     *  */
    public function actionSeo(){
    	//
    	$ctype=1;
    	if(Yii::$app->request->isGet){
    		$seo=$this->model_class->getObj($ctype);
    		return $this->render('seo',['seo'=>$seo]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    
    }
    
    /* 
     * 上传配置
     * $ctype配置类型2
     *  */
    
    public function actionUpload(){
    	//
    	$ctype=2;
     	if(Yii::$app->request->isGet){
    		$upload=$this->model_class->getObj($ctype);
    		return $this->render('upload',['upload'=>$upload]);
    	}else{
    		$post=Yii::$app->request->post();
    
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	} 
    	//return $this->render('upload');
    
    }
    
    /*
     * 水印配置
     * $ctype配置类型3
     *  */
    
    public function actionWatermark(){
    	//
    	$ctype=3;
    	if(Yii::$app->request->isGet){
    		$water_mark=$this->model_class->getObj($ctype);
    		return $this->render('watermark',['water'=>$water_mark]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    	//return $this->render('upload');
    
    }
    
    /*
     * 邮箱配置
     * $ctype配置类型4
     *  */
    
    public function actionEmail(){
    	//
    	$ctype=4;
    	if(Yii::$app->request->isGet){
    		$email=$this->model_class->getObj($ctype);
    		return $this->render('email',['email'=>$email]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    	//return $this->render('upload');
    
    }
    
    /*
     * 中奖通知设置
     * $ctype配置类型5
     *  */
    
    public function actionSend(){
    	//
    	$ctype=5;
    	if(Yii::$app->request->isGet){
    		$notice=$this->model_class->getObj($ctype);
    		return $this->render('lottery_notice',['notice'=>$notice]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    	//return $this->render('upload');
    
    }
    
    public function actionCardverification() {
        $ctype=15;
    	if(Yii::$app->request->isGet){
    		$notice=$this->model_class->getObj($ctype);
    		return $this->render('card',['basic'=>$notice]);
    	}else{
    		$post=Yii::$app->request->post();
    		$res=$this->model_class->saveConfig($post,$ctype);
    		if($res==0){
    			$this->success();
    		}
    	}
    }




    /*
     * 添加短信接口
     *   */
    public function actionMessage(){
    	$model=new MessageSupportModel();
    	return $this->baseIndex($model);
    }
    
    /*
     *更新短信配置
     *   */
    public function actionMessage_update(){
    	$model=new MessageSupportModel();
    	 return $this->baseUpdate(get_class($model),'message');
    }
    
    /*
     *删除短信配置
     *   */
    public function actionMessage_delete()
    {
    	$model=new MessageSupportModel();
        return $this->baseDelete(get_class($model),'message');
    }
    
    /*
     *增加         短信配置
     *   */
    public function actionMessage_create()
    {
    	$model=new MessageSupportModel();
    	return $this->baseCreate($model,'message');
    }
    
    /*
     *支付方式
     *   */
    public function actionPay()
    {
    	$model=new PayConfigModel();
    	return $this->baseIndex($model);
    }
    
    public function actionPay_update(){
    	$model=new PayConfigModel();
    	return $this->baseUpdate(get_class($model),'pay');
    }
    
    public function actionPay_create(){
    	$model=new PayConfigModel();
    	return $this->baseUpdate($model,'pay');
    }
    
    public function actionPay_delete()
    {
    	$model=new PayConfigModel();
    	return $this->baseDelete(get_class($model),'pay');
    }
    
    /*
     *模拟域名绑定
     *   */
    public function actionDomain()
    {
    	$model=new DomainModel();
    	return $this->baseIndex($model);
    }
    
    public function actionDomain_update(){
    	$model=new DomainModel();
    	return $this->baseUpdate(get_class($model),'domain');
    }
    
    public function actionDomain_create(){
    	$model=new DomainModel();
    	return $this->baseUpdate($model,'domain');
    }
    
    public function actionDomain_delete()
    {
    	$model=new DomainModel();
    	return $this->baseDelete(get_class($model),'domain');
    } 
    
    public function actionWeixin()
    {
        $model = \common\models\ConfigModel::findOne("weixin");
        if(!$_POST){
            $this->assign("datas", json_decode($model->value,true));
            return $this->render();
        }else{
            $model->value = json_encode($_POST['value']);
            $model->save(false);
            $this->success("配置成功");
        }
    }
    
    public function actionUpdate_weixin()
    {
        $model = \common\models\ConfigModel::findOne("weixin");
        $value = json_decode($model->value,true);
        $result = [];
        foreach($value as $k => $v){
            $count = count($v['data']);
            if($count == 0){
                continue;
            }
            if($count > 1){//多于一个
                $array = [];
                $array['name'] = $v['name'];
                foreach($v['data'] as $kk => $vv){
                    $array['sub_button'][] = $this->_createArrWeixin($vv);
                }
                $result['button'][] = $array;
            }else{//只有一个
                $vc = array_values($v['data']);
                $result['button'][] = $this->_createArrWeixin($vc[0]);
            }
        }
        $state = \common\components\weixin\Menu::create($result);
        if($state === true){
            $this->success("更新成功");
        }else{
            $this->error(json_encode($state));
        }
    }
    
    
    private function _createArrWeixin($vc)
    {
        $arr = [];
        $arr['name'] = $vc['name'];
        $arr['type'] = $vc['type'];
        if($arr['type'] == "view"){
            $arr['url'] = $vc['content'];
        }else{
            $arr['key'] = $vc['key'];
        }
        return $arr;
    }
}
