<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 商品
 */
class Cha_goodsController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ChaGoodsCommonModel();
    }

    public function actionIndex($limit = 20)
    {
        if($this->user->role_id != \backend\components\Member::SUPER_ADMIN){
            $_GET['admin_id'] = $this->user->uid;
        }
        return $this->baseIndex($this->model_class,[],$limit,[],"index");
    }
    
    public function actionIndex_excel()
    {
        $this->layout = false;
        $datas = $this->actionIndex(99999);
        
        //print_r($this->tVar['sum_money']);exit;
        $other_data = [];
        //$other_data[0][0] = "总提现：300万";
        
        $obj = new \common\components\Excel_Download(['title' => "商品列表","download_data" => $datas,"other_data" => $other_data]);
        $obj->run();
        exit;                 
    }
    
    public function actionSelect_index()
    {
        if($this->user->role_id != \backend\components\Member::SUPER_ADMIN){
            $_GET['admin_id'] = $this->user->uid;
        }
        return $this->baseIndex(new \common\models\ChaGoodsModel());
    }
    
    
    public function actionCreate()
    {
        if($this->user->role_id != \backend\components\Member::SUPER_ADMIN){
            if($_POST){
                $_POST['status'] = 2;//给其上传的时候是默认需要审核的
                $_POST['admin_id'] = $_POST['uid'] = $this->user->uid;
                $member_model = \common\models\MemberModel::findOne(['admin_id' => $this->user->uid]);
                if($member_model){
                    $_POST['city_id'] = $member_model['city_id'];
                    $_POST['area_id'] = $member_model['area_id'];
                    $_POST['province_id'] = $member_model['province_id'];
                }
            }
        }
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    //审核
    public function actionExamine()
    {
        $_POST['ok'] = true;
        if($this->user->role_id != \backend\components\Member::SUPER_ADMIN){
            $admin_id = \common\models\ChaGoodsCommonModel::findOne(\common\components\GuestInfo::getParam("id"));
            $admin_id = $admin_id['admin_id'];
            //查看其上级
            $member_model = \common\models\MemberModel::find()->limit(1)->where(['admin_id' => $admin_id])->one();
            if($member_model && $member_model['parent_id']){
                //看其上级的这个admin_id
                $model = \common\models\MemberModel::findOne($member_model['parent_id']);
                if($model && $model['admin_id'] == $this->user->uid){
                    
                }else{
                    $this->error("权限不足");
                }
            }else{
                $this->error("权限不足");
            }
        }
        \common\models\ChaGoodsModel::updateAll(['status' => \common\components\GuestInfo::getParam("status")], ['common_id' => \common\components\GuestInfo::getParam("id")]);
        return $this->baseUpdate(get_class($this->model_class));
    }
}
