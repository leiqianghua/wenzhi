<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 菜单表
 */
class Cha_goods_classController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ChaGoodsClassModel();
    }

    public function actionIndex()
    {
        $object = get_class($this->model_class);
        $menus = $object::showMenu();
        $this->assign('menus',$menus);
        return $this->render();
        
        //return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate($parent_id = '')
    {
        $this->model_class->parent_id = $parent_id;
        $this->model_class->status = 1;
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}
