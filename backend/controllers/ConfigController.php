<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
/*
 *
 */
class ConfigController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ConfigModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }
    //如果其字符串
    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }    
    
    //修改其初始配置
    public function actionUpdate_init()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }  
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    //所有的相关的配置操作
    public function actionSwoole()
    {
        $_GET['varname'] = "swoole";
        //print_r($_POST);exit;
        return $this->baseUpdate(get_class($this->model_class)); 
    }    
 
    
    public function actionSelect_data()
    {
        error_reporting(0);
        $name = \common\components\GuestInfo::getParam("name");
        $type = \common\components\GuestInfo::getParam("type");
        $name = str_replace("[select_key]", "[data_key]", $name);
        if(!$type){
            $this->success(['content' => ""]);
        }
        
        $content = $this->renderPartial("_config_{$type}",['name' => $name,"content" => ""]);
        $this->success(['content' => $content]);
    }  
    
    
    public function actionSelect_data_1()
    {
        error_reporting(0);
        $name = \common\components\GuestInfo::getParam("name");
        $name = str_replace("[type]", "[data]", $name);
        
        //$content = \common\components\Tool::getDataForViewList($name, "/config/_config_more_config_init", [], "值");

        $content = $this->renderPartial("_config_more_config_init_1",['base_name' => $name,"value" => []]);
        $this->success(['content' => $content]);
    }
}
