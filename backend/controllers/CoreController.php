<?php
namespace backend\controllers;
use backend\components\BaseController;
use Exception;
//核心的相关功能可视化
class CoreController extends BaseController
{
    public function init()
    {
        ignore_user_abort(true);
        set_time_limit(0);
        parent::init();
    }
    
    //执行相关功能
    public function actionIndex()
    {
        return $this->render("index");
    }
    
    //商品通知的功能。
    public function actionPay()
    {
        $obj = new \common\components\pay\run\Base();
        $data = $obj->orderProcessing($_POST['out_trade_no'], $_POST['trade_no'], $_POST['total_fee'], $_POST['pay_type']);
        print_r($data);exit;
    }
    
    
    
}
