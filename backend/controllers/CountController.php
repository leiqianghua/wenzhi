<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii\data\SqlDataProvider;

/*
 * 各相关的统计
 * 商品1    销量  
 * 商品2    销量   或者是 其它的情况
 * 
 * 套装商品1   套装商品2
 * 
 * 客户1    销量   销量
 * 客户2    销量   销量
 * 
 * 城市1    销量   销量
 * 城市2    销量   销量
 */
class CountController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
    }

    public function actionGoods($limit = 20)
    {
        $this->model_class = new \common\models\GoodsModel();
        
        $where = [];
        $where[] = "  1=1 ";
        //$where[] = "  type=1 ";
        
        if(!empty($_GET['start_add_time'])){
            $time = strtotime($_GET['start_add_time']);
            $where[] = "  add_time>={$time} ";
        }
        if(!empty($_GET['end_add_time'])){
            $time = strtotime($_GET['end_add_time']);
            $where[] = "  add_time<{$time} ";
        }
        $where = implode("and", $where);
        
        
        $sql = "select a.*,b.* from yii_goods a left join (select goods_id,sum(money) money from yii_order_goods_all_count where {$where} group by goods_id) as b on a.id=b.goods_id";
        $count = \common\models\GoodsModel::find()->count();
        
        
        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'pagination' => [
              'pageSize' => $limit,
            ], 
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    "num",
                    "b.money",
                    "sale_num",
                    "purchase_num",
                ],
                'defaultOrder' => [ 'b.money' => SORT_DESC],
            ],
        ]);
        $this->assign(['dataProvider' => $dataProvider]);
        return $this->render("goods");    
        
    }
    
    public function actionGood_excel()
    {
        $this->layout = false;
        $datas = $this->actionGoods(99999);
        
        //print_r($this->tVar['sum_money']);exit;
        $other_data = [];
        //$other_data[0][0] = "总提现：300万";
        
        $obj = new \common\components\Excel_Download(['title' => "普通商品统计","download_data" => $datas,"other_data" => $other_data]);
        $obj->run();
        exit;                 
    }
    
    public function actionGoods_package_excel()
    {
        $this->layout = false;
        $datas = $this->actionGoods_package(99999);
        
        //print_r($this->tVar['sum_money']);exit;
        $other_data = [];
        //$other_data[0][0] = "总提现：300万";
        
        $obj = new \common\components\Excel_Download(['title' => "套装商品统计","download_data" => $datas,"other_data" => $other_data]);
        $obj->run();
        exit;                 
    }
    
    public function actionGoods_package($limit = 20)
    {
        $this->model_class = new \common\models\GoodsPackageModel();
        
        $where = [];
        $where[] = "  1=1 ";
        //$where[] = "  type=2 ";
        
        if(!empty($_GET['start_add_time'])){
            $time = strtotime($_GET['start_add_time']);
            $where[] = "  add_time>={$time} ";
        }
        if(!empty($_GET['end_add_time'])){
            $time = strtotime($_GET['end_add_time']);
            $where[] = "  add_time<{$time} ";
        }
        $where = implode("and", $where);
        
        
        $sql = "select a.*,b.* from yii_goods_package a left join (select province,city,goods_id,sum(money) money,sum(num) num from yii_order_goods_all_count_package where {$where} group by goods_id) as b on a.id=b.goods_id group by a.id,b.province,b.city";
        $count = \common\models\GoodsPackageModel::find()->count();
        
        
        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'pagination' => [
              'pageSize' => $limit,
            ], 
            'sort' => [
                'attributes' => [
                    "num",
                    "b.money",
                ],
                'defaultOrder' => [ 'b.money' => SORT_DESC],
            ],
             'totalCount' => $count,
        ]);
        
        $this->assign(['dataProvider' => $dataProvider]);
        return $this->render("goods_package");    
        
    }
    
    public function actionUser_excel()
    {
        $this->layout = false;
        $datas = $this->actionUser(99999);
        
        //print_r($this->tVar['sum_money']);exit;
        $other_data = [];
        //$other_data[0][0] = "总提现：300万";
        
        $obj = new \common\components\Excel_Download(['title' => "套装商品统计","download_data" => $datas,"other_data" => $other_data]);
        $obj->run();
        exit;                 
    }
    
    public function actionUser($limit = 20)
    {
        
        $where = [];
        $where[] = "  type='拥金' ";
        
        if(!empty($_GET['start_add_time'])){
            $time = strtotime($_GET['start_add_time']);
            $where[] = "  add_time>={$time} ";
        }
        if(!empty($_GET['end_add_time'])){
            $time = strtotime($_GET['end_add_time']);
            $where[] = "  add_time<{$time} ";
        }
        $where = implode("and", $where);
        
        $sql1 = "select sum(money) yon_money,uid from yii_member_money_record where {$where} group by uid";//用户的拥金
        
        
        $where = [];
        $where[] = "  1=1 ";
        
        if(!empty($_GET['start_add_time'])){
            $time = strtotime($_GET['start_add_time']);
            $where[] = "  add_time>={$time} ";
        }
        if(!empty($_GET['end_add_time'])){
            $time = strtotime($_GET['end_add_time']);
            $where[] = "  add_time<{$time} ";
        }
        $where = implode("and", $where);
        
        $sql2 = "select sum(order_amount) order_amount,uid from yii_order where {$where} group by uid";//用户的订单金额       
        
        $sql = "select a.*,b.yon_money yon_money,c.order_amount order_amount from yii_member a left join ({$sql1}) as b on a.uid=b.uid left join ({$sql2}) as c on  a.uid=c.uid";
        $count = \common\models\MemberModel::find()->count();
        
        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'pagination' => [
              'pageSize' => $limit,
            ], 
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    "order_amount",
                    "yon_money",
                ],
                'defaultOrder' => [ 'order_amount' => SORT_DESC],
            ],
        ]);
        $this->assign(['dataProvider' => $dataProvider]);
        return $this->render("user");    
    }
    
    public function actionIndex()
    {
        $query = \common\models\MemberAddmoneyRecordModel::find();
        if(empty($_GET['start_rechange_time']) && empty($_GET['end_rechange_time'])){
            $_GET['start_rechange_time'] = date('Y-m-d');
            $_GET['end_rechange_time'] = date('Y-m-d',  strtotime('tomorrow'));
        }
        if(!empty($_GET['start_rechange_time'])){
            $time = strtotime($_GET['start_rechange_time']);
            $query->andWhere("r.add_time >= {$time}");
        }
        
        if(!empty($_GET['end_rechange_time'])){
            $time = strtotime($_GET['end_rechange_time']);
            $query->andWhere("r.add_time < {$time}");
        }
        
        
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}
