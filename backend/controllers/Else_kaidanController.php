<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
/*
 * 菜单表
 */
class Else_kaidanController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ElseKaidanModel();
    }

    public function actionIndex($limit = 20)
    {
        if(\common\components\GuestInfo::getParam("status",false) === false){
            $_GET['status'] = 0;
        }
        return $this->baseIndex($this->model_class,[],$limit,[],"index");
    }
    
    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    //确认其打款正确
    public function actionConfirm_pay()
    {
        $model = \common\models\ElseKaidanModel::findOne(\common\components\GuestInfo::getParam("id"));
        if($model['last_setp_is_confirm']){
            $this->error("上一次的已经处理了");
        }
        $model->last_setp_is_confirm = 1;
        
        $model->save(false);
        
        //通知开单的人。
        $model->shenAfterNotice();
        $model->jiesuan();
        $this->success("处理成功");
    }
    
    //上传相关的成本功能
    public function actionUpload_cost()
    {
        $model = \common\models\ElseKaidanModel::findOne(\common\components\GuestInfo::getParam("id"));
        if(!$model['cost_price']){
            $model->cost_price = \common\components\GuestInfo::getParam("cost_price");
        }
        $model->jiesuan();
        $model->save(false);
        $this->success("处理成功");
    }
    
    
    
    
}
