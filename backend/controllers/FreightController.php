<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
/*
 * 商品
 */
class FreightController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ElseFreightModel();
    }

    public function actionIndex()
    {
        if($this->user->role_id != \backend\components\Member::SUPER_ADMIN){
            $_GET['admin_id'] = $this->user->uid;
        } 
        return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate()
    {
        if($this->user->role_id != \backend\components\Member::SUPER_ADMIN){
            $_GET['admin_id'] = $this->user->uid;
        } 
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        if($this->user->role_id != \backend\components\Member::SUPER_ADMIN){
            $_GET['admin_id'] = $this->user->uid;
        } 
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    public function actionSelect_city()
    {
        $content = \common\models\ElseFreightModel::addCity($_POST['id']);
        $this->ajaxReturn(['content' => $content]);
    }
}
