<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 菜单表
 */
class Goods_labelController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\GoodsLabelModel();
    }

    public function actionIndex()
    {
        $object = get_class($this->model_class);
        $menus = $object::showMenu();
        $this->assign('menus',$menus);
        return $this->render();
        
        //return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate($parent_id = '')
    {
        $this->model_class->parent_id = $parent_id;
        $this->model_class->status = 1;
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    //ajax获取标签内容
    public function actionAjaxgetlabels()
    {
        $object = get_class($this->model_class);
        $datas = $object::getMenuKeyValue(false);
        $html = '<select name="label_list">';
        foreach($datas as $key => $data){
            
            $html .= "<option value='{$key}'>{$data}</option>";
        }
        $html .= "</select>";
        $this->ajaxReturn(['content' => $html]);
    }
}
