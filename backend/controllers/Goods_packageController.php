<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 商品
 */
class Goods_packageController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\GoodsPackageModel();
    }

    public function actionIndex($limit = 20)
    {
        return $this->baseIndex($this->model_class,[],$limit,[],"index");
    }
    
    public function actionIndex_excel()
    {
        $this->layout = false;
        $datas = $this->actionIndex(99999);
        
        //print_r($this->tVar['sum_money']);exit;
        $other_data = [];
        //$other_data[0][0] = "总提现：300万";
        
        $obj = new \common\components\Excel_Download(['title' => "商品列表","download_data" => $datas,"other_data" => $other_data]);
        $obj->run();
        exit;                 
    }
    
    public function actionCreate()
    {
        if($_POST){
            $price = 0;
            foreach ($_POST['shop_list'] as $key => $value) {
                $goods_id = $value['id'];
                $num = $value['num'];
                $goods_price = \common\models\GoodsModel::getOne($goods_id, 'price');
                $price = $price + $goods_price*$num;
            }
            
            if(!empty($_POST['GoodsPackageModel']['shop_fuwu'])){
                $fujia = explode(',', $_POST['GoodsPackageModel']['shop_fuwu']);
                foreach($fujia as $d){
                    $p = \common\models\GoodsModel::getOne($d, 'price');
                    if($p){
                        $price += $p;
                    }
                }
            }
            
            
            $_POST['price'] = $price;
        }
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        if($_POST){
            $price = 0;
            foreach ($_POST['shop_list'] as $key => $value) {
                $goods_id = $value['id'];
                $num = $value['num'];
                $goods_price = \common\models\GoodsModel::getOne($goods_id, 'price');
                $price = $price + $goods_price*$num;
            }
            if(!empty($_POST['GoodsPackageModel']['shop_fuwu'])){
                $fujia = explode(',', $_POST['GoodsPackageModel']['shop_fuwu']);
                foreach($fujia as $d){
                    $p = \common\models\GoodsModel::getOne($d, 'price');
                    if($p){
                        $price += $p;
                    }
                }
            }            
            
            $_POST['price'] = $price;
        }        
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    public function actionAjax_select_goods()
    {
        $this->assign("shop_ids", $_POST['shop_ids']);
        $content = $this->renderPartial("_shop_select", $_POST);
        $this->ajaxReturn(['content' => $content]);
        
    }
}
