<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
/**
 * 所有人都可以操作上传，超管则是指定 谁的，，其它的身份则是自己上传的就是自己的。
 */
class Hy_caseController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_caseModel();
    }

    public function actionIndex()
    {
        
        //我登录进不后哪些案例可看哪些不可看    如果不是超管则是我自己的或者是我下级 的我可以看。
        
        if($this->user->isSuper()){
            return $this->baseIndex($this->model_class);
        }else{
            
            //找到所有的
            
            //$_GET['designer_id'] = $this->user->frontend_uid;//一定是只有我自己的。
            return $this->baseIndex($this->model_class);
        }
    }

    public function actionCreate()
    {
        if(!$this->user->isSuper()){
            $user = \common\models\hy_designerModel::findOne($this->user->frontend_uid);
            if(!$user){
                $this->error("不允许操作");
            }
            $this->model_class->designer_id = $user['uid'];
            $this->model_class->designer_name = $user['nickname'];
            $this->model_class->studio_id = $user['studio_id'];
            $this->model_class->studio_name = $user['studio_name'];
            $this->model_class->use_type = $this->user->role_id;        
        }else{
            if(!empty($_POST['designer_id'])){
                $this->model_class->designer_id = $_POST['designer_id'];
                $user = \common\models\hy_designerModel::findOne($_POST['designer_id']);
                
                $this->model_class->designer_name = $user['nickname'];
                $this->model_class->studio_id = $user['studio_id'];
                $this->model_class->studio_name = $user['studio_name'];
                
                $user_admin = \backend\models\AdminMemberModel::findOne(['frontend_uid' => $_POST['designer_id']]);
                if(!$user_admin){
                    $this->error("未指定用户");
                }
                $this->model_class->use_type = $user_admin['role_id'];      
            }
            
        }

        
        return $this->baseCreate($this->model_class);
    }

    public function actionUpdate()
    {

        if(!$this->user->isSuper()){
            $id = \common\components\GuestInfo::getParam("id");
            $model = \common\models\hy_caseModel::findOne($id); 
            if(!$this->is_can_operation($model['designer_id'])){
                $this->error("不是你的花园案例没有权限操作");
            }    
        }else{
            if(!empty($_POST['designer_id'])){
                $this->model_class->designer_id = $_POST['designer_id'];
                $user = \common\models\hy_designerModel::findOne($_POST['designer_id']);
                
                $this->model_class->designer_name = $user['nickname'];
                $this->model_class->studio_id = $user['studio_id'];
                $this->model_class->studio_name = $user['studio_name'];
                
                $user_admin = \backend\models\AdminMemberModel::findOne(['frontend_uid' => $_POST['designer_id']]);
                if(!$user_admin){
                    $this->error("未指定用户");
                }
                
                $this->model_class->use_type = $user_admin['role_id'];      
            }
            
        }
        
        
        return $this->baseUpdate(get_class($this->model_class));
    }

    public function actionDelete()
    {
        if(!$this->user->isSuper()){
            $id = \common\components\GuestInfo::getParam("id");
            $model = \common\models\hy_caseModel::findOne($id); 
            if(!$this->is_can_operation($model['designer_id'])){
                $this->error("不是你的花园案例没有权限操作");
            }
        }
        return $this->baseDelete(get_class($this->model_class));
    }
}
