<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_categoryController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_categoryModel();
    }

    public function actionIndex()
    {
        
        $data = \common\models\hy_categoryModel::getDataForJs();
        
        $this->assign("datas", $data);
        
        return $this->baseIndex($this->model_class);
    }
    
    public function actionAll_update()
    {
        $state = \common\models\hy_categoryModel::all_update(\common\components\GuestInfo::getParam("data"));
        if($state){
            $this->ajaxReturn(['status' => "-1","info" => "处理成功"]);
        }else{
            $this->ajaxReturn(['status' => "-1","info" => "处理失败"]);
        }
    }

    public function actionCreate()
    {
        if($_POST){
            $str_model = get_class($this->model_class);
            $datas = array_merge($_GET,$_POST);
            if(strripos($str_model, "\\") !== false){
                $b = strripos($str_model, "\\");
                $str = substr($str_model, $b + 1);
            }else{
                $str = $str_model;
            }
            if(isset($_POST[$str])){
                $datas = array_merge($datas,$_POST[$str]);
            }
            
            $arr_title = explode("\n", $datas['title']);
            
            if($arr_title){
                foreach($arr_title as $title){
                    $model = new \common\models\hy_categoryModel();
                    $model->attributes = $datas;
                    $arr_data = explode("@@", $title);
                    $model->title = isset($arr_data[0]) ? $arr_data[0] : "";
                    $model->title_1 = isset($arr_data[1]) ? $arr_data[1] : "";
                    $model->save(false);
                }
                $this->success("保存成功");
            }else{
                $this->success("保存失败");
            }
            
            
        }else{
            $this->model_class->status = 1;
            $this->model_class->type = \common\components\GuestInfo::getParam("type");
            return $this->baseCreate($this->model_class);
        }
    }

    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class),"",true);
    }

    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    public function actionAjax_ok()
    {
        $type = \common\components\GuestInfo::getParam("type");
        
        $content = $this->renderPartial("_get_type", array('type' => $type,'select_data' => ""));
        
        $this->success(['content' => $content]);
    }
}
