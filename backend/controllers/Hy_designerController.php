<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_designerController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_designerModel();
    }

    public function actionIndex()
    {
        $studio_id = \common\components\GuestInfo::getParam("studio_id");
        if(!$studio_id){
            $this->error("需要指定好工作室");
        }
        
        if(!$this->user->isSuper()){//不是超管只允许查看自己工作室的员工
            $frontend_uid = $this->user->frontend_uid;
            if(\common\models\hy_designerModel::getOne($frontend_uid, "studio_id") != $studio_id){
                $this->error("只允许查看自己工作室员工");
            }
        }
        
        $this->model_class->studio_id = $studio_id;
        return $this->baseIndex($this->model_class);
    }
    
    //更改资料
    public function actionUpdate()
    {
        $save_uid = \common\components\GuestInfo::getParam("uid");
        if(!$save_uid){
            $save_uid = $this->user->frontend_uid;
            $_GET['uid'] = $save_uid;
        }
        $this->_is_ok($save_uid);
        
        $shenfen = \common\models\hy_designerModel::createDesigner($save_uid, false);
        $this->assign("shenfen", $shenfen);
        
        return $this->baseUpdate(get_class($this->model_class),"update");
    }
    
    //设置上级
    public function actionSet_parent()
    {
        $save_uid = \common\components\GuestInfo::getParam("uid");
        $is_ok = \common\models\hy_designerModel::is_shenfen($this->user->uid, $save_uid);
        if(!$is_ok){
            $this->error("不允许被设置");
        }
        

        
        
        if($_POST){
            $obj = new \common\components\newfs\SetUseParent_id();
            $obj->uid = $save_uid;
            $obj->parent_id = \common\components\GuestInfo::getParam("parent_id");
            $is_ok = $obj->run();
            if(!$is_ok){
                $this->error($obj->error);
            }else{
                $this->success("操作成功");
            }            
        }else{
            $uid = \common\components\GuestInfo::getParam("uid");
            $can_set = \common\models\MemberModel::getAllCanSetParent($uid);
            $this->assign("can_set", $can_set);
            $model = \common\models\MemberModel::findOne($uid);
            $this->assign("model", $model);
            return $this->render();            
        }

    }
    
    //修改身份
    public function actionCreate_or_update()
    {
        
        $save_uid = \common\components\GuestInfo::getParam("uid");
        $is_ok = \common\models\hy_designerModel::is_shenfen($this->user->uid, $save_uid);
        if(!$is_ok){
            $this->error("不允许被设置");
        }
        
        if($_POST){
            $obj = new \common\components\newfs\UpdateDesigner();
            $obj->do_uid = \common\components\GuestInfo::getParam("uid");
            $obj->operation_uid = $this->user->uid;
            $obj->studio_id = \common\components\GuestInfo::getParam("studio_id");
            $obj->studio_level = \common\components\GuestInfo::getParam("studio_level");
            $state = $obj->run();
            if(!$state){
                $this->error($obj->error);
            }else{
                if(is_array($state)){
                    $this->success(['status' => -1,"content" => "设置成功,生成了管理员账号:{$state['username']},默认密码：123456,请注意保存"]);
                }else{
                    $this->success("设置成功");
                }
            }
        }else{
            $datas = \common\models\hy_designerModel::createDesigner(\common\components\GuestInfo::getParam("uid"), false);
            $this->assign("datas", $datas);
            if($this->user->isSuper()){
                $studio = \common\models\hy_studioModel::getBaseKeyValue("id", "name");
            }else{
                $frontend_uid = $this->user->frontend_uid;
                $studio_id = \common\models\hy_designerModel::getOne($frontend_uid, "studio_id");
                $studio = \common\models\hy_studioModel::getBaseKeyValue("id", "name",['id' => $studio_id]);
            }
            $this->assign("studio", $studio);
            
            $model = new \backend\models\CountForm();
            $this->assign("model", $model);
            
            return $this->render();
        }
        
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    
    public function _is_ok($save_uid)
    {
        $is_ok = $this->is_can_operation($save_uid);
        if(!$is_ok){
            $this->error("不允许操作");
        }
    }
    
    
    //删除员工身份
    public function actionDelete()
    {
        $save_uid = \common\components\GuestInfo::getParam("uid");
        $this->_is_ok($save_uid);
        
        $obj = new \common\components\newfs\DeleteDesigner();
        $obj->uid = $save_uid;
        $is_ok = $obj->run();
        if($is_ok){
            $this->success("处理成功");
        }else{
            $this->error($obj->error);
        }
    }
}
