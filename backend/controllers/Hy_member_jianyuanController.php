<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_member_jianyuanController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_member_jianyuanModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }

//    public function actionCreate()
//    {
//        return $this->baseCreate($this->model_class);
//    }
//
//    public function actionUpdate()
//    {
//        return $this->baseUpdate(get_class($this->model_class));
//    }
//
//    public function actionDelete()
//    {
//        return $this->baseDelete(get_class($this->model_class));
//    }

    public function actionShow()
    {
        $model = \common\models\hy_member_jianyuanModel::findOne(\common\components\GuestInfo::getParam("id"));
        $this->assign("model", $model);
        
        return $this->render();
    }
    
    //分配给业务员 或者其它用户
    public function actionFeipei()
    {
        if(!($this->user->isSuper() || $this->user->role_id == 37)){
            $this->error("管理员才可以分配业务员");
        }
        
        $id = \common\components\GuestInfo::getParam("id");//需要列出哪些用户出来
        $request = \common\models\hy_member_requestModel::findOne($id);
        if(!$request){
            $this->error("找不到用户需求");
        }else{
            
        }
        
        if($_POST){
            $service_uid = \common\components\GuestInfo::getParam("service_uid");
            \common\components\newfs\CreateUseRquest::updateOtherUser($id, $service_uid);
            $this->success("分配成功");
        }else{

            $user = \common\models\hy_member_requestModel::canFenPeiUser($id);
            $this->assign("user", $user);
            
            
            
            $model = new \backend\models\CountForm();
            $model->uid = $request['service_uid'];
            $this->assign("model", $model);
            return $this->render();
        }
    }
    
    //确认项目金额
    public function actionSetmoney()
    {
        \common\components\newfs\CreateUseRquest::updateMoney(\common\components\GuestInfo::getParam("id"), \common\components\GuestInfo::getParam("money"));
        $this->success("设置成功");
    }
}
