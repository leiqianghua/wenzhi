<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_member_requestController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_member_requestModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }

    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }

    public function actionOperation()//操作相关的东西
    {
        $_POST['ok'] = true;
        
        $speed = \common\components\GuestInfo::getParam("speed");
        if($speed){
            switch ($speed) {
                case 2:
                    $_GET['speed_2_time'] = time();

                    break;
                case 3:
                    $_GET['speed_3_time'] = time();

                    break;
                case 4:
                    $_GET['speed_4_time'] = time();

                    break;

                default:
                    break;
            }
        }
        
        return $this->baseUpdate(get_class($this->model_class));
    }
}
