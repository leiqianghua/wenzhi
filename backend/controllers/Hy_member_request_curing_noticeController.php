<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_member_request_curing_noticeController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_member_request_curing_noticeModel();
        
        $request_id = \common\components\GuestInfo::getParam("request_id");
        if(!$request_id){
            $this->error("找不到相关的需求");
        }
        if(!$this->is_ok($request_id)){
            $this->error("不允许操作");
        }
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }
    
    
    protected function is_ok($request_id)
    {
        if(!$this->user->isSuper()){
            $studio_id = \common\models\hy_designerModel::getOne(\common\models\hy_member_requestModel::getOne($request_id, "service_uid"), "studio_id");//找到是属于哪一个团队的
            if(\common\models\hy_designerModel::getOne($this->user->frontend_uid, "studio_id") == $studio_id){
                return true;
            }else{
                $this->error("不允许操作");
            }
        }else{
            return true;
        }         
    }
}
