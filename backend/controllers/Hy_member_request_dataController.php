<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_member_request_dataController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_member_request_dataModel();
    }

    public function actionIndex()
    {
        $request_id = \common\components\GuestInfo::getParam("request_id");
        $this->is_ok($request_id);        
        
        
        
        //把所有数据找出来
        $condition = [];
        $condition['noget'] = true;
        $condition['pagesize'] = 999999;
        $condition['request_id'] = $request_id;
        $datas = \common\models\hy_member_request_dataModel::baseGetDatas($condition);
        
        $result = [];
        foreach ($datas as $key => $data){
            $result[$data['type']][] = $data;
        }
        $this->assign("datas", $result);
        $this->assign("request_id", $request_id);
        $this->assign("types", \common\models\hy_categoryModel::getTypeHtml());
        
        return $this->render();
    }
    
    //找出可以添加的来，然后多选添加
    public function actionCreate()
    {
        $request_id = \common\components\GuestInfo::getParam("request_id");
        $this->is_ok($request_id);        
        
        if($_POST){
            $type_id = \common\components\GuestInfo::getParam("type_id");
            if(!$type_id){
                $this->error("请选择增加分类");
            }
            foreach ($type_id as $key => $value){
                $obj = new \common\models\hy_member_request_dataModel();
                $obj->type_id = $value;
                $obj->title = \common\models\hy_categoryModel::getOne($value, "title");
                $obj->sel_uid = $this->user->frontend_uid;
                $obj->request_id = \common\components\GuestInfo::getParam("request_id");
                $obj->type = \common\components\GuestInfo::getParam("type");
                $obj->save(false);
            }
            $this->success("处理成功");
        }else{
            //找出所有的分类来
            $condition = [];
            $condition['noget'] = true;
            $condition['pagesize'] = 99999;
            $condition['request_id'] = \common\components\GuestInfo::getParam("request_id");
            $condition['type'] = \common\components\GuestInfo::getParam("type");//一定是这个类型


            $notypes = \common\models\hy_member_request_dataModel::find()->where(['request_id' => \common\components\GuestInfo::getParam("request_id")])->select("type_id")->column();
            if($notypes){
                $ids = implode(",", $notypes);
               $condition[] = "id not in({$ids})"; 
            }
            $datas = \common\models\hy_categoryModel::baseGetDatas($condition);

            $result = [];
            foreach($datas as $key => $value){
                $result[$value['id']] = $value['title'];
            }
            $this->assign("datas", $result);   
            $this->assign("model", new \backend\models\CountForm());   
            return $this->render();
        }

        
        
        return $this->baseCreate($this->model_class);
    }

    public function actionDelete()
    {
        $request_id = \common\components\GuestInfo::getParam("request_id");
        $this->is_ok($request_id);        
        return $this->baseDelete(get_class($this->model_class),"index?request_id={$request_id}");
    }

    public function actionOver()
    {
        $request_id = \common\components\GuestInfo::getParam("request_id");
        $this->is_ok($request_id);     
        
        $_GET['over_time'] = time();
        $_POST['ok'] = true;
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    
    protected function is_ok($request_id)
    {
        if(!$this->user->isSuper()){
            $studio_id = \common\models\hy_designerModel::getOne(\common\models\hy_member_requestModel::getOne($request_id, "service_uid"), "studio_id");//找到是属于哪一个团队的
            if(\common\models\hy_designerModel::getOne($this->user->frontend_uid, "studio_id") == $studio_id){
                return true;
            }else{
                $this->error("不允许操作");
            }
        }else{
            return true;
        }         
    }
}
