<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_member_request_data_recordController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_member_request_data_recordModel();
    }

    public function actionIndex()
    {
        $data_id = \common\components\GuestInfo::getParam("data_id");
        $this->is_ok($data_id);        
        
        
        return $this->baseIndex($this->model_class);
    }

    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }

    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }

    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    
    protected function is_ok($data_id)
    {
        if(!$this->user->isSuper()){
            $request_id = \common\models\hy_member_request_data_recordModel::getOne($data_id, "request_id");
            $studio_id = \common\models\hy_designerModel::getOne(\common\models\hy_member_requestModel::getOne($request_id, "service_uid"), "studio_id");//找到是属于哪一个团队的
            if(\common\models\hy_designerModel::getOne($this->user->frontend_uid, "studio_id") == $studio_id){
                return true;
            }else{
                $this->error("不允许操作");
            }
        }else{
            return true;
        }         
    }
}
