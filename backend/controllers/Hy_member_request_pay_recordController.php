<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_member_request_pay_recordController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_member_request_pay_recordModel();
    }

    public function actionIndex()
    {
        $request_id = \common\components\GuestInfo::getParam("request_id");
        $this->is_ok($request_id);           
        return $this->baseIndex($this->model_class);
    }

    public function actionCreate()
    {
        if($_POST){
            $datas = $_POST['h_member_request_pay_recordModel'];
            $datas = \common\models\h_member_request_pay_recordModel::rechargePay($datas['money'], $datas['title'], \common\components\GuestInfo::getParam("request_id"));
            if($datas){
                $this->success("支付需求创建成功","index?request_id={$_SESSION['request_id']}");
            }else{
                $this->error("创建失败");
            }
        }else{
            $is_data = \common\models\h_member_request_pay_recordModel::find()->where(['request_id' => $_SESSION['request_id'],'status' => 0])->limit(1)->one();
            if($is_data){
                $this->error("还存在未支付的记录,占时无法创建支付","index?request_id={$_SESSION['request_id']}");
            }
        }
        
        return $this->baseCreate($this->model_class,"index?request_id={$_SESSION['request_id']}");
    }

    public function actionUpdate()
    {
        $model = \common\models\h_member_request_pay_recordModel::findOne(\common\components\GuestInfo::getParam("id"));
        if($model && $model['status']){
            $this->error("已经付完款了，不允许再修改","index?request_id={$_SESSION['request_id']}");
        }
        return $this->baseUpdate(get_class($this->model_class),"index?request_id={$_SESSION['request_id']}");
    }

    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class),"index?request_id={$_SESSION['request_id']}");
    }
    
    public function actionUpdate_post()
    {
        $_POST['ok'] = true;
        return $this->baseUpdate(get_class($this->model_class),"index?request_id={$_SESSION['request_id']}");
    }
    
    
    protected function is_ok($request_id)
    {
        if(!$this->user->isSuper()){
            $studio_id = \common\models\hy_designerModel::getOne(\common\models\hy_member_requestModel::getOne($request_id, "service_uid"), "studio_id");//找到是属于哪一个团队的
            if(\common\models\hy_designerModel::getOne($this->user->frontend_uid, "studio_id") == $studio_id){
                return true;
            }else{
                $this->error("不允许操作");
            }
        }else{
            return true;
        }         
    }
}
