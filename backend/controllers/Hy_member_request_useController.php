<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_member_request_useController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_member_request_useModel();
    }

    public function actionIndex()
    {
        $request_id = \common\components\GuestInfo::getParam("request_id");
        if(!$request_id){
            $this->error("请指定相关的用户的需求ID");
        }
        
        $_GET['studio_id'] = \common\models\hy_designerModel::getOne(\common\models\hy_member_requestModel::getOne($request_id, "service_uid"), "studio_id");//找到是属于哪一个团队的
        
        return $this->baseIndex($this->model_class);
    }
    
    
    
    protected function is_can_delete($request_id)
    {
        if($this->user->isSuper()){
            return true;
        }
        //如果是我自己发起的，创建的则可以进行相关的操作
        if(\common\models\hy_member_requestModel::getOne($request_id, "service_uid") == $this->user->frontend_uid){
            return true;
        }
        
        //如果我是这个团队的核心人员才是可以的  管理人员
        $studio_id = \common\models\hy_designerModel::getOne(\common\models\hy_member_requestModel::getOne($request_id, "service_uid"), "studio_id");//找到是属于哪一个团队的
        if(\common\models\hy_studioModel::getOne($studio_id, "founder_uid") == $this->user->frontend_uid){
            return true;
        }
        return false;
    }
    
    
    protected function is_can_add($request_id)
    {
        if($this->user->isSuper()){
            return true;
        }
        
        
        //如果我是这个团队的核心人员才是可以的  管理人员
        $studio_id = \common\models\hy_designerModel::getOne(\common\models\hy_member_requestModel::getOne($request_id, "service_uid"), "studio_id");//只要是同一个工作室的都允许操作
        if(\common\models\hy_designerModel::getOne($this->user->frontend_uid, "studio_id") == $studio_id){
            return true;
        }
        return false;
    }




    public function actionDelete()
    {
        
        $ids = \common\components\GuestInfo::getParam("id");
        if($ids){
            $uid = [];
            $request_id = null;
            foreach($ids as $key => $value){
                $dd = \common\models\hy_member_request_useModel::getOne($value);
                $request_id = $dd['request_id'];
                $uid[] = $dd['uid'];
            }
            
        }else{
            $uid = \common\components\GuestInfo::getParam("uid");
            $request_id = \common\components\GuestInfo::getParam("request_id");        
        }
        
        
        
        //检查权限
        $is_ok = $this->is_can_delete($request_id);
        if(!$is_ok){
            $this->error("不允许操作");
        }
        
        
        
        $obj = new \common\components\newfs\CreateUpdateRequest_use();
        if(!is_array($uid)){
            $kid = $uid;
            $uid = [];
            $uid[] = $kid;
        }
        $obj->uids = $uid;
        $obj->type = 2;
        $obj->request_id = $request_id;
        $state = $obj->run();
        if(!$state){
            $this->error($obj->error);
        }else{
            $this->success("删除成功");
        }
    }
    
    
    public function actionAdd_uid()
    {
        
        $uid = \common\components\GuestInfo::getParam("id");
        if(!$uid){
            $uid = \common\components\GuestInfo::getParam("uid");
        }
        
        $request_id = \common\components\GuestInfo::getParam("request_id");   
        
        
        //检查权限
        $is_ok = $this->is_can_delete($request_id);
        if(!$is_ok){
            $this->error("不允许操作");
        }        
        
        
        $obj = new \common\components\newfs\CreateUpdateRequest_use();
        if(!is_array($uid)){
            $kid = $uid;
            $uid = [];
            $uid[] = $kid;
        }
        $obj->uids = $uid;
        $obj->type = 1;
        $obj->request_id = $request_id;
        $state = $obj->run();
        if(!$state){
            $this->error($obj->error);
        }else{
            $this->success("添加成功");
        }        
    }
    
    //找到这工作室的员工来 可以把哪些员工拉入进去
    public function actionPublic_list_user()
    {
        $studio_id = \common\components\GuestInfo::getParam("studio_id");  
        $request_id = \common\components\GuestInfo::getParam("request_id");  
        $datas = \common\models\hy_member_request_useModel::getBaseKeyValue("uid", "username", ['request_id' => $request_id]);//本身有谁了
        
        $uids_key = array_keys($datas);
        $str_uids = implode(",", $uids_key);
        
        $this->assign("studio_id", $studio_id);
        $this->assign("request_id", $request_id);
        
        $this->model_class = new \common\models\hy_designerModel();
        $this->model_class->studio_id = $studio_id;
        $dataProvider = $this->baseIndex($this->model_class,[],10000,[],true);
        
        if($datas){
            $query = $dataProvider->query;
            $query->andWhere("uid not in({$str_uids})");            
        }

        
        $this->assign('object', get_class($this->model_class));   
        $this->assign(['dataProvider' => $dataProvider]);
        $this->assign(['model' => $this->model_class]);
        return $this->render("public_list_user");            
    }
    
}
