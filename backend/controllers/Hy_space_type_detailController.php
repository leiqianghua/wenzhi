<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_space_type_detailController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_space_type_detailModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }

    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }

    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }

    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}
