<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class Hy_studioController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\hy_studioModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }

    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }

    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }

    public function actionDelete()
    {
        if($_POST){
            $m = \common\models\hy_designerModel::findOne(['studio_id' => \common\components\GuestInfo::getParam("id")]);
            if($m){
                $this->error("此工作室底下有员工,请删除所有员工后再进行删除工作室操作");
            }
        }
        return $this->baseDelete(get_class($this->model_class));
    }
}
