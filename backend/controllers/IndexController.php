<?php
namespace backend\controllers;
use Yii;

class IndexController extends \backend\components\BaseController
{
    public function actionIndex()
    {
        //$menu = \backend\models\AdminMenuModel::getNeedMenu();
        
        //print_r($menu);exit;
        
        //print_r(\backend\models\AdminMenuModel::getTableSchema());exit;
        $this->layout = false;
        $menu = \backend\models\AdminMenuModel::getNeedMenu();
        $this->assign('menu',$menu);
        $this->assign('user',$this->user->member_info);
        return $this->render();
    }
    
    public function actionOk()
    {
        $this->layout = false;
        return $this->render('ok');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect('/public/login');
        //return $this->goHome();
    } 
    
    public function actionCeshi()
    {
        
        $ab = \common\models\hy_designerModel::findAllXiaji(244);
        print_r($ab);exit;
        
        if(!empty($_POST['table'])){
            $sql = "SHOW CREATE TABLE `{$_POST['table']}`";
            $code=\yii::$app->db->createCommand($sql)->queryOne();
            if($code){
                $table = $code['Create Table'];
                $table = explode("\n", $table);
                $result = [];
                foreach($table as $key => $value){
                    if($key == 0){
                        continue;
                    }
                    preg_match('/`(.*)`/', $value,$ab);
                    if(isset($ab[1])){
                        preg_match("/COMMENT '(.*)'/", $value,$abc);
                        if(empty($result[$ab[1]])){
                            $result[$ab[1]] = isset($abc[1]) ? $abc[1] : "";
                        }
                    }
                }
                $this->assign("result",$result);
            }
        }
        
        return $this->render('ceshi');
    }
    
    
    public function actionCreate_table()
    {
        $this->layout = "main";
        if(!$_POST){
            $this->assign("model",new \backend\models\CountForm());
            return $this->render();            
        }else{
            $content = $_POST['content'];
            $arr_content = explode("\n", $content);
            $arr_params = [];
            foreach($arr_content as $key => $value){
                if($key == 0){
                    continue;
                }
                if($value == ")"){
                    break;
                }
                $string_to_search = $value;
                $regex = "/`(.*)`/";
                $num_matches = preg_match($regex, $string_to_search,$matches);
                $k = $matches[1];
                
                
                $regex_1 = "/COMMENT '(.*)'/";
                $comment = preg_match($regex_1, $string_to_search,$matches_1);
                if(!empty($matches_1[1])){
                    $arr_params[$k] = $matches_1[1];
                }else{
                    continue;
                    $arr_params[$k] = "";
                }
               // print_r($arr_params);exit;
            }
            print_r($arr_params);exit;
        }
    }
    
    
    
    public function actionCreate()
    {
        $this->layout = "main";
        if(!$_POST){
            $this->assign("model",new \backend\models\CountForm());
            return $this->render();
        }else{
            $controller = \common\components\GuestInfo::getParam("controller");
            $str_model = $model = \common\components\GuestInfo::getParam("model");
            $has_data = \common\components\GuestInfo::getParam("has_data");
            //$model = ucwords($model);
            if(stripos($model, "\\") === false){
                if($has_data){
                    $model = '\\common\\models\\' . $has_data . "\\" . $model;
                }else{
                    $model = '\\common\\models\\' . $model;
                }
                
            }
            if(!class_exists($model)) {//用便捷方式找模型
                $length = strlen($model);
                for($i = 0;$i < $length;$i++){
                    if($model[$i] == "_"){
                        $j = $i + 1;
                        if($j < $length){
                            //$model[$j] = ucwords($model[$j]);
                        }
                    }
                }
                //$model = str_replace("_", "", $model);
                $model = $model . "Model";
                
            }       
            //echo $model;exit;
            if(!class_exists($model)) {
                $this->error("不是一个对象");
            }else{
                if(!method_exists($model, "tableName")){
                    $this->error("指定对象错误");
                }
            }

            
            $controller_filename = "../controllers/" . ucwords($controller) . "Controller.php";//控制器文件名

            $view_file = "../views/" . $controller;//V图层文件夹

            $this->createController($controller_filename, $controller, $model);
            $this->createView($view_file,$model);     
            $this->success("添加成功");
        }
    }
    
    // $controller_filename 控制器的路径 $controller控制器名称  $model 用到的模型
    public function createController($controller_filename,$controller,$model)
    {
        if(!file_exists($controller_filename)){//创建控制器
            $html = 
'<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
class '.ucwords($controller).'Controller extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new '.$model.'();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }

    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }

    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }

    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}';
            $handle = fopen($controller_filename, 'a');
            fwrite($handle, $html . "\n");
            fclose($handle);
        }        
    }
    
    //$view_file 文件夹名称
    public function createView($view_file,$model)
    {
        if(!file_exists($view_file)){//如果没有这文件夹则创建
            \common\components\TmpLog::createDir($view_file);
            $form_filename = $view_file . "/_form.php";
            $create_filename = $view_file . "/create.php";
            $index_filename = $view_file . "/index.php";
            $update_filename = $view_file . "/update.php";   
            
            $html = '<?php include \'_form.php\';  ?>';
            $handle = fopen($create_filename, 'a');
            fwrite($handle, $html . "\n");
            fclose($handle);   
            $handle = fopen($update_filename, 'a');
            fwrite($handle, $html . "\n");
            fclose($handle);  
            
            $html = 
'<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                \'options\' => [\'class\' => \'J_ajaxForm form-horizontal\'],
                \'enableClientScript\' => false,
                \'fieldConfig\' => [
                    \'template\' => "<div class=\'col-xs-3 col-sm-2 text-right\'>{label}</div><div class=\'col-xs-6 col-sm-4\'>{input}</div><div style=\'padding-top:7px;\' class=\'col-xs-3 col-sm-2\'>{hint}</div>",
                ]        
        ]);
    ?>
    
    '.$this->createFormData($model).'

    <div class="form-group">
        <div class=\'col-xs-3 col-sm-4 text-right\'>
        <?= Html::button($model->isNewRecord ? \'添加\' : \'修改\', [\'class\' => $model->isNewRecord ? \'J_ajax_submit_btn btn btn-success\' : \'J_ajax_submit_btn btn btn-primary\']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>';
            $handle = fopen($form_filename, 'a');
            fwrite($handle, $html . "\n");
            fclose($handle); 
            
            $html = 
'<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>列表示例</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                \'encodeErrorSummary\' => false,
                \'enableClientScript\' => false,
                \'fieldConfig\' => [\'errorOptions\' => []],
                \'method\' => \'get\',
                \'action\' => \'index\',//这时可能会有变化
                \'options\' => [\'class\' => \'form-inline J_ajaxForm\'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET[\'start_add_time\']) ? \'\' : $_GET[\'start_add_time\'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET[\'end_add_time\']) ? \'\' : $_GET[\'end_add_time\'],"","J_date length_2");  ?>            
            
            <div class="form-group">
                <?= Html::submitButton(\'搜索\', [\'class\' => \'btn btn-primary\']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        \'dataProvider\' => $dataProvider,  //固定不变
        \'summary\' => \'总共{totalCount}条数据\',   //固定不变
        \'columns\' => [
            [\'class\' => \'yii\grid\CheckboxColumn\'], //全选
            
            '.$this->createIndexData($model).'

            [\'class\' => \'yii\grid\ActionColumn\',"template" => \'{view} {update} {delete}\'],    
        ],
    ]); ?>

    <?= Html::button(\'删除\',[\'class\' => \'select_list btn\',\'url\' => $this->context->to(["delete"])]) ?>

</div>';
            
            $handle = fopen($index_filename, 'a');
            fwrite($handle, $html . "\n");
            fclose($handle); 
            
        }
        
    }
    
    //找到这个模型的所有的字符来
    public function createFormData($model_str)
    {
        $show_html = "";
        $fields = $model_str::getTableSchema()->columns;
        if($fields){
            foreach($fields as $field => $obj){//字段名  对应  相应的格式数据
                if($field == "add_time" || $field == "update_time" || $field == "id"){
                    continue;
                }
                if(stripos($field, "img") !== false || stripos($field, "image") !== false){//说明是用图片的
                    $html = '<?php  $date = date(\'Ymd\'); ?>';
                    $html .= "\r\n";
                    if(stripos($field, "list") !== false){
                        $html .= '<?= Form::getFileContent($form,$model,\''.$field.'\',\''.$field.'\', $date . \'/list\',\'\',[],\'\',true); ?>';
                    }else{
                        $html .= '<?= Form::getFileContent($form,$model,\''.$field.'\',\''.$field.'\', $date . \'/dan\'); ?>';
                    }               
                }else if(stripos($field, "time") !== false){
                    $html = '<?= $form->field($model, \''.$field.'\')->textInput([\'class\' => \'form-control J_date length_3\']) ?>';
                }else if(substr ($obj->dbType, 0,7) == 'tinyint'){
                    $html = '<?= $form->field($model, \''.$field.'\')->textInput() ?>';
                }else if(substr ($obj->dbType, 0,3) == 'int'){
                    $html = '<?= $form->field($model, \''.$field.'\')->textInput() ?>';
                }else if(substr ($obj->dbType, 0,7) == 'varchar'){
                    $html = '<?= $form->field($model, \''.$field.'\')->textInput() ?>';
                }else if(substr ($obj->dbType, 0,4) == 'text'){
                    $html = '<?= $form->field($model, \''.$field.'\')->textInput() ?>';
                }else{
                    $html = '<?= $form->field($model, \''.$field.'\')->textInput() ?>';
                }
                $show_html .= "\r\n" . $html;
            }
        }
        return $show_html;
    }
    
    public function createIndexData($model_str)
    {
        $show_html = "";
        $fields = $model_str::getTableSchema()->columns;
        if($fields){
            foreach($fields as $field => $obj){//字段名  对应  相应的格式数据
                $html = "'{$field}',";
                $show_html .= "\r\n" . $html;
            }
            
        }
        return $show_html;
    }   
    
    public function actionUploadsdk()
    {
        if($_POST){
            $name = \common\components\GuestInfo::getParam("filename");
            if(!preg_match('/^\w+$/', $name)){
                $this->error("文件命名不符合要求");
            }
            if(!isset($_FILES["sdk"])){
                $this->error("请上传相应文件");
            }
            $file = $_FILES["sdk"];
            $filePath = $name . ".apk";
            $filePath = "../../frontend_platform/web/" . $filePath;
            if (!(move_uploaded_file($file["tmp_name"], $filePath))) { //移动失败
                $this->error("上传失败");
            } else { //移动成功
                $this->success("上传成功");
            }            
        }else{
            $model = new \backend\models\CountForm();
            $this->assign("model", $model);
            return $this->render();
        }
    }    
}