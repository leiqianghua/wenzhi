<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 菜单表
 */
class LogController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \backend\models\AdminLogModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }
    
//    public function actionCreate()
//    {
//        return $this->baseCreate($this->model_class);
//    }
//    
//    public function actionUpdate()
//    {
//        return $this->baseUpdate(get_class($this->model_class));
//    }
//    
//    public function actionDelete()
//    {
//        return $this->baseDelete(get_class($this->model_class));
//    }
}
