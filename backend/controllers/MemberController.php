<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 菜单表
 */
class MemberController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\MemberModel();
    }

    public function actionIndex($limit = 20)
    {
        if(isset($_GET['province_id']) && $_GET['province_id'] == 0){
            unset($_GET['province_id']);
        }
        if(isset($_GET['city_id']) && $_GET['city_id'] == 0){
            unset($_GET['city_id']);
        }
        return $this->baseIndex($this->model_class,[],$limit,[],"index");
    }
    
    public function actionIndex_excel()
    {
        $this->layout = false;
        $datas = $this->actionIndex(99999);
        
        //print_r($this->tVar['sum_money']);exit;
        $other_data = [];
        //$other_data[0][0] = "总提现：300万";
        
        $obj = new \common\components\Excel_Download(['title' => "会员列表","download_data" => $datas,"other_data" => $other_data]);
        $obj->run();
        exit;                 
    }
    

    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    public function actionTihuan()
    {
        $t_uid = \common\components\GuestInfo::getParam('t_uid','');
        $uid = \common\components\GuestInfo::getParam('uid','');
        $state = \common\models\MemberModel::dinti($t_uid,$uid);
        if($state){
            $this->success('替换成功');
        }else{
            $this->error(\common\models\MemberModel::$error);
        }
    }
    
    public function actionView($uid)
    {
        return $this->render('view', [
            'model' => $this->findModel($uid),
        ]);
    }
    protected function findModel($id)
    {
        if (($model = \common\models\MemberModel::findOne($id)) !== null) {
            return $model;
        } else {
            $this->error("找不到相关数据");
        }
    }      
    
    public function actionAjax_ok()
    {
       $model =  \backend\models\AdminMemberModel::findOne(['frontend_uid' => \common\components\GuestInfo::getParam("uid")]);
       if($model){
           exit($model['username']);
       }else{
           exit("no_data");
       }
    }
}
