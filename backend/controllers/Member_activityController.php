<?php
namespace backend\controllers;
use backend\components\BaseController;
//活动相关
class Member_activityController extends BaseController
{
    public function actionOp()
    {
        $time = time();
        $condition[] = "start_time >= {$time} and end_time <= {$time}";
        
        $datas = \common\models\ActivityCouponModel::baseGetDatas($condition);
        $this->assign('datas', $datas);
        return $this->render();
    }
}
