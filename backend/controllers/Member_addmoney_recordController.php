<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
/*
 * 菜单表
 */
class Member_addmoney_recordController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\MemberAddmoneyRecordModel();
    }

    public function actionIndex($limit = 20)
    {
        return $this->baseIndex($this->model_class,[],$limit,[],"index");
    }
    
    public function actionCreate()
    {
//        $data = yii::$app->request->hostinfo;
//        print_r($data);exit;
//        $data = \yii::$app->getComponents();
//        print_r($data);exit;
//        //获取目录
//        print_r(\yii::$aliases);exit;
//        echo \yii::getAlias('@web');exit;
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}
