<?php
namespace backend\controllers;
use backend\components\BaseController;

class Member_applyController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\MemberApplyModel();
    }

    public function actionIndex()
    {
        $_GET['status'] = 0;
        return $this->baseIndex($this->model_class);
    }
    

    
    public function actionUpdate()
    {
        $model = \common\models\MemberApplyModel::findOne(\common\components\GuestInfo::getParam('id', 0));
        if(!$model){
            $this->error('找不到数据');
        }else{
            $status = \common\components\GuestInfo::getParam('status');
            if($model['status'] != 0){
                $this->error('已经处理过了');
            }
            if($status == 1){//同意
                $member_model = \common\models\MemberModel::findOne($model['uid']);
                if(!$member_model){
                    $model->status = 2;
                    $model->save();                    
                    $this->error('找不到此会员信息,已被拒绝申请');
                }else{
                    $member_model->label = $model['label'];
                    $state = $member_model->save();
                    if($state){
                        $model->status = 1;
                        $model->save();
                        $this->success('处理成功');                        
                    }else{
                        $this->error($member_model->getOneError());                          
                    }
                }
            }else{//不同意
                $model->attributes = $_POST;
                $model->status = 2;
                $model->save();
                $this->success('处理成功');
            }
        }
    }
    
}
