<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
/*
 * 案例
 */
class Member_caseController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\MemberCaseModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    protected function findModel($id)
    {
        if (($model = \common\models\MemberCaseModel::findOne($id)) !== null) {
            return $model;
        } else {
            $this->error("找不到相关数据");
        }
    }    
    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionOk()
    {
        $_GET['status'] = 1;
        if($_POST){
            $model = \common\models\MemberCaseModel::findOne($_GET['id']);
            $score = 10 * $_POST['ok_du'];
            \common\models\Score_detailModel::changeScore(1, $score, "上传案例库赠送", $_GET['id'], $model['uid']);
        }
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}
