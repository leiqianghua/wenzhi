<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 商品
 */
class Member_cashController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\MemberCashModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }
    //同意其提现
    public function actionOk()
    {
        $model = \common\models\MemberCashModel::findOne($_GET['id']);
        if($model){
            $model->status = 1;
            $model->save(false);
            \common\models\MemberMoneyRecordModel::changeMoney(-1, $model['money'], '提现', $model['id'],$model['uid']);
            $this->success('处理成功');         
        }else{
            $this->error('处理失败');
        }    
    }
    //拒绝其提现
    public function actionChannel()
    {
        $_POST['status'] = 3;
        return $this->baseUpdate(get_class($this->model_class));
    }
}
