<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 商品
 */
class OrderController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\OrderModel();
    }

    public function actionIndex($limit = 20)
    {
        return $this->baseIndex($this->model_class,[],$limit,[],"index");
    }
    //订单商品
    public function actionIndexgoods()
    {
        $menu_return['name'] = "订单列表";
        $menu_return['url'] = empty($_SERVER['HTTP_REFERER']) ? $this->to(['index']) : $_SERVER['HTTP_REFERER'];
        \yii::$app->cache->set('menu_return', $menu_return);
        $model = new \common\models\OrderGoodsModel();
        return $this->baseIndex($model);
    }
    
    //发货
    public function actionUpdate()
    {
        $_POST['order_state'] = 2;
        return $this->baseUpdate(get_class($this->model_class));
    }
}
