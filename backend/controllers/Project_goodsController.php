<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 商品
 */
class Project_goodsController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\Project_goodsModel();
    }

    public function actionIndex($limit = 20)
    {
        return $this->baseIndex($this->model_class,[],$limit,[],"index");
    }
    
    public function actionSelect_index()
    {
        return $this->baseIndex(new \common\models\Project_goodsModel());
    }
    
    
    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
}
