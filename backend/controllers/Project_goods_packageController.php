<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 商品
 */
class Project_goods_packageController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\Project_goods_packageModel();
    }

    public function actionIndex($limit = 20)
    {
        
        
        
        return $this->baseIndex($this->model_class,[],$limit,[],"index");
    }
    
    public function actionCreate()
    {
        if($_POST){
            $price = 0;
            foreach ($_POST['shop_list'] as $cid => $value) {
                foreach($value as $k => $v){
                    $_POST['shop_list'][$cid][$k]['total'] = $_POST['shop_list'][$cid][$k]['price']*$_POST['shop_list'][$cid][$k]['num'];
                    if(!$_POST['shop_list'][$cid][$k]['total']){
                        $this->error("数据填写错误");
                    }else{
                        $price += $_POST['shop_list'][$cid][$k]['total'];
                    }
                }
            }
            
            if(empty($_POST['Project_goods_packageModel']['price'])){
                $_POST['Project_goods_packageModel']['price'] = $price;
            }
        }else{
            $this->model_class->cid = \common\components\GuestInfo::getParam("type");
            //这个分类的所有的子分类拿出来
            
            $query = \common\models\Project_classModel::find()->where(['parent_id' => $this->model_class->cid])->limit(10000);
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $query,
            ]);
            $dataProvider->pagination->pageSize = 10000;
            $dataProvider->setTotalCount(10000);
            
            $this->assign("dataProvider", $dataProvider);
        }
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        if($_POST){
            $price = 0;
            foreach ($_POST['shop_list'] as $cid => $value) {
                foreach($value as $k => $v){
                    $_POST['shop_list'][$cid][$k]['total'] = $_POST['shop_list'][$cid][$k]['price']*$_POST['shop_list'][$cid][$k]['num'];
                    if(!$_POST['shop_list'][$cid][$k]['total']){
                        $this->error("数据填写错误");
                    }else{
                        $price += $_POST['shop_list'][$cid][$k]['total'];
                    }
                }
            }
            if(empty($_POST['Project_goods_packageModel']['price'])){
                $_POST['Project_goods_packageModel']['price'] = $price;
            }
            
        }else{
            $m = \common\models\Project_goods_packageModel::findOne(\common\components\GuestInfo::getParam("id"));
            $_GET['cid'] = $m['cid'];
            //这个分类的所有的子分类拿出来
            
            $query = \common\models\Project_classModel::find()->where(['parent_id' => $m['cid']])->limit(10000);
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $query,
            ]);
            $dataProvider->pagination->pageSize = 10000;
            $dataProvider->setTotalCount(10000);
            
            $this->assign("dataProvider", $dataProvider);
        }      
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    public function actionAjax_select_goods()
    {
        error_reporting(0);
        $shop_ids = $_POST['shop_ids'];
        //组成我们要的东西
        $datas = \common\models\Project_goodsModel::find()->where(['id' => $shop_ids])->asArray()->all();
        
        $_POST['shop_ids'] = $datas;
        
        $_POST['name_show'] = "shop_list[{$datas[0]['cid']}]";
        
        $content = $this->renderPartial("_shop_select", $_POST);
        $this->ajaxReturn(['content' => $content]);
        
    }
}
