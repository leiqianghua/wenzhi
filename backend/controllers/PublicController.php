<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;

/*
 * 用户相关控制器
 */
class PublicController extends BaseController
{
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    
    public function actionUpload()
    {
        if(!$_POST){
            $model = new \backend\models\CountForm();
            $this->assign("model", $model);
            return $this->render();            
        } 
        //下载下来
//        $content = file_get_contents($_FILES["file"]['tmp_name']);
//        echo $content;
//        exit;
        
        $file_name = $_FILES["file"]["tmp_name"];
        $ext = explode('.', $_FILES["file"]["name"]);
        $count = count($ext) - 1;
        $ext = $ext[$count];
        \common\components\Excel_upload::$file_name = $file_name;
        $datas = \common\components\Excel_upload::getArrData($ext);//然后把这个转为数组。 
        
        $result = [];
        $result_arr = [];
        foreach($datas  as $key => $value){
            $arr = explode("-", $value["A"]);
            
            if(count($arr) != 6){
                continue;
            }
            
            $order_id = $arr[0] . "-" . $arr[1];
            $store_name = $arr[2];
            $price = $arr[3];
            $date = $arr[5];
            $datef = explode(" ", $date)[0];
            
            $all_all = [];
            $all_all['日期'] = $datef;
            $all_all['店铺'] = $store_name;
            $all_all['订单'] = $order_id;
            $all_all['价格'] = $price;
            $all_all['佣金'] = 4;
            
            $result_arr[$store_name][$datef][] = $all_all;
        }
        
        //print_r($result_arr);exit;
        foreach($result_arr as $store_name => $value){
            foreach($value as $date => $v){
                foreach($v as $kk => $vv){
                    $result[] = $vv;
                }
            }
        }
        //print_r($result);exit;
        $title = date("m.d",time());
        return $this->actionCeshi($result, $title);
    }
    
    
    /**
     * 完全的通过arr数组来其展示数据的一个示范
     */
    public function actionCeshi()
    {
        $obj = new \common\components\Curl();
        $address = "https://mms.pinduoduo.com/sydney/api/goodsDataShow/queryGoodsPageRT";
        
        $header = [
            "Accept: application/json",
            'Accept-Language: zh-CN,zh;q=0.9',
//                'Accept-Encoding: gzip, deflate',
            'Connection: keep-alive',
            'Content-Type: application/json;charset=UTF-8',
            'Host: mms.pinduoduo.com',
            'Connection: Keep-Alive',
            'Origin: https://mms.pinduoduo.com',
            'Referer: https://mms.pinduoduo.com/sycm/goods_effect',
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36",
            'Cookie: api_uid=rBQhwlyDVhoaZC3IHSs0Ag==; _nano_fp=XpdyX5gan0dalpXqX9_8KiHz40oZdS5yHg8g7H4x; newUserTag29579922=1; PASS_ID=1-WMp53aLt/cY/bivpqTt5YkIm1DDoQd4rw4MkHHiwuGv+yMQwmLJqkGZiVuavzDdnne79Qbq17BODhRDpO82AzQ_868187864_29579922',
        ];
        $fields = [];
        //$fields['crawlerInfo'] = "0anAfxnpHslCY9dW4a10RY00je5QG_ao-beu1UNBTi_y6KKUaPhqTfGPTz7sKSzpM396iYAR94pxdox-e-X2T0z0NZSpoxuzYnK09pOHrArDAh-j7zI1M61JZHNkTXMYe7DugMFLIwl-38tXKzY9gndb5em8uSoiPeLvtCLBK292nqMM6cmAl_fiKaaWYESEx_9Bbp6x9fJTszfgbEx1MalkmDEvffXeGXCZjM1ZdwPLHgylZwze3_7wCDSmI02BHPeOkKe3IJS_iM30HB55p-1qo8izBnt60G6ac2H5Zim5WsG7eu1SifsYD1ifaxQJNgZ74RWfLCHNC1R1z_haRCyQiKtcmPXlKJTatsHZFmxUrWZIoEpy0z51g3LxnRfGbGtHSJlYzoDGP1oeYwRKZcR48W2pSpHEOtMNORK7iWgD4E"; 
        $result = $obj->post($address, $fields, "", $header);
        print_r($result);exit;
        
        
        
        
        
        
        
//        $datas = [
//            [
//                'a' => "11",
//                'b' => "22",
//            ],
//            [
//                'a' => "33",
//                'b' => "44",
//            ]
//        ];
        $dataProvider = new \yii\data\ArrayDataProvider();
        $dataProvider->allModels = $datas;
        $dataProvider->setTotalCount(count($datas));
        $dataProvider->pagination->totalCount = count($datas);
        $dataProvider->pagination->pageSize = 999999999;
        
        $this->assign("dataProvider",$dataProvider);    
        $datas = $this->render("ceshi");
        
        
        $this->layout = false;
        //$other_data = [];
        //$other_data[0][0] = "总提现：300万";
        
        
        $config = [ 
            '1' => 20,
            '2' => 30,
            '3' => 40,
            '4' => 10,
            '5' => 15,
            '6' => 15,
        ];
        
        $obj = new \common\components\Excel_Download(['title' => $title,"download_data" => $datas,"other_data" => $other_data,"config_size" => $config]);
        
        $obj->run();
        exit;           
    }
    
    public function actionLogin()
    {
        if($this->user->uid){
            $this->redirect('/index/index');
        }
        $this->layout = false;
        if($_POST){
            $datas['username'] = $_POST['username'];
            $datas['password'] = $_POST['password'];
            $state = $this->user->login($datas);
            if($state){
                $this->redirect('/index/index');
            }else{
                $this->error($this->user->error);
            }
        }else{
            return $this->render();
        }
    }
    //刷新相关缓存
    public function actionReset()
    {
        \yii::$app->cache->flush();
        $this->success('刷新成功');
    }
    
    //刷新结构 $table为模型的路径
    public function actionDb($table = '')
    {
        if($table){
            $table_name = $table::tableName();
            $db = \yii::$app->db->getSchema()->refreshTableSchema($table_name);
        }else{
            $db = \yii::$app->db->getSchema()->refresh();
        }
        $this->success('刷新成功');
    }    
    
    public function actionMemcached() {
        // 获取所有的key
        $cache = \yii::$app->cache;
        $cache = $cache->getMemcache ();
        $data = $cache->getstats ( );
        print_r($data);exit;
    }    
     public function actionRedis() {
        print_r ( \yii::$app->redis->isConnect () );
    }   
}
