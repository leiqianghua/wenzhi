<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 积分明细
 */
class Score_orderController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ScoreOrderModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}
