<?php
namespace backend\controllers;
use backend\components\BaseController;

/*
 * 申请处理
 */
class Seller_applyController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\SellerApplyModel();
    }

    public function actionIndex()
    {
        if(!isset($_GET['status'])){
            $_GET['status'] = 0;
        }
        return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate()
    {
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
    
    public function actionAgree()
    {
        $_POST['status'] = 1;//变成同意
        return $this->baseUpdate(get_class($this->model_class));
    }        
    public function actionRefuse()
    {
        $_POST['status'] = 2;//变成拒绝
        return $this->baseUpdate(get_class($this->model_class));
    } 
}
