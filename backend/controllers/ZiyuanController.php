<?php
namespace backend\controllers;
use backend\components\BaseController;
use yii;
/*
 * 菜单表
 */
class ZiyuanController extends BaseController
{
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ZiyuanModel();
    }

    public function actionIndex()
    {
        return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate()
    {
        if($_POST){
            if(empty($_POST['ZiyuanModel']['name'])){
                $this->error('保存名称不能为空');
            }
            if(isset($_POST['label'])){
                \yii::$app->redis->set("label_backend_select",$_POST['label']);
            }
            if(!empty($_FILES)){
                $obj = new \common\components\UploadFile();
                $result = $obj->upload("ziyuan", $_FILES, $_POST['ZiyuanModel']['name']);
                if(!$result){
                    $this->error = "保存文件失败";
                }else{
                    $_POST['url'] = $result['url'];
                }
            }
        }else{
            $this->model_class->status = 1;
            $label = \yii::$app->redis->get("label_backend_select");
            if($label){
                $this->model_class->label = $label;
            }
        }
        return $this->baseCreate($this->model_class);
    }
    
    public function actionUpdate()
    {
        if($_POST){
            if(empty($_POST['ZiyuanModel']['name'])){
                $this->error('保存名称不能为空');
            }
            if(!empty($_FILES)){
                $obj = new \common\components\UploadFile();
                $result = $obj->upload("ziyuan", $_FILES, $_POST['ZiyuanModel']['name']);
                if(!$result){
                    $this->error = "保存文件失败";
                }else{
                    $_POST['url'] = $result['url'];
                }
            }else{
                unset($_POST['url']);
            }
        }
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionAjaxupdate()
    {
        if(empty($_POST)){
            $_POST = [true];
        }
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionAjaxupdates()
    {
        if(empty($_POST)){
            $_POST = [true];
        }
        return $this->baseUpdates(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        return $this->baseDelete(get_class($this->model_class));
    }
}
