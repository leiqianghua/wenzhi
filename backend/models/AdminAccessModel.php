<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%admin_access}}".
 *
 * @property string $id
 * @property integer $menu_id
 * @property integer $role_id
 * @property integer $add_time
 */
class AdminAccessModel extends \backend\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_access}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'role_id', 'add_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => '菜单ID',
            'role_id' => '角色ID',
            'add_time' => '添加时间',
        ];
    }
    
    //通过数据来更改需要更改的参数
    //$datas 为菜单 ID的列表  
    //$role_id 为角色ID
    public static function updateMenu($datas,$role_id)
    {
        if(!$datas || !is_array($datas)){
            return false;
        }
        if(!$role_id){
            return false;
        }
        self::deleteAll(['role_id' => $role_id]);
        foreach($datas as $menu_id){
            $d = [];
            $d['menu_id'] = $menu_id;
            $d['role_id'] = $role_id;
            $d['add_time'] = TIMESTAMP;
            self::insert(false,$d);
        }
        return true;
    }
    
    
    //通过角色ID获取其支持的所有的菜单 
    public static function getAllForRoleID($role_id)
    {
        $result = [];
        $d = self::find()->where(["role_id" => $role_id])->asArray()->all();
        foreach($d as $value){
            $result[] = $value['menu_id'];
        }
        return $result;
    }
    
    
    //权限检查，判断用户是否有操作这个方法的权限
    //通过模块，控制器，方法，参数 来判断用户是否有权限
    public static function rbac($user,$controller,$action,$params = '')
    {
        //获取用户的角色
        $role_id = $user['role_id'];
        //超级管理员直接通过
        if(\backend\components\Member::SUPER_ADMIN == $role_id){
            return true;
        }
        
        //如果访问的是主页直接通过
        if(\yii::$app->controller->id == 'index'){
            return true;
        }
        
        //获取这个操作的菜单ID
        $where = [
            'controller' => $controller,
            'action' => $action,
            'params' => $params,
        ];
        $menu = AdminMenuModel::find()->where($where)->one();
        if(!$menu){
            return false;
        }
        $menu_id = $menu['id'];
        $data = self::find()->where(['menu_id' => $menu_id, 'role_id' => $role_id])->one();
        if($data){
            return true;
        }else{
            return false;
        }
    }
    
    //根据某个角色ID查询其所有的菜单
    public static function getDataForRole($role_id)
    {
        $where = ['role_id' => $role_id];
        $datas = self::find()->where($where)->indexBy('menu_id')->asArray()->all();
        return $datas;
    }    
    
}
