<?php

namespace backend\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%admin_log}}".
 *
 * @property string $id
 * @property string $admin_id
 * @property string $username
 * @property string $add_time
 * @property string $params
 * @property string $module
 * @property string $controller
 * @property string $action
 * @property string $url
 */
class AdminLogModel extends \backend\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admin_id', 'add_time'], 'integer'],
            [['username', 'params', 'module', 'controller', 'action', 'url'], 'required'],
            [['username', 'params'], 'string', 'max' => 50],
            [['module', 'controller', 'action', 'url'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => '会员ID',
            'username' => '用户名',
            'add_time' => '添加时间',
            'params' => '参数',
            'module' => '模块',
            'controller' => '控制器',
            'action' => '方法',
            'url' => 'Url',
            'post' => 'POST数据',
            'remarks' => '备注',
        ];
    }
   
}
