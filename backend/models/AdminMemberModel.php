<?php

namespace backend\models;

use Yii;

class AdminMemberModel extends \backend\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['add_time', 'last_login_time', 'role_id'], 'integer'],
            [['username'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => '会员ID',
            'username' => '用户名',
            'password' => '密码',
            'add_time' => '添加时间',
            'last_login_time' => '最后登录时间',
            'role_id' => '角色ID',
        ];
    }
    
    //获取角色名
    public function getRolename()
    {
        $data = AdminRoleModel::findOne($this->role_id);
        return $data['name'];
    }
    
    //设置相关的密码
    public function beforeSave($insert)
    {
        if($insert == true){
            
            $this->password = \yii::$app->controller->user->createPassword($this->password);
        }else{
            if(empty($this->password) || strlen($this->password) == 32){
                unset($this->password);
            }else{
                $this->password = \yii::$app->controller->user->createPassword($this->password);
            }
        }
        return parent::beforeSave($insert);
    }
    
    public static function GetUpdateLog(){
        $controller=Yii::$app->controller->id ;
        $index=Yii::$app->controller->action->id;
        $params=json_encode($_GET);
        $list=\common\models\AdminLogModel::find()
        ->select(['username','add_date','post','action'])
        ->where(['controller'=>$controller,'action'=>$index,'params'=>$params])
        ->orderby('add_date')
        ->asArray()
        ->all();
        return $list;
    }
    
    
}
