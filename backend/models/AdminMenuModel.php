<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%admin_menu}}".
 *
 * @property string $id
 * @property string $controller
 * @property string $action
 * @property integer $add_time
 * @property string $params
 * @property integer $parent_id
 * @property integer $sorting
 * @property integer $label
 */
class AdminMenuModel extends \common\models\BaseCategory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_time', 'parent_id', 'sorting', 'label'], 'integer'],
            [['controller', 'action'], 'string', 'max' => 50],
            [['params','title'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller' => '控制器',
            'action' => '方法',
            'add_time' => '添加时间',
            'params' => '参数',
            'parent_id' => '父ID',
            'sorting' => '排序',
            'label' => '级别',
            'title' => '菜单名称',
        ];
    }



   //获取用户需要的菜单
   //根据其权限判断 需要给出哪些的红包，然后再通过
   public static function getNeedMenu($member_info = [])
   {
       $member_info = empty($member_info) ? \yii::$app->user->member_info : $member_info;
       $role_id = $member_info['role_id'];
       if($role_id == \backend\components\Member::SUPER_ADMIN){//超级管理员
           return self::getListMenu(false);
       }else{
           $accss_list = AdminAccessModel::find()->where(['role_id' => $role_id])->asArray()->all();
           $accss_list_key = [];//所有拥有权限的菜单 
           $menu_list = self::getList();
           foreach($accss_list as $key => $value){
               $accss_list_key[$value['menu_id']] = $menu_list[$value['menu_id']];
           }
           
           //把所有的权限的菜单按需要的格式输出
           $result = self::getListMenuForDatas($accss_list_key,false);
           return $result;
       }
   }
   

    
    //通过模块，控制器，方法 来找到下级，且是有权限的下级，并且包括本身。
    public static function getSameLabel()
    {
        //目前访问的控制器跟方法 
        $controller = \yii::$app->controller->id;
        $action = \yii::$app->controller->action->id;
        
        //找到此菜单ID
        $where = [
            'controller' => $controller,
            'action' => $action,
        ];
        $model = self::find()->where($where)->asArray()->one();
        
        //一个是通过层数来判断，一个是通过其有无子菜单来判断
        //如果没有子菜单，则是只显示一个，如果有子菜单则把自己显示并显示其子菜单 。
        //还是通过层数来判断，这样的话我保存的菜单 必须要包括其层数。
        if($model){
            if($model['label'] == 4){//找到其父级
                $model = self::findOne($model['parent_id']);
            }
            
            //找到其自身与底下所有的显示的元素
            $result = self::find()->where(['parent_id' => $model['id']])->asArray()->all();//找到这个的所有的下级
            return self::getDataForSameLabel($result, $model);              
            
        }else{
            $where[] = $where;
            return $where;            
        }
        
        
        
//        if($model){
//            while(true){
//                if($model['status'] == 0){
//                    $model = self::find()->where(['id' => $model['parent_id']])->asArray()->one();
//                }else{
//                    break;
//                }
//            }
//            
//            
//            if($model['label'] == 4){//最后一级
//                $result = self::find()->where(['parent_id' => $model['parent_id']])->asArray()->all();
//                $arr_data = self::findOne($model['parent_id']);
//                $arr_data = $arr_data->attributes;
//                
//                return self::getDataForSameLabel($result, $arr_data);
//            }else{//不是最后一级
//                $result = self::find()->where(['parent_id' => $model['id']])->asArray()->all();//找到这个的所有的下级
//                return self::getDataForSameLabel($result, $model);  
//            }
//        }else{
//            $where[] = $where;
//            return $where;
//        }
    }
    
    protected static function getDataForSameLabel($result,$detail)
    {
        //用户的权限ID
        $role_id = \yii::$app->user->role_id;//用户的权限ID
        //用户的访问权限 以数据的形式呈现
        $access_list = AdminAccessModel::find()->where(['role_id' => $role_id])->asArray()->all();
        $access_array = [];
        foreach($access_list as $value){
            $access_array[] = $value['menu_id'];
        }        
        $class = get_class(\yii::$app->user);
        if($role_id != $class::SUPER_ADMIN){
            foreach($result as $key => $value){
                if(!in_array($value['id'], $access_array)){
                    unset($result[$key]);
                }
            }            
        }
        
        $arr_parent[0] = $detail;
        $result = array_merge($arr_parent,$result);
        return $result;              
    }

    

    //删除后事件回调
    public static function afterDeleteAll($condition = '', $params = [])
    {
        if(!empty($condition['id'])){
            AdminAccessModel::deleteAll(['menu_id' => $condition['id']]);
        }
        return true;
    }



    //不是以整个的二维数组表示，而是以多维的数组来表示
    public static function getAllForRole($role_id)
    {
        $access = AdminAccessModel::getDataForRole($role_id);//这个角色可以操作的菜单
        $datas = self::getListMenu(false);//所有的菜单
        foreach($datas as $key => $value){
            $datas[$key] = self::getDataForRole($value, $access);
        }
        return $datas;
//        $access = AdminAccessModel::getDataForRole($role_id);//这个角色可以操作的菜单
//        $datas = self::showMenu();
//        foreach($datas as $key  => $value){
//            if(!empty($access[$key])){
//                $datas[$key]['access'] = 1;
//            }else{
//                $datas[$key]['access'] = 0;
//            }
//        }
//        return $datas;
    }
    
    //给定一个数据然后把另一个给弄进去
    public static function getDataForRole($datas,$access)
    {
        if(!empty($access[$datas['id']])){
            $datas['is_select'] = 1;
        }else{
            $datas['is_select'] = 0;
        }
        foreach($datas['data'] as $key => $value){
            $datas['data'][$key] = self::getDataForRole($value, $access);
        }
        return $datas;
    }
    
    public function beforeSave($insert)
    {
        //新增项增加时间
        if($insert == true){
            $this->add_time = TIMESTAMP;
        }
        
        
        if(empty($this->parent_id)){
            $this->label = 1;
        }else{
            $d = self::findOne($this->parent_id);
            if(!$d){
                $this->addError('parent_id', '找不到此父级ID菜单');
                return false;
            }
            $this->label = $d->label + 1;
        }
        
        if($this->label > 4){
            $this->addError('label', '最多只到4级菜单');
            return false;
        }
        
        if($this->label > 2){//控制器跟方法都不能为空，且不能跟别人重复
            if(!$this->controller){
                $this->addError('controller', '控制器不能为空');
                return false;
            }
            if(!$this->action){
                $this->addError('action', '方法不能为空');
                return false;
            }
            if($insert){
                $is_data = self::find()->where(['controller' => $this->controller, 'action' => $this->action])->asArray()->one();
            }else{
                $id = $this->id;
                //echo self::find()->where(['controller' => $this->controller, 'action' => $this->action])->andWhere(['!=','id',$id])->createCommand()->getRawSql();exit;
                $is_data = self::find()->where(['controller' => $this->controller, 'action' => $this->action])->andWhere(['!=','id',$id])->asArray()->one();
            }
            if($is_data){
                $this->addError('action', '已经存在此操作了');
                return false;                
            }
        }else{
            $this->controller = $this->action = '';//如果是菜单项的话是为空的
        }
        
        return parent::beforeSave($insert);
    }
    


}
