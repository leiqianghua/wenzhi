<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%admin_role}}".
 *
 * @property string $id
 * @property string $name
 * @property integer $add_time
 */
class AdminRoleModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['add_time'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '角色名称',
            'add_time' => '添加时间',
        ];
    }

    
    public static function getKeyValue($is_all = true)
    {
        $result = [];
        if($is_all){
            $result[''] = '全部';
        }
        $datas = self::find()->asArray()->select(['id','name'])->indexBy('id')->all();
        foreach ($datas as $data){
            $result[$data['id']] = $data['name'];
        }
        return $result;        
    }
    
    public static function beforeDeleteAll(&$condition = '', $params = array())
    {
        if(!empty($condition['id'])){
            if(is_array($condition['id'])){
                if(in_array(1, $condition['id'])){
                    static::$error = '超级管理员角色不能删除';
                    return false;
                }
            }else{
                if($condition['id'] == 1){
                    static::$error = '超级管理员角色不能删除';
                    return false;
                }
            }
        }
        return true;
    }
    
    //更换整个角色的菜单
    public static function updateData($role_id,$datas)
    {
        try {
            $model = self::findOne($role_id);
            if(!$model){
                throw new \Exception('不存在此角色');
            }
            //删除这个角色的所有的菜单
            AdminAccessModel::deleteAll(['role_id' => $role_id]);
            //新增所有
            foreach($datas as $data){
                $model = new AdminAccessModel();
                $model->role_id = $role_id;
                $model->menu_id = $data;
                $model->save();
            }
            return true;
        } catch (\Exception $exc) {
            static::$error = $exc->getMessage();
            return false;
        }
    }
}
