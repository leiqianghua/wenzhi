<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CountForm extends Model
{
    public $uid;
    public $start_register_time;
    public $end_register_time;
    public $start_rechange_time;
    public $end_rechange_time;
    public $channel;
    public $v_param;




    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => '用户ID',
            'start_register_time' => '开始注册时间',
            'end_register_time' => '结束注册时间',
            'channel' => '渠道',
            'v_param' => '马甲',
        ];
    }

}
