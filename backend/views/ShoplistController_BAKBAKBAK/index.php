<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>列表示例</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'shop_id') ?>
            <?= $form->field($model, 'title') ?>
            <?= $form->field($model, 'goods_state')->dropDownList($object::getGoods_stateHtml(),['prompt' => '全部']) ?>
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

<!--            <?= Html::button('隐藏/显示', ['id' => "show_hide",'class' => 'btn']) ?>            -->
        </div>
    </div>    

<div style="display:none" id="dddd">
    <form class="layermmain">
        <input type="text" name="bbbbbb" value="ccc" />
    </form>    
</div>


<script>
    var DIALOG_CONTENT = $("#dddd").html();
</script>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'shop_id',//直接展示
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示
            ['attribute' => 'thumb','format' => ['image']],//图片展示
            [
                'value' => function($model, $key, $index, $column){//想要展示成不一样的东西的时候
                    return '<a>ab</a>';
                },
                 'attribute' => 'keywords',
                'format' => 'raw',//  有上面列的图片，时间等
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getStatusHtml($model->status);
                }
            ],
            [
               'value' => function($model, $key, $index, $column){
                    $html = "<a href='".\yii::$app->controller->to(['update','id' => $model->id])."'  >编辑</a>";
                    $html .= "</br>";
                    $html .= "<a href='".\yii::$app->controller->to(['delete','id' => $model->id])."' class='ajax_request' >删除</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
            ['class' => 'yii\grid\ActionColumn'],
            [
               'value' => function($model, $key, $index, $column){
                    $html = "<a href='ddd/ss' class='ajax_request' >aaaaa</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

    <?= Html::button('删除',['class' => 'select_list btn','url' => $this->context->to(["delete"])]) ?>

</div>
