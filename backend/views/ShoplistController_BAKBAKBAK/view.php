<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Admin Role Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-role-model-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    
    <?php 
        $prize = common\models\ShoplistPrizeModel::findOne($model->number_id);
        if($prize && $prize['is_update']){
            $is_update = "是";
        }else{
            $is_update = "否";
        }
    ?>



    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view dddd'],
        'template' => '<tr><th style="width:10%">{label}</th><td>{value}</td></tr>', 
        'attributes' => [
            'number_id',
            'shop_id',
            'is_auto',//是否是自动购买的
            'qishu',
            [
                'label' => "人数",
                'format' => 'raw',
                'value' => $model->zongrenshu . "/" . $model->canyurenshu,
            ],
            'q_uid',
            'q_user_code',
            [
                'label' => "开奖",
                'format' => 'raw',
                'value' => "<span id='q_end_time'></span>",
            ],
            'auto_buy_num',
            [
                'label' => "是否修过",
                'value' => $is_update,
            ]
        ],
    ]) ?>

</div>

<script>
    $(function(){
        var times = "<?= $model->q_end_time*1000 ?>";
        var objc = "#q_end_time";
        var ok_html = "已经揭晓";
        var not_ok_html = "未揭晓";
        gg_show_Time_fun(times,objc,ok_html,not_ok_html);
    })
</script>
