<style>
   ul, ol, li{ list-style:none;}
.wr_list ul ul{padding-left: 17px;}
.wp_form_submit input{width:80px;height:24px;text-align: center;line-height:24px;font-size: 14px;color: #fff;background: #106BAB;border-radius: 5px;margin:25px 0 10px 0;border: none;cursor: pointer;}
.c{background: url(/images/admin/zTreeStandard.png) no-repeat center center;}
.wr_list ul li i{display: inline-block;width: 9px;height:10px;vertical-align: middle;background-position:-99px -77px;cursor: pointer;}
.wr_list ul li input{vertical-align: middle;margin: 0 5px;}
.wr_list ul li a{cursor: pointer;}
.wr_list ul li a em{display: inline-block;width:16px;height:16px;margin-right:5px;background-position:-109px -16px;vertical-align: middle;}
.wr_list ul li a span.c{display: inline-block;width: 12px;height: 14px;background-position: -112px -33px;vertical-align: middle;margin-right: 5px;}    
</style>
<h1>角色授权</h1>

<div class="wr_list">
    <form method="post">
        <!--列表-->
        <ul>
                
                <?php  foreach($menus as $key_1 => $value_1){  ?>
                    <li class="li0">
                        <i class="c i1"></i><input type="checkbox" <?php if($value_1['is_select']){ echo 'checked'; }  ?> layer="layer1" value="<?= $value_1['id'] ?>" name="menu[]"><a href="#" class="a1"><em class="c"></em><?= $value_1['title'] ?></a>
                        <ul class="list1">
                            <?php  foreach($value_1['data'] as $key_2 => $value_2){  ?>
                                <li class="li1">
                                    <i class="c i2"></i><input type="checkbox"  <?php if($value_2['is_select']){ echo 'checked'; }  ?>  layer="layer2" value="<?= $value_2['id'] ?>" name="menu[]"><a href="#" class="a2"><em class="c"></em><?= $value_2['title'] ?></a>
                                    <ul class="list2">
                                        <?php  foreach($value_2['data'] as $key_3 => $value_3){  ?>
                                            <li class="li2">
                                                <i class="c i3"></i><input type="checkbox"   <?php if($value_3['is_select']){ echo 'checked'; }  ?> layer="layer3" value="<?= $value_3['id'] ?>" name="menu[]"><a href="#" class="a3"><em class="c"></em><?= $value_3['title'] ?></a>
                                                <ul class="list3">
                                                    <?php  foreach($value_3['data'] as $key_4 => $value_4){  ?>
                                                        <li class="li3">
                                                            <i class="c i4"></i><input type="checkbox"   <?php if($value_4['is_select']){ echo 'checked'; }  ?> layer="layer4" value="<?= $value_4['id'] ?>" name="menu[]"><a href="#" class="a4"><em class="c"></em><?= $value_4['title'] ?></a>
                                                            <ul class="list4">

                                                            </ul>
                                                        </li>

                                                    <?php }  ?>
                                                </ul>
                                            </li>

                                        <?php }  ?>
                                    </ul>
                                </li>

                            <?php }  ?>
                        </ul>
                    </li>
            
                <?php }  ?>
            
            
                <!--设置菜单项-->
<!--                <li class="li0"><i class="c i1"></i><input type="checkbox" name="name1"><a href="#" class="a1"><em class="c"></em>设置（菜单项）</a>
                        <ul class="list1">
                                <li class="li1"><i class="c i2"></i><input type="checkbox" name="name2"><a href="#" class="a2"><em class="c"></em>系统设置（菜单项）</a>
                                        <ul class="list2">
                                                <li class="li2"><i></i><input type="checkbox" name="name3"><a href="#"><span class="c"></span>网站配置</a></li>
                                                <li class="li2"><i class="c i3"></i><input type="checkbox" name="name3"><a href="#" class="a3"><em class="c"></em>后台菜单管理</a>
                                                        <ul class="list3">
                                                                <li><i class="c i4"></i><input type="checkbox" name="name4"><a href="#"><em class="c"></em>添加菜单</a>
                                                                        <ul class="list4">
                                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>四级菜单1</a></li>
                                                                        <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>四级菜单2</a></li>
                                                                        </ul>
                                                                </li>
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>编辑菜单</a></li>
                                                                <li><i class="c i4"></i><input type="checkbox" name="name4"><a href="#"><em class="c"></em>删除菜单</a>
                                                                        <ul class="list4">
                                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>四级菜单1</a></li>
                                                                        <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>四级菜单2</a></li>
                                                                        </ul>
                                                                </li>
                                                        </ul>
                                                </li>
                                        </ul>
                                </li>
                                <li class="li1"><i class="c i2"></i><input type="checkbox" name="name2"><a href="#" class="a2"><em class="c"></em>网站管理员（菜单项）</a>
                                        <ul class="list2">
                                                <li><i class="c i3"></i><input type="checkbox" name="name3"><a href="#" class="a3"><em class="c"></em>管理员列表</a>
                                                        <ul class="list3">
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>添加管理员</a></li>
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>编辑管理员</a></li>
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>删除管理员</a></li>
                                                        </ul>
                                                </li>
                                                <li><i class="c i3"></i><input type="checkbox" name="name3"><a href="#" class="a3"><em class="c"></em>管理员角色</a>
                                                        <ul class="list3">
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>添加角色单</a></li>
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>编辑角色</a></li>
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>删除角色</a></li>
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>角色授权管理</a></li>
                                                                <li><i></i><input type="checkbox"><a href="#"><span class="c"></span>成员列表</a></li>
                                                        </ul>
                                                </li>
                                        </ul>
                                </li>
                        </ul>
                </li>-->

        </ul>
        <!--列表结束-->
        <div class="wp_form_submit"><input type="submit" value="授权"></div>
  </form>
</div>


<script>
    $(function(){

   var $i = $(".wr_list ul li i");
   var $i1 = $(".wr_list ul li .i1");
   var $i2 = $(".wr_list ul li .i2");
   var $i3 = $(".wr_list ul li .i3");
   var $i4 = $(".wr_list ul li .i4");
   var $a = $(".wr_list ul li a");
   
   
   $i.click(function(){
       if($(this).hasClass('show_tab')){
            $(this).css("background-position","-99px -77px");
            $(this).siblings("a").children("em").css("background-position","-109px -16px");              
            $(this).removeClass('show_tab');
       }else{
            $(this).css("background-position","-81px -77px");
            $(this).siblings("a").children("em").css("background-position","-109px 0px");  
            $(this).addClass('show_tab');
       }
   })
   
//   $i.toggle(function(){
//      $(this).css("background-position","-81px -77px");
//      $(this).siblings("a").children("em").css("background-position","-109px 0px");
//   },function(){
//      $(this).css("background-position","-99px -77px");
//      $(this).siblings("a").children("em").css("background-position","-109px -16px");
//   });

   $i1.bind("click",function(){
      if($(this).parent().find(".list1").is(":visible")){
        $(this).parent().find(".list1").slideUp();
      }else{
        $(this).parent().find(".list1").slideDown();
      }
    });
   
  $i2.bind("click",function(){
    if($(this).parent().find(".list2").is(":visible")){
        $(this).parent().find(".list2").slideUp();
    }else{
        $(this).parent().find(".list2").slideDown();
    }
   });
   
   $i3.bind("click",function(){
    if($(this).parent().find(".list3").is(":visible")){
        $(this).parent().find(".list3").slideUp();
    }else{
        $(this).parent().find(".list3").slideDown();
    }
   });
   
   $i4.bind("click",function(){
    if($(this).parent().find(".list4").is(":visible")){
        $(this).parent().find(".list4").slideUp();
    }else{
        $(this).parent().find(".list4").slideDown();
    }
   });

  $(".list4 input").click(function(){
    if($(this).parent().parent(".list4").find('input:checked').length>0){
        $(this).parent("li").parent("ul").parent("li").find("input[layer=layer4]").prop("checked",true);
        $(this).parent("li").parent("ul").parent("li").parent("ul").parent("li").find("input[layer=layer3]").prop("checked",true);
        $(this).parents(".li1").find("input[layer=layer2]").prop("checked",true);
        $(this).parents(".li0").find("input[layer=layer1]").prop("checked",true);
    }else{
        $(this).parent("li").parent("ul").parent("li").find("input[layer=layer4]").prop("checked",false);
    }
    
  });
  
    $(".list3 input[layer=layer4]").click(function(){
    if($(this).prop("checked")){
        $(this).parent().find("input").prop("checked",true);
    }else{
        $(this).parent().find("input").prop("checked",false);
    }
  });
  
  $(".list3 input").click(function(){
    if($(this).parents(".list3").find('input:checked').length>0){
        $(this).parent("li").parent("ul").parent("li").find("input[layer=layer3]").prop("checked",true);
        $(this).parent("li").parent("ul").parent("li").parent("ul").parent("li").find("input[layer=layer2]").prop("checked",true);
        $(this).parent("li").parent("ul").parent("li").parent("ul").parent("li").parent("ul").parent("li").find("input[layer=layer1]").prop("checked",true);
    }else{
        $(this).parent("li").parent("ul").parent("li").find("input[layer=layer3]").prop("checked",false);
    }
    
  });
    $(".list3 input").click(function(){
    if($(this).parents(".list3").find('input:checked').length<=0){
        $(this).parents(".li2").find("input[layer=layer3]").prop("checked",false);
    }
    
  });
  
  $(".list2 input[layer=layer3]").click(function(){
    if($(this).prop("checked")){
        $(this).parent().find("input").prop("checked",true);
        $(this).parent().parent("ul").parent("li").find("input[layer=layer2]").prop("checked",true);
        $(this).parent().parent("ul").parent("li").parent("ul").parent("li").find("input[layer=layer1]").prop("checked",true);
    }else{
        $(this).parent().find("input").prop("checked",false);
    }
  });
  
$(".list2 input").click(function(){
    if($(this).parents(".list2").find('input:checked').length<=0){
        $(this).parents(".li1").find("input[layer=layer2]").prop("checked",false);
    }
});

  
  $(".list1 input[layer=layer2]").click(function(){
    if($(this).prop("checked")){
        $(this).parent().find("input").prop("checked",true);
        $(this).parent("li").parent("ul").parent("li").find("input[layer=layer1]").prop("checked",true);
    }else{
        $(this).parent().find("input").prop("checked",false);
    }
  });
  
$(".list1 input").click(function(){
    if($(this).parents(".list1").find('input:checked').length<=0){
        $(this).parents(".li0").find("input[layer=layer1]").prop("checked",false);
    }
});
  
  $("input[layer=layer1]").click(function(){
    if($(this).prop("checked")){
        $(this).parent().find("input").prop("checked",true);
    }else{
        $(this).parent().find("input").prop("checked",false);
    }
  });
  
 //tab切换
 $(".menu li").click(function(){
    $(this).addClass("active");
    $(this).siblings().removeClass("active");
    var index = $(".menu li").index(this);
    $(".content").hide();
    $(".content").eq(index).show();
})
//menu下面的a关闭
$(".menu a i").click(function(){
    $(this).parent().hide();
    
})
   
  
  
   
 })
                
</script>