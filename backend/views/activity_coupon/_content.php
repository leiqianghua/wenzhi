<div class="form-group">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label">优惠券</label>
    </div>
    <div id="_<?= $name ?>_list" class="col-xs-6 col-sm-4">
        <?php if($values){ ?>
            <?php foreach($values as $key => $value){  ?>

        
                <div data="<?= $key ?>" class="form-group">
                    <div class="col-xs-12 col-sm-12"> 
                        <label> 是否叠加</label>
                        <input type="checkbox" name="<?= $name ?>[<?= $key ?>][is_overlay]" <?php if(!empty($value['is_overlay'])){ echo "checked"; }  ?>>
                    </div>                
                    <div class="col-xs-12 col-sm-12"> 
                        <label class="control-label">优惠券金额：</label>
                        <input type="text" name="<?= $name ?>[<?= $key ?>][money]" value="<?= $value['money']  ?>">
                    </div>   

                    <div class="col-xs-12 col-sm-12"> 
                        <label class="control-label">满减 金额：</label>
                        <input type="text" name="<?= $name ?>[<?= $key ?>][must_money]" value="<?= $value['must_money']  ?>">
                    </div> 

                    <div class="col-xs-12 col-sm-12"> 
                        <label class="control-label">不允许特价商品使用：</label>
                        <input type="checkbox" name="<?= $name ?>[<?= $key ?>][is_tejia]" <?php if(!empty($value['is_tejia'])){ echo "checked"; }  ?>>
                    </div> 

                    <div class="col-xs-12 col-sm-12"> 
                        <label class="control-label">只允许部份商品集合：</label>
                        <input type="text" name="<?= $name ?>[<?= $key ?>][goods_ids]" value="<?= $value['goods_ids']  ?>">
                    </div> 
                    
                    
                </div>           
                <?php if($key != 0){  ?>
                    <div class="col-xs-2 col-sm-2">
                        <button data="<?= $key ?>" type="button" class="delete_element_<?= $name ?> btn btn-danger">删除</button>  
                    </div>                         
                <?php }  ?>           
        
            <?php }  ?>            
        <?php  }else{ ?>
            <div data="0" class="form-group">
                <div class="col-xs-12 col-sm-12"> 
                    <label> 是否叠加</label>
                    <input type="checkbox" name="<?= $name ?>[0][is_overlay]">
                </div>                
                <div class="col-xs-12 col-sm-12"> 
                    <label class="control-label">优惠券金额：</label>
                    <input type="text" name="<?= $name ?>[0][money]" value="1">
                </div>   
                
                <div class="col-xs-12 col-sm-12"> 
                    <label class="control-label">满减 金额：</label>
                    <input type="text" name="<?= $name ?>[0][must_money]" value="0">
                </div> 
                
                <div class="col-xs-12 col-sm-12"> 
                   <label class="control-label">不允许特价商品使用：</label>
                   <input type="checkbox" name="<?= $name ?>[0][is_tejia]">
               </div> 

               <div class="col-xs-12 col-sm-12"> 
                   <label class="control-label">只允许部份商品集合：</label>
                   <input type="text" name="<?= $name ?>[0][goods_ids]" value="">
               </div>                
            </div>            
        <?php }  ?>

    </div>
    <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
        <button type="button" class="add_element_<?= $name ?> btn btn-info">增加</button>  
    </div>
</div>      


<div id="_<?= $name ?>_from" style="display:none">
    <div data="NUM_TODO" class="form-group">
        <div class="col-xs-12 col-sm-12"> 
            <label> 是否叠加</label>
            <input type="checkbox" name="ATTRIBUTES_TODO[NUM_TODO][is_overlay]">
        </div>   
        
    
        
        
        <div class="col-xs-12 col-sm-12"> 
            <label class="control-label">优惠券金额：</label>
            <input type="text" name="ATTRIBUTES_TODO[NUM_TODO][money]" value="1">
        </div>   

        <div class="col-xs-12 col-sm-12"> 
            <label class="control-label">满减 金额：</label>
            <input type="text" name="ATTRIBUTES_TODO[NUM_TODO][must_money]" value="0">
        </div> 
        
        <div class="col-xs-12 col-sm-12"> 
            <label class="control-label">不允许特价商品使用：</label>
            <input type="checkbox" name="ATTRIBUTES_TODO[NUM_TODO][is_tejia]">
        </div> 

        <div class="col-xs-12 col-sm-12"> 
            <label class="control-label">只允许部份商品集合：</label>
            <input type="text" name="ATTRIBUTES_TODO[NUM_TODO][goods_ids]" value="">
        </div>             
        
        <div class="col-xs-2 col-sm-2">
            <button data="NUM_TODO" type="button" class="delete_element_<?= $name ?> btn btn-danger">删除</button>  
        </div>         
    </div>     
    
</div>

    
<script>
    $(function(){
        //增加元素
        function add_element(num)
        {
            var html = $("#_<?= $name ?>_from").html();//寻找需要的字符串
            html = html.replace(/NUM_TODO/g,num);//进行要关的替换
            html = html.replace(/ATTRIBUTES_TODO/g,'<?= $name ?>');//进行要关的替换
            //开始插入
            $("#_<?= $name ?>_list").append(html);
        }
        //删除元素
        function delete_element(num)
        {
            $("#_<?= $name ?>_list > [data='"+num+"']").eq(0).remove();
        }

        $(document).on('click','.delete_element_<?= $name ?>',function(){
            var num = $(this).attr('data');
            delete_element(num);
        })

        $(".add_element_<?= $name ?>").click(function(){
            var num = $("#_<?= $name ?>_list div[data]").last().attr('data');
            num = parseInt(num) + 1;
            add_element(num);
        })            
    })
</script>