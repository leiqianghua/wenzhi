<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    <?= $form->field($model, 'title')->textInput() ?>
    
    <?php if(!$model->start_time){ $model->start_time = strtotime('today'); }  $model->start_time = date('Y-m-d H:i:s',$model->start_time); ?>
    <?php if(!$model->end_time){ $model->end_time = strtotime('today'); }    $model->end_time = date('Y-m-d H:i:s',$model->end_time); ?>
    <?php if(!$model->invalid_time){ $model->invalid_time = strtotime('today')+86400*7; }    $model->invalid_time = date('Y-m-d H:i:s',$model->invalid_time); ?>
    <?= $form->field($model, 'start_time',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3']) ?>
    <?= $form->field($model, 'end_time',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3']) ?>
    <?= $form->field($model, 'invalid_time',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3']) ?>
    <?php  $model->label = json_decode($model->label, true); ?>
    <?= $form->field($model, 'label')->checkboxList(common\models\MemberModel::getLabelHtmlAll());  ?>
    <?= $form->field($model, 'money')->textInput() ?>
    <?= $form->field($model, 'max_num')->textInput() ?>
    <?= $form->field($model, 'use_num')->textInput() ?>
    <?= $form->field($model, 'limit_num')->textInput() ?>
    
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'img','img', $date . '/active'); ?>    
    
    <?php  echo $this->render("_content",['model' => $model,'id' => "content",'name' => "content",'values' => json_decode($model->content, true)]);   ?>


    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function dddd(url)
    {
        alert(url);
    }
</script>