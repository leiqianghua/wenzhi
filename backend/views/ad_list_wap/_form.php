<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
error_reporting(0);
$content=(array)json_decode($model->content,true);

if(!$model->isNewRecord){
	$model->start_time=$model->start_time>0?date("Y-m-d H:i",$model->start_time):'';
	$model->end_time=$model->end_time>0?date("Y-m-d H:i",$model->end_time):'';
}else{
	$model->start_time='';
	$model->end_time='';
	$model->status=1;
}
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
	        'options' => ['class' => 'form-horizontal J_ajaxForm'],
	        'fieldConfig' => [
	            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
	        ]        
        ]); ?>
     
    <?= $form->field($model, 'title') ?>
    <?php  $date = date('Ymd'); ?>
    <?= \common\components\Form::getFileContent($form, $model, 'img', 'img', $date . '/ad_wap_img', '"750*350"', [], '', false); ?>
    <?= \common\components\Form::getFileContent($form, $model, 'img_list', 'img_list', $date . '/ad_wap_img', '多 ', [], '', true); ?>
    
    
    <?= $form->field($model, 'jump_data') ?>
    
    
    
    <?= $form->field($model, 'sorts')->textInput(['maxlength' => 2]) ?>
    <?= $form->field($model, 'start_time',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3']) ?>
    <?= $form->field($model, 'end_time',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}{hint}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3'])->hint("开始时间和结束时间，同时有值，时间区间就生效。适用于所有广告位") ?>
    
    
    <?php $model->channel = json_decode($model->channel,true);  ?>
    <?php // $form->field($model, 'is_sh')->radioList([0=>'否',1=>'是']) ?>
    <?= $form->field($model, 'status')->radioList([1=>'显示',0=>'隐藏']) ?>    
    
    
    <?= $form->field($model, 'post_alias')->dropDownList(\common\models\AdPostWapModel::getKeyValue(true)) ?>
    
    </br>
    </br>
    </br>
    
<!--    根据我们选的广告位来确定我们是要展示哪些东西。-->
    <div id='key_type'><?php include '_sel.php';?></div>
    
    
    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success J_ajax_submit_btn' : 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<script>
    $(function(){
        var category_data = {};
        var key_type = {};
        $(document).on("change","#adlistwapmodel-post_alias",function(){
            cate_change_key_type($(this));
        })
        function cate_change_key_type(obj_this)
        {
            var data = obj_this.val();
            var obj = obj_this.parents(".form-group").eq(0);
            if(typeof(key_type[data]) == "undefined"){
                $.post("select_key_type",{"id":"<?= $model->id ?>","key_type":data},function(obj_content){
                    if(obj_content.status){
                        key_type[data] = obj_content.content;
                        $("#key_type").html(key_type[data]);
                    }
                },"json");
            }else{
                $("#key_type").html(key_type[data]);
            }            
        }
        
    })
</script>