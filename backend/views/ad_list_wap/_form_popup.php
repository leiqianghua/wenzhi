<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
error_reporting(0);
$content=(array)json_decode($model->content,true);

if(!$model->isNewRecord){
	$model->start_time=$model->start_time>0?date("Y-m-d H:i",$model->start_time):'';
	$model->end_time=$model->end_time>0?date("Y-m-d H:i",$model->end_time):'';
}else{
	$model->start_time='';
	$model->end_time='';
	$model->status=1;
}
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
	        'options' => ['class' => 'form-horizontal J_ajaxForm'],
	        'fieldConfig' => [
	            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
	        ]        
        ]); ?>
     
    <?= $form->field($model, 'title')->label("弹窗标题"); ?>
    <?= $form->field($model, 'content')->textarea()->label("弹窗内容") ?>
    
    
    <?php $model->v_param = json_decode($model->v_param,true);  ?>
    <?= $form->field($model, 'v_param')->checkboxlist(\common\models\VParamModel::getKeyValue()) ?>
    
    <?php  $date = date('Ymd'); ?>
    <?= \common\components\Form::getFileContent($form, $model, 'img', 'img', $date . '/ad_wap_img', '', [], '', false); ?>
    
    
    <?= $form->field($model, 'start_time',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3']) ?>
    <?= $form->field($model, 'end_time',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}{hint}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3'])->hint("开始时间和结束时间，同时有值，时间区间就生效。") ?>
    
    
    <?php $model->channel = json_decode($model->channel,true);  ?>
    <?= $form->field($model, 'channel')->checkboxlist(\common\models\ChannelModel::getKeyValue()) ?>
    <?php // $form->field($model, 'is_sh')->radioList([0=>'否',1=>'是']) ?>
    <?= $form->field($model, 'status')->radioList([1=>'开启',0=>'关闭']) ?>
    
    
    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success J_ajax_submit_btn' : 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>