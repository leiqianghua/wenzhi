<?php
    if(empty($form)){
        //把其这个处理掉。
        $request = \Yii::$app->getRequest();
        $request->enableCsrfValidation = false;
        
        $form = \yii\widgets\ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
        $is_can_data = true;
    }
?>


<?php 
    $type_model = common\models\AdPostWapModel::findOne($model->post_alias);
    $des = json_decode($type_model->des,true);//看到这样的一个配置信息
    $config = [];
    if($des){
        foreach($des as $k => $v){
            if(!empty($v['key_data'])){
                $config[$v['param']] = $v;
            }
        }
    }
?>

<?php  foreach($config as $key => $value){  ?>
<?php  
    if($value['type'] == "text"){
        echo $form->field($model, $key)->textInput()->label($value['title']);
    }else if($value['type'] == "textarea"){
        echo $form->field($model, $key)->textarea(['style' => "width:100%;height:300px"])->label($value['title']);
    }else{
        echo $form->field($model, $key)->textInput()->label($value['title']);
    }
?>


<?php } ?>



<?php
    if(!empty($is_can_data)){
        \yii\widgets\ActiveForm::end();
    }
?>
