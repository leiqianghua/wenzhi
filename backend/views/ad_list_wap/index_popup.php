<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">
    <h1></h1>
<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
	                'encodeErrorSummary' => false,
	                'enableClientScript' => false,
	                'fieldConfig' => ['errorOptions' => []],
	                'method' => 'get',
	                'action' => 'index',
	                'options' => ['class' => 'form-inline'],
            ]); ?>
            <?= $form->field($model, 'id') ?>
            <?= $form->field($model, 'title') ?>
            <?= $form->field($model, 'post_alias')->dropDownList(\common\models\AdPostModel::getKeyValue()) ?>
            


            <br/>
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
    
    <?php //$this->context->getAssocData($dataProvider->getModels());?>
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '总共{totalCount}条数据',
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
    		'id',
    		'title',
            
            'v_param',
            

            'channel',
            'title',
            'content',


    		[
    			'attribute' => 'status',
	    		'value' => function($model, $key, $index, $column) {
	    			return $model->status?'开启':'关闭';
	    		},
    		],
    		[
    			'label' => '时间',
	    		'value' => function($model, $key, $index, $column) {
	    			if($model->start_time>0&&$model->end_time>0){
	    				return date("Y-m-d H:i",$model->start_time).'<br/>--<br/>'.date("Y-m-d H:i",$model->end_time);
	    			}
	    			return '--';
	    		},
	    		'format' => 'html',
    		],
                [
                   'value' => function($model, $key, $index, $column){
                        $delete_url = \yii::$app->controller->to(["/".Yii::$app->controller->id."/delete_popup",'id' => $model['id']]);
                        $update_url = \yii::$app->controller->to(["/".Yii::$app->controller->id."/update_popup",'id' => $model['id']]);
                        $delete = '<a href="'.$delete_url.'" title="Delete" aria-label="Delete" data-confirm="你确定要删除吗?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                        $update = '<a href="'.$update_url.'" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                        $html =  $update . "&nbsp;&nbsp;&nbsp;" . $delete . "&nbsp;&nbsp;&nbsp;";
                        return $html;
                   },
                   'label' => '管理', 
                   'format' => 'raw',                      
                ],  
        ],
    ]); ?>
</div>
<script>
