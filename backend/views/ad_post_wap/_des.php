<?php 
    $my_select_datas = [
        'param1' => "参数1",
        'param2' => "参数2",
        'param3' => "参数3",
        'param4' => "参数4",
        'param5' => "参数5",
        'param6' => "参数6",
    ];
?>
<?php  echo common\components\Form::getSelectData($my_select_datas, "{$base_name}[param]", ['class' => "form-control"], false, $value['param'])  ?>

标题：<input type="text" class="form-control  length_2" name="<?= $base_name ?>[title]" value="<?= $value['title'] ?>">
键的值：<input type="text" class="form-control  length_2" name="<?= $base_name ?>[key_data]" value="<?= $value['key_data'] ?>">
键的类型：<?php  echo common\components\Form::getSelectData(['text' => "文本","textarea" => "textarea","editor" => "editor"], "{$base_name}[type]", ['class' => "form-control"], false, $value['type'])  ?>