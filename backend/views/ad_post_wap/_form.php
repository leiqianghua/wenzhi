<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
error_reporting(0);

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
	        'options' => ['class' => 'form-horizontal J_ajaxForm'],
	        'fieldConfig' => [
	            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
	        ]        
        ]); ?>
     
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'keywords') ?>
    <?= $form->field($model, 'descript')->textarea(); ?>
    <?= $form->field($model, 'num') ?>
    
    
    <?php 
        $content = json_decode($model->des,true);
        echo  \common\components\Tool::getDataForViewList("des", "/ad_post_wap/_des", $content, "参数说明");     
    ?>
    
    
    
    
    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success J_ajax_submit_btn' : 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>