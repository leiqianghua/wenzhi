<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>会员列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',
                'options' => ['class' => 'form-inline'],
            ]); ?>

            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'role_id')->dropDownList(\backend\models\AdminRoleModel::getKeyValue()) ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
    
    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '总共{totalCount}条数据',
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            //['class' => 'yii\grid\SerialColumn'],
            'uid',
            'username',
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],
            ['attribute' => 'last_login_time','format' => ['date', 'Y-M-d']],
            ['class' => 'yii\grid\ActionColumn','template' => '{update} {delete}'],
        ],
    ]); ?>
</div>
