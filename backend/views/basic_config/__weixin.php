<?php 
    $datas = [
        'click' => "点击",
        'view' => "显示链接",
    ];
?>
类型：<?php echo common\components\Form::getSelectData($datas, "{$base_name}[type]", ['class' => "form-control"], false, $value['type'])  ?>
名称：<input type="text" class="form-control  length_2" name="<?= $base_name ?>[name]" value="<?= $value['name'] ?>">
</br>
操作key(类型为点击的时候填写，菜单中不能出现重复的操作key,填随意的英文,客服填key_kefu)：<input type="text" class="form-control  length_2" name="<?= $base_name ?>[key]" value="<?= $value['key'] ?>">
</br>
<textarea style="width:100%;height:200px" name="<?= $base_name ?>[content]"><?= $value['content'] ?></textarea>
</br>
回复内容标题（只回复文字内容则为空,否则标题地址与内容都需要填写）：<input type="text" class="form-control  length_3" name="<?= $base_name ?>[content_title]" value="<?= $value['content_title'] ?>">
</br>
图片地址：<input type="text" class="form-control  length_3" name="<?= $base_name ?>[content_img]" value="<?= $value['content_img'] ?>">
链接：<input type="text" class="form-control  length_3" name="<?= $base_name ?>[content_url]" value="<?= $value['content_url'] ?>">
</br>
<font style="color:red">---------------------------</font>
    

