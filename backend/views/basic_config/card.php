<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php  $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
    	'enableClientScript' => false,
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
        ]        
        ]);
     foreach ($basic as $model){
     	if($model->varname=='is_card_verification_02'){
    ?>
    <?= $form->field($model, 'value')->label($model->info)->textInput(['name'=>$model->varname]) ?>
    <?php }else{?>
    <?= $form->field($model, 'value')->label($model->info)->radiolist(
     		$model::getWatermarkOffHtml(),['name'=>$model->varname])?>
    <?php }}?>
     
    <div class="form-group">
                <?= Html::submitButton('提交', ['class' => 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>	
<?php ActiveForm::end();?>
</div>

