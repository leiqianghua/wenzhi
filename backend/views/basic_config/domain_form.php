<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
        ]        
        ]); ?>


 <!-- uploadfile($id,$name,$savepath) $id 为绑定触发上传的元素id -->
     <?= $form->field($model, 'domain_name') ?>
     <?= $form->field($model, 'module') ?>
     <?= $form->field($model, 'controller') ?>
     <?= $form->field($model, 'function') ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success J_ajax_submit_btn' : 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
