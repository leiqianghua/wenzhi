<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php  $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
    	'enableClientScript' => false,
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
        ]        
        ]);
     foreach ($basic as $model){
     	if($model->varname=='error_log'||$model->varname=='web_off'||$model->varname=='gzip'){
    ?>
    <!-- 额外的提示信息 -->
     <?= $form->field($model, 'value')->label($model->info)->radiolist(
     		$model::getWatermarkOffHtml(),['name'=>$model->varname]) ?>
     <?php }else if($model->varname=='pay_config'){
        echo \common\components\Form::editor_element_detail_new("pay_config", "支付方式",$model->value); 
         //echo $form->field($model, 'value')->label($model->info)->textarea(['name'=>$model->varname,'style' => "height:200px"]);
     }else{ ?>
            
		 <?= $form->field($model, 'value')->label($model->info)->textInput(['name'=>$model->varname])?>

	<?php }}?>
    <div class="form-group">
                <?= Html::submitButton('提交', ['class' => 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>	
<?php ActiveForm::end();?>
</div>

