<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-announce-model-index">

    <h1></h1>

   

<button type="submit" class="btn btn-primary" onclick="location.href='/basic_config/message_create'">短信服务商添加</button>
<script>
    var DIALOG_CONTENT = $("#dddd").html();
</script>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '总共{totalCount}条数据',
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            //['class' => 'yii\grid\SerialColumn'],
            'id',
        	'support_name',
        	'user_mark',
        	['attribute' => 'user_password',
        	'value'=>function(){
        	return '******';
        	},
        	],
        	'url',
        	['attribute' => 'm_status',
        	'value'=>function($model){
        	return $model->getStatusHtml($model->m_status);
        	},
        	],
        	'm_sign',
        	'm_appid',
                [
                   'value' => function($model, $key, $index, $column){
                        $delete_url = \yii::$app->controller->to(["/basic_config/message_delete",'id' => $model['id']]);
                        $update_url = \yii::$app->controller->to(["/basic_config/message_update",'id' => $model['id']]);
						$delete = '<a href="'.$delete_url.'" title="Delete" aria-label="Delete" data-confirm="你确定要删除吗?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                        $update = '<a href="'.$update_url.'" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                      
                        $html = $update . "&nbsp;&nbsp;&nbsp;" . $delete ;
                        return $html;
                   },
                   'label' => '管理', 
                   'format' => 'raw',                      
                ],  
        ],
    ]); ?>


</div>
