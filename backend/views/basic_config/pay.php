<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-announce-model-index">

    <h1></h1>
<button type="submit" class="btn btn-primary" onclick="location.href='/basic_config/pay_create'">支付方式添加</button>
<script>
    var DIALOG_CONTENT = $("#dddd").html();
</script>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '总共{totalCount}条数据',
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            //['class' => 'yii\grid\SerialColumn'],
            'id',
        	'pay_name',
        	'name_english',
        	['attribute' => 'pay_type',
        		'value'=>function($model){
        			return $model->getTypeHtml($model->pay_type);
        		},
        	],
        	['attribute' => 'pay_start',
        	'value'=>function($model){
        	return $model->getStartHtml($model->pay_start);
        	},
        	],
        	['attribute' => 'pay_thumb_pc','format' => ['image'],],
        	['attribute' => 'pay_thumb','format' => ['image'],],
        	'pay_des',
        	'appid',
        	'appsecret',
                ['attribute' => 'ma_jia',
        	'value'=>function($model){
        	return $model->getMj($model->ma_jia);
        	}],
                [
                   'value' => function($model, $key, $index, $column){
                        $delete_url = \yii::$app->controller->to(["/basic_config/pay_delete",'id' => $model['id']]);
                        $update_url = \yii::$app->controller->to(["/basic_config/pay_update",'id' => $model['id']]);
						$delete = '<a href="'.$delete_url.'" title="Delete" aria-label="Delete" data-confirm="你确定要删除吗?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                        $update = '<a href="'.$update_url.'" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                      
                        $html = $update . "&nbsp;&nbsp;&nbsp;" . $delete ;
                        return $html;
                   },
                   'label' => '管理', 
                   'format' => 'raw',                      
                ],  
        ],
    ]); ?>


</div>
