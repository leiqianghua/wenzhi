<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
        ]        
        ]); ?>
     <?= $form->field($model, 'pay_name') ?>
     <?= $form->field($model, 'name_english') ?>
     <?= $form->field($model, 'pay_type')->radiolist($model::getTypeHtml(),[]) ?>
     <?= $form->field($model, 'pay_start')->radiolist($model::getStartHtml(),[]) ?>
     <?=Form::getFileContent($form,$model,'pay_thumb_pc','pay_thumb_pc', 'pay_way','',[],'',false);?>
     <?=Form::getFileContent($form,$model,'pay_thumb','pay_thumb', 'pay_way','',[],'',false);?>        
     <?= $form->field($model, 'pay_des')->textarea(); ?>  
     <?= $form->field($model, 'appid') ?>
     <?= $form->field($model,'ma_jia')->dropDownList($model::getMj(),[]);?>
     <?= $form->field($model, 'appsecret') ?>
   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success J_ajax_submit_btn' : 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
