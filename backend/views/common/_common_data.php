<?php foreach($content as $key => $value){  ?>
    <div data="<?= $key ?>" class="list">
        <div class="row ">
           
            <div class="col-sm-12 form-inline">
                <label class="" for=""><?= $label  ?></label>
                <?php  echo $this->context->renderPartial($file,["value" => $value,"base_name" => "{$name}[{$key}]","other_data" => $other_data]);   ?>

                <?php if($key != -1){  ?>
                    <button style='padding:0' type="button" class="delete_element btn btn-danger">删除</button>  
                <?php }  ?>                                 
            </div> 
        </div>        
    </div>
<?php } ?>
