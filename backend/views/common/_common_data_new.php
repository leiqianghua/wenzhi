<?php foreach($content as $key => $value){  ?>
    <div data="<?= $key ?>" class="list">
        <div class="row ">
            <?php  if( $key == -1 && !empty($other_data['one_data_file'])){  ?>
                <div class="col-sm-12 form-inline">
                    <?php if(!empty($label)){ ?>
                        <label class="" for=""><?= $label  ?></label>
                    <?php } ?>
                    <?php  echo $this->context->renderPartial($other_data['one_data_file'],["value" => $value,"base_name" => "{$name}[{$key}]","other_data" => $other_data]);   ?>                                
                </div>             
            
            <?php }else{  ?>
            
                <div class="col-sm-12 form-inline">
                    <?php if(!empty($label)){ ?>
                        <label class="" for=""><?= $label  ?></label>
                    <?php } ?>
                    <?php  echo $this->context->renderPartial($file,["value" => $value,"base_name" => "{$name}[{$key}]","other_data" => $other_data]);   ?>

                    <?php if($key != -1){  ?>
                        <button style='padding:0' type="button" class="delete_element btn btn-danger">删除</button>  
                    <?php }  ?>                                 
                </div>             
            
            <?php } ?>

        </div>        
    </div>
<?php } ?>
