<?php 
    error_reporting(0);
    $old_name = $name;
    $name = md5($name);
?>
<div  class="row content_<?= $name ?>">
        <div style="" class="col-xs-2 col-sm-2  text-right">
            <button type="button" class="add_element_<?= $name ?> btn btn-info">增加</button>  
        </div>   
    
        <div style="" class="col-xs-12 col-sm-12">
            <div class='row'>
                <?php if(!empty($label)){   ?>
                    <div class="col-xs-2 col-sm-2 text-right">
                        <label class=""><?= $label;  ?></label>
                    </div>        
                <?php  } ?>


                <div class="content_list content_list_<?= $name ?>  col-xs-10 col-sm-10">
                    <?php 
                        if(empty($content)){
                            $content = ['-1' => []];
                        }
                        if(empty($_common_data)){
                            $_common_data = "_common_data";
                        }
                        if(empty($_common_data_label)){
                            $_common_data_label = "";
                        }
                        echo $this->context->renderPartial($_common_data,["label" => $_common_data_label,'content' => $content,'name' => $old_name,"other_data" => $other_data]);
                    ?>
                </div>                
            </div>
        </div>     
    
   




    <div class="hidden_data hidden_data_<?= $name ?>" style="display:none">
        <?php  
            $data = ["NUM_TODO_{$name}" => []];
                if(empty($_common_data)){
                    $_common_data = "_common_data";
                }  
                if(empty($_common_data_label)){
                    $_common_data_label = "";
                }                
            echo $this->context->renderPartial($_common_data,["label" => $_common_data_label,'content' => $data ,'name' => $old_name,"other_data" => $other_data]);
        ?>
    </div>    
</div>


<script>

    var is_ok = false;
    var js_name = "<?= $name ?>";
    if(typeof(NAME_VALUE) == "undefined"){
        var NAME_VALUE = [];
        NAME_VALUE.push(js_name);
        is_ok = true;
    }else{
        if(NAME_VALUE.indexOf(js_name) < 0){
            is_ok = true;
            NAME_VALUE.push(js_name);
        }
    }
    if(is_ok){
        $(function(){
            var parent_class = ".content_<?= $name ?>";
            //删除的功能  把跟自己一组的删除掉
            $(document).on('click','.content_<?= $name ?> .delete_element',function(){
                $(this).parents('.list').eq(0).remove();
            })


            $(document).on("click",".content_<?= $name ?> .add_element_<?= $name ?>",function(){
            //$(parent_class + " .add_element_<?= $name ?>").click(function(){
                var obj = $(this).parents(".content_<?= $name ?>").eq(0);
                var html = obj.find(".hidden_data_<?= $name ?>").eq(0).html();
                var num = obj.find(".content_list_<?= $name ?> > .list").not("[data='NUM_TODO']").last().attr('data');//最后一项了
                //console.log(html);
                num = parseInt(num) + 1;
                html = html.replace(/NUM_TODO_<?= $name ?>/g,num);//进行要关的替换
                //开始插入
                obj.find('.content_list_<?= $name ?>').append(html);            
            })
            //隐藏域提交的时候
            $(".J_ajax_submit_btn").click(function(){
                //console.log($(".hidden_data").find(".list[data='NUM_TODO']"));return false;
                $(".hidden_data").find(".list[data='NUM_TODO_<?= $name ?>']").remove();
            })    
        })        
    }

</script>

