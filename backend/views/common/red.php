<!--$name $red_content -->
<?php 
    //获取红包类型的键值对
    if(empty($GLOBALS['red_type'])){
        $GLOBALS['red_type'] = \common\models\RedKeyTypeModel::getBaseKeyValue("id", "type_name");
    }
?>


<div class="red_select">
    
    <div class="row">
        <div class="col-xs-3 col-sm-2 text-right">
            <label class="control-label" for="">红包类型</label>
        </div>
        <div class="col-xs-6 col-sm-4">
            <select id="activitylistmodel-title" class="form-control" name="<?= "{$name}[red_type]"   ?>">
            <option value="">请选择</option>
            <?php  foreach($GLOBALS['red_type'] as $k => $v){  ?>
                <option <?php if($red_content['red_type'] == $k){ echo "selected"; } ?> value="<?php echo $k;  ?>"><?php echo $v; ?></option>
            <?php }  ?>
            </select>
        </div>
        <div style="padding-top:7px;"></div>
    </div>







    <?php  
        //调用这个需要附带的值有  $name,data
        $value = $red_content['red_list'];    //红包的值
        $other_data = common\models\RedKeyModel::getBaseKeyValue("red_key", "remark", ["red_type_id" => $red_content['red_type']]);
        $mine_name = "{$name}[red_list]";
        echo $this->context->renderPartial("/activitylist/content/common/_ajax_red_list",["name" => $mine_name,"content" => $value,"other_data" => $other_data]);
    ?>

    <script>
        var is_ok = false;
        var js_name = "<?= $name ?>";
        if(typeof(NAME_VALUE_red) == "undefined"){
            var NAME_VALUE_red = [];
            NAME_VALUE_red.push(js_name);
            is_ok = true;
        }else{
            if(NAME_VALUE_red.indexOf(js_name) < 0){
                is_ok = true;
                NAME_VALUE_red.push(js_name);
            }
        }         
        
        if(is_ok){
            $(function(){
                $(document).on("change","[name='<?php echo "{$name}[red_type]"  ?>']",function(){
                //$("[name='<?php echo "{$name}[red_type]"  ?>']").change(function(){
                    var name = $(this).val();
                    var obj = $(this);
                    if(!name){
                        $(this).parents(".form-group").eq(0).next().remove();
                    }else{
                        var url = "/activitylist/ajax_getred_select";
                        $.post(url,{red_type_id:name,base_name:"<?= "{$name}[red_list]" ?>"},function(data){
                            if(data.status){
                                console.log(data.content);
                                obj.parents(".row").eq(0).next().remove();
                                obj.parents(".row").eq(0).after(data.content);
                            }
                        },"json");
                    }
                })
            })            
        }
    </script>    
    
</div>
