<?php //print_r($other_data['my_config']);  ?>


<?php  

    foreach($other_data['my_config'] as $my_key => $my_value){  
        $class = isset($my_value['class']) ? $my_value['class'] . ' form-control' : 'form-control';
        $style = isset($my_value['style']) ? $my_value['style'] : '';
        $name = isset($my_value['name']) ? "{$base_name}[{$my_value['name']}]" : "{$base_name}[]";
        $data_value = isset($value[$my_value['name']]) ? $value[$my_value['name']] : (isset($my_value['value']) ? $my_value['value'] : "");
        $title = isset($my_value['title']) ? $my_value['title'] : '';
        $type = isset($my_value['type']) ? $my_value['type'] : '';
        switch ($type) {
            case 1://展示字符串
                echo "{$title}:<input type='text'  {$style} class='{$class}' name='{$name}' value='{$data_value}'/>&nbsp";

                break;
            case 2://展示图
                $date = date('Ymd');
                //$id = md5($name);
                echo \common\components\Form::getFileContent_new($data_value, $title, $name, $name, $date . '/config_new', false, true, "", "",true);

                break;
            case 3://列表展示
                echo  \common\components\Tool::getDataForViewList($name, "/config/_config_new", $data_value, $title,['my_config' => $my_value['data']]);                

                break;
            case 4://单图
                $date = date('Ymd');
                //$id = md5($name);
                echo \common\components\Form::getFileContent_new($data_value, $title, $name, $name, $date . '/config_new', false, false, "", "",true);

                break;
            case 5://调用类型
                $type = $my_value['value'];
                $datas = \common\models\ConfigModel::getHtml($type);
                
                $select_data = isset($value[$my_value['name']]) ? $value[$my_value['name']] : "";
                echo common\components\Form::getSelectData($datas, $name, ['class' => "form-control"], false, $select_data);
                
                break;
            case 6://时间选择
                echo "{$title}:<input type='text'  {$style} class='{$class} J_datetime' name='{$name}' value='{$data_value}'/>&nbsp";

                break;

            default:
                break;
        }


    }


?>
