<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
error_reporting(0);
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{hint}</div>",
        ]        
        ]); ?>
    <?php if($model->isNewRecord){  ?>
        <?= $form->field($model, 'varname')->textInput() ?>
    <?php }else{  ?>
        <?= $form->field($model, 'varname')->textInput(['disabled' => true]) ?>
    <?php }  ?>
    <?= $form->field($model, 'type')->hint('')->textInput()->label("类型")  ?>
    <?= $form->field($model, 'info')->textarea()?>
    
    <?php //echo common\components\Form::getChushiData_New($model, $form, "varname", "选择值的类型", "value", "1", "radiolist", ["字符串","KEY-VALUE值"], "data")  ?>
    
<!--    一种是直接是字符串 一种是JSON格式字符串  两种选一种来处理这个需求-->

    <div class="form-group">
        <div class="col-xs-3 col-sm-2 text-right">
            <label class="control-label" for="configmodel-info">配置值</label>
        </div>
        <div class="col-xs-9 col-sm-10">
            
            <?php  $content = json_decode($model->value,true) ?>
            <?php  
                if(!$model->varname){
                    $save_key = strtolower($model->varname);
                    if(file_exists(__DIR__ . "/other/{$save_key}.php")){
                        require_once ("other/{$save_key}.php");
                    }else{
                        if(!$content){
                            echo common\components\Form::getSelectData(['1' => "字符关键字","2" => "图片","3" => "多值"], "value[select_key]", ['class' => "form-control select_mix_data length_2"], true, $content['select_key']); 
                        }else{
                            echo common\components\Form::getSelectData(['1' => "字符关键字","2" => "图片","3" => "多值"], "value[select_key]", ['class' => "form-control select_mix_data length_2"], true, $content['select_key']);
                            echo  $this->render("_config_has_select", ["content" => $content['data_key'],"select_key" => $content['select_key'],"name" => "value[data_key]"]);
                        }
                        //echo  \common\components\Tool::getDataForViewList("value", "/config/arr", $content, " ");
                    }  
                }else{
                $config = json_decode($model->more_config_init,true);
                if(empty($config) || empty($config['-1']['name'])){
                    echo $form->field($model, 'value');
                }else{
                    //动态配置的值
                    $content = json_decode($model->value,true);
                    echo  \common\components\Tool::getDataForViewList("value", "/config/_config_new", $content, "配置",['my_config' => $config]);     
                } 
                    
                    
//                        if(!$content){
//                            echo common\components\Form::getSelectData(['1' => "字符关键字","2" => "图片","3" => "多值"], "value[select_key]", ['class' => "form-control select_mix_data length_2"], true, $content['select_key']); 
//                        }else{
//                            echo common\components\Form::getSelectData(['1' => "字符关键字","2" => "图片","3" => "多值"], "value[select_key]", ['class' => "form-control select_mix_data length_2"], true, $content['select_key']);
//                            echo  $this->render("_config_has_select", ["content" => $content['data_key'],"select_key" => $content['select_key'],"name" => "value[data_key]"]);
//                        }
                        //echo  \common\components\Tool::getDataForViewList("value", "/config/arr", $content, " ");                    
                }            
            ?>
            
        </div>
    </div>


    

    <?php //echo  \common\components\Tool::getDataForViewList("value", "/config/arr", $content, " ");  ?>
    <?php // $form->field($model, 'value')?>
    
    

   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success J_ajax_submit_btn' : 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    //选择的时候要适当的展示我们所需要的东西  给其想要的一个字符串，然后让其底下成为一个可以用的东西. 图片跟第三方的东西都是不好取的。
    $(document).on("change",".select_mix_data",function(){
        var name = $(this).attr("name");
        var type = $(this).val();
        var obj = $(this);
        if(!type){
            $(this).next(".select_data").remove();
            return true;
        }
        $.post("select_data",{name:name,type:type},function(data){
            obj.next(".select_data").remove();
            obj.after(data.content);
        },"json");
    })
</script>
