<?php
//                $config = [
//                        [
//                                'name' => "key",
//                                'title' => '关键字',
//                                'type' => 1,
//                                'style' => 'abcd',
//                                'value' => 'abcd',
//                        ],
//                        [
//                                'name' => "title",
//                                'title' => '标题',
//                                'type' => 1,
//                        ],
//                        [
//                                'name' => "img",
//                                'title' => '图片',
//                                'type' => 2,
//                        ],
//                        [
//                                'name' => "data",
//                                'title' => '内容',
//                                'type' => 3,
//                                'data' => [
//                                    [
//                                        'name' => 'aa',
//                                        'title' => 'aa',
//                                        'type' => '1',                                        
//                                    ],
//                                    [
//                                        'name' => 'bb',
//                                        'title' => 'bb',
//                                        'type' => '3',   
//                                        'data' => [
//                                            [
//                                                    'name' => "title",
//                                                    'title' => '标题',
//                                                    'type' => 1,
//                                            ],
//                                            [
//                                                    'name' => "img",
//                                                    'title' => '图片',
//                                                    'type' => 2,
//                                            ],                                            
//                                        ]
//                                    ],
//                                ],
//                        ],
//                ];  
use yii\helpers\Html;
use yii\widgets\ActiveForm;
error_reporting(0);
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{hint}</div>",
        ]        
        ]); ?>
    <?php if($model->isNewRecord){  ?>
        <?= $form->field($model, 'varname')->textInput() ?>
    <?php }else{  ?>
        <?= $form->field($model, 'varname')->textInput(['disabled' => true]) ?>
    <?php }  ?>
    <?= $form->field($model, 'type')->hint('')->textInput()->label("类型")  ?>
    <?= $form->field($model, 'info')->textarea()?>
    
    <?php //echo common\components\Form::getChushiData_New($model, $form, "varname", "选择值的类型", "value", "1", "radiolist", ["字符串","KEY-VALUE值"], "data")  ?>
    
<!--    一种是直接是字符串 一种是JSON格式字符串  两种选一种来处理这个需求-->

    <div class="form-group">
        <div class="col-xs-3 col-sm-2 text-right">
            <label class="control-label" for="configmodel-info">配置值</label>
        </div>
        <div class="col-xs-9 col-sm-10">
            
            <?php  $content = json_decode($model->more_config_init,true) ?>
            
            <?php echo \common\components\Tool::getDataForViewList("more_config_init", "/config/_config_more_config_init", $content, "值");  ?>
            
            
        </div>
    </div>


    

    <?php //echo  \common\components\Tool::getDataForViewList("value", "/config/arr", $content, " ");  ?>
    <?php // $form->field($model, 'value')?>
    
    

   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success J_ajax_submit_btn' : 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    //选择的时候要适当的展示我们所需要的东西  给其想要的一个字符串，然后让其底下成为一个可以用的东西. 图片跟第三方的东西都是不好取的。
    $(document).on("change",".select_mix_data",function(){
        var name = $(this).attr("name");
        var type = $(this).val();
        var obj = $(this);
        if(!type || type != 3){
            $(this).next(".select_data").remove();
            return true;
        }
        $.post("select_data_1",{name:name,type:type},function(data){
            obj.next(".select_data").remove();
            obj.after(data.content);
        },"json");
    })
</script>
