<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
error_reporting(0);
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{hint}</div>",
        ]        
        ]); ?>
    <?php if($model->isNewRecord){  ?>
        <?= $form->field($model, 'varname')->textInput() ?>
    <?php }else{  ?>
        <?= $form->field($model, 'varname')->textInput(['disabled' => true]) ?>
    <?php }  ?>
    <?= $form->field($model, 'type')->hint('')->textInput()->label("类型")  ?>
    <?= $form->field($model, 'info')->textarea()?>
    
    <?php //echo common\components\Form::getChushiData_New($model, $form, "varname", "选择值的类型", "value", "1", "radiolist", ["字符串","KEY-VALUE值"], "data")  ?>
    
<!--    一种是直接是字符串 一种是JSON格式字符串  两种选一种来处理这个需求-->

    <?php echo $form->field($model, 'value')?>
    
    

   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success J_ajax_submit_btn' : 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
