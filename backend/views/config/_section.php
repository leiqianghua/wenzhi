<div class="form-group">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label">时间段</label>
    </div>
    <div id="_<?= $name ?>_list" class="col-xs-9 col-sm-10">
        <?php $values =  json_decode($value,true); if(!$values){ $values = unserialize($value); } ?>
        <?php  if($values){  ?>
        <?php foreach($values as $key => $value){  ?>
            <div data="<?= $key ?>" class="">
                <div class="col-xs-6 col-sm-6">
                    <div class="col-xs-4 col-sm-4 text-right">
                        <label class="control-label" for="configmodel-value"><?= $key ?>-<?= $key+1 ?>点间隔时间段(秒)</label>
                    </div>                    
                    <div class="col-xs-3 col-sm-3">
                        <input class="form-control" type="text" value="<?= $value[0]  ?>" name="<?= $name ?>[<?= $key ?>][0]" placeholder="请填写起始范围！" nullmsg="请填写起始范围！"/>
                    </div> 
                    <div class="col-xs-3 col-sm-3">
                        <input class="form-control" type="text" value="<?= $value[1]  ?>" name="<?= $name ?>[<?= $key ?>][1]" placeholder="请填写结束范围！" nullmsg="请填写结束范围！"/>
                    </div>                     
                </div>                   
            </div>            
        <?php }}  ?>  

    </div>
</div>