<div class="form-group">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label"><?php if(!empty($label)){ echo $label; }else{ echo '用户ID范围'; }  ?></label>
    </div>
    <div id="_<?= $name ?>_list" class="col-xs-6 col-sm-4">
        <?php $values =  json_decode($value,true); if(!$values){ $values = unserialize($value); } ?>
        <?php if($values){  ?>
            <?php foreach($values as $key => $value){  ?>
                <div data="<?= $key ?>" class="form-group">
                    <div class="col-xs-5 col-sm-5">
                        <input class="form-control" type="text" value="<?= $value['low']  ?>" name="<?= $name ?>[<?= $key ?>][low]" placeholder="请填写名称！" nullmsg="请填写名称！"/>
                    </div>   
                    <div class="col-xs-5 col-sm-5">
                        <input class="form-control" type="text" value="<?= $value['hide']  ?>" name="<?= $name ?>[<?= $key ?>][hide]" placeholder="请填写对应的值！" nullmsg="请填写对应的值！"/>
                    </div>  
                    <?php if($key != 0){  ?>
                        <div class="col-xs-2 col-sm-2">
                            <button data="<?= $key ?>" type="button" class="delete_element_<?= $name ?> btn btn-danger">删除</button>  
                        </div>                         
                    <?php }  ?>                  
                </div>            
            <?php }  ?>            
        <?php  }else{ ?>
            <div data="0" class="form-group">
                <div class="col-xs-5 col-sm-5">
                    <input class="form-control" type="text" name="<?= $name ?>[0][low]" placeholder="请填写起始范围！" nullmsg="请填写起始范围！"/>
                </div>   
                <div class="col-xs-5 col-sm-5">
                    <input class="form-control" type="text" name="<?= $name ?>[0][hide]" placeholder="请填写结束范围！" nullmsg="请填写结束范围！"/>
                </div>                
            </div>            
        <?php }  ?>

    </div>
    <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
        <button type="button" class="add_element_<?= $name ?> btn btn-info">增加</button>  
    </div>
</div>      


<div id="_<?= $name ?>_from" style="display:none">
    <div data="NUM_TODO" class="form-group">
        <div class="col-xs-5 col-sm-5">
            <input class="form-control" type="text" name="ATTRIBUTES_TODO[NUM_TODO][low]" placeholder="请填写起始范围" nullmsg="请填写起始范围！"/>
        </div>   
        <div class="col-xs-5 col-sm-5">
            <input class="form-control" type="text" name="ATTRIBUTES_TODO[NUM_TODO][hide]" placeholder="请填写结束范围！" nullmsg="请填写结束范围！"/>
        </div>      
        <div class="col-xs-2 col-sm-2">
            <button data="NUM_TODO" type="button" class="delete_element_<?= $name ?> btn btn-danger">删除</button>  
        </div>                   
    </div> 
</div>

    
<script>
    $(function(){
        //增加元素
        function add_element(num)
        {
            var html = $("#_<?= $name ?>_from").html();//寻找需要的字符串
            html = html.replace(/NUM_TODO/g,num);//进行要关的替换
            html = html.replace(/ATTRIBUTES_TODO/g,'<?= $name ?>');//进行要关的替换
            //开始插入
            $("#_<?= $name ?>_list").append(html);
        }
        //删除元素
        function delete_element(num)
        {
            $("#_<?= $name ?>_list > [data='"+num+"']").eq(0).remove();
        }

        $(document).on('click','.delete_element_<?= $name ?>',function(){
            var num = $(this).attr('data');
            delete_element(num);
        })

        $(".add_element_<?= $name ?>").click(function(){
            var num = $("#_<?= $name ?>_list div[data]").last().attr('data');
            num = parseInt(num) + 1;
            add_element(num);
        })            
    })
</script>