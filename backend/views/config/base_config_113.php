<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php  $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
    	'enableClientScript' => false,
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
        ]        
        ]);
    ?>
    <?php foreach ($datas as $model){ ?>
    
            <?php 
                if($model->varname == "ios_is_open"){
                    echo $form->field($model, 'value')->checkbox(['name'=>$model->varname,"label" => $model->info]);
                }else if($model->varname == "ios_v_param"){
                    $base_name = $model->varname;
                    $content = json_decode($model->value,true);
                    echo \common\components\Tool::getDataForViewList("{$base_name}", "/basic_config/_base_config_113", $content, "马甲/版本");
                    
                    //echo $form->field($model, 'value')->label($model->info)->dropDownList(\common\models\VParamModel::getMj(),['name'=>$model->varname]);
                }else if($model->varname == "ios_edition"){
                    $base_name = $model->varname;
                    $content = json_decode($model->value,true);
                    echo \common\components\Tool::getDataForViewList("{$base_name}", "/basic_config/_base_config_vparam_113", $content, "不同马甲版本提示更新");
                }else if($model->varname == "ios_shopids"){
                    $base_name = $model->varname;
                    $content = json_decode($model->value,true);
                    echo Form::getChushiData($model, $form, "value", "提示更新标题", "ios_shopids[title]", $content['title']);
                    
                    echo Form::getChushiData($model, $form, "value", "提示更新内容", "ios_shopids[content]", $content['content']);
                    
        
                    $value = $model->value;
                    $model->value = $content['is_update'];
                    echo $form->field($model, 'value')->checkbox(['name' => "ios_shopids[is_update]","label" => ""])->label("是否强制更新")->hint("是否需要强制更新");
                    $model->value = $value;                    

                }else{
                    //echo $form->field($model, 'value')->label($model->info)->textInput(['name'=>$model->varname]);
                }
            ?>

	<?php }?>
    
    
    <div class="form-group">
                <?= Html::submitButton('提交', ['class' => 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>	
<?php ActiveForm::end();?>
</div>

