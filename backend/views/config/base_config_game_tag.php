<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php  $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal J_ajaxForm'],
    	'enableClientScript' => false,
        'fieldConfig' => [
            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
        ]        
        ]);
    ?>
    <?php foreach ($datas as $model){ ?>
    
            <?php 
                if($model->varname == "game_tag"){
                    $base_name = $model->varname;
                    $content = json_decode($model->value,true);
                    echo \common\components\Tool::getDataForViewList("{$base_name}", "/config/other/game_tag", $content, "配置");
                }
            ?>

	<?php }?>
    
    
    <div class="form-group">
                <?= Html::submitButton('提交', ['class' => 'btn btn-primary J_ajax_submit_btn']) ?>
    </div>	
<?php ActiveForm::end();?>
</div>

