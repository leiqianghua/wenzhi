<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
?>
<div class="admin-announce-model-index">

    <h1></h1>

    <!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php
            $form = ActiveForm::begin([
                        'encodeErrorSummary' => false,
                        'enableClientScript' => false,
                        'fieldConfig' => ['errorOptions' => []],
                        'method' => 'get',
                        'action' => 'index', //这时可能会有变化
                        'options' => ['class' => 'form-inline J_ajaxForm'],
            ]);
            ?>

            <!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'info') ?>
            <?= $form->field($model, 'varname') ?>
                <?= $form->field($model, 'value') ?>        
            <div class="form-group">
            <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
<?php ActiveForm::end(); ?>
        </div>
    </div>    

    <div style="display:none" id="dddd">
        <form class="layermmain">
            <input type="text" name="bbbbbb" value="ccc" />
        </form>    
    </div>


    <script>
        var DIALOG_CONTENT = $("#dddd").html();
    </script>    

    <!--    列表-->
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '总共{totalCount}条数据',
        'columns' => [
            'varname',
            'info',
            [
            'value' => function($model, $key, $index, $column) {

                $delete_url = \yii::$app->controller->to(["/config/delete", 'varname' => $model['varname']]);
                $update_url = \yii::$app->controller->to(["/config/update", 'varname' => $model['varname']]);
                $update_init_url = \yii::$app->controller->to(["/config/update_init", 'varname' => $model['varname']]);
                $delete = '<a href="' . $delete_url . '" title="Delete" aria-label="Delete" data-confirm="你确定要删除吗?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                $update = '<a href="' . $update_url . '" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                $update_init = '<a href="' . $update_init_url . '">初始配置</a>';
                $html = $update . "&nbsp;&nbsp;&nbsp;" . $delete . "&nbsp;&nbsp;&nbsp;" . $update_init;
                return $html;
            },
                'label' => '管理',
                'format' => 'raw',
            ],
        ],
    ]);
    ?>


</div>
