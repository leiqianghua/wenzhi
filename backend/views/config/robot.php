<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    
    <?php
        foreach($models as $varname => $model){
            $json_data_arr = ['uids','section','proportion'];
            if(in_array($varname, $json_data_arr)){
                $mine_key = "_attribute_{$varname}";
                echo $this->render("_{$varname}",['model' => $model,'id' => $varname,'name' => $varname,'value' => $model->value]);
            }else{
                echo $form->field($model, 'value')->hint($model->info)->label($model->info)->textInput(['name' => $varname]);
            }
        }
    ?>
    

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>