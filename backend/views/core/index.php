<div class="row">
    <div class="col-sm-12">
        <button><a href="<?= $this->context->to(['createqishu']) ?>">创建新一期（无影响）</a></button>
        <button><a href="<?= $this->context->to(['autobuy']) ?>">自动购买（借助缓存,线上执行会影响期配置的相关时间设定）其实同一用户可以一起购买(扣红包/扣用户金额 有做判断）</a></button>
        <button><a href="<?= $this->context->to(['jiesuan']) ?>">结算（无影响,中奖订单有做唯一性）</a></button>
        <button><a href="<?= $this->context->to(['createqishu_cache']) ?>">缓存中创建新一期（无影响）</a></button>
    </div>
</div>

<form method="post" action="<?= $this->context->to(['pay']) ?>" role="form" class="form-inline">
    <div class="form-group">
        <label>我们订单out_trade_no</label>
        <input type="text" name="out_trade_no" value="" class="form-control"/>
    </div>
    <div class="form-group">
        <label>他们订单trade_no</label>
        <input type="text" name="trade_no" value="" class="form-control"/>
    </div>
    <div class="form-group">
        <label>价格total_fee</label>
        <input type="text" name="total_fee" value="" class="form-control"/>
    </div>
    <div class="form-group">
        <label>支付方式pay_type</label>
        <input type="text" name="pay_type" value="" class="form-control"/>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">处理</button>
    </div>    
</form>