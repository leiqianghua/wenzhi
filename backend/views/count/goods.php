<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\components\Form;

?>
<div class="admin-role-model-index">
<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'goods',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
            
            <?php echo Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"时间范围统计","J_date length_2");  ?>
            <span >到</span>
            <?php echo Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>
            
            
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                
                <a style="color:white;text-decoration:none" href="good_excel?title=普通商品统计&<?= $_SERVER['QUERY_STRING'] ?>">
                    <?= Html::buttonInput('导出', ['class' => 'btn btn-primary excel_download']) ?>
                </a>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            'id',//直接展示
            [
                 'label' => '产品类别',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    $url = \common\models\GoodsClassModel::getOne($data['cid'], "title");
                    return $url;
                 },
             ], 
            [
                 'label' => '产品名称',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods/index?gid={$data['id']}";
                    $html = "<a target='_blank' href='{$url}' class='' >{$data['name']}</a>";
                    return $html;
                 },
             ], 
            [
                 'label' => '供应商',
                 'format' => 'raw',    
                 'value' => function ($data) {
                     if(!$data['admin_id']){
                         return "古笆特出品";
                     }
                     $html = common\models\AdminMember::getOne($data['admin_id'], "username");
                    return $html;
                 },
             ],   
             [
                 'label' => "入库价",
                 'value' => function ($data) {
                     $html = \common\models\ElsePurchaseModel::find()->where(['goods_id' => $data['id']])->asArray()->orderBy("add_time desc")->one();
                     if(!$html){
                         return "占无入库价";
                     }else{
                         return $html['total_money']/$html['num'];
                     }
                    
                 },
             ],    
             [
                 'label' => "销售价",
                 'attribute' => 'discount_4',
             ],    
             [
                 'label' => "采购总量",
                 //'attribute' => 'purchase_num',
                 'value' => function ($data) {
//                     $where = [];
//                     $where[] = "and";
//                     $where[] = ['goods_id' => $data['id']];
//                    if(!empty($_GET['start_add_time'])){
//                        $time = strtotime($_GET['start_add_time']);
//                        $where[] = "add_time>={$time} ";
//                    }
//                    if(!empty($_GET['end_add_time'])){
//                        $time = strtotime($_GET['end_add_time']);
//                        $where[] = "add_time<{$time} ";
//                    }
//                    $num = \common\models\ElsePurchaseModel::find()->where($where)->sum("num");
                    $num = $data['purchase_num'];
                    if(!$num){
                        $url = \yii::$app->controller->to(['else_purchase/index','goods_id' => $data['id']]);
                        return "<a href='{$url}'>0</a>";
                    }else{
                        $url = \yii::$app->controller->to(['else_purchase/index','goods_id' => $data['id']]);
                        return "<a href='{$url}'>{$num}</a>";
                    }
                 },
                 'format' => 'raw',    
             ],    
             [
                 'label' => "销售总量",
                 'attribute' => 'sale_num',
                 'value' => function ($data) {
                    $num = $data['sale_num'];
                    if(!$num){
                        $url = \yii::$app->controller->to(['order/indexgoods','goods_id' => $data['id'],'type' => 2]);
                        return "<a href='{$url}'>0</a>";
                    }else{
                        $url = \yii::$app->controller->to(['order/indexgoods','goods_id' => $data['id'],'type' => 2]);
                        return "<a href='{$url}'>{$num}</a>";
                    }
                 },
                 'format' => 'raw',                     
             ],    
             [
                 'label' => "库存总量",
                 'attribute' => 'num',
                 'value' => function ($data) {
                    $num = $data['num'];
                    if(!$num){
                        $url = \yii::$app->controller->to(['member_canku/index']);
                        return "<a href='{$url}'>0</a>";
                    }else{
                        $url = \yii::$app->controller->to(['member_canku/index']);
                        return "<a href='{$url}'>{$num}</a>";
                    }
                 },
                 'format' => 'raw',             
             ], 
                         
            [
                'label' => "销量总额",
                 'attribute' => 'b.money',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    if($data['money']){
                        $html = "<font style='color:red'>{$data['money']}</font>";
                    }else{
                        $html = "<font style='color:red'>0</font>";
                    }
                    return $html;
                 },
             ],               
        ],
    ]); ?>

</div>
