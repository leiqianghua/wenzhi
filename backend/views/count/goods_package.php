<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\components\Form;

?>
<div class="admin-role-model-index">
<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'goods_package',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
            
            <?php echo Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"时间范围统计","J_date length_2");  ?>
            <span >到</span>
            <?php echo Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>
            
            
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                
                <a style="color:white;text-decoration:none" href="goods_package_excel?title=套装商品统计&<?= $_SERVER['QUERY_STRING'] ?>">
                    <?= Html::buttonInput('导出', ['class' => 'btn btn-primary excel_download']) ?>
                </a>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            'id',//直接展示
            [
                 'label' => '商品名称',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods_package/detail?gid={$data['id']}";
                    $html = "<a target='_blank' href='{$url}' class='' >{$data['name']}</a>";
                    return $html;
                 },
             ],         
            [
                'label' => "销量金额",
                 'format' => 'raw',    
                 'value' => function ($data) {
                    if($data['money']){
                        $html = "<font style='color:red'>{$data['money']}</font>";
                    }else{
                        $html = "<font style='color:red'>0</font>";
                    }
                    return $html;
                 },
             ],                  
            [
                'label' => "销量数量",
                 'format' => 'raw',    
                 'value' => function ($data) {
                    if($data['num']){
                        $html = "<font style='color:red'>{$data['num']}</font>";
                    }else{
                        $html = "<font style='color:red'>0</font>";
                    }
                    return $html;
                 },
             ],  
            [
                'attribute' => 'province',   
                'label' => "省",
            ],   
            [
                'attribute' => 'city',   
                'label' => "市",
            ],       
        ],
    ]); ?>

</div>
