<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\components\Form;

?>
<div class="admin-role-model-index">
<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'user',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
            
            <?php echo Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"时间范围统计","J_date length_2");  ?>
            <span >到</span>
            <?php echo Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>
            
            
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                
                <a style="color:white;text-decoration:none" href="user_excel?title=用户统计&<?= $_SERVER['QUERY_STRING'] ?>">
                    <?= Html::buttonInput('导出', ['class' => 'btn btn-primary excel_download']) ?>
                </a>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
             [
                 'label' => "用户ID",
                 'attribute' => 'uid',
             ],   
             [
                 'label' => "用户名",
                 'attribute' => 'username',
             ],   
            [
                'label' => "购买金额",
                 'attribute' => 'order_amount',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    if($data['money']){
                        $html = "<font style='color:red'>{$data['order_amount']}</font>";
                    }else{
                        $html = "<font style='color:red'>0</font>";
                    }
                    return $html;
                 },
             ],         
            [
                'label' => "获取到的拥金",
                 'attribute' => 'yon_money',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    if($data['money']){
                        $html = "<font style='color:red'>{$data['yon_money']}</font>";
                    }else{
                        $html = "<font style='color:red'>0</font>";
                    }
                    return $html;
                 },
             ],             
            [
                'label' => "下属会员数量",
                 'format' => 'raw',    
                 'value' => function ($data) {
                     return \common\models\MemberModel::find()->where(['parent_id' => $data['uid']])->count();
                 },
             ],     
                         
            [
                'label' => "区域",
                'attribute' => "address_info",
             ],    
        ],
    ]); ?>

</div>
