<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'title')->hint('<a>增加我要的说明</a>')->textInput() ?>
    <?= $form->field($model, 'title')->hint('sdf',['abbbb' => 'gsdfsdfsdf'])->textarea()->label('sfsdf', [])->error(['ab' => 'gsdfsf']) ?>
    <?= $form->field($model, 'title')->hint('<a>sddfsf</a>',['abbbb' => 'gsdfsdfsdf'])->textarea()->label('sfsdf', [])->error(['ab' => 'gsdfsf']) ?>
    <?= ''//\common\components\Form::editor_element_detail($model, 'title2');  ?>
    <?= $form->field($model, 'cateid')->dropDownList(\common\models\CategoryModel::getMenuKeyValue(false));  ?>
    
<!--    下载图片-->
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'img','img', $date . '/goods'); ?>
    <?= Form::getFileContent($form,$model,'img_list','img_list', $date . '/goods','',[],'',true); ?>
    
<!--    编辑器-->
    <?= Form::editor_element_detail($model, 'details');  ?>
    
    <?= Form::getFileContent($form,$model,'title2','title2', 'ee/ee','',[],'',true); ?>
   <?php echo \common\components\Form::getDataForForm("show_start_date",empty($_GET['show_start_date']) ? '' : $_GET['show_start_date'],"show_end_date",empty($_GET['show_end_date']) ? '' : $_GET['show_end_date'],"展示时间范围","J_date length_4","J_date length_4");  ?>
        
    <?= $form->field($model, 'title')->radioList(['a' => 'aa','b' => 'sd']);  ?>
    <?= $form->field($model, 'title')->radio()->label('abcd');  ?>
    <?= $form->field($model, 'title')->radio();  ?>
    <?= $form->field($model, 'title')->checkbox();  ?>
    <?php $model->title = ['a','b']; ?>
    <?= $form->field($model, 'title')->checkboxList(['a' => 'aa','b' => 'sd']);  ?>
    <?= $form->field($model, 'title')->dropDownList(['a' => 'aa','b' => 'sd']);  ?>
    <?= $form->field($model, 'title')->checkboxList(['a' => 'aa','b' => 'sd'],['style' => 'padding-top:7px;']);  ?>

<!--    时间的使用-->
    <?= $form->field($model, 'title')->textInput(['class' => 'form-control J_date length_3']) ?>
    <?= $form->field($model, 'title',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3']) ?>
    

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function dddd(url)
    {
        alert(url);
    }
</script>