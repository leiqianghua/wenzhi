<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>开单列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'type')->dropDownList($object::getTypeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'last_setp_is_confirm')->dropDownList($object::getlast_setp_is_confirmHtml(),['prompt' => '全部']) ?>
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                
                <a style="color:white;text-decoration:none" href="index_excel?title=开单&<?= $_SERVER['QUERY_STRING'] ?>">
                    <?= Html::buttonInput('导出', ['class' => 'btn btn-primary excel_download']) ?>
                </a>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    




<script>
    var DIALOG_CONTENT = $("#dddd").html();
</script>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            'uid',//直接展示
            [
                'label' => "开单人",
                'attribute' => 'user.username',
            ],
            [
               'label' => "收货信息",
               'value' => function($model, $key, $index, $column){
                    $html = "姓名:{$model['customer_name']},地址：{$model['customer_address']},手机号：{$model['customer_iphone']}";
                    return $html;
               },
               'format' => 'raw',                      
            ],  
            'price',//直接展示
            [
                'attribute' => 'cost_price',
                'value' => function($model){
                    if($model->cost_price){
                        return $model->cost_price;
                    }else{
                        return "未提交";
                    }
                }
            ],
            'zhe_price',//直接展示
            'step_has_ok',//直接展示
            [
               'label' => "用户截图",
               'value' => function($model, $key, $index, $column){
                    $result = [];
                    $result_str = [];
                    $html = json_decode($model->user_upload_img,true);
                    if(empty($html)){
                        return "未截图";
                    }
                    foreach($html as $step => $img){
                        $result_str[] = "<img src='{$img}' style='width:100px;height:100px' />";
                    }
                    $result_str = implode("</br>", $result_str);
                    return $result_str;
               },
               'format' => 'raw',                      
            ],  
            'step_pay_num',//直接展示
            'step',//直接展示
            'extract',//直接展示
            [
                'attribute' => 'type',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::gettypeHtml($model->type);
                }
            ],
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示
            [
                'attribute' => 'status',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getStatusHtml($model->status);
                }
            ],
            'beizhu',//直接展示
            'title',//直接展示
            [
                'label' => "详情",
               'value' => function($model, $key, $index, $column){
                    $result = [];
                    $html = json_decode($model->content,true);
                    if(!is_array($html)){
                        return "";
                    }
                    foreach($html as $kk => $dd){
                        foreach($dd as $k => $d){
                            if($kk == "content_3"){
                                foreach($d as $n_k => $n_d){
                                    $n_d['discount_0'] = isset($n_d['discount_0']) ? $n_d['discount_0'] : $n_d['price'];
                                    $result[] = "{$n_d['name']},价格:{$n_d['discount_0']} ,数量:{$n_d['num']}";
                                }
        
                            }else{
                              $d['name'] = isset($d['name']) ? $d['name'] : $d['title'];
                              if(empty($d['name'])){
                                  continue;
                              }
                              if(!isset($d['buy_num'])){
                                  if(isset($d['num'])){
                                      $d['buy_num'] = $d['num'];
                                  }else{
                                      $d['buy_num'] = "未知";
                                  }
                              }
                              $d['buy_num'] = isset($d['buy_num']) ? ",购买数量" . $d['buy_num'] : "";
                              $d['price'] = isset($d['price']) ? "，价格：" . $d['price'] : "";
                              $result[] = "{$d['name']},{$d['price']} {$d['buy_num']}";
                              //$result[] =  json_encode($d);                     
                            }
                        }
                    }
                    $result_str = implode("</br>", $result);
                    $html = "<a content=\"{$result_str}\" class='show_layer'  >展示</a>";
                    return $html;
               },
               'format' => 'raw',                      
            ],  
            [
               'value' => function($model, $key, $index, $column){
                   $html = "";
                    if(!$model->last_setp_is_confirm && $model->user_upload_img){
                        $html .= "<a href='confirm_pay?id={$model['id']}' class='ajax_request' >确认打款</a>";
                    }
                    if(!$model->cost_price){
                        $content = "<form class='layermmain'>"
                                . "<label>成本价</label><input type='text' name='cost_price' value=''/>"
                                . "</form>";                        
                        $html .= "<a content=\"{$content}\" class='ajax_layer' href='upload_cost?id={$model['id']}' class='' >上传成本价</a>";
                    }
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
                    
                    
                    
        ],
    ]); ?>


</div>
