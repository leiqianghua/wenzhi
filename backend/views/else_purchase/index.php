<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>采购列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'goods_id') ?>
            <?= $form->field($model, 'goods_name') ?>
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>            
                        
            
            <?= $form->field($model, 'canku_id')->dropDownList(\common\models\MemberCankuModel::getBaseKeyValue("id", "title"),['prompt' => '全部']);  ?>  
            

            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    

    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'goods_id',//直接展示
            'goods_name',//直接展示
            'cangou_time',//直接展示
            [
                'attribute' => 'canku_id',
                'value' => function($model){
                    return \common\models\MemberCankuModel::getOne($model['canku_id'], "title");
                }
            ],
            'num',//直接展示
            'total_money',//直接展示


            ['class' => 'yii\grid\ActionColumn','template' => '{update} {delete}'],
        ],
    ]); ?>

    <?= Html::button('删除',['class' => 'select_list btn','url' => $this->context->to(["delete"])]) ?>

</div>
