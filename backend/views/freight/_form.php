<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    
<div class="form-group field-elsecarouselmodel-sorting">
    <div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="elsecarouselmodel-sorting">地区</label></div>
    <div class="col-xs-9 col-sm-9">
        <?php  echo \common\models\ElseFreightModel::getDataForArea($model->province_id,$model->city_id);  ?>
    </div>
</div>    
    
    
    
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'first_weight') ?>
    <?= $form->field($model, 'first_cose') ?>
    <?= $form->field($model, 'next_weight') ?>
    <?= $form->field($model, 'next_cose') ?>
    <?php 
        if($this->context->user->role_id == \backend\components\Member::SUPER_ADMIN){
            $d = common\models\ElseFreightTypeModel::getBaseKeyValue("id","title",["admin_id" => 0]);
        } else{
            $d = common\models\ElseFreightTypeModel::getBaseKeyValue("id","title",["admin_id" => $this->context->user->uid]);
        }       
    ?>
    <?= $form->field($model, 'type_id')->dropDownList($d) ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(document).on('change','.province_id',function(){
        var obj = $(this);
        if(obj.val() == 0){
            obj.next("select").remove();
        }else{
            var data_obj = {};
            data_obj.id = obj.val();
            $.post('/freight/select_city',data_obj,function(data){
                if(data.status){
                    obj.next("select").remove();
                    obj.after(data.content);
                }
            },'json');
        }
    })
</script>