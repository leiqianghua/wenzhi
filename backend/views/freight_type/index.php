<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>运费模板类型</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'title') ?>
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml()) ?>
            <?= $form->field($model, 'admin_id') ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    

 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            'id',//直接展示
            'title',//直接展示
            [
                'attribute' => 'status',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getStatusHtml($model->status);
                }
            ],
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示    
            'admin_id',//直接展示     
            [
               'value' => function($model, $key, $index, $column){
                    $html = "<a href='update?id={$model['id']}' class='' >修改</a>";
                    $html .= "</br>";
                    $html .= "<a href='delete?id={$model['id']}' class='' >删除</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

    <?= ''//Html::button('我要删除',['class' => 'select_list btn','url' => '/ab/sd']) ?>

</div>
