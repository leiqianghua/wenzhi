<?php

error_reporting(0);

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
\backend\assets\JcropAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    <div id="spec_name_label_list">
        <?php  $content = json_decode($model->spec_1, true); ?>
        <?php echo Form::getChushiData($model, $form, "name", "规格1名称", "spec_1[spec_name]", $content['spec_name'])  ?>
        <?php echo common\components\Tool::getDataForViewList("spec_1[data]", "/goods/basedata/_spec_value", $content['data'], "规格1", ['model' => $model,"form" => $form]) ?>
        <br>    <br>
        <?php  $content = json_decode($model->spec_2, true); ?>
        <?php echo Form::getChushiData($model, $form, "name", "规格2名称", "spec_2[spec_name]", $content['spec_name'])  ?>
        <?php echo common\components\Tool::getDataForViewList("spec_2[data]", "/goods/basedata/_spec_value", $content['data'], "规格2", ['model' => $model,"form" => $form]) ?>
        <br>    <br>    
        <?php  $content = json_decode($model->spec_3, true); ?>
        <?php echo Form::getChushiData($model, $form, "name", "规格3名称", "spec_3[spec_name]", $content['spec_name'])  ?>
        <?php echo common\components\Tool::getDataForViewList("spec_3[data]", "/goods/basedata/_spec_value", $content['data'], "规格3", ['model' => $model,"form" => $form]) ?>
        <br>    <br>         
    </div>
   
    
<table id="kucun_config"  class="table table-striped table-bordered">
    <thead >
    <tr id="kucun_config_thead">
        <th>规格1</th>
        <th>规格2</th>
        <th>规格3</th>
        <th>原价</th>
        <th>出厂价</th>
        <th>事业合伙人价</th>
        <th>品牌合伙人价</th>
        <th>销售合伙人价</th>
        <th>普通会员价</th>
        <th>库存</th>
    </tr>
    </thead>

    <tbody id="kucun_config_tbody">
        
    </tbody>
</table>
    
    
    <script>
        <?php  if($model->spec_value){ 
            $d_list = json_decode($model->spec_value,true);
            foreach($d_list as $k_1 => $v_1){
                foreach($v_1 as $k_2 => $v_2){
                    foreach($v_2 as $k_3 => $v_3){
                        if(!empty($v_3['goods_id'])){
                            $goods_model = \common\models\GoodsModel::findOne($v_3['goods_id']);
                            //$d_list[$k_1][$k_2][$k_3]['goods_id'] = $goods_model['id'];
                            $d_list[$k_1][$k_2][$k_3]['num'] = $goods_model['num'];
                        }
                    }                    
                }
            }
            $model->spec_value = json_encode($d_list);
        ?>
            var spec_value = <?php  echo $model->spec_value;  ?>;
        <?php }else{  ?>
            var spec_value = {};
        <?php }  ?>
            
        

        function createKucun_config()
        {
            var spec_1_obj_list = $(".content_list_dc6113878ce66f8171051e188ae056ca      > .list[data]");
            var spec_2_obj_list = $(".content_list_4272a3468bef904d024fadb2a1301126      > .list[data]");
            var spec_3_obj_list = $(".content_list_cf36c5630f43274f1122b674d3b66225      > .list[data]");
            var length_1 = spec_1_obj_list.length;
            var length_2 = spec_2_obj_list.length;
            var length_3 = spec_3_obj_list.length;
            
            var html = "";
            for(var i = 0;i<length_1;i++){
                for(var j = 0;j<length_2;j++){
                    for(var m = 0;m<length_3;m++){
                        var data_1 = spec_1_obj_list.eq(i).find(".spec_name_detail").val();
                        var data_2 = spec_2_obj_list.eq(j).find(".spec_name_detail").val();
                        var data_3 = spec_3_obj_list.eq(m).find(".spec_name_detail").val();
                        html += "<tr>"; 
                        html += "<td>"+data_1+"</td>";
                        html += "<td>"+data_2+"</td>";
                        html += "<td>"+data_3+"</td>";
                        if(typeof(spec_value[i]) != "undefined" && typeof(spec_value[i][j]) != "undefined" && typeof(spec_value[i][j][m]) != "undefined"){
                            var price = spec_value[i][j][m].price;
                            var discount_0 = spec_value[i][j][m].discount_0;
                            var discount_1 = spec_value[i][j][m].discount_1;
                            var discount_2 = spec_value[i][j][m].discount_2;
                            var discount_3 = spec_value[i][j][m].discount_3;
                            var discount_4 = spec_value[i][j][m].discount_4;
                            var num = spec_value[i][j][m].num;
                            var goods_id = spec_value[i][j][m].goods_id;
                        }else{
                            var price = $("#goodscommonmodel-price").val();
                            var discount_0 = $("#goodscommonmodel-discount_0").val();
                            var discount_1 = $("#goodscommonmodel-discount_1").val();
                            var discount_2 = $("#goodscommonmodel-discount_2").val();
                            var discount_3 = $("#goodscommonmodel-discount_3").val();
                            var discount_4 = $("#goodscommonmodel-discount_4").val();
                            var num = goods_id = "";
                        }

                        html += "<td><input type='text' class=' form-control' value='"+price+"' name='spec_value["+i+"]["+j+"]["+m+"][price]'/></td>";
                        html += "<td><input type='text' class=' form-control' value='"+discount_0+"' name='spec_value["+i+"]["+j+"]["+m+"][discount_0]'/></td>";
                        html += "<td><input type='text' class=' form-control' value='"+discount_1+"' name='spec_value["+i+"]["+j+"]["+m+"][discount_1]'/></td>";
                        html += "<td><input type='text' class=' form-control' value='"+discount_2+"' name='spec_value["+i+"]["+j+"]["+m+"][discount_2]'/></td>";
                        html += "<td><input type='text' class=' form-control' value='"+discount_3+"' name='spec_value["+i+"]["+j+"]["+m+"][discount_3]'/></td>";
                        html += "<td><input type='text' class=' form-control' value='"+discount_4+"' name='spec_value["+i+"]["+j+"]["+m+"][discount_4]'/></td>";
                        html += "<td><input type='text' class='num_list form-control' value='"+num+"'  name='spec_value["+i+"]["+j+"]["+m+"][num]'/></td>";
                        html += "<td><input type='hidden' class=' form-control' value='"+goods_id+"' name='spec_value["+i+"]["+j+"]["+m+"][goods_id]'/></td>";
                        html += "</tr>"; 
                    }
                }                
            }
            $("#kucun_config_tbody").html(html);
            
            //库存
            var obj_num = $(".num_list");
            var length = obj_num.length;
            var kucun = 0;
            for(var i = 0;i<length;i++){
                var l = obj_num.eq(i).val();
                if(parseInt(l)){
                    kucun += parseInt(l);
                }
                
            }
            $("#goodscommonmodel-num").val(kucun);
        }
        
        
        $(function(){
            createKucun_config();
        })
        
        


        $(document).on("blur",".spec_name_detail",function(){
            createKucun_config();
        })
        $(document).on("blur",".num_list",function(){
            //库存
            var obj_num = $(".num_list");
            var length = obj_num.length;
            var kucun = 0;
            for(var i = 0;i<length;i++){
                var l = obj_num.eq(i).val();
                if(parseInt(l)){
                    kucun += parseInt(l);
                }
            }
            $("#goodscommonmodel-num").val(kucun);            
        })        
        
    </script>
    
    
    <?= $form->field($model, 'name') ?>

    <div class="form-group field-goodsmodel-name required">
        <div class="col-xs-3 col-sm-2 text-right">
            <label class="control-label" for="goodsmodel-name">商品标签</label>
        </div>
        <div class="label_list col-xs-9 col-sm-10">
            <?php $labels = $model->getLabels();  ?>
            <?php  foreach($labels as $label){  ?>
                <?php include '_label.php';  ?>
            <?php }  ?>
        </div>
        <div class="col-xs-3 col-sm-2 text-right">
            <button type="button" class="add_label btn btn-info">增加</button>
        </div>        
    </div>   
    
    <script>
        //删除标签
        $(document).on('click',".label-close",function(){
            $(this).parents('.label_detail').eq(0).remove();
        })
        var LABELS = '';
        //新加标签
        $(".add_label").click(function(){
            if(LABELS){
                label_open();
            }else{
                $.post("/goods_label/ajaxgetlabels",{},function(data){
                    if(data.status){
                        LABELS = data.content;
                        label_open();
                    }
                },'json');
            }
        })
        
        function label_open()
        {
            layer.open({
                content: LABELS,
                btn: ['确认', '取消'],
                yes: function(){
                    
                    var obj = $("[name='label_list']").eq(0);
                    var value = obj.val();
                    var data_value = obj.find('option[value="'+value+'"]').html();
                    data_value = data_value.replace(/├\s*/,"");
                    console.log(value);
                    var labels = $(".label_detail input[name='label[]']");
                    var length = labels.length;
                    for(var i = 0;i<length;i++){
                        var v = labels.eq(i).val();
                        if(value == v){
                            layer.open({
                                className: 'alertMsg',
                                content: '已经存在此标签了',
                                time:2
                            }); 
                            return false;
                        }
                    }
                    //增加标签
                    var html = '';
                    html += '<div class="label_detail">';
                    html += '<span>'+data_value+'</span>';
                    html += '<input type="hidden" value="'+value+'" name="label[]"/>';
                    html += '<div class="update label-close"></div>          ';
                    html += '</div>';
                    $(".label_list").append(html);
                    layer.close();
                }
            });                
        }
    </script>
    
    
    
    
    <?= $form->field($model, 'num') ?>
    <?= $form->field($model, 'price') ?>
    <?= $form->field($model, 'discount_0')->hint('最多一个小数,请填写百分比') ?>
    <?= $form->field($model, 'discount_1')->hint('同上') ?>
    <?= $form->field($model, 'discount_2')->hint('同上') ?>
    <?= $form->field($model, 'discount_3')->hint('同上') ?>
    <?= $form->field($model, 'discount_4')->hint('同上') ?>
    <?= $form->field($model, 'weight')->hint('单位为千克') ?>
    <?= $form->field($model, 'sorting')->hint('排序越大越靠前') ?>
    <?= $form->field($model, 'cid')->dropDownList(\common\models\GoodsClassModel::getMenuKeyValue(false)) ?>
    <?= $form->field($model, 'has_zheko_bili')->hint('提成比例(填写百分比数字比如提成百分10,则填10)')->label("提成比例") ?>
    
    <?php if($this->context->user->role_id == \backend\components\Member::SUPER_ADMIN){ ?>
        <?= $form->field($model, 'status')->dropDownList(\common\models\GoodsModel::getStatusHtml()) ?>  
    
    <?php }  ?>
    
    
    <?= $form->field($model, 'type')->dropDownList(\common\models\GoodsModel::getTypeHtml()) ?>  
    <?= $form->field($model, 'score')->hint('所需要积分(0或者留空,表示是普通商品 >0表示的是积分商品,需要消费相应的积分才能购买)') ?>
    
    <?php  $date = date('Ymd'); ?>
    <?php //Form::getFileContent($form,$model,'img','img', $date . '/goods'); ?>
    <?= Form::getFileContentJietu($form,$model,'img','img', $date . '/goods'); ?>  
    
    <?= Form::getFileContent($form,$model,'img_list','img_list', $date . '/goods','',[],'',true); ?>
    
    
    <?= Form::editor_element_detail($model, 'details');  ?>
    
    <?php include '_mine_attributes.php';  ?>
<!--    属性相关的东西-->    
    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>
    

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(function(){
        $(document).on("click","#spec_name_label_list .delete_element",function(){
            createKucun_config();
        })         
    })
</script>