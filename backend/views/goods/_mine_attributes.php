<!--    考虑怎么选你想要的商品呢？？？？？把你想要的商品给加入进来-->
    <div class="form-group">
        <div class="col-xs-3 col-sm-2 text-right">
            <label class="control-label">属性</label>
        </div>
        <div id="_mine_attributes_list" class="col-xs-6 col-sm-4">
            <?php $shop_list =  json_decode($model['mine_attributes'],true); ?>
            <?php if($shop_list){  ?>
                <?php foreach($shop_list as $key => $value){  ?>
                    <div data="<?= $key ?>" class="form-group">
                        <div class="col-xs-5 col-sm-5">
                            <input class="form-control" type="text" value="<?= $value['key']  ?>" name="mine_attributes[<?= $key ?>][key]" placeholder="请填写名称！" nullmsg="请填写名称！"/>
                        </div>   
                        <div class="col-xs-5 col-sm-5">
                            <input class="form-control" type="text" value="<?= $value['value']  ?>" name="mine_attributes[<?= $key ?>][value]" placeholder="请填写对应的值！" nullmsg="请填写对应的值！"/>
                        </div>  
                        <?php if($key != 0){  ?>
                            <div class="col-xs-2 col-sm-2">
                                <button data="<?= $key ?>" type="button" class="delete_element_mine_attributes btn btn-danger">删除</button>  
                            </div>                         
                        <?php }  ?>                  
                    </div>            
                <?php }  ?>            
            <?php  }else{ ?>
                <div data="0" class="form-group">
                    <div class="col-xs-5 col-sm-5">
                        <input class="form-control" type="text" name="mine_attributes[0][key]" placeholder="请填写名称！" nullmsg="请填写名称！"/>
                    </div>   
                    <div class="col-xs-5 col-sm-5">
                        <input class="form-control" type="text" name="mine_attributes[0][value]" placeholder="请填写对应的值！" nullmsg="请填写对应的值！"/>
                    </div>                
                </div>            
            <?php }  ?>

        </div>
        <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
            <button type="button" class="add_element_mine_attributes btn btn-info">增加</button>  
        </div>
    </div>        
    <?php include '_mine_attributes_from.php';  ?>
    
    <script>
    $(function(){
        //增加元素
        function add_element(num)
        {
            var html = $("#_mine_attributes_from").html();//寻找需要的字符串
            html = html.replace(/NUM_TODO/g,num);//进行要关的替换
            html = html.replace(/ATTRIBUTES_TODO/g,'mine_attributes');//进行要关的替换
            //开始插入
            $("#_mine_attributes_list").append(html);
        }
        //删除元素
        function delete_element(num)
        {
            $("#_mine_attributes_list > [data='"+num+"']").eq(0).remove();
        }

        $(document).on('click','.delete_element_mine_attributes',function(){
            var num = $(this).attr('data');
            delete_element(num);
        })

        $(".add_element_mine_attributes").click(function(){
            var num = $("#_mine_attributes_list div[data]").last().attr('data');
            num = parseInt(num) + 1;
            add_element(num);
        })            
    })
</script>