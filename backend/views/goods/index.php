<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>商品列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'type')->dropDownList($object::getTypeHtml(),['prompt' => '全部']) ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                
                <a style="color:white;text-decoration:none" href="index_excel?<?= $_SERVER['QUERY_STRING'] ?>">
                    <?= Html::buttonInput('导出', ['class' => 'btn btn-primary excel_download']) ?>
                </a>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    

 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'id',//直接展示
            [
                 'attribute' => 'name',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods/index?gid={$data->id}";
                    $html = "<a target='_blank' href='{$url}' class='' >{$data['name']}</a>";
                    return $html;
                 },
             ],            
            'price',//直接展示
            'discount_1',//直接展示
            'discount_2',//直接展示
            'discount_3',//直接展示
            'num',//直接展示        
            [
                'attribute' => 'sorting',
                'value' => function($model){
                     $html = "<input class='sorting_img form-control' data='{$model['id']}' type='text' name='' value='{$model['sorting']}'/>";
                     return $html;
                },  
               'format' => 'raw',     
            ],               
            [
                'attribute' => 'status',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getStatusHtml($model['status']);
                },   
            ],            
            [
                'label' => "上传用户",
                'attribute' => 'uid',
                'value' => function($model){
                    return \backend\models\AdminMemberModel::getOne($model['uid'], 'username');
                },   
            ],     
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示
            [
                 'attribute' => 'img',
                 'format' => 'html',    
                 'value' => function ($data) {
                     return Html::img($data['img'],
                         ['width' => '70px']);
                 },
             ],            
            [
               'value' => function($model, $key, $index, $column){
                    $is_operation = \backend\models\AdminAccessModel::rbac($this->context->user->getMember(), "goods", "update");
                    if($is_operation){
                        $html = "<a href='update?id={$model['id']}' class='' >修改</a>";
                        $html .= "</br>";   
                    }
                    
                    
                    $is_operation = \backend\models\AdminAccessModel::rbac($this->context->user->getMember(), "goods", "delete");
                    if($is_operation){
                        $html .= "<a href='delete?id={$model['id']}' class='' >删除</a>";
                        $html .= "</br>";
                    }     
                    
                    
                    $is_operation = \backend\models\AdminAccessModel::rbac($this->context->user->getMember(), "goods", "examine");
                    if($is_operation && $model['status'] == 2){
                        $html .= "<a href='examine?id={$model['id']}&status=1' class='' >审核通过</a>";
                        $html .= "</br>";
                        $html .= "<a href='examine?id={$model['id']}&status=3' class='' >拒绝</a>";
                    }
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

    <?= ''//Html::button('我要删除',['class' => 'select_list btn','url' => '/ab/sd']) ?>

</div>
<script>
    $(function(){
        $(".sorting_img").keyup(function(){
            var data = parseInt($(this).val());
            var id = $(this).attr("data");
            $.post("update",{id:id,sorting:data},function(data){
                //layer.msg(data.info);
            },"json");
        })
    })
</script>