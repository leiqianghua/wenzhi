<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>商品列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'cid')->dropDownList(common\models\GoodsClassModel::getMenuKeyValue(),['prompt' => '全部'])->label("分类") ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        
 <div>
    <button type="button" id="is_ok" class="btn btn-info">确认</button>
    <button type="button" id="is_no_ok" class="btn btn-info">取消</button>
</div>       
        
    </div>    

 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'id',//直接展示
            [
                 'attribute' => 'name',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods/index?gid={$data->id}";
                    $html = "<a target='_blank' href='{$url}' class='' >{$data['name']}</a>";
                    return $html;
                 },
             ],                
            [
                 'attribute' => 'img',
                 'format' => 'html',    
                 'value' => function ($data) {
                     return Html::img($data['img'],
                         ['width' => '70px']);
                 },
             ],             
        ],
    ]); ?>

    <?= ''//Html::button('我要删除',['class' => 'select_list btn','url' => '/ab/sd']) ?>

</div>



<script>
    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引

    //让层自适应iframe
    $('#add').on('click', function(){
        $('body').append('插入很多酱油。插入很多酱油。插入很多酱油。插入很多酱油。插入很多酱油。插入很多酱油。插入很多酱油。');
        parent.layer.iframeAuto(index);
    });
    //在父层弹出一个层
    $('#new').on('click', function(){
        parent.layer.msg('Hi, man', {shade: 0.3})
    });
    //给父页面传值
    $('#is_ok').on('click', function(){
        //选择了哪些东西拿到具体的数据来.
        var obj = $("[name='selection[]']:checked");
        var select_ids = [];
        var length = obj.length;
        if(!length){
            layer.msg("请选择相关商品");
        }
        for(var i = 0;i<length;i++){
            select_ids[i] = {};
            select_ids[i].num = 1;
            select_ids[i].id = obj.eq(i).val();
        }
        
        var num = parent.$('#goods_list_select_ok').find("tr[key]").last().attr("key");
        
        var obj_parent = parent.$('.shop_detail_id');
        var length = obj_parent.length;
        var has_select_id = [];
        for(var i = 0;i<length;i++){
            has_select_id[i] = obj_parent.eq(i).val();
        }        
        
        
        $.post("/goods_package/ajax_select_goods",{shop_ids:select_ids,num:num,has_select_id:has_select_id},function(data_content){
            parent.$('#goods_list_select_ok').append(data_content.content);
            parent.layer.close(index);
        },"json")
//        
//        parent.$('#parentIframe').text('我被改变了');
//        parent.layer.tips('Look here', '#parentIframe', {time: 5000});
//        parent.layer.close(index);
    });
    //关闭iframe
    $('#is_no_ok').click(function(){
//        var val = $('#name').val();
//        if(val === ''){
//            parent.layer.msg('请填写标记');
//            return;
//        }
//        parent.layer.msg('您将标记 [ ' +val + ' ] 成功传送给了父窗口');
        parent.layer.close(index);
    });    
</script>