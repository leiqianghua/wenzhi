<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    <?= $form->field($model, 'name') ?>
    
    <div class="form-group field-goodsmodel-name required">
        <div class="col-xs-3 col-sm-2 text-right">
            <label class="control-label" for="goodsmodel-name">商品标签</label>
        </div>
        <div class="label_list col-xs-9 col-sm-10">
            <?php $labels = $model->getLabels();  ?>
            <?php  foreach($labels as $label){  ?>
                <?php include '_label.php';  ?>
            <?php }  ?>
        </div>
        <div class="col-xs-3 col-sm-2 text-right">
            <button type="button" class="add_label btn btn-info">增加</button>
        </div>        
    </div>   
    
    <script>
        //删除标签
        $(document).on('click',".label-close",function(){
            $(this).parents('.label_detail').eq(0).remove();
        })
        var LABELS = '';
        //新加标签
        $(".add_label").click(function(){
            if(LABELS){
                label_open();
            }else{
                $.post("/goods_label/ajaxgetlabels",{},function(data){
                    if(data.status){
                        LABELS = data.content;
                        label_open();
                    }
                },'json');
            }
        })
        
        function label_open()
        {
            layer.open({
                content: LABELS,
                btn: ['确认', '取消'],
                yes: function(){
                    
                    var obj = $("[name='label_list']").eq(0);
                    var value = obj.val();
                    var data_value = obj.find('option[value="'+value+'"]').html();
                    data_value = data_value.replace(/├\s*/,"");
                    console.log(value);
                    var labels = $(".label_detail input[name='label[]']");
                    var length = labels.length;
                    for(var i = 0;i<length;i++){
                        var v = labels.eq(i).val();
                        if(value == v){
                            layer.open({
                                className: 'alertMsg',
                                content: '已经存在此标签了',
                                time:2
                            }); 
                            return false;
                        }
                    }
                    //增加标签
                    var html = '';
                    html += '<div class="label_detail">';
                    html += '<span>'+data_value+'</span>';
                    html += '<input type="hidden" value="'+value+'" name="label[]"/>';
                    html += '<div class="update label-close"></div>          ';
                    html += '</div>';
                    $(".label_list").append(html);
                    layer.close();
                }
            });                
        }
    </script>    
    
    
    <?php //include '_shop_list.php';  ?>
    <h3><button type="button" id="select_goods" class="btn btn-info">选择商品</button></h3>
    <script>
        $(function(){
            $(document).on("click","#select_goods",function(){
                var obj = $(this);

            layer.open({
              type: 2,
              title: '选择商品',
              shadeClose: true,
              shade: 0.8,
              area: ['80%', '80%'],
              content: '/goods/select_index' //iframe的url
            });  


            })
        })
    </script>    
    <div id="_shop_select">
        <?php  $shop_ids = json_decode($model->shop_list,true);  ?>
        
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>商品ID</th>
                    <th>商品名称</th>
                    <th>商品图片</th>
                    <th>商品件数</th>
                </tr>
            </thead>
            <tbody id="goods_list_select_ok">
                <?php include '_shop_select.php';  ?>

            </tbody>
        </table>        
        
        
    </div>

    
    
    
    <?= $form->field($model, 'sorting') ?>
    <?= $form->field($model, 'shop_fuwu')->hint('填写附加商品的ID。示例：1,3,6') ?>


    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'img','img', $date . '/goods_package'); ?>    
    <?= Form::getFileContent($form,$model,'img_list','img_list', $date . '/goods_package','',[],'',true); ?>
    <?= $form->field($model, 'status')->dropDownList(\common\models\GoodsPackageModel::getStatusHtml()) ?>  
    
    <?= $form->field($model, 'cid')->dropDownList(\common\models\GoodsPackageClassModel::getMenuKeyValue(false))->label("分类") ?>

    <?= Form::editor_element_detail($model, 'details');  ?>
    
    
    <?php include '_mine_attributes.php';  ?>
<!--    属性相关的东西-->
    



    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(document).on("click",".delete_select",function(){
        $(this).parents("tr").eq(0).remove();
    })    
</script>