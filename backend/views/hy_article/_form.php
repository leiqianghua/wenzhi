<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    
<?= $form->field($model, 'title')->textInput() ?>
<?= $form->field($model, 'synopsis')->textarea() ?>
<?= $form->field($model, 'title_1')->textInput()->label("子标题")->hint("当是子篇的时候需要填写") ?>
<?php  $date = date('Ymd'); ?>
<?= Form::getFileContent($form,$model,'img','img', $date . '/dan'); ?>
    
    <?= Form::editor_element_detail($model, 'content');  ?>
<?= $form->field($model, 'status')->dropDownList($object::getStatusHtml()) ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
