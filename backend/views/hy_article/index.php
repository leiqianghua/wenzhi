<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>文章列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>            
            
            <?= $form->field($model, 'status')->dropDownList($object::getstatusHtml(),['prompt' => '全部']) ?>
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            
            
'id',
'title',
'synopsis',
'img',
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示
            [
                'attribute' => 'status',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getStatusHtml($model->status);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',"template" => ' {update} {delete}'],    
        ],
    ]); ?>

    <?= Html::button('删除',['class' => 'select_list btn','url' => $this->context->to(["delete"])]) ?>

</div>
