<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
?>
<script src="/js/area_array.js"></script>
<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    
<?= $form->field($model, 'zaojia')->dropDownList($object::getzaojiaHtml()) ?>
<?= $form->field($model, 'fenge')->dropDownList($object::getfengeHtml()) ?>
    
    
    
<?php require_once '_type.php';   ?>    

    
    
<?= $form->field($model, 'title')->textInput() ?>
<?= $form->field($model, 'title1')->textInput() ?>

<?php 
    $content = json_decode($model->label,true);
    echo  \common\components\Tool::getDataForViewList("label", "/hy_case/_label", $content, "标签"); 
?>    
    
    
<?php  $date = date('Ymd'); ?>
<?= Form::getFileContent($form,$model,'img','img', $date . '/hy_case'); ?>
<?= $form->field($model, 'sorting')->textInput() ?>

    
    
<div class="form-group field-h_casemodel-title1">
    <div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="h_casemodel-title1">城市选择</label></div>
    <div class="col-xs-9 col-sm-10 form-inline">
        <?php echo Form::getSelectData([], "shen", ['id' => "shen","class" => "form-control"], true, "", "...请选择");  ?>   
        <?php echo Form::getSelectData([], "city", ['id' => "city","class" => "form-control"], true, "", "...请选择");  ?>      
    </div>
</div>    

    
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'content_imgs','content_imgs', $date . '/content_imgs/imgs','',[],'',true); ?>
    
    
<?= $form->field($model, 'status')->dropDownList($object::getstatusHtml()) ?> 
    
<?php  if(\yii::$app->controller->user->isSuper()){   ?>    
<?= $form->field($model, 'designer_id')->dropDownList(common\models\hy_designerModel::getAllForHtml([3,4,7,8,11,12]),['name' => "designer_id"])->label("指定上传用户") ?> 
<?php } ?>    
    
<?= $form->field($model, 'price')->label("造价")->hint("单位：万元") ?> 
<?= $form->field($model, 'size')->label("大小")->hint("单位：平方米") ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>
    $("#h_casemodel-designer_name").val($("#h_casemodel-designer_id").find("option:selected").text());
    $("#h_casemodel-studio_name").val($("#h_casemodel-studio_id").find("option:selected").text());
    
    $("#h_casemodel-designer_id").change(function(){
        $("#h_casemodel-designer_name").val($("#h_casemodel-designer_id").find("option:selected").text());
    })    
    $("#h_casemodel-studio_id").change(function(){
        $("#h_casemodel-studio_name").val($("#h_casemodel-studio_id").find("option:selected").text());
    })
    
</script>


<script>
    $(function(){
        var html = [];
        var length = nc_a[0].length;
        for(var i = 0;i<length;i++){
            var h = "<option value='"+nc_a[0][i][0]+"'>"+nc_a[0][i][1]+"</option>";
            html[i] = h;
        }
        $("#shen").append(html);
        
        
        var city = "<?= $model->city ?>";
        if(city){
            var shen = "";
            for(var i = 0;i<length;i++){
                var parent_id = nc_a[0][i][0];
                var lenlen = nc_a[parent_id].length;
                for(var j = 0;j<lenlen;j++){
                    if(nc_a[parent_id][j][1] == city){
                        shen = parent_id;
                        break;
                    }
                }
                if(shen){
                    break;
                }
            }
            if(shen){
                $("#shen").val(shen);
                setDataForShen(shen);
                $("#city").val(city);
            }
        }
        
        
        
        $("#shen").change(function(){
            var shen = $(this).val();
            if(shen){
                console.log(shen);
                setDataForShen(shen);
            }
        })
        
        function setDataForShen(shen){
            var html = [];
            var length = nc_a[shen].length;
            for(var i = 0;i<length;i++){
                var h = "<option value='"+nc_a[shen][i][1]+"'>"+nc_a[shen][i][1]+"</option>";
                html[i] = h;
                $("#city").html(html);
            }            
        }
        
        
        
        //指定谁就是谁的
//        function getMineData()
//        {
//            var uid = $("#hy_casemodel-designer_id").val();//根据这个名字来确认我们的类型是要哪一个
//        }
    })
</script>