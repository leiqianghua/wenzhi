<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>案例</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>   
            
            <?= $form->field($model, 'zaojia')->dropDownList($object::getzaojiaHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'fenge')->dropDownList($object::getfengeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'type')->dropDownList($object::gettypeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'studio_id')->dropDownList(\common\models\hy_studioModel::getBaseKeyValue("id","name"),['prompt' => '全部']) ?>
            <?= $form->field($model, 'designer_id')->dropDownList(\common\models\hy_designerModel::getBaseKeyValue("uid","nickname"),['prompt' => '全部']) ?>
            <?= $form->field($model, 'status')->dropDownList($object::getstatusHtml(),['prompt' => '全部']) ?>
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            
            
'id',
            [
                'attribute' => 'zaojia',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getzaojiaHtml($model->zaojia);
                }
            ],
            [
                'attribute' => 'fenge',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getfengeHtml($model->fenge);
                }
            ],
            [
                'attribute' => 'type',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::gettypeHtml($model->type);
                }
            ],
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示
'title',
'title1',
            [
                'attribute' => 'label',
                'value' => function($model){
                    $html = [];
                    $d = json_decode($model['label'],true);
                    if($d){
                        foreach($d as $k => $v){
                            $html[] = $v['data'];
                        }
                    }
                    return implode(" ", $html);
                }
            ],
            ['attribute' => 'img','format' => ['image',['style' => 'width:100%;height:100px']]],//图片展示
'sorting',
'city',
'designer_name',
'studio_name',
            [
                'attribute' => 'status',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getstatusHtml($model->status);
                }
            ],
                    

            [
               'value' => function($model, $key, $index, $column){
                    $html = "<a href='update?id={$model['id']}' class='layer_open_url is_reload' ><span class='glyphicon glyphicon-pencil'></span></a>";
                    $html .= "<a href='delete?id={$model['id']}' class='ajax_request' ><span class='glyphicon glyphicon-trash'></span></a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],                      

        ],
    ]); ?>

    <?= Html::button('删除',['class' => 'select_list btn','url' => $this->context->to(["delete"])]) ?>

</div>
