<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
<?php 
    if(!$model->parent_id){
        $model->parent_id = common\components\GuestInfo::getParam("parent_id"); 
    }

    if(!$model->type){
        $model->type = common\models\hy_categoryModel::getOne($model->parent_id, "type");
    }   
    if(!$model->type){
        $model->type = 1;
    }
?>

    
<?= $form->field($model, 'type')->radioList($object::getTypeHtml());  ?>
<?php  
if(\yii::$app->controller->action->id == "create"){
    echo $form->field($model, 'title')->textarea(['style' => "height:300px;widht:100%"])->hint("名称与简介用两个@@分开,多个分行输入");
}else{
    echo $form->field($model, 'title')->textInput();
    echo $form->field($model, 'title_1')->textarea()->label("简介");
}
?>

    
<div class="form-group field-hy_categorymodel-parent_id">
<div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="hy_categorymodel-parent_id">父类id</label></div>


<div id="parent_select" class="col-xs-6 col-sm-4">
    <?php 
    $type = $model->type;
    $select_data = $model->parent_id;
    require '_get_type.php';
    ?>

    
</div>

<div style="padding-top:7px;" class="col-xs-3 col-sm-2"></div>
</div>    
    

    
<?= $form->field($model, 'status')->checkbox() ?>
<?= $form->field($model, 'sorting')->textInput() ?>
<?php  $date = date('Ymd'); ?>
<?= Form::getFileContent($form,$model,'img','img', $date . '/dan'); ?>
    
    

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>
    $(function(){
        $("input[name='hy_categoryModel[type]']").change(function(){
            var type = $(this).val();
            console.log(type);
            $.post("ajax_ok",{type:type},function(data){
                if(data.status){
                    $("#parent_select").html(data.content);
                }
            },"json");
        })
    })
</script>