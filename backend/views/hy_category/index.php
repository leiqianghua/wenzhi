<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<link rel="stylesheet" href="/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
  <script type="text/javascript" src="/zTree_v3/js/jquery.ztree.core.js"></script>
  <script type="text/javascript" src="/zTree_v3/js/jquery.ztree.excheck.js"></script>
  <script type="text/javascript" src="/zTree_v3/js/jquery.ztree.exedit.js"></script>
<div class="admin-role-model-index">

    <h1>列表示例</h1>

    <div class="zTreeDemoBackground left">
            <ul id="treeDemo" class="ztree"></ul>
            
            
            <button id="ok">确认</button>
    </div>    

</div>
	<style type="text/css">
            .ztree li span.button.add {margin-left:2px; margin-right: -1px; background-position:-144px 0; vertical-align:top; *vertical-align:middle}
	</style>  
  
  
<SCRIPT type="text/javascript">
        var setting = {
            check: {
                enable: true,
                chkStyle:"checkbox",
                chkboxType:{ "Y": "s", "N": "s" },
                chkDisabledInherit:true,
            },            
            callback:{
//                onNodeCreated:function(event,treeId,treeNode){
//                    $("#" + treeNode.tId + " > a").eq(0).addClass("layer_open_url").addClass("is_reload");
//                },
                beforeDrag: beforeDrag,
                beforeDrop: beforeDrop,
                beforeDragOpen: beforeDragOpen,
                onDrag: onDrag,
                onDrop: onDrop,
                
            },
            data: {
                    simpleData: {
                            enable: true
                    }
            },
            edit: {
                    drag: {
                            autoExpandTrigger: true,
                            prev: true,
                            //inner: dropInner,
                            next: true,
                            isMove:true,
                    },
                    enable: true,
                    showRemoveBtn: true,
                    showRenameBtn: true
            },
            view: {
                    addHoverDom: addHoverDom,
                    removeHoverDom: removeHoverDom,
                    selectedMulti: false
            },            
        };

        var zNodes =  <?= json_encode($datas);  ?>;
        $(document).ready(function(){
            $.fn.zTree.init($("#treeDemo"), setting, zNodes);
        });
        
        
		var newCount = 1;
		function addHoverDom(treeId, treeNode) {
			var sObj = $("#" + treeNode.tId + "_span");
			if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
			var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='add node' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var btn = $("#addBtn_"+treeNode.tId);
			if (btn) btn.bind("click", function(){
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				zTree.addNodes(treeNode, {id:(100 + newCount), pId:treeNode.id, name:"new node" + (newCount++)});
				return false;
			});
		};
		function removeHoverDom(treeId, treeNode) {
			$("#addBtn_"+treeNode.tId).unbind().remove();
		};        
        
        
        $(function(){
            $("#delete").click(function(){
                var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                var checkCount = zTree.getCheckedNodes(true);
                console.log(checkCount);
                if(checkCount.length){
                    var ids = [];
                    for(var i = 0;i<checkCount.length;i++){
                        ids[i] = checkCount[i].id;
                    }
                    $.post("delete",{id:ids},function(data){
                        if(data.status){
                            layer.msg(data.info,function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.info);
                        }
                    },"json");
                }else{
                    layer.msg("请勾选相关的要删除的选项");
                }
            })
        })
        
		function addHoverDom(treeId, treeNode) {
			var sObj = $("#" + treeNode.tId + "_span");
			if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
			var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='add node' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var btn = $("#addBtn_"+treeNode.tId);
			if (btn) btn.bind("click", function(){
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				zTree.addNodes(treeNode, {id:(100 + newCount), pId:treeNode.id, name:"new node" + (newCount++)});
				return false;
			});
		};
		function removeHoverDom(treeId, treeNode) {
			$("#addBtn_"+treeNode.tId).unbind().remove();
		};        
        
        
		function dropPrev(treeId, nodes, targetNode) {
			var pNode = targetNode.getParentNode();
			if (pNode && pNode.dropInner === false) {
				return false;
			} else {
				for (var i=0,l=curDragNodes.length; i<l; i++) {
					var curPNode = curDragNodes[i].getParentNode();
					if (curPNode && curPNode !== targetNode.getParentNode() && curPNode.childOuter === false) {
						return false;
					}
				}
			}
			return true;
		}
		function dropInner(treeId, nodes, targetNode) {
			if (targetNode && targetNode.dropInner === false) {
				return false;
			} else {
				for (var i=0,l=curDragNodes.length; i<l; i++) {
					if (!targetNode && curDragNodes[i].dropRoot === false) {
						return false;
					} else if (curDragNodes[i].parentTId && curDragNodes[i].getParentNode() !== targetNode && curDragNodes[i].getParentNode().childOuter === false) {
						return false;
					}
				}
			}
			return true;
		}
		function dropNext(treeId, nodes, targetNode) {
			var pNode = targetNode.getParentNode();
			if (pNode && pNode.dropInner === false) {
				return false;
			} else {
				for (var i=0,l=curDragNodes.length; i<l; i++) {
					var curPNode = curDragNodes[i].getParentNode();
					if (curPNode && curPNode !== targetNode.getParentNode() && curPNode.childOuter === false) {
						return false;
					}
				}
			}
			return true;
		}

		var log, className = "dark", curDragNodes, autoExpandNode;
		function beforeDrag(treeId, treeNodes) {
			className = (className === "dark" ? "":"dark");
			showLog("[ "+getTime()+" beforeDrag ]&nbsp;&nbsp;&nbsp;&nbsp; drag: " + treeNodes.length + " nodes." );
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].drag === false) {
					curDragNodes = null;
					return false;
				} else if (treeNodes[i].parentTId && treeNodes[i].getParentNode().childDrag === false) {
					curDragNodes = null;
					return false;
				}
			}
			curDragNodes = treeNodes;
			return true;
		}
		function beforeDragOpen(treeId, treeNode) {
			autoExpandNode = treeNode;
			return true;
		}
		function beforeDrop(treeId, treeNodes, targetNode, moveType, isCopy) {
			className = (className === "dark" ? "":"dark");
			showLog("[ "+getTime()+" beforeDrop ]&nbsp;&nbsp;&nbsp;&nbsp; moveType:" + moveType);
			showLog("target: " + (targetNode ? targetNode.name : "root") + "  -- is "+ (isCopy==null? "cancel" : isCopy ? "copy" : "move"));
			return true;
		}
		function onDrag(event, treeId, treeNodes) {
			className = (className === "dark" ? "":"dark");
			showLog("[ "+getTime()+" onDrag ]&nbsp;&nbsp;&nbsp;&nbsp; drag: " + treeNodes.length + " nodes." );
		}
		function onDrop(event, treeId, treeNodes, targetNode, moveType, isCopy) {
			className = (className === "dark" ? "":"dark");
			showLog("[ "+getTime()+" onDrop ]&nbsp;&nbsp;&nbsp;&nbsp; moveType:" + moveType);
			showLog("target: " + (targetNode ? targetNode.name : "root") + "  -- is "+ (isCopy==null? "cancel" : isCopy ? "copy" : "move"))
		}
		function onExpand(event, treeId, treeNode) {
			if (treeNode === autoExpandNode) {
				className = (className === "dark" ? "":"dark");
				showLog("[ "+getTime()+" onExpand ]&nbsp;&nbsp;&nbsp;&nbsp;" + treeNode.name);
			}
		}

		function showLog(str) {
			if (!log) log = $("#log");
			log.append("<li class='"+className+"'>"+str+"</li>");
			if(log.children("li").length > 8) {
				log.get(0).removeChild(log.children("li")[0]);
			}
		}
		function getTime() {
			var now= new Date(),
			h=now.getHours(),
			m=now.getMinutes(),
			s=now.getSeconds(),
			ms=now.getMilliseconds();
			return (h+":"+m+":"+s+ " " +ms);
		}

		function setTrigger() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			zTree.setting.edit.drag.autoExpandTrigger = $("#callbackTrigger").attr("checked");
		}        
        
</SCRIPT>

<script>
    function getAllData(obj)
    {
        var arr_data = [];
        //var data = $("#treeDemo > li");
        var length = obj.length;
        for(var i = 0;i<length;i++){
            var mine_data = obj.eq(i);
            arr_data[i] = {};
            arr_data[i].name = mine_data.children("a").attr("title");
            var nn_obj = mine_data.children("ul");
            if(nn_obj.length == 0){
                arr_data[i].data = {};
            }else{
                nn_obj = nn_obj.children("li");
                arr_data[i].data = getAllData(nn_obj);
            }
        }
        return arr_data;
    }
    $(function(){
        $("#ok").click(function(){
            var obj = $("#treeDemo > li");
            var data = getAllData(obj);
            var url = "/hy_category/all_update";
            $.post(url,{data:data},function(data){
                if(data.status){
                    layer.msg("更新成功");
                }else{
                    layer.msg("更新失败");
                }
            },"json");
        })
    })
</script>