<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<script src="/js/area_array.js"></script>
<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    
<?= $form->field($model, 'uid')->textInput(['disabled' => "true"]) ?>
<?= $form->field($model, 'nickname')->textInput() ?>
<?php  $date = date('Ymd'); ?>
<?= Form::getFileContent($form,$model,'img','img', $date . '/h_designer'); ?>
<?= $form->field($model, 'company')->textInput() ?>

    

    
    
<?= $form->field($model, 'work_year')->textInput() ?>
    
    
<div class="form-group field-h_casemodel-title1">
    <div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="h_casemodel-title1">城市选择</label></div>
    <div class="col-xs-9 col-sm-10 form-inline">
        <?php echo Form::getSelectData([], "shen", ['id' => "shen","class" => "form-control"], true, "", "...请选择");  ?>   
        <?php echo Form::getSelectData([], "city", ['id' => "city","class" => "form-control"], true, "", "...请选择");  ?>      
    </div>
</div>  
    
<?= $form->field($model, 'studio_name')->textInput(['disabled' => "true"]) ?>
    
    
<?php $model->gold_type = json_decode($model->gold_type,true)   ?>    
<?= $form->field($model, 'gold_type')->checkboxList($object::getgold_typeHtml(),['prompt' => '无']) ?>
<?= $form->field($model, 'mobile')->label("电话号码")->textInput() ?>
<?= $form->field($model, 'sexy')->dropDownList($object::getsexyHtml()) ?>
<?= $form->field($model, 'jiguan')->textInput() ?>
<?= $form->field($model, 'email')->textInput() ?>
<?= $form->field($model, 'biye')->textInput() ?>
<?= $form->field($model, 'zhuangye')->textInput() ?>
<?= $form->field($model, 'zizhi')->textarea() ?>
    
    <?= $form->field($model, 'jinyan')->textarea() ?>
    <?= $form->field($model, 'fenge_d')->textarea() ?>
    <?= $form->field($model, 'linian')->textarea() ?>    
    
    
<?php  if(!empty($shenfen['num_levels'][3])){  ?>    
    <?= $form->field($model, 'design_fee_min')->textInput() ?>
    <?= $form->field($model, 'design_fee_max')->textInput() ?>
    <?= $form->field($model, 'design_fee_type')->dropDownList($object::getdesign_fee_typeHtml()) ?>
    <?= $form->field($model, 'celiang_fee_min')->textInput() ?>
    <?= $form->field($model, 'celiang_fee_max')->textInput() ?>
    <?= $form->field($model, 'celiang_fee_type')->dropDownList($object::getceliang_fee_typeHtml()) ?>    
    <?= $form->field($model, 'xinshui')->textInput() ?>
<?php }  ?>    

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $("#h_designermodel-studio_name").val($("#h_designermodel-studio_id").find("option:selected").text());
    
    $("#h_designermodel-studio_id").change(function(){
        $("#h_designermodel-studio_name").val($("#h_designermodel-studio_id").find("option:selected").text());
    })    
    
</script>


<script>
    $(function(){
        var html = [];
        var length = nc_a[0].length;
        for(var i = 0;i<length;i++){
            var h = "<option value='"+nc_a[0][i][0]+"'>"+nc_a[0][i][1]+"</option>";
            html[i] = h;
        }
        $("#shen").append(html);
        
        
        var city = "<?= $model->city ?>";
        if(city){
            var shen = "";
            for(var i = 0;i<length;i++){
                var parent_id = nc_a[0][i][0];
                var lenlen = nc_a[parent_id].length;
                for(var j = 0;j<lenlen;j++){
                    if(nc_a[parent_id][j][1] == city){
                        shen = parent_id;
                        break;
                    }
                }
                if(shen){
                    break;
                }
            }
            if(shen){
                $("#shen").val(shen);
                setDataForShen(shen);
                $("#city").val(city);
            }
        }
        
        
        
        $("#shen").change(function(){
            var shen = $(this).val();
            if(shen){
                console.log(shen);
                setDataForShen(shen);
            }
        })
        
        function setDataForShen(shen){
            var html = [];
            var length = nc_a[shen].length;
            for(var i = 0;i<length;i++){
                var h = "<option value='"+nc_a[shen][i][1]+"'>"+nc_a[shen][i][1]+"</option>";
                html[i] = h;
                $("#city").html(html);
            }            
        }
    })
</script>