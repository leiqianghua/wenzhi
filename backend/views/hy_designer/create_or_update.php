<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

<?php $model->uid = $datas['designer_user']['studio_id'];  ?>    
<?= $form->field($model, 'uid')->dropDownList($studio,['name' => "studio_id",'prompt' => '请选择'])->label("团队"); ?>    
    
    

<?php 
if(!empty($datas['levels'])){
    $model->uid = array_keys($datas['levels']);
}else{
    $model->uid = [];
} 

?>    
<?= $form->field($model, 'uid')->checkboxList(common\models\hy_studio_levelModel::getBaseKeyValue("id", "title"),['name' => "studio_level",'prompt' => '请选择'])->label("身份标签"); ?>    
    


    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button('确定', ['class' => 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>