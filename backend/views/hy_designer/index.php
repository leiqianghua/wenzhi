<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>列表示例</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>   
            
            
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'design_fee_type')->dropDownList($object::getdesign_fee_typeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'celiang_fee_type')->dropDownList($object::getceliang_fee_typeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'gold_type')->dropDownList($object::getgold_typeHtml(),['prompt' => '全部']) ?>
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            
            
'uid',
            
            [
                'label' => "用户",
                'value' => function($model){
                    return common\models\hy_designerModel::getOne($model['uid'], "nickname");
                }
            ], 
            ['attribute' => 'img','format' => ['image',['style' => 'width:100%;height:100px']]],//图片展示
'company',
'work_year',
            
            [
                'label' => "设计费",
                'attribute' => 'label',
                'value' => function($model){
                    return "{$model['design_fee_min']}-{$model['design_fee_max']}";
                }
            ], 
            [
                'attribute' => 'design_fee_type',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getdesign_fee_typeHtml($model->design_fee_type);
                }
            ],  
            [
                'label' => "测量费",
                'attribute' => 'label',
                'value' => function($model){
                    if(!$model['design_fee_min'] && !$model['design_fee_min']){
                        return "免费";
                    }
                    return "{$model['celiang_fee_min']}-{$model['celiang_fee_max']}";
                }
            ],    
            [
                'attribute' => 'celiang_fee_type',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getceliang_fee_typeHtml($model->celiang_fee_type);
                }
            ],       
            
'city',
'studio_name',
            [
                'attribute' => 'gold_type',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getgold_typeHtml($model->gold_type);
                }
            ],
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示

            [
               'value' => function($model, $key, $index, $column){
                    $designer_level = $model->designer_level;
                    $html = [];
                    if($designer_level){
                        foreach($designer_level as $key => $value){
                            $html[] = $value['studio_level_name'];
                        }
                    }
                    return implode(",", $html);
               },
               'label' => '身份', 
               'format' => 'raw',                      
            ],  
                    

            [
               'value' => function($model, $key, $index, $column){
                    $html = "<a class='layer_open_url is_reload' href='".\yii::$app->controller->to(['set_parent','uid' => $model->uid])."'  >设置上级</a>";
                    $html .= "</br>";
                    $html .= "<a class='layer_open_url is_reload' href='".\yii::$app->controller->to(['create_or_update','uid' => $model->uid])."'  >修改身份</a>";
                    $html .= "</br>";
                    $html .= "<a class='layer_open_url is_reload' href='".\yii::$app->controller->to(['update','uid' => $model->uid])."'  >修改资料</a>";
                    $html .= "</br>";
                    $html .= "<a class='ajax_request' href='".\yii::$app->controller->to(['delete','uid' => $model->uid])."'  >删除员工</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
                       
                       
        ],
    ]); ?>


</div>
