<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    
<?= $form->field($model, 'uid')->dropDownList($can_set,['name' => "parent_id"])->label("上级") ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button("确定", ['class' => 'J_ajax_submit_btn btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
