<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    
<?= $form->field($model, 'uid')->textInput() ?>
<?= $form->field($model, 'name')->textInput() ?>
<?= $form->field($model, 'sex')->textInput() ?>
<?= $form->field($model, 'address_detail')->textInput() ?>
<?= $form->field($model, 'demand_type')->textInput() ?>
<?= $form->field($model, 'acreage')->textInput() ?>
<?= $form->field($model, 'space_type')->textInput() ?>
<?= $form->field($model, 'element')->textInput() ?>
<?= $form->field($model, 'style_type')->textInput() ?>
<?= $form->field($model, 'budget')->textInput() ?>
<?= $form->field($model, 'design_fee')->textInput() ?>
<?= $form->field($model, 'measure_fee')->textInput() ?>
<?= $form->field($model, 'complete_time')->textInput(['class' => 'form-control J_date length_3']) ?>
<?= $form->field($model, 'remark')->textInput() ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
