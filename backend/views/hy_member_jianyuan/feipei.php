<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    <?= $form->field($model, 'uid')->dropDownList($user, ['name' => "service_uid"])->label("选择项目跟踪人员") ?>
    


    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button('确认', ['class' => 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>