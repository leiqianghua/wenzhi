<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>用户建园需求</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>   
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'demand_type')->dropDownList($object::getdemand_typeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'acreage')->dropDownList($object::getacreageHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'space_type')->dropDownList(common\models\hy_space_typeModel::getBaseKeyValue("id", "title"),['prompt' => '全部']) ?>
            <?= $form->field($model, 'style_type')->dropDownList(\common\models\hy_caseModel::getfengeHtml(),['prompt' => '全部']) ?>  
            <?= $form->field($model, 'budget')->dropDownList($object::getbudgetHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'design_fee')->dropDownList($object::getdesign_feeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'measure_fee')->dropDownList($object::getmeasure_feeHtml(),['prompt' => '全部']) ?>       
            <?= $form->field($model, 'complete_time')->dropDownList($object::getcomplete_timeHtml(),['prompt' => '全部']) ?> 
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            
            
'id',
'uid',
'name',   
//            [
//                'attribute' => 'sex',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getsexHtml($model->sex);
//                }
//            ],     
//            [
//                'label' => "地址",
//                'value' => function($model){
//                    $html = "{$model['address_city']}";
//                    $html .= "{$model['address_detail']}";
//                    
//                }
//            ],   
//            [
//                'attribute' => 'demand_type',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getdemand_typeHtml($model->demand_type);
//                }
//            ],  
//            [
//                'attribute' => 'acreage',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getacreageHtml($model->acreage);
//                }
//            ],  
//            [
//                'attribute' => 'sex',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getsexHtml($model->sex);
//                }
//            ],   
//
//'space_type.title',
//'element.title', 
//            [
//                'attribute' => 'style_type',
//                'value' => function($model){
//                    return \common\models\h_caseModel::getfengeHtml($model['style_type']);
//                }
//            ],  
//            [
//                'attribute' => 'budget',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getbudgetHtml($model->budget);
//                }
//            ],    
//            [
//                'attribute' => 'design_fee',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getdesign_feeHtml($model->design_fee);
//                }
//            ],    
//            [
//                'attribute' => 'measure_fee',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getmeasure_feeHtml($model->measure_fee);
//                }
//            ],    
//            [
//                'attribute' => 'complete_time',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getcomplete_timeHtml($model->complete_time);
//                }
//            ],   

//'remark',
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示  
            
            
                    
            'request.money',    
                    
            
            ['attribute' => 'request.add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示      
            ['attribute' => 'request.speed_2_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示   
            ['attribute' => 'request.speed_3_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示   
            ['attribute' => 'request.speed_4_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示     
                  
            [
                'label' => "分配的业务员",
                'value' => function($model){
                    return \common\models\hy_designerModel::getOne($model->request->service_uid, "nickname");
                }
            ],        
            [
                'label' => "进度",
                'value' => function($model){
                    $d = get_class($model);
                    return \common\models\hy_member_requestModel::getspeedHtml($model->request->speed);
                }
            ],        
            
            [
               'value' => function($model, $key, $index, $column){
                    $html = "";
                    $html .= "<a href='show?id={$model['id']}' class='layer_open_url' >查看详情</a>";
                    $html .= "&nbsp&nbsp";
                    $html .= "<a href='/hy_member_request_use/index?request_id={$model['id']}' class='layer_open_url' >组成员</a>";
                    
                    $html .= "</br>";
                    $html .= "<a href='/hy_member_request_data/index?request_id={$model['id']}' class='layer_open_url' >类型</a>";

                    $html .= "&nbsp&nbsp";
                    $html .= "<a href='/hy_member_request_pay_record/index?request_id={$model['id']}' class='layer_open_url' >用户支付</a>";
                    
                    
                    $html .= "</br>";
                    $html .= "<a href='/hy_member_jianyuan/feipei?id={$model['id']}' class='layer_open_url' >分配跟踪项目业务员</a>";
                    $html .= "&nbsp&nbsp";
                    
                    
                    $input = "<input type='text' name='money' />";
                    $content = "<form class='layermmain'>"
                            . "<label>金额</label>"
                            . $input
                            . "</form>";                      
                    
                    $html .= "<a content=\"{$content}\" href='/hy_member_jianyuan/setmoney?id={$model['id']}' class='ajax_layer' >设置项目金额</a>";
                    
                    
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
//            [
//                'attribute' => 'status',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getstatusHtml($model->status);
//                }
//            ],   

        ],
    ]); ?>


</div>
