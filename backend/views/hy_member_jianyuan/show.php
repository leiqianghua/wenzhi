<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
$d = get_class($model);
?>
<div class="admin-role-model-view">


    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view dddd'],
        'template' => '<tr><th style="width:10%">{label}</th><td>{value}</td></tr>', 
        'attributes' => [
            [
                'attribute' => 'sex',
                'value' => $d::getsexHtml($model->sex),
            ],
            [
                'label' => "地址",
                'value' => implode(',', json_decode($model['address_city'])) . "{$model['address_detail']}",
                'format' => 'raw',
            ],   
            [
                'attribute' => 'demand_type',
                'value' => $d::getdemand_typeHtml($model->demand_type),
            ],  
            [
                'attribute' => 'acreage',
                'value' => $d::getacreageHtml($model->acreage),
            ],

'space_type.title',
'element.title', 
            [
                'attribute' => 'style_type',
                'value' => \common\models\hy_caseModel::getfengeHtml($model['style_type']),
            ],  
            [
                'attribute' => 'budget',
                'value' => $d::getbudgetHtml($model->budget),
            ],    
            [
                'attribute' => 'design_fee',
                'value' => $d::getdesign_feeHtml($model->design_fee),
            ],    
            [
                'attribute' => 'measure_fee',
                'value' => $d::getmeasure_feeHtml($model->measure_fee),
            ],    
            [
                'attribute' => 'complete_time',
                'value' => $d::getcomplete_timeHtml($model->complete_time),
            ],             
        ],
    ]) ?>

</div>
