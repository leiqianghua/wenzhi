<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    
<!--    下载图片-->
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'design_img','design_img', $date . '/design_img','',[],'',true); ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
