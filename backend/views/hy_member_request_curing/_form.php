<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">
    <?php if(\yii::$app->controller->action->id == "update" ){  ?>
<div class="return"><a href="javascript:history.go(-1);">返回</a></div>
    <?php  } ?>
    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
<?php $model->request_id = \common\components\GuestInfo::getParam("request_id");  ?>    
<?= $form->field($model, 'title')->textInput() ?>
<?= $form->field($model, 'notice_day_num')->textInput() ?>

<?php  if($model->next_notice_time){ $model->next_notice_time = date("Y-m-d", $model->next_notice_time); }   ?>    
<?php  if($model->next_notice_mine_time){ $model->next_notice_mine_time = date("Y-m-d", $model->next_notice_mine_time); }   ?>    
<?= $form->field($model, 'next_notice_time')->textInput(['class' => 'form-control J_date length_3']) ?>
    
<?= $form->field($model, 'is_notice_mine')->textInput()->hint("需要通知工作人员则填具体相隔天数,不通知则填"); ?>
    
<?= $form->field($model, 'next_notice_mine_time')->textInput(['class' => 'form-control J_date length_3']) ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    
    function GetDateStr(AddDayCount) { 
       var dd = new Date();
       dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
       var y = dd.getFullYear(); 
       var m = (dd.getMonth()+1)<10?"0"+(dd.getMonth()+1):(dd.getMonth()+1);//获取当前月份的日期，不足10补0
       var d = dd.getDate()<10?"0"+dd.getDate():dd.getDate();//获取当前几号，不足10补0
       return y+"-"+m+"-"+d; 
    }    
    
    
    $(function(){
        $("#hy_member_request_curingmodel-notice_day_num").blur(function(){
            var num = parseInt($(this).val());
            var date = GetDateStr(num);
            $("#hy_member_request_curingmodel-next_notice_time").val(date);
        })
        $("#hy_member_request_curingmodel-is_notice_mine").blur(function(){
            var num = parseInt($(this).val());
            if(num){
                var date = GetDateStr(num);
                $("#hy_member_request_curingmodel-next_notice_mine_time").val(date);                
            }else{
                $("#hy_member_request_curingmodel-next_notice_mine_time").val("");        
            }

        })
    })
</script>