<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>列表示例</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                &nbsp&nbsp&nbsp&nbsp
                
                <a class='layer_open_url is_reload' href="create?request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button class="btn btn-primary">添加记录</button></a>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            
            
'id',
'request_id',
'title',
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示  
'notice_day_num',
'is_need',
            ['attribute' => 'next_notice_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示 
'is_notice_mine',
            ['attribute' => 'next_notice_mine_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示   
            
            
            [
               'value' => function($model, $key, $index, $column){
                    $html = "<a href='update?id={$model['id']}&request_id={$_GET['request_id']}' class='' ><span class='glyphicon glyphicon-pencil'></span></a>";
                    $html .= "<a href='delete?id={$model['id']}&request_id={$_GET['request_id']}' class='ajax_request' ><span class='glyphicon glyphicon-trash'></span></a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

</div>
