<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>列表示例</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            
            
'id',
'request_id',
'curing_ids',
'add_time',
'is_docking',
            
            [
               'value' => function($model, $key, $index, $column){
                    $uids = common\models\hy_member_request_useModel::find()->alias("main")->leftJoin("yii_hy_designer_level l","l.uid=main.uid")->andWhere("l.studio_level_id in(5,9,13)")->where(['request_id' => $_GET['request_id']])->select(['uid'])->column();
                    if(!$uids){
                        return "请先拉入相关用户进项目组";
                    }
                    $values = common\models\hy_designer_levelModel::find()->where(['uid' => $uids])->asArray()->all();
                    $arr = [];
                    foreach($values as $key => $value){
                        $arr[$value['uid']][] = $value['studio_level_name'];
                    }
                    $result = [];
                    foreach($arr as $key => $value){
                        $result[$key] = common\models\hy_designerModel::getOne($key, "nickname") . "---" . implode(",", $value);
                    }
                    $sel = common\components\Form::getSelectData($result, "select_uid", ["data-id" => $model->id,'class' => "form-control select_duijie"], true, $model->work_uid, "请选择...");
                    return $sel;
               },
               'label' => '安排对接用户', 
               'format' => 'raw',                      
            ],  
            
            [
               'value' => function($model, $key, $index, $column){
                   $html = "";
                   if($model->work_uid && !$model->is_docking){
                       $html .= "<a class='ajax_request' href='update?is_docking=1&id={$model['id']}&request_id=".\common\components\GuestInfo::getParam("request_id")."'>变更为已处理</a>";
                   }
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>


</div>
<script>
    $(function(){
        $(".select_duijie").change(function(){
            var uid = $(this).val();
            var id = $(this).attr("data-id");
            var request_id = "<?= \common\components\GuestInfo::getParam("request_id") ?>";
            
            $.post("update",{work_uid:uid,id:id,request_id:request_id},function(data){
                if(data.status){
                    layer.msg(data.info);
                }else{
                    layer.msg(data.info);
                }
            },"json");
        })
    })
</script>