<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

<?php $model->uid = [];  ?>    
<?= $form->field($model, 'uid')->checkboxList($datas,['name' => "type_id"])->label("选择"); ?>    

    
    <input type="hidden" name="request_id" value="<?= \common\components\GuestInfo::getParam("request_id") ?>"/>
    <input type="hidden" name="type" value="<?= \common\components\GuestInfo::getParam("type") ?>"/>


    

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button('确定', ['class' => 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>