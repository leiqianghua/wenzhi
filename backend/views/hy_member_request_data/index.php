<?php

?>
<div class="admin-role-model-index">


    
<div id="w1" class="grid-view">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>类型</th>
                    <th>分类ID</th>
                    <th>分类名称</th>
                    <th>操作员工</th>
                    <th>完成时间</th>
                    <th>操作</th>
                </tr>
            </thead>


            <tbody>
                <?php  foreach($types as $type_id => $type_name){   ?>
                    <?php  
                        $nn = empty($datas[$type_id]) ? 1 : count($datas[$type_id]);
                    ?>
                
                
                    <?php  if(empty($datas[$type_id])){  ?>
                <tr>
                    <td >
                        <?= $type_name ?><a  class='layer_open_url is_reload' href="create?type=<?= $type_id?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button>增加</button></a>
                    </td>
                    <td colspan=6 >
                    </td>                    
                </tr>
                
                    <?php  }else if(count($datas[$type_id]) == 1){  ?>  
                        <tr>
                            <td>
                                <?= $type_name ?><a  class='layer_open_url is_reload' href="create?type=<?= $type_id?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button>增加</button></a>
                            </td>
                            <td><?= $datas[$type_id][0]['type_id'] ?></td>
                            <td><?= $datas[$type_id][0]['title'] ?></td>
                            <td><?= common\models\hy_designerModel::getOne($datas[$type_id][0]['sel_uid'], "nickname") ?></td>  
                            <td>
                                <?php if($datas[$type_id][0]['over_time']){  echo date("Y-m-d H:i:s", $datas[$type_id][0]['over_time']); }else{ echo "未完成"; } ?>
                            </td> 
                            <td>
                                <a  class='ajax_request' href="delete?id=<?= $datas[$type_id][0]['id']?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button>删除</button></a>
                                <a  href="/hy_member_request_data_record/index?data_id=<?= $datas[$type_id][0]['id']?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button>查看跟踪记录</button></a>
                                <?php if(!$datas[$type_id][0]['over_time']){  ?>
                                    <a  class='ajax_request' href="over?id=<?= $datas[$type_id][0]['id']?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button>确认完成</button></a>
                                <?php }  ?>
                            </td>                                                         
                        </tr>                
                    <?php  }else{  ?>
                        
                            <?php $num = 0; foreach($datas[$type_id] as $key => $value){ $num++;   ?>
                                <tr>
                                    <?php if($num == 1){ ?>
                                        <td rowspan="<?= count($datas[$type_id]) ?>">
                                        <?= $type_name ?> <a  class='layer_open_url is_reload' href="create?type=<?= $type_id?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button>增加</button></a>
                                        </td>
                                    <?php } ?>
                                        
                                    <td><?= $value['type_id'] ?></td>
                                    <td><?= $value['title'] ?></td>
                                    <td><?= common\models\hy_designerModel::getOne($value['sel_uid'], "nickname") ?></td>
                            <td>
                                <?php if($value['over_time']){  echo date("Y-m-d H:i:s", $value['over_time']); }else{ echo "未完成"; } ?>
                            </td>                                     
                                    <td>
                                        <a  class='ajax_request' href="delete?id=<?= $value['id']?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button>删除</button></a>
                                        <a  href="/hy_member_request_data_record/index?data_id=<?= $value['id']?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button>查看跟踪记录</button></a>
                                        <?php if(!$datas[$type_id][0]['over_time']){  ?>
                                        <a  class='ajax_request' href="over?id=<?= $value['id']?>&request_id=<?= \common\components\GuestInfo::getParam("request_id") ?>"><button><button>确认完成</button></button></a>
                                        <?php }  ?>                                        
                                    </td>                                                                        
                                </tr>

                                
                            <?php  }  ?>                        
                        
                    <?php  }  ?>  
                

                <?php  }  ?>                    
                                </tbody>
        </table>
    </div>    
    
    
    

</div>
