<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>列表示例</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            <div class="return"><a href="javascript:history.go(-1);">返回</a></div>
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            
            
'id',
'data_id',
            [
               'value' => function($model, $key, $index, $column){
                    
                    return "名：" . common\models\MemberModel::getOne($model['do_uid'], "username") . "</br>ID:".$model['do_uid'];
                    //return $html;
               },
               'label' => '操作用户', 
               'format' => 'raw',                      
            ],  
            [
               'value' => function($model, $key, $index, $column){
                    $arr = json_decode($model->images,true);
                    if($arr){
                        $str = implode(",", $arr);
                        $html = "<button class='layer_open_images' data='{$str}'>查看</button>";
                    }else{
                        return "占无图片";
                    }
                    
                    return $html;
               },
               'label' => '图片查看', 
               'format' => 'raw',                      
            ],  
'title',
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示

        ],
    ]); ?>


</div>
