<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>支付列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                <?= Html::button('<a style="color:white" href="/h_member_request_pay_record/create?request_id='.$_GET["request_id"].'">新增</a>', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            
            
'ordresn',
'title',
'money',
            [
                'attribute' => 'status',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getStatusHtml($model->status);
                }
            ],
            [
                'attribute' => 'is_underline_payment',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getis_underline_paymentHtml($model->is_underline_payment);
                },
                'label' => "是否线下支付",
            ],
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示
                    
            ['attribute' => 'update_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示
'pay_type',
'scookies',
'consume_type',
'trade_no',
'underline_payment_mark',
'relation_id',
            ['attribute' => 'underline_payment_img','format' => ['image']],//图片展示
                    
            
//            [
//               'value' => function($model, $key, $index, $column){
//                    $html = "";
//                    if($model['is_underline_payment'] && $model['status'] == 0){
//                        $html .= "<a href='".\yii::$app->controller->to(['update_post','ordresn' => $model->ordresn,"status" => 1])."'  >确认用户线下支付</a>";
//                        $html .= "</br>";
//                        $html .= "<a href='".\yii::$app->controller->to(['update_post','ordresn' => $model->ordresn,"status" => 0,"is_underline_payment" => 2])."'  >未收到款项</a>";
//                    }
//                    return $html;
//               },
//               'label' => '管理', 
//               'format' => 'raw',                      
//            ],  

        ],
    ]); ?>


</div>
