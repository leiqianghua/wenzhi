<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>员工列表</h1>

<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            
            
'uid',
'nickname',
            ['attribute' => 'img','format' => ['image',['style' => 'width:100%;height:100px']]],//图片展示
//'company',
//'work_year',
//            
//            [
//                'label' => "设计费",
//                'attribute' => 'label',
//                'value' => function($model){
//                    return "{$model['design_fee_min']}-{$model['design_fee_max']}";
//                }
//            ], 
//            [
//                'attribute' => 'design_fee_type',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getdesign_fee_typeHtml($model->design_fee_type);
//                }
//            ],  
//            [
//                'label' => "测量费",
//                'attribute' => 'label',
//                'value' => function($model){
//                    if(!$model['design_fee_min'] && !$model['design_fee_min']){
//                        return "免费";
//                    }
//                    return "{$model['celiang_fee_min']}-{$model['celiang_fee_max']}";
//                }
//            ],    
//            [
//                'attribute' => 'celiang_fee_type',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getceliang_fee_typeHtml($model->celiang_fee_type);
//                }
//            ],       
//            
//'city',
//'studio_name',
//            [
//                'attribute' => 'gold_type',
//                'value' => function($model){
//                    $d = get_class($model);
//                    return $d::getgold_typeHtml($model->gold_type);
//                }
//            ],
//            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示

            [
               'value' => function($model, $key, $index, $column){
                    $designer_level = $model->designer_level;
                    $html = [];
                    if($designer_level){
                        foreach($designer_level as $key => $value){
                            $html[] = $value['studio_level_name'];
                        }
                    }
                    return implode(",", $html);
               },
               'label' => '身份', 
               'format' => 'raw',                      
            ],  
                    

            [
               'value' => function($model, $key, $index, $column){
                    $html = "<a class='ajax_request' href='".\yii::$app->controller->to(['add_uid','uid' => $model->uid,'request_id' => common\components\GuestInfo::getParam("request_id")])."'  >增加</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
                       
                       
        ],
    ]); ?>


    <?= Html::button('添加',['class' => 'select_list btn','url' => $this->context->to(["add_uid",'request_id' => common\components\GuestInfo::getParam("request_id")])]) ?>    

</div>
