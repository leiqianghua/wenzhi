<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>工作室</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
            
            <?php echo \common\components\Form::getDataForSearch("start_add_time",empty($_GET['start_add_time']) ? '' : $_GET['start_add_time'],"添加时间范围","J_date length_2");  ?>
            <span style="margin-left:-10px;">到</span>
            <?php echo \common\components\Form::getDataForSearch("end_add_time",empty($_GET['end_add_time']) ? '' : $_GET['end_add_time'],"","J_date length_2");  ?>     
            
             
            <?= $form->field($model, 'founder_uid') ?>
            <?= $form->field($model, 'founder_name') ?>
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            
            
'id',
'name',
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd HH:mm:ss']],//时间展示
'founder_name',
'introduction',
            ['attribute' => 'img','format' => ['image',['style' => 'width:100%;height:100px']]],//图片展示
'city',

     

            [
               'value' => function($model, $key, $index, $column){
                    $html = "<a href='update?id={$model['id']}' class='layer_open_url is_reload' ><span class='glyphicon glyphicon-pencil'></span></a>";
                    $html .= "<a href='delete?id={$model['id']}' class='ajax_request' ><span class='glyphicon glyphicon-trash'></span></a>";
                    $html .= "</br>";
                    $html .= "<a href='/hy_designer/index?studio_id={$model['id']}' class='layer_open_url' >员工查看</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],        
                       
                       
        ],
    ]); ?>

    <?= Html::button('删除',['class' => 'select_list btn','url' => $this->context->to(["delete"])]) ?>

</div>
