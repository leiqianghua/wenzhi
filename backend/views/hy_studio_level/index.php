<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>工作室身份层级</h1>

 
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            
            
'id',
'title',
'level',
            [
                'attribute' => 'type',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::gettypeHtml($model->type);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',"template" => '{update}'],    
        ],
    ]); ?>


</div>
