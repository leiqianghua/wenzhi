<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
	        'options' => ['class' => 'form-horizontal J_ajaxForm'],
	        'fieldConfig' => [
	            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
	        ]        
        ]); ?>
     
    <?= $form->field($model, 'uid')->textInput(['name' => "controller"])->label("控制器名称") ?>
    <?= $form->field($model, 'uid')->textInput(['name' => "model"])->label("模型名称") ?>
    <?= $form->field($model, 'uid')->textInput(['name' => "has_data"])->label("底下是否还有文件夹的东西") ?>
    <div class="form-group text-center">
        <?= Html::submitButton('添加', ['class' => 'btn btn-success J_ajax_submit_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>