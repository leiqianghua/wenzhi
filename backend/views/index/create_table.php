<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
	        'options' => ['class' => 'form-horizontal J_ajaxForm'],
	        'fieldConfig' => [
	            'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
	        ]        
        ]); ?>
     
    <?= $form->field($model, 'uid')->textarea(['name' => "content",'style' => 'height:600px;'])->label("内容") ?>
    <div class="form-group text-center">
        <?= Html::submitButton('添加', ['class' => 'btn btn-success J_ajax_submit_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>