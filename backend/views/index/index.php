<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title> <?= Yii::$app->name; ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" type="text/css" href="../css/92.css">
    <link rel="stylesheet" type="text/css" href="//at.alicdn.com/t/font_1126819_k12s0dd6n6.css">
</head>
<body>
    <?php $this->beginBody() ?>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="brand">
                <a href="<?= \yii\helpers\Url::home();  ?>"><img src="../images/logo2.jpg" alt="<?= Yii::$app->name ?>" class="img-responsive logo"></a>
            </div>
            
            <div class="container-fluid">
                <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="float:left;">
                    <ul id="menu" class="nav navbar-nav navbar-left navbar_box">
                        <?php $num = 0; foreach($menu as $id => $value){ $num++; ?>
                            <?php if(!$value['status']){ continue; }  ?>
                            <li <?php if($num == 1){ echo 'class="active"'; }  ?> num = "<?= $id ?>"><a href="<?= \yii::$app->controller->to("/{$value['controller']}/{$value['action']}");  ?>" target="_blank"><?=  $value['title'] ?></a></li>
                        <?php }  ?>
                    </ul>
                </div>

                <div class="navbar-btn navbar-btn-right">
                    <a class="btn btn-success update-pro" href="<?= \yii::$app->controller->to('/index/logout') ?>" title="退出系统" target="_self"><span>退出</span></a>
                </div>
                <div id="navbar-menu">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" id="refresh_page">
                               <i class="iconfont icon-shuaxin"></i><span>刷新页面</span>
                            </a>
                        </li>
                        <li class="dropdown" style="display:none;">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="iconfont icon-xiaoxi"></i>
                                <span class="badge bg-danger">5</span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li><a href="#" class="notification-item"><span class="dot bg-warning"></span>你有一篇文章待审核</a></li>
                                <li><a href="#" class="notification-item"><span class="dot bg-danger"></span>你有一篇文章待审核</a></li>
                                <li><a href="#" class="notification-item"><span class="dot bg-success"></span>你有一篇文章待审核</a></li>
                                <li><a href="#" class="notification-item"><span class="dot bg-warning"></span>你有一篇文章待审核</a></li>
                                <li><a href="#" class="more">查看所有通知</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../images/avatar.jpg" class="img-circle" alt=""> <span><?= $user['username'] ?></span> <i class="icon-submenu iconfont icon-xia"></i></a>
                            <ul class="dropdown-menu">
                                <li><a target="RbtFrame" href="#"><i class="iconfont icon-gerenzhongxin"></i> <span>个人信息</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
    	</nav>

        <div id="sidebar-nav" class="sidebar left_side">
            <?php $nnum = 0; foreach($menu as $ids => $values){ $nnum++; ?>
                <?php if(!$values['status']){ continue; }  ?>
                <ul <?php if($nnum > 1){ echo 'style="display:none"'; } ?> num="<?= $ids ?>" class="nav nav-pills nav-stacked custom_nav">
                    <?php  foreach($values['data'] as $id => $value){  ?>
                        <?php if(!$value['status'] || count($value['data']) == 0){ continue; }  ?>
                        <li class="menu_list menu_active">
                            <a href="<?= \yii::$app->controller->to("/{$value['controller']}/{$value['action']}");  ?>">
                                <span><?= $value['title']  ?></span>
                            </a>
                            <ul class="sub_menu_list list-unstyled">
                                <?php  foreach($value['data'] as $k => $v){  ?>
                                    <?php if(!$v['status']){ continue; }  ?>
                                    <li>
                                        <?php  
                                            $url = "/{$v['controller']}/{$v['action']}";
//                                                                if($v['module']){
//                                                                    $v['controller'] = $v['module'] . "/" . $v['controller'];
//                                                                }
//                                                                if($v['params']){
//                                                                    $url = "/{$v['controller']}/{$v['action']}?{$v['params']}";
//                                                                }else{
//                                                                    $url = "/{$v['controller']}/{$v['action']}";
//                                                                }
                                        ?>
                                        <a data-id="<?= $k ?>" href="<?= \yii::$app->controller->to($url);  ?>"><i></i><?= $v['title']  ?></a>
                                    </li>                                                
                                <?php  } ?>
                            </ul>
                        </li>                                    
                    <?php  } ?>
                </ul>
            <?php  } ?>
        </div>


    	<div class="viewFramework">
    		<div class="main_content">
                <div class="tr tab">
                    <div class="td">
                        <div id="B_tabA" class="tabA"> 
                            <a href="javascript:void(0)" tabindex="-1" class="tabA_pre" id="J_prev" title="上一页">上一页</a> 
                            <a href="javascript:void(0)" tabindex="-1" class="tabA_next" id="J_next" title="下一页">下一页</a>
                            <div style="margin:0 25px;min-height:1px;">
                              <div style="position:relative;height:30px;width:100%;overflow:hidden;">
                                <ul id="b_history" style="white-space:nowrap;position:absolute;left:0;top:0;">
                                    <li class="current" data-id="0" tabindex="0"><span><a>后台首页</a></span></li>
                                    
<!--                                        <li tabindex="0" data-id="8admin" class=""><span><a>网站配置</a><a class="del" title="关闭此页">关闭</a></span></li>
                                    <li tabindex="0" data-id="14admin" class="current"><span><a>后台菜单管理</a><a class="del" title="关闭此页">关闭</a></span></li>
                                    <li tabindex="0" data-id="96admin" class=""><span><a>后台IP限制</a><a class="del" title="关闭此页">关闭</a></span></li>-->
                                </ul>
                              </div>
                            </div>
                        </div>	
                    </div>
                </div>			
                
                <div class="main_body">
                	<!-- <div class="back_left">《</div>                    	 -->
                    <iframe data-id="0" name="iframe_0" id="iframe_0" class="iframe is_show" frameborder="no" border="1" marginwidth="0" marginheight="0" src="<?=  \yii\helpers\Url::to(['index/ok'])  ?>" scrolling="auto" allowtransparency="yes" style="width:100%; height:100%"></iframe>   
                </div>
    		</div>
    	</div>
	</div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<script>
    $(function(){
    	//顶部菜单选择
        $("#menu li").click(function(e){
            $("#menu li").removeClass('active');
            $(this).addClass('active');
            var num = $(this).attr('num');
            $(".left_side > ul").hide();
            $(".left_side > ul[num='"+num+"']").show();
            return false;
        })
        
        //侧边栏菜单 选择
        $(".sub_menu_list a").click(function(){
            //增加选中
            $(".sub_menu_list li").removeClass('active');
            $(this).parent().addClass('active');


            //修改地址
            var url = $(this).attr('href');
            var id = $(this).attr('data-id');
            var length = $(".main_body > iframe[data-id='"+id+"']").length;
            //增加frame框
            if(length == 0){
            	var frame = '<iframe data-id="'+id+'" id="iframe_'+id+'" name="iframe_'+id+'" class="is_show iframe" frameborder="no" border="1" marginwidth="0" marginheight="0" src="'+url+'" scrolling="auto" allowtransparency="yes" style="width:100%; height:100%"></iframe> ';
            	$(".main_body").append(frame);
                selectLabel($(".main_body > iframe[data-id='"+id+"']").eq(0));
            }else{
                
                var obj = $(".main_body > iframe[data-id='"+id+"']");
                selectLabel(obj);          
                
                var show_id = $(".main_body iframe.is_show").data('id');
                var show_id = 'iframe_' + show_id;
                var OPERATION = $(window.frames[show_id].document).find("#OPERATION").val();//目前页面的url操作
                
                
                var url = obj.attr('src');
                if(url == OPERATION){
                    //console.log(url);
                    //selectLabel(obj);                 
                }else{
                    obj.attr('src',url);
                    //selectLabel($(".main_body > iframe[data-id='"+id+"']").eq(0));
                }
            }
            
            
            //增加历史标签
            var obj = $("#b_history > li[data-id='"+id+"']");
            if(obj.length == 0){
                var str = $(this).html();
                var name = str.replace("<i></i>","",str);
                var html = '<li  tabindex="0" data-id="'+id+'" class=""><span><a>'+name+'</a><a style="display:none" class="del" title="关闭此页"></a></span></li>';
                $("#b_history").append(html);
            }
            
            $("#b_history > li").removeClass('current');
            $("#b_history > li[data-id='"+id+"']").addClass('current');
            var P_num = $(this).parent().parent().parent().parent("ul").attr("num");
            $("#b_history > li[data-id='"+id+"']").attr('num',P_num);

            $("#iframe_src").attr('src',url);
            
            return false;

        })
        
        
        //删除标签，前一个变选择高亮
        $(document).on('click',"#b_history li .del",function(){
            var parent_obj = $(this).parent().parent();//自己关闭的这个的li
            var prev_obj = parent_obj.prev();//它的上一个li
            if(parent_obj.hasClass('current')){//本身自己选中
                selectLabel(prev_obj);             
            }
            var id = parent_obj.data('id');
            $(".main_body > iframe[data-id='"+id+"']").remove();       
            parent_obj.remove();
        })
        
        //点击上一页
        $(".tabA_pre").click(function(){
            var mine_obj = $("#b_history > li.current").eq(0);
            var prev_obj = mine_obj.prev();
            if(prev_obj.length){//存在上一页
                selectLabel(prev_obj);
            }
        })
        
        //点击下一页
        $(".tabA_next").click(function(){
            var mine_obj = $("#b_history > li.current").eq(0);
            var next_obj = mine_obj.next();
            if(next_obj.length){//存在下一页
                selectLabel(next_obj);
            }
        })
        
        //历史标签点击
        $(document).on('click','#b_history li span a:not(.del)',function(){
            var obj = $(this).parent().parent();
            //$(this).addClass('active');
            var num=$(this).parent().parent().attr('num');
            var d_id=$(this).parent().parent().attr('data-id');
            $(".left_side > ul[num='"+num+"']").children().find("a").each(function(){
                if($(this).attr("data-id")==d_id){
                	$(this).parent().addClass('active');
                }else{
                	$(this).parent().removeClass('active');
                }
            });
            $("#menu >li").each(function(){
                if($(this).attr('num')==num){
                    $(this).addClass("active");
                }else{
                	$(this).removeClass("active");
                }   
            });
            //console.log(a_data[0]);
            //if(a_data==d_id).addClass("active");
            $(".left_side > ul").hide();
            $(".left_side > ul[num='"+num+"']").show();
          
            selectLabel(obj);
        })
        
        
        /*
        * mine_obj 你要选择的这个obj
         */
        function selectLabel(mine_obj)
        {
            var id = mine_obj.data('id');
            
            $("#b_history li").removeClass('current');
            mine_obj.addClass('current');
            
            $(".main_body iframe").hide();
            $(".main_body iframe").removeClass('is_show');
            $(".main_body > iframe[data-id='"+id+"']").show();      
            $(".main_body > iframe[data-id='"+id+"']").addClass('is_show');   
            //$(this).addClass('active');
            var num=mine_obj.attr('num');
            var d_id=mine_obj.attr('data-id');
           /*  $(".left_side > ul[num='"+num+"']").children().find("a").each(function(){
                if(mine_obj.attr("data-id")==d_id){
                	$(this).parent().addClass('active');
                }else{
                	$(this).parent().removeClass('active');
                }
            });
            $("#menu >li").each(function(){
                if($(this).attr('num')==num){
                    $(this).addClass("active");
                }else{
                	$(this).removeClass("active");
                }   
            });         */
        };
        //左边全屏显示
	      // $(".back_left").bind("click",function(){
	      // 	$(".main_content").toggleClass("full");
	      // 	$(".main_content").find(".back_left").html("《");
	      // 	$(".full").find(".back_left").html("》");
	      // });
        //向上全屏显示
        // $(".back_up").bind("click",function(){
        // 	$(".viewFramework").toggleClass("full_up");
        // 	$(this).toggleClass("full_down");
        //  	$(this).html("︽").css("line-height","11px");
        //  	$(".navbar_define").toggleClass("z9");
        // 	$(".full_up").siblings(".navbar").find(".back_up").html("︾").css("line-height","20px");;
        // });

        // $(".navbar-toggle").click(function(){
        // 	if($("#menu").is(":visible")){
        // 		$("#menu").slideUp()
        // 	}else{
        // 		$("#menu").slideDown()
        // 	}
        // })
        
    })
</script>
<script type="text/javascript">
	$(function(){
	  // $("#full_btn").bind("click",function(){
	  // 	if($(".left_side").is(":visible")){
	  // 		$(".left_side").hide()
	  // 	}else{
	  // 		$(".left_side").show()
	  // 	}
	  // });
	// $(".navbar-toggle").bind("click",function(){
	//   	if($(".left_side").is(":visible")==false){
	//   		$(".left_side").show();
	//   	}
	//   });
	  
	// $(".left_side li a,#full_btn").click(function(){
	// 	$("#navbar").css("height","0px")
	// });
        
        
        $("body").bind("keydown",function(event){
             if (event.keyCode == 116) {
                  event.preventDefault(); //阻止默认刷新
                var src = $(".is_show").attr('src');
                  $(".is_show").attr("src",src);
            }
        })
        
        $(document).on("mouseover",".tabA li",function(){
            $(this).find(".del").show();
        })
        
        $(document).on("mouseout",".tabA li",function(){
            $(this).find(".del").hide();
        })
        
//        $(".tabA li").hover(function(){
//            alert('ab');
//            $(this).find(".del").show();
//        },function(){
//            alert('cd');
//            $(this).find(".del").hide();
//        })  
  })
</script>













