<style>
    body{background-color:#F0F0F0}
    
    .form-inline .form-group{
        margin-right: 15px;
    }
/*
===================
主选项卡
===================
*/
.nav_label{
/*    margin-bottom:10px;*/
    height:20px;
}
.nav_label ul{
    height:20px;
    float:left;
}
.nav_label li,
.nav_label .return{
    float:left;
}
.nav_label li a,
.nav_label .return a{
    float:left;
    line-height:32px;
    color:#666;
    padding:0 15px;
    text-align:center;
}
.nav_label li a:hover{
    border-bottom:2px solid #aaa;
    color:#333;
    text-decoration:none;
}

.nav_label li.current a{
    border-bottom:2px solid #266aae;
    font-weight:700;
    color:#266aae;
}
.nav_label .return a{
    background:url(../images/admin/content/return.png) 2px center no-repeat;
    color:#990000;
    padding:0 20px;
}
/*
===================
二级选项卡
===================
*/
.nav_label_minor{
    margin-top:-5px;
    position:relative;
    margin-bottom:15px;
    height:24px;
    overflow:hidden;
}
.nav_label_minor li{
    float:left;
}
.nav_label_minor li a{
    float:left;
    line-height:24px;
    color:#666;
    padding:0 15px;
    text-align:center;
}
.nav_label_minor li a:hover{
    text-decoration:none;
    background:#e4e4e4;
}
.nav_label_minor li.current a{
    background:#ddd;
    color:#333;
    font-weight:700;
}

li{
    list-style:none;
}
.nav_label{
    background: url(../images/admin/content/hr.png) 0 bottom repeat-x;
}

    .nav_label .cc{
        padding-left:0px;
    }
   
</style>

<?php 
    $controller = \yii::$app->controller->id;
    $action = \yii::$app->controller->action->id;
    $list = \backend\models\AdminMenuModel::getSameLabel();
    foreach($list as $key => $value){
        if(empty($value['status'])){ unset($list[$key]);  }
    }
?>
<?php if(!empty($list) && count($list) > 1){  ?>
    <div class="nav_label">
        <?php
            $menu_return = \yii::$app->cache->get('menu_return');
            //返回的上一个页面
            if(!empty($menu_return)){
                    echo '<div class="return"><a href="'.$menu_return['url'].'">返回'.$menu_return['name'].'</a></div>';
                    \yii::$app->cache->delete('menu_return');
            }
        ?>    
        <ul class="cc">
            <?php  foreach($list as $key => $value){  ?>
                <?php  if(empty($value['status'])){ continue; }  ?>
                <?php
                    if(in_array($value['controller'], ['adlistalias','countinputbase'])&&$value['action']=='create'){
                            $url = "/{$value['controller']}/{$value['action']}?{$_SERVER['QUERY_STRING']}";
                    }
                    elseif($value['params']){
                        $url = "/{$value['controller']}/{$value['action']}?{$value['params']}";
                    }else{
                        $url = "/{$value['controller']}/{$value['action']}";
                    }
                ?>    
                <?php if(!empty($value['module'])){ $url = "/{$value['module']}{$url}"; }  ?>
                <li class="<?php if($value['controller'] == $controller && $value['action'] == $action){ echo 'current'; }  ?>"><a href="<?= $this->context->to($url) ?>"><?=  $value['title'] ?></a></li>
            <?php  } ?>
        </ul>
    </div>

<?php } ?>

<input id="OPERATION" type="hidden" value="" name="OPERATION"/>

<script>
    //本页面的模块/控制器/操作
    var OPERATION = "<?= $_SERVER['REQUEST_URI']  ?>";
    $("#OPERATION").val(OPERATION);
    //$(window.parent.document).find('.main_body iframe.is_show').eq(0).attr('src',OPERATION);
    
</script>