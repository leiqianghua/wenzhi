<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    <?= $form->field($model, 'uid')->textInput(['disabled' => 'disabled']) ?>
    <?= $form->field($model, 'username')->textInput(['disabled' => 'disabled']) ?>
    <?= $form->field($model, 'iphone')->textInput(['disabled' => 'disabled']) ?>
    <?= $form->field($model, 'exten_code')->textInput(['disabled' => 'disabled']) ?>
    <?= $form->field($model, 'parent_id')->hint('-1表示不确定级别,0表示无上级') ?>
    <?= $form->field($model, 'label')->dropDownList(\common\models\MemberModel::getLabelHtmlAll()) ?>
    <?= $form->field($model, 'score')->hint('用户积分') ?>
    <?= $form->field($model, 'kaidang_bili')->hint('开单提成百分比比例') ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>