<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>会员列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'iphone') ?>
            <?= $form->field($model, 'address') ?>
            </br>       
            </br>     
<div class="form-group field-membermodel-area">
    <label class="control-label" for="membermodel-username">地区来源</label>
    <?php  echo \common\models\ElseFreightModel::getDataForArea($model->province_id,$model->city_id);  ?>
</div>            
            

<script>
    $(document).on('change','.province_id',function(){
        var obj = $(this);
        if(obj.val() == 0){
            obj.next("select").remove();
        }else{
            var data_obj = {};
            data_obj.id = obj.val();
            $.post('/freight/select_city',data_obj,function(data){
                if(data.status){
                    obj.next("select").remove();
                    obj.after(data.content);
                }
            },'json');
        }
    })
</script>            
            
            
            </br>     
            </br>       
            
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml(), ['prompt' => '全部']) ?>
            <?= $form->field($model, 'type')->dropDownList($object::getTypeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'parent_id') ?>
            
            <?= $form->field($model, 'label')->dropDownList($object::getLabelHtmlAll(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'exten_code')  ?>
            
            <label><input type="checkbox" id="shopmodel-is_show" name="is_gongyin" value="1" <?php  if(!empty($_GET['is_gongyin'])){ echo "checked"; } ?> >是否供应商</label>
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                
                <a style="color:white;text-decoration:none" href="index_excel?<?= $_SERVER['QUERY_STRING'] ?>">
                    <?= Html::buttonInput('导出', ['class' => 'btn btn-primary excel_download']) ?>
                </a>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
</div>    

 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'uid',//直接展示
            //'username',//直接展示
            
            [
                'label' => '会员名称',
                'value' => function($data){
                    $html = "<a href='view?uid={$data['uid']}' class='' >{$data['username']}</a>";
                    return $html;
                },
                'format' => 'raw',   
            ],  
            'iphone',//直接展示
            'address_info',//直接展示
            'address',//直接展示
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示
            [
                'attribute' => 'status',
                'value' => function($data){
                    $obj_name = get_class($data);
                    return $obj_name::getStatusHtml($data['status']);
                }
            ],  
            [
                'label' => '真实姓名',
                'value' => function($data){
                    return common\models\MemberExtendModel::getOne($data['uid'], "real_username");
                }
            ],  
             [
                 'label' => '上级',
                 'attribute' => 'parent_name',
             ],
            'exten_code',//直接展示
             [
                 'label' => '级别',
                 'value' => function($data){
                     $obj_name = get_class($data);
                     return $obj_name::getLabelHtmlAll($data['label']);
                 }
             ],
             [
                 'label' => '下属会员数量',
                 'value' => function($data){
                     return \common\models\MemberModel::find()->where(['parent_id' => $data['uid']])->count();
                 }
             ],
             [
                 'label' => '花园身份',
                 'value' => function($data){
                     $user = common\models\hy_designerModel::getOne($data['uid']);
                     if($user){
                         return "工作室：{$user['studio_name']}";
                     }else{
                         return "";
                     }
                 }
             ],
            [
               'value' => function($model, $key, $index, $column){
                    $content = "<form class='layermmain'>"
                            . "<label>请输入你想要替换的会员ID</label><input type='text' name='t_uid' value=''/>"
                            . "<input type='hidden' name='uid' value='{$model->uid}'/>"
                            . "<div>注意：顶替后此用户原本底下的会员会变成他的上上级的</div>"
                            . "</form>";
                    $html = "<a href='update?id={$model['uid']}' class='' >修改</a>";
                    $html .= "</br>";
                    $html .= "<a href='delete?id={$model['uid']}' class='' >删除</a>";
                    $html .= "</br>";
                    $html .= "<a content=\"{$content}\" class='ajax_layer' href='tihuan' class='' >顶替用户</a>";
                    $html .= "</br>";
                    $html .= "<a href='/order/index?uid={$model['uid']}' class='' >查看订单</a>";
                    $html .= "</br>";
                    $html .= "<a href='view?uid={$model['uid']}' class='' >查看详情</a>";
                    
                    
                    if(!common\models\hy_designerModel::getOne($model->uid)){
                        $html .= "</br>";
                        $html .= "<a href='/hy_designer/create_or_update?uid={$model['uid']}' class='layer_open_url is_reload' >设置为员工</a>";                        
                    }else{
                        $html .= "</br>";
                        $html .= "<a href='/member/ajax_ok?uid={$model['uid']}' class='layer_open_url' >查看员工账号</a>";                          
                    }

                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>


</div>
