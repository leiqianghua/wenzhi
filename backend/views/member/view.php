<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
$obj = get_class($model);



?>
<div class="admin-role-model-view">

    <h1>会员详情</h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uid',//直接展示 
            'username',//直接展示 
            'iphone',//直接展示     
            'money',//直接展示             
            'score',//直接展示       
        ],
    ]) ?>
    
    交易记录
    <?php  
        $query = common\models\OrderGoodsModel::find();
        $query->andWhere(['uid' => $model['uid']]);
        $query->limit(10000);
        $query->orderBy("id desc");
        //echo $query->createCommand()->getRawSql();exit;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);    
    ?>
    <?= \kartik\grid\GridView::widget([
        'export' => false,
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
                        
            [
                 'label' => '图',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    if($data['type'] == 1){
                        $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods/index?gid={$data->goods_id}";
                    }else{
                        $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods_package/detail?gid={$data->goods_id}";
                    }
                    
                    $html = "<a target='_blank' href='{$url}'><img width='100px' src='{$data['img']}'/></a>";
                    return $html;
                 },
             ],  
                         
            [
                'class'=>'kartik\grid\ExpandRowColumn',
                'detailAnimationDuration' => 200,
                'width'=>'70px',
                'expandIcon' => '<a class="glyphicon glyphicon-expand " data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="全部展开"></a>',
                'collapseIcon' => '<a class="glyphicon glyphicon-collapse-down" data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="收合"></a>',
                //'header' => '<a class="show_detail" data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="全部展开"></a>',
                'value'=>function ($model, $key, $index, $column) {
                    return \kartik\grid\GridView::ROW_COLLAPSED;
                },
                'detail'=>function ($model, $key, $index, $column) {
                    return Yii::$app->controller->renderPartial('/order/_good_detail', ['model'=>$model]);
                },
                'headerOptions'=>['class'=>'kartik-sheet-style'], 
                'expandOneOnly'=>true,
                'detailRowCssClass' => 'success',
            ],  
                        
            [
                'attribute' => 'type',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getTypeHtml($model['type']);
                },  
                'format' => 'raw',         
                
            ],    
                        
                        
            [
                'attribute' => 'fujia',
                'label' => "附加的",
                'value' => function($model){
                    $html = "";
                    if($model->fujia){
                        $fujia = json_decode($model->fujia, true);
                        foreach($fujia as $v){
                            $model = common\models\GoodsModel::findOne($v);
                            if($model){
                                $html .= "{$model['name']}：费用：{$model['price']}";
                                $html .= "</br>";
                            }
                            
                        }
                    }
                    return $html;
                },  
                'format' => 'raw',         
                
            ],                      
            
            'order_id',//直接展示
            'goods_id',//直接展示

            'name',//直接展示
  
                        
            'uid',//直接展示
            'username',//直接展示
            'num',//直接展示
                         
                         
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示    
        ],
    ]); ?>
    
    下属会员
    <?php  
        $query = common\models\MemberModel::find();
        $query->andWhere(['parent_id' => $model['uid']]);
        $query->limit(10000);
        $query->orderBy("uid desc");
        //echo $query->createCommand()->getRawSql();exit;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);    
    ?> 
    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'uid',//直接展示
            //'username',//直接展示
            
            [
                'label' => '会员名称',
                'value' => function($data){
                    $html = "<a href='view?uid={$data['uid']}' class='' >{$data['username']}</a>";
                    return $html;
                },
                'format' => 'raw',   
            ],  
            'iphone',//直接展示
            'address_info',//直接展示
            'address',//直接展示
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示
            [
                'attribute' => 'status',
                'value' => function($data){
                    $obj_name = get_class($data);
                    return $obj_name::getStatusHtml($data['status']);
                }
            ],  
            [
                'label' => '真实姓名',
                'value' => function($data){
                    return common\models\MemberExtendModel::getOne($data['uid'], "real_username");
                }
            ],  
             [
                 'label' => '上级',
                 'attribute' => 'parent_name',
             ],
            'exten_code',//直接展示
             [
                 'label' => '级别',
                 'value' => function($data){
                     $obj_name = get_class($data);
                     return $obj_name::getLabelHtmlAll($data['label']);
                 }
             ],
             [
                 'label' => '下属会员数量',
                 'value' => function($data){
                     return \common\models\MemberModel::find()->where(['parent_id' => $data['uid']])->count();
                 }
             ],
            [
               'value' => function($model, $key, $index, $column){
                    $content = "<form class='layermmain'>"
                            . "<label>请输入你想要替换的会员ID</label><input type='text' name='t_uid' value=''/>"
                            . "<input type='hidden' name='uid' value='{$model->uid}'/>"
                            . "<div>注意：顶替后此用户原本底下的会员会变成他的上上级的</div>"
                            . "</form>";
                    $html = "<a href='update?id={$model['uid']}' class='' >修改</a>";
                    $html .= "</br>";
                    $html .= "<a href='delete?id={$model['uid']}' class='' >删除</a>";
                    $html .= "</br>";
                    $html .= "<a content=\"{$content}\" class='ajax_layer' href='tihuan' class='' >顶替用户</a>";
                    $html .= "</br>";
                    $html .= "<a href='/order/index?uid={$model['uid']}' class='' >查看订单</a>";
                    $html .= "</br>";
                    $html .= "<a href='view?uid={$model['uid']}' class='' >查看详情</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>    
</div>
