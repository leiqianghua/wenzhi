<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>申请列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'label')->dropDownList(\common\models\MemberModel::getLabelHtmlAll(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml(), ['prompt' => '全部']) ?>
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    

 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'uid',//直接展示
            'username',//直接展示
            'reason',//直接展示
            'rejects_case',//直接展示
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示
            
            [
                'attribute' => 'status',
                'value' => function($data){
                    $obj_name = get_class($data);
                    return $obj_name::getStatusHtml($data['status']);
                }
            ],  
            [
                'attribute' => 'label',
                'value' => function($data){
                    return \common\models\MemberModel::getLabelHtmlAll($data['label']);
                }
            ],                     
            [
               'value' => function($model, $key, $index, $column){
                    
                    $html = "";
                    if($model['status'] == 0){
                       $html .= "<a class='ajax_request' href='/member_apply/update?id={$model['id']}&status=1' class='' >同意</a>";
                       $html .= "</br>";
                       
                        $content = "<form class='layermmain'>"
                            . "<label>请填写拒绝理由</label><input type='text' name='rejects_case' value=''/>"
                            . "</form>";                       
                       
                       $html .= "<a class='ajax_layer' content=\"{$content}\" href='/member_apply/update?id={$model['id']}&status=2' class='' >拒绝</a>";         
                    }
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

</div>
