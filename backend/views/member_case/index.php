<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">


<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'title') ?>
            <?= $form->field($model, 'address_info') ?>
            <?= $form->field($model, 'address') ?>
            <?= $form->field($model, 'winxin') ?>
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'type')->dropDownList($object::getTypeHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'scene')->dropDownList($object::getSceneHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'styles')->dropDownList($object::getStylesHtml(),['prompt' => '全部']) ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
  
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            'address_info',//直接展示 
            'address',//直接展示 
            'iphone',//直接展示 
            'title',//直接展示 
            'winxin',//直接展示 
            'money',//直接展示 
            'realname',//直接展示 
            'describe',//直接展示 
            'uid',//直接展示 
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示
            [
                'attribute' => 'status',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getStatusHtml($model['status']);
                },   
            ],            
            'score',//直接展示 
            'ok_du',//直接展示 
            [
                'attribute' => 'type',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getTypeHtml($model['type']);
                },   
            ],            
            [
                'attribute' => 'scene',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getsceneHtml($model['scene']);
                },   
            ],            
            [
                'attribute' => 'styles',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getStylesHtml($model['styles']);
                },   
            ],
            [
               'value' => function($model, $key, $index, $column){
                    $html = '';
                    if($model->status == 0){
                            $content = "<form class='layermmain'>"
                          . "<label>请填写满意度分数(总分100)</label><input type='text' name='ok_du' value=''/>"
                          . "</form>";                  
                        $html .= "<a content=\"{$content}\" href='ok?id={$model->id}' class='ajax_layer' >填写满意度</a>";
                        $html .= "</br>";
                    }
                    
                    $html .= "<a href='view?id={$model->id}'  >查看详情</a>";
                    $html .= "</br><a href='delete?id={$model->id}'  >删除</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],                          
        ],
    ]); ?>

</div>
