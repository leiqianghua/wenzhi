<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
$obj = get_class($model);

$imgs = "";
$imgs_arr = json_decode($model->imgs,true);
if($imgs_arr){
    foreach($imgs_arr as $key => $value){
        $imgs .= "<img src='{$value}' style='width:100px;' />";
        $imgs .= "</br>";
    }   
}


?>
<div class="admin-role-model-view">

    <h1>详情</h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'address_info',//直接展示 
            'address',//直接展示 
            'iphone',//直接展示 
            'username',//直接展示 
            'winxin',//直接展示 
            'money',//直接展示 
            'realname',//直接展示 
            'describe',//直接展示 
            'uid',//直接展示 
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示
            [
                'attribute' => 'status',
                'value' => $obj::getStatusHtml($model['status']),
            ],            
            'score',//直接展示 
            'ok_du',//直接展示 
            [
                'attribute' => 'type',
                'value' => $obj::getTypeHtml($model['type']),   
            ],            
            [
                'attribute' => 'scene',
                'value' => $obj::getsceneHtml($model['scene']),   
            ],            
            [
                'attribute' => 'styles',
                'value' => $obj::getStylesHtml($model['styles']),   
            ],  
            [
                 'attribute' => 'imgs',
                 'format' => 'raw',       
                 'value' => $imgs,
            ],              
        ],
    ]) ?>

</div>
