<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>申请列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml(),['prompt' => '全部']) ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    

 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'id',//直接展示
            'uid',//直接展示
            [
                'label' => '用户名',
                'value' => function($model){
                    return common\models\MemberModel::getOne($model['uid'], 'username');
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    return $model->getStatusHtml($model['status']);
                }
            ],                    
            'money',//直接展示     
            'account',//直接展示     
            'type',//直接展示     
            'name',//直接展示     
            'remark',//直接展示
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示            
            [
               'value' => function($model, $key, $index, $column){
                    $html = '';
                    if($model['status'] == 0){
                        $html = "<a class='ajax_request' href='ok?id={$model['id']}'  >确认资金已发放</a>";
                        $html .= "</br>";
                        $html .= "<a class='ajax_request' href='channel?id={$model['id']}'  >拒绝提现</a>";                        
                    }
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

    <?= ''//Html::button('我要删除',['class' => 'select_list btn','url' => '/ab/sd']) ?>

</div>
