<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">


<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'is_overlay')->dropDownList($object::getIs_overlayHtml(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'money') ?>
            <?= $form->field($model, 'is_use')->dropDownList($object::getIs_useHtml(),['prompt' => '全部']) ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
  
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            'uid',//直接展示 
            [
                'attribute' => 'is_overlay',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getIs_overlayHtml($model['is_overlay']);
                },   
            ],            
            'money',//直接展示
            ['attribute' => 'invalid_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示
            'must_money',//直接展示
            'is_tejia',//直接展示
            [
                'attribute' => 'is_use',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getIs_useHtml($model['is_use']);
                },   
            ],            
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示
        ],
    ]); ?>

</div>
