<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">


<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'type')->dropDownList($object::getTypeHtml(),['prompt' => '全部']) ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                
                <a style="color:white;text-decoration:none" href="index_excel?title=金额使用记录&<?= $_SERVER['QUERY_STRING'] ?>">
                    <?= Html::buttonInput('导出', ['class' => 'btn btn-primary excel_download']) ?>
                </a>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    

    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            'uid',//直接展示
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示
            [
                'attribute' => 'type',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getTypeHtml($model['type']);
                },   
            ],            
            'money',//直接展示
            'balance',//直接展示
            'relation_data',//直接展示 
        ],
    ]); ?>

</div>
