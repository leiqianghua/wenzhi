<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */

$this->title = '修改菜单';
?>
<div class="admin-role-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php include '_form.php';  ?>

</div>
