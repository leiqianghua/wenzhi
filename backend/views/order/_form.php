<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'price') ?>
    <?= $form->field($model, 'discount_1')->hint('最多一个小数,请填写百分比') ?>
    <?= $form->field($model, 'discount_2')->hint('同上') ?>
    <?= $form->field($model, 'discount_3')->hint('同上') ?>
    <?= $form->field($model, 'sorting')->hint('排序越大越靠前') ?>
    <?= $form->field($model, 'cid')->dropDownList(\common\models\GoodsClassModel::getMenuKeyValue(false)) ?>
    <?= $form->field($model, 'status')->dropDownList(\common\models\GoodsModel::getStatusHtml()) ?>     
    
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'img','img', $date . '/goods'); ?>
    
    <?= Form::getFileContent($form,$model,'img_list','img_list', $date . '/goods','',[],'',true); ?>
    
    
    <?= Form::editor_element_detail($model, 'details');  ?>
    
    
    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>