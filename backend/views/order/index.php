<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>订单管理</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'order_id') ?>
            <?= $form->field($model, 'order_sn') ?>
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'order_state')->dropDownList($object::getOrder_stateHtml(),['prompt' => '全部']) ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
                
                
                <a style="color:white;text-decoration:none" href="index_excel?title=订单&<?= $_SERVER['QUERY_STRING'] ?>">
                    <?= Html::buttonInput('导出', ['class' => 'btn btn-primary excel_download']) ?>
                </a>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        
        <div style="margin: 15px;" class="kuaijie">
            <?= Html::Button('<a href="'.$this->context->to(["index","order_state" => '']).'">全部</a>', ['class' => 'btn']) ?>
            <?= Html::Button('<a href="'.$this->context->to(["index","order_state" => 0]).'">未付款</a>', ['class' => 'btn']) ?>
            <?= Html::Button('<a href="'.$this->context->to(["index","order_state" => 1]).'">已付款</a>', ['class' => 'btn']) ?>
            <?= Html::Button('<a href="'.$this->context->to(["index","order_state" => 2]).'">已发货</a>', ['class' => 'btn']) ?>
            <?= Html::Button('<a href="'.$this->context->to(["index","order_state" => 3]).'">已完成</a>', ['class' => 'btn']) ?>
            <?= Html::Button('<a href="'.$this->context->to(["index","order_state" => 4]).'">已关闭</a>', ['class' => 'btn']) ?>
        </div>   
    </div>    

 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'order_id',//直接展示
            'order_sn',//直接展示
            'uid',//直接展示
            'username',//直接展示
            'order_amount',//直接展示
            'fujia',//附加的费用
            'red_ids',//使用的优惠券ID
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示    
            ['attribute' => 'payment_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示   
            ['attribute' => 'finish_time','format' => ['date', 'Y-M-d H:i:s']],//时间展示  
            'shipping_code',//直接展示        
            [
               'value' => function($model, $key, $index, $column){
                    $html = [];
                    if(is_array($model->order_goods)){
                        foreach($model->order_goods as $key => $value){
                            $str = "商品名称：{$value['name']},购买数量:{$value['num']}";
                            $html[] = $str;
                        }
                        $return = implode("</br>", $html);
                        return $return;
                    }else{
                        return "";
                    }
               },
               'label' => '购买商品', 
               'format' => 'raw',                      
            ],  
            [
                'label' => '收件人地址',
                'value' => function($model){
                    $obj = get_class($model);
                    return "地址:{$model->address}</br>电话:{$model->phone}</br>姓名:{$model->user_truename}";
                },
                        'format' => 'raw',         
                
            ],           
            [
                'attribute' => 'order_state',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getOrder_stateHtml($model['order_state']);
                },   
                
            ],           
            [
               'value' => function($model, $key, $index, $column){
                    $html = '';
                    if($model->order_state == 1){
                        $v = common\models\ExpressModel::getBaseKeyValue('e_code', 'e_name');
                        $select = "<select name='e_code'>";
                        foreach($v as $key => $value){
                            $select .= "<option value='{$key}'>{$value}</option>";
                        }
                        $select .= "</select>"; 
                        
                        
                        $content = "<form class='layermmain'>"
                            . "<label>请填写单号</label><input type='text' name='shipping_code' value=''/>"
                            . "<label>请选择物流公司</label>"
                            . $select
                            . "</form>";
                            
                        $html .= "<a content=\"{$content}\" href='update?order_id={$model['order_id']}' class='ajax_layer' >发货</a>";
                        $html .= "</br>";                        
                    }
                    
                    $html .= "<a href='indexgoods?order_id={$model['order_id']}' class='' >订单商品列表</a>";
                    $html .= "</br>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

    <?= ''//Html::button('我要删除',['class' => 'select_list btn','url' => '/ab/sd']) ?>

</div>
