<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>订单商品</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'indexgoods',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'order_id') ?>
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'goods_id') ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    

 
    
<!--    列表-->
    <?= \kartik\grid\GridView::widget([
        'export' => false,
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
                        
            [
                 'label' => '图',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    if($data['type'] == 1){
                        $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods/index?gid={$data->goods_id}";
                    }else{
                        $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods_package/detail?gid={$data->goods_id}";
                    }
                    
                    $html = "<a target='_blank' href='{$url}'><img width='100px' src='{$data['img']}'/></a>";
                    return $html;
                 },
             ],  
                         
            [
                'class'=>'kartik\grid\ExpandRowColumn',
                'detailAnimationDuration' => 200,
                'width'=>'70px',
                'expandIcon' => '<a class="glyphicon glyphicon-expand " data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="全部展开"></a>',
                'collapseIcon' => '<a class="glyphicon glyphicon-collapse-down" data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="收合"></a>',
                //'header' => '<a class="show_detail" data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="全部展开"></a>',
                'value'=>function ($model, $key, $index, $column) {
                    return \kartik\grid\GridView::ROW_COLLAPSED;
                },
                'detail'=>function ($model, $key, $index, $column) {
                    return Yii::$app->controller->renderPartial('_good_detail', ['model'=>$model]);
                },
                'headerOptions'=>['class'=>'kartik-sheet-style'], 
                'expandOneOnly'=>true,
                'detailRowCssClass' => 'success',
            ],  
                        
            [
                'attribute' => 'type',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getTypeHtml($model['type']);
                },  
                'format' => 'raw',         
                
            ],    
                        
                        
            [
                'attribute' => 'fujia',
                'label' => "附加的",
                'value' => function($model){
                    $html = "";
                    if($model->fujia){
                        $fujia = json_decode($model->fujia, true);
                        foreach($fujia as $v){
                            $model = common\models\GoodsModel::findOne($v);
                            if($model){
                                $html .= "{$model['name']}：费用：{$model['price']}";
                                $html .= "</br>";
                            }
                            
                        }
                    }
                    return $html;
                },  
                'format' => 'raw',         
                
            ],                      
            
            'order_id',//直接展示
            'goods_id',//直接展示

            'name',//直接展示
  
                        
            'uid',//直接展示
            'username',//直接展示
            'num',//直接展示
                         
                         
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示    
        ],
    ]); ?>

    <?= ''//Html::button('我要删除',['class' => 'select_list btn','url' => '/ab/sd']) ?>

</div>
