<h1>报价器</h1>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>排序</td>
            <td>ID</td>
            <td>名称</td>
            <td>状态(1显示,0不显示)</td>
            <td>管理操作</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($menus as $key => $value){  ?>
            <?php 
                $layer = $value['Layer'];
                $layer_html = '';
                for($i = 0;$i < $layer;$i++){
                    $layer_html .= "└─ ";
                }
            ?>
            <tr data-id="<?= $value['id'] ?>">
                <td data-id="<?= $value['id'] ?>"><input type="text" value="<?= $value['sorting'] ?>" name="sorting[<?= $value['id'] ?>]"/></td>
                <td><?= $value['id'] ?></td>            
                <td><?= $layer_html . $value['title'] ?></td>            
                <td><?= $value['status'] ?></td>            
                <td>
                    <a href="<?= $this->context->to(['create','parent_id' => $value['id']]) ?>">添加子分类</a> | 
                    <a href="<?= $this->context->to(['update','id' => $value['id']]) ?>">修改</a> | 
                    <a href="<?= $this->context->to(['/project_goods_package/create','type' => $value['id']]) ?>">添加套装</a> | 
                    <a class="J_ajax_del" href="<?= $this->context->to(['delete','id' => $value['id']]) ?>">删除</a>                     
                </td>            
            </tr>
        <?php }  ?>
    </tbody>
</table>