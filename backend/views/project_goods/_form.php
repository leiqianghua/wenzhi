<?php

error_reporting(0);

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
\backend\assets\JcropAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    
    
    
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'price') ?>
    <?= $form->field($model, 'discount_0')->hint('出厂') ?>
    <?= $form->field($model, 'weight')->hint('单位') ?>
    <?= $form->field($model, 'cid')->dropDownList(\common\models\Project_classModel::getMenuKeyValue(false)) ?>
    
    <?php  $date = date('Ymd'); ?>
    <?php //Form::getFileContent($form,$model,'img','img', $date . '/goods'); ?>
    <?= Form::getFileContentJietu($form,$model,'img','img', $date . '/project'); ?>  
    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
