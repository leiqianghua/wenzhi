<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
$GLOBALS['my_data'] = $model['shop_list'];
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'cid')->hiddenInput()->label(""); ?>
    
  
    
    <?= \kartik\grid\GridView::widget([
        'export' => false,
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '',   //固定不变
        'columns' => [
            
            "title",
            
            [
                'class'=>'kartik\grid\ExpandRowColumn',
                'detailAnimationDuration' => 200,
                'width'=>'70px',
                'expandIcon' => '<a class="glyphicon glyphicon-expand " data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="全部展开"></a>',
                'collapseIcon' => '<a class="glyphicon glyphicon-collapse-down" data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="收合"></a>',
                //'header' => '<a class="show_detail" data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="全部展开"></a>',
                'value'=>function ($model, $key, $index, $column) {
                    return \kartik\grid\GridView::ROW_COLLAPSED;
                },
                'detail'=>function ($model, $key, $index, $column) {
                    $model['label'] = $GLOBALS['my_data'];
                    return Yii::$app->controller->renderPartial('/project_goods_package/_good_detail', ['model'=>$model]);
                },
                'headerOptions'=>['class'=>'kartik-sheet-style'], 
                'expandOneOnly'=>true,
                'detailRowCssClass' => 'success',
            ],             
            
//                        
//            [
//                 'label' => '小计',
//                 'format' => 'raw',    
//                 'value' => function ($data) {
//                    $html = "ab";
//                    return $html;
//                 },
//             ],  
                         
 
        ],
    ]); ?>    
    
    
    
    
    
    <script>
        $(function(){
            $(document).on("click",".select_goods",function(){
                var obj = $(this);
                var cid = obj.attr("data");

                layer.open({
                  type: 2,
                  title: '选择商品',
                  shadeClose: true,
                  shade: 0.8,
                  area: ['80%', '80%'],
                  content: '/project_goods/select_index?cid='+cid //iframe的url
                });  


            })
            
           
        })
    </script>    
<!--    <div id="_shop_select">
        <?php  $shop_ids = json_decode($model->shop_list,true);  ?>
        
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>商品ID</th>
                    <th>商品名称</th>
                    <th>商品图片</th>
                    <th>商品件数</th>
                </tr>
            </thead>
            <tbody id="goods_list_select_ok">
                <?php include '_shop_select.php';  ?>

            </tbody>
        </table>        
        
        
    </div>-->

  
    
    
    
<!--<table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>分类</th>
                    <th>商品ID</th>
                    <th>商品名称</th>
                    <th>商品图片</th>
                    <th>商品件数</th>
                </tr>
            </thead>
            <tbody id="goods_list_select_ok">
                
                <tr>
                    <td>分类1</td>
                    <td>
                  
    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <td>产品1</td>
            </tr>
        </tbody>
    </table>                          
                        
                    </td>
                </tr>  
                
                
            <tr key="0">
    
    <td><input class="shop_detail_id" type="text" name="shop_list[0][id]" value="337"></td>
    <td>仙人掌－仿真旱地植物 室内外家居酒店会所别墅店面装饰景观 规格:高198cm</td>
    <td><img src="http://image.gubatoo.com/20180509/goods/38827666854673.jpg" width="70" alt=""></td>
    <td>
        <input type="text" name="shop_list[0][num]" value="1">
        <button class="delete_select" type="button">删除</button>
    </td>
</tr>

</tbody>
</table>    -->
    
    
    
    
    <?= $form->field($model, 'sorting') ?>

    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'img','img', $date . '/goods_package'); ?>    
    <?= Form::getFileContent($form,$model,'img_list','img_list', $date . '/goods_package','',[],'',true); ?>
    <?= $form->field($model, 'status')->dropDownList(\common\models\Project_goods_packageModel::getStatusHtml()) ?>  
    <?= Form::editor_element_detail($model, 'details');  ?>
    <?= $form->field($model, 'price') ?>
    
    



    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(document).on("click",".delete_select",function(){
        $(this).parents("tr").eq(0).remove();
    })
    
    
     function calcPrice()
     {
        var obj = $(".num_show");
        var length = obj.length;
        var price = 0;
        for(var i = 0;i<length;i++){
            if(parseInt(obj.eq(i).val())){
                price += parseInt(obj.eq(i).val()) * parseInt(obj.eq(i).attr("price"));
            }
        }
               
        console.log(price);
        $("#project_goods_packagemodel-price").val(price);
     } 
     
     $(document).on('keyup',".num_show",function(){
                var total = $(this).attr("price") * $(this).val();
                $(this).parents("tr").find(".num_total").eq(0).val(total);
                $(this).parents("tr").find(".num_total_html").eq(0).html(total);          
         calcPrice();
     })       
     $(document).on('click',".delete_select",function(){
         calcPrice();
     })       
    
</script>