<?php 
//model就是分类的model  label里面有保存我们想要的值。
$datas = json_decode($model->label,true);
?>
<h3><button data="<?= $model->id  ?>" type="button" class="select_goods" class="btn btn-info">选择商品</button></h3>
<table class="table table-bordered table-striped">
    <tbody id="goods_list_select_ok_<?= $model->id  ?>">
        <tr>
            <th>商品ID</th>
            <th>商品名称</th>
            <th>单位</th>
            <th>出厂价</th>
            <th>市场价</th>
            <th>数量</th>
            <th>小计/操作</th>
        </tr>  
        
        
        <?php  $shop_ids = $datas[$model->id]; $name_show = "shop_list[{$model->id}]"  ?>
        <?php include '_shop_select.php';  ?>        
    </tbody>
</table>