    <div class="form-group">
        <div class="col-xs-3 col-sm-2 text-right">
            <label class="control-label">内部商城所需商品</label>
        </div>
        <div id="_good_from_list" class="col-xs-6 col-sm-4">
            <?php $shop_list =  json_decode($model['shop_list'],true); ?>
            
            <?php if($shop_list){  ?>
                <?php foreach($shop_list as $key => $value){  ?>
                    <div data="<?= $key ?>" class="form-group">
                        <div class="col-xs-4 col-sm-4">
                            <input class="form-control shop_id" type="text" value="<?= $value['id']  ?>" name="shop_list[<?= $key ?>][id]" placeholder="请填写商品ID！" nullmsg="请填写商品ID！"/>
                        </div>   
                        <div class="col-xs-4 col-sm-4">
                            <input class="form-control shop_id" type="text" value="<?= $value['num']  ?>" name="shop_list[<?= $key ?>][num]" placeholder="请填写相应数量！" nullmsg="请填写相应数量！"/>
                        </div>  
                        <?php if($key != 0){  ?>
                            <div class="col-xs-4 col-sm-4">
                                <button data="<?= $key ?>" type="button" class="delete_element btn btn-danger">删除</button>  
                            </div>                         
                        <?php }  ?>                  
                    </div>            
                <?php }  ?>            
            <?php  }else{ ?>
                <div data="0" class="form-group">
                    <div class="col-xs-6 col-sm-6">
                        <input class="form-control shop_id" type="text" name="shop_list[0][id]" placeholder="请填写商品ID！" nullmsg="请填写商品ID！"/>
                    </div>   
                    <div class="col-xs-6 col-sm-6">
                        <input class="form-control shop_id" type="text" name="shop_list[0][num]" placeholder="请填写相应数量！" nullmsg="请填写相应数量！"/>
                    </div>                
                </div>            
            <?php }  ?>

        </div>
        <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
            <button type="button" class="add_element btn btn-info">增加</button>  
        </div>
    </div>        
    <?php include '_good_from.php';  ?>
    
    <script>
    $(function(){
        //增加元素
        function add_element(num)
        {
            var html = $("#_good_from").html();//寻找需要的字符串
            html = html.replace(/NUM_TODO/g,num);//进行要关的替换
            html = html.replace(/ATTRIBUTES_TODO/g,'shop_list');//进行要关的替换
            //开始插入
            $("#_good_from_list").append(html);
        }
        //删除元素
        function delete_element(num)
        {
            $("#_good_from_list > [data='"+num+"']").eq(0).remove();
        }

        $(document).on('click','.delete_element',function(){
            var num = $(this).attr('data');
            delete_element(num);
        })

        $(".add_element").click(function(){
            var num = $("#_good_from_list div[data]").last().attr('data');
            num = parseInt(num) + 1;
            add_element(num);
        })            
    })
</script>

<script>
    $(function(){
        $(document).on("click",".shop_id",function(){
            var obj = $(this);
            
        layer.open({
          type: 2,
          title: '选择商品',
          shadeClose: true,
          shade: 0.8,
          area: ['80%', '80%'],
          content: '/goods/index' //iframe的url
        });  
            
            
        })
    })
</script>