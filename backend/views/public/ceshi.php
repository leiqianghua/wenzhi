<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>角色列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',
                'options' => ['class' => 'form-inline'],
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
    
    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            '日期',
            '店铺',
            '订单',                
            '价格',
            '佣金',
        ],
    ]); ?>
</div>
