<!doctype html>
<html lang="en" class="fullscreen-bg">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>欢迎登陆后台管理系统</title>
    <link rel="stylesheet" href="<?= \yii::$app->request->getHostInfo() . "/css/bootstrap.min.css"  ?>" type="text/css">
    <link rel="stylesheet" href="<?= \yii::$app->request->getHostInfo() . "/css/login.css"  ?>" type="text/css">
</head>
<body>
    <div id="wrapper">
        <div class="vertical-align-wrap">
            <div class="vertical-align-middle">
                <div class="auth-box ">
                    <div class="left">
                        <div class="content">
                            <div class="header">
                                <div class="logo text-center"><img src="/images/logo2.jpg" width="162" alt="华艺卫浴"></div>
                                <p class="lead">欢迎登陆后台管理系统</p>
                            </div>
                            <form class="form-auth-small" method="post" id="form" action="">
                                <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only">Email</label>
                                    <input type="text" name="username" required class="form-control" id="signin-email" value="" placeholder="用户名">
                                </div>
                                <div class="form-group">
                                    <label for="signin-password" class="control-label sr-only">Password</label>
                                    <input type="password" name="password" required class="form-control" id="signin-password" value="" placeholder="密码">
                                </div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">登 录</button>
                            </form>
                        </div>
                    </div>
                    <div class="right">
                        <div class="overlay"></div>
                        <div class="content text">
                            <h1 class="heading">古笆兔</h1>
                            <p>通过与多家大型资材公司联手打造供应链系统，成功开发石、竹、<br>木、根、藤、金、陶、饰、植、辅等多个系列的私家景观素材产品。</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>