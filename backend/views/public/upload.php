<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => ' form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    
    
<?= $form->field($model, 'uid')->fileInput(['name' => "file"])->label("上传") ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
            <button type="submit">确认</button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
