<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>角色列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',
                'options' => ['class' => 'form-inline'],
            ]); ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'id') ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    
    
    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '总共{totalCount}条数据',
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
//            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],
//            ['attribute' => 'add_time','format' => ['image']],
                [
                   'value' => function($model, $key, $index, $column){
                        $delete_url = \yii::$app->controller->to(["/role/delete",'id' => $model['id']]);
                        $update_url = \yii::$app->controller->to(["/role/update",'id' => $model['id']]);
                        $access_url = \yii::$app->controller->to(["/access/index",'role_id' => $model['id']]);
                        $delete = '<a href="'.$delete_url.'" title="Delete" aria-label="Delete" data-confirm="你确定要删除吗?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                        $update = '<a href="'.$update_url.'" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                        $access = '<a  href="'.$access_url.'"><i></i>权限设置</a>';
                        $html = $update . "&nbsp;&nbsp;&nbsp;" . $delete . "&nbsp;&nbsp;&nbsp;" .$access;
                        return $html;
                   },
                   'label' => '管理', 
                   'format' => 'raw',                      
                ],  
        ],
    ]); ?>
</div>
