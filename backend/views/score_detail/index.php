<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>积分明细</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'score_type')->dropDownList($object::getScore_typeHtml(),['prompt' => '全部']) ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    

  
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'id',//直接展示
            'uid',//直接展示
            [
                'attribute' => 'score_type',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getScore_typeHtml($model->score_type);
                }
            ],
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示
            'score',//直接展示
            'remark',//直接展示
  
        ],
    ]); ?>

</div>
