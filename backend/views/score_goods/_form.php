<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
<!--<div class="form-group">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label">一级代理折扣</label>
    </div>
    <div class="col-xs-6 col-sm-4">
        <input type="text" id="" class="form-control" name="">
    </div>
    <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
        <div class="hint-block">最多一个小数,请填写百分比</div>
    </div>
</div>    -->
    
    
    
    <!-- 
    说明
    每个filed是一个div包含着的
        <div class="form-group field-shoplistmodel-title required">
            <div class="col-xs-3 col-sm-2 text-right">
                <label class="control-label" for="shoplistmodel-title">Title</label>
            </div>
            <div class="col-xs-6 col-sm-4">
                <input type="text" id="shoplistmodel-title" class="form-control" name="ShoplistModel[title]">
            </div>
            <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
                <div class="hint-block">
                    <a>增加我要的说明</a>
                </div>
            </div> 
            <div class="col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0">
                <div class="help-block">
                </div>
            </div>
        </div>  
    整个的展示的模板由form配置的template来控制
    一：最外围的div由field的第三个参数来控制
    二：label() 可以控制label元素的属性  由label元素组成
    三：textInput/或其它 可以控制input的相关的参数  由其本身元素组成
    四：hint 可以控制 hint的元素内容 由div组成
    五：error可以控制 错误的展示的展示  由一个 div组成
    总的是由label,inpub,hint,error四部份的内容来组成的
    -->
    <?= $form->field($model, 'name')->hint('<a>商品名称</a>')->textInput() ?>
    <?= $form->field($model, 'sorting')->textInput() ?>
    <?= $form->field($model, 'status')->radioList($object::getStatusHtml());  ?>
    <?= $form->field($model, 'num')->textInput() ?>
    <?= $form->field($model, 'sale_num')->textInput() ?>
<!--    下载图片-->
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'img','img', $date . '/score_gooods'); ?>
    

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>