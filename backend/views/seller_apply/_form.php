<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
<!--<div class="form-group">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label">一级代理折扣</label>
    </div>
    <div class="col-xs-6 col-sm-4">
        <input type="text" id="" class="form-control" name="">
    </div>
    <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
        <div class="hint-block">最多一个小数,请填写百分比</div>
    </div>
</div>    -->
    
    
    
    <!-- 
    说明
    每个filed是一个div包含着的
        <div class="form-group field-shoplistmodel-title required">
            <div class="col-xs-3 col-sm-2 text-right">
                <label class="control-label" for="shoplistmodel-title">Title</label>
            </div>
            <div class="col-xs-6 col-sm-4">
                <input type="text" id="shoplistmodel-title" class="form-control" name="ShoplistModel[title]">
            </div>
            <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
                <div class="hint-block">
                    <a>增加我要的说明</a>
                </div>
            </div> 
            <div class="col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0">
                <div class="help-block">
                </div>
            </div>
        </div>  
    整个的展示的模板由form配置的template来控制
    一：最外围的div由field的第三个参数来控制
    二：label() 可以控制label元素的属性  由label元素组成
    三：textInput/或其它 可以控制input的相关的参数  由其本身元素组成
    四：hint 可以控制 hint的元素内容 由div组成
    五：error可以控制 错误的展示的展示  由一个 div组成
    总的是由label,input,hint,error四部份的内容来组成的
    -->
    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'title')->hint('<a>增加我要的说明</a>')->textInput() ?>
    <?= $form->field($model, 'title')->hint('sdf',['abbbb' => 'gsdfsdfsdf'])->textarea()->label('sfsdf', [])->error(['ab' => 'gsdfsf']) ?>
    <?= $form->field($model, 'title')->hint('<a>sddfsf</a>',['abbbb' => 'gsdfsdfsdf'])->textarea()->label('sfsdf', [])->error(['ab' => 'gsdfsf']) ?>
    <?= ''//\common\components\Form::editor_element_detail($model, 'title2');  ?>
    <?= $form->field($model, 'cateid')->dropDownList(\common\models\CategoryModel::getMenuKeyValue(false));  ?>
    
<!--    下载图片-->
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'img','img', $date . '/goods'); ?>
    <?= Form::getFileContent($form,$model,'img_list','img_list', $date . '/goods','',[],'',true); ?>
    
<!--    编辑器-->
    <?= Form::editor_element_detail($model, 'details');  ?>
    
    <?= Form::getFileContent($form,$model,'title2','title2', 'ee/ee','',[],'',true); ?>
   <?php echo \common\components\Form::getDataForForm("show_start_date",empty($_GET['show_start_date']) ? '' : $_GET['show_start_date'],"show_end_date",empty($_GET['show_end_date']) ? '' : $_GET['show_end_date'],"展示时间范围","J_date length_4","J_date length_4");  ?>
        
    <?= $form->field($model, 'title')->radioList(['a' => 'aa','b' => 'sd']);  ?>
    <?= $form->field($model, 'title')->radio()->label('abcd');  ?>
    <?= $form->field($model, 'title')->radio();  ?>
    <?= $form->field($model, 'title')->checkbox();  ?>
    <?php $model->title = ['a','b']; ?>
    <?= $form->field($model, 'title')->checkboxList(['a' => 'aa','b' => 'sd']);  ?>
    <?= $form->field($model, 'title')->dropDownList(['a' => 'aa','b' => 'sd']);  ?>
    <?= $form->field($model, 'title')->checkboxList(['a' => 'aa','b' => 'sd'],['style' => 'padding-top:7px;']);  ?>

<!--    时间的使用-->
    <?= $form->field($model, 'title')->textInput(['class' => 'form-control J_date length_3']) ?>
    <?= $form->field($model, 'title',['template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>"])->textInput(['class' => 'form-control J_datetime length_3']) ?>
    

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function dddd(url)
    {
        alert(url);
    }
</script>