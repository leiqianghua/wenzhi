<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>供应商申请列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'uid') ?>
            <?= $form->field($model, 'type_1')->dropDownList($object::getType_1Html(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'type_2')->dropDownList($object::getType_2Html(),['prompt' => '全部']) ?>
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml(),['prompt' => '全部']) ?>
            

            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>    
 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'uid',//直接展示
            'username',//直接展示
            'company_address',//直接展示
            'company_name',//直接展示
            'company_iphone',//直接展示
            'company_corporation',//直接展示
            'registered_capital',//直接展示
            'main_products',//直接展示
            'company_profile',//直接展示
            [
                 'attribute' => 'business_license_img',
                 'format' => 'html',    
                 'value' => function ($data) {
                     return Html::img($data['business_license_img'],
                         ['width' => '70px']);
                 },
             ],            
            [
                 'attribute' => 'card_id_img',
                 'format' => 'html',    
                 'value' => function ($data) {
                     return Html::img($data['card_id_img'],
                         ['width' => '70px']);
                 },
             ], 
            [
                'attribute' => 'type_1',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getType_1Html($model->type_1);
                }
            ],
            [
                'attribute' => 'type_2',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getType_2Html($model->type_2);
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getStatusHtml($model->status);
                }
            ],
                         
                         
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示

            [
               'value' => function($model, $key, $index, $column){
                    $html = '';
                    if($model['status'] == 0){//未审核
                        $html .= "<a href='".\yii::$app->controller->to(['agree','id' => $model->id])."' class='ajax_request' >同意</a>";
                        $html .= "</br>";
                        $html .= "<a href='".\yii::$app->controller->to(['refuse','id' => $model->id])."' class='ajax_request' >拒绝</a>";                        
                    }else{
                        $html .= "<a href='".\yii::$app->controller->to(['delete','id' => $model->id])."' class='ajax_request' >删除</a>";
                    }
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

    <?= Html::button('删除',['class' => 'select_list btn','url' => $this->context->to(["delete"])]) ?>

</div>
