<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    

    <?= $form->field($model, 'name')->hint('<a>你想要保存的文件名,包括后缀</a>')->textInput() ?>
    <?= $form->field($model, 'sorting')->hint('<a>资源列表排序</a>')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml()) ?>
    <?= $form->field($model, 'url')->hint('<a>上传的文件</a>')->fileInput(['name' => "url"]) ?>
    <?= $form->field($model, 'cid')->dropDownList(\common\models\ZiyuanClassModel::getMenuKeyValue(false)) ?>
    <?= $form->field($model, 'label')->dropDownList(\common\models\MemberModel::getLabelHtmlAll(false)) ?>
    
    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::button($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
