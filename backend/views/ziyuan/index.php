<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>列表示例</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'name') ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>    

<div style="display:none" id="dddd">
    <form class="layermmain">
        <input type="text" name="bbbbbb" value="ccc" />
    </form>    
</div>


<script>
    var DIALOG_CONTENT = $("#dddd").html();
</script>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'url',//直接展示
            ['attribute' => 'add_time','format' => ['date', 'Y-M-d']],//时间展示
            'name',//直接展示
            'sorting',//直接展示
            [
                'attribute' => 'status',
                'value' => function($model){
                    $obj = get_class($model);
                    return $obj::getStatusHtml($model['status']);
                },   
                
            ],                
            [
               'value' => function($model, $key, $index, $column){
                    $html = '';
                    if($model->status == 1){
                        $html = "<a href='ajaxupdate?status=0&id={$model->id}' class='ajax_request' >下架</a>";
                    }else{
                        $html = "<a href='ajaxupdate?status=1&id={$model->id}' class='ajax_request' >上架</a>";
                    }
                    
                    $html .= "<a href='delete?id={$model->id}' class='ajax_request' >删除</a>";
                    $html .= "<a href='update?id={$model->id}'  >修改</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

    <?= Html::button('批量下架',['class' => 'select_list btn','url' => $this->context->to(['ajaxupdates',"status" => 0])]) ?>
    <?= Html::button('批量上架',['class' => 'select_list btn','url' => $this->context->to(['ajaxupdates',"status" => 1])]) ?>



</br>
<?= Html::button('批量修改级别',["id" => "all_update_label",'class' => 'select_list btn','url' => $this->context->to(['ajaxupdates'])]) ?>    
<?php echo \common\components\Tool::getSelectData(\common\models\MemberModel::getLabelHtmlAll(false), "ziyuan", ["id" => "change_all"], true, []) ?>
    
</div>

<script>
    $("#change_all").change(function(){
        var label = $(this).val();
        if(!label){
            return false;
        }
        $("#all_update_label").attr("url","/ziyuan/ajaxupdates?label=" + label);
    })
</script>