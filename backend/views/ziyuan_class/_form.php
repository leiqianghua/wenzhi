<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<div class="admin-role-model-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'J_ajaxForm form-horizontal'],
        'fieldConfig' => [
                  'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-7'>{input}</div><div class='col-xs-12 col-xs-offset-3 col-sm-3 col-sm-offset-0'>{error}</div>",
        ]        
        ]); ?>

    <?= $form->field($model, 'parent_id')->dropDownList($object::getMenuKeyValue()) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sorting')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml()) ?>
    
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'img','img', $date . '/goods_class'); ?>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
