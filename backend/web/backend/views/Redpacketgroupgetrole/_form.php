<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RedPacketGroupGetRoleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="red-packet-group-get-role-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meet')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'allblance')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'humans')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gotime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endtime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_open')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
