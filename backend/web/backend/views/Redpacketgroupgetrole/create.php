<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RedPacketGroupGetRoleModel */

$this->title = 'Create Red Packet Group Get Role Model';
$this->params['breadcrumbs'][] = ['label' => 'Red Packet Group Get Role Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="red-packet-group-get-role-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<!--<style type="text/css">
#w0 .content_list{margin-top: -5px;}
#w0 .content_list .btn{padding: 3px 12px !important;}
.red_select .content_list{padding-left: 4.4%;}
</style>
<script type="text/javascript">
  $(function(){
  	var f_input = $("#w0 .content_list .list .form-inline input.form-control");
  	var red_con = $("#w0 .content_list .list .form-inline .red_select");
  	var red_con_f = $(".red_select>div");
  	var red_con_f_label = $(".red_select .text-right");
  	var red_con_f_select = $(".red_select select").parent();
  	f_input.addClass("col-sm-3");
  	red_con.addClass("col-sm-9");
  	red_con_f.addClass("col-sm-7");
  	red_con_f_label.addClass("col-xs-5 col-sm-4");
  	red_con_f_select.addClass("col-xs-6 col-sm-8");
  	
  })
</script>-->














