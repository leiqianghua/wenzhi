<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RedPacketGroupGetRoleModel */

$this->title = 'Update Red Packet Group Get Role Model: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Red Packet Group Get Role Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="red-packet-group-get-role-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
