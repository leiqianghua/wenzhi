(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD (Register as an anonymous module)
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var _init = function(){
        // 刷新页面
        $("#refresh_page").click(function(){
            var show_id = $(".main_body iframe.is_show").data('id');
            var show_id = 'iframe_' + show_id;
            window.frames[show_id].location.reload();   
        })
        
    }

    _init();

}));




$(function(){ 

    // btn tooltip
     $('[data-toggle="tooltip"]').tooltip();

    // add Left Menu last
    $(".sub_menu_list li:last-child").addClass("last");

    // Toggle Left Menu
    $('.menu_list > a').click(function() {
        var parent = $(this).parent();
        var sub = parent.find('> ul');
        if(sub.is(':visible')){
            sub.slideUp(200,function(){
                parent.removeClass('menu_active');
            });
//         parent.siblings().find('> ul').slideUp(200); 
        }else{
            //visibleSubMenuClose();
            sub.slideDown(200,function(){
                parent.addClass('menu_active');
            });
            parent.siblings().find('> ul').slideUp(200); 
        }
        return false;
    });

});

//show only one subMenu
function visibleSubMenuClose() {
    jQuery('.menu_list').each(function(){
        var t = jQuery(this);
        if(t.hasClass('menu_active')) {
            t.find('> ul').slideUp(200, function(){
                t.removeClass('menu_active');
            });
        }
    });
}