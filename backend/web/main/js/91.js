$(function(){
    var tip={
            mousePos:function(e){
                     var x,y;
                     var e = e||window.event;
                     return{x:e.clientX+document.body.scrollLeft+document.documentElement.scrollLeft,
                     y:e.clientY+document.body.scrollTop+document.documentElement.scrollTop};
            },
    }


    function isEllipsis(dom) {
           var checkDom = dom.cloneNode(), parent, flag;
           checkDom.style.width = dom.offsetWidth + 'px';
           checkDom.style.height = dom.offsetHeight + 'px';
           checkDom.style.overflow = 'auto';
           checkDom.style.position = 'absolute';
           checkDom.style.zIndex = -1;
           checkDom.style.opacity = 0;
           checkDom.style.whiteSpace = "nowrap";
           checkDom.innerHTML = dom.innerHTML;

           parent = dom.parentNode;
           parent.appendChild(checkDom);
           flag = checkDom.scrollWidth > checkDom.offsetWidth;
           parent.removeChild(checkDom);
           return flag;
   };



//    $(".table-striped tbody td").mouseover(function(e){
//        var is_shen = isEllipsis($(this)[0]);
//        if(is_shen){//这个是要处理的。
//            $(this).parent().css("position","relative");
//            var mouse = tip.mousePos(e);
//            var value = $(this).html();
//            var left = mouse.x + 10 + 'px';
//            var top = mouse.y + 10 + 'px';
//            var html = "<td class='tip' style='position:absolute;left:"+left+";top:"+top+";'>"+value+"</td>";
//            $("body").append(html);
//        }else{//这个是不要处理的
//            return true;
//        }
//    })     
//    $(".table-striped tbody td").mouseout(function(e){
//        var is_shen = isEllipsis($(this)[0]);
//        if(is_shen){//这个是要处理的。
//            $("body").find(".tip").remove();
//            //$(this).next(".tip").remove();
//        }else{//这个是不要处理的
//            return true;
//        }
//    })  
})





$(function(){
    //不支持placeholder浏览器下对placeholder进行处理
    if (document.createElement('input').placeholder !== '') {
        $('[placeholder]').focus(function () {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function () {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        }).blur().parents('form').submit(function () {
            $(this).find('[placeholder]').each(function () {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            });
        });
    }
})


$(function(){
    var dateInput = $("input.J_date")
    var dateTimeInput = $("input.J_datetime")
    if (dateInput.length) {
        loadJsCss('/resources/datePicker/style.css');
        loadJsCss('/resources/datePicker/datePicker.js',function(){
            $(document).on("click","input.J_date",function(){
                $(this).datePicker().focus();;
            })
            //dateInput.datePicker();
        });
    }
    if (dateTimeInput.length) {
        loadJsCss('/resources/datePicker/style.css');
        loadJsCss('/resources/datePicker/datePicker.js',function(){
            $(document).on("click","input.J_datetime",function(){
                $(this).datePicker({
                    time: true
                }).focus();;
            })            
//            dateTimeInput.datePicker({
//                time: true
//            });
        });
    }
})


/*** js和css按需加载 ***/
//url 为加载的链接 //callback为加载后的回调 函数
function loadJsCss(url, callback ){// 非阻塞的加载 后面的js会先执行
  function onloaded(script, callback){//绑定加载完的回调函数
    if(script.readyState){ //ie
      script.attachEvent('onreadystatechange', function(){
        if(script.readyState == 'loaded' || script.readyState == 'complete'){
          script.className = 'loaded';
          callback && callback.constructor === Function && callback();
        }
      });
    }else{
      script.addEventListener('load',function(){
        script.className = "loaded";
        callback && callback.constructor === Function && callback();
      }, false); 
    }
  }


  
  var isJs = /\/.+\.js($|\?)/i.test(url) ? true : false;  
  if(!isJs){ //加载css
    var links = document.getElementsByTagName('link');
    for(var i = 0; i < links.length; i++){//是否已加载
      if(links[i].href.indexOf(url)>-1){ 
        return; 
      }
    }
    var link = document.createElement('link');
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    var head = document.getElementsByTagName('head')[0]; 
    head.insertBefore(link,head.getElementsByTagName('link')[0] || null );
  }else{ //加载js
    var scripts = document.getElementsByTagName('script');
    for(var i = 0; i < scripts.length; i++){//是否已加载
      if(scripts[i].src.indexOf(url)>-1 && callback && (callback.constructor === Function) ){ 
      //已创建script
        if(scripts[i].className === 'loaded'){//已加载
          callback();
        }else{//加载中
          onloaded(scripts[i], callback);
        }
        return; 
      }
    }
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = url; 
    document.body.appendChild(script);
    onloaded(script, callback); 
    
  }
}


//重新刷新页面，使用location.reload()有可能导致重新提交
function reloadPage(win) {
    var location = win.location;
    location.href = location.pathname + location.search;
}




//layer的弹窗提示
function layer_alert(info)
{
    layer.msg(info);
}

//执行回调并返回相关对象
function excallback(obj)
{
    var callback = obj.attr('callback');
    if(callback){
        var data_obj = window[callback]();
        if(callback === false){
            return false;
        }
        return data_obj;
    }else{
        return {};
    }
} 


//进行服务端请求 通过url 与请求的data_obj
function requestajax(url,data_obj)
{
    $.post(url, data_obj, function(data){
        if(((data.status === true || data.status === 1) && data.status != 500)){
            if(data.url){
                window.location.href = data.url;                        
            }else{
                window.location.reload();
            }                
        }else{
            layer.alert(data.info);
        }
    },'json');    
}

//a标签如果 有带 .ajax_request  则会变成ajax请求链接并跳转
//请求前会先调用 excallback(obj),如果返回false则不处理，如果返回true则继续请求
//excallback 中会看是否有带callback属性，如果有的话则会去执行这个方法。
$(function(){
    $(document).on('click','a.ajax_request',function(e){
    e.preventDefault();
    var msg = $(this).attr('msg');
    msg = !msg ? '确认此操作吗？' : msg;
    var obj = $(this);
    var url = obj.attr('href');
    

    
    layer.open({
        content: msg,
        btn: ['确认', '取消'],
        yes: function(){
            var data_obj = excallback(obj);
            if(data_obj === false){
                var error = window.ERROR == '' ? '请求出错' : window.ERROR;
                layer.open({
                    className: 'alertMsg',
                    content: error,
                    time:2
                }); 
                return false;
            }            
            requestajax(url,data_obj);
        }
    });      
})
})



//弹窗的操作增加了表单的一些元素的功能。。。
//a标签有带 ajax_layer 的请求
//定义这个  DIALOG_CONTENT  或者带有content属性  来确定content
//弹出来的内容必须含有  layermmain 这个class 这个class是form的class
$(function(){
    $(document).on('click','a.ajax_layer',function(e)
    {

        var FUNCTION_REQUEST = {};
        FUNCTION_REQUEST.findContentForLayer = function(obj)
        {     
            var content = typeof DIALOG_CONTENT == 'string' ? DIALOG_CONTENT : obj.attr('content');
            return content;
        }

        FUNCTION_REQUEST.ajaxLayerContent = function(content,data_obj,url)
        {
            layer.open({
                content:content,
                btn:['确认','取消'],
                yes:function(index, layers){
                    var obj_form = $(".layui-layer-content .layermmain");
                    //获取其它的参数
                    FUNCTION_REQUEST.addFormData(data_obj,obj_form);//增加元素的值，
                    requestajax(url,data_obj);//然后再次的请求某个链接
                },
            });         
        }

        FUNCTION_REQUEST.addFormData = function (data_obj,form_obj)
        {
            var obj_from = form_obj.find('input,textarea,select');
            var length = obj_from.length;
            if(length){
                for(var i=0; i<length; i++){
                    var name = obj_from.eq(i).attr('name');
                    if(typeof data_obj[name] == 'undefined'){
                        if(form_obj.find('[name="'+name+'"]').attr("type") == "checkbox"){
                            data_obj[name] = form_obj.find('[name="'+name+'"]:checked').length;
                        }else{
                            data_obj[name] = form_obj.find('[name="'+name+'"]').val();
                        }
                    }
                }
            }        
        }    




        e.preventDefault();
        var obj = $(this);
        //第一步执行callback确定对象
        var data_obj = excallback(obj);
        if(data_obj === false){
            var error = window.ERROR == '' ? '请求出错' : window.ERROR;
            layer.msg(error);
            return false;
        }        
        var url = obj.attr('href');

        var content = FUNCTION_REQUEST.findContentForLayer(obj);
        if(content){
            FUNCTION_REQUEST.ajaxLayerContent(content,data_obj,url);           
        }else{
            var u = obj.attr('url');
            if(!u){
                return false;
            }else{
                $.post(u,data_obj,function(data){
                    if(data){
                        var content = data;
                        FUNCTION_REQUEST.ajaxLayerContent(content,data_obj,url);                                  
                    }
                });
            }
        }
    })
    

})


//任意表单   form class J_ajaxForm   button为J_ajax_submit_btn  表单通过ajax的方式去提交这个表单
$(function(){
    var ajaxForm_list = $('form.J_ajaxForm');
    if (ajaxForm_list.length) {
            $(document).on('click', '.J_ajax_submit_btn', function (e) {
                e.preventDefault();
                var btn = $(this);
                var form = btn.parents('form.J_ajaxForm');
                
                var index_load = null;
                
                form.ajaxSubmit({
                    url: btn.data('action') ? btn.data('action') : form.attr('action'), //按钮上是否自定义提交地址(多按钮情况)
                    dataType: 'json',
                    beforeSubmit: function (arr, $form, options) {
                        index_load = layer.load(2, {shade: false});
                    },
                    success: function (data, statusText, xhr, $form) {
                        if(index_load){
                            layer.close(index_load);
                        }
                        
                        if(data.status){
                            if(data.status == -1){
                                layer.open({
                                    content:data.content,
                                }); 
                                return true;
                            }
                            layer.msg(data.info,{time: 1000, icon:6},function(){
                                if(btn.hasClass("close_layer")){
                                    var index=parent.layer.getFrameIndex(window.name);
                                    parent.layer.close(index);
                                    return true;
                                }
                                if (data.url) {
                                    window.location.href = data.url;
                                }else{
                                    window.history.back();
                                }                                 
                            })
                        }else{
                            layer.msg(data.info,{icon:2});
                        }
                    }
                });
            });
    }
})


//全选后的操作
//需要 class 为select_list  且带有属性 url的才可以操作
$(function(){
    $(".select_list").click(function(){
        //获取ID列表
        var objs = $("[name='selection[]']:checked");
        var length = objs.length;
        if(length == 0){
            layer.msg('请选择需要操作的对象',{icon:2});
            return false;
        }
        var id = [];
        for(var i=  0;i<length;i++){
            id[i] = objs.eq(i).val();
        }
        var url = $(this).attr('url');
        if(!url){
            layer.msg('找不到需要请求的链接',{icon:2});
            return false;            
        }
        requestajax(url,{id:id});
    })
})


$(function(){
    $("#show_hide").click(function(){
        $(".J_ajaxForm").slideToggle(500,function(){});  
    })
    $(".show_hide_slide").click(function(){
        var obj = $(this).next();
        obj.slideToggle(500,function(){});  
    })
})


$(function(){
    $(".layer_show").click(function(){
        var   value = $(this).attr("content");
        layer.open({
          type: 1,
          skin: 'layui-layer-rim', //加上边框
          area: ['420px', '240px'], //宽高
          content: value
        });
    })
})


/*
 * 倒计时
 * times 现在的毫秒数
 * objc 操作的是哪一个的jquery的字符串选择器
 * ok_html 已经结束的时候 显示的文字
 * not_ok_html 未开始，，比如是0的时候  的显示的文字
 */
function gg_show_Time_fun(times,objc,ok_html,not_ok_html){
    if(times == 0){
        $(objc).html(not_ok_html);return true;
    }

    time=(times-(new Date().getTime()));
    i=parseInt((time/1000)/60);
    s=parseInt((time/1000)%60);
    ms=String(Math.floor(time%1000));
    ms=parseInt(ms.substr(0,2));
    h=parseInt(i/60);
    if(i<10)i='0'+i;
    if(s<10)s='0'+s;
    if(ms<10)ms='00';
    if(h>0){
        mm =parseInt(i%60);
        $(objc).html('<font class="houst">'+h+'</font>:<font class="minute">'+mm+'</font>:<font class="second">'+s+'</font>秒');	
    }else{
        $(objc).html('<font class="minute">'+i+'</font>:<font class="second">'+s+'</font>:<font class="millisecond">'+ms+'</font>');	
    }
    if(time<0){
        $(objc).html(ok_html);return true;
    }
    setTimeout(function(){										 	
        gg_show_Time_fun(times,objc,ok_html,not_ok_html);				 
    },1); 
}


//从今天算获取AddDayCount天后的日期
function GetDateStr(AddDayCount)
{ 
    var dd = new Date(); 
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期 
    var y = dd.getFullYear(); 
    var m = dd.getMonth()+1;//获取当前月份的日期 
    var d = dd.getDate(); 
    if(m < 10){
        m = "0" + m;
    }
    if(d < 10){
        d = "0" + d;
    }
    return y+"-"+m+"-"+d; 
}

//获取本周开始的日期num，往前推几天
function getWeekTime()
{
    var mydate = new Date();
    var xinqi = mydate.getDay();//当前是星期几  星期0到星期6  可以知道我们应该是要往前推几天了.
    if(xinqi == 0){
        return 6;
    }else{
        return xinqi - 1;
    }
}                            


function getPreMonth(date) {
    var arr = date.split('-');
    var year = arr[0]; //获取当前日期的年份
    var month = arr[1]; //获取当前日期的月份
    var day = arr[2]; //获取当前日期的日
    var days = new Date(year, month, 0);
    days = days.getDate(); //获取当前日期中月的天数
    var year2 = year;
    var month2 = parseInt(month) - 1;
    if (month2 == 0) {//如果是1月份，则取上一年的12月份
        year2 = parseInt(year2) - 1;
        month2 = 12;
    }
    var day2 = day;
    var days2 = new Date(year2, month2, 0);
    days2 = days2.getDate();
    if (day2 > days2) {//如果原来日期大于上一月的日期，则取当月的最大日期。比如3月的30日，在2月中没有30
        day2 = days2;
    }
    if (month2 < 10) {
        month2 = '0' + month2;//月份填补成2位。
    }
    var t2 = year2 + '-' + month2 + '-' + day2;
    return t2;
}


function getPreMonthDate(date) {
    var day2 = new Date(date);
    day2.setTime(day2.getTime());
    var month = day2.getMonth()+1;
    var date = day2.getDate();
    if (month < 10) {
        month = '0' + month;
    }
    if (date < 10) {
        date = '0' + date;
    }
    var s2 = day2.getFullYear()+"-" + month + "-" + date;
    var s2 = getPreMonth(s2);
    return s2;
}


function dateContent(start_date_obj,end_date_obj,data_obj)
{
    console.log(getWeekTime());
    var data = data_obj.attr("date");

    switch(data){
        case "date_1"://今天
            start_date_obj.val(GetDateStr(0));
            end_date_obj.val(GetDateStr(1));
            break;
        case "date_2"://昨天
            start_date_obj.val(GetDateStr(-1));
            end_date_obj.val(GetDateStr(0));
            break;
        case "date_3"://前天
            start_date_obj.val(GetDateStr(-2));
            end_date_obj.val(GetDateStr(-1));
            break;
        case "date_4"://本周
            var start_date_num = getWeekTime();
            start_date_obj.val(GetDateStr(-start_date_num));
            end_date_obj.val(GetDateStr(1));
            break;
        case "date_5"://本月
            var mydate = new Date();
            var today_date = mydate.getDate() - 1;
            start_date_obj.val(GetDateStr(-today_date));
            end_date_obj.val(GetDateStr(1));
            break;
        case "date_6"://同比，上个月的这一天

            var start_date = getPreMonthDate(start_date_obj.val());
            var end_date = getPreMonthDate(end_date_obj.val());
            start_date_obj.val(start_date);
            end_date_obj.val(end_date);
            break;
    }
    return false;
}
                   
$(function(){
    $(".copy_data").click(function(){
        var kid = $(this).attr("data");
        var str_model = $(this).attr("str_model");
        $.post("copy_data",{id:kid,str_model:str_model},function(data){
            if(data.status){
                window.location.reload();
            }else{
                layer_alert(data.info);
            }
        },"json");
    })
    

    $(".select_detail").click(function(){
        var select_id = $(this).attr("data");
        var select_game_name = $(this).attr("data_name");
        
        var html = select_game_name;
        
        var id = window.parent.SELECT_DATA.id;
        var data = window.parent.SELECT_DATA.data;
        $(window.parent.document).find("#" + id).val(select_id);
        $(window.parent.document).find("#" + data).html(html);
        var index=parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }) 
    
    

    $("body").bind("keydown",function(event){
           if (event.keyCode == 116) {
                     event.preventDefault(); //阻止默认刷新
                     //location.reload();
                     //采用location.reload()在火狐下可能会有问题，火狐会保留上一次链接
                     location=location;
         }
     })
})
$(function(){
    
    //打开窗口的方式  通过链接
    $(document).on('click','a.layer_open_url',function(e){
        e.preventDefault();
        var url = $(this).attr("href");
        var is_reload = $(this).hasClass("is_reload");
        layer.open({
            type: 2,
            //shadeClose: true,
            //shade: 0.8,
            area: ['70%', '70%'],
            maxmin:true,
            content: url, //iframe的url
            end:function(){
                if(is_reload){
                    reloadPage(window);
                }
            },
            //scrollbar:false,
            success:function(layero, index){
                //layer.iframeAuto(index);
            }
        });
    }) 
    
    //打开窗口的方式  通过链接  data里面需要有 图片
    $(document).on('click','.layer_open_images',function(e){
        e.preventDefault();
        var data = $(this).attr("data");
        console.log(data);
        var array = data.split(",");
        //var array = JSON.parse(data);
        var obj = {};
        obj.title = "图片查看";
        obj.id = 123;
        obj.start = 0;
        obj.data = [];
        for(var i=0;i<array.length;i++){
            obj.data[i] = {};
            obj.data[i].alt = "";
            obj.data[i].pid = 99;
            obj.data[i].src = array[i];
            obj.data[i].thumb = "";
        }
        layer.photos({
          photos: obj,
          anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        });
    }) 
    
    
}) 


$(function(){
    $("img.fanda").hover(function(){
        $(this).after("<p id='pic'><img style='max-width:600px;min-width:100px' src='"+this.src+"' id='pic1'></p>");
        $("img.fanda").mousemove(function(e){
            $("#pic").css({
                "top":(e.pageY+10)+"px",
                "left":(e.pageX+20)+"px"
            }).fadeIn("fast");
            // $("#pic").fadeIn("fast");
        });
    },function(){
        $("#pic").remove();
    });
});


//$(function(){
//    $(document).on("click",".urllayer",function(e){
//        e.preventDefault();
//        var url = $(this).attr("href");
//        
//        var index = layer.open({
//            type:2,
//            content:url,
//            area:["70%","70%"],
//            maxmin:true,
//            scrollbar:false,
//            success:function(layero, index){
//                //layer.iframeAuto(index);
//            }
//        });        
//    })
//})
