<?php
//各应用都需要继承的控制器基类。  包括各增删改查基类控制器
namespace common\components;


use yii;
use yii\web\Controller;
class BaseController extends Controller 
{
    
    protected $tVar = [];//变量
    public $modelClass;//对应的对象  你需要的对象
    public $user;//用户的模型
    public static $error = null;//静态执行的时候错误字符串或者是数组等。
    
    public function init()
    {
        $this->user = yii::$app->user;
        $this->enableCsrfValidation = false;
        yii::$app->controller = $this;
        //$this->user->isLogin();
        parent::init();
    }
    
    //定义我们需要的一些常量
    public function beforeAction($action)
    {
        defined('TIMESTAMP') or define('TIMESTAMP', time());//时间戳
        return parent::beforeAction($action);
    }
    
    public function to($url = '', $scheme = false)
    {
        return yii\helpers\Url::to($url,$scheme);
    }
    
    
    
    //便捷的跳转数据
    public function success($arr = [], $url = '', $is_ajax = false)
    {
        if(yii::$app->request->isAjax){
            $is_ajax = true;
        }
        
        if(is_string($arr)){
            $info = $arr;
            $arr = ['info' => $info];
        }
        $data = ['status' => 1, 'info' => '处理成功'];
        $url == '' ? $url = Yii::$app->request->getReferrer() : $url;
        empty($url) ? $url = \yii\helpers\Url::to(['index/index']) : '';
        $data['url'] = $url;
        $data = array_merge($data,$arr);
        if($is_ajax){
            exit(json_encode($data));  
        }else{
            empty($data['wait_second']) ? $data['wait_second'] = 3 : '';
            $content = $this->renderPartial("@app/views/common/success", $data);
            exit($content);
        }      
    }
    
    //便捷的跳转数据
    public function error($arr = [],  $url = '',$is_ajax = false)
    {
        if(yii::$app->request->isAjax){
            $is_ajax = true;
        }
        
        if(is_string($arr) || empty($arr)){
            $info = empty($arr) ? '处理失败' : $arr;
            $arr = ['info' => $info];
        }
        $data = ['status' => 0, 'info' => '处理失败'];
        if($url){
            $data['url'] = $this->to($url);
        }else{
            $data['url'] = '';
        }
        
        
        $data = array_merge($data,$arr);
        if($is_ajax){
            exit(json_encode($data));  
        }else{
            empty($data['wait_second']) ? $data['wait_second'] = 3000 : '';
            $content = $this->renderPartial("@app/views/common/error", $data);
            exit($content);
        }   
    }
    
    public function ajaxReturn($arr = [])
    {
        if(is_string($arr)){
            $info = $arr;
            $arr = ['info' => $info];
        }
        if(!isset($arr['status'])){
            $arr['status'] = 1;
        }
            
        exit(json_encode($arr));  
    }
    
    public function assign($name, $value = '')
    {
        if (is_array($name)) {
            $this->tVar = array_merge($this->tVar, $name);
        } else {
            $this->tVar[$name] = $value;
        }
        return $this;
    }
    
    
    public function render($view = '', $params = []) 
    {
        //为空时尝试自动定位
        if (empty($view)) {
            $view = \yii::$app->requestedAction->id;
        }
        //分配变量到模板
        if (!empty($params) && is_array($params)) {
            $this->assign($params);
        }
        return parent::render($view, $this->tVar);
    }
    


    
    /*
     * $model 模型
     * $params 条件参数
     * $limit 每页显示的条数
     * $render 页面
     * $order 排序相关
     */
    protected function baseIndex($model, $params = array(), $limit = 20, $order = [], $render = '',$search = "search")
    {
        if(is_object($model)){
            $obj_model = $model;
            $model = get_class($model);
        }else{
            $obj_model = new $model();
        }

        //参数
        $params = array_merge($_GET,$params);
        
        //数据
        //数据
        if(method_exists($obj_model, $search)){
            $dataProvider = $obj_model->$search($params);
        }else{
            $dataProvider = $obj_model->search($params);
        }
        if($order){
           $dataProvider->sort->defaultOrder =  $order;
        }else{
            $str_model = get_class($obj_model);
            $id_key = $str_model::getTableSchema()->primaryKey;
            $id_key = $id_key[0];  
            $dataProvider->sort->defaultOrder =  [$id_key => SORT_DESC];
        }
        $dataProvider->pagination->pageSize = $limit;
        if(!empty($params['no_total'])){
            $dataProvider->setTotalCount(1000000);
            if($dataProvider->getCount() < $limit){//已经是最后一页了 计算总数
                if(empty($_GET['page'])){
                    $all_total = $dataProvider->getCount();
                }else{
                    $all_total = $limit*($_GET['page'] - 1) + $dataProvider->getCount();
                }
                
            }else{
                $all_total = 1000000;
            }
            $dataProvider->setTotalCount($all_total);
            $dataProvider->pagination->totalCount = $all_total;
        }
        if($render === true){
            return $dataProvider;
        }
        $this->assign('object', $model);   
        $this->assign(['dataProvider' => $dataProvider]);
        $this->assign(['model' => $obj_model]);
        return $this->render($render);             
    }
    

    /**
     * @param type $str_model  模型对象或者模型名
     * @param type $url
     */
    protected function baseCreate($str_model, $url = 'index',$success_msg = "")
    {
        if(is_object($str_model)){
            $obj_model = $str_model;
            $str_model = get_class($obj_model);
        }else{
            $obj_model = new $str_model(); 
            $obj_model->resetAttributes();           
        }
        //$obj_model->scenario = 'create';


        $this->assign('model', $obj_model);
        $this->assign('object', $str_model);  
        if ($_POST) {
            $obj_model->attributes = array_merge($_GET,$_POST);
            if(strripos($str_model, "\\") !== false){
                $b = strripos($str_model, "\\");
                $str = substr($str_model, $b + 1);
            }else{
                $str = $str_model;
            }
            if(isset($_POST[$str])){
                $obj_model->attributes = $_POST[$str];  
            }
            
            if ($obj_model->save()) {
                if($success_msg){
                    $this->success($success_msg, $this->to($url));
                }else{
                    $this->success('添加成功！', $this->to($url));
                }
            } else {
                $error = $obj_model->getOneError();
                $this->error($error ? $error : '添加失败！');
            }
        } else {
            return $this->render();
        }
    }

    /**
     * 基础信息编辑
     * 如果修改成功后需要返回数据这一条的后台数据，$is_content设置为true，
     * 且需要找到data.php这个HTML页面，并做为info参数
     * 且要修改的数据必须是post过来的数据。
     * @param model $model 模型对象或者模型名
     * @param type $url
     */
    protected function baseUpdate($str_model, $url = '', $is_content = false, $viewdata = 'data')
    {
        if(is_object($str_model)){
            $obj_model = $str_model;
            $str_model = get_class($obj_model);
        }else{
            $id_key = $str_model::getTableSchema()->primaryKey;
            $id_key = $id_key[0];
            $id = \common\components\GuestInfo::getParam($id_key, '');
            if(!$id){
                $id = \common\components\GuestInfo::getParam('id', '');
            }
            $obj_model = $str_model::findOne($id);
        }
        $this->assign('object', $str_model);   
        if (empty($obj_model)) {
            $this->error('该信息不存在！');
        }
        //$obj_model->scenario = 'update';
        if ($_POST) {
            $obj_model->attributes = array_merge($_GET,$_POST); 
            if(strripos($str_model, "\\") !== false){
                $b = strripos($str_model, "\\");
                $str = substr($str_model, $b + 1);
            }else{
                $str = $str_model;
            }            
            if(isset($_POST[$str])){
                $obj_model->attributes = $_POST[$str];                
            }        

            if ($obj_model->save()){
                if($is_content){
                    if($is_content == -1){
                        $arr = array(
                            'status' => -1,
                            'content' => "修改成功",
                        );
                        $this->ajaxReturn($arr);                        
                    }
                    $content = $this->renderPartial($viewdata, array('rs' => $obj_model,'object' => $str_model));
                    $arr = array(
                        'status' => 1,
                        'content' => $content,
                    );
                    $this->ajaxReturn($arr);
                }
                if($url){
                    $this->success('修改成功！', $this->to($url));
                }else{
                    $url = \yii::$app->cache->get($this->id . "_" . $this->action->id . "_url");
                    $this->success('修改成功！',$url);
                }
                
            } else {
                $error = $obj_model->getOneError();
                $this->error($error ? $error : '修改失败！');
            }
        } else {   
            $this->assign('model', $obj_model);
            //记住上一个页面，到时候跳转的时候直接到这里面来
            \yii::$app->cache->set($this->id . "_" . $this->action->id . "_url",\yii::$app->request->getReferrer());
            return $this->render();
        }
    }

    /**
     * 基本信息删除
     * @param model $model 模型对象或者模型名
     * @param type $url
     */
    protected function baseDelete($str_model, $url = 'index')
    {   
        if(is_object($str_model)){
            $obj_model = $str_model;
            $str_model = get_class($obj_model);
            $update = $obj_model->delete();
        }else{
            $id_key = $str_model::getTableSchema()->primaryKey;
            $id_key = $id_key[0];
            $id = \common\components\GuestInfo::getParam($id_key, '');
            if(!$id){
                $id = \common\components\GuestInfo::getParam('id', '');
            }
            $update = $str_model::deleteAll([$id_key => $id]);
            
        }        

        if (!$update) {
            $this->error($str_model::$error);
        }else{
            $this->success('删除成功！',$this->to($url));
        }
    }
    
    public function baseView($str_model)
    {
        if(is_object($str_model)){
            $obj_model = $str_model;
            $str_model = get_class($obj_model);
        }else{
            $id = \common\components\GuestInfo::getParam($str_model::getTableSchema()->primaryKey, '');
            if(!$id){
                $id = \common\components\GuestInfo::getParam('id', '');
            }
            $obj_model = $str_model::findOne($id);
        }
        
        $this->assign(['model' => $obj_model]);
        $this->assign(['object' => $str_model]);
        return  $this->render();        
    }
    
    protected function setReturnMenu($title,$url = "")
    {
        $menu_return['name'] = $title;
        if($url){
            $menu_return['url'] = $url;
        }else{
            if(empty($_SERVER['HTTP_REFERER'])){
                $menu_return['url'] = "";
            }else{
                $menu_return['url'] = $_SERVER['HTTP_REFERER'];
            }
        }
        \yii::$app->cache->set('menu_return_erp', $menu_return);//设置其链接
        return $menu_return;
    }
}