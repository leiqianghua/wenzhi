<?php

//直接继承自model的基类MODEL
namespace common\components;
class BaseFormModel extends \yii\base\Model
{
    public function getOneError()
	{     //对象返回错误时单个的错误
        $error = $this->getErrors();
        if (!empty($error)) {
            $current = current($error);
            return $current[0];
        } else {
            return null;
        }
    }
    public function setSafeAttributes($values, $safeOnly = true)
    {
		$this->setAttributes($values, false);
    }	
}