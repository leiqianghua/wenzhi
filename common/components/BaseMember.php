<?php
//基本的会员类
namespace common\components;

abstract class BaseMember
{
    public $uid;//登录的UID
    public $member_info;//会员信息
    public $error;
    
    public function __construct()
    {
        $this->init();
    }
    
    public function init()
    {
        return true;
    }
    
    //是否登录
    abstract public function isLogin();
    
    //退出
    abstract public function logout();
    
    
    //注册
    abstract public function register($datas);
    
    //获取用户数据
    abstract public function getMember();
    
    //登录
    abstract public function login($datas);
    
    

    public function __get($name)
    {
        if(!$this->member_info){
            $this->member_info = $this->getMember();
        }
        return !isset($this->member_info[$name]) ? '' : $this->member_info[$name];
    }   
    
    public function getIdentity($autoRenew = true)
    {
        return $this;
    }
    public function getId()
    {
        return $this->uid;
    }
    
    //是否是管理员
    public function isAdmin()
    {
        return 0;
    }
    
    
    //加密操作
    public function createPassword($password)
    {
        return md5($password);
    }        
}