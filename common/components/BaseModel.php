<?php
//数据模型扩展
//各个数据模型都应该直接继承
namespace common\components;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use yii;
class BaseModel extends ActiveRecord 
{
    public static $error = null;//静态执行的时候错误字符串或者是数组等。
    public function getOneError() 
    {    
        //对象返回错误时单个的错误
        $error = $this->getErrors();
        if (!empty($error)) {
            $current = current($error);
            return $current[0];
        } else {
            return '';
        }
    }
    
    //默认的能赋值的都可以赋值
    //默认的能赋值的都可以赋值
    public function setAttributes($values, $safeOnly = false)
    {
        //echo get_called_class();exit;
        
        $str = get_called_class();
        $str_model = substr($str,strrpos($str, '\\')+1);
        if(!empty($values[$str_model])){
            $values = array_merge($values, $values[$str_model]);
            unset($values[$str_model]);            
        }
        
        $columns = static::getTableSchema()->columns;
        //var_dump($values);exit;
        foreach($values as $key => $value){
            
            if(!isset($columns[$key])){
                unset($values[$key]);
                continue;
            }
            
            if($value === null){
                $values[$key] = '';
            }            
            
            if(isset($columns[$key]) && strpos($key,'_time') !== false){
                if(strpos($columns[$key]->dbType,'int') !== false){
                    if($time = strtotime($value)){
                        $values[$key] = $time;//把时间格式的数据直接转为时间戳，  1：要满足字段是带 _time 2:字段的类型为int型
                    }
                }              
            }
           
            // if($value === null && isset($columns[$key]) && $columns[$key]->phpType != 'string'){
            //     unset($values[$key]);
            // }
            if(is_array($value)){
                $values[$key] = json_encode($value);
            }
        }
        parent::setAttributes($values, $safeOnly);
    }
    
    //增加两个相应的方法
    public static function deleteAll($condition = '', $params = [])
    {
        $state = static::beforeDeleteAll($condition,$params);
        if(!$state){
            return false;
        }
        $parent = parent::deleteAll($condition,$params);
        static::afterDeleteAll($condition,$params);
        return $parent;
    } 
    
    public static function beforeDeleteAll(&$condition = '', $params = [])
    {
        return true;
    }    
    public static function afterDeleteAll($condition = '', $params = [])
    {
        return true;
    }
    
    //基本的搜索
    public function search($params)
    {
        $alias = "main";
        $query = static::find();
        $query->alias($alias);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $name = $this->formName();
        if(!empty($params[$name])){
            $da = $params[$name];
            $params = array_merge($da,$params);
        }
        $this->load($params,'');

//        if (!$this->validate()) {
//            return $dataProvider;
//        }
        
        $attributes = $this->attributes;
        $filter = [];
        if(is_array($attributes)){
            $str_model = get_class($this);
            $id_key = $str_model::getTableSchema()->primaryKey;
            $id_key = $id_key[0];  
            //确定条件 如果数据类型是字符串的话则是用搜索的条件，其它的则是用相等的条件
            $columns = static::getTableSchema()->columns;
            foreach($attributes as $attribute => $value){
                $filter = [];
                $str_key = "{$alias}.{$attribute}";
                if(!empty($columns[$attribute]) && $columns[$attribute]->type == 'string'){
                    //如果这个字段是主键的话是用查询，如果不是主键的话则用其它的
                    if($attribute == $id_key){
                        $filter[$str_key] = $this[$attribute];
                    }else{
                        if(!empty($params['attributes_no_search']) && in_array($attribute, $params['attributes_no_search'])){
                            $filter[$str_key] = $this[$attribute];
                        }else{
                            $filter = ['like', $str_key, $this[$attribute]];
                        }
                    }
                }else{
                    $filter[$str_key] = $this[$attribute];
                }
                $query->andFilterWhere($filter);       
            }            
        }
        
        
        if (!empty($_GET['start_add_time'])) {
            $time = strtotime($_GET['start_add_time']);
            $query->andWhere("{$alias}.add_time>={$time}");
        }
        if (!empty($_GET['end_add_time'])) {
            $time = strtotime($_GET['end_add_time']);
            $query->andWhere("{$alias}.add_time<{$time}");
        }        
         
        return $dataProvider;
    }
    
    //自动的添加相关的时间
    public function behaviors()
    {
        $parent = parent::behaviors();
        if($this->hasAttribute('add_time') || $this->hasAttribute('update_time')){
            if(!$this->hasAttribute('add_time')){
                $time =  [
                    [
                        'class' => \yii\behaviors\TimestampBehavior::className(),
                        'createdAtAttribute' => false,
                        'updatedAtAttribute' => 'update_time',
                       // 'value' => new Expression('NOW()'),
                    ],
                ];                
            }else if(!$this->hasAttribute('update_time')){
                $time =  [
                    [
                        'class' => \yii\behaviors\TimestampBehavior::className(),
                        'updatedAtAttribute' => false,
                        'createdAtAttribute' => 'add_time',
                       // 'value' => new Expression('NOW()'),
                    ],
                ];                 
            }else{
                $time =  [
                    [
                        'class' => \yii\behaviors\TimestampBehavior::className(),
                        'createdAtAttribute' => 'add_time',
                        'updatedAtAttribute' => 'update_time',
                       // 'value' => new Expression('NOW()'),
                    ],
                ];                 
            }         
        }
        if(!empty($time)){
            $data = array_merge($parent,$time);
            return $data;
        }else{
            return $parent;
        }
    } 
    
    //获取键值对数据
    //$is_all  是否增加一个全部的选项
    public static function getBaseKeyValue($key,$value,$where = [])
    {
        $result = [];
        $datas = static::find()->where($where)->asArray()->all();
        foreach($datas as $data){
            $result[$data[$key]] = $data[$value];
        }
        return $result;
    }
    
    //根据主键来查询数据  利用缓存
    public static function getOne($key, $name = '',$expire = 600)
    {
        $init_key = $key;
        $cache_obj = \yii::$app->cache;
        $class_name = get_class(new static());
        $key = $class_name . "_getOne_{$key}";
        
        if($data = $cache_obj->get($key)){
            if($name){
                return $data[$name];
            }
            return $data;
        }
        
        $model = static::findOne($init_key);
        if(!$model){
            return false;
        }
        $data = $model->attributes;
        $cache_obj->set($key,$data,$expire);
        if($name){
            return $data[$name];
        }        
        return $data;
    }
    
    //删除单条记录的缓存
    public static function removeGetOne($key)
    {
        $cache_obj = \yii::$app->cache;
        $class_name = get_class(new static());
        $key = $class_name . "_getOne_{$key}";
        $cache_obj->delete($key);
    }

    //修改商品时默认的给调用一下修正单条记录缓存的记录
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if(!$insert){
            $id_key = static::getTableSchema()->primaryKey;
            $id_key = $id_key[0];            
            static::removeGetOne($this->$id_key);
        }
    }
    
    public static function getDBname()
    {
        $db = static::getDb();
        $config = $db->dsn;
        preg_match('/dbname=([\w_-]+)/',$config, $matches);  
        return $matches[1];
    }
    
    

    public function getAttribute($name)
    {
        $data = parent::getAttribute($name);
        if($data_json = json_decode($data)){
            return $data_json;
        }else{
            return $data;
        }
    }


    public function setAttribute($name, $value)
    {
        if(is_array($value)){
            $value = json_encode($value);
        }
        return parent::setAttribute($name, $value);
    }
    
    //基础的获取数据的接口
    //获取最终sql语句的方法  $query->createCommand()->getRawSql();
    /* ---------------------------$condition参数说明--------------------------------------  */
    /*
     * noget  如果传了这个键值  则表示  $_get $_post 参数  不与 $condition合并，否则 会合并
     * page   第几页
     * pagesixe 一页多少条数据
     * order  排序方式
     * 其它的参数则会 参与到 where中去，  示例： ab => 'gsd'  where(其它的)  
       
    
    */
    /* ---------------------------------------------------------------------------  */
    //array $condition 各条件 
            
    //$is_cache 是否缓存
    //$cache_time 缓存的时间
    //$is_getsql 获取相应的sql语句
    public static function baseGetDatas($condition = [], $is_cache = false, $cache_time = 600,$is_getsql = false)
    {
        $result = [];
        if(!isset($condition['noget'])){
            $getpost = array_merge($_GET, $_POST);
            foreach($getpost as $key => $value){
                if(is_numeric($key)){
                    unset($getpost[$key]);
                }
            }
            if(!empty($getpost['from'])){
                unset($getpost['from']);
            }
            $condition = array_merge($getpost,$condition);
        }
        
        $query = static::find();//对象
        

        static::addConditionQuery($query, $condition);//增加其它相应的条件

        //确定分页 与  排序
        $condition['page'] = empty($condition['page']) ? 1 : $condition['page'];  //第几页
        $condition['pagesize'] = empty($condition['pagesize']) ? 20 : $condition['pagesize']; //一页显示的条数
        $condition['order'] = empty($condition['order']) ? '' : $condition['order']; //排序
        $limit = $condition['pagesize'];
        $offset = ($condition['page']-1)*$condition['pagesize'];
        $query->offset($offset)->limit($limit);
        if(!empty($condition['order'])){
            static::getOrderd($query,$condition['order']);
        }
        
        
        
        //生成相应的条件，且记录相关所会用到的键
        $where = [];
        $where[] = 'and';
        $condition_need = [];
        $columns = static::getTableSchema()->columns;
        foreach ($condition as $key => $value) {
            if(is_numeric($key)){
                $where[] = $value;
            }else if(isset($columns[$key])){
                $where[] = [$key => $condition[$key]];
            }            
        }
        $query->where($where);
        

        if($is_getsql){
            if(count($query->where) == 1){
                $query->where = '';
            }            
            return $query->createCommand()->getRawSql();
        }
        //是否有缓存 如果有则直接读取
        if($is_cache){
            //确定各会用到的需要加入到键值的条件
            $key_condition = [];
            $system_array = [
                'order',
                'page',
                'pagesize',
                'select',
                'distinct',
                'from',
                'groupBy',
                'orderBy',
                'having',
                'union',
                'alias',
                'leftJoin',
                'join',
                'innerJoin',
                'rightJoin',
                'noget',
                'gettype',
            ];
            foreach($condition as $key => $value){
                if(is_numeric($key)){
                    $key_condition[$key] = $value;
                }else if(!empty($columns[$key])){
                    $key_condition[$key] = $value;
                }else if(in_array($key, $system_array)){
                    $key_condition[$key] = $value;
                }
            }            
            $cache_key = static::createKey($key_condition);
            $cache_key = get_called_class() . "_" . $cache_key;
            $datas = yii::$app->cache->get($cache_key);
            if($datas){
                return $datas;
            }
        }
        
        
        $gettype = isset($condition['gettype']) ?  $condition['gettype'] : '';//取数据模式 0只取数据 1取总数+分页  2取数据+总数+分页
        if(count($query->where) == 1){
            $query->where = '';
        }
        
        
        switch ($gettype) {
            case 1://取分页 + 数据  + 总数
                $datas = [];
                $data = $query->asArray()->all();//数据
                $total = $query->offset('')->limit('')->count();//总数
                $page_obj = new \yii\data\Pagination();
                $page_obj->setPage($condition['page']);
                $page_obj->setPageSize($condition['pagesize']);
                $page_obj->totalCount = $total;
                $datas = [
                    'page' => $page_obj,
                    'total' => $total,
                    'datas' => $data,
                ];
                break;
            case 0: // 取数据
            default:
                $datas = $query->asArray()->all();
                break;
        }
        
        //最终数据
        if($is_cache){
            yii::$app->cache->set($cache_key,$datas,$cache_time);
        }
        return $datas;       
    }
    
    
    public static function addConditionQuery($query,$condition)
    {
        if(!empty($condition['select'])){
            $query->select($condition['select']);
        }        
        if(!empty($condition['distinct'])){
            $query->distinct($condition['distinct']);
        }      
        if(!empty($condition['from'])){
            $query->from($condition['from']);
        }      
        if(!empty($condition['alias'])){
            $query->alias($condition['alias']);
        }           
        if(!empty($condition['groupBy'])){
            $query->groupBy($condition['groupBy']);
        }    
        if(!empty($condition['orderBy'])){
            $query->orderBy($condition['orderBy']);
        }    
        if(!empty($condition['having'])){
            $query->having($condition['having']);
        }   
        if(!empty($condition['union'])){
            $query->union($condition['union']);
        }  
        
        
        //联合
        if(!empty($condition['leftJoin'])){
            $query->leftJoin($condition['leftJoin']['table'],$condition['leftJoin']['on']);
        }  
        if(!empty($condition['innerJoin'])){
            $query->innerJoin($condition['innerJoin']['table'],$condition['innerJoin']['on']);
        }    
        if(!empty($condition['rightJoin'])){
            $query->rightJoin($condition['rightJoin']['table'],$condition['rightJoin']['on']);
        }  
        if(!empty($condition['join'])){
            $query->join($condition['join']['type'],$condition['join']['table'],$condition['join']['on']);
        }        
    }
    
    
    public static  function getOrderd($query,$orderd)
    {
        return true;
        //$query->orderBy('title desc');
        //return true;	
    }    
    
    
    //生成key
    public static function createKey($keys)
    {
        $keys = static::KsortDatas($keys);
        if(is_array($keys)){
            return json_encode($keys);
        }else{
            return $keys;
        }
    } 
    
    //如果是数组则直接变成排序后的数组
    public static function KsortDatas($array)
    {
        if(is_array($array)){
            ksort($array);
            foreach($array as $key => $value){
                if(is_array($value)){
                    $array[$key] = static::KsortDatas($value);
                }
            }       
        }
        return $array;
    }    
    
    
    /**
     * 基础的处理
     * $is_lock  是否是需要锁，
     * $is_transaction  是否是需要事务
     * $check_lock  true  为一直等待锁  如果false拿不到锁则直接返回
     * @param type $cache_key   为你需要处理的cache_key
     * @return boolean
     */
    public static function baseTit($is_lock,$is_transaction,$check_lock,$cache_key,$callback = 'tit',$params = [])
    {
        try {
            //锁
            if($is_lock){
                if($check_lock){
                    Tool::addLock($cache_key);//增加锁had
                }else{
                    $state = Tool::checkLock($cache_key);
                    if(!$state){//没有拿到锁则直接不处理 返回true
                        throw new Exception('正在处理');
                        //return true;
                    }
                }
            }
            //事务
            if($is_transaction){
                $transaction = Yii::$app->db->beginTransaction();//开启事务      
            }
                       

            //TODO
            if(empty($params)){
                $result = $this->$callback();//处理具体的逻辑的操作
            }else{
                $result = $this->$callback($params);//处理具体的逻辑的操作
            }
            
            if(!$result){
                throw new Exception(static::$error);
            }
            
            if($is_lock){
                Tool::deleteLock($cache_key);//释放锁
            }
            if($is_transaction){
                $transaction->commit();//提交事务
            }            
            
            return $result;
        } catch (Exception $exc) {
            if($is_lock){
                Tool::deleteLock($cache_key);//释放锁
            }
            if(!empty($transaction)){
                $transaction->rollBack();//释放事务          
            }            
            
            static::$error = $exc->getMessage();
            return false;
        }
    }
    
    //$attribte是哪一个属性
    public function getHesetImg($attribte = "img")
    {
        $name = "{$attribte}_img_jietu_";
        if(!empty($_POST[$name]["x1"])){
            $obj = new \common\components\UploadFile();
            $file = $obj->getNameForJie($this->$attribte, $_POST[$name]["x1"], $_POST[$name]["y1"], $_POST[$name]["x2"], $_POST[$name]["y2"], $_POST[$name]["w1"], $_POST[$name]["h1"], '');
            if($file){
                $this->$attribte = $file;
            }
        }        
    }
    
    public static function add_Field(&$datas)
    {
        return $datas;
    }
            
}