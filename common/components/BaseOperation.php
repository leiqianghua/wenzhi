<?php

namespace common\components;
use Exception;
use common\components\TmpLog;
use yii;

class BaseOperation extends \yii\base\Object
{
    public $error;//错误的信息
    public $data_list = [];  //多个的处理
    public $data_detail = null;//单个的处理
    /**
     * 基础的处理
     * $is_lock  是否是需要锁，
     * $is_transaction  是否是需要事务
     * $check_lock  true  为一直等待锁  如果false拿不到锁则直接返回
     * @param type $cache_key   为你需要处理的cache_key
     * @return boolean
     */
    public function baseTit($is_lock,$is_transaction,$check_lock,$cache_key,$callback = 'tit',$params = [])
    {
        try {
            //锁
            if($is_lock){
                if($check_lock){
                    Tool::addLock($cache_key);//增加锁had
                }else{
                    $state = Tool::checkLock($cache_key);
                    if(!$state){//没有拿到锁则直接不处理 返回true
                        throw new Exception('没有拿到锁');
                        //return true;
                    }
                }
            }
            //事务
            if($is_transaction){
                $transaction = Yii::$app->db->beginTransaction();//开启事务      
            }
                       

            //TODO
            if(empty($params)){
                $result = $this->$callback();//处理具体的逻辑的操作
            }else{
                $result = $this->$callback($params);//处理具体的逻辑的操作
            }
            
            if(!$result){
                throw new Exception($this->error);
            }
            
            if($is_lock){
                Tool::deleteLock($cache_key);//释放锁
            }
            if($is_transaction){
                $transaction->commit();//提交事务
            }            
            
            return $result;
        } catch (Exception $exc) {
            if($is_lock){
                Tool::deleteLock($cache_key);//释放锁
            }
            if(!empty($transaction)){
                $transaction->rollBack();//释放事务          
            }            
            
                     $this->error   = "行数:" . $exc->getLine() . "文件："  . $exc->getfile() . "错误信息：" . $exc->getMessage() . "调试信息" . $exc->getTraceAsString();
            \common\components\TmpLog::addData("支付商品", $this->error, "");
            return false;
        }
    }
    
    /**
     * 处理单个的逻辑
     * @return boolean
     */
    protected function tit()
    {
        return true;
    }
    
    /**
     * 获取相关的数据
     * @return type
     */
    public function getDatas()
    {
        $this->data_list = [];
        return $this->data_list;
    }    
    
    /**
     * 多个的处理
     * $is_lock  是否是需要锁，
     * $is_transaction  是否是需要事务
     * $check_lock  true  为一直等待锁  如果false拿不到锁则直接返回
     * $cache_key_qian 为单值 的key  比如 number_id shop_id
     * @param type $error_type  发生错误时的保存的类型
     * @return boolean
     */
    public function baseRun($error_type,$is_lock = true,$is_transaction = true,$check_lock = true,$cache_key_qian = '')
    {
        try {
            $success_num = 0;//成功的个数
            $error_num = 0;//失败的个数
            $result = [];//结果的汇总
            $this->getDatas();//获取需要处理的相关的数据
            
            //逻辑处理
            foreach($this->data_list as $model){
                $this->data_detail = $model;
                if($is_lock){
                    $cache_key = md5($error_type . $this->data_detail[$cache_key_qian]);
                }else{
                    $cache_key = '';
                }
                $state = $this->baseTit($is_lock,$is_transaction,$check_lock,$cache_key);
                if($state){ //单个的处理成功
                    $result['success'][] = $state;
                    $success_num += 1;
                }else{ //单个的处理失败
                    $error_num += 1;
                    $result['error'][] = $this->error;
                    if(is_object($model)){
                        TmpLog::addData($error_type, $this->error . "---" . json_encode($model->attributes));
                    }else{
                        TmpLog::addData($error_type, $this->error . "---" . json_encode($model));
                    }
                }
            }
            //结果展示
            $html = '';
            foreach($result as $key => $value){
                if($key == 'success'){
                    $str_value = implode('<br/>', $value);
                    $html .= "成功的个数{$success_num},值为<br/>{$str_value}.</br>";
                }else{
                    $str_value = implode('<br/>', $value);
                    $html .= "失败的个数{$error_num},值为<br/>为{$str_value}.</br>";
                }
            }
            return $html;
        } catch (Exception $exc) {
            TmpLog::addData($error_type, "多个的处理" . $exc->getMessage());
            $this->error = $exc->getMessage();
            return false;
        }
    }
}