<?php
/*
 * 包括了一些操作的回调,操作前或者是操作后的一些回调
 * 操作如果返回false都是会影响其具体的结果的
 */
namespace common\components;
use Exception;
class CallbackOperation extends \yii\base\Object
{
    static $error;
    
    //商品购买完了后的相关的通知
    public static function afterGoodsBuy($order_model)
    {
        $first = "您好,古笆兔用户:{$order_model['username']}购买商品如下：";
        $product = [];
        $order_shoplist = \common\models\OrderGoodsModel::baseGetDatas(["pagesize" => 999,'order_id' => $order_model['order_id']],false);
        foreach($order_shoplist as $data){
            $product[] = $data['name'];
        }
        $product = implode(',', $product);
        
        $price = $order_model['order_amount'];
        $time = date('Y-m-d H:i:s',$order_model['add_time']);
        $data = [
                "first" => [
                    "value" => $first,
                    "color" => "#fffff",
                ],
                "product" => [
                    "value" => $product,
                    "color" => "#fffff",
                ],
                "price" => [
                    "value" => $price,
                    "color" => "#fffff",
                ],
                "time" => [
                    'value' => $time,
                    "color" => "#fffff",
                ],
                "remark" => [
                    "value" => "祝您生活愉快！",
                    "color" => "#fffff",
                ],
        ];        
        //$url =  "http://www.gubatoo.com" . \yii::$app->controller->to(['/wap/member_order/detail','id' => $order_model['order_id']]);
        $url = "http://www.gubatoo.com/wap/member_order/detail?id={$order_model['order_id']}";
        //通知上级，通知我们平台
        $notice_uid = \common\models\ConfigModel::getListForKey("goods_buy_notice");
        if($notice_uid){
            $uids = explode(',', $notice_uid);
            foreach($uids as $uid){
                $obj = new TemplateMessage();
                $obj->uid = $uid;//固定写死
                $obj->buy_notice($data,$url);                
            }
        }

        
        
        $m = \common\models\MemberModel::getOne($order_model['uid']);
        if($m && $m['parent_id']){
            $obj = new TemplateMessage();
            $obj->uid = $m['parent_id'];//固定写死
            $obj->buy_notice($data,$url);            
        }
        
        $gongying_uids = static::notice_gongyin($order_model);
        if($gongying_uids){
            foreach($gongying_uids as $uid){
                $obj = new TemplateMessage();
                $obj->uid = $uid;//固定写死
                $obj->buy_notice($data,$url);                
            }            
        }
    }
    
    public static function notice_gongyin($order)
    {
        $condition = [];
        $condition['noget'] = true;
        $condition['order_id'] = $order['order_id'];
        $condition['pagesize'] = 9999;
        $order_goods = \common\models\OrderGoodsModel::baseGetDatas($condition);
        if(!$order_goods){
            return [];
        }
        $uids = [];
        foreach($order_goods as $key => $value){
            if($value['admin_id']){
                if(!in_array($value['admin_id'], $uids)){
                    $uids[] = $value['admin_id'];
                }
            }
        }
        return $uids;
    }
    
    
}