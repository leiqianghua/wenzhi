<?php
//导出文件
namespace common\components;
use PHPExcel;
use PHPExcel_IOFactory;
class Excel_Download extends \yii\base\Object
{
    public $config = [];//设置文件的一些属性 如果需要设置的话     ['setCreator' => "Maarten Balliauw"]
    
    private $objPHPExcel;//对象
    
    public $title = '';//展示的标题
    
    public $download_data; //你需要载的相关的数据   是一个二维的数据    一行一行的数据   然后每一行中列的相关的数据
    
    public $other_data = [];
    
    public $config_size = [];//第几列其设置的宽度是多少。
    public function init()
    {
        require_once (__DIR__.'/PHPExcel/Classes/PHPExcel.php');        
        $this->objPHPExcel = new PHPExcel(); 
        
        if($this->config){
            $obj = $this->objPHPExcel->getProperties();
            foreach($this->config as $method => $data){
                $obj->$method($data);
            }
        }
        
        if(is_string($this->download_data)){
            //print_r($this->download_data);
            $this->download_data = $this->get_td_array($this->download_data);
            //print_r($this->download_data);exit;
            if($this->other_data){
                $this->download_data = array_merge($this->other_data, $this->download_data);
            }
        }
    }
    
    
    
    public  function createData($obj)
    {
        $row = $column = 0;//行与列
        foreach($this->download_data as $keys => $values){
            $row++;//行
            $column = 0;//列
            foreach($values as $key => $value){
                $column++;
                $row_string = chr($column + 64);
                $row_column = $row_string . $row;
                if(is_numeric($value)){
                    $obj->setCellValueExplicit($row_column,$value,\PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    //$value = $value + 0;
                }else{
                    $obj->setCellValueExplicit($row_column,$value,\PHPExcel_Cell_DataType::TYPE_STRING);
                }
                
            }
            $obj->getColumnDimension($row_string)->setAutoSize(true);   
        }
        if($this->config_size){
            foreach ($this->config_size as $row => $size) {
                $row_string = chr($row + 64);
                $obj->getColumnDimension($row_string)->setWidth($size);   
            }
        }
    }
    
    
    public function run($table_num = 0)
    {
        $obj = $this->objPHPExcel->setActiveSheetIndex($table_num);//设置表
        
        $this->createData($obj);//填充数据
        
        
        
        $this->objPHPExcel->getActiveSheet()->setTitle($this->title);  //设置标题
        //下载相关的数据
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');  
        //header('Content-Disposition: attachment;filename="01simple.xlsx"');  
        header('Content-Disposition: attachment;filename="'.$this->title.'.xlsx"');  
        header('Cache-Control: max-age=0');  
        $objWriter = PHPExcel_IOFactory:: createWriter($this->objPHPExcel, 'Excel2007'); 
        $objWriter->save( 'php://output');  
        exit;
    }
    
    
    function get_td_array($table) 
    {
        //$table = preg_match('/<table [\s\S]?table>/', $table, $matches);
        $table = preg_match('/<table[\w\W]*<\/table>/', $table, $matches);
        $table = $matches[0];
        
        
        $table = preg_replace("'<table[^>]*?>'si","",$table);
        $table = preg_replace("'<tr[^>]*?>'si","",$table);
        $table = str_replace("</th>","</td>",$table);
        $table = str_replace("<th>","<td>",$table);
        $table = preg_replace("'<td[^>]*?>'si","",$table);
        $table = str_replace("</tr>","{tr}",$table);
        $table = str_replace("</td>","{td}",$table);
        $table = str_replace("&nbsp","",$table);
        //print_r($table);exit;
        //去掉 HTML 标记 
        $table = preg_replace("'<[/!]*?[^<>]*?>'si","",$table);
        $table = preg_replace("'<[/!]*?[^<>]*?>'si","",$table);
        //print_r($table);exit;
        $table = preg_replace("[\r\n\\s]","",$table);
        //去掉空白字符 
        $table = preg_replace("'([rn])[s]+'","",$table);
        $table = str_replace(" ","",$table);
        $table = str_replace(" ","",$table);
//        echo $table;exit;
//        echo "</br>";
//        echo "</br>";
        $tt = [];
        $table = explode('{tr}', $table);
        foreach($table as $k => $v){
            $table[$k] = trim($v);
            if(!empty($table[$k])){
                $tt[] = $table[$k];
            }
        }
        $table = $tt;
        //print_r($table);
        //array_pop($table);
        foreach ($table as $key=>$tr) {
            $td = explode('{td}', $tr);
            array_pop($td);
            $td_array[] = $td;
        }
        return $td_array;
    }    
    
}
