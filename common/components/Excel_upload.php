<?php
//导出文件
namespace common\components;
use PHPExcel;
use PHPExcel_IOFactory;
class Excel_upload extends \yii\base\Object
{
    public static $file_name = "TaokeDetail-2018-07-31 (1).xls";

//Array
//(
//    [0] => Array
//        (
//            [A] => 创建时间
//            [B] => 点击时间
//            [C] => 商品信息
//        )
//
//    [1] => Array
//        (
//            [A] => 2018-07-19 17:48:23
//            [B] => 2018-07-19 17:47:21
//            [C] => 儿童奖励贴纸女孩手工粘贴钻石贴画亚克力水晶装饰宝石立体包邮
//        )
//
//)    
    //可以把其转为我们想要的一个数组。通过一个文件让其转为我们想要的一个数组。
    public static function getArrData($exts = 'xls')
    {
        require_once (__DIR__.'/PHPExcel/Classes/PHPExcel.php');     
        //如果excel文件后缀名为.xls，导入这个类
        if ($exts == 'xls') {
            require_once (__DIR__.'/PHPExcel/Classes/PHPExcel/Reader/Excel5.php');    
            $PHPReader = new \PHPExcel_Reader_Excel5();
        } else if ($exts == 'xlsx') {
            require_once (__DIR__.'/PHPExcel/Classes/PHPExcel/Reader/Excel2007.php');    
            $PHPReader = new \PHPExcel_Reader_Excel2007();
        }
 
        //载入文件
        $PHPExcel = $PHPReader->load(static::$file_name);
        //获取表中的第一个工作表，如果要获取第二个，把0改为1，依次类推
        $currentSheet = $PHPExcel->getSheet(0);
        //获取总列数
        $allColumn = $currentSheet->getHighestColumn();
        //获取总行数
        $allRow = $currentSheet->getHighestRow();
        //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
        for ($currentRow = 1; $currentRow <= $allRow; $currentRow++) {
            //从哪列开始，A表示第一列
            for ($currentColumn = 'A';strlen($allColumn) > strlen($currentColumn) || $currentColumn <= $allColumn; $currentColumn++) {
                //数据坐标
                $address = $currentColumn . $currentRow;
                //读取到的数据，保存到数组$data中
                $cell = $currentSheet->getCell($address)->getValue();
 
                if ($cell instanceof PHPExcel_RichText) {
                    $cell = $cell->__toString();
                }
                $data[$currentRow - 1][$currentColumn] = $cell;
                //  print_r($cell);
            }
 
        }
        if(!empty($data)){
            return $data;
        }else{
            return [];
        }
        
        
    }    
    
}
