<?php
namespace common\components;
class Form 
{
    
    //多张或一张图片
    //$form 表单域
    //$model model的对象
    //$attribute 属性
    //$id 要用的ID
    //$hint 提示说明
    public static function getFileContent($form,$model,$attribute,$id,$filepath,$hint='',$options = [],$callbackdata = '',$is_more = false,$upload_url = '',$delete_url = '')
    {
        $obj = $form->field($model, $attribute);
        $template = $obj->template;
        $img = $model[$attribute];
        $img_arr = json_decode($img,true);
        $str_image = '';
        if(is_array($img_arr)){//是数组
            $str_image = '<div id="div_img_'.$id.'">';
            foreach($img_arr as $dd){
                $str_image .= '
                    <div class="div_img">
                        <img data-action="zoom"  src="'.$dd.'">
                        <input type="hidden" value="'.$dd.'" name="'.$attribute.'[]">
                        <div class="update uploader-close">
                        </div>
                    </div>
                ';                
            }
            $str_image .= '</div>';
        }else if($img){//是一个字符串
            $str_image = '
                <div class="div_img">
                    <img data-action="zoom"  src="'.$img.'">
                    <input type="hidden" value="'.$img.'" name="'.$attribute.'">
                    <div class="update uploader-close">
                    </div>
                </div>
            ';
        }else{//没有内容
            $str_image = '';
        }
        //\common\components\TmpLog::addData("支付宝支付", $img, '188');
        
        
        //\common\components\TmpLog::addData("支付宝支付", $str_image, '188');
        $button_name = isset($options['button']['name']) ? $options['button']['name'] : "上传文件";
        $button_class = isset($options['button']['class']) ? $options['button']['class'] : "btn";
        $input = \yii\helpers\Html::button($button_name,['id' =>$id,'class' => $button_class]);
        $label = $model->attributeLabels();
        $label = $label[$attribute];
        if($id=='seo'||$id=='summer_new_goods'){
        	$label='图片上传';
        }
        if(!$label){
            $html = '<div class="form-group">
                        <div class="col-xs-12 col-sm-12">
                            '.$input.'
                            '.$str_image.'
                        </div>
                    </div>';            
        }else{
            $html = '<div class="form-group">
                        <div class="col-xs-3 col-sm-2 text-right">
                            <label class="control-label">'.$label.'</label>
                        </div>
                        <div class="col-xs-9 col-sm-7">
                            '.$input.'
                            '.$str_image.'
                        </div>
                    </div>';            
        }

        
        
        
        $html .= static::uploadfile($id,$attribute,$filepath,$callbackdata,$is_more,$upload_url,$delete_url);
        
        return $html;
    }
    
    //多张或一张图片
    //$img 图片内容 链接  可以是数组。
    //$form 表单域
    //$model model的对象
    //$attribute 属性
    //$id 要用的ID
    //$hint 提示说明
    public static function getFileContent_new($img,$label,$attribute,$id,$filepath,$callbackdata = '',$is_more = false,$upload_url = '',$delete_url = '',$inline = false)
    {
        $img_arr = json_decode($img,true);
        $str_image = '';
        if(is_array($img_arr)){//是数组
            $str_image = '<div id="div_img_'.$id.'">';
            foreach($img_arr as $dd){
                $str_image .= '
                    <div class="div_img">
                        <img data-action="zoom" src="'.$dd.'">
                        <input type="hidden" value="'.$dd.'" name="'.$attribute.'[]">
                        <div class="update uploader-close">
                        </div>
                    </div>
                ';                
            }
            $str_image .= '</div>';
        }else if($img){//是一个字符串
            $str_image = '
                <div class="div_img">
                    <img data-action="zoom" src="'.$img.'">
                    <input type="hidden" value="'.$img.'" name="'.$attribute.'">
                    <div class="update uploader-close">
                    </div>
                </div>
            ';
        }else{//没有内容
            $str_image = '';
        }
        //\common\components\TmpLog::addData("支付宝支付", $img, '188');
        
        
        //\common\components\TmpLog::addData("支付宝支付", $str_image, '188');
        $input = \yii\helpers\Html::button('上传文件',['id' =>$id,'class' => 'btn']);
        if(!$inline){
            $html = '<div class="form-group">
                        <div class="col-xs-3 col-sm-2 text-right">
                            <label class="control-label">'.$label.'</label>
                        </div>
                        <div class="col-xs-9 col-sm-7">
                            '.$input.'
                            '.$str_image.'
                        </div>
                    </div>';            
        }else{
            $html = $label . $input . $str_image;
        }

        
        
        
        $html .= static::uploadfile($id,$attribute,$filepath,$callbackdata,$is_more,$upload_url,$delete_url);
        
        return $html;
    }        
    
    //id  为id元素的
    //name 为你需要的name的属性
    //savepath为你希望图片保存的路径
    //callbackdata为成功后这个图片 然后的回调的函数
    //is_more是否是传多张图片
    //upload_url 为上传的服务器路径
    //delete_url为删除图片的路径
    public static function uploadfile($id,$name,$savepath,$callbackdata = '',$is_more = false,$upload_url = '',$delete_url = '')
    {
        $upload_url = $upload_url === '' ? \yii::$app->request->hostinfo . \yii::$app->controller->to(['/sns/uploadfile']) : $upload_url;
        $delete_url = $delete_url === '' ? \yii::$app->request->hostinfo . \yii::$app->controller->to(['/sns/deletefile']) : $delete_url;
         
        $str = "";
        
        //增加相关的文件
        $file = [ //加载相关的文件
            "/resources/plupload-2.1.2/js/plupload.full.min.js",
            "/resources/plupload-2.1.2/js/common.js?id=4",
            //"/resources/dragsort/jquery.dragsort-0.5.1.min.js",
        ];

        $file_html = '';
        foreach($file as $f){
            $file_html .= "<script type='text/javascript' src='{$f}'></script>";
        }
        
        //增加相关的调用代码
        $flash_swf_url = \yii::$app->request->hostinfo . "/resources/plupload-2.1.2/js/Moxie.swf";
        $silverlight_xap_url = \yii::$app->request->hostinfo . "/resources/plupload-2.1.2/js/Moxie.xap";        
        //加载编辑器所需JS，多编辑器字段防止重复加载
        if (!defined('EDITOR_INIT_UPLOADFILE')) {
            $str .= $file_html;
            
            $str .= "
                <script type='text/javascript'>
                var savePath = '{$savepath}';
                var deleteUrl = '{$delete_url}';
                var uploadUrl = '{$upload_url}';
                var flash_swf_url = '{$flash_swf_url}';
                var silverlight_xap_url = '{$silverlight_xap_url}';
                </script>
            ";            
            define('EDITOR_INIT_UPLOADFILE', 1);
        }
        


        if($is_more){
            $name = $name . "[]";
            $is_more = 'true';
        }else{
            $is_more = 'false';
        }
        if(!$callbackdata){
            $callbackdata = "''";
        }
        $str .= "
            <script type='text/javascript'>
                saveImg('{$id}', '{$name}', savePath, {$is_more},{$callbackdata});
            </script>
        ";
        return  $str;
    }


    
    //通过id，$model内容，$form来确认生成的html  百度编辑器
    public static function editor_element_detail($model,$name)
    {
        $label = $model->attributeLabels();
        $label = $label[$name];
        
        $script = static::editor_element($name, $model[$name]);
        $js = static::editor($name);
        
        $html = '
            <div class="form-group">
                <div class="col-xs-3 col-sm-2 text-right">
                    <label class="control-label">'.$label.'</label>
                </div>
                <div class="col-xs-6 col-sm-8">
                    '.$script.'
                    '.$js.'
                </div>
            </div>                  
                ';
        return $html;
    }    
    //通过id，$model内容，$form来确认生成的html  百度编辑器
    public static function editor_element_detail_new($name,$label,$value)
    {
        
        $script = static::editor_element($name, $value);
        $js = static::editor($name);
        
        $html = '
            <div class="form-group">
                <div class="col-xs-3 col-sm-2 text-right">
                    <label class="control-label">'.$label.'</label>
                </div>
                <div class="col-xs-6 col-sm-8">
                    '.$script.'
                    '.$js.'
                </div>
            </div>                  
                ';
        return $html;
    }
    
    
    public static function editor_element($id,$content)
    {
        return '<script type="text/plain" id="'.$id.'" name="'.$id.'">'.$content.'</script>';
    }
    

    /**
     * 编辑器
     * @param int $textareaid 字段名
     * @param int $toolbar 标准型 full 简洁型 basic
     * @param string $module 模块名称
     * @param int $catid 栏目id
     * @param boole $allowupload  是否允许上传
     * @param boole $allowbrowser 是否允许浏览文件
     * @param string $alowuploadexts 允许上传类型
     * @param string $allowuploadnum 每次允许上传的文件数量
     * @param string $height 编辑器高度
     */
    public static function editor($textareaid = 'content', $toolbar = 'basic', $height = 400, $allowupload = 1, $allowbrowser = 0, $alowuploadexts = '', $allowuploadnum = '10') 
    {
        
        $file = [ //加载相关的文件
            "/resources/UEditor/ueditor.config.js",
            "/resources/UEditor/ueditor.all.min.js",
        ];
        $server_url = "/ueditor/index";//处理上传的链接
        $file_html = '';
        foreach($file as $f){
            $file_html .= "<script type='text/javascript' src='{$f}'></script>";
        }
        
        
        $str = "";
        //加载编辑器所需JS，多编辑器字段防止重复加载
        if (!defined('EDITOR_INIT')) {
            $str  = $file_html;
            define('EDITOR_INIT', 1);
        }
        
        //通过参数传送的
        $time = time();
        $uid = \yii::$app->controller->user->uid;
        $authkey = md5(\common\components\GuestInfo::getParam('authcode') . $time . $uid);//验证
        //编辑器类型
        if ($toolbar == 'basic') {//简洁型
            $toolbar = "['FullScreen', 'Source', '|', 'Undo', 'Redo', '|','FontSize','Bold', 'forecolor', 'Italic', 'Underline', 'Link',  '|',  'InsertImage', 
                 'ClearDoc',  'CheckImage','Emotion',  " . ($allowupload && $allowbrowser ? "'attachment'," : "") . " 'PageBreak','insertcode', 'WordImage','RemoveFormat', 'FormatMatch','AutoTypeSet']
                ";
        } elseif ($toolbar == 'full') {//标准型
            $toolbar = "[
            'fullscreen', 'source', '|', 'undo', 'redo', '|',
            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
            'directionalityltr', 'directionalityrtl', 'indent', '|',
            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
            'simpleupload', 'insertimage', 'emotion', 'scrawl', 'insertvideo', 'music', 'attachment', 'map', 'gmap', 'insertframe', 'insertcode', 'webapp', 'pagebreak', 'template', 'background', '|',
            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
            'print', 'preview', 'searchreplace', 'help', 'drafts'
            ]";
        }elseif($toolbar=='header'){
        	$toolbar="['InsertImage']";
        	$save_header='member/header_img';
        	$server_url = "/ueditor/index?savepath=$save_header";//处理上传的链接
        }
        

        $str .= "\r\n<script type=\"text/javascript\">\r\n";
        $str .= " var editor{$textareaid} = UE.getEditor('{$textareaid}',{  
                            textarea:'$textareaid',
                            toolbars:[$toolbar],
                            autoHeightEnabled:false,
                            serverUrl:'{$server_url}'
                      });
                      editor{$textareaid}.ready(function(){
                            editor{$textareaid}.execCommand('serverparam', {
                                  'uid':'{$uid}',
                                  'time':'{$time}',
                                  'authkey':'$authkey',
                                  'allowupload':'{$allowupload}',
                                  'allowbrowser':'{$allowbrowser}',
                                  'alowuploadexts':'{$alowuploadexts}'
                             });
                             editor{$textareaid}.setHeight($height);
                      });
                      ";
        $str .= "\r\n</script>";
        return $str;
    }
    
    public static function getDataForSearch($name,$value,$label,$class)
    {
        $html = '';
        $html .= '
            <div class="form-group">
                <label class="control-label">'.$label.'</label>
                <input type="text" value="'.$value.'" class="form-control '.$class.'" name="'.$name.'">
            </div>
        ';  
        return $html;
    }     
    

    
    
    public static function getDataForForm($name,$value,$name1,$value1,$label,$class = '',$class1 = '')
    {
        $html = '';
        $html .= '
            <div class="form-group">
            <div class="col-xs-3 col-sm-2 text-right">
                <label class="control-label">'.$label.'</label>
            </div>
            <div class="col-xs-9 col-sm-10">
                <input style="display:inline-block" type="text" value="'.$value.'" class="form-control '.$class.'" name="'.$name.'"> 到
                <input style="display:inline-block" type="text" value="'.$value1.'" class="form-control '.$class1.'" name="'.$name1.'">
            </div>            
          
                
                
            </div>
        ';  
        return $html;
    }
    
    public  static function getDataForKey($title,$name,$value)
    {
        $datas = \common\models\RedKeyModel::getBaseKeyValue("red_key", "remark");
        $red = \common\components\Tool::getSelectData($datas, $name, ["class" => "form-control"], 1, $value);
        $html = "";
        $html .= '<div class="form-group">
                        <div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="">'.$title.'</label></div>
                        <div class="form-inline col-xs-6 col-sm-4">
                            送红包  
                            '.$red.'
                        </div>
                 </div>';
        return $html;
    }
    
    public  static function getDataForKey_extend($title,$name,$value,$name2,$chance)
    {
        $datas = \common\models\RedKeyModel::getBaseKeyValue("red_key", "remark");
        $red = \common\components\Tool::getSelectData($datas, $name, ["class" => "form-control"], 1, $value);
        $html = "";
        $html .= '<div class="form-group">
                        <div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="">'.$title.'</label></div>
                        <div class="form-inline col-xs-6 col-sm-3">
                            送红包
                            '.$red.'
                        </div>
                        <div class="form-inline col-xs-3 col-sm-6"><label class="control-label" for="">获取概率</label>
                                <input name="'.$name2.'" class="control-label text-center" type="text" value="'.$chance.'"/>
                        </div>
                 </div>';
        return $html;
    }
    /*
     * 获取红包类型HTML
     */
    public static function getDataForRedType($title,$name,$value,$label){
        $datas = \common\models\RedPacketTypeModel::getBaseKeyValue('id', 'name');
        $red = \common\components\Tool::getSelectData($datas, $name, ["class" => "form-control"], 1, $value);
        $html = "";
        $html .= '<div class="form-group">
                        <div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="">'.$title.'</label></div>
                        <div class="form-inline col-xs-6 col-sm-4">
                            '.$label.'
                            '.$red.'
                        </div>
                 </div>';
        return $html;
    }
    
     /*
     * 获取红包规则HTML
     */
    public static function getDataForRedRole($title,$name,$value,$label){
        $datas = \common\models\RedPacketGetRoleModel::getBaseKeyValue('id', 'title');
        $red = \common\components\Tool::getSelectData($datas, $name, ["class" => "form-control"], 1, $value);
        $html = "";
        $html .= '<div class="form-group">
                        <div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="">'.$title.'</label></div>
                        <div class="form-inline col-xs-6 col-sm-4">
                            '.$label.'
                            '.$red.'
                        </div>
                 </div>';
        return $html;
    }
    
    public static function getChushiData($model,$form,$field,$label,$name,$data,$base_name = "")
    {
        $chuli_data = $model[$field];
        $model[$field] = $data;
        if($base_name){
            $string_name = "{$base_name}[$name]";
        }else{
            $string_name = $name;
        }
        $d = $form->field($model, $field)->textInput(['name' => $string_name])->label($label);
        $model[$field] = $chuli_data;   
        return $d;
    }
    
    //多张或一张图片
    //$form 表单域
    //$model model的对象
    //$attribute 属性
    //$id 要用的ID
    //$hint 提示说明
    public static function getFileContentJietu($form,$model,$attribute,$id,$filepath,$hint='',$options = [],$callbackdata = '',$is_more = false,$upload_url = '',$delete_url = '')
    {
        $obj = $form->field($model, $attribute);
        $template = $obj->template;
        $img = $model[$attribute];
        $img_arr = json_decode($img,true);
        $str_image = '';
        if(is_array($img_arr)){//是数组
            $str_image = '<div id="div_img_'.$id.'">';
            foreach($img_arr as $dd){
                $str_image .= '
                    <div class="div_img">
                        <img src="'.$dd.'">
                        <input type="hidden" value="'.$dd.'" name="'.$attribute.'[]">
                        <div class="update uploader-close">
                        </div>
                    </div>
                ';                
            }
            $str_image .= '</div>';
        }else if($img){//是一个字符串
            $str_image = '
                <div class="div_img">
                    <img src="'.$img.'">
                    <input type="hidden" value="'.$img.'" name="'.$attribute.'">
                    <div class="update uploader-close">
                    </div>
                </div>
            ';
        }else{//没有内容
            $str_image = '';
        }
        
        $hidden  = '<input type="hidden"  name="'.$attribute.'_img_jietu_[x1]"autocomplete="off" />';
        $hidden .= '<input type="hidden"  name="'.$attribute.'_img_jietu_[y1]"autocomplete="off" />';
        $hidden .= '<input type="hidden"  name="'.$attribute.'_img_jietu_[x2]"autocomplete="off" />';
        $hidden .= '<input type="hidden"  name="'.$attribute.'_img_jietu_[y2]"autocomplete="off" />';
        $hidden .= '<input type="hidden"  name="'.$attribute.'_img_jietu_[w1]"autocomplete="off" />';
        $hidden .= '<input type="hidden"  name="'.$attribute.'_img_jietu_[h1]"autocomplete="off" />';
        
        $input = \yii\helpers\Html::button('上传文件',['id' =>$id,'class' => 'btn']);
        $label = $model->attributeLabels();
        $label = $label[$attribute];
        
        $html = '<div class="form-group">
                    <div class="col-xs-3 col-sm-2 text-right">
                        <label class="control-label">'.$label.'</label>
                    </div>
                    <div class="col-xs-9 col-sm-7">
                        '.$input.'
                        '.$str_image.'
                        '.$hidden.'
                    </div>
                </div>';
        
        $html .= '    <script>
        function '.$attribute.'_callback(obj)
        {
            
            var oImage = $("#'.$id.'").next(".div_img").find("img")[0];
            oImage.onload = function() {
                    console.log(oImage.naturalWidth);
                    console.log(oImage.naturalHeight);
                    
                    var obj = {
                      onSelect: updateCoords,
                      onChange: updateCoords,
                      onRelease: clearInfo
                    };                    
                    $("#'.$id.'").next(".div_img").css("width",oImage.naturalWidth).css("height",oImage.naturalHeight);
                        
                    $("#'.$id.'").next(".div_img").find("img").eq(0).Jcrop({
                        minSize: [32, 32], // min crop size
                        aspectRatio: 1, // keep aspect ratio 1:1
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onSelect: updateCoords,
                        onChange: updateCoords,
                        onRelease: clearInfo
                    }, function() {


                    });

                }
 




            function updateCoords(e)
            {
                $("[name=\''.$attribute.'_img_jietu_[x1]\']").val(e.x);
                $("[name=\''.$attribute.'_img_jietu_[y1]\']").val(e.y);
                $("[name=\''.$attribute.'_img_jietu_[x2]\']").val(e.x2);
                $("[name=\''.$attribute.'_img_jietu_[y2]\']").val(e.y2);
                $("[name=\''.$attribute.'_img_jietu_[w1]\']").val(e.w);
                $("[name=\''.$attribute.'_img_jietu_[h1]\']").val(e.h);
            };
            function clearInfo() {
                $("[name=\''.$attribute.'_img_jietu_[x1]\']").val("");
                $("[name=\''.$attribute.'_img_jietu_[y1]\']").val("");
                $("[name=\''.$attribute.'_img_jietu_[x2]\']").val("");
                $("[name=\''.$attribute.'_img_jietu_[y2]\']").val("");
                $("[name=\''.$attribute.'_img_jietu_[w1]\']").val("");
                $("[name=\''.$attribute.'_img_jietu_[h1]\']").val("");
            }            
        }

    </script>';
        
        
        $html .= static::uploadfile($id,$attribute,$filepath,$attribute . "_callback",$is_more,$upload_url,$delete_url);
        
        return $html;
    }
    
    /**
     * 获取我们所需要的select的数据 
     * @param type $datas  需要的数据
     * @param type $name select 的name名称
     * @param type $option 附带的一些参数
     * @param type $is_empty  是否是需要请选择这样的
     * @param type $select_data  目前选的一个数据
     */
    public static function getSelectData($datas,$name,$option = [],$is_empty = false,$select_data = '',$empty_data = "请选择...")
    {
        $option_string = '';
        foreach($option as  $key => $value){
            $option_string .= " {$key} = '{$value}'";
        }
        $html = "<select name='{$name}' {$option_string}>";
        
        if($is_empty){
            $html .= "<option value=''>{$empty_data}</option>";
        }
        foreach($datas as $key => $value){
            if($key == $select_data){
                $select = "selected";
            }else{
                $select = '';
            }
            $html .= "<option {$select} value='{$key}'>{$value}</option>";
        }
        
        $html .= "</select>";
        return $html;
    }     
}
