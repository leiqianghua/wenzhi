<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//访问的一些基本的数据信息
namespace common\components;
class GuestInfo
{
    /**
     * @name 获取浏览器名称
     * @return String
     */
    static public function getBrowser() {
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $br = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/MSIE/i', $br)) {
                $br = 'MSIE';
            } elseif (preg_match('/Firefox/i', $br)) {
                $br = 'Firefox';
            } elseif (preg_match('/Chrome/i', $br)) {
                $br = 'Chrome';
            } elseif (preg_match('/Safari/i', $br)) {
                $br = 'Safari';
            } elseif (preg_match('/Opera/i', $br)) {
                $br = 'Opera';
            } else {
                $br = 'Other';
            }
        }
        return $br;
    }

    /**
     * @name 获取IP地址
     * @return String
     */
    static public function getIp() {
        if (getenv("HTTP_CLIENT_IP")) {
            $ip = getenv("HTTP_CLIENT_IP");
        } elseif (getenv("HTTP_X_FORWARDED_FOR")) {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        } elseif (getenv("REMOTE_ADDR")) {
            $ip = getenv("REMOTE_ADDR");
        } else {
            $ip = "127.0.0.1";
        }
        return $ip;
    }
    //负载均衡方式获取到IP
    static public function getIpForLvs()
    {
        //阿里云负载均衡获取IP
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
            $ip = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = trim($ip[0]);
            return $ip ? $ip : '127.0.0.1';
        }
        //传统获取IP方法
        if (@$_SERVER['HTTP_CLIENT_IP'] && $_SERVER['HTTP_CLIENT_IP'] != 'unknown') {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (@$_SERVER['HTTP_X_FORWARDED_FOR'] && $_SERVER['HTTP_X_FORWARDED_FOR'] != 'unknown') {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return preg_match('/^\d[\d.]+\d$/', $ip) ? $ip : '127.0.0.1';
    }    
    

    /**
     * @name 获取字符类型
     * @return String
     */
    static public function getLang() {
        if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            $lang = substr($lang, 0, 5);
            if (preg_match("/zh-cn/i", $lang)) {
                $lang = "简体中文";
            } elseif (preg_match("/zh/i", $lang)) {
                $lang = "繁体中文";
            } else {
                $lang = "English";
            }
            return $lang;
        }
    }

    /**
     * @name 获取操作系统名称
     * @return String
     */
    static public function getOs() {
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $OS = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/win/i', $OS)) {
                $OS = 'Windows';
            } elseif (preg_match('/mac/i', $OS)) {
                $OS = 'MAC';
            } elseif (preg_match('/linux/i', $OS)) {
                $OS = 'Linux';
            } elseif (preg_match('/unix/i', $OS)) {
                $OS = 'Unix';
            } elseif (preg_match('/bsd/i', $OS)) {
                $OS = 'BSD';
            } else {
                $OS = '未知';
            }
            return $OS;
        }
    }

    /**
     * @name 获取访客物理地址
     * @return String
     */
    static public function getAddr() {
        $ip = self::Ip();
        if ($ip == "127.0.0.1") {
            $add = "未知地址";
        } else {
            //根据新浪api接口获取
            if ($ipadd = @file_get_contents('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=' . $ip)) {
                $ipadd = json_decode($ipadd);
                if (!is_object($ipadd) or $ipadd->ret == '-1') {
                    $add = '未知地址';
                } elseif (is_object($ipadd) and $ipadd->ret <> '-1') {
                    $add = $ipadd->province . $ipadd->isp;
                } else {
                    $add = '未知地址';
                }
            } else {
                $add = '未知地址';
            }
        }
        return $add;
    }

    /**
     * @name 获取访客来源地址
     * @return String
     */
    static public function getReferer() {
        $Urs = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : '';
        return $Urs;
    }

    /**
     * @name 获取访客GET查询
     * @return String
     */
    static public function getQuery() {
        if (isset($_SERVER["QUERY_STRING"])) {
            $uquery = addslashes($_SERVER["QUERY_STRING"]);
        } else {
            $uquery = '';
        }
        return $uquery;
    }

    /**
     * @name 获取访客终端综合信息
     * @return String
     */
    static public function getAgent() {
        if (isset($_SERVER["HTTP_USER_AGENT"])) {
            $uagent = addslashes($_SERVER["HTTP_USER_AGENT"]);
        } else {
            $uagent = "未知终端";
        }
        return $uagent;
    }

    /**
     * 是否手机访问
     * @return boolean
     */
    static public function isMobileRequest() {
        $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';
        $mobile_browser = '0';
        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
            $mobile_browser++;
        if ((isset($_SERVER['HTTP_ACCEPT'])) and ( strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') !== false))
            $mobile_browser++;
        if (isset($_SERVER['HTTP_X_WAP_PROFILE']))
            $mobile_browser++;
        if (isset($_SERVER['HTTP_PROFILE']))
            $mobile_browser++;
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
            'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
            'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
            'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
            'newt', 'noki', 'oper', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
            'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
            'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
            'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
            'wapr', 'webc', 'winw', 'winw', 'xda', 'xda-'
        );
        if (in_array($mobile_ua, $mobile_agents))
            $mobile_browser++;
        if (strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)
            $mobile_browser++;
        // Pre-final check to reset everything if the user is on Windows   
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false)
            $mobile_browser = 0;
        // But WP7 is also Windows, with a slightly different characteristic   
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false)
            $mobile_browser++;
        if ($mobile_browser > 0)
            return true;
        else
            return false;
    }



    /**
     * 判断是否微信内置浏览器访问
     * @return boolean
     */
    static public function isWeixinRequest() {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($userAgent, 'MicroMessenger') || strpos(strtolower($userAgent), 'micromessenger')) {
            return true;
        } else {
            return false;
        }
    }
    
    //获取用户点击的设备 1为安卓设备  2为IOS设备
    public static function equipment()
    {
        if (stripos($_SERVER['HTTP_USER_AGENT'], 'iphone') === false) {//安卓
            return 1;
        } else {//iPhone
            return 2;
        }        
    }
    
    public static function getParam($name,$defaultValue='')
    {
        return isset($_GET[$name]) ? $_GET[$name] : (isset($_POST[$name]) ? $_POST[$name] : $defaultValue);
    }
    
    /**
     * 判断是否蜘蛛访问
     * @return boolean true是蜘蛛
     */
    public static  function isSpider() {
        $kw_spiders = 'Bot|Crawl|Spider|slurp|sohu-search|lycos|robozilla|360Spider';
        $kw_browsers = 'MSIE|Netscape|Opera|Konqueror|Mozilla|360User-agent';
        $spider = false;
        if (!strpos($_SERVER['HTTP_USER_AGENT'], 'http://') && preg_match("/($kw_browsers)/i", $_SERVER['HTTP_USER_AGENT'])) {
            $spider = false;
        } elseif (preg_match("/($kw_spiders)/i", $_SERVER['HTTP_USER_AGENT'])) {
            $spider = true;
        } else {
            $spider = false;
        }
        return $spider;
    }
    
    public static function setData($value,$name = 'showdata')
    {
        \yii::$app->session->open();
        $_SESSION[$name] = $value;
    }    
    
    public static function  getData($name = 'showdata')
    {
        \yii::$app->session->open();
        if(empty($_SESSION[$name])){
            return '';
        }
        $data = $_SESSION[$name];
        unset($_SESSION[$name]);
        return $data;
    }    
    
    //如果本身的url有带nexturl参数，则用这个
    //如果 保存了上一页的一个链接 $_SESSION['url'] 则用这个
    // 不满足上面的条件 话则用  上一页的链接
    public static function getCallbackUrl()
    {
        if(isset($_GET['nexturl'])){
            return $_GET['nexturl'];
        }else if($url = static::getData('url')){
            return $url;
        }else if(!empty($_SERVER['HTTP_REFERER'])){
            return $_SERVER['HTTP_REFERER'];
        }else{
            return \yii::$app->controller->to('/');//跳到首页
        }
    }
    
    /*根据ip获取省市  */
    /***********************************
     **
     **          ip地址获取
     **
     ************************************/
    public static function get_ip_dizhi_bak($ip)
    {
        if($ip){
            $QQWry = new qqwry();
            $ifErr = $QQWry->QQWry($ip);
            $res=$QQWry->Country;
            if($res){
                $res= iconv("gb2312", "utf-8//TRANSLIT//IGNORE", $res);
                
                $province=strrpos($res,'省');
                $length=strlen($res);
                if($province){
                    $arr['province']=substr($res,0,$province).'省';
                    if($province){
                        $arr['city']=mb_substr($res,$province/2,$length,'UTF-8');
                    }
                }else{
                    $arr['province']=$res;
                }
                return $arr;
            }else{
                return '';
            }
        }else{
            return '';
        }


    } 
    
    
    public static function get_ip_dizhi($ip)
    {
        $url="http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
        //print_r(file_get_contents($url));
        $ipinfo=@json_decode(file_get_contents($url),true); 
        if($ipinfo['code']=='1'){
            return false;
        }
        //$city = $ipinfo->data->region.$ipinfo->data->city;
        $ipinfo['data']['province'] = isset($ipinfo['data']['region']) ? $ipinfo['data']['region'] : "";
        $ipinfo['data']['city'] = isset($ipinfo['data']['city']) ? $ipinfo['data']['city'] : "";
        return $ipinfo['data']; 
    }    
    
    
    //通过IP来确认其是属于哪里的
    public static function getDataForIp($ip = '')
    {
        if(!$ip){
            $ip = \common\components\GuestInfo::getIp();
        }
        return \common\components\GuestInfo::get_ip_dizhi($ip);
    }
    
    public static function getIDForName($name)
    {
        require_once \yii::getAlias('@backend') . "/config/area.php";
        foreach($area_array as $keys => $values){
            foreach($values as $key => $value){
                if($value[1] == $name){
                    return $value[0];
                }
            }
        }
    }    
    public static function getNameForId($name)
    {
        require_once \yii::getAlias('@backend') . "/config/area.php";
        foreach($area_array as $keys => $values){
            foreach($values as $key => $value){
                if($value[0] == $name){
                    return $value[1];
                }
            }
        }
    }
    
}