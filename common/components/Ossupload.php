<?php
namespace common\components;
class Ossupload
{
    public $config = [
        'accessKeyId' => 'iviBmloY3qSE1pub',
        'accessKeySecret' => 'FmE2LTwDkpj3uyuGwaf23Gm1Cw7Siz',
        'endpoint' => 'oss-cn-qingdao.aliyuncs.com',
        'is_cname' => false,
    ];
    public $client = null;
    public $error;
    public $bucket = 'gubatu';
    
    public function __construct($config = [])
    {
        $this->config = array_merge($this->config,$config);
        $obj = new \OSS\OssClient($this->config['accessKeyId'], $this->config['accessKeySecret'], $this->config['endpoint'],$this->config['is_cname']);
        $this->client = $obj;
    }
    
    //创建bucket
    public function createBucket($bucket)
    {  
        try {
            $this->client->createBucket($bucket);
        } catch (\OSS\Core\OssException $e) {
            $this->error = $e->getMessage();
            return false;
        }        
    }
    /*
     * $savefile  'D:/ab.txt'; //本地文件  用户上传的路径文件也是一样的可以的。
     * $loadfile "aaa/ab.txt";  //上传到  OSS的路径
     */
    public function uploadFile($loadfile, $savefile)
    {
        try {
            $this->client->uploadFile($this->bucket, $loadfile, $savefile);
            return true;
        } catch (\OSS\Core\OssException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    

    //字节流的上传方式
    //$loadfile 你需要上传到阿里云的这个用户名称
    //$savefile  字节流字符串
    public function putObject($loadfile, $savefile)
    {
        try {
            
            $options = array(\OSS\OssClient::OSS_HEADERS => array(
                'x-oss-meta-self-define-title2' => 'user define meta info',
            ));            
            
            $this->client->putObject($this->bucket, $loadfile, $savefile,$options);
            return true;
        } catch (\OSS\Core\OssException $e) {
            $this->error = $e->getMessage();
            return false;;
        }
    }    
    
    //删除的路径
    public function deleteObject($object)
    {
        try {
            $this->client->deleteObject($this->bucket, $object);
            return true;
        } catch (\OSS\Core\OssException $e) {
            $this->error = $e->getMessage();
            return false;
        }        
    }
    
    //$filename需要保存的路径  $data这是需要保存的字符
    function saveData($filename,$data)
    {
        if(is_array($data)){
            $data = json_encode($data);
        }
        $data = date('Y-m-d H:i:s') . "----" . $data;
        $handle = fopen($filename, 'a');
        fwrite($handle, $data . "\n");
        fclose($handle);
    }
    
}