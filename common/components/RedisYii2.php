<?php

namespace common\components;
use yii\base\Object;
use \Exception;
use Redis;

class RedisYii2 extends Object {


    public $host = '127.0.0.1';
    public $port = 6379;
    private $_linkHandle;

    public function init()
    {
        parent::init();
        $this->connect();
    }
    
    public function __call($name, $params)
    {
        $redis = $this->getRedis();
        if(method_exists($redis,$name)){
            return call_user_func_array([$redis, $name], $params);
        }else{
            return false;
        }
    }

    /**
     * 连接服务器,注意：这里使用长连接，提高效率，但不会自动关闭
     */
    public function connect()
    {
        // 设置 Master 连接
        $this->_linkHandle = new Redis();
        $ret = $this->_linkHandle->pconnect($this->host, $this->port);
        return $ret;
    }

    /**
     * 关闭连接
     * @return boolean
     */
    public function close()
    {
        $this->getRedis()->close();
        return true;
    }

    /**
     * 得到 Redis 原始对象可以有更多的操作
     */
    public function getRedis() {
        return $this->_linkHandle;
    }

    /**
     * 写缓存
     *
     * @param string $key 组存KEY
     * @param string $value 缓存值
     * @param int $expire 过期时间， 0:表示无过期时间
     */
    public function set($key, $value, $expire = 300)
    {
        // 永不超时
        $value = json_encode($value);
        if ($expire == 0) {
            $ret = $this->getRedis()->set($key, $value);
        } else {
            $ret = $this->getRedis()->setex($key, $expire, $value);
        }
        return $ret;
    }

    /**
     * 读缓存
     *
     * @param string $key 缓存KEY,支持一次取多个 $key = array('key1','key2')
     * @return string || boolean  失败返回 false, 成功返回字符串
     */
    public function get($key)
    {
        // 是否一次取多个值
        $func = is_array($key) ? 'mGet' : 'get';
        $data =  $this->getRedis()->{$func}($key);
        if(is_array($key)){
            foreach($data as $k => $v){
                $data[$k] = json_decode($v,true);
            }
        }else{
            $data = json_decode($data,true);
        }
        return $data;
    }

    /**
     * 条件形式设置缓存，如果 key 不存时就设置，存在时设置失败
     *
     * @param string $key 缓存KEY
     * @param string $value 缓存值
     * @return boolean
     */
    public function setnx($key, $value)
    {
        return $this->getRedis()->setnx($key, $value);
    }

    /**
     * 删除缓存
     *
     * @param string || array $key 缓存KEY，支持单个健:"key1" 或多个健:array('key1','key2')
     * @return int 删除的健的数量
     */
    public function remove($key)
    {
        // $key => "key1" || array('key1','key2')
        return $this->getRedis()->delete($key);
    }

    /**
     * 数据入队列 
     * @param string $key KEY名称 
     * @param string|array $value 获取得到的数据 
     * @param bool $right 是否从右边开始入 
     */
    public function push($key, $value, $right = true)
    {
        $value = json_encode($value);
        return $right ? $this->getRedis()->rPush($key, $value) : $this->getRedis()->lPush($key, $value);
    }

    /**
     * 数据出队列 
     * @param string $key KEY名称 
     * @param bool $left 是否从左边开始出数据 
     */
    public function pop($key, $left = true)
    {
        $val = $left ? $this->getRedis()->lPop($key) : $this->getRedis()->rPop($key);
        return json_decode($val, true);
    }

    /**
     * 值加加操作,类似 ++$i ,如果 key 不存在时自动设置为 0 后进行加加操作
     *
     * @param string $key 缓存KEY
     * @param int $default 操作时的默认值
     * @return int　操作后的值
     */
    public function incr($key, $default = 1)
    {
        if ($default == 1) {
            return $this->getRedis()->incr($key);
        } else {
            return $this->getRedis()->incrBy($key, $default);
        }
    }

    /**
     * 值减减操作,类似 --$i ,如果 key 不存在时自动设置为 0 后进行减减操作
     *
     * @param string $key 缓存KEY
     * @param int $default 操作时的默认值
     * @return int　操作后的值
     */
    public function decr($key, $default = 1)
    {
        if ($default == 1) {
            return $this->getRedis()->decr($key);
        } else {
            return $this->getRedis()->decrBy($key, $default);
        }
    }

    /**
     * 添空当前数据库
     *
     * @return boolean
     */
    public function clear()
    {
        return $this->getRedis()->flushDB();
    }



    public function isConnect()
    {
        try {
            return $this->getRedis()->ping();
        } catch (Exception $e) {
            return false;
        }
    }

    //如果没有连就停止执行
    public function isConnectThrow()
     {
        try {
            return $this->getRedis()->ping();
        } catch (Exception $e) {
            throw new \yii\base\UserException("没有连接redis,程序停止执行");            
        }
    }
}
