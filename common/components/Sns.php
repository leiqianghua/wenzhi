<?php
namespace common\components;
use Yii;
use \yii\base\Component;
use \nusoap_client;
class Sns extends Component{
	public static $client = null;
	protected $error = null;
	private $parameters = array(
		'userId'=>'J02251',
		'password'=>'327906',
		'pszMobis'=>'',
		'pszMsg'=>'',
		'iMobiCount'=>1,
		'pszSubPort'=>1,		
	);

	public function init(){
		parent::init();
		require_once(__DIR__ . '/nusoap/lib/nusoap.php');
		if(empty(self::$client)){
			self::$client=new nusoap_client('http://61.145.229.29:9003/MWGate/wmgw.asmx?wsdl');	
			self::$client->soap_defencoding='gb2312';
			self::$client->decode_utf8=true;			
		}		
	}
	public function getError(){
		return $this->error;
	}
	/**
	 * 发送内容
	 * @param  [type] $phone   [description]
	 * @param  [type] $content [description]
	 * @return [type]          [description]
	 */
	public function send($phone, $content){
		$this->parameters['pszMsg'] = $content;
		$this->parameters['pszMobis'] = $phone;
		$aryResult=self::$client->call('MongateCsSpSendSmsNew',$this->parameters);
		if($err = self::$client->getError()){
			$this->error = $err;
			return false;
		}else{
			return true;
		}	
	}
}