<?php
//模板消息
namespace common\components;


use yii;
class TemplateMessage
{
    public $uid;//哪一个用户
    public $error;
    

    public function getOauthData()
    {
        $oauth = \common\models\MemberOauthDataModel::find()->where(['uid' => $this->uid,'oauth_type' => 'weixin'])->asArray()->one();
        if($oauth){
            $oauth = \common\models\MemberOauthWeixinModel::find()->where(['unionid' => $oauth['oauth_key']])->asArray()->one();
            if($oauth){
                return $oauth['openid'];
            }
        }
    }

    public function getAccess_token()
    {
        $access_token = \yii::$app->cache->get("access_token");
        if(!$access_token){
            $obj = new WeixinPublicNumberBase();
            $arr = $obj->getAccess_token();
            if(is_array($arr)){
                $access_token = $arr['access_token'];
                $expires_in = $arr['expires_in'];
                $expires_in = $expires_in - 60;
                \yii::$app->cache->set("access_token",$access_token,$expires_in);
            }
        }
        return $access_token;
    }
    
    /*
     * $params 的格式参考下面的这个注释的data
     */
    public function buy_notice($params,$my_url = '',$template_id = 'r9N_wVBHrEr4wW8_ZUZBmxxkkHX2P6lk8fMENSio6ck')
    {

        
        $oauth = $this->getOauthData();
        if(!$oauth){
            $this->error = "用法没有绑定微信或找不到相关的token";
            TmpLog::addData("notice_error", $this->error);
            return false;
        }        
        
        
        $access_token = $this->getAccess_token();
        if(!$access_token){
            $this->error = "找不到相关的access_token";
            TmpLog::addData("notice_error", $this->error);
            return false;            
        }
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $access_token;
        $curl = new Curl();
        
//        $data = [
//                "first" => [
//                    "value" => "sdf",
//                    "color" => "#FF0000",
//                ],
//                "product" => [
//                    "value" => "sdf",
//                    "color" => "#FF0000",
//                ],
//                "price" => [
//                    "value" => "5.56",
//                    "color" => "#FF0000",
//                ],
//                "time" => [
//                    'value' => '2015/10/5 14:00~14:45',
//                    "color" => "#FF0000",
//                ],
//                "remark" => [
//                    "value" => "sdf",
//                    "color" => "#FF0000",
//                ],
//        ];
        $data = $params;
        //$data = json_encode($data);
        //print_r($data);exit;
        $post = [
            'touser' => $oauth,
            'template_id' => $template_id,
            //'url' => "www.baidu.com",
            'data' => $data,
        ];
        if($my_url){
            $post['url'] = $my_url;
        }
        $post = urldecode(json_encode($post));
        $result = $curl->post($url, $post);
        
        $result_arr = json_decode($result,true);
        if(is_array($result_arr) && $result_arr['errcode'] == 0){
            return true;
        }else{
            TmpLog::addData("微信通知", $result);
            $this->error = $result;
            return false;
        }
    }
}