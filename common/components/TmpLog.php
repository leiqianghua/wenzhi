<?php
//打印日志
namespace common\components;


use yii;
class TmpLog
{
    //$filename需要保存的路径  $data这是需要保存的字符
    public  static function saveData($filename,$data)
    {
        if(is_array($data)){
            $data = json_encode($data);
        }
        $data = date('Y-m-d H:i:s') . "----" . $data;
        $handle = fopen($filename, 'a');
        fwrite($handle, $data . "\n");
        fclose($handle);
    }
    
    
    //$path 为相关的文件夹（不是文件） dirname($path) 为取文件夹
    public static function  createDir($path)
    {
        if (!file_exists($path)){ 
            static::createDir(dirname($path)); 
            mkdir($path, 0777); 
        } 
    }     
    //收集一些错误并保存
    
    public static function addData($type,$remark = '',$str_num = "zidong")
    {
        if(!$str_num){
            $str_num = "zidong";
        }
        \common\models\ErrorLogModel::$table_num = $str_num;
        $model = new \common\models\ErrorLogModel();
        $data['url'] = '';
        if($str_num == 'swoole'){
            $data['url'] = '';
        }else{
            if(!empty($_SERVER['HTTP_HOST']) && !empty($_SERVER['REQUEST_URI'])){
                $data['url'] = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                $data['post'] = json_encode($_POST);
            }else{
                $data['url'] = '';
            }
        }
        if(is_array($remark)){
            $remark = json_encode($remark);
        }
        $data['remark'] = $remark;
        $data['type'] = $type;
        $data['add_date'] = date('Y-m-d H:i:s');
        $model->attributes = $data;
        $model->save(false);
    }
    
}