<?php
//一些工具
namespace common\components;


use yii;
class Tool
{
    //获取锁 如果获取不到则直接返回
    public static function checkLock($cache_key,$extime = 90,$random = 1)
    {
        return true;
//        $memcache = yii::$app->cache->getMemcache();
//        return $memcache->add($cache_key,1,false,$extime);
        
        
        $redis = \yii::$app->redis;
        $redis = $redis->getRedis();
        $state = $redis->set($cache_key,$random,array('nx', 'ex' => $extime));        
        if($state){//这个东西有值  说明拿到了锁
            return true;
        }else{
            return false;
        }        
    }
    
    //获取锁，如果第一时间获取不到则等待，直到获取到为止
    public static function addLock($cache_key,$extime = 90,$random = 1)
    {
       return true;
//        $memcache = yii::$app->cache->getMemcache();
//        while (true) {
//            $state = $memcache->add($cache_key,1,false,$extime);
//            if($state){
//                break;
//            }else{
//                sleep(1);
//            }
//        }
//        return true;
        
        
        $redis = \yii::$app->redis;
        $redis = $redis->getRedis();
        while (true) {
            $state = $redis->set($cache_key,$random,array('nx', 'ex' => $extime));       
            if($state){
                break;
            }
        }
        return true;        
    }    
    
    //释放锁
    public static function deleteLock($cache_key,$random = 1)
    {
        return true;
//        $memcache = yii::$app->cache->getMemcache();
//        return $memcache->delete($cache_key);
        
        
        $redis = \yii::$app->redis;
        $redis = $redis->getRedis();
        if($redis->get($cache_key) == $random){
            $redis->del($cache_key);
        }        
    }
    
    /**
     * 获取我们所需要的select的数据 
     * @param type $datas  需要的数据
     * @param type $name select 的name名称
     * @param type $option 附带的一些参数
     * @param type $is_empty  是否是需要请选择这样的
     * @param type $select_data  目前选的一个数据
     */
    public static function getSelectData($datas,$name,$option = [],$is_empty = false,$select_data = '',$empty_data = "请选择...")
    {
        $option_string = '';
        foreach($option as  $key => $value){
            $option_string .= " {$key} = '{$value}'";
        }
        $html = "<select name='{$name}' {$option_string}>";
        
        if($is_empty){
            $html .= "<option value=''>{$empty_data}</option>";
        }
        foreach($datas as $key => $value){
            if($key == $select_data){
                $select = "selected";
            }else{
                $select = '';
            }
            $html .= "<option {$select} value='{$key}'>{$value}</option>";
        }
        
        $html .= "</select>";
        return $html;
    }
    
    public static function memory_usage() { 
        $memory     = ( ! function_exists('memory_get_usage')) ? '0' : round(memory_get_usage()/1024/1024, 2).'MB'; 
        return $memory; 
    } 
    
    
    public static function caleMoney($config)
    {
        if(!is_array($config)){
            $config = json_decode($config,true);
        }
        if(empty($config)){
            return false;
        }
        $odds = 0;
        $rand = rand(1, 100);
        foreach($config as $key => $value){
            $odds = $odds + $value['odds'];
            if($rand <= $odds){//在这里中了这个
                $min_money = $value['min_money'];
                $max_money = $value['max_money'];
                $money = rand($min_money, $max_money);
                return $money;
            }
        }
        return false;
    }
    
    public static function chaifenRed($config,$red_key,$uid)
    {
        if(is_array($config)){
            $money = \common\components\Tool::caleMoney($config);
        }else{
            $money = $config;
        }
        
        $money1 = $money2 = 0;
        if($money%2){
            $money2 = (int)($money/2);
            $money1 = $money2 + 1;
        }else{
            $money1 = $money2 = $money/2;
        }
        \common\models\ActivitySendRedModel::sendRedSave($red_key,$uid, 4,  1,$money1);
        \common\models\ActivitySendRedModel::sendRedSave($red_key,$uid, 4,  2,$money2);
    }
    
    //获取多个的一个东西。
    //$common_data_file 里面的文件包含的两个东西   一个是 $value其具体的这一项的值。  一个是 base_name(外面的基本的name)
    /**
     * 在这个comman_data_file里面  我们可以直接用的值 是  value里面的具体的一项一项的内容  还有一个是base_name这个就是具体的这个name的值。
     * @param type $name  具体的属性名称外层的
     * @param type $common_data_file  里面需要写的具体的这个文件的名称
     * @param type $content  具体的我们需要用的内容
     * @param type $label  label需要的值
     * @param type $other_data 其它的一些具体的值
     * @return type
     */
    public static function getDataForViewList($name,$common_data_file,$content,$label = '',$other_data = [])
    {
        $view = \yii::$app->view;
        return $view->render("/common/_content_new",["_common_data" => $common_data_file,'label' => $label,'name' => $name,"content" => $content,"other_data" => $other_data]);
    }
    
    /**
     * 判断一个变量  是否是  整型的数字。
     * @param type $data 判断的数据
     * @param type $min 是否是需要最小值的一个判断，值不能小于这个
     * @return boolean true/false
     */
    public static function is_int_ok($data,$min = '')
    {
        if($min !== ''){
            if($data < $min){
                return false;
            }
        }
        if(is_numeric($data) && is_int($data+0)){
            return true;
        }
    } 
    
    /**
     * 自动拼接url
     *   */
    public static function getFormatUrl($url='',$param=[])
    {
        if($url){
            if(!is_array($param)){
                $param=explode(',',$param);
            }
            if($param){
                $re_url=$url.'?';
                foreach ($param as $v){
                    $params=\common\components\GuestInfo::getParam($v);
                    if($params){
                        $re_url.=$v.'='.$params.'&';
                    }
                }
                $re_url=substr($re_url,0,-1);
                return $re_url;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /* 手机号保密处理 */
    
//    public static function hidtel_bak($phone) {
//        // $IsWhat = preg_match('/(0[0-9]{2,3}[\-]?[2-9][0-9]{6,7}[\-]?[0-9]?)/i',$phone); //固定电话
//        // if($IsWhat == 1){
//        // return preg_replace('/(0[0-9]{2,3}[\-]?[2-9])[0-9]{3,4}([0-9]{3}[\-]?[0-9]?)/i','$1****$2',$phone);
//        // }else{
//        $temp = preg_replace('/(1[35678]{1}[0-9])[0-9]{4}([0-9]{4})/i', '$1****$2', $phone);
//        return str_replace(array(
//            "\r\n",
//            "\r",
//            "\n"
//        ), "", $temp);
//        // }
//    }
    
    
    public static function hidtel($phone,$qian = 2,$zhong = 7,$hou = 2) {
        if(strlen($phone) != 11){
            return $phone;
        }
        
        if(!$qian || !$zhong || !$hou){
            return $phone;
        }
        if(($qian + $zhong + $hou) != 11){
            return $phone;
        }
        if($qian > 1){
            $qian_1 = 1;
            $qian_2 = $qian - 2;            
        }else{
            $qian_1 = $qian_2 = 0;
        }

        
        $preg = "/(1[345678]{{$qian_1}}[0-9\*]{{$qian_2}})[0-9\*]{{$zhong}}([0-9\*]{{$hou}})/i";
        
        $xin = '';
        for($i = 0;$i<$zhong;$i++){
            $xin .= "*";
        }
        $temp = preg_replace($preg, '$1'.$xin.'$2', $phone);
        return str_replace(array(
            "\r\n",
            "\r",
            "\n"
        ), "", $temp);
    }
    
    
    public static function operation_method($cache_key,$operation,$params = [])
    {
        $result = [];
        try {
            $state = \common\components\Tool::checkLock($cache_key);
            if (!$state) {
                $result['status'] = 0;
                $result['info'] = "正在处理";
                return $result;
            }
            $transaction = Yii::$app->db->beginTransaction(); //开启事务  
            
            
            $state = call_user_func($operation,$params);
            if(!$state){
                throw new Exception("出现异常");
            }
            
            
            \common\components\Tool::deleteLock($cache_key); //释放锁
            $transaction->commit(); //释放事务
            $result['status'] = 1;
            $result['info'] = $state;
            return $result;        
        } catch (\Exception $exc) {
            static::$error = $exc->getMessage();
            \common\components\Tool::deleteLock($cache_key); //释放锁
            $transaction->rollBack(); //释放事务 
            $result['status'] = 0;
            $result['info'] = $exc->getMessage();
            return $result; 
        }        
    }
    
    //生成订单号
    public static function createOrderID($dingdanzhui='')
    {
        return $dingdanzhui.time().substr(microtime(),2,6).rand(0,9);
    } 
}