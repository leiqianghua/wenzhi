<?php
namespace common\components;
class Unit
{
    public static function U($a,$b = '')
    {
        $params = [];
        $params[0] = $a;
        if($b && is_array($b)){
            $params = array_merge($params,$b);
        }
        return \yii::$app->controller->to($params);
    }
}

