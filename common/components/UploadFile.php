<?php
//上传的目录
namespace common\components;
use common\components\Ossupload;
use Exception;
use common\models\AttachmentModel;
class UploadFile
{
    public $error;
    //通过字节流的方式保存文件
    //$loadfile 要保存的文件路径
    public function saveObject($loadfile, $savedata)
    {
        $obj = new Ossupload();
        $state = $obj->putObject($loadfile, $savedata);
        if($state){
            $data = [];
            $data['url'] = \yii::$app->params['file_url'] . "/" . $loadfile;
            $data['path'] = $this->getPathName($loadfile);
            $data['file'] = $this->getFileName($loadfile);
            $data['pathfile'] = $loadfile;
            $data['uid'] = \yii::$app->controller->user->uid;
            $data['is_admin'] = \yii::$app->controller->user->isAdmin();
            $obj = new AttachmentModel;
            $obj->attributes = $data;
            $obj->save(false);
            return $obj;
        }
    }
    
    
    //存储路径 与  文件名
    //$savepath  存储的路径
    //$files 保存的文件源
    //$filename 保存的文件的名称 当只有一个文件的时候有效  当$filename=true时希望的名称是跟上传时候一样的名称
    
    //返回数组，如果是上传多张图 则返回  
    /*
                    $array = [
                        'file_name' => $filename,
                        'path_name' => $savepath,
                        'path_file' => $pathfile,
                    ];
                    $result[$name] = array_merge($file,$array);
     */
    //如果是单张图，则去除了键值name的 一个单维数组
    public function upload($savepath,$files = [],$filename = '')
    {
        try {
            $result = [];
            $files = empty($files) ? $_FILES : $files;
            if(!$files || !is_array($files)){
                throw new Exception('上传文件出错');
            } 
            foreach($files as $name => $file){
                if(!$filename || count($files) > 1){//自定义文件名
                    $filename = $this->getCustomFilename($file['name']);
                }else if($filename === true){//希望保存的跟上传的文件名一样
                    $filename = $file['name'];
                }
                
                $pathfile = $savepath . "/" . $filename;
                $ossupload = new Ossupload();
                $state = $ossupload->uploadFile($pathfile, $file['tmp_name']);  
                if(!$state){
                    if(count($files) == 1){
                        throw new Exception($ossupload->error);
                    }else{
                        continue;
                    }                    
                    
                }else{
                    $array = [
                        'file_name' => $filename,
                        'path_name' => $savepath,
                        'path_file' => $pathfile,
                        'url' => \yii::$app->params['file_url'] . "/" . $pathfile,
                    ];
                    $result[$name] = array_merge($file,$array);
                    //生成附录文件
                    $data = [];
                    $data['url'] = \yii::$app->params['file_url'] . "/" . $pathfile;
                    $data['path'] = $savepath;
                    $data['file'] = $filename;
                    $data['pathfile'] = $pathfile;
                    $data['uid'] = \yii::$app->controller->user->uid;
                    $data['is_admin'] = \yii::$app->controller->user->isAdmin();
                    $obj = new AttachmentModel;
                    $obj->attributes = $data;
                    $obj->save(false);   
                    
                }
            }
            if(count($files) == 1){
                return $result[$name];
            }else{
                return $result;
            }
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            TmpLog::addData("upload_error", $this->error);
            return $result;
        }
        
    }    
    
    //通过一个文件名(主要是通过这个后缀)  获取随机分配的一个文件名
    public function getCustomFilename($filename)
    {
        $rand = rand(10,99).substr(microtime(),2,6).substr(time(),4,6);	
        $filetype = $this->getFileExt($filename);
        $filename = $rand.".".$filetype;    
        return $filename;
    }       
    
    //获取文件的大小
    public function getSize($file)
    {
        if(is_array($file)){
            return $file['size'];
        }else{
            $img = base64_decode($file);
            return strlen($img);
        }
    }
    
    /**
     * 通过文件名获取文件扩展名
     * @return string
     */
    public function getFileExt($file)
    {
        $data = strtolower(strrchr($file, '.'));
        return substr($data, 1);
    }

    /**
     * 通过文件的路径 获取文件名
     * @return string
     */
    public function getFileName ($filePath) 
    {
        return substr($filePath, strrpos($filePath, '/') + 1);
    }
    
    /**
     * 通过文件的路径 获取文件夹名
     * @return string
     */
    public function getPathName ($filePath) 
    {
        return substr($filePath, 0,strrpos($filePath, '/'));
    }
    
    public function deletefile($pathfile)
    {
        $pathfile = $this->deleteDomain($pathfile);
        $ossupload = new Ossupload();
        $state = $ossupload->deleteObject($pathfile);   
        if(!$state){
            $this->error = $ossupload->error;
        }
        
        AttachmentModel::deleteAll(['pathfile' => $pathfile]);//删除此图片
        
        
        return $state;
    }
    
    //一个链接 'http://file.91duobao365.com/ab/cd/28494140381662.png' 去除 前面域名部份 变成 ab/cd/28494140381662.png
    public function deleteDomain($domain)
    {
        return preg_replace('/http:\/\/(.*?\/)/', '', $domain);        
    }

    //$pic_name,$x,$y,$w,$h,$tw,$th,$filep = ""
    public function getNameForJie($pic_name,$x,$y,$w,$h,$tw,$th,$save_path = "",$filep = "")
    {
//        $x = 203.171875;
//        $y = 203.171875;
//        $w = 203.171875;
//        $h = 203.171875;
//        $tw = 203.171875;
//        $th = 203.171875;
//
//        $filep = "";
//        $pic_name = "http://image.gubatoo.com/20170218/goods_class/28201362424555.jpg";        
        $obj = new Jcrop_image($filep, $pic_name, $x, $y, $tw, $th, $tw, $th);
        
        $chuli_file = $file = $obj->crop();   
        if(!$save_path){
            $save_path = date("Ymd");
        }
        $file = $this->uploadFileForOssupload("{$save_path}/{$file}",$file);
        @unlink ($chuli_file); 
        return $file;
    }
    /*
     * $savefile  'D:/ab.txt'; //本地文件  用户上传的路径文件也是一样的可以的。
     * $loadfile "aaa/ab.txt";  //上传到  OSS的路径
     */    
    public function uploadFileForOssupload($loadfile, $savefile)
    {
        $ossupload = new Ossupload();
        $state = $ossupload->uploadFile($loadfile, $savefile);  
        if(!$state){
            $this->error = $ossupload->error;
            return false;
        }else{
            return \yii::$app->params['file_url'] . "/" . $loadfile;
        }        
    }
    
}