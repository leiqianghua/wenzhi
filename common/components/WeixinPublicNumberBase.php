<?php
//微信公众号  获取接口相关的操作
namespace common\components;


use yii;
use common\components\Curl;
use common\components\GuestInfo;
use Exception;
class WeixinPublicNumberBase extends yii\base\Object
{
    public $error;
    
    protected $config = [
        'appid' => "wx2b9f9f871029a93f",
        'appsecret' => "0f632eff5155ac87ca702c1dac4eee49",
    ];    
    public $session_array = [];//通过CURL请求获取到的参数
    
    
    public function init()
    {
        parent::init();
        $config = \yii::$app->params['sanfan']['weixin_public_number'];
        $this->config = array_merge($this->config,$config);     
    }      
    
    //获取access_token
    public  function getAccess_token()
    {
        try {
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$this->config['appid']}&secret={$this->config['appsecret']}";
            $curl = new Curl();
            $result = $curl->get($url);
            $result = json_decode($result,true);
            if(empty($result['access_token'])){
                throw_exception($result['errmsg']);
            }else{
                //{"access_token":"ACCESS_TOKEN","expires_in":7200}
                return $result;
            }             
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }   
    }
    
    //对微信的请求进行相关的验证 进行相关的接入。。。。
    public  function yanzheng()
    {
        if(static::checkSignature()){
            exit($_GET['echostr']);
        }else{
            exit(0);
        }
    }
    
    //私有的验证接口的
    private  function checkSignature()
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];

        $token = TOKEN;
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );

        if( $tmpStr == $signature ){
            return true;
        }else{
            return false;
        }
    }
    
    
    
    
}