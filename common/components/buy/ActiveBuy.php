<?php
/*
 * 活动支付
 * $detail_data 的格式为 json  id,buy_num的格式
 * $red_ids = 固定0;
 * $type只有两种   1个是  三方支付   2：余额支付
 * 
 */
namespace common\components\buy;
use common\models\ActivityCouponModel;
use Exception;

class ActiveBuy extends BaseBuy
{
    
    public function init()
    {
        parent::init();
        if(!is_array($this->detail_data)){
            $this->detail_data = json_decode($this->detail_data, true);
        }
        if(!empty($this->detail_data)){
            $active = ActivityCouponModel::findOne($this->detail_data['id']);
            if(!empty($active)){
                $this->detail_data = array_merge($this->detail_data,$active->attributes);
                $this->total_money = $this->detail_data['money'] * $this->detail_data['buy_num'];
            }            
        }
    }
    
    protected function yanzheng()
    {
        //验证用户的购买的条件
        $user = $this->user;
        if(empty($user)){
            throw new Exception('用户没有登录');
        }
        if(empty($this->detail_data['money'])){
            throw new Exception('找不到相关的活动');
        }
        if(TIMESTAMP < $this->detail_data['start_time']){
            throw new Exception('活动还未开始');
        }
        if(TIMESTAMP >= $this->detail_data['end_time']){
            throw new Exception('活动已经结束');
        }
        if($this->detail_data['label']){
            $arr = json_decode($this->detail_data['label'], true);
            if(!in_array($user['label'], $arr)){
                throw new Exception('用户不会合购买条件');
            }
        }
        $shengyu = $this->detail_data['max_num'] - $this->detail_data['use_num'];
        if($this->detail_data['buy_num'] > $shengyu){
            throw new Exception('剩余库存不足');
        }
        if($this->detail_data['limit_num']){
            $num = \common\models\ActivityOrderModel::find()->where(['uid' => $this->uid,'active_id' => $this->detail_data['id']])->count();
            if(($this->detail_data['buy_num'] + $num) > $this->detail_data['limit_num']){
                throw new Exception('一个用户最多购买' . $this->detail_data['limit_num'] . "件");
            }
        }
        
        return true;
    }
    //三方支付  需要返回相应的信息  订单号   
    protected function sanfanPay($money = '') 
    {
        $this->yanzheng();
        if(!$money){
            $money = $this->detail_data['buy_num']*$this->detail_data['money'];
        }
        $code = $this->createSanfanPay($money, "C_A", 3);//C代表充值   A代表活动
        return $code;
    }
    
    //余额支付   需要返回相应的信息  订单号   
    protected function payAddData()
    {
        $this->yanzheng();
        $yuer = $this->user['money'];
        $has_money = $this->detail_data['buy_num']*$this->detail_data['money'];
        if($yuer < $has_money){//余额不够
            $money = $has_money - $yuer;
            return $this->sanfanPay($money);
        }
            //确认去购买
            // 1扣除用户余额
            //2 生成订单
            //3 增加其券
            //4 返回相关内容
        $state_id = \common\models\MemberMoneyRecordModel::changeMoney(2, $has_money, 3, $this->detail_data['id'],$this->uid);
        if(!$state_id){
            throw new Exception(MemberMoneyRecordModel::$error);
        } 
        
        //创建订单
        $data = [];
        $data['money'] = $this->total_money;
        $data['uid'] = $this->uid;
        $data['active_id'] = $this->detail_data['id'];
        $data['recharge_order_id'] = $state_id;
        $data['order_id'] = $this->pay_get_dingdan_code('O_A');//O代码订单  A充表活动
        $data['addmoney_record_id'] = $this->addmoney_order_id;
        $member_order_model = new \common\models\ActivityOrderModel();
        $member_order_model->attributes = $data;
        $state = $member_order_model->save();     
        if(!$state){
            throw new Exception($member_order_model->getOneError());
        }
        
        //为用户增加相应的优惠券
        \common\models\ActivityCouponModel::addYouhui($this->detail_data['id'], $this->detail_data['buy_num'], $this->uid);
        
        return $member_order_model['order_id'];//返回生成的订单号
    }    
}

