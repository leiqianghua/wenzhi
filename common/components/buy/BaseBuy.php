<?php

namespace common\components\buy;
use Exception;
use common\models\MemberAddmoneyRecordModel;
//基础的购买
class BaseBuy extends \common\components\BaseOperation
{
    public $detail_data;  //购买的商品列表   json格式  后来会转为数据 增加一些数据
    public $red_ids = '';//使用的红包的ID 可能是红包，可能是优惠券等集合   json格式
    public $type = 2;//购买的类型  1第三方支付   2余额支付 3三方+余额
    public $error;//错误的提示  
    public $uid = '';//用户ID
    public $addmoney_order_id = '';//充值的订单号 当通过充值的时候回调来支付的时候需要传送这个参数过来
    
    
    protected $total_money = 0;//计算后得到的值 需要支付的金额
    protected $detail_data_json = '';//$detail_data的备份的数据
    protected $red_detail = null;//红包详情或者是优惠券详情等
    protected $user;//用户具体数据
    
    public function init()
    {
        if(!$this->uid){
            $this->uid = \yii::$app->controller->user->uid;
        }
        $this->user = \common\models\MemberModel::findOne($this->uid);
        
        $this->detail_data_json = $this->detail_data;
    }
    
    public function baseBuyDetail()
    {
        $result = '';
        switch ($this->type) {
            case 1://第三方的支付
                $result = $this->sanfanPay();
                break;
            case 2://余额支付
                $result = $this->payAddData();
                break;;
            case 3://余额+第三方
            default:
                $result = $this->yuer_sanfanPay();
                break;
        }
        if(!$result){
            throw new Exception($this->error);
            \common\components\TmpLog::addData("支付商品", $this->error, "");
        }
        return $result;
    }
    
    protected function sanfanPay()
    {
        return false;
    }
    protected function payAddData()
    {
        return false;
    }

    protected function yuer_sanfanPay()
    {
        return false;
    }



    //需要充值的金额 $has_money
    //返回  创建的订单号
    protected function createSanfanPay($has_money,$dingdan_code,$consume_type,$trade_no='')
    {
        //添加未支付的充值记录订单
        $data = [];
        $data['code'] = $this->pay_get_dingdan_code($dingdan_code);
        $data['uid'] = $this->uid;
        $data['money'] = $has_money;
        $data['status'] = 0;
        $data['scookies'] = $this->detail_data_json;
        $data['consume_type'] = $consume_type;
        $data['red_ids'] = $this->red_ids;
        $data['trade_no'] = $trade_no;
        $model = new MemberAddmoneyRecordModel();
        $model->attributes = $data;
        $state = $model->save(false);
        if($state){
            return $model->code;
        }else{
            return false;
        }        
    }    
    
    
    public function buy()
    {
        $method = get_class($this);
        $cache_key = "{$method}_{$this->uid}";
        return $this->baseTit(true,true,true,$cache_key,$callback = 'baseBuyDetail',[]);
    }    
    


    //生成订单号
    public function pay_get_dingdan_code($dingdanzhui='',$admin_id = '')
    {
        return $dingdanzhui.time().substr(microtime(),2,6).rand(0,9).$admin_id;
    }     
    
}

