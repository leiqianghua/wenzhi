<?php
/*
 * 活动支付
 * $detail_data 的格式为 json  通过  购物车，或者是直接购买 post过来的数据
 * $red_ids = 使用到的优惠券
 * $type只有两种   1个是  三方支付   2：余额支付
 * 
 * 
 *  不管其怎么变化,价格策略永远都不会变。一:看购买的人是什么级别，再根据其购买的商品  总的 来加 起来。算出其具体的金额是多少
 *  $d['buy_num'] * $d['discount_0']  商品价格永远都是这样算的      $model['price'] 附加费永远是这样算的    运费又是一套体系，总的是这样算的。
 */
namespace common\components\buy;
use common\models\ActivityCouponModel;
use common\models\OrderModel;
use Exception;

class GoodsBuy extends BaseBuy
{
    private $reds_use_money = 0;//所要用的总的优惠的价格
    public $address_id = 0;  //商品的地址
    private $freight = 0; //运费  商品运费   所要用的总的运费
    
    private $goods_amount=0;//商品总价  没有折扣，没有运费的   总价格
    private $goods_amount_discount=0;//商品折扣价   //商品扣扣价格
    
    private $goods_calc_money; //通过\common\models\GoodsModel::calcTotalMoney($this->detail_data);算下来的价格

    private $total_score;//要花费的积分
    
    
    //后面新加的相关的属性。。。。。
    private $admin_arr;
    public function init()
    {
        parent::init();
        if(!is_array($this->detail_data)){
            $this->detail_data = json_decode($this->detail_data, true);
        }
        if(!empty($this->detail_data)){
            $_POST = $this->detail_data;
            $this->detail_data = \common\models\GoodsModel::getDataForPost();//获取来源的数据
            $this->goods_calc_money = $calc_money = \common\models\GoodsModel::calcTotalMoney($this->detail_data,$this->uid);
            $this->goods_amount = $calc_money[0];
            $this->goods_amount_discount = $calc_money[1];
            $this->total_money = $calc_money[1];//总共要花的价格
            $this->freight = \common\models\GoodsModel::calcFreight($this->address_id, $this->detail_data);//运费
            if(!$this->freight){
                $freight = 0;
            }else{
                $freight = $this->freight[0];
            }
            $this->total_money = $freight + $this->total_money;
            $this->total_score = $calc_money[2];
            
            //用了优惠券
            if($this->red_ids){
                if(!is_array($this->red_ids)){
                    $this->red_ids = json_decode($this->red_ids, true);
                }
                foreach($this->red_ids as $key => $d){
                    $this->red_detail[$key] = \common\models\MemberCouponModel::findOne($d);
                    if($this->red_detail[$key]){
                        $this->red_detail[$key] = $this->red_detail[$key]->attributes;
                        $this->reds_use_money += $this->red_detail[$key]['money'];
                    }
                }
            }
        }
        
        if(is_array($this->detail_data_json)){
            $d = json_encode($this->detail_data_json);
        }else{
            $d = $this->detail_data_json;
        }
        //print_r($this);exit;
    }
    
    private function yanzheng()
    {
        $state = \common\models\GoodsModel::buy2Verification($this->detail_data);
    }

    //三方支付  需要返回相应的信息  订单号   
    protected function sanfanPay() 
    {
        $this->yanzheng();
        if($this->total_money <= $this->reds_use_money){
            return $this->payAddData();
        }
        
        $has_money = $this->total_money - $this->reds_use_money - $this->user->money;
        $has_money = $has_money + 0;
        if($has_money <= 0){
            return $this->payAddData();
        }
        $code = $this->createSanfanPay($has_money, "C_S", 2,$this->address_id);//C代表充值  S代表商品支付
        return $code;        
    }
    
    
    private function createOrderForBuys($order_sn)
    {
        foreach($this->detail_data as $admin_id => $values){
            //然后订单里面有N多的商品
        
            $city = \common\components\GuestInfo::getDataForIp();
            $data = [];
            $data['province'] = empty($city['province']) ? '' : $city['province'];
            $data['city'] = empty($city['city']) ? '' : $city['city'];
            $data['order_sn'] = $order_sn . "_" . $admin_id;
            $data['order_sn_base'] = $order_sn;
            $data['admin_id'] = $admin_id;


            $data['uid'] = $this->uid;
            $data['username'] = $this->user['username'];
            $data['payment_time'] = TIMESTAMP;

            $data['goods_amount'] = $this->goods_calc_money[3][$admin_id]['total_money'];
            $data['goods_amount_discount'] = $this->goods_calc_money[3][$admin_id]['total_money_zhe'];
            if(!$this->freight){
                $freight = 0;
            }else{
                $freight = $this->freight[1][$admin_id];
            }
            $data['order_amount'] = $this->goods_calc_money[3][$admin_id]['total_money_zhe'] + $freight;
            $data['order_state'] = 1;
            $data['freight'] = $freight;
            $data['addmoney_record_id'] = $this->addmoney_order_id;
            $data['fujia'] = 0;
            $data['other_data'] = $this->detail_data_json;

            //这一笔订单的收货信息
            $address_arr = \common\models\MemberAddressModel::getOne($this->address_id);
            $data['address'] = $address_arr['address_info'] . $address_arr['address'];
            $data['phone'] = $address_arr['iphone'];
            $data['user_truename'] = $address_arr['username'];


            $order_model = new OrderModel();
            $order_model->attributes = $data;
            $order_model->save(false);   
            //减少相应的库存等   ------------------------------------------        
            foreach($values['data'] as $d){
                $shop_id = $d['id'];
                $express = new \yii\db\Expression("num-{$d['buy_num']}");
                $express1 = new \yii\db\Expression("sale_num+{$d['buy_num']}");
                $state = \common\models\GoodsModel::updateAll(['num' => $express,'sale_num' => $express1], ['id' => $shop_id]);
                if(!$state){
                    throw new Exception('商品库存不足');
                }
                $common_id = \common\models\GoodsModel::getOne($shop_id, "common_id");
                if($common_id){
                    $state = \common\models\GoodsCommonModel::updateAll(['num' => $express,'sale_num' => $express1], ['id' => $common_id]);
                }
            }
            
            foreach($values['data'] as $v){//具体的每个订单商品
                $d = [];
                $d['order_id'] = $order_model->order_id;
                $d['province'] = $order_model->province;
                $d['city'] = $order_model->city;
                $d['goods_id'] = $v['common_id'];
                $d['gid_detail'] = $v['id'];
                $d['type'] = 1;
                $d['name'] = $v['name'];
                $d['img'] = $v['img'];
                $d['uid'] = $this->user['uid'];
                $d['username'] = $this->user['username'];
                $d['money'] = $v['price']* $v['buy_num'];
                $d['order_sn_base'] = $order_sn;
                $d['admin_id'] = $admin_id;
                $d['province_id'] = $this->user['province_id'];
                $d['city_id'] = $this->user['city_id'];
                $d['area_id'] = $this->user['area_id'];
                
                $label = $this->user['label'];
                if($label == 0){//出厂价
                    $money_zhe = $v['buy_num'] * $v['discount_0'];
                }else if($label == 1){
                    $money_zhe = $v['buy_num'] * $v['discount_1'];
                }else if($label == 2){
                    $money_zhe = $v['buy_num'] * $v['discount_2'];
                }else if($label == 3){
                    $money_zhe = $v['buy_num'] * $v['discount_3'];
                }else{
                    $money_zhe = $v['buy_num'] * $v['discount_4'];
                }                        
                $d['discount_money'] = $money_zhe;
                $d['num'] = $v['buy_num'];
                $order_shop_model = new \common\models\OrderGoodsModel();
                $order_shop_model->attributes = $d;
                $order_shop_model->save(false);

                //用于后面可能需要的统计的处理。
//                $o = new \common\models\OrderGoodsAllCountModel();
//                $o->attributes = $d;
//                $o->province = empty($city['province']) ? '' : $city['province'];
//                $o->city = empty($city['city']) ? '' : $city['city'];
//                $o->save(false);                
            }
        }
    }
    

    //余额支付   需要返回相应的信息  订单号   
    protected function payAddData()
    {
        $this->yanzheng();
        if($this->total_money <= $this->reds_use_money){
            $user_money = 0;//用户不需要支付金额
        }else{
            $user_money = $this->total_money - $this->reds_use_money;//用户需要支付的金额
        }  
        
        //积分
        if($this->user['score'] < $this->total_score){
            $this->error = "积分不足";
            return false;
        }    
        
        //用户余额不足
        $use_has_money = $this->user['money'] + 0;
        if($use_has_money < $user_money){
            return $this->sanfanPay();
        }
        
        $order_sn = $this->pay_get_dingdan_code('O_S');//O代码订单  A充表活动
        
        $this->createOrderForBuys($order_sn);
        //删除购物车数据
        //删除购物车中的商品
        if (!empty($_POST['ifcart'])) {
            \common\models\GoodsCartModel::deleteAll(['id' => $_POST['cart_list']]);
        }
        
        //扣除用户钱
        //扣除用户的优惠券
        if($user_money){
            $state_id = \common\models\MemberMoneyRecordModel::changeMoney(2, $user_money, 3, $order_sn,$this->uid);
            if(!$state_id){
                throw new Exception(\common\models\MemberMoneyRecordModel::$error);
            }             
        }
        //扣除用户积分
        if($this->total_score){
            $state = \common\models\Score_detailModel::changeScore(2, $this->total_score, "购买商品", $order_sn, $this->uid);
            if(!$state){
                $this->error = \common\models\Score_detailModel::$error;
                return false;
            }
        }
        
        if($this->red_ids){//把这些的全部变成已使用
            \common\models\MemberCouponModel::updateAll(['is_use' => 1], ['id' => $this->red_ids]);
        }
        
        //每个人拿到其相应的金额 他的上级，或上上级等这样的级别该拿到的金额
        $uids = \common\models\MemberModel::getParents($this->uid);//购买者 及其上级的所有的ID
        foreach($uids as $key => $uid){
            $d = \common\models\MemberModel::findOne($uid);
            $uids[$key] = [
                'uid' => $uid,
                'label' => $d['label'],
            ];
        }
        $use_money = 0;
        if(count($uids) >= 1){
            $goods_order = \common\models\OrderGoodsModel::find()->where(['order_sn_base' => $order_sn])->asArray()->all();
            foreach($goods_order as $key => $value){
                $use_money += $this->changeMoneyForBuyGoods($value, $uids);//再加上相应的这个供应商
            }             
        }        

        
        //如果这个商品是属于供应商的,不是总部的,则剩下的钱要直接打给供应商,而不是给总部.但是这个用户是买 了N多产品，里面有的是给供应商的,有的是给总共的,有的是要
        //给代理的,有的是要给 加盟商的(另如这个附加商品的费用) 所以我们是要看其这个商品 每个商品  买的  付了最后得到的钱然后扣掉总的支出最后给这个供应商。
        
        $order_model_lsit = OrderModel::findAll(["order_sn_base" => $order_sn]);
        foreach($order_model_lsit as $value){
            \common\components\CallbackOperation::afterGoodsBuy($value);//购买完了后的通知
        }
        
        return $order_sn;
    } 
    
    //通过购买的订单商品，会员的级别来给其相应的增加钱
    //$data 为订单商品
    //$uids 为这个用户的所有的上级，包括他自己
    public  function changeMoneyForBuyGoods($data,$uids)
    {
        $use_money = 0;
        
        $goods_model = \common\models\GoodsModel::findOne($data['gid_detail']);
        for($i = 0;$i < count($uids);$i++){
            $m = $i + 1;
            if(isset($uids[$m])){//存在第二个的
                if($uids[$i]['label'] > $uids[$m]['label']){
                    $key1 = "discount_{$uids[$i]['label']}";
                    $key2 = "discount_{$uids[$m]['label']}";
                    $money = ($goods_model[$key1] - $goods_model[$key2])*$data['num'];
                    $use_money += $money;
                    \common\models\MemberMoneyRecordModel::changeMoney(1, $money, '拥金', $data['id'], $uids[$m]['uid']);                        
                }
            }
        }
        
        $all_money = $data['discount_money'];
        $has_money = $all_money - $use_money;
        if($data['admin_id']){
            //看相应的这个商品给的折扣是多少
            if($goods_model['has_zheko_bili']){
                $ko_money = $has_money * (100 - $goods_model['has_zheko_bili'])/100;//这个钱直接给供应商
                if($ko_money){
                    $uidsss = \common\models\MemberModel::find()->where(['admin_id' => $data['admin_id']])->limit(1)->asArray()->one();
                    if($uidsss){
                        \common\models\MemberMoneyRecordModel::changeMoney(1, $ko_money, '供应商销售', $data['order_sn_base'], $uidsss['uid']);   
                    }
                }
            }
        }
        return $use_money;
        return true;
    }      
}

