<?php

/* 
 * 阿里云具体的搜索的组件
 */
namespace components\cloudsearch;
class Search
{
    public $search_obj;//阿里云搜索的OBJ
    public $searchData = [];//查找数据的条件 get  post等条件
    public $result = [];//搜索结果
    public $config = [
        'open' => true, //开放搜索开关
        'app_name' => 'new_zhonghua',
        'host' => 'http://opensearch-cn-hangzhou.aliyuncs.com'//公网
    ];
    public function __construct($config = [], $condition = []) 
    {
        $this->searchData = array_merge($_POST, $_GET,$condition);//各个准备好的条件
        if($config){
            $this->config = array_merge($this->config, $config);
        }        
        $this->init();
    }
    
    public function init()
    {
        defined("CLOUDSEARCH_PATH") or define("CLOUDSEARCH_PATH", './cloudsearch/');
        require_once CLOUDSEARCH_PATH . DIRECTORY_SEPARATOR . 'conf.inc.php';
        require_once CLOUDSEARCH_PATH . DIRECTORY_SEPARATOR . 'CloudsearchClient.php';
        require_once CLOUDSEARCH_PATH . DIRECTORY_SEPARATOR . 'CloudsearchDoc.php';
        require_once CLOUDSEARCH_PATH . DIRECTORY_SEPARATOR . 'CloudsearchIndex.php';
        require_once CLOUDSEARCH_PATH . DIRECTORY_SEPARATOR . 'CloudsearchSearch.php';      
        
        $host = $this->config['host'];
        $key_type = "aliyun";  //固定值，不必修改
        $opts = array('host'=>$host);
        // 实例化一个client 使用自己的accesskey和Secret替换相关变量
        $client = new CloudsearchClient(CLOUDSEARCH_APPKEY,CLOUDSEARCH_APPSECRET,$opts,$key_type);

        $app_name = $this->config['app_name'];
        
        // 实例化一个搜索类
        $this->search_obj = new CloudsearchSearch($client);
        // 指定一个应用用于搜索
        $this->search_obj->addIndex($app_name);
      
    }
    
    
    
    //返回搜索结果,,针对于此东西的具体的搜索相关确定相关的一些条件等。
    public function search()
    {
        //条件搜索
        //增加搜索的条件
        $this->search_obj->setQueryString("default:{$this->searchData['keyword']}");
        
        $this->search_obj->addFilter("goods_state=1");
        $this->search_obj->addFilter("goods_verify=1");
        
        //某个品牌下搜索
        if (!empty($this->searchData['bid'])) {
            $this->search_obj->addFilter("brand_id={$this->searchData['bid']}");
        }
        
        //店铺下搜索
        if (!empty($this->searchData['sid'])) {
            $this->search_obj->addFilter("store_id={$this->searchData['sid']}");
        }  
        
        //某个分类
        if (!empty($this->searchData['cid'])) {
            $cid = Model('goods_class')->getChildClassForin($this->searchData['cid']);        
            $this->search_obj->addFilter("gc_id={$this->searchData['cid']}");
        }
        
        //价格区间
        if (!empty($this->searchData['low'])) {
            $this->search_obj->addFilter("goods_price>={$this->searchData['low']}");
        }   
        if (!empty($this->searchData['hide'])) {
            $this->search_obj->addFilter("goods_price<={$this->searchData['hide']}");
        }
        //条件搜索结束
        
        //增加排序
        $this->addorder();
        
        //设置偏移量
        $this->setOffset();
        
        //返回搜索的结果
        $this->getResult($result);
        
        //搜索成功
        if(!empty($result['status']) && $result['status'] == 'OK'){
            $this->result = $result['result'];
            $page = $this->setPage();
            $datas['data'] = $this->result['items'];
            $datas['page'] = $page;
            $datas['count'] = $this->result['total'];
            return $datas;
        }else{
            return false;
        }
    }
    
    //设置分页
    public function setPage()
    {
        //处理分页
        pagecmd('setPageName', 'page');
        pagecmd('setnowpage', Unit::I('page',1));
        pagecmd('setEachNum', $this->searchData['pagesize']);
        pagecmd('setTotalNum', $this->result['total']);  
        return pagecmd('show', 5);
    }
    
    public function addorder()
    {
        if (!empty($this->searchData['order'])) {
            $order = $this->getOrderd($this->searchData['order']);
            if($order){
                $this->search_obj->addSort($order[0],$order[1]);
            }
            
        }         
    }
    
    public function setOffset()
    {
        //增加分页数据
        //限制条件 先写死20
        
        $pagesize = $this->searchData['pagesize'] = empty($this->searchData['pagesize']) ? 21 : $this->searchData['pagesize'];
        $this->search_obj->setHits($pagesize);        
        $offset = ($this->searchData['page'] - 1) < 0 ? 0 : ($this->searchData['page'] - 1);
        $offset = $offset*$pagesize;//起始位置        
        $this->search_obj->setStartHit($offset); //偏移量        
    }
    
    public  function getOrderd($orderd){
        $order = '';
        switch ($orderd) {
            //销量从大到小
            case 1:
                $order = ['goods_salenum','-'];
                break;
            //人气
            case 2:
                $order = ['collect','-'];
                break;
            //价格从小到大
            case 3:
                $order = ['goods_price','+'];
                break;
            //价格从大到小
            case 4:
                $order = ['goods_price','-'];
                break;                                                                                             
            default:
                //$order = 'goods_selltime desc';//默认排序
                break;  
        }
        return $order;     	
    }
    
    public function getResult(&$result)
    {
       $this->search_obj->setFormat("json"); 
       $json = $this->search_obj->search();
       $result = json_decode($json,true);
    }
}

