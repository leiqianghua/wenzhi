<?php

if (!defined('OPENSEARCH_PATH')) {
    define('OPENSEARCH_PATH', dirname(__FILE__));
}

require_once OPENSEARCH_PATH . DIRECTORY_SEPARATOR . 'conf.inc.php';

require_once OPENSEARCH_PATH . DIRECTORY_SEPARATOR . 'CloudsearchClient.php';
require_once OPENSEARCH_PATH . DIRECTORY_SEPARATOR . 'CloudsearchDoc.php';
require_once OPENSEARCH_PATH . DIRECTORY_SEPARATOR . 'CloudsearchIndex.php';
require_once OPENSEARCH_PATH . DIRECTORY_SEPARATOR . 'CloudsearchSearch.php';

$client = new CloudsearchClient(OPENSEARCH_APPKEY, OPENSEARCH_APPSECRET, ['host'=>'http://opensearch-cn-hangzhou.aliyuncs.com'],'aliyun');

$app_name = '1zw';
$index_obj = new CloudsearchIndex($app_name, $client);
$result = $index_obj->listIndexes();
$result = json_decode($result, true);

$search_obj = new CloudsearchSearch($client);
$search_obj->addIndex('1zw');
$search_obj->setQueryString("default:''");
$search_obj->setFormat('json');
$json = $search_obj->search();
$result = json_decode($json, true);
var_dump($result);

