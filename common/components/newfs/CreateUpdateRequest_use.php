<?php
//用户创建组   删除人员  增加人员  等都是在这里面体现
/**
 * go_h_member_request_use  会变化的表
 */
namespace common\components\newfs;
use Exception;
class CreateUpdateRequest_use extends \common\components\BaseOperation
{
    public $uids;//要操作的用户列表  是一个UID的数组      占时不区分上下级关系 直接进入过来
    public $type;// 1:创建工作组，如果有则增加工作组成员  2：删除工作组成员
    public $request_id;//哪一个需求
    
    protected $jianyuan_model;
    //会有专门指定的吗？？应该是没有的
    public function run()
    {
        return $this->baseTit(true, true, false, "CreateUpdateRequest_use_{$this->request_id}");
    }
    
    public function tit()
    {
        if($this->type == 1){
            $uids = [];
            foreach($this->uids as $uid){//找到其所有的上级来 再进行处理。
                if(!in_array($uid, $uids)){
                    $uids[] = $uid;
                }
                $uid_all = \common\models\hy_designerModel::findAllParent($uid);
                if($uid_all){
                    foreach($uid_all as $k => $v){
                        if(!in_array($v, $uids)){
                            $uids[] = $v;
                        }                        
                    }
                }
            }
            
            
            foreach($uids as $uid){
                $is_has_use = \common\models\hy_member_request_useModel::findOne(['request_id' => $this->request_id,"uid" => $uid]);
                if($is_has_use){
                    continue;
                }
                $model = new \common\models\hy_member_request_useModel();
                $data = [];
                $data['request_id'] = $this->request_id;
                $data['uid'] = $uid;
                $data['username'] = \common\models\MemberModel::getOne($uid, "username");
                $model->attributes = $data;
                $model->save(false);                
            }
            
            $request = \common\models\hy_member_requestModel::findOne($this->request_id);
            if($request['speed'] == 1){
                $request->speed = 2;
                $request->speed_2_time = time();
                $request->save(false);//进入到方案设计阶段
            }
            return true;
        }else{//删除工作组成员
            \common\models\hy_member_request_useModel::deleteAll(['uid' => $this->uids,'request_id' => $this->request_id]);
            return true;
        }
    }
}

