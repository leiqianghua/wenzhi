<?php
//用户创建相关的需求
/**
 * 用户创建需求  跟  修改需求 都是通过这个来操作的
 * 会变化的表有 go_h_member_jianyuan  go_h_member_request 这两个表会变化
 */
namespace common\components\newfs;
use Exception;
class CreateUseRquest extends \common\components\BaseOperation
{
    public $params;//相关的参数  创建这个需求提供了怎么样的一个表单的数据
    public $uid;//哪一个用户
    public $service_uid;//指定的业务员
    
    protected $jianyuan_model;
    
    
    //会有专门指定的吗？？应该是没有的
    public function run()
    {
        return $this->baseTit(true, true, false, "CreateUseRquest_{$this->uid}");
    }
    
    public function tit()
    {
        $this->v();
        if($this->jianyuan_model){
            $this->jianyuan_model->attributes = $this->params;
            $this->jianyuan_model->save(false);
        }else{
            //之前是没有这相关的记录的
            $this->jianyuan_model = new \common\models\hy_member_jianyuanModel();
            $this->jianyuan_model->attributes = $this->params;
            $this->jianyuan_model->uid = $this->uid;
            $this->jianyuan_model->save(false);            
        }
        
        //如果没有需要正在对接则创建一个需求
        $request_data = \common\models\hy_member_requestModel::findOne(['uid' => $this->uid,'status' => 1]);
        if($request_data){
            return 1;
        }

        
        $model = new \common\models\hy_member_requestModel();
        $data = [];
        $data['uid'] = $this->uid;
        $data['speed'] = 1;
        if($this->service_uid){//如果有指定则用指定的
            $data['service_uid'] = $this->service_uid;
        }else{
            $data['service_uid'] = $this->fenpei_service_uid();
        }
        $data['title'] = \common\models\MemberModel::getOne($this->uid, "username") . "-项目";
        $model->attributes = $data;
        $model->save(false);
        
        //通知这个人有人有创建需求啦需要尽快的去处理
        $obj = new \common\components\TemplateMessage();
        $obj->uid = $data['service_uid'];
        $member = \common\models\MemberModel::getOne($this->uid);
        $params = [
            "first" => "有新的花园需要,请确认",
            "keyword1" => $member['username'],
            "keyword2" => $member['iphone'],
            "keyword3" => $member['address_info'] . $member['address'],
            "keyword4" => date("Y-m-d H:i:s", time()),
            "keyword5" => "待跟进",
            "remark" => "请您尽快确认安排~",
        ];
        $obj->buy_notice($params, "http://www.gubatoo.com/hy/#/pages/member/notice?id=2", "WQlAZIWub-SzI-wVoNJh9xmRVAhFRgRDD0P_brU_7w4");
        
        return 2;
        
    }
    //如果已经有了，则用户只是修改其需求而已
    public function v()
    {
        $this->jianyuan_model = \common\models\hy_member_jianyuanModel::findOne(['uid' => $this->uid]);
    }
    
    //为用户的需求分配一个业务员来去联系  或者是主动的换一个或者是其它的情况 肯定是得有一个人去追踪
    public function fenpei_service_uid()
    {
        
        $parent_id = \common\models\MemberModel::getOne($this->uid, "parent_id");
        if($parent_id){
            $par_user = \common\models\hy_designerModel::findOne($parent_id);
            if($par_user){
                $d = \common\models\hy_designer_levelModel::find()->where(['uid' => $parent_id,"studio_level_id" => [1,2,6,10]])->limit(1)->one();
                if($d){
                    return $parent_id;
                }
            }
        }
        
        //$address_city = json_decode($this->jianyuan_model->address_city,true);//省市县  先从县里面找，没有则从市里面找，然后再从省里面找，再没有则随机的一个
//        $studio = \common\models\hy_studioModel::findOne(['city' => $address_city[2]]);
//        if(!$studio){
//            $studio = \common\models\hy_studioModel::findOne(['city' => $address_city[1]]);
//        }
//        if(!$studio){
//            $studio = \common\models\hy_studioModel::findOne(['city' => $address_city[0]]);
//        }

        $address_city = json_decode($this->jianyuan_model['address_city'],true);
        if($address_city){
            $arr = explode("-", $address_city['label']);
            if(!empty($arr[1])){
                $city = $arr[1];
            }
        }
        if(!empty($city)){
            $studio = \common\models\hy_studioModel::find()->limit(1)->where(['city' => $city])->one();
        }
        
        
        if(empty($studio)){
            $studio = \common\models\hy_studioModel::find()->limit(1)->one();
        }
        
        //从这个工作室中找到一个符合的人来 首先是得这个工作室的  然后是必须得是符合身份的  业务或者管理   等于这个是需要符合要求的 studio_level_id
        $condition = [];
        $condition['noget'] = true;
        $condition['pagesize'] = 1;
        //$condition['studio_id'] = $studio['id'];
        
        $condition[] = "main.studio_id={$studio['id']}";
        $condition['pagesize'] = 1;
        $condition['alias'] = "main";
        $condition['leftJoin'] = [
            'table' => \common\models\hy_designer_levelModel::tableName() . " level",
            'on' => "main.uid=level.uid",
        ];
        $condition[] = "level.studio_level_id in(1,2,6)";//得先有一个管理员
        $condition['orderBy'] = "level.studio_level_id asc";
        $datas = \common\models\hy_designerModel::baseGetDatas($condition);
        if(!$datas){
            return false;//找不到需要给其安排的一个人员
        }else{
            return $datas[0]['uid'];
        }
    }
    
    
    
    /*  如果是针对我们的其它逻辑业务则通过static静态方面直接确定修改  */
    
    
    
    //需要分配业务员的request_id 跟 分配给这个$fenpei_id
    public static function updateOtherUser($request_id,$fenpei_id)
    {
        $is_update = \common\models\hy_member_requestModel::updateAll(['service_uid' => $fenpei_id], ['id' => $request_id]);
        if(!$is_update){
            return false;
        }
        \common\models\hy_member_requestModel::removeGetOne($request_id);
        return true;
    } 
    
    
    //其谈的金额是多少  设置其项目金额
    public static function updateMoney($request_id,$money)
    {
        $is_update = \common\models\hy_member_requestModel::updateAll(['money' => $money], ['id' => $request_id]);
        if(!$is_update){
            return false;
        }
        \common\models\hy_member_requestModel::removeGetOne($request_id);
        return true;
    }
    
    
}

