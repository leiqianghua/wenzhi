<?php
//删除员工身份
namespace common\components\newfs;
use Exception;
class DeleteDesigner extends \common\components\BaseOperation
{
    public $uid;//要删除的员工的UID
    
    //会有专门指定的吗？？应该是没有的
    public function run()
    {
        return $this->baseTit(true, true, false, "DeleteDesigner_{$this->uid}");
    }
    
    public function tit()
    {
        $datas = \common\models\hy_designerModel::createDesigner($this->uid, false);
        if(!$datas){
            throw new Exception("找不到用户");
        }
        if($datas['max_levels'] <= 1){
            throw new Exception("不允许直接删除管理员");
        }
        
        //$datas['designer_user']['parentuid']//它的上级是这个
        \common\models\hy_designerModel::updateAll(['parentuid' => $datas['designer_user']['parentuid']], ['parentuid' => $this->uid]);//把所有的用户的上级改为此用户的上级
        \common\models\hy_designerModel::deleteAll(['uid' => $this->uid]);//员工表
        
        \common\models\hy_designerModel::removeGetOne($this->uid);
        
        \common\models\hy_designer_levelModel::deleteAll(['uid' => $this->uid]);//员工身份表
        \backend\models\AdminMemberModel::deleteAll(['uid' => $datas['admin_user']['uid']]);//后台会员表
        \common\models\hy_member_request_useModel::deleteAll(['uid' => $this->uid]);//用户需求组表
        
        return true;
        
    }
}

