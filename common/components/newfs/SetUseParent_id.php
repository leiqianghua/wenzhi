<?php
//设置用户的上级
/*
 * 规则 只有当用户是管理员 最上面的标签的时候才可能没有上级，否则都是会有上级的  所有当设置为0的时候需要判断一下
 * 设置成的上级必须是在同一个工作室的 然后级别是有比它低的才可以
 */
/**
 * go_member  会变化的表
 */
namespace common\components\newfs;
use Exception;
class SetUseParent_id extends \common\components\BaseOperation
{
    public $uid;//设置的用户是这个
    public $parent_id;// 设置成其上级是这个
    
    //会有专门指定的吗？？应该是没有的
    public function run()
    {
        return $this->baseTit(true, true, false, "SetUseParent_id_{$this->uid}");
    }
    
    public function tit()
    {
        $user = \common\models\hy_designerModel::findOne($this->uid);
        if($this->parent_id){
            $parent_user = \common\models\hy_designerModel::findOne($this->parent_id);
            if(!$parent_user){
                $this->error = "找不到会员";
                return false;
            }
        }
        if(!$user){
            $this->error = "找不到会员";
            return false;
        }
        
        if(!$this->parent_id){//如果设置成没有上级  则看一下你的最大身份是哪一个
            $biao = \common\models\hy_designerModel::createDesigner($this->uid, 0);
            if($biao['max_levels'] > 1){
                throw new Exception("非管理员必须设置一个上级");
            }else{
                $user->parentuid = $this->parent_id;
                $user->save(false);

                return true;                
            }              
        }else{
            $us = \common\models\hy_designerModel::findOne($this->parent_id);
            $us1 = \common\models\hy_designerModel::findOne($this->uid);
            if($us['studio_id'] != $us1['studio_id']){
                throw new Exception("上下级工作室必须一样");
            }
        }
        
        //求出上级的一个，跟我的来对比
        $shang_biao = \common\models\hy_designerModel::createDesigner($this->parent_id, 0);
        $mine_biao = \common\models\hy_designerModel::createDesigner($this->uid, 0);
        if($shang_biao['max_levels'] >= $mine_biao['max_levels']){
            throw new Exception("不允许被设置,上级级别过低");
        }
        
        $user->parentuid = $this->parent_id;
        $user->save(false);
        
        return true;
        
    }
}

