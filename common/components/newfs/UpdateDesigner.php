<?php
//设置用户的身份
namespace common\components\newfs;
use Exception;
class UpdateDesigner extends \common\components\BaseOperation
{
    public $do_uid;//处理的UID是谁（前台的UDI）
    public $operation_uid;//操作的UID是谁（后台的UID）
    
    public $studio_id;//团队ID  设置为哪个团队的
    public $studio_level;//设置为什么身份  是一个数组
    
    
    protected $operation_designer;//操作的人的情况 空的则说明是超管 
    protected $do_designer;//要设置的人的情况
    public function run()
    {
        return $this->baseTit(true, true, false, "UpdateDesigner_{$this->do_uid}");
    }
    
    public function tit()
    {
        $this->v();
        
        //进行设置
        //原来有则修改 原来没有则创建
        if(!$this->do_designer){//原来是没有的  1增加账号  2：增加后台账号 3：增加其标签数据
            $member = \common\models\MemberModel::getOne($this->do_uid);
            $datas = [];
            $datas['nickname'] = $member['username'];
            $datas['img'] = $member['img'];
            $datas['mobile'] = $member['iphone'];
            $datas['uid'] = $member['uid'];
            $datas['studio_id'] = $this->studio_id;
            $datas['studio_name'] = \common\models\hy_studioModel::getOne($this->studio_id, "name");
            
            $model = new \common\models\hy_designerModel();
            $model->attributes = $datas;
            $model->save(false);  
            

        
            //再为其创建一个后台的用户
            $data = [];
            $model = new \backend\models\AdminMemberModel();
            while (true){
                $data['username'] = "gubatoo".  rand(100000, 999999);
                $m = \common\models\AdminMember::findOne(['username' => $data['username']]);
                if($m){
                    continue;
                }else{
                    break;
                }
            }
            
            $data['password'] = "123456";
            $data['role_id'] = $this->getRole();//为其建立的角色
            $data['status'] = 1;
            $data['frontend_uid'] = $member['uid'];
            $model->attributes = $data;
            $model->save(false); 
            $admin_model = $model;
            
        }else{//原来已经有身份了
            $do_designer = $this->do_designer;
            if($do_designer['designer_user']['studio_id'] != $this->studio_id){
                $datas = [];
                $datas['studio_id'] = $this->studio_id;
                $datas['studio_name'] = \common\models\hy_studioModel::getOne($this->studio_id, "name");                
                $model = \common\models\hy_designerModel::findOne($this->do_uid);
                $model->attributes = $datas;
                $model->save(false);
            }
            
            $role_id = $this->getRole();
            if($role_id != $do_designer['admin_user']['role_id']){
                $model = \backend\models\AdminMemberModel::findOne($do_designer['admin_user']['uid']);
                $model->role_id = $role_id;
                $model->save(false);
            }
        }

        \common\models\hy_designer_levelModel::deleteAll(['uid' => $this->do_uid]);

        foreach($this->studio_level as $key => $value){
            
            if($value == 1){
                $is_ok = \common\models\hy_designer_levelModel::findOne(['studio_id' => $this->studio_id,'studio_level_id' => 1]);
                if($is_ok){
                    throw new Exception("已经存在管理员了,一个工作室只允许一个管理员");
                }
            }            
            
            $model = new \common\models\hy_designer_levelModel();
            $data = [];
            $data['uid'] = $this->do_uid;
            $data['studio_id'] = $this->studio_id;
            $data['username'] = \common\models\MemberModel::getOne($this->do_uid, "username");
            $data['studio_level_id'] = $value;
            $data['studio_level_name'] = \common\models\hy_studio_levelModel::getOne($value, "title");
            $model->attributes = $data;
            $model->save(false);
            
            if($value == 1){
                \common\models\hy_studioModel::updateAll(['founder_uid' => $data['uid'],'founder_name' => $data['username']], ['id' => $this->studio_id]);
                \common\models\hy_studioModel::removeGetOne($this->studio_id);
            }            
            

        }          
        if(!empty($admin_model)){
            return $admin_model->attributes;
        }else{
            return true;
        }
        
    }
    
    //是否允许操作这个用户
    public function v()
    {
        
        if(!$this->studio_id){
            throw new Exception("需要指定设置的团队");
        }
        
        $admin_model = \backend\models\AdminMemberModel::getOne($this->operation_uid);
        if(!$admin_model){
            throw new Exception("找不到操作会员");
        }
        

        //设置的身份中求出最高的级别来
        $levels = \common\models\hy_studio_levelModel::find()->asArray()->where(['id' => $this->studio_level])->select("min(level)")->scalar();
        if(!$levels){
            throw new Exception("未选择设置的身份");
        }        
        
        
        $do_user = \common\models\hy_designerModel::getOne($this->do_uid);
        //找这个用户上下级 然后把这个用户的上下级的 身份找出来 然后再看一下是否是符合要求
        if(!$do_user){
            $xiaji = $shangji = null;
        }else{
            if(!$do_user['parentuid']){
                $shangji = null;
            }else{
                $shangji = \common\models\hy_designerModel::getOne($do_user['parentuid']);
            }
            
            $xiaji = \common\models\hy_designerModel::findAll(['parentuid' => $do_user['uid']]);
        }
//        if(!$do_user || !$do_user['parentuid']){
//            $shangji = null;
//        }else{
//            $shangji = \common\models\hy_designerModel::getOne($do_user['parentuid']);
//        }
//        
//        $xiaji = \common\models\MemberModel::findAll(['parent_id' => $do_user['uid']]);
        if($shangji){//要设置的标签 不能超过其上级的
            $biao = \common\models\hy_designerModel::createDesigner($shangji['uid'], 0);
            if($biao['max_levels'] >= $levels){//上级的身份比我的还小或者同级
                throw new Exception("上级身份过小,不允许被设置");
            }
        }
        if($xiaji){
            foreach($xiaji as $key => $value){
                $biao = \common\models\hy_designerModel::createDesigner($value['uid'], 0);
                if($biao['max_levels'] <= $levels){
                    throw new Exception("下级身份过大,不允许被设置");
                }                
            }
        }
        
        
        $this->do_designer = $do_designer = \common\models\hy_designerModel::createDesigner($this->do_uid, 0);
        
        if($admin_model['role_id'] == 1){//超管身份则肯定允许操作
            return true;
        }
        
        
        
        $this->operation_designer = $operation_designer = \common\models\hy_designerModel::createDesigner($this->operation_uid, 1);
        
        if($do_designer && $do_designer['designer_user']['studio_id'] != $operation_designer['designer_user']['studio_id']){//用户原先有工作室 且设置的工作室跟原先的工作室不一样
            throw new Exception("原先工作室与所设置的不一致");
        }else if(!$do_designer){//原先没有工作室
            return true;
        }
        
        
        //原先身份与 设置我的人的身份对比
        if($do_designer['max_levels'] <= $operation_designer['max_levels']){
            throw new Exception("设置的人身份不符合要求");
        }
        
        //设置成什么身份  什么工作室  这个再要清楚 对比
        if($this->studio_id != $do_designer['designer_user']['studio_id']){
            throw new Exception("不能设置成其它的公司团队");
        }
        
        
        if($levels <= $operation_designer['max_levels']){
            throw new Exception("不能设置自己平台或更高身份");
        }
        
        return true;
    }
    
    public function getRole()
    {
        //设置为这个角色，这个角色其最大角色的身份是什么
        $type = \common\models\hy_studio_levelModel::find()->asArray()->where(['id' => $this->studio_level])->select("type")->orderBy("level asc")->scalar();
        
        switch ($type) {
            case 1://管理员
                return 37;//团队管理员
            case 2://业务
                return 36;//业务
            case 3://设计师
                return 33;//设计师
            case 4://施工人员
                return 34;//施工人员
            case 5://养护员
                return 35;//养护员
            case 6://养护员
                return 38;//养护员
            default:
                return 36;//业务
        }
    }
}

