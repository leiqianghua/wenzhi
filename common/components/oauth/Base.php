<?php
/*
 * 雷强华 第三方联合登录 oauth2 基础方法
 */
namespace common\components\oauth;
class Base extends \yii\base\Object
{
    protected $error;//错误的提示信息
    protected $config;//配置的信息
    protected function setError($data)
    {
        $this->error = $data;
    }
    public function getError()
    {
        return $this->error;
    }
    public function setConfig($config = [])
    {
        $this->config = $config;
    }
    
    
    //获取要请求code的url
    public function getCodeUrl()
    {
        return true;
    }
    
    //获取access_token
    public function getAccess_token()
    {
        return true;
    }
    
    
    //更新Access_token
    public function updateAccess_token()
    {
        return true;
    }
    
    
    //获取openID   / reg_key
    public function getOpenID()
    {
        return true;
    }
    
    //获取会员信息
    public function getUserinfo()
    {
        return true;
    }        
}