<?php
/* 
 * QQ第三方应用
 * 由于调用接口有限制，一天最多可以调取的数量有限制，所以需要自行的保存好相关的acces_token
 */
namespace common\components\oauth;
use common\components\Curl;
use common\components\GuestInfo;
use Exception;
class QQ extends Base
{
    protected $config = [
        'appid' => "101310820",
        'appsecret' => "49bd9003eca202d4e3afb6e6182e3ce2",
        'callback' => "http://www.gubatoo.com/wap/oauth/callback_qq",
        'scope' => "get_user_info,add_t,add_pic_t",
    ];
    public $session_array = [];//通过CURL请求获取到的参数
    public function init()
    {
        parent::init();
        $config = \yii::$app->params['sanfan']['qq'];
        $this->config = array_merge($this->config,$config);
    }
    
    //获取需要请求的url
    public function getCodeUrl()
    {
        $state= md5(GuestInfo::getIpForLvs());
        $url = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id={$this->config['appid']}&redirect_uri=" . urlencode($this->config['callback']) . "&state={$state}&scope={$this->config['scope']}";   
        return $url;
    }
    //获取access_token
    //data 参数包括 code state  可以不传，不传的话是通过get参数获取
    //正确则返回  数据  access_token，expires_in，refresh_token 三个键  赋值给$this->session_array
    public function getAccess_token()
    {
        try {
            $code = $_GET['code'];
            $state = $_GET['state'];
            if(empty($code) || empty($state)){
                throw new Exception('参数出错');
            }
            if($state != md5(GuestInfo::getIpForLvs())){
                throw new Exception('登录授权状态出现错误');
            }
            $url = "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&client_id={$this->config['appid']}&client_secret={$this->config['appsecret']}&code={$code}&state={$state}&redirect_uri=" . urlencode($this->config['callback']);
            $curl = new Curl();
            $result = $curl->get($url);
            $params = array();
            parse_str($result, $params);
            if(empty($params['access_token'])){
                throw new Exception('获取不到登录授权信息');
            }            
            $session_array["access_token"] = $params["access_token"];
            $session_array['expires_in'] = intval(time()) + intval($params["expires_in"]);  
            $session_array['refresh_token'] = $params["refresh_token"];
            $this->session_array = $session_array;
            return true;
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    
    
    
    
    
    //刷新时间并重新获取相应的access_token
    ////正确则返回  数据  access_token，expires_in，refresh_token 三个键  赋值给$this->session_array
    public function updateAccess_token()
    {
        try {
            if(empty($this->session_array['refresh_token'])){
                throw new Exception('授权参数出错');
            }
            $url = "https://graph.qq.com/oauth2.0/token?grant_type=refresh_token&client_id={$this->config['appid']}&client_secret={$this->config['appsecret']}&refresh_token={$this->session_array['refresh_token']}";
            $curl = new Curl();
            $result = $curl->get($url);
            $params = array();
            parse_str($result, $params);
            if(empty($params['access_token'])){
                throw new Exception('获取不到登录授权信息');
            }
            $session_array["access_token"] = $params["access_token"];
            $session_array['expires_in'] = intval(time()) + intval($params["expires_in"]);  
            $session_array['refresh_token'] = $params["refresh_token"];
            $this->session_array = $session_array;
            return true;
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }      
    }
    //通过参数获取openid  如果有数据的话直接返回相应的数据
    public function getOpenID()
    {
        try {
            $url = "https://graph.qq.com/oauth2.0/me?access_token={$this->session_array["access_token"]}";
            $curl = new Curl();
            $str = $curl->get($url);
            preg_match('/callback\(\s+(.*?)\s+\)/i', $str, $aTemp);
            $user = json_decode($aTemp[1], true);   
            if(empty($user['client_id']) || empty($user['openid'])){
                throw new Exception('获取不到用户信息');
            }
            $result = [];
            $result['openid'] = $user['openid'];
            $result['expires_in'] = $this->session_array['expires_in'];
            $result['access_token'] = $this->session_array['access_token'];
            $this->session_array = $result;
            
        
            
            return true;
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    
//    { 
//    "access_token":"ACCESS_TOKEN", 
//    "expires_in":7200, 
//    "refresh_token":"REFRESH_TOKEN", 
//    "openid":"OPENID", 
//    "scope":"SCOPE" 
//    }    
    //通过access_token的返回信息更新相应的表数据
    public function getuserInfo()
    {
        try {
            $url = "https://graph.qq.com/user/get_user_info?";
            $aGetParam = array(
                "access_token" => $this->session_array['access_token'],
                "oauth_consumer_key" => $this->config['appid'],
                "openid" => $this->session_array['openid'],
                "format" => "json"
            );
            $curl = new Curl();
            $user_info = $curl->get($url . http_build_query($aGetParam));  
            $user_info = json_decode($user_info, true);
            if(empty($user_info)){
                $this->error = '获取不到用户会员信息';
                return false;
            }            
            //$this->session_array = $user_info;
            return $user_info;             
            /***************  保存用户信息  *********************/
            /*****  返回的会员信息的键值对
            ret 返回码
            msg 如果ret<0，会有相应的错误信息提示，返回数据全部用UTF-8编码。
            nickname    用户在QQ空间的昵称。
            figureurl   大小为30×30像素的QQ空间头像URL。
            figureurl_1 大小为50×50像素的QQ空间头像URL。
            figureurl_2 大小为100×100像素的QQ空间头像URL。
            figureurl_qq_1  大小为40×40像素的QQ头像URL。
            figureurl_qq_2  大小为100×100像素的QQ头像URL。需要注意，不是所有的用户都拥有QQ的100x100的头像，但40x40像素则是一定会有。
            gender  性别。 如果获取不到则默认返回"男"
            is_yellow_vip   标识用户是否为黄钻用户（0：不是；1：是）。
            vip 标识用户是否为黄钻用户（0：不是；1：是）
            yellow_vip_level    黄钻等级
            level   黄钻等级
            is_yellow_year_vip  标识是否为年费黄钻用户（0：不是； 1：是）    
            ***/      
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }    
}

