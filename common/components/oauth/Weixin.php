<?php

/* 
 * 由于调用接口有限制，一天最多可以调取的数量有限制，所以需要自行的保存好相关的acces_token
 * //获取code 且 获取Access_token 然后直接与库里面对比，如果存在的话则是直接取到相应的数据       如果不存在则所有都是通过接口来获取相应的数据并保存
 */
namespace common\components\oauth;
use common\components\Curl;
use common\components\GuestInfo;
use Exception;
class Weixin extends Base
{
    protected $config = [
        'appid' => "wx2b9f9f871029a93f",
        'appsecret' => "0f632eff5155ac87ca702c1dac4eee49",
        //'token' => 'H1VU0f0hCbpFBQTc', //微信服务号交互token[自定义]
        //'sender' => 'gh_c07127775924', //微信原始ID,发送消息时的发送者
        'callback' => "http://j.china.com/login/callback_weixin.html",
    ];    
    public $session_array = [];//通过CURL请求获取到的参数
    
    
    public function init()
    {
        parent::init();
        $config = \yii::$app->params['sanfan']['weixin'];
        $this->config = array_merge($this->config,$config);
        
        if (GuestInfo::isWeixinRequest()) {
            $this->config['scope'] = 'snsapi_userinfo';
        } else {
            $this->config['scope'] = 'snsapi_login,snsapi_userinfo';
        }        
    }    
  
    //请求code
    public function getCodeUrl()
    {
        $state= md5(GuestInfo::getIpForLvs());
        $url = "https://open.weixin.qq.com/connect/qrconnect?response_type=code&appid={$this->config['appid']}&redirect_uri="
                . urlencode($this->config['callback'])
                . "&state={$state}"
                . "&scope={$this->config['scope']}#wechat_redirect";        
        //$url = "https://open.weixin.qq.com/connect/qrconnect?appid={$config['appid']}&redirect_uri=".urlencode($config['callback'])."&response_type=code&scope={$config['scope']}&state={$state}#wechat_redirect";
        return $url;
    }
    //参数为 接口返回结果
    //如果已经有保存相应的数据 则返回的是数组   如果没有则返回的是布尔型
    /*
     * 正确的获取$session_array
     * { 
"access_token":"ACCESS_TOKEN", 
"expires_in":7200, 
"refresh_token":"REFRESH_TOKEN",
"openid":"OPENID", 
"scope":"SCOPE",
"unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
}
     */
    public function getAccess_token()
    {
        try {
            $code = $_GET['code'];
            $state = $_GET['state'];
            if(empty($code) || empty($state)){
                throw new Exception('参数出错');
            }
            if($state != md5(GuestInfo::getIpForLvs())){
                throw new Exception('登录授权状态出现错误');
            }
            $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$this->config['appid']}&secret={$this->config['appsecret']}&code={$code}&grant_type=authorization_code"; 
            $curl = new Curl();
            $result = $curl->get($url);
            $result = json_decode($result,true);
            if(empty($result['access_token'])){
                throw new Exception('获取不到登录授权信息');
            }
            $this->session_array = $result;

            
            return true;
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    

    
    //刷新时间并重新获取相应的access_token
    //如果带unionid的话有带
    public function updateAccess_token()
    {
        try {
            $url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={$this->config['appid']}&grant_type=refresh_token&refresh_token={$this->session_array['refresh_token']}";
            $curl = new Curl();
            $result = $curl->get($url);
            $result = json_decode($result,true);  
            if(empty($result['access_token'])){
                throw new Exception('获取不到登录授权信息');
            }
            $result['unionid'] = $this->session_array['unionid'];
            $this->session_array =  $result;    
            return true;
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }      
    }

    /*
     * 正确的返回结果
     * { 
"openid":"OPENID",
"nickname":"NICKNAME",
"sex":1,
"province":"PROVINCE",
"city":"CITY",
"country":"COUNTRY",
"headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
"privilege":[
"PRIVILEGE1", 
"PRIVILEGE2"
],
"unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"

}
     */
    //通过access_token的返回信息更新相应的表数据
    public function getUserinfo()
    {
        try {
            //访问相应的接口
            $url  = "https://api.weixin.qq.com/sns/userinfo?access_token={$this->session_array['access_token']}&openid={$this->session_array['openid']}";
            $curl = new Curl();
            $user_info = $curl->get($url);
            $user_info = json_decode($user_info,true);
            if(empty($user_info['nickname'])){//获取不到
                $this->updateAccess_token();
            }
            
            $url  = "https://api.weixin.qq.com/sns/userinfo?access_token={$this->session_array['access_token']}&openid={$this->session_array['openid']}";
            $curl = new Curl();
            $user_info = $curl->get($url);
            $user_info = json_decode($user_info,true);
            if(empty($user_info['nickname'])){//获取不到
                $this->error = '获取个人信息出错';
                return false;
            }            
            
            $this->session_array = array_merge($this->session_array,$user_info);
            return $this->session_array;            
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }    
}

