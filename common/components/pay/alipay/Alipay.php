<?php

/**
 * 支付宝接口类，具体的参与逻辑的运算的一个类
 *
 * @copyright  Copyright (c) 2007-2013 Yzw Inc. (http://www.1zw.com)
 * @license    http://www.1zw.com
 * @link       http://www.1zw.com
 * @since      File available since Release v1.1
 */
namespace component\pay\alipay;
class Alipay {

    /**
     * 支付宝网关地址（新）
     */
    private $alipay_gateway_new = 'https://mapi.alipay.com/gateway.do?';

    /**
     * 消息验证地址
     *
     * @var string
     */
    private $alipay_verify_url = 'https://mapi.alipay.com/gateway.do?service=notify_verify&';


    /**
     * 支付接口配置信息
     *
     * @var array
     */
    private $payment = [];


    /**
     * 发送至支付宝的参数
     *
     * @var array
     */
    public $parameter;


    //错误信息
    private $error = NULL;

    
    public function getParameterConfig()
    {
        $alipay_config = require_once("alipay.config.php");//获取配置
        $parameter = array(
            'service' => $alipay_config['alipay_service'], //服务名
            'partner' => $alipay_config['alipay_partner'], //合作伙伴ID
            'key' => $alipay_config['alipay_key'],
            'seller_email' => $alipay_config['alipay_account'], //卖家邮箱
            '_input_charset' => 'UTF-8', //网站编码
            'sign_type' => 'MD5', //签名方式
            'receive_address' => 'N', //收货人地址
            'receive_zip' => 'N', //收货人邮编
            'receive_phone' => 'N', //收货人电话
            'receive_mobile' => 'N', //收货人手机
            'quantity' => 1, //商品数量
            'total_fee' => 0, //物流配送费用
            'extend_param' => "isv^sh32",
            'payment_type' => 1, //支付类型
            'logistics_type' => 'EXPRESS', //物流配送方式：POST(平邮)、EMS(EMS)、EXPRESS(其他快递)
            'logistics_payment' => 'BUYER_PAY', //物流费用付款方式：SELLER_PAY(卖家支付)、BUYER_PAY(买家支付)、BUYER_PAY_AFTER_RECEIVE(货到付款)
        );
        return $parameter;
    }
    
    
    
    /**
     *
     * @param string $name
     * @return
     */
    public function __get($name)
    {
        return $this->$name;
    }

    
    
    
    public function __construct()
    {
        $alipay_config = require_once("alipay.config.php");
        $alipay_config['alipay_service'] = 'create_direct_pay_by_user';
        $this->payment['payment_config'] = $alipay_config;
        //是否支持SSL
        if (!extension_loaded('openssl')) {
            $this->alipay_verify_url = 'http://notify.alipay.com/trade/notify_query.do?';
        }
    }

    //获取错误信息
    public function getError()
    {
        return $this->error;
    }


    /**
     * 通知地址验证
     *
     * @return bool
     */
    public function notify_verify()
    {
        $param = $_POST;
        $param['key'] = $this->payment['payment_config']['alipay_key'];
        $veryfy_url = $this->alipay_verify_url . "partner=" . $this->payment['payment_config']['alipay_partner'] . "&notify_id=" . $param["notify_id"];
        $veryfy_result = $this->getHttpResponse($veryfy_url);
        $mysign = $this->sign($param);
        if (preg_match("/true$/i", $veryfy_result) && $mysign == $param["sign"]) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 返回地址验证
     *
     * @return bool
     */
    public function return_verify()
    {
        $param = $_GET;
        $param['act'] = ""; //将系统的控制参数置空，防止因为加密验证出错
        $param['op'] = "";
        $param['key'] = $this->payment['payment_config']['alipay_key'];
        $veryfy_url = $this->alipay_verify_url . "partner=" . $this->payment['payment_config']['alipay_partner'] . "&notify_id=" . $param["notify_id"];
        $veryfy_result = $this->getHttpResponse($veryfy_url);
        $mysign = $this->sign($param);
        if (preg_match("/true$/i", $veryfy_result) && $mysign == $param["sign"]) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * 远程获取数据
     * $url 指定URL完整路径地址
     * @param $time_out 超时时间。默认值：60
     * return 远程输出的数据
     */
    private function getHttpResponse($url, $time_out = "60")
    {
        $urlarr = parse_url($url);
        $errno = "";
        $errstr = "";
        $transports = "";
        $responseText = "";
        if ($urlarr["scheme"] == "https") {
            $transports = "ssl://";
            $urlarr["port"] = "443";
        } else {
            $transports = "tcp://";
            $urlarr["port"] = "80";
        }
        $fp = @fsockopen($transports . $urlarr['host'], $urlarr['port'], $errno, $errstr, $time_out);
        if (!$fp) {
            die("ERROR: $errno - $errstr<br />\n");
        } else {
            if (trim(CHARSET) == '') {
                fputs($fp, "POST " . $urlarr["path"] . " HTTP/1.1\r\n");
            } else {
                fputs($fp, "POST " . $urlarr["path"] . '?_input_charset=' . CHARSET . " HTTP/1.1\r\n");
            }
            fputs($fp, "Host: " . $urlarr["host"] . "\r\n");
            fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-length: " . strlen($urlarr["query"]) . "\r\n");
            fputs($fp, "Connection: close\r\n\r\n");
            fputs($fp, $urlarr["query"] . "\r\n\r\n");
            while (!feof($fp)) {
                $responseText .= @fgets($fp, 1024);
            }
            fclose($fp);
            $responseText = trim(stristr($responseText, "\r\n\r\n"), "\r\n");
            return $responseText;
        }
    }

    /**
     * 制作支付接口的请求地址
     *
     * @return string
     */
    private function create_url($config)
    {
        
        $con = $this->getParameterConfig();
        
        $this->parameter = array_merge($con,$config);
        //签名
        $this->parameter['sign'] = $this->sign($this->parameter);
        
        
        $url = $this->alipay_gateway_new;
        $filtered_array = $this->para_filter($this->parameter);
        $sort_array = $this->arg_sort($filtered_array);
        $arg = "";
        while (list ($key, $val) = each($sort_array)) {
            $arg .= $key . "=" . urlencode($val) . "&";
        }
        $url .= $arg . "sign=" . $this->parameter['sign'] . "&sign_type=" . $this->parameter['sign_type'];
        return $url;
    }

    /**
     * 取得支付宝签名
     *
     * @return string
     */
    private function sign($parameter)
    {
        $mysign = "";
        $filtered_array = $this->para_filter($parameter);
        $sort_array = $this->arg_sort($filtered_array);
        $arg = "";
        while (list ($key, $val) = each($sort_array)) {
            $arg .= $key . "=" . $this->charset_encode($val, (empty($parameter['_input_charset']) ? "UTF-8" : $parameter['_input_charset']), (empty($parameter['_input_charset']) ? "UTF-8" : $parameter['_input_charset'])) . "&";
        }
        $prestr = substr($arg, 0, -1);  //去掉最后一个&号
        $prestr .= $parameter['key'];
        if ($parameter['sign_type'] == 'MD5') {
            $mysign = md5($prestr);
        }
        return $mysign;
    }

    /**
     * 除去数组中的空值和签名模式
     *
     * @param array $parameter
     * @return array
     */
    private function para_filter($parameter)
    {
        $para = array();
        while (list ($key, $val) = each($parameter)) {
            if ($key == "sign" || $key == "sign_type" || $key == "key" || $val == "")
                continue;
            else
                $para[$key] = $parameter[$key];
        }
        return $para;
    }

    /**
     * 重新排序参数数组
     *
     * @param array $array
     * @return array
     */
    private function arg_sort($array)
    {
        ksort($array);
        reset($array);
        return $array;
    }

    /**
     * 实现多种字符编码方式
     */
    private function charset_encode($input, $_output_charset, $_input_charset = "UTF-8")
    {
        $output = "";
        if (!isset($_output_charset))
            $_output_charset = $this->parameter['_input_charset'];
        if ($_input_charset == $_output_charset || $input == null) {
            $output = $input;
        } elseif (function_exists("mb_convert_encoding")) {
            $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
        } elseif (function_exists("iconv")) {
            $output = iconv($_input_charset, $_output_charset, $input);
        } else
            die("sorry, you have no libs support for charset change.");
        return $output;
    }

}
