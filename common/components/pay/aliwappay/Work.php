<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 进行相关的工作的class
 */
namespace component\pay\aliwappay;
use component\pay\alipay\Aliwappay;
class Work
{
//    配置信息需要的键
//    $this->config = array(
//            'notify_url' => SHOP_SITE_URL . "/api/pay/aliwappay/notify_url.php", //通知URL
//            'return_url' => SHOP_SITE_URL . "/api/pay/aliwappay/return_url.php", //返回URL
//            'body' => $this->order['order_type'], //product_buy商品购买,predeposit预存款充值
//            'subject' => $this->order['subject'], //商品名称
//            'out_trade_no' => $this->order['pay_sn'], //外部交易编号
//
//            'total_fee' => (defined('DEBUG') && DEBUG === true) ? 0.01 : $this->order['pay_amount'], //订单总价
//    );      
    public $config;
    public $obj_alipay = null;//默认的一个数据
    public $error;
    
    //设置获取相关的配置数据信息
    public function __construct($config = []) 
    {
        $this->config = array_merge($this->config, $config);
        $this->obj_alipay = new Aliwappay();
    }
    
    //获取需要跳转的一个URL
    public function get_payurl()
    {
        return $this->obj_alipay->create_url($this->config);
    }
    
    //支付宝异步回调
    public function alipayNotify()
    {
        $state = $this->obj_alipay->notify_verify();//验证
        if($state){
            if ($_POST['trade_status'] == 'TRADE_FINISHED') {
                exit('success');
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                $this->orderProcessing();
                exit('success');
            }
        }else{
            exit('fail');
        }
    }    
    
    //支付宝同步回调
    public function alipayReturn()
    {
        try {
            if ($this->obj_alipay->return_verify() !== true) {
                throw new Exception('数据验证不通过');
            }
            $trade_status = $_GET['trade_status'];
            if ($trade_status == 'TRADE_FINISHED' || $trade_status == 'TRADE_SUCCESS') {
                $this->orderProcessing();
                return true;
            }else{
                throw new Exception('支付状态出错');
            }         
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    
    
    /**
     * 支付宝支付完成后订单处理
     * @param type $out_trade_no 商户订单号
     * @param type $trade_no 支付宝交易号
     * @param type $total_fee  支付金额
     * @param type $extra_common_param 用户自定义上传的参数
     */
    private function orderProcessing(){
        $params = empty($_POST['out_trade_no']) ? $_GET : $_POST;   
    }    
}
