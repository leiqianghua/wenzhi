<?php

//与pc的支付宝对比多了一个shop_url需要配置的
namespace common\components\pay\run;
use AlipaySubmit;
use AlipayNotify;

class Alipaywap extends Base
{
    public $out_trade_no;//商户的订单号
    public $subject;//订单的名称
    public $total_fee;//订单金额
    public $body;//商品描述
    public $show_url;//收银台页面上，商品展示的超链接，必填
    
    
    public $alipay_config;//支付宝相关的配置参数
    
    public function init()
    {
        parent::init();
        require_once(__DIR__."/../alipaywap/alipay.config.php");
        require_once(__DIR__."/../alipaywap/lib/alipay_submit.class.php");
        require_once(__DIR__."/../alipaywap/lib/alipay_notify.class.php");
        $this->alipay_config = $this->initConfig();
        $this->alipay_config = array_merge($alipay_config, $this->alipay_config);//覆盖配置
    }
    
    protected function initConfig()
    {
        return \yii::$app->params['pay']['alipaywap'];
    }
    

    //获取HTML并打印，打印这个就可以直接跳到支付的相关的页面
    public function getHmtl()
    {
        $alipay_config = $this->alipay_config;
        $out_trade_no = $this->out_trade_no;//商户订单号
        $subject = $this->subject;//订单名称
        $total_fee = $this->total_fee;//订单金额
//        if(YII_DEBUG === true){
//            $total_fee = 0.01;
//        }
        $body = $this->body;//商品描述
        $show_url = $this->show_url;//商品描述
        
        $parameter = [
            "service"            => $alipay_config['service'],
            "partner"            => $alipay_config['partner'],
            "seller_id"          => $alipay_config['seller_id'],
            "payment_type"       => $alipay_config['payment_type'],
            "notify_url"         => $alipay_config['notify_url'],
            "return_url"         => $alipay_config['return_url'],
            "_input_charset"	=> trim(strtolower($alipay_config['input_charset'])),
            "out_trade_no"	 => $out_trade_no,
            "subject"            => $subject,
            "total_fee"          => $total_fee,
            "show_url"          => $show_url,
            "body"	         => $body,
        ]; 
        
        $alipaySubmit = new AlipaySubmit($alipay_config);
        $html_text = $alipaySubmit->buildRequestForm($parameter,"get", "确认");
        echo $html_text;                
    }
    
    
    //回调的处理,返回正确或者是错误的结果//异步的通知
    //异步通知，如果处理成功则打印success 并退出
    //否则打印fail并退出
    public function notify_url()
    {
        $alipay_config = $this->alipay_config;
        
        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();    
        
        if($verify_result){//验证成功
            $out_trade_no = $_POST['out_trade_no'];
            $trade_no = $_POST['trade_no'];
            $trade_status = $_POST['trade_status'];
            
            $total_fee = $_POST['total_fee'];//金额
            
            if($_POST['trade_status'] == 'TRADE_FINISHED') {
                $this->orderProcessing($out_trade_no, $trade_no, $total_fee,'alipaywap');
            }else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                $this->orderProcessing($out_trade_no, $trade_no, $total_fee,'alipaywap');
            }
            echo "success";    
            exit;
        }else{//验证失败
            \common\components\TmpLog::saveData('/tmp/alipaywap_error_log', $_POST);
            echo "fail";
            exit;
        }
    }
    
    //回调的处理,返回正确或者是错误的结果//同步通知
    //如果处理成功则返回true  失败则返回false
    public function return_url()
    {
        $alipay_config = $this->alipay_config;
        
        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyReturn();  
        
        if($verify_result){//验证成功
            $out_trade_no = $_GET['out_trade_no'];

            //支付宝交易号

            $trade_no = $_GET['trade_no'];

            //交易状态
            $trade_status = $_GET['trade_status'];

            $total_fee = $_GET['total_fee'];//金额
            if($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
                //验证成功
                $this->orderProcessing($out_trade_no, $trade_no, $total_fee,'alipaywap');
                return true;
            }else {
                //验证失败
                $data = array_merge($_POST, $_GET);
                \common\components\TmpLog::saveData('/tmp/alipay_error_log', $data);
                return false;
            }
        }else{
            //验证失败
            $data = array_merge($_POST, $_GET);
            \common\components\TmpLog::saveData('/tmp/alipay_error_log', $data);
            return false;
        }
    }
    

}