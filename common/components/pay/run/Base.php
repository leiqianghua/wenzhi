<?php

namespace common\components\pay\run;
use yii;
class Base extends \yii\base\Object
{
    public $error;
    
    public function orderProcessing($out_trade_no,$trade_no,$total_fee,$pay_type)
    {
        try {
            $cache_key = "orderProcessing_{$out_trade_no}";
            $state = \common\components\Tool::checkLock($cache_key);
            if(!$state){
                \common\components\TmpLog::addData("支付回调", $out_trade_no, "");
                return false;
            }
            $model = \common\models\MemberAddmoneyRecordModel::findOne($out_trade_no);
            if(!$model || $model['status'] == 1 || $total_fee != $model['money']){
                return false;
            }            
            
            $transaction = Yii::$app->db->beginTransaction();//开启事务   
            

            $address_id = $model['trade_no'];
            
            //变成已处理
            $model->status = 1;
            $model->trade_no = $trade_no;
            $model->pay_type = $pay_type;            
            \common\models\MemberAddmoneyRecordModel::updateAll(["status" => 1,"trade_no" => $trade_no,"pay_type" => $pay_type], ["status" => 0,'code' => $model['code']]);

            $code_begin = substr($model['code'], 0,3);
            //增加金额
            if($code_begin != "C_K"){
                \common\models\MemberMoneyRecordModel::changeMoney(1,$model['money'],$model['consume_type'],$model['code'],$model['uid']);    
            }
            
            
            \common\components\Tool::deleteLock($cache_key);
            $transaction->commit();//提交事务   
            
            //继续触发其它的相关的操作
            
            switch ($code_begin) {
                case "C_A"://活动带来的,则继续去购买这个活动
                    $config = [
                        'detail_data' => $model['scookies'],
                        'type' => 2,
                        'uid' => $model['uid'],
                        'addmoney_order_id' => $model['code'],
                    ];
                    $obj = new \common\components\buy\ActiveBuy($config);
                    $code = $obj->buy();//去支付
                    break;
                case "C_S"://商品购买
                    $config = [
                        'detail_data' => $model['scookies'],
                        'type' => 2,
                        'uid' => $model['uid'],
                        'addmoney_order_id' => $model['code'],
                        'red_ids' => $model['red_ids'],
                        'address_id' => $address_id,
                    ];
                    $obj = new \common\components\buy\GoodsBuy($config);
                    $code = $obj->buy();//去支付
                    break;
                case "C_K"://让其可以为此增加一个让你这个代表付款成功
                    $model_kaidang = \common\models\ElseKaidanModel::findOne(['order_id' => $model['code']]);
                    if($model_kaidang){
                        
                        $model_kaidang->step_has_ok = $model_kaidang->step_has_ok + 1;
                        $model_kaidang->last_setp_is_confirm = 1;
                        
                        if($model_kaidang->step_has_ok == $model_kaidang->step){

                        }else{
                            
                        }
                        $model_kaidang->jiesuan();
 
                        $model_kaidang->save(false);
                        //扣除我们给其收的这个税百分比
//                        $libi = \common\models\ConfigModel::getListForKey("kaidang_bili");
//                        if($libi){
//                            $money = (int)($model_kaidang['zhe_price']/100*$libi);
//                            if($money){
//                                \common\models\MemberMoneyRecordModel::changeMoney(1, $money, "kaidang", $model_kaidang['id'],$model_kaidang['uid']);
//                            }
//                        }
                        
                        //付款成功后通知相关的用户
                        //谁开单的  跟  系统管理员
                        \common\components\TmpLog::addData("uuccbb-u", $model_kaidang->id);
                        $model_kaidang->payAfterNotice();
                        return true;

                    }
                    break;

                default:
                    break;
            }
            
            return true;            
        } catch (\Exception $exc) {
            $this->error = $exc->getMessage();
            \common\components\Tool::deleteLock($cache_key);//释放锁
            $transaction->rollBack();//释放事务
            \common\components\TmpLog::addData("支付回调错误", $this->error, "");
            return false;
        }        
    }
    
    
    
    //订单成功后的处理
    //$out_trade_no 商家的订单号
    //$trade_no 支付宝订单号
    //$total_fee 金额
    //返回false / true
    protected function orderProcessing_bak($out_trade_no,$trade_no,$total_fee)
    {
        try {
            $cache_key = "orderProcessing_{$out_trade_no}";
            $state = \common\components\Tool::checkLock($cache_key);
            if($state){
                return true;
            }
            $transaction = Yii::$app->db->beginTransaction();//开启事务   

            $model = \common\models\OrderModel::find()->where(['order_sn' => $out_trade_no])->limit(1)->one();
            if(!$model || $model['order_state'] != 0){
                return true;
            }
            $model->order_state = 1;
            $model->update_time = $model->payment_time = time();
            $model->payment_code = 'alipaywap';
            $model->trade_sn = $trade_no;
            $model->callback_state = 1;
            $model->save(false);


            //商品销量加1  库存减1
            $datas = \common\models\OrderGoodsModel::find()->where(['order_id' => $model->order_id])->asArray()->all();
//            foreach($datas as $data){
//                if($data['type'] == 1){
//                    $goods_model = \common\models\GoodsModel::findOne($data['goods_id']);
//                    $goods_model->num = $goods_model->num - $data['num'];
//                    $goods_model->sale_num = $goods_model->sale_num + $data['num'];
//                    $goods_model->save(false);                
//                }else{
//                    $shop_list = json_decode($data['shop_list'],true);
//                    foreach($shop_list as $da){
//                        $goods_model = \common\models\GoodsModel::findOne($da['goods_id']);
//                        $goods_model->num = $da['buy_num'];
//                        $goods_model->sale_num = $goods_model->sale_num + $da['buy_num'];
//                        $goods_model->save(false);    
//                    }
//                }
//            }

            //每个人拿到其相应的金额 他的上级，或上上级等这样的级别该拿到的金额
            $uids = \common\models\MemberModel::getParents($model['uid']);//购买者 及其上级的所有的ID
            foreach($uids as $key => $uid){
                $d = \common\models\MemberModel::findOne($uid);
                $uids[$key] = [
                    'uid' => $uid,
                    'label' => $d['label'],
                ];
            }
            if(count($uids) > 1){
                //对相应的会员进行加钱表示的是底下的会员拿到的钱
                foreach($datas as $data){
                    $this->changeMoneyForBuyGoods($data, $uids);
                }                
            }
            \common\components\Tool::deleteLock($cache_key);
            $transaction->commit();//提交事务   
            return true;            
        } catch (\Exception $exc) {
            $this->error = $exc->getMessage();
            \common\components\Tool::deleteLock($cache_key);//释放锁
            $transaction->rollBack();//释放事务
            \common\components\TmpLog::saveData('/tmp/pay_error_log', $this->error);
            return false;
        }
        
    }
    
    //通过购买的订单商品，会员的级别来给其相应的增加钱
    //$data 为订单商品
    //$uids 为这个用户的所有的上级，包括他自己
    public  function changeMoneyForBuyGoods($data,$uids)
    {
        if($data['type'] == 1){
            $goods_model = \common\models\GoodsModel::findOne($data['goods_id']);
            for($i = 0;$i < count($uids);$i++){
                $m = $i + 1;
                if(isset($uids[$m])){//存在第二个的
                    if($uids[$i]['label'] > $uids[$m]['label']){
                        $key1 = "discount_{$uids[$i]['label']}";
                        $key2 = "discount_{$uids[$m]['label']}";
                        $money = ($goods_model[$key1] - $goods_model[$key2])*$data['num'];
                        \common\models\MemberMoneyRecordModel::changeMoney(1, $money, '拥金', $data['id'], $uids[$m]['uid']);                        
                    }
                }
            }
        }else{
            $shop_list = json_decode($data['shop_list'],true);
            foreach($shop_list as $da){
                $goods_model = \common\models\GoodsModel::findOne($da['goods_id']);
                for($i = 0;$i < count($uids);$i++){
                    $m = $i + 1;
                    if($uids[$m]){//存在第二个的
                        if($uids[$i]['label'] > $uids[$m]['label']){
                            $key1 = "discount_{$uids[$i]['label']}";
                            $key2 = "discount_{$uids[$m]['label']}";
                            $money = ($goods_model[$key1] - $goods_model[$key2])*$data['num'];
                            \common\models\MemberMoneyRecordModel::changeMoney(1, $money, '拥金', $data['id'], $uids[$m]['uid']);                        
                        }
                    }
                }                  
            }
        }
        return true;
    }    
}