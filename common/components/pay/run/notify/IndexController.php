<?php
//各个的通知在这里写
//包含微信 支付宝等
namespace common\components\pay\run\notify;

use common\components\pay\run\Alipay;//支付宝
use common\components\pay\run\Alipaywap;//支付宝wap
use common\components\pay\run\wxpay\Saoma;//扫码
use common\components\pay\run\wxpay\jsapi\Jsapi;//JSAPI

class IndexController extends \common\components\BaseController
{
    //支付宝的异步回调通知
    public function actionAlipay_notify_url()
    {
        $obj = new Alipay();
        $obj->notify_url();
    }
    
    //支付宝的同步回调通知
    public function actionAlipay_return_url()
    {
        $obj = new Alipay();
        $state = $obj->return_url();        
        if($state){
            $this->success('支付成功',$this->to(['/wap/index/index']));
        }else{
            $this->error($obj->error);
        }
    }
    
    //支付宝wap的异步回调通知
    public function actionAlipay_wap_notify_url()
    {
        \common\components\TmpLog::addData("alipay_pay", "111111");
        $obj = new Alipaywap();
        $obj->notify_url();        
    }
    
    //支付宝wap的同步回调通知
    public function actionAlipay_wap_return_url()
    {
        \common\components\TmpLog::addData("alipay_pay", "111111");
        $obj = new Alipaywap();
        $state = $obj->return_url();        
        if($state){
            $this->success('支付成功',$this->to(['/wap/index/index']));
        }else{
            $this->error($obj->error);
        }        
    }
    
    //微信扫码
    public function actionWx_saoma()
    {
        $obj = new Saoma();
        $obj->notify();
    }
    
    //微信JS api的回调
    public function actionWx_jspai()
    {
        $obj = new Jsapi();
        $obj->notify();        
    }
    
}