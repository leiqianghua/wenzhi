<?php

class PayNotifyCallBack extends WxPayNotify
{
        public $jsapi_obj = null;
        public function initData($jsapi_obj)
        {
            $this->jsapi_obj = $jsapi_obj;
        }
    
	//查询订单
	public function Queryorder($transaction_id)
	{
		$input = new WxPayOrderQuery();
		$input->SetTransaction_id($transaction_id);
		$result = WxPayApi::orderQuery($input);
		Log::DEBUG("query:" . json_encode($result));
		if(array_key_exists("return_code", $result)
			&& array_key_exists("result_code", $result)
			&& $result["return_code"] == "SUCCESS"
			&& $result["result_code"] == "SUCCESS")
		{
			return true;
		}
		return false;
	}
	
	//重写回调处理函数
        //成功返回true  失败返回false
	public function NotifyProcess($data, &$msg)
	{
            //这里处理正常的逻辑操作
            return $this->jsapi_obj->NotifyProcess($data, $msg,$this);//回到此对象处理操作       
	}
}