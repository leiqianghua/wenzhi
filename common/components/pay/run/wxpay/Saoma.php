<?php
//扫码支付
namespace common\components\pay\run\wxpay;
use common\components\pay\run\Base;
class Saoma extends Base
{
    public $body = 'text';//标题
    public $attach = 'text';//可以不用更新，固定值
    public $out_trade_no = 'text';//订单号
    public $total_fee = 3;//金额
    public $goods_tag = 'text';//可以固定的值
    public $notify_url = 'text';//回调的链接    
    
    public function init()
    {
        parent::init();
        ini_set('date.timezone','Asia/Shanghai');
        require_once("../../wxpay/lib/WxPay.Api.php");
        require_once("../../wxpay/example/WxPay.NativePay.php");
    }
    
    //模式一 获取url
    //模式一
    /**
     * 流程：
     * 1、组装包含支付信息的url，生成二维码
     * 2、用户扫描二维码，进行支付
     * 3、确定支付之后，微信服务器会回调预先配置的回调地址，在【微信开放平台-微信支付-支付配置】中进行配置
     * 4、在接到回调通知之后，用户进行统一下单支付，并返回支付信息以完成支付（见：native_notify.php）
     * 5、支付完成之后，微信服务器会通知支付成功
     * 6、在支付成功通知中需要查单确认是否真正支付成功（见：notify.php）
     */    
    protected function getUrl1()
    {
        $notify = new NativePay();
        $url1 = $notify->GetPrePayUrl("123456789");       
        return $url1;
    }
    
    //模式二 获取url
    //模式二
    /**
     * 流程：
     * 1、调用统一下单，取得code_url，生成二维码
     * 2、用户扫描二维码，进行支付
     * 3、支付完成之后，微信服务器会通知支付成功
     * 4、在支付成功通知中需要查单确认是否真正支付成功（见：notify.php）
     */    
    protected function getUrl2()
    {   
        $notify = new NativePay();           
        $input = new WxPayUnifiedOrder();

        $input->SetBody($this->body);
        $input->SetAttach($this->attach);
//        if(YII_DEBUG === true){
//            $this->total_fee = 0.01;
//        }        
        $input->SetOut_trade_no($this->out_trade_no);
        $input->SetTotal_fee($this->total_fee);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag($this->goods_tag);
        $input->SetNotify_url($this->notify_url);
        $input->SetTrade_type("NATIVE");
        $input->SetProduct_id("123456789");
        $result = $notify->GetPayUrl($input);
        $url2 = $result["code_url"];   
        return $url2;
    }
    
    //获取二维码,一般我们要模式2，模式1的话还需要去系统sdk里面配置相关的东西
    public function twoCode($type = 2)
    {
        if($type == 1){
            $url = $this->getUrl1();
        }else{
            $url = $this->getUrl2();
        }
        $url = "http://paysdk.weixin.qq.com/example/qrcode.php?data=" . urlencode($url);
        return $url;
    }
    
    //回调通知
    public function notify()
    {
        require_once "PayNotifyCallBack.php";
        $notify = new PayNotifyCallBack();
        $notify->initData($this);
        $notify->Handle(false);    
    }
    
    //回调的处理操作
    //$data 包含  transaction_id  out_trade_no  attach 等这些参数
    public function NotifyProcess($data, &$msg)
    {
        try {
            if(empty($data['transaction_id']) || empty($data['out_trade_no'])){
                throw new Exception("参数不正确,transaction_id商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            }

            //查询订单，判断订单真实性
            if (!$this->Queryorder($data['transaction_id'])) {//这里查询可能是使用out_trade_no的参数，到时候需要试验一下
                throw new Exception("订单查询失败！商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            }
            if ($data['result_code'] !== 'SUCCESS') {
                throw new Exception("支付失败！商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            }    
            
            //这里处理正常的逻辑操作
            return $this->orderProcessing($data['out_trade_no'], $data['transaction_id'],$data['total_fee'],'saoma');//回到此对象处理操作

        } catch (Exception $exc) {
            \common\models\ErrorLogModel::addData("wxpay_NotifyProcess",$exc->getMessage());
            return false;
        }
    }


}