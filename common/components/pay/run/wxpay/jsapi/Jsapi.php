<?php
/**
 * 微信JSAPI支付
 */
namespace common\components\pay\run\wxpay\jsapi;
use common\components\pay\run\Base;
use JsApiPay;
use WxPayUnifiedOrder;
use PayNotifyCallBack;
use Exception;
use WxPayApi;
class Jsapi extends Base
{
    
    public $body = 'text';//标题
    public $attach = 'text';//可以不用更新，固定值
    public $out_trade_no = 'text';//订单号
    public $total_fee = 3;//金额
    public $goods_tag = 'text';//可以固定的值
    public $notify_url = 'text';//回调的链接
    
    public $ming_return_url = 'gsdfsdf';//成功后我们自己的回调 
    
    
    
    public function init()
    {
        parent::init();
        ini_set('date.timezone','Asia/Shanghai');
        //error_reporting(E_ERROR);
        require_once __DIR__."/../../../wxpay/lib/WxPay.Api.php";
        require_once __DIR__."/../../../wxpay/example/WxPay.JsApiPay.php";
        $this->notify_url = \yii::$app->params['pay']['jsapi_weixin']['notify_url'];//回调 的地址
    }
    
    protected function getJsApiParameters()
    {
        //①、获取用户openid
        $tools = new JsApiPay();
        $openId = $tools->GetOpenid();

        //②、统一下单
        $input = new WxPayUnifiedOrder();
        $input->SetBody($this->body);
        $input->SetAttach($this->attach);
        $input->SetOut_trade_no($this->out_trade_no);
//        if(YII_DEBUG === true){
//            $this->total_fee = 1;
//        }       
        $input->SetTotal_fee($this->total_fee);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag($this->goods_tag);
        $input->SetNotify_url($this->notify_url);
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);
        $order = WxPayApi::unifiedOrder($input);
        if(!empty($order['return_code']) && $order['return_code'] == 'FAIL'){
            echo $order['return_msg'];exit;
        }
        $jsApiParameters = $tools->GetJsApiParameters($order);     
        return $jsApiParameters;
    }
    
    //返回JS的内容部份  包括 jsApiCall  callpay 两部份
    public function getJs()
    {
        $jsApiParameters = $this->getJsApiParameters();
        $return_url = $this->ming_return_url;
        ob_start();        
        ob_implicit_flush(false);         
        include_once 'js.php';
        return ob_get_clean();        
    }
    
    //通知回调处理
    public function notify()
    {
        require_once __DIR__."/../../../wxpay/lib/WxPay.Notify.php";
        require_once __DIR__."/../PayNotifyCallBack.php";
        $notify = new PayNotifyCallBack();
        $notify->initData($this);
        $notify->Handle(false);        
    }
    
    //回调的处理操作
    //$data 包含  transaction_id  out_trade_no  attach 等这些参数
    public function NotifyProcess($data, &$msg,$obj)
    {
        \common\components\TmpLog::addData("sddsdsf", $data);
        try {
            if(empty($data['transaction_id']) || empty($data['out_trade_no'])){
                throw new Exception("参数不正确,transaction_id商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            }

            //查询订单，判断订单真实性
//            if (!$obj->Queryorder($data['transaction_id'])) {//这里查询可能是使用out_trade_no的参数，到时候需要试验一下
//                throw new Exception("订单查询失败！商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
//            }
            if ($data['result_code'] !== 'SUCCESS') {
                throw new Exception("支付失败！商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            }    
            
            //这里处理正常的逻辑操作
            return $this->orderProcessing($data['out_trade_no'], $data['transaction_id'],$data['total_fee']/100,'jsapi');//回到此对象处理操作

        } catch (Exception $exc) {
            \common\components\TmpLog::addData("JS支付", $exc->getMessage(), "");
            return false;
        }
    }
}