<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 进行相关的工作的class
 */
namespace component\pay\wxpay;
use component\pay\alipay\Wxpay;
class Work
{
//        $input->SetBody($config['subject']);
//        $input->SetAttach($config['order_type']);
//        $input->SetOut_trade_no($config['pay_sn']);
//        $input->SetTotal_fee($config['pay_amount']);
//        $input->SetProduct_id($config['pay_sn']);
//        $input->SetNotify_url($config['notify_url']);    
    public $config;
    public $obj_alipay = null;//默认的一个数据
    public $error;
    
    //设置获取相关的配置数据信息
    public function __construct($config = []) 
    {
        $this->config = array_merge($this->config, $config);
        $this->obj_alipay = new Wxpay();
    }
    
    /**
     * 获取支付接口的请求地址
     * @return type
     */
    public function get_payurl()
    {       
        $code = $this->obj_alipay->get_payurl($this->config);
        return isset($code['code_url']) ? $code['code_url'] : false;    
    }
    //输出HTML
    public function html($code_url)
    {
        extract($this->config);
        require 'html.php';
    }
    
    function ncPriceFormat($price)
    {
        $price_format = number_format($price, 2, '.', '');
        return $price_format;
    }    
    
}
