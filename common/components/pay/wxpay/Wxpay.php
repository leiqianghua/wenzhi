<?php

/**
 * File Name：wxpay.php
 * File Encoding：UTF-8
 * File New Time：2015-8-26 14:15:02
 * Author：宋星
 * Mailbox：376438848@qq.com
 */
require_once 'WxPay.Api.php';
require_once 'WxPay.Notify.php';
require_once 'wxlog.php';
namespace component\pay\wxpay;
class Wxpay extends \WxPayNotify {

    public $data = null;

    /**
     * 发送至微信的参数
     * @var array
     */
    private $parameter;
    //错误信息
    private $error = NULL;

    /**
     * 订单信息
     * @var array
     */
    private $order;

    /**
     * @param string $name
     * @return
     */
    public function __get($name)
    {
        return $this->$name;
    }

    //获取错误信息
    public function getError()
    {
        return $this->error;
    }

    public function __construct($order_info = [])
    {
        //自动创建 日志调试对象
        $logHandler = new CLogFileHandler();
        $log = wxLog::Init($logHandler, 15);
        if (!empty($order_info)) {
            $this->order = $order_info;
            $this->order['member_id'] = empty($this->order['member_id']) ? $this->order['buyer_id'] : $this->order['member_id'];
            
            $userInfo = Model('member')->getMemberInfo(['member_id' => $this->order['member_id']]);
            //$this->order['pay_amount'] = (defined('DEBUG') && DEBUG === true) ? 1 : in_array($userInfo['member_truename'], C('buyer_name_rand')) ? 1     : intval($order_info['pay_amount'] * 100);
            $this->order['pay_amount'] = (defined('DEBUG') && DEBUG === true) ? 1 : intval($order_info['pay_amount'] * 100);
            if (floor($this->order['pay_amount']) != $this->order['pay_amount']) {
                throw new WxPayException("金额参数不正确");
            }
        }
    }

    //获取参数
    public function GetParameter($openId, $setTrade_type = 'JSAPI')
    {
        $input = new WxPayUnifiedOrder();
        $input->SetBody($this->order['subject']);
        $input->SetAttach($this->order['order_type']);
        $input->SetOut_trade_no($this->order['pay_sn']);
        $input->SetTotal_fee($this->order['pay_amount']);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetNotify_url(SHOP_SITE_URL . "/api/pay/wxpay/notify_url.php");
        $input->SetTrade_type($setTrade_type);
        $input->SetOpenid($openId);
        $order = WxPayApi::unifiedOrder($input);

        $jsApiParameters = $this->GetJsApiParameters($order);
        return $jsApiParameters;
    }

    //重写回调处理函数
    public function NotifyProcess($data, &$msg)
    {
        if (!array_key_exists("transaction_id", $data)) {
            $msg = "输入参数不正确!";
            Model('errorlogs')->logs("wxpay_NotifyProcess", "{$msg}transaction_id商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            return false;
        }
        if (empty($data['out_trade_no'])) {
            $msg = "输入参数不正确!";
            Model('errorlogs')->logs("wxpay_NotifyProcess", "{$msg}商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            return false;
        }
        //查询订单，判断订单真实性
        if (!$this->order_query($data["out_trade_no"])) {
            $msg = "订单查询失败!";
            Model('errorlogs')->logs("wxpay_NotifyProcess", "{$msg}商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            return false;
        }
        if ($data['result_code'] !== 'SUCCESS') {
            $msg = "支付失败!";
            Model('errorlogs')->logs("wxpay_NotifyProcess", "{$msg}商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            return false;
        }
        
        //$order_type = 'product_buy';
        $order_type = $data['attach'];
        $inc_file = BASE_ROOT_PATH . "/api/pay/base/" . $order_type . ".php";
        if (!file_exists($inc_file)) {
            $msg = '接口错误，无法完成支付！';
            Model('errorlogs')->logs("wxpay_NotifyProcess", "{$msg}商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            return false;
        }
        require_once($inc_file);
        //更改支付订单状态
        $class_name = $order_type . "_base";
        $base_class = new $class_name($data['out_trade_no'], $data['transaction_id'], ($data['total_fee'] / 100), 'wxpay');
        $result = $base_class->update_pay_state();

        if (!$result) {
            $msg = '更新订单状态失败！';
            Model('errorlogs')->logs("wxpay_NotifyProcess", "{$msg}商城订单号:{$data['out_trade_no']},微信单号:{$data['transaction_id']},支付金额:{$data['total_fee']}");
            return false;
        }
        //回调
        try {
            $file = BASE_ROOT_PATH . "/api/pay/callback.php";
            require_once($file);
            //进行回调处理
            $callback_result = callback::getInstance($order_type)->data($result)->run();
            //Model('errorlogs')->logs("wxpay_callback_result", "回调结果{$callback_result},商城订单号:{$data['out_trade_no']},");
            return $callback_result;
        } catch (Exception $exc) {
            //错误收集
            Model('errorlogs')->logs("wxpay_Exception", "回调结果{$exc->getMessage()},商城订单号:{$data['out_trade_no']},");
        }
    }

    /**
     * 微信 订单查询
     */
    public function order_query($out_trade_no)
    {
        $input = new WxPayOrderQuery();
        $input->SetOut_trade_no($out_trade_no);
        $data = WxPayApi::orderQuery($input);
        if (array_key_exists("return_code", $data) && array_key_exists("result_code", $data) && $data["return_code"] == "SUCCESS" && $data["result_code"] == "SUCCESS") {
            return $data;
        }
        return false;
    }

    //回调参数
    public function get_notify()
    {
        $xml = file_get_contents("php://input");
        $result = '';
        if ($xml) {
            $result = WxPayResults::Init($xml);
        }
        return $result;
    }

    public function reply($code, $message = '')
    {
        $input = new WxPayNotifyReply();
        $input->SetReturn_code($code);
        if ($message != '') {
            $input->SetReturn_msg($message);
        }
        return $input->ToXml();
    }

    /**
     * 
     * 通过跳转获取用户的openid，跳转流程如下：
     * 1、设置自己需要调回的url及其其他参数，跳转到微信服务器https://open.weixin.qq.com/connect/oauth2/authorize
     * 2、微信服务处理完成之后会跳转回用户redirect_uri地址，此时会带上一些参数，如：code
     * 
     * @return 用户的openid
     */
    public function GetOpenid()
    {
        //通过code获得openid
        if (!isset($_GET['code'])) {
            //触发微信返回code码
            $baseUrl = urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . $_SERVER['QUERY_STRING']);
            $url = $this->__CreateOauthUrlForCode($baseUrl);
            Header("Location: $url");
            exit();
        } else {
            //获取code码，以获取openid
            $code = $_GET['code'];
            $openid = $this->getOpenidFromMp($code);
            return $openid;
        }
    }

    /**
     * 
     * 获取jsapi支付的参数
     * @param array $UnifiedOrderResult 统一支付接口返回的数据
     * @throws WxPayException
     * 
     * @return json数据，可直接填入js函数作为参数
     */
    public function GetJsApiParameters($UnifiedOrderResult)
    {
        if (!array_key_exists("appid", $UnifiedOrderResult) || !array_key_exists("prepay_id", $UnifiedOrderResult) || $UnifiedOrderResult['prepay_id'] == "") {
            throw new WxPayException("参数错误");
        }
        $jsapi = new WxPayJsApiPay();
        $jsapi->SetAppid($UnifiedOrderResult["appid"]);
        $timeStamp = time();
        $jsapi->SetTimeStamp((string) $timeStamp);
        $jsapi->SetNonceStr(WxPayApi::getNonceStr());
        $jsapi->SetPackage("prepay_id=" . $UnifiedOrderResult['prepay_id']);
        $jsapi->SetSignType("MD5");
        $jsapi->SetPaySign($jsapi->MakeSign());
        $parameters = json_encode($jsapi->GetValues());
        return $parameters;
    }

    /**
     * 
     * 通过code从工作平台获取openid机器access_token
     * @param string $code 微信跳转回来带上的code
     * 
     * @return openid
     */
    public function GetOpenidFromMp($code)
    {
        $url = $this->__CreateOauthUrlForOpenid($code);
        //初始化curl
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOP_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if (WxPayConfig::CURL_PROXY_HOST != "0.0.0.0" && WxPayConfig::CURL_PROXY_PORT != 0) {
            curl_setopt($ch, CURLOPT_PROXY, WxPayConfig::CURL_PROXY_HOST);
            curl_setopt($ch, CURLOPT_PROXYPORT, WxPayConfig::CURL_PROXY_PORT);
        }
        //运行curl，结果以jason形式返回
        $res = curl_exec($ch);
        curl_close($ch);
        //取出openid
        $data = json_decode($res, true);
        $this->data = $data;
        $openid = $data['openid'];
        return $openid;
    }

    /**
     * 
     * 拼接签名字符串
     * @param array $urlObj
     * 
     * @return 返回已经拼接好的字符串
     */
    public function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v) {
            if ($k != "sign") {
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * 
     * 获取地址js参数
     * 
     * @return 获取共享收货地址js函数需要的参数，json格式可以直接做参数使用
     */
    public function GetEditAddressParameters()
    {
        $getData = $this->data;
        $data = array();
        $data["appid"] = WxPayConfig::APPID;
        $data["url"] = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $time = time();
        $data["timestamp"] = "$time";
        $data["noncestr"] = "1234568";
        $data["accesstoken"] = $getData["access_token"];
        ksort($data);
        $params = $this->ToUrlParams($data);
        $addrSign = sha1($params);

        $afterData = array(
            "addrSign" => $addrSign,
            "signType" => "sha1",
            "scope" => "jsapi_address",
            "appId" => WxPayConfig::APPID,
            "timeStamp" => $data["timestamp"],
            "nonceStr" => $data["noncestr"]
        );
        $parameters = json_encode($afterData);
        return $parameters;
    }

    /**
     * 
     * 构造获取code的url连接
     * @param string $redirectUrl 微信服务器回跳的url，需要url编码
     * 
     * @return 返回构造好的url
     */
    private function __CreateOauthUrlForCode($redirectUrl)
    {
        $urlObj["appid"] = WxPayConfig::APPID;
        $urlObj["redirect_uri"] = "$redirectUrl";
        $urlObj["response_type"] = "code";
        $urlObj["scope"] = "snsapi_base";
        $urlObj["state"] = "STATE" . "#wechat_redirect";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://open.weixin.qq.com/connect/oauth2/authorize?" . $bizString;
    }

    /**
     * 
     * 构造获取open和access_toke的url地址
     * @param string $code，微信跳转带回的code
     * 
     * @return 请求的url
     */
    private function __CreateOauthUrlForOpenid($code)
    {
        $urlObj["appid"] = WxPayConfig::APPID;
        $urlObj["secret"] = WxPayConfig::APPSECRET;
        $urlObj["code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/sns/oauth2/access_token?" . $bizString;
    }

    /**
     * 获取支付接口的请求地址
     * @return type
     */
    public function get_order($config)
    {
        $input = new WxPayUnifiedOrder();
        $input->SetBody($config['subject']);
        $input->SetAttach($config['order_type']);
        $input->SetOut_trade_no($config['pay_sn']);
        $input->SetTotal_fee($config['pay_amount']);
        $input->SetProduct_id($config['pay_sn']);
        $input->SetNotify_url($config['notify_url']);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 1800));
        $input->SetTrade_type("NATIVE");
        $order = WxPayApi::unifiedOrder($input);
        return $order;

    }

}
