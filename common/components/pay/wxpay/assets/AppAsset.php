<?php

namespace component\pay\wxpay\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    //public $basePath = '@webroot';
    //public $baseUrl = '@web';    
    public $sourcePath = '@component/pay/wxpay/page';
    public $css = [
        'css/style.css',
    ];
    public $js = [
         'js/jquery.js',
    ];
    public $jsOptions = [
        //'position' => \yii\web\View::POS_
    ];
    public $depends = [        
       // 'yii\bootstrap\BootstrapPluginAsset',
    ];
}
