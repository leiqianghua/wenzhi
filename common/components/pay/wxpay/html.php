<!doctype html>
<html lang="zh">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta name="x5-fullscreen" content="true">
<meta name="x5-page-mode" content="app">
<title>微信支付</title>
<?php 
        \component\pay\wxpay\assets\AppAsset::register($this);
?>
</head>
<body>
<div class="weixin_pay_hd">
    <p><label>支付单号：</label><?php echo $pay_sn ?></p>
    <p><label>应付金额：</label><span><?php echo $this->ncPriceFormat($pay_amount); ?></span>元</p>
</div>

<div class="mod_weixin_pay">
    <div class="code_wrap">
        <div class="code_img" id="codeImg" data-url="<?= $code_url; ?>"></div>
        <p>请使用微信扫描二维码</p>
    </div>
    <img src="<?php echo SHOP_TEMPLATES_URL; ?>/resource/images/weixin_pay.png" alt="" class="prompt">
    
    <?php 
    
        switch ($order_type) {
            case 'predeposit':
                $url = Unit::U('buy/pd_pay', ['pay_sn' => $paySn]);

                break;
            case 'product_buy':

                $url = Unit::U('buy/pay', ['pay_sn' => $paySn]);
                break;

            default:
                $url = Unit::U('buy/pay', ['pay_sn' => $paySn]);
                break;
        }

    ?>
    
    
    
    <a href="<?php echo $url; ?>" class="a_back"><<选择其他支付方式</a>
</div>

<?php require_once template('layout/base/footer'); ?>

<!--[if IE]>
<script type="text/javascript">
var isIE = 0;
</script>
<![endif]-->
<script type="text/javascript">
    $(function() {
        var url = $('#codeImg').attr('data-url');
        jQuery.getScript("<?php echo RESOURCE_SITE_URL; ?>/js/jquery.qrcode.js",
        function(data, status, jqxhr) {
            if ($('#codeImg').html())
                return;
            if(typeof(isIE) == 'undefined'){
                $('#codeImg').qrcode({
                    width: 160,
                    height: 160,
                    correctLevel: 0,
                    text: url
                });
            }else{
                $('#codeImg').qrcode({
                    render  : "table", //用table，默认Canvas
                    width: 160,
                    height: 160,
                    correctLevel: 0,
                    text: url
                });
            }
        });
        $(document).ready(function() {
            setInterval(queryOrderState, 3000);
        });
        function queryOrderState() {
            $.ajax({
                type: "GET",
                url: "<?= Unit::U('wxpay/getstate', ['pay_sn' => $paySn,'order_type' => $order_type]) ?>",
                data: "",
                dataType: "json",
                timeout: 4000,
                async: false,
                success: function(result) {
                    if (result.status == 1) {
                        switch('<?= $order_type ?>'){
                            case 'predeposit':
                                var url = "<?php echo Unit::U('predeposit/index'); ?>";
                                break;
                            case 'product_buy':
                                var url = "<?php echo Unit::U('buy/pay_ok', ['pay_sn' => $paySn, 'pay_amount' => $payAmount]); ?>";
                                break;
                                
                        }
                        
                        
                        //直接跳到成功页
                        window.location.href = url;
                    }
                }
            });
        }
    })
</script>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/resource/js/common.js"></script>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/resource/js/jquery.SuperSlide.2.1.1.js"></script>
</body>
</html>




