<?php
/**
 * 
 * @filename scan_notify_url.php
 * @encoding UTF-8
 * @author jinhui Zhang <www.008zjh@gmail.com>
 * @date 2015-11-19 03:23:29
 * @copyright Copyright ©   2015 itroad
 */
$_GET['act'] = 'wxpay';
$_GET['op'] = 'notify';

define('SITE_PATH', getcwd() . '/../../../');
require SITE_PATH. 'index.php';
