<?php

//微信二维码
namespace common\components\weixin;
class Erweima {
    public static $error;
    
    //获取永久二维码
    public static function getPermanent($uid = '')
    {
        $uid == '' ? $uid = \yii::$app->controller->user->uid : '';
        
        $access_token = WeixinTool::getAccess_token();
        $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={$access_token}";
        
        $obj = new \common\components\Curl();
        $data = [
            "expire_seconds" => 2591999,//有效期
            "action_name" => "QR_LIMIT_SCENE",//永久的二维码
            "action_info" => [
                "scene" => [
                    "scene_id" => $uid,//会返回过来的东西
                    "scene_str" => $uid,                    
                ],
            ],
        ];
        \common\components\TmpLog::addData("二维码data", $data);
        $post = urldecode(json_encode($data));
        $result = $obj->post($url, $post);
        \common\components\TmpLog::addData("二维码", $result);
        $result = json_decode($result, true);
        if(empty($result) || empty($result['ticket'])){
            static::$error = "找不到相关的二维码";
            return false;
        }else{
            //保存起来，这个用户的二维码的ticket
            $member = \common\models\MemberModel::findOne($uid);
            $member->ticket = $result['ticket'];
            $member->save(false);
            //\common\models\MemberModel::updateAll(['ticket' => $result['ticket']], ['uid' => $uid]);
        }
        
        //获取图片地址
        $img = static::getPermanentForSceneID($result);
        //$img = static::longChangeShort($img);
        return $img;
    }
    
    public static function  getPermanentForSceneID($result)
    {
        $ticket = urlencode($result['ticket']);
        $url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={$ticket}";
        return $url;
    }
    
    //长的转为短的
    public static function longChangeShort($long_url)
    {
        $access_token = WeixinTool::getAccess_token();
        $url = "https://api.weixin.qq.com/cgi-bin/shorturl?access_token={$access_token}";
        
        $obj = new \common\components\Curl();
        $data = [
            "access_token" => $access_token,
            "action" => "long2short",
            "long_url" => $long_url,
        ];
        $post = urldecode(json_encode($data));
        $result = $obj->post($url, $post);  
        $result = json_decode($result, true);
        
        \common\components\TmpLog::addData("二维码长转短", $result);
        if(empty($result) || empty($result['short_url'])){
            return $long_url;
        }else{
            return $result['short_url'];
        }
    }

}
