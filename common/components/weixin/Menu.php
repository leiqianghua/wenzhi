<?php

//微信 菜单
namespace common\components\weixin;
use common\components\WeixinPublicNumberBase;
use yii;

class Menu
{
    public static function create($menu)
    {
        $access_token = WeixinTool::getAccess_token();
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token={$access_token}";
        
        $obj = new \common\components\Curl();
        $post = json_encode($menu,JSON_UNESCAPED_UNICODE);
        
        $result = $obj->post($url, $post);
        \common\components\TmpLog::addData("增加菜单", $result);
        $result = json_decode($result, true);
        if(isset($result['errcode']) && $result['errcode'] == 0){
            return true;
        }else{
            return $result;
        }
    }
}
