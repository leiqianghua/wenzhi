<?php

//微信 基础
namespace common\components\weixin;
use common\components\WeixinPublicNumberBase;
use yii;

class WeixinTool {
    
    //获取用户的  Access_token
    public static function getAccess_token()
    {
        $access_token = \yii::$app->cache->get("access_token");
        if(!$access_token){
            $obj = new WeixinPublicNumberBase();
            $arr = $obj->getAccess_token();
            if(is_array($arr)){
                $access_token = $arr['access_token'];
                $expires_in = $arr['expires_in'];
                $expires_in = $expires_in - 60;
                \yii::$app->cache->set("access_token",$access_token,$expires_in);
            }
        }
        return $access_token;
    }
    
    //获取用户的  openid
    public static function getOauthData()
    {
        $oauth = \common\models\MemberOauthDataModel::find()->where(['uid' => $this->uid,'oauth_type' => 'weixin'])->asArray()->one();
        if($oauth){
            $oauth = \common\models\MemberOauthWeixinModel::find()->where(['unionid' => $oauth['oauth_key']])->asArray()->one();
            if($oauth){
                return $oauth['openid'];
            }
        }
    }      

    
    public static function getUserMessage($token,$open_id)
    {
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={$token}&openid={$open_id}&lang=zh_CN";
        $obj = new \common\components\Curl();
        $message = $obj->get($url);
        $message = json_decode($message,true);
        if(!empty($message['openid'])){
            return $message;
        }else{
            return false;
        }
    }  


}
