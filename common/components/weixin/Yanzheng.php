<?php
namespace common\components\weixin;
use yii;
use Exception;
class Yanzheng
{
    public function valid()
    {
        //\common\components\TmpLog::addData("ddddd", 'cccc');
        $echoStr = $_GET["echostr"];

        //valid signature , option
        if($this->checkSignature()){
            echo $echoStr;
            exit;
        }
    }

		
    private function checkSignature()
    {
        $token = \yii::$app->params['sanfan']['weixin']['token'];

        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );

        if( $tmpStr == $signature ){
            return true;
        }else{
            return false;
        }
    }
    
    
    
    //接收相关的消息   微信通知相关的消息 
    public function responseMsg()
    {
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
        if (!empty($postStr)){
            $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            $ab = json_decode(json_encode($postObj),TRUE);
            \common\components\TmpLog::addData("微信通知消息",$ab);
            $RX_TYPE = trim($postObj->MsgType);
            switch ($RX_TYPE)
            {
                case "text":
                    $resultStr = $this->receiveText($postObj);
                    break;
                case "event":
                    $resultStr = $this->receiveEvent($postObj);
                    break;
                default:
                    $resultStr = "";
                    break;
            }

        \common\components\TmpLog::addData("xmldata", json_decode(json_encode($resultStr),TRUE));            
//            $resultStr  = " <xml>
// <ToUserName><![CDATA[toUser]]></ToUserName>
// <FromUserName><![CDATA[fromUser]]></FromUserName>
// <CreateTime>12345678</CreateTime>
// <MsgType><![CDATA[text]]></MsgType>
// <Content><![CDATA[content]]></Content>
// </xml>";
            echo $resultStr;exit;
        }else {
            echo "";
            exit;
        }
    }

    private function receiveText($object)
    {
        $funcFlag = 0;
        $contentStr = "你发送的内容为：".$object->Content;
        $resultStr = $this->transmitText($object, $contentStr, $funcFlag);
        return $resultStr;
    }

    private function receiveEvent($object)
    {
        $contentStr = "";
        switch ($object->Event)
        {
            case "subscribe":
                $this->findParent($object);
                $contentStr = "欢迎关注古笆免";
                break;
            case "SCAN":
                return $this->erweima($object);
            case "unsubscribe":
                break;
            case "kf_create_session":
                $contentStr = "已经成功接入客服,请描述您的问题";
                $this->setMessage($object->FromUserName, $contentStr);
                exit;
                //$resultStr = $this->transmitText($object, $contentStr);
                break;
            case "kf_close_session":
                $contentStr = "会话已断开";
                $this->setMessage($object->FromUserName, $contentStr);
                exit;
                //$resultStr = $this->transmitText($object, $contentStr);
                break;
            case "CLICK":
                if($object->EventKey == "key_kefu"){
                    $this->sendKefu($object);
                    exit;
                }
                $content = \common\models\ConfigModel::getWeixinContentForKey($object->EventKey);
                \common\components\TmpLog::addData("gsdfsf", $content);
                $contentStr[] = $content;
                
//                switch ($object->EventKey)
//                    {
//                        case "key1":
//                            $contentStr[] = array("Title" =>"公司简介", 
//                            "Description" =>"    厦门古笆兔建筑装饰有限公司于2016年5月在素有花园城市之美称的厦门市正式成立，公司自成立伊始，以长期发展为目标，以开创并引领室内景观行业为己任，立志在室内景观领域为行业及消费者倾心尽力。
//
//         公司通过与多家大型资材公司联手打造供应链系统，成功开发石、竹、木、根、藤、金、陶、饰、植、辅等多个系列的室内景观素材产品。同时，团结了一批优秀的行业人才，针对“地面、桌面、墙面、悬空”四个维度进行景观开发。从而，打造“景观阳台、景观通道、景观办公室、景观前台、禅茶空间”等 “室内整体景观”空间的全方位综合服务。
//
//          为了更好的服务客户，公司投入重金打造独立官方平台——古笆兔商城，为客户提供产品展示、案例分享、设计参考、一键采购等线上高效服务。以此同时，公司将对互联网平台持续投入，开发室内景观DIY设计软件，以更好地顺应市场发展的需求。
//      目前，公司以“P（F2C）+S（B2C）”的全新运营模式，快速打开室内景观市场，同时，抓紧实施“品牌”战略，开展线下连锁加盟体验空间的现代化品牌运营模式，统一形象，强化管理，加速发展，专注室内整体景观，致力于打造中国室内景观行业第一品牌——古笆兔！", 
//                            "PicUrl" =>"http://image.gubatoo.com/20180121/goods/55860694496688.jpg",  
//                            "Url" =>"weixin://addfriend/pondbaystudio");
//                            break;
//                        case "key2":
//                            $contentStr[] = array("Title" =>"发展历程", 
//                            "Description" =>"2013年，魅丽石族泉州红星美凯龙专卖店正式开业
//    2015年，成功注册商标：魅丽石族
//    2016年5月，魅丽石族进行公司化运营，更名为厦门古笆兔建筑装饰有限公司
//    2016年6月，古笆兔正式进驻小样青年社区
//    2016年8月，古笆兔正式发布第一套室内模块景观，开始试点
//    2016年12月，古笆兔福州运营中心正式成立
//    2017年1月，完成市场试点，正式确定“室内整体景观”的发展方向
//    2017年3月，古笆兔自主开发的古笆兔商城成功上线
//    2017年5月，古笆兔首家专卖店于龙岩正式开业
//    2017年8月，古笆兔总部成立，设于中国·厦门的“花园梦工场”
//    2017年11月，厦门古笆兔科技有限公司正式成立
//
//    2018年，古笆兔，梦想起航......", 
//                            "PicUrl" =>"http://image.gubatoo.com/20180121/goods/55860694496688.jpg", 
//                            "Url" =>"weixin://addfriend/pondbaystudio");
//                            break;
//                        case "key3":
//                            $contentStr[] = array("Title" =>"企业优势", 
//                            "Description" =>"Ø  室内景观开创者
//    Ø  首创【九大场景·五大风格】模块景观
//    Ø  首次提出【十大素材·四大景观·五大空间】的室内整体景观
//    Ø  中国第一家专注室内装饰景观企业
//    Ø  全球采购·拥有强劲的供应商整合能力
//    Ø  拥有独立的线上平台——古笆兔商城
//    Ø  引进国际造景理念，独创市场领先商业模式
//    Ø  ……", 
//                            "PicUrl" =>"http://image.gubatoo.com/20180121/goods/55860694496688.jpg",    
//                            "Url" =>"weixin://addfriend/pondbaystudio");
//                            break;
//                        case "key4":
//                            $contentStr[] = array("Title" =>"客服咨询", 
//                            "Description" =>"地址：中国·厦门  集美区灌口景山路869号
//    网址：www.gubatoo.com
//    客服热线：0592-5222968", 
//                            "PicUrl" =>"http://image.gubatoo.com/20171129/goods/12807878946068.jpg", 
//                            "Url" =>"weixin://addfriend/pondbaystudio");
//                            break;
//                        default:
//                            $contentStr[] = array("Title" =>"客服咨询", 
//                            "Description" =>"地址：中国·厦门  集美区灌口景山路869号
//    网址：www.gubatoo.com
//    客服热线：0592-5222968", 
//                            "PicUrl" =>"http://image.gubatoo.com/20180121/goods/55860694496688.jpg",  
//                            "Url" =>"weixin://addfriend/pondbaystudio");
//                            break;
//                    }
                break;
            default:
                break;      

        }
        if (is_array($contentStr)){
            $resultStr = $this->transmitNews($object, $contentStr);
        }else{
            $resultStr = $this->transmitText($object, $contentStr);
        }
        return $resultStr;
    }

    private function transmitText($object, $content, $funcFlag = 0)
    {
        $textTpl = "<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[%s]]></Content>
</xml>";
        $resultStr = sprintf($textTpl,$object->FromUserName,  $object->ToUserName, time(),$content);
        return $resultStr;
    }

    private function transmitNews($object, $arr_item, $funcFlag = 0)
    {
        //首条标题28字，其他标题39字
        if(!is_array($arr_item))
            return;

        $itemTpl = "    <item>
        <Title><![CDATA[%s]]></Title>
        <Description><![CDATA[%s]]></Description>
        <PicUrl><![CDATA[%s]]></PicUrl>
        <Url><![CDATA[%s]]></Url>
    </item>
";
        $item_str = "";
        foreach ($arr_item as $item)
            $item_str .= sprintf($itemTpl, $item['Title'], $item['Description'], $item['PicUrl'], $item['Url']);

        $newsTpl = "<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[news]]></MsgType>
<Content><![CDATA[]]></Content>
<ArticleCount>%s</ArticleCount>
<Articles>
$item_str</Articles>
<FuncFlag>%s</FuncFlag>
</xml>";

        $resultStr = sprintf($newsTpl, $object->FromUserName, $object->ToUserName, time(), count($arr_item), $funcFlag);
        \common\components\TmpLog::addData("字符串", $resultStr);
        return $resultStr;
    }
    
    
    public function getUserMessage($object)
    {
        $token = WeixinTool::getAccess_token();
        $open_id = $object->FromUserName;
        $message = WeixinTool::getUserMessage($token, $open_id);
        if(!isset($message['nickname'])){
            return false;
        }
        return $message;
    }
    
    public function findParent($object)
    {
        $ab = json_decode(json_encode($object),TRUE);
        if(!empty($ab['Ticket']) && !empty($ab['FromUserName'])){
            //扫的是谁的二维码
            $member = \common\models\MemberModel::findOne(['ticket' => $ab['Ticket']]);
            if($member){//扫的是这个人的二维码
                try {

                    $model = \common\models\MemberWeixinExtenCodeModel::findOne($ab['FromUserName']);
                    if(!$model){
                        $model = new \common\models\MemberWeixinExtenCodeModel();
                    }
                    $model->open_id = $ab['FromUserName'];
                    $model->ticket = $ab['Ticket'];
                    $model->save(false);
                    
                    
                    
                    
                    
                    $member_s = $this->getUserMessage($object);
                    $date = date('Y-m-d H:i:s', time());
                    if($member_s){
                        //发送消息出去
                        $data = [
                            'first' => [
                                'value' => "微信用户-{$member_s['nickname']}，通过扫码进入",
                            ],
                            'keyword1' => [
                                'value' => "成功",
                            ],
                            'keyword2' => [
                                'value' => $date,
                            ],
                            'keyword3' => [
                                'value' => $date,
                            ],
                            'remark' => [
                                'value' => "祝您生活愉快",
                            ],
                        ];
                        \common\models\MemberModel::notice_user($data, "", "SJqMlGXfvPGoENGRuhZWvM2bALfNSByd_PmeuMR_q5A", false, false, false, [$member['uid']]);                        
                    }

                    
                } catch (Exception $exc) {
                    \common\components\TmpLog::addData("处理数据4", $exc->getMessage());
                }
            }else{
                \common\components\TmpLog::addData("处理数据3", "找不到用户");
            }                 
        }   
    }
    
    public function erweima($object)
    {
        $this->findParent($object);
        //回复内容
        $this->setMessage($object->FromUserName);
        
    }
    
    public function setMessage($openid,$message = "欢迎光临古笆免")
    {
        $ACCESS_TOKEN = WeixinTool::getAccess_token();
        $url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$ACCESS_TOKEN}";
        $obj = new \common\components\Curl();
        $data = [
            'touser' => "ou7OgwjWEKI4WfAAV1s424RdEMDU",
            "msgtype" => "text",
            "text" => [
                "content" => "欢迎光临古笆免",
            ],
        ];
        
        $post = '{
                "touser":"'.$openid.'",
                "msgtype":"text",
                "text":
                {
                     "content":"'.$message.'"
                }
            }'
                ;
        \common\components\TmpLog::addData("send_m", $post);
        \common\components\TmpLog::addData("send_m_mm", $openid);
        $result = $obj->post($url, $post);  
        $result = json_decode($result, true);      
        return $result;
    }
    
    
    
    
    //将消息转给客服
    private function sendKefu($object)
    {
        
        
        //告诉用户正在接入客服
        $message = "正在为您接入客服，请稍等

若长时间没有接入加微信在线客服咨询（ance_ly）";
        $this->setMessage($object->FromUserName, $message);
        
        
        sleep(1);
        
        
        $kefu = $this->getOkKefu();
        if(!$kefu){
            $message = "客服占时不在线,工作时间为工作日9：00-18：00";
            $str = $this->transmitText($object,$message);
            exit($str);
        }
        
        //接一个客服
        $kk = array_rand($kefu, 1);//接的客服是这个。
        $arr = $kefu[$kk];
//        $this->jieKefu($object, $arr);
//        
//        
//        exit("接客服");
        
        
        
        $str = "<xml>
     <ToUserName><![CDATA[%s]]></ToUserName>
     <FromUserName><![CDATA[%s]]></FromUserName>
     <CreateTime>%s</CreateTime>
     <MsgType><![CDATA[transfer_customer_service]]></MsgType>
     <TransInfo>
         <KfAccount><![CDATA[%s]]></KfAccount>
     </TransInfo>     
 </xml>";
        
        $resultStr = sprintf($str,$object->FromUserName,  $object->ToUserName, time(),$arr['kf_account']);
        
        exit($resultStr);
        
    }
    
    /**
     * 返回数组   里面的键值是这样的。
kf_account	完整客服帐号，格式为：帐号前缀@公众号微信号
status	客服在线状态，目前为：1、web 在线
kf_id	客服编号
accepted_case	客服当前正在接待的会话数
     * @return type
     */
    private function getOkKefu()
    {
        $token = WeixinTool::getAccess_token();
        $url = "https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist?access_token={$token}";
        
        $obj = new \common\components\Curl();
        $result = $obj->get($url);
        
        $res = json_decode($result,true);
        if(!empty($res['kf_online_list'])){
            $return = [];
            foreach($res['kf_online_list'] as $key => $value){
                if($value['status']){
                    $return[] = $value;
                }
            }
            return $return;
        }else{
            return [];
        }
        
        
    }
    
    
    private function jieKefu($object,$kefu)
    {
        \common\components\TmpLog::addData("kefu", $kefu);
        $token = WeixinTool::getAccess_token();
        $url = "https://api.weixin.qq.com/customservice/kfsession/create?access_token={$token}";
        $abss = json_decode(json_encode($object),TRUE);
        $param = [];
        $param['kf_account'] = $kefu['kf_account'];
        $ss = $abss['FromUserName'];
        if(is_string($ss)){
            $param['openid'] = $ss;
        }
        
        \common\components\TmpLog::addData("ssssss", $param);
        $obj = new \common\components\Curl();
        $result = $obj->post($url,$param);
        
        \common\components\TmpLog::addData("sgsdfsf", $result);
    }    
}

?>