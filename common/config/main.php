<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'sns' => [
            'class' => 'common\components\Sns',
        ],
    ],
    'language' => 'zh-CN',
    //公共的控制器
    'controllerMap' => [
        'sns' => 'common\controllers\CommonController',
        'ueditor' => 'common\controllers\UeditorController',
        'callback_pay' => 'common\components\pay\run\notify\IndexController',
    ],
    'modules' => [
       'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],    
];
