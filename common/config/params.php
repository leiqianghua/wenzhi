<?php
return [
    'sanfan' => [
        'qq' => [//QQ应用
            'appid' => "101341960",
            'appsecret' => "b7d51bf3f60996cde3da6586821f3fc3",
            'callback' => "http://www.gubatoo.com/wap/oauth/callback_qq",
            'scope' => "get_user_info,add_t,add_pic_t",            
        ],
        'weixin' => [//微信应用
            'appid' => "wxee23ef6f09bcb5ac",
            'appsecret' => "79879beb3d1bdf0e05a843c42f53eed0",
            'token' => 'fX8D5AO0k0SL5Bfztr2Z2lbpjWZNisDP', //微信服务号交互token[自定义]
            //'sender' => 'gh_c07127775924', //微信原始ID,发送消息时的发送者
            'callback' => "http://www.gubatoo.com/wap/oauth/callback_weixin",            
        ],
//        'weixin_public_number' => [//微信公众号应用
//            'appid' => "wxd876722743b65fd8",
//            'appsecret' => "e88a42a565d4ea24fce886e55645e5d3",
//            'callback' => "http://www.91duobao365.com/wap/oauth/callbackWeixinPublicNumber",    
//            'scope' => "snsapi_userinfo",           
//        ],
        'weixin_public_number' => [//微信公众号应用
            'appid' => "wx8432306d7fa1b743",
            'appsecret' => "eccdde5b917532d74be1b99c73b47717",
            'callback' => "http://www.gubatoo.com/wap/oauth/number",    
            'scope' => "snsapi_userinfo",           
        ],
    ],
    
    'sql' => [
        'go_shoplist_code' => "
            CREATE TABLE `TABLENAME` (
                    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `shop_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '商品ID  每期的商品',
                    `number_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '期数商品ID',
                    `total` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '云购码总数',
                    `rest` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '剩余的数量',
                    `num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '第几组的云购码',
                    `add_time` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '添加时间',
                    `code` TEXT NOT NULL COMMENT '未使用的云购码',
                    `code_tmp` TEXT NOT NULL COMMENT '全部的云购码',
                    PRIMARY KEY (`id`),
                    INDEX `number_id` (`number_id`)
            )
            COMMENT='商品云购码表'
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB;
        ",
    ],
    'code_limit' => [
        'length' => 100000,//一张表不超过10万条
        'size' => 2048,//一张表的大小不超过2G
        'notes_code_limit' => 3000,//一条记录最多保存3000条的云购码
    ],
    
    'pay' => [
        'alipay' => [
            'partner' => '2088421568543628',
            'seller_id' => '2088421568543628',
            'key' => 'tylgkhs2sik68ca22oh4fqpvcgacyad8',
            'notify_url' => 'http://www.gubatoo.com/callback_pay/alipay_notify_url',
            'return_url' => 'http://www.gubatoo.com/callback_pay/alipay_return_url',
        ],
        'alipaywap' => [
            'partner' => '2088421568543628',
            'seller_id' => '2088421568543628',
            'key' => 'tylgkhs2sik68ca22oh4fqpvcgacyad8',
            'notify_url' => 'http://www.gubatoo.com/callback_pay/alipay_wap_notify_url',
            'return_url' => 'http://www.gubatoo.com/callback_pay/alipay_wap_return_url',
        ],
        'jsapi_weixin' => [
            'notify_url' => 'http://www.gubatoo.com/weixin/index_weixin_wap_notify.php',
        ],
    ],
    
    'file_url' => 'https://gubatu.oss-cn-qingdao.aliyuncs.com',
];
