<?php

namespace common\controllers;

use common\components\BaseController;
use common\components\SnsVerification;
use yii;
use common\components\Curl;
/**
 * 登录注册等相关的功能
 */
class CommonController extends BaseController
{
    //发短信验证码
    public function actionSend_message($phone)
    {
        $verif = new SnsVerification();
        $state = $verif->send($phone);
        if($state){
            $this->success('发送成功');
        }else{
            $this->error($verif->error);
        }
    }
    //验证短信验证码
    public function actionVerification_message($code,$mobile)
    {
        $verif = new SnsVerification();
        $state = $verif->verification($code,$mobile);
        if($state){
            $this->success('验证成功');
        }else{
            $this->error($verif->error);
        }        
    }
    //上传图片  //单张
    // 需要包含 path 路径这键值
    //返回的是json数据  content status
    //这里返回的一定是需要一个图片的路径字符串，不能是其它的东西，不然不兼容
    public function actionUploadfile()
    {
        \common\components\TmpLog::addData("uploadfile", $_POST);
        $this->user->isLogin();
        $dir = \common\components\GuestInfo::getParam('path', 'common/upload');
        $obj = new \common\components\UploadFile();
        $file = $obj->upload($dir);
        echo $obj->error;
        echo $file['url'];exit;
    }
    
    //删除图片
    //需要通过Post传送  filename  键值的数据 这个值包含了前面的域名
    //返回值为 json数据
    public function actionDeletefile()
    {
        $this->user->isLogin();
        $uid = $this->user->uid;
        if(empty($_POST['filename'])){
            $this->error('删除图片的路径不存在');
        }
        if(!$uid){
            $this->error('你不能删除此图');
        }
        //去除前面的域名
        
        
        $data = \common\models\AttachmentModel::find()->where(['url' => $_POST['filename']])->limit(1)->one();
        
        if(!\yii::$app->controller->user->isAdmin()){//如果不是管理员登录的话
            if($uid != $data['uid']){
                $this->error('你不能删除别人的图片');
            }            
        }
        
        
        $obj = new \common\components\UploadFile();
        $state = $obj->deletefile($_POST['filename']);
        if($state){
            $this->success('删除成功');
        }else{
            $this->error($obj->error);
        }
    }
    
    /**
     * $_GET['e_code'];
     * $_GET['shipping_code'];
     */
    public function actionGet_express()
    {
        $e_code = $_GET['e_code'];
        $shipping_code = $_GET['shipping_code'];
        $cacheKey = "getExpressDetail-{$e_code}{$shipping_code}";
        $expressDetail = yii::$app->cache->get($cacheKey);
        if (!empty($expressDetail)){
            $this->ajaxReturn(['status' => 1, 'content' => $expressDetail]);
        }
        
        $cUrl = new Curl();
        $url = 'http://www.kuaidi100.com/query?';
        $param = [
            'type' => $e_code,
            'postid' => $shipping_code,
            'id' => 1,
            'valicode' => '',
            'temp' => rand(1,9999),
        ];
        $url.=http_build_query($param);
        $contentByUrl = $cUrl->get($url, $_SERVER['HTTP_USER_AGENT'], '', 'http://www.kuaidi100.com/');
        $content = json_decode($contentByUrl, true);
        if ($content['status'] !== '200' || empty($content)) {
            $this->ajaxReturn(['status' => 0, 'msg' => $content['message']]);
        }
        $output = "";
        if (is_array($content['data']) && !empty($content['data'])) {
            foreach ($content['data'] as $k => $v) {
                if (!empty($v['time'])) {
                    $output .= ' <li>' . $v['time'] . '&nbsp;&nbsp;' . $v['context'] . '</li>';
                }
            }
        }
        if (empty($output)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '系统繁忙,暂未获取到物流信息']);
        }
        yii::$app->cache->set($cacheKey,$output,600);
        $this->ajaxReturn(['status' => 1, 'content' => $output]);
    }    
}

