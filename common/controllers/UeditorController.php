<?php

namespace common\controllers;
use common\components\BaseController;
use common\components\UploadFile;
use common\components\GuestInfo;
class UeditorController extends BaseController
{
    public $file_url = '';//图片的根目录的路径
    //编辑器初始配置
    private $confing = array(
        /* 上传图片配置项 */
        'imageActionName' => 'uploadimage',
        'imageFieldName' => 'upfilesss',
        'imageMaxSize' => 20971520, /* 上传大小限制，单位B */
        'imageAllowFiles' => array('.png', '.jpg', '.jpeg', '.gif', '.bmp'),
        'imageCompressEnable' => false,
        'imageCompressBorder' => 1600,
        'imageInsertAlign' => 'none',
        'imageUrlPrefix' => '',
        'imagePathFormat' => '',
        /* 涂鸦图片上传配置项 */
        'scrawlActionName' => 'uploadscrawl',
        'scrawlFieldName' => 'upfile',
        'scrawlPathFormat' => '',
        'scrawlMaxSize' => 0,
        'scrawlUrlPrefix' => '',
        'scrawlInsertAlign' => 'none',
        /* 截图工具上传 */
        'snapscreenActionName' => 'uploadimage',
        'snapscreenPathFormat' => '',
        'snapscreenUrlPrefix' => '',
        'snapscreenInsertAlign' => 'none',
        /* 抓取远程图片配置 */
        'catcherLocalDomain' => array('127.0.0.1', 'localhost', 'img.baidu.com'),
        'catcherActionName' => 'catchimage',
        'catcherFieldName' => 'source',
        'catcherPathFormat' => '',
        'catcherUrlPrefix' => '',
        'catcherMaxSize' => 20971520,
        'catcherAllowFiles' => array('.png', '.jpg', '.jpeg', '.gif', '.bmp'),
        /* 上传视频配置 */
        'videoActionName' => 'uploadvideo',
        'videoFieldName' => 'upfile',
        'videoPathFormat' => '',
        'videoUrlPrefix' => '',
        'videoMaxSize' => 20971520,
        'videoAllowFiles' => array(".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg", ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid"),
        /* 上传文件配置 */
        'fileActionName' => 'uploadfile',
        'fileFieldName' => 'upfile',
        'filePathFormat' => '',
        'fileUrlPrefix' => '',
        'fileMaxSize' => 20971520,
        'fileAllowFiles' => array('.png', '.jpg', '.jpeg', '.gif', '.bmp', ".flv", ".swf", 'zip', 'rar', '7z'),
        /* 列出指定目录下的图片 */
        'imageManagerActionName' => 'listimage',
        'imageManagerListPath' => '',
        'imageManagerListSize' => 20,
        'imageManagerUrlPrefix' => '',
        'imageManagerInsertAlign' => 'none',
        'imageManagerAllowFiles' => array('.png', '.jpg', '.jpeg', '.gif', '.bmp'),
        /* 列出指定目录下的文件 */
        'fileManagerActionName' => 'listfile',
        'fileManagerListPath' => '',
        'fileManagerUrlPrefix' => '',
        'fileManagerListSize' => '',
        'fileManagerAllowFiles' => array(".flv", ".swf",),
    );
    private $uid = 0;

    //初始化
    public function init() 
    {
        $this->file_url = \yii::$app->params['file_url'];
        if($_POST){
            $authkey = GuestInfo::getParam('authkey');
            $time = GuestInfo::getParam('time');
            $uid = GuestInfo::getParam('uid');
            $authcode = GuestInfo::getParam('authcode');

            $key = md5($authcode . $time . $uid);
            if ($key != $authkey) {
                exit(json_encode(array('state' => '身份认证失败！')));
            } else {
                $this->uid = $uid;
            }            
        }
        parent::init();
    }
    
    //上传涂鸦
    protected function uploadscrawl(&$result)
    {
        $base64_data = $_POST[$this->confing['scrawlFieldName']];//base数据
        if (empty($base64_data)) {
            exit(json_encode(['state' => '没有涂鸦内容！']));
        }
        $img = base64_decode($base64_data);//需要保存的字符串
        //目录
        //你想要保存的文件名
        $savepath = "ueditor/uploadscrawl";
        $filename = uniqid().'.png';
        $filepath = "{$savepath}/{$filename}";
        $uploadfile = new UploadFile();
        $state = $uploadfile->saveObject($filepath, $base64_data);
        if($state){
            
            $oriName = 'scrawl.png';//占时不清楚这个是用来干什么的
            $result = [
                'state' => 'SUCCESS', //成功返回标准，否则是错误提示
                'url' => $this->file_url . "/$filepath",
                'title' => $filename, //上传后的文件名
                'original' => $oriName, //原来的
                'type' => 'png', //后缀
                'size' => strlen($img), //大小
            ];            
        }else{
            exit(json_encode(['state' => '保存失败']));
        }    
    }    
    
    public function actionIndex()
    {
        $action = GuestInfo::getParam('action');
        $result = [];
        switch ($action) {
            case 'config':
                $result = $this->confing;
                break;
            case 'uploadscrawl':
                $this->uploadscrawl($result);
                break;
            case 'uploadimage':
                $this->uploadimage($result);
                break;
            
            default:
                break;
        }
        exit(json_encode($result));
    }
    
    //上传头像
    protected function uploadimage(&$result)
    {
        $savepath = "ueditor/uploadimage";
        $uploadfile = new UploadFile();
        $file = $uploadfile->upload($savepath);
        
        
        if($file){
            $result = [
                'state' => 'SUCCESS', //成功返回标准，否则是错误提示
                'url' => $this->file_url . "/{$file['path_file']}",
                'title' => $file['file_name'], //上传后的文件名
                'original' => $this->file_url . "/{$file['path_file']}",
                'type' => $uploadfile->getFileExt($file['file_name']), //后缀
                'size' => $file['size'], //大小
            ];
        }else{
            exit(json_encode(['state' => $uploadfile->error]));
        }    
    }
}
