<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%activity_coupon}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $start_time
 * @property integer $end_time
 * @property string $label
 * @property integer $money
 * @property string $content
 * @property integer $max_num
 * @property integer $use_num
 * @property integer $limit_num
 * @property integer $add_time
 * @property string $img
 */
class ActivityCouponModel extends \common\components\baseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%activity_coupon}}';
    }

    /**
     * @inheritdoc
     */
//    public function rules()
//    {
//        return [
//            [['start_time', 'end_time', 'money', 'max_num', 'use_num', 'limit_num', 'add_time'], 'integer'],
//            [['content'], 'string'],
//            [['title', 'label'], 'string', 'max' => 50],
//            [['img'], 'string', 'max' => 200],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '活动主题',
            'start_time' => '开始时间',
            'end_time' => '结束时间',
            'label' => '会员',
            'money' => '购买金额',
            'content' => '内容',
            'max_num' => '最大量',
            'use_num' => '已使用',
            'limit_num' => '一个会员最多购买的数量',
            'add_time' => '添加时间',
            'img' => '图片',
        ];
    }
    
    //生成用户的优惠券
    public static function addYouhui($id,$num = 1,$uid = '')
    {
        $uid = empty($uid) ? \yii::$app->controller->user->uid : $uid;
        $model = static::findOne($id);
        for($i = 0;$i<$num;$i++){
            $arr_content = json_decode($model['content'], true);
            foreach($arr_content as $content){
                $model_youhui = new MemberCouponModel();
                $data = [];
                $data['uid'] = $uid;
                $data['is_overlay'] = empty($content['is_overlay']) ? 0 : 1;     
                $data['money'] = $content['money'];            
                $data['invalid_time'] = $model['invalid_time'];            
                $data['must_money'] = $content['must_money'];            
                $data['goods_ids'] = $content['goods_ids'];            
                $data['is_tejia'] = empty($content['is_tejia']) ? 0 : 1;     
                $data['remark_relation'] = $model['id'];
                $model_youhui->attributes = $data;
                $model_youhui->save(false);
            }
        }
        return true;
    }
}
