<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%activity_order}}".
 *
 * @property string $order_id
 * @property string $add_time
 * @property string $add_microtime
 * @property string $money
 * @property string $red_id
 * @property string $red_money
 * @property string $use_money
 * @property string $uid
 * @property integer $auto_user
 * @property string $recharge_order_id
 * @property string $addmoney_record_id
 */
class ActivityOrderModel extends \common\components\baseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%activity_order}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'add_time' => 'Add Time',
            'add_microtime' => 'Add Microtime',
            'money' => 'Money',
            'red_id' => 'Red ID',
            'red_money' => 'Red Money',
            'use_money' => 'Use Money',
            'uid' => 'Uid',
            'auto_user' => 'Auto User',
            'recharge_order_id' => 'Recharge Order ID',
            'addmoney_record_id' => 'Addmoney Record ID',
        ];
    }
}
