<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_ad_list".
 *
 * @property integer $id
 * @property string $post_alias
 * @property string $title
 * @property integer $jump_type
 * @property string $jump_data
 * @property string $img
 * @property integer $sorts
 * @property integer $be_id
 * @property integer $status
 * @property integer $start_time
 * @property integer $end_time
 * @property integer $add_time
 * @property integer $update_time
 * @property string $content
 * @property string $v_param
 */
class AdListWapModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'go_ad_list_wap';
    }
    public function rules()
    {
        return [

        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_alias' => '广告位',
            'title' => '标题',
            'jump_type' => '跳转类型',
            'jump_data' => '跳转值',
            'img' => '图片',
            'sorts' => '排序',
            'be_id' => '关联ID',
            'status' => '状态',
            'start_time' => '开始时间',
            'end_time' => '结束时间',
            'add_time' => '创建时间',
            'update_time' => '修改时间',
            'content' => '扩展',
            'v_param' => '马甲',
            'title_color'=>'标题颜色',
            'title_second'=>'副标题',
            'descript'=>'描述',
            'ext_img'=>'扩展图片',
            'platform'=>'平台',
            'channel'=>'渠道',
            'is_sh'=>'审核显示',
            'param1' => '参数1',
            'param2' => '参数2',
            'param3' => '参数3',
            'param4' => '参数4',
            'param5' => '参数5',
            'param6' => '参数6',
            'content' => '相关内容',
            'img' => '单图',
            'img_list' => '列表图',
        ];
    } 
    
    public function getPost_with()
    {
        return $this->hasOne(\common\models\AdPostWapModel::className(), ['keywords'=>'post_alias'])->select(['keywords','name']);
    }
    
    public function getJump_with()
    {
        return $this->hasOne(\common\models\ConfigAppJumpModel::className(), ['click_type'=>'jump_type'])->select(['click_type','name']);
    } 
    
    public function afterSave($insert, $changedAttributes) {
        $state = parent::afterSave($insert, $changedAttributes);
        static::getList($this->post_alias, "", "", false);
        return $state;
    }
    
    //对应前端接口找到符合条件的广告位
    public static function getList($post_alias = "",$is_cache = true,$cache_time = 3600)
    {
        
        
        //加上缓存处理起来会好一点
        $cache_key = "adlist_wap_{$post_alias}";
        $dd = \yii::$app->cache->get($cache_key);
        if($is_cache && $dd){
            return $dd;
        }
        
        
        $post_alias = empty($post_alias) ? \common\components\GuestInfo::getParam("post_alias") : $post_alias;
        $v_param = empty($v_param) ? \common\components\GuestInfo::getParam("v_param") : $v_param;
        $channel = empty($channel) ? \common\components\GuestInfo::getParam("channel") : $channel;
        //是在条件里面过滤掉还是在  找出来之后一个一个的过滤掉呢？
        $condition['pagesize'] = 99999;
        $condition['page'] = 1;
        $condition['status'] = 1;//显示的
        $condition['noget'] = true;
        $condition['post_alias'] = $post_alias;//这一个类型的
        if(!$condition['post_alias']){
            return [];
        }
        $condition['orderBy'] = "sorts desc";
        $datas = static::baseGetDatas($condition, false);//先把所有的符合条件的给找出来 
//        $datas = static::baseGetDatas($condition, false,false,true);//先把所有的符合条件的给找出来 
//        print_r($datas);exit;
        $now_time = time();
        $max_num = AdPostWapModel::getOne($post_alias, "num");
        $init_num = 0;
        if(!$max_num){
            $max_num = 999999;
        }
        
        $result = [];//把需要的参数给拿出来就可以了，不需要所有的参数都要有的
        
        $type_model = \common\models\AdPostWapModel::findOne($post_alias);
        if($type_model){
            $des = json_decode($type_model->des,true);//看到这样的一个配置信息
            $config = [];
            if($des){
                foreach($des as $k => $v){
                    $config[$v['param']] = $v;
                }
            }            
        }else{
            $config = [];
        }

        
        
        
        //再过滤掉我们不需要的.   从马甲 渠道 时间 用户等过滤
        foreach($datas as $data){
            //过滤完
            if($init_num >= $max_num){//已经数据找够了
                break;
            }            
            if($data['start_time'] && $now_time < $data['start_time']){
                continue;
            }
            if($data['end_time'] && $now_time > $data['end_time']){
                continue;
            }
//            if(!\common\components\Tool::v_data($v_param, $data['v_param'])){//说明马甲不符合条件 
//                continue;
//            }
//            if(!\common\components\Tool::v_data($channel, $data['channel'])){//说明渠道不符合条件 
//                continue;
//            }
            
            
            $result[$init_num]['id'] = $data['id'];
            $result[$init_num]['title'] = $data['title'];
            $result[$init_num]['img'] = $data['img'];
            $result[$init_num]['img_list'] = $data['img_list'];
            $result[$init_num]['jump_type'] = $data['jump_type'];
            $result[$init_num]['jump_data'] = $data['jump_data'];
            
            foreach($config as $con_key => $con_value){
                if(isset($data[$con_key])){//value
                    $k = $con_value['key_data'];
                    $result[$init_num][$k] = $data[$con_key];
                }
            }
 
            //$result[$init_num] = $data;
            $init_num++;
        }
        \yii::$app->cache->set($cache_key,$result,$cache_time);
        return $result;
    }    
}
