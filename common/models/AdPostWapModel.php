<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_ad_post".
 *
 * @property integer $id
 * @property string $alias
 * @property string $name
 */
class AdPostWapModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'go_ad_post_wap';
    }    
    public static function getKeyValue($all=true){
        
    	$res=self::find()->select('keywords,name')->all();
    	$data=[];
    	if($all){
    	    $data['']='请选择';
    	}
    	foreach ($res as $k=>$v){
    	    $data[$v->keywords]=$v->name;
    	}
    	return $data;
    }    
    public function attributeLabels()
    {
        return [
            'keywords' => '关键字',
            'name' => '类型名称',
            'descript' => '类型描述',
            'num' => '展示数量(0不限制)',
            'add_time' => '添加时间',
        ];
    } 
}
