<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%admin_member}}".
 *
 * @property string $uid
 * @property string $username
 * @property string $password
 * @property string $add_time
 * @property string $last_login_time
 * @property string $role_id
 */
class AdminMember extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_time', 'last_login_time', 'role_id'], 'integer'],
            [['username'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'username' => 'Username',
            'password' => 'Password',
            'add_time' => 'Add Time',
            'last_login_time' => 'Last Login Time',
            'role_id' => 'Role ID',
        ];
    }
}
