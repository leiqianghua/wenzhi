<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%attachment}}".
 *
 * @property string $id
 * @property string $url
 * @property integer $add_time
 * @property string $path
 * @property string $file
 * @property string $pathfile
 * @property integer $uid
 * @property integer $is_admin
 */
class AttachmentModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%else_attachment}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'add_time' => 'Add Time',
            'path' => 'Path',
            'file' => 'File',
            'pathfile' => 'Pathfile',
            'uid' => 'Uid',
            'is_admin' => 'Is Admin',
        ];
    }
}
