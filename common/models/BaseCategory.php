<?php

namespace common\models;

use Yii;

/**
 * // 必须含有  id 主键
 *  parent_id 父ID
 * sorting 排序
 * add_time 添加时间
 * status 状态
 * label 层数
 * title标题
 *  这些字段
 */
class BaseCategory extends \common\components\BaseModel
{

    //获取所有的排好序的菜单  键值为   主键的值
    //  1 => []  2 => []  二维数组
    public static function getList()
    {
        static $all;
        if($all){
            return $all;
        }
        $datas = static::find()->orderBy("parent_id asc,sorting desc,id asc")->asArray()->all();
        $contents = [];
        foreach($datas as $data){
            $contents[$data['id']] = $data;
        }
        return $all = $contents;
    }    
    
    
    /**
     * 根据ID来获取其子分类包括自身
     * 此分类ID不能为0，不能为空。
     * $layer 取几层的数据  默认false所有的都取出来
     * 格式   a => 'af'， data => [data的数据]
     * @return [type] [description]
     */    
    public static function getListForID($id, $layer = false, $layer_num = 1)
    {
        $datas = static::getList();

        $arrResult = $datas[$id];
        $arrResult['Layer'] = $layer_num;
        $arrResult['data'] = array();
        $layer_num++;
        if($layer && $layer_num > $layer){
                return $arrResult;
        }		
        foreach ($datas as $cid => $data) {
            if($data['parent_id'] == $id){
                $arrResult['data'][$cid] = static::getListForID($cid, $layer, $layer_num);
            }
        }
        return $arrResult;
    }
    
    
    /**
     * 获取所有的数据  一直往下的排
     * $layer定义了第几层  如果是第1层则获取所有第一层的数据
     * @param  boolean $layer [description]
     * @return [type]            [description]
     */
    public static function  getListMenu($layer = 1)
    {
        $cache = [];
        $datas = static::getList();
        foreach ($datas as $id => $data) {
            if($data['parent_id'] == 0){
                $cache[$id] = static::getListForID($id, $layer, 1);
            }
        }
        return $cache;       
    }    
    
    
    
    /**
     * 获取子栏目ID列表,,返回数组  包括自身
     * 取其底下的所有的1,2,3格式
     * @param type $catid id
     * @return string 返回栏目子列表，以逗号隔开
     */
    public static function getArrChildidDetail($catid)
    {
        //栏目数据
    	$datas = static::getList();
        $arrchildid[] = $catid;
        foreach ($datas as $id => $cat) {
            if ($cat['parent_id'] && $id != $catid && $cat['parent_id'] == $catid) {
                $arrchildid = array_merge($arrchildid,static::getArrChildidDetail($id));
            }
        }
        return $arrchildid;
    }

    
    

    
    /**
     * 使用递归的方式，获取全部的父栏目id列表  包括自身
     * @param type $id 栏目ID
     * @return string 返回父栏目id列表，以逗号隔开
     */
    public static function getArrParentid($id) 
    {
    	$result[] = $id;
        $datas = static::getList();
        $parent = $datas[$id]['parent_id'];//当前栏目的父ID
        if($parent != 0){
            $result = array_merge($result,static::getArrParentid($parent));
        }
        return $result;
    }

   
   //根据具体的数据获取其输出  $datas  若干的数据组成  是需要父ID是0的
    public static function  getListMenuForDatas($datas,$layer = false)
    {
        $cache = [];
        foreach ($datas as $id => $data) {
            if($data['parent_id'] == 0){
                $cache[$id] = static::getListForIDForDatas($datas, $id, $layer, 1);
            }
        }
        return $cache;       
    } 
    
    
   //根据具体的数据获取其输出  $datas  若干的数据组成  是需要父ID是0的
    public static function  getListForArr($datas)
    {
        $result = [];
        $num = 0;
        foreach ($datas as $key => $value){
            $result[$num] = $value;
            if(!empty($value['data'])){
                $result[$num]['data'] = static::getListForArr($value['data']);
            }else{
                $result[$num]['data'] = [];
            }
            $num++;
        }
        return $result;
    }
    
    
    
    
    //根据具体数据与id来获取其底下输出
    public static function getListForIDForDatas($datas, $id, $layer = false, $layer_num = 1)
    {
        $arrResult = $datas[$id];
        $arrResult['Layer'] = $layer_num;
        $arrResult['data'] = array();
        $layer_num++;
        if($layer && $layer_num > $layer){
                return $arrResult;
        }
        foreach ($datas as $cid => $data) {
            if($data['parent_id'] == $id){
                $arrResult['data'][$cid] = static::getListForIDForDatas($datas,$cid, $layer, $layer_num);
            }
        }
        return $arrResult;
    }
    


    

    //把相应的菜单信息展示好，按相应的格式一个个的来展示
    // 输出后，然后按其二维数据来展示相关的所有的数据
    public static function showMenu()
    {
        $list = static::getListMenu(false);
        $result = [];
        foreach($list as $key => $value){
            $d = static::getShowMenu($value);
            $result = array_merge($result,$d);
        }
        return $result;
    }
    
    //通过数据把其变成二维数组
    public static function getShowMenu($datas)
    {
        $result = [];
        if(empty($datas)){
            return [];
        }
        if(isset($datas['id'])){
            $d[0] = $datas;
            $datas = $d;
        }
        foreach($datas as $data){
            $result_data = $data['data'];
            unset($data['data']);
            $result[] = $data;
            $result = array_merge($result,static::getShowMenu($result_data));
        }
        return $result;
    }
    
    //获取所有的key value对应的这个菜单
    public static function getMenuKeyValue($insert_parent = true)
    {
        $datas = static::showMenu();
        if($insert_parent){
            $result[0] = '请选择';
        }else{
            $result = [];
        }
        
        foreach($datas as $key => $value){
            $v = $value['title'];
            $html = '';
            for($i = 1;$i < $value['Layer'];$i++){
                $html .= "├ ";
            }
            $result[$value['id']] = $html . $v;
        }
        return $result;
    }
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '不显示',
                '1' => '显示',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }
    
    //删除后事件回调
    public static function beforeDeleteAll(&$condition = '', $params = [])
    {
        if(!empty($condition['id'])){
            $ids = [];
            if(is_array($condition['id'])){
                $ids = $condition['id'];
            }else{
                $ids[0] = $condition['id'];
            }
            $ji_ids = [];
            foreach($ids as $value){
                $ji_ids = array_merge($ji_ids,static::getArrChildidDetail($value));//此菜单 ID和其底下的所有的父级
            }
            $condition['id'] = $ji_ids;
        }
        return true;
    }
    




    //单个的执行删除
    public function afterDelete() {
        parent::afterDelete();
        $class_name = get_class($this);
        $condition['id'] = $this->id;
        $class_name::deleteAll($condition);
    }




    
    public function beforeSave($insert)
    {

        if(empty($this->parent_id)){
            $this->label = 1;
        }else{
            $d = static::findOne($this->parent_id);
            if(!$d){
                $this->addError('parent_id', '找不到此父级ID菜单');
                return false;
            }
            $this->label = $d->label + 1;
        }
        return parent::beforeSave($insert);
    }
        
}
