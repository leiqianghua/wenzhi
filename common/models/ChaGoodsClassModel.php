<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_class}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $status
 * @property integer $sorting
 * @property string $add_time
 * @property integer $label
 * @property string $title
 */
class ChaGoodsClassModel extends \common\models\GoodsClassModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cha_goods_class}}';
    }
}
