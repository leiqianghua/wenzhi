<?php

namespace common\models;

use Yii;
use Exception;
/**
 * This is the model class for table "{{%goods}}".
 *
 * @property string $id
 * @property string $name
 * @property string $price
 * @property string $discount_1
 * @property string $discount_2
 * @property string $discount_3
 * @property string $add_time
 * @property integer $sorting
 * @property string $cid
 * @property string $img
 * @property string $img_list
 * @property string $details
 * @property integer $status
 */
class ChaGoodsCommonModel extends GoodsCommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cha_goods_common}}';
    }

    
    
    //根据其POST的数据获取我们想要的数据
    public static function getDataForPost()
    {
        $_POST = array_merge($_POST,$_GET);
        empty($_POST['ifcart']) ? $_POST['ifcart'] = 0 : '';//是否来源购物车
        empty($_POST['invalid_cart']) ? $_POST['invalid_cart'] = [] : '';//有无无效商品
        empty($_POST['goods_id']) ? $_POST['goods_id'] = 0 : '';//商品ID
        if(empty($_POST['goods_id'])){
            if(isset($_POST['id'])){
                $_POST['goods_id'] = $_POST['id'];
            }
        }
        empty($_POST['num']) ? $_POST['num'] = 0 : '';//购物数量
        empty($_POST['shop_list']) ? $_POST['shop_list'] = '' : '';//购物的东西 多渠道
        empty($_POST['cart_list']) ? $_POST['cart_list'] = '' : '';//直接来源购物车
        empty($_POST['type']) ? $_POST['type'] = '1' : '';//直接来源购物车
        
        if(is_string($_POST['invalid_cart'])){
            $_POST['invalid_cart'] = json_decode($_POST['invalid_cart'],true);
        }        
        if(is_string($_POST['cart_list'])){
            $_POST['cart_list'] = json_decode($_POST['cart_list'],true);
        }
        
        
        $datas = [];
        
        if($_POST['ifcart']){//来源于购物车，找到所有的商品列表
            if(!empty($_POST['invalid_cart'])){
                //\common\models\GoodsCartModel::deleteAll(['id' => $_POST['invalid_cart']]);
            }
            $putong = [];
            foreach($_POST['cart_list'] as $key => $value){
                $model = \common\models\GoodsCartModel::findOne($value);
                if($model['type'] == 2){
                    $d = json_decode($model['shop_list'],true);
                    $lishi = [];
                    $lishi['name'] = $model['goods_name'];
                    $lishi['id'] = $model['id'];//购物车的ID
                    $lishi['gid'] = $model['goods_id'];//内部商城的ID
                    
                    //特殊的东西
                    $other = json_decode($model['other_data'], true);
                    $lishi['other_data'] = $other;
                    
                    foreach($d as $k => $v){
                        $l = ChaGoodsModel::findOne($v['goods_id']);
                        $l = $l->attributes;
                        $l['buy_num'] = $v['quantity'];
                        $lishi['data'][] = $l;
                    }
                    $datas[] = $lishi;
                }else{
                    //普通的商品
                    $l = [];
                    $l = ChaGoodsModel::findOne($model['goods_id']);
                    $l = $l->attributes;
                    $l['buy_num'] = $model['num'];
                    $putong[] = $l;
                }
            }
            if(!empty($putong)){
                $li = [];
                $li['name'] = '单件商品';
                $li['id'] = -1;
                $li['gid'] = -1;
                $li['data'] = $putong;
                $datas[] = $li;
            }
        }else{//不是来源于购物车
            if($_POST['type'] == 2){//内部商城
                $model = \common\models\GoodsPackageModel::findOne($_POST['goods_id']);
                
                $datas[0]['name'] = $model['name'];
                $datas[0]['id'] = $model['id'];
                $datas[0]['gid'] = $model['id'];
                
                //特殊的东西
                if(!empty($_POST['fujia'])){
                    $datas[0]['other_data'] = explode(',', $_POST['fujia']);       
                }

                $d = explode(',', $_POST['shop_list']);
                
                foreach($d as $k => $v){
                    $ll = explode('|', $v);
                    $v = [];
                    $v['goods_id'] = $ll[0];
                    $v['quantity'] = $ll[1];
                    $lishi = ChaGoodsModel::findOne($v['goods_id']);
                    $lishi = $lishi->attributes;
                    $lishi['buy_num'] = $v['quantity'];
                    $datas[0]['data'][] = $lishi;
                }
                
            }else{
                $datas[0]['name'] = '单件商品';
                $datas[0]['id'] = -1;
                $datas[0]['gid'] = -1;
                $lishi = [];
                $lishi = ChaGoodsModel::findOne($_POST['goods_id']);
                $lishi = $lishi->attributes;
                $lishi['buy_num'] = $_POST['num'];
                $datas[0]['data'][] = $lishi;
            }
        }
        
        
        

        if($_POST['ifcart']){
            $d = GoodsCartModel::find()->where(['id' => $_POST['cart_list']])->asArray()->limit(1)->one();
            if(!$d){
                return false;
            }
        } 
        
        return $datas;
    }
    
    //计算商品金额  包括扣了后跟没有扣的价格
    public static function calcTotalMoney($datas = [],$uid = '')
    {
        $uid === '' ? $uid = \yii::$app->user->uid : '';
        if(empty($datas)){
            $datas = $this->getDataForPost();
        }
        $member = MemberModel::findOne($uid);
        $label = $member['label'];
        
        $total_money = 0;
        $total_money_zhe = 0;
        $fujia = 0;
        $total_score = 0;
        
        foreach($datas as $data){
            foreach($data['data'] as $d){
                $total_money += $d['price'] * $d['buy_num'];
                $total_score += $d['score'] * $d['buy_num'];
                if($label == 0){//出厂价
                    $total_money_zhe += $d['buy_num'] * $d['discount_0'];
                }else if($label == 1){
                    $total_money_zhe += $d['buy_num'] * $d['discount_1'];
                }else if($label == 2){
                    $total_money_zhe += $d['buy_num'] * $d['discount_2'];
                }else if($label == 3){
                    $total_money_zhe += $d['buy_num'] * $d['discount_3'];
                }else{
                    $total_money_zhe += $d['buy_num'] * $d['discount_4'];
                }
            }
            
            if(!empty($data['other_data'])){
                foreach($data['other_data'] as $id){
                    $model = ChaGoodsModel::findOne($id);
                    if(!$model){
                        $fujia += 300;
                    }else{
                        $fujia += $model['price'];
                    }
                }
            }            
        }
        //商品总金额  商品折扣金额  附加费金额
        return [$total_money,$total_money_zhe,$fujia,$total_score];
    }
    
    
   
    //返回订单模型
    public static function creataOrder($address_id)
    {
        $post = $_POST;
        try {
            $cache_key = "creataOrder_" . \yii::$app->user->uid;
            $transaction = \yii::$app->db->beginTransaction();
            $state = \common\components\Tool::checkLock($cache_key);
            if(!$state){
                return false;
            }
            
            $datas = ChaGoodsModel::getDataForPost();//获取来源的数据
            static::buy2Verification($datas);//验证  是不是有购买失败的东西，库存不足什么的
            
            
            $total_money = static::calcTotalMoney($datas);
            $freight = static::calcFreight($address_id, $datas);//运费
            $buy_money = $freight + $total_money[1];
            
            //然后购买，减库存，创建订单
            
            //验证通过后，开始创建订单,订单创建好了后要把相应的数量减少
            //创建订单，创建订单商品，减少库存
            $data = [];
            $data['order_sn'] = static::makePaySn(\yii::$app->user->uid);
            $data['uid'] = \yii::$app->user->uid;
            $data['username'] = \yii::$app->user->username;
            $data['goods_amount'] = $total_money[0];
            $data['goods_amount_discount'] = $total_money[1];
            $data['order_amount'] = $buy_money;
            $data['freight'] = $freight;
            $data['update_time'] = time();
            //这一笔订单的收货信息
            $address_arr = \common\models\MemberAddressModel::getOne($address_id);
            $data['address'] = $address_arr['address_info'] . $address_arr['address'];
            $data['phone'] = $address_arr['iphone'];
            $data['user_truename'] = $address_arr['username'];
            
            
            $order_model = new OrderModel();
            $order_model->attributes = $data;
            $order_model->save(false);
            
            
            //减少相应的库存，与增加相应的销量
            $shop = [];
            foreach($datas as $data){
                foreach($data['data'] as $d){
                    $shop[$d['id']]['num'] = $d['num'];
                    if(empty($shop[$d['id']]['buy_num'])){
                        $shop[$d['id']]['buy_num'] = $d['buy_num'];
                    }else{
                        $shop[$d['id']]['buy_num'] += $d['buy_num'];
                    }
                }
            }        
            foreach($shop as $shop_id => $data){
                $express = new \yii\db\Expression("num-{$data['buy_num']}");
                $express1 = new \yii\db\Expression("sale_num+{$data['buy_num']}");
                $state = static::updateAll(['num' => $express,'sale_num' => $express1], ['id' => $shop_id]);
                if(!$state){
                    throw new Exception('商品库存不足');
                }
            }            
            
            
            //创建相关的订单商品   再创建订单商品   再创建订单套件商品
            foreach($datas as $data){
                if($data['gid'] == -1){//这里的都是普通的商品
                    foreach($data['data'] as $k => $v){
                        $d = [];
                        $d['order_id'] = $order_model->order_id;
                        $d['goods_id'] = $v['id'];
                        $d['type'] = 1;
                        $d['name'] = $v['name'];
                        $d['img'] = $v['img'];
                        $d['uid'] = \yii::$app->user->uid;
                        $d['username'] = \yii::$app->user->username;
                        $d['money'] = $v['price']* $v['buy_num'];
                        $label = \yii::$app->user->label;
                        if($label == 0){//出厂价
                            $money_zhe = $v['buy_num'] * $v['discount_0'];
                        }else if($label == 1){
                            $money_zhe = $v['buy_num'] * $v['discount_1'];
                        }else if($label == 2){
                            $money_zhe = $v['buy_num'] * $v['discount_2'];
                        }else if($label == 3){
                            $money_zhe = $v['buy_num'] * $v['discount_3'];
                        }else{
                            $money_zhe = $v['buy_num'] * $v['buy_num'];
                        }                        
                        $d['discount_money'] = $money_zhe;
                        $d['num'] = $v['buy_num'];
                        $order_shop_model = new OrderGoodsModel();
                        $order_shop_model->attributes = $d;
                        $order_shop_model->save(false);
                    }
                }else{//这里的是内部商城
                    $model = GoodsPackageModel::findOne($data['gid']);
                    $d = [];
                    $d['order_id'] = $order_model->order_id;
                    $d['goods_id'] = $data['gid'];
                    $d['type'] = 2;
                    $d['name'] = $model['name'];
                    $d['uid'] = \yii::$app->user->uid;
                    $d['username'] = \yii::$app->user->username;
                    $d['money'] = 0;                  
                    $d['discount_money'] = 0;
                    $d['num'] = 1;   
                    $ddd = [];
                    foreach($data['data'] as $ll){
                        $lihsi = [];
                        $lihsi['goods_id'] = $ll['id'];
                        $lihsi['buy_num'] = $ll['buy_num'];
                        $ddd[] = $lihsi;
                    }
                    $d['shop_list'] = json_encode($ddd);
                    $order_shop_model = new OrderGoodsModel();
                    $order_shop_model->attributes = $d;
                    $order_shop_model->save(false);                    
                }
            }

            //删除购物车数据
            //删除购物车中的商品
            if ($_POST['ifcart']) {
                GoodsCartModel::deleteAll(['id' => $_POST['cart_list']]);
            }
            
            
            
            $transaction->commit();
            \common\components\Tool::deleteLock($cache_key);
            return $order_model;//订单模型
            
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            $transaction->rollBack();
            \common\components\Tool::deleteLock($cache_key);
            return false;
        }
    }       
    
    
    
    //新增或者是修改后的操作
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($_POST){
            try {
                $transaction = Yii::$app->db->beginTransaction();//开启事务   
                if(!empty($_POST['label'])){//把旧的删除掉，然后留下新的
                    GoodsLabelDataModel::deleteAll(['goods_id' => $this->id]);
                    $time = time();
                    foreach($_POST['label'] as $key => $value){
                        $model = new GoodsLabelDataModel();
                        $model->label_id = $value;
                        $model->goods_id = $this->id;
                        $model->save(false);
                    }
                }else{
                    GoodsLabelDataModel::deleteAll(['goods_id' => $this->id]);
                }
                
                //GoodsModel::deleteAll(['common_id' => $this->id]);//把之前的删除掉先。
                $gods_common = ChaGoodsModel::findAll(['common_id' => $this->id]);
                $gods_common_arr = [];
                if($gods_common){
                    foreach($gods_common as $k => $v){
                        $gods_common_arr[$v['id']] = $v;
                    }
                }
                $use_goods_id = [];
                
                $datas = json_decode($this->spec_value,true);
                $spec_1_data = json_decode($this->spec_1,true);
                $spec_2_data = json_decode($this->spec_2,true);
                $spec_3_data = json_decode($this->spec_3,true);
                foreach($datas as $spec_1_key => $value_1){
                    foreach($value_1 as $spec_2_key => $value_2){
                        foreach($value_2 as $spec_3_key => $value_3){
                            if(!empty($value_3['goods_id'])){
                                if(!empty($gods_common_arr[$value_3['goods_id']])){
                                    $model = $gods_common_arr[$value_3['goods_id']];
                                    $use_goods_id[] = $value_3['goods_id'];
                                    
                                }else{
                                    $model = new ChaGoodsModel();
                                }
                            }else{
                                $model = new ChaGoodsModel();
                            }
                            
                            $arr = $this->attributes;;
                            unset($arr['id']);
                            $model->attributes = $arr;
                            $model->common_id = $this->id;
                            $model->spec_1_num = $spec_1_key;
                            $model->spec_2_num = $spec_2_key;
                            $model->spec_3_num = $spec_3_key;
                            $model->price = $value_3['price'];
                            $model->discount_0 = $value_3['discount_0'];
                            $model->discount_1 = $value_3['discount_1'];
                            $model->discount_2 = $value_3['discount_2'];
                            $model->discount_3 = $value_3['discount_3'];
                            $model->discount_4 = $value_3['discount_4'];
                            $model->num = $value_3['num'];
                            
                            if(!empty($spec_1_data['spec_name'])){
                                $n = 0;
                                $spec_name_result = "";
                                foreach($spec_1_data['data'] as $k => $v){
                                    if($spec_1_key == $n){
                                        $spec_name_result = $v['spec_name'];
                                        break;
                                    }
                                    $n++;
                                }
                                $model->name = $model->name . " " . $spec_1_data['spec_name'] . ":" . $spec_name_result;
                            }  
                            if(!empty($spec_2_data['spec_name'])){
                                $n = 0;
                                $spec_name_result = "";
                                foreach($spec_2_data['data'] as $k => $v){
                                    if($spec_2_key == $n){
                                        $spec_name_result = $v['spec_name'];
                                        break;
                                    }
                                    $n++;
                                }
                                $model->name = $model->name . " " . $spec_2_data['spec_name'] . ":" . $spec_name_result;
                            }                             
                            if(!empty($spec_3_data['spec_name'])){
                                $n = 0;
                                $spec_name_result = "";
                                foreach($spec_3_data['data'] as $k => $v){
                                    if($spec_3_key == $n){
                                        $spec_name_result = $v['spec_name'];
                                        break;
                                    }
                                    $n++;
                                }
                                $model->name = $model->name . " " . $spec_3_data['spec_name'] . ":" . $spec_name_result;
                            } 
                            
                            $model->save(false);
                            $goods_id = $model->id;
                            $datas[$spec_1_key][$spec_2_key][$spec_3_key]['goods_id'] = $goods_id;
                        }                        
                    }
                }
                
                
                if($gods_common_arr){
                    foreach($gods_common_arr as $k => $v){
                        if(!in_array($k, $use_goods_id)){//没有用过的则删除掉
                            $v->delete();
                        }
                    }
                }
                
                //$this->spec_value = json_encode($datas);
                //print_r($datas);exit;
                ChaGoodsCommonModel::updateAll(['spec_value' => json_encode($datas)], ['id' => $this->id]);
                $transaction->commit();//提交事务                
            } catch (\Exception $exc) {
                $transaction->rollBack();//释放事务
                print_r("行数:" . $exc->getLine() . "文件："  . $exc->getfile() . "错误信息：" . $exc->getMessage() . "调试信息" . $exc->getTraceAsString());exit;
            }

        }
        return true;
    }
    
    public function afterDelete() {
        parent::afterDelete();
        GoodsLabelDataModel::deleteAll(['goods_id' => $this->id]);
        ChaGoodsModel::deleteAll(['common_id' => $this->id]);
        return true;
    }
    
    public static function afterDeleteAll($condition = '', $params = array()) {
        if(!empty($condition['id'])){
            GoodsLabelDataModel::deleteAll(['goods_id' => $condition['id']]);
            ChaGoodsModel::deleteAll(['common_id' => $condition['id']]);
        }
        return true;
    }
    
    //获取标签
    public function getLabels()
    {
        $data = GoodsLabelDataModel::find()->asArray()->where(['goods_id' => $this->id])->all();
        return $data;
    }


}
