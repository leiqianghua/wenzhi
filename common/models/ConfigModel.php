<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%config}}".
 *
 * @property string $varname
 * @property string $info
 * @property string $vlaue
 */
class ConfigModel extends \common\components\BaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%config}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['varname', 'info', 'value', 'type'], 'required'],
            //[['varname', 'info', 'value'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'varname' => '配置项标识',
            'info' => '配置向描述',
            'value' => '配置项的值',
        ];
    }

    //取若干的配置 根据key

    public static function getListForKey($key_list)
    {
        if (is_array($key_list)) {
            $key_cache = json_encode($key_list);
        } else {
            $key_cache = $key_list;
        }
        if ($cache = \yii::$app->cache->get($key_cache)) {
            return $cache;
        }

        if (is_array($key_list)) {
            $datas = self::find()->where(['varname' => $key_list])->asArray()->all();
            $result = [];
            foreach ($datas as $data) {
                $result[$data['varname']] = $data['value'];
            }
            \yii::$app->cache->set($key_cache, $result, 7200);
            return $result;
        } else {
            $datas = self::find()->where(['varname' => $key_list])->asArray()->one();
            $d = empty($datas['value']) ? '' : $datas['value'];
            \yii::$app->cache->set($key_cache, $d, 7200);
            return $d;
        }
    }

    //获取的配置参数并以配置参数个对象
    public function getObj($type = 1)
    {
        //1 seo配置 2上传配置
        $type = (int) $type;
        //$key_list=array('short_web_name','web_copyright','web_describe','web_keywords','web_logo','web_name','web_path');
        $datas = self::find()->where(['type' => $type])->orderBy('info')->all();
        return $datas;
    }

    //保存seo配置信息
    public function saveConfig($para, $type)
    {
        $key_list = self::find()->where(['type' => $type])->column();
        //$key_list=array('short_web_name','web_copyright','web_describe','web_keywords','web_logo','web_name','web_path');
        //合法性验证
        $keyA = self::getKeyArray($para);
        if (is_array($keyA)) {
            if (count($keyA) <= count($key_list)) {
                $check = 1;
                foreach ($keyA as $v) {
                    if (!in_array($v, $key_list)) {
                        $check = 0;
                    }
                }
                if ($check) {
                    //数据合法
//                    if ($type == 1) {
//                        //处理seo图片上传1
//                        $web_logo = 'web_logo';
//                        $web = self::findOne($web_logo);
//                        $web->value = $para['value'];
//                        $web->save();
//                    } else if ($type == 3) {
//                        //处理水印图片3
//                        $water_pic = 'water_pic';
//                        $web = self::findOne($water_pic);
//                        $web->value = $para['value'];
//                        $web->save();
//                    }
                    foreach ($keyA as $k) {
                        $new = self::findOne($k);
                        $new->value = $para[$k];
                        $new->save();
                    }
                    return 0;
                } else {
                    return 1;
                }
            } else {
                return 2;
            }
        }
    }

    /*
     * 获取数组的key
     * reurn the keys of array
     *  */

    public static function getKeyArray($para)
    {
        if (is_array($para)) {
            foreach ($para as $k => $v) {
                if ($k == '_csrf' || $k == 'value')
                    continue;
                $reA[] = $k;
            }
            return $reA;
        }else {
            return false;
        }
    }

    /*
     * 获取开关菜单
     */

    public static function getWatermarkOffHtml($mix_data = false)
    {
        $arr_data = [
            '0' => '关闭',
            '1' => '开启',
        ];
        if ($mix_data !== false) {
            return $arr_data[$mix_data];
        } else {
            return $arr_data;
        }
    }

    /*
     * 获取水印类型
     */

    public static function getWatermarkTypeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '文字水印',
            '2' => '图片水印',
        ];
        if ($mix_data !== false) {
            return $arr_data[$mix_data];
        } else {
            return $arr_data;
        }
    }

    /*
     * 获取位置
     */

    public static function getWatermarkPositionHtml($mix_data = false)
    {
        $arr_data = [
            '0' => '随机位置',
            '1' => '顶部居左',
            '2' => '顶部居中',
            '3' => '顶部居右',
            '4' => '中部居左',
            '5' => '中部局中',
            '6' => '中部居右',
            '7' => '底部居左',
            '8' => '底部居中',
            '9' => '底部居右',
        ];
        if ($mix_data !== false) {
            return $arr_data[$mix_data];
        } else {
            return $arr_data;
        }
    }

    /*
     * 获取邮件类型
     */

    public static function getMailTypeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => 'SMTP 函数发送',
        ];
        if ($mix_data !== false) {
            return $arr_data[$mix_data];
        } else {
            return $arr_data;
        }
    }

    /*
     * 获取中奖通知类型
     */

    public static function getNoticeTypeHtml($mix_data = false)
    {
        $arr_data = [
            '0' => '不通知 ',
            '1' => '只推送通知 ',
            '2' => '只发短信通知 ',
            '3' => '短信和推送通知 ',
        ];
        if ($mix_data !== false) {
            return $arr_data[$mix_data];
        } else {
            return $arr_data;
        }
    }
    
    public static function getListForType($type)
    {
        $datas = \common\models\ConfigModel::find()->where(['type' => $type])->all();
        $result = [];
        foreach($datas as $data){
            $result[$data['varname']] = $data['value'];
        }
        return $result;
    }    
    
    public static function getWeixinContentForKey($key_string)
    {
        $model = \common\models\ConfigModel::findOne("weixin");
        $value = json_decode($model->value,true);
        foreach($value as $k => $v){
            foreach($v['data'] as $kk => $vv){
                if($vv['key'] == $key_string && $vv['type'] == "click"){
                    if($vv['content_title']){
                        $arr = [];
                        $arr['Title'] = $vv['content_title'];
                        $arr['Description'] = $vv['content'];
                        $arr['PicUrl'] = $vv['content_img'];
                        //$arr['Url'] = $vv['content_url'];
                        $arr['Url'] = "http://www.gubatoo.com/wap/public/show_weixin?key={$key_string}";
                        return $arr;
                    }else{
                        return $vv['content'];
                    }
                }
            }
        }
    }

    
    //取得数据  如果带了这个值 则是以  $key_param这个键来进行相关的排序的。意思是看这个键的值是什么来进行相关的排序。
    public static function getNewDatas($key_list,$key_param = "")
    {
        if(!is_array($key_list)){
            $k = json_decode($key_list,true);
            if($k){
                $key_list = $k;
            }
        }
        
        if (is_array($key_list)) {
            $datas = self::find()->where(['varname' => $key_list])->asArray()->all();
            $result = [];
            foreach ($datas as $data) {
                $result[$data['varname']] = static::getOneDataForKey($data['varname'], $key_param);
            }
            return $result;
        } else {
            $datas = static::getOneDataForKey($key_list,$key_param);
            return $datas;
        }           
    }
    
    //取得这相关的数据
    public static function getOneDataForKey($key_str,$key_param = "")
    {
        $data = ConfigModel::getOne($key_str);
        if(!$data){
            return [];
        }
        $data_arr = json_decode($data['value'],true);
        if(!is_array($data_arr)){
            return $data['value'];
        } 
        if(count($data_arr) == 1){
            return $data_arr['-1'];
        }
        $result = [];
        if(isset($data_arr[-1]['sort'])){
            foreach($data_arr as $key => $value){//对这个进行相关的处理
                $result[$value['sort']] = $value;                
            }
            ksort($result);
            $result = array_values($result); 
        }else{
            $result = array_values($data_arr); 
        }
        
        foreach($result as $k => $v){
            if($key_str == "share_config"){
                if(isset($v['link'])){
                    $result[$k]['link'] = $result[$k]['link'] . "?exten_code=" . \yii::$app->controller->user->exten_code;
                }
            }           
        }
        $result_1 = [];
        foreach($result as $key => $value){
            if(count($value) == 1){
                $dd = array_values($value); 
                $result[$key] = $dd[0];
                continue;
            }
            if($key_param && isset($value[$key_param])){
                $result_1[$value[$key_param]] = $value;
            }else if(isset($value['key'])){
                $result_1[$value['key']] = $value;
            }else{
                break;
            }            
        }
        return $result_1 ? $result_1 : $result;
    }

}
