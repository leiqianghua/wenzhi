<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_carousel".
 *
 * @property string $id
 * @property integer $type
 * @property string $title
 * @property string $img
 * @property string $url
 * @property integer $sorting
 * @property integer $add_time
 */
class ElseCarouselModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%else_carousel}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'title' => 'Title',
            'img' => 'Img',
            'url' => 'Url',
            'sorting' => 'Sorting',
            'add_time' => 'Add Time',
        ];
    }
    
    /**
     * 根据类型获取相关数据
     * @param type $type
     */
    public static function getDataForType($type)
    {
        $datas = self::find()->where(['type' => $type])->orderBy("sorting desc")->asArray()->all();
        return $datas;
    }
    
    //类型
    public  static function getTypeHtml($mixData = false)
    {
        $arrData = array(
            1 => 'wap首页',	
        );
        if($mixData !== false){
            return $arrData[$mixData];
        }else{
            return $arrData;
        }
    }
}
