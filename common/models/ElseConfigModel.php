<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%config}}".
 *
 * @property string $varname
 * @property string $info
 * @property string $vlaue
 */
class ElseConfigModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%else_config}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'varname' => 'Varname',
            'info' => 'Info',
            'value' => 'value',
        ];
    }
    //取若干的配置 根据key
    
    public static function getListForKey($key_list)
    {
        $datas = self::find()->where(['varname' => $key_list])->asArray()->all();
        $result = [];
        foreach($datas as $data){
            $result[$data['varname']] = $data['value'];
        }
        return $result;
    }
}
