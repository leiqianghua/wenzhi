<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%else_error_log}}".
 *
 * @property integer $id
 * @property string $type
 * @property integer $uid
 * @property string $url
 * @property string $module
 * @property string $controller
 * @property string $action
 * @property string $gets
 * @property string $post
 * @property string $logs
 * @property string $referer
 * @property string $ip
 * @property integer $add_time
 */
class ElseErrorLogModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%else_error_log}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'uid' => 'Uid',
            'url' => 'Url',
            'module' => 'Module',
            'controller' => 'Controller',
            'action' => 'Action',
            'gets' => 'Gets',
            'post' => 'Post',
            'logs' => 'Logs',
            'referer' => 'Referer',
            'ip' => 'Ip',
            'add_time' => 'Add Time',
        ];
    }
}
