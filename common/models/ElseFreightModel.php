<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%else_freight}}".
 *
 * @property string $id
 * @property integer $city_id
 * @property integer $province_id
 * @property string $name
 * @property integer $first_weight
 * @property integer $first_cose
 * @property integer $next_weight
 * @property integer $next_cose
 * @property integer $add_time
 */
class ElseFreightModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%else_freight}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => '城市ID',
            'province_id' => '省ID',
            'name' => '名称',
            'first_weight' => '首重',
            'first_cose' => '首重费用',
            'next_weight' => '每增加多少公斤',
            'next_cose' => '每增加多少公斤增加的费用',
            'add_time' => '添加时间',
            'type_id' => '类型',
            'admin_id' => '会员',
        ];
    }
    //通过一个ID来给出其具体的信息  给出selected的来
    //如果等于空则没有选，给出一个选省的
    //如果有的话看选的是哪一个 多一个全部
    public static function getDataForArea($province_id = 0,$city_id = 0)
    {
        require_once \yii::getAlias('@backend') . "/config/area.php";
        if($province_id == 0 && $city_id == 0){//找所有的省
            $html = "<select class='province_id area_select' name='province_id'>";
            $html .= "<option value='0'>全国</option>";
            
            foreach($area_array[0] as $data){
                $html .= "<option value='{$data[0]}'>{$data[1]}</option>";
            }
            $html .= "</select>";
        }else{//选择了省  与  市
            $html = "<select class='province_id area_select' name='province_id'>";
            $html .= "<option value='0'>全国</option>";
            
            foreach($area_array[0] as $data){
                if($province_id == $data[0]){
                    $html .= "<option selected value='{$data[0]}'>{$data[1]}</option>";
                }else{
                    $html .= "<option value='{$data[0]}'>{$data[1]}</option>";
                }
            }
            $html .= "</select>";  
            
            
            
            $html .= "<select class='area_select city' name='city_id'>";
            $html .= "<option value='0'>全省</option>";
            foreach($area_array[$province_id] as $data){
                if($city_id == $data[0]){
                    $html .= "<option selected value='{$data[0]}'>{$data[1]}</option>";
                }else{
                    $html .= "<option value='{$data[0]}'>{$data[1]}</option>";
                }
            }
            $html .= "</select>";              
        }
        
        return $html;
    }
    
    public static function addCity($province_id)
    {        
        require_once \yii::getAlias('@backend') . "/config/area.php";

        $html = "<select class='area_select city' name='city_id'>";
        $html .= "<option value='0'>全省</option>";
        foreach($area_array[$province_id] as $data){
            $html .= "<option value='{$data[0]}'>{$data[1]}</option>";
        }
        $html .= "</select>"; 
        return $html;
    }

}
