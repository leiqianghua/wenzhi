<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%else_freight_type}}".
 *
 * @property string $id
 * @property string $title
 * @property integer $status
 * @property string $add_time
 * @property string $admin_id
 */
class ElseFreightTypeModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%else_freight_type}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'status' => '状态',
            'add_time' => '添加时间',
            'admin_id' => '会员ID',
        ];
    }
    
    //类型
    public  static function getStatusHtml($mixData = false)
    {
        $arrData = array(
            1 => '上架',	
            0 => '下架',	
        );
        if($mixData !== false){
            return $arrData[$mixData];
        }else{
            return $arrData;
        }
    }    
}
