<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%else_kaidan}}".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $order_ids
 * @property string $content
 * @property integer $price
 * @property integer $zhe_price
 * @property integer $add_time
 */
class ElseKaidanModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%else_kaidan}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户ID',
            'order_ids' => '订单列表',
            'content' => '内容',
            'price' => '原价',
            'zhe_price' => '折扣价',
            'add_time' => '添加时间',
            'status' => '状态',
            'title' => '标题',
            'type' => '开单类型',
            'extract' => '总支付提成',
            'step' => '需要步骤',
            'step_pay_num' => '步骤支付百分比',
            'user_upload_img' => '',
            'step_has_ok' => '用户完成步骤',
            'cost_price' => '成本价',
            'customer_iphone' => '客户联系方式',
            'customer_address' => '详情地址',
            'customer_name' => '客户姓名',
        ];
    }
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '未完成',
                '1' => '已完成',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }    
    
    //显示的HTML
    public static  function getTypeHtml($mix_data = false)
    {
        //$condition['']
        $datas = Project_classModel::getBaseKeyValue("id", "title", ['status' => 1,'parent_id' => 0]);
        $datas[0] = "商品列单";
        $arr_data = $datas;
        if($mix_data !== false){
                return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : "未知分类";
        }else{
                return $arr_data;
        }
    }      
    //显示的HTML
    public static  function getlast_setp_is_confirmHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '最后提交未确认',
                '1' => '最后提交已确认',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }  
    
    public function jiesuan()
    {
        $model = $this;
        //没有处理过  且用户最后一步完成了  且有成本价了   且最后的一步也已经确定了 
        if(!$model['status'] && $model->last_setp_is_confirm && $model->cost_price && $model->step_has_ok == $model->step){//有成本价   步骤已完成
            $money = $model['zhe_price'] - $model['cost_price'];//卖的钱  减去成本钱
            $libi = \common\models\MemberModel::getOne($model['uid'],"kaidang_bili");//这个人开的单
            if($libi){
                $money = (int)($money/100*$libi);
                if($money){
                    \common\models\MemberMoneyRecordModel::changeMoney(1, $money, "kaidang", $model['id'],$model['uid']);
                    //通知 这个用户你是已经得到了这个佣金了
                    $product_title = $model['title'] . "-分成";
                    $price = $money;
                    $date = date("Y-m-d H:i:s",time());
                    $remark = "古笆兔祝你生活愉快";
                    $url = "";
                    $is_has_parant = false;
                    $is_has_system = true;
                    $is_mine = false;
                    $other_user = [$model['uid']];
                    \common\models\MemberModel::buyOkNotice("开单分成成功提醒", $product_title, $price, $date, $remark, $url, $is_has_parant, $is_has_system, $is_mine,$other_user);                    
                }
            }
            $model->status = 1; 
            if($model['goods_id']){
                $sale_num = new \yii\db\Expression("sale_num + 1");
                Project_goods_packageModel::updateAll(['sale_num' => $sale_num], ['id' => $model['goods_id']]);
            }
        }  
        $model->save(false);
        return true;
    }
    
    public function beforeSave($insert) {
        $parentf = parent::beforeSave($insert);
        
        if(!$this->type && $_POST){
            $this->cost_price = $this->zhe_price;
        }
        
        return $parentf;
    }
    
    /**
     * 返回具体的一个状态数字
     * 1 用户完成了支付步骤  且 最后一步也已经确认支付成功
     * 2 用户完成了支付步骤  且 最后一步没有确认。
     * 
     * 3:等用户再次的来支付
     * 
     * 4:等着管理员确认用户的支付
     */
    public function isStatus()
    {
        $status = 0;
        $model = $this;
        if($model['step_has_ok'] == $model['step'] && $model['last_setp_is_confirm']){//用户已经完成了支付步骤 且 最后一个已经确认
            if($model['last_setp_is_confirm']){
                $status = 1;
            }else{
                $status = 2;
            }
        }else if($model['last_setp_is_confirm'] || !$model['step_has_ok']){//
            $status = 3;
        }else{//
            $status = 4;
        }
        return $status;
    }
    
    //某个步骤需要支付多少钱  如果0则是最后一次用户支付的步骤
    public function mastPayMoney($step = 0,$is_get_bili = false)
    {
        if(!$step){
            $step = $this->step_has_ok;
        }
        $arr = explode(',', $this->step_pay_num); 
        if($is_get_bili){
            return $arr[$step-1];
        }
        return $arr[$step-1]/100*$this->zhe_price;
    } 
    
    
    public function payAfterNotice()
    {
        $model = $this;
        //付款成功后通知相关的用户
        //谁开单的  跟  系统管理员
        $bili = $model->mastPayMoney(0, true);
        $product_title = $model['title'] . "-支付百分{$bili}";
        $price = $model->mastPayMoney();
        $date = date("Y-m-d H:i:s",time());
        $remark = "古笆兔祝你生活愉快";
        $url = "http://www.gubatoo.com/wap/notice/kaidang?id={$model['id']}";
        $is_has_parant = false;
        $is_has_system = true;
        $is_mine = false;
        $other_user = [$model['uid']];
        \common\models\MemberModel::buyOkNotice("开单支付提醒", $product_title, $price, $date, $remark, $url, $is_has_parant, $is_has_system, $is_mine,$other_user);
                
    }
    
    //审核通过后通知
    public function shenAfterNotice()
    {
        //付款成功后通知相关的用户
        //谁开单的  跟  系统管理员
        $bili = $this->mastPayMoney(0, true);
        $product_title = $this->title . "-支付百分{$bili}";
        $price = $this->mastPayMoney();
        $date = date("Y-m-d H:i:s",time());
        $remark = "古笆兔祝你生活愉快";
        $url = "http://www.gubatoo.com/wap/else_kaidan/show?id={$this->id}";
        $is_has_parant = false;
        $is_has_system = false;
        $is_mine = false;
        $other_user = [$this->uid];
        \common\models\MemberModel::buyOkNotice("开单支付审核通过通知", $product_title, $price, $date, $remark, $url, $is_has_parant, $is_has_system, $is_mine,$other_user);        
    }
    
    function getUser(){
    	return $this->hasOne(MemberModel::className(), ['uid'=>'uid']);
    } 
    

    //对于我们搜索的时候先把这个我们要一起连接的先搜索出来。表
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->with("user");
        return $dataProvider;
    }    
}
