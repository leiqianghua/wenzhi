<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yii_else_navigation".
 *
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property string $url
 * @property integer $new_open
 * @property integer $sorting
 * @property string $add_time
 */
class ElseNavigationModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_else_navigation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'new_open', 'sorting', 'add_time'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'title' => 'Title',
            'url' => 'Url',
            'new_open' => 'New Open',
            'sorting' => 'Sorting',
            'add_time' => 'Add Time',
        ];
    }
}
