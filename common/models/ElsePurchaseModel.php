<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%else_purchase}}".
 *
 * @property integer $id
 * @property integer $goods_id
 * @property string $goods_name
 * @property string $cangou_time
 * @property integer $add_time
 * @property integer $canku_id
 * @property integer $num
 * @property integer $total_money
 */
class ElsePurchaseModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%else_purchase}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_id', 'add_time', 'canku_id', 'num', 'total_money'], 'integer'],
            [['cangou_time'], 'safe'],
            [['goods_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_id' => '商品ID',
            'goods_name' => '商品名称',
            'cangou_time' => '采购时间',
            'add_time' => '添加时间',
            'canku_id' => '仓库ID',
            'num' => '采购数量',
            'total_money' => '采购总价',
        ];
    }
    
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $query = $dataProvider->query;

        
        if (!empty($_GET['start_add_time'])) {
            $query->andWhere("cangou_time>='{$_GET['start_add_time']}'");
        }
        if (!empty($_GET['end_add_time'])) {
            $query->andWhere("cangou_time<'{$_GET['end_add_time']}'");
        }        
        return $dataProvider;
    }

    public function beforeSave($insert) {
        $state = parent::beforeSave($insert);
        
        $this->goods_name = GoodsModel::getOne($this->goods_id, "name");
        

        
        return $state;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert){
            $goods_model = GoodsModel::findOne($this->goods_id);
            if($goods_model){
                $num = new \yii\db\Expression("num+{$this->num}");
                $purchase_num = new \yii\db\Expression("purchase_num+{$this->num}");
                $goods_model->num = $num;  
                $goods_model->purchase_num = $num;  
                $goods_model->save(false);
            }
        }
    }
}
