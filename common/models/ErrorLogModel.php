<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%error_log}}".
 *
 * @property integer $id
 * @property string $url
 * @property string $post
 * @property string $remark
 * @property string $type
 * @property integer $add_time
 */
class ErrorLogModel extends \common\components\BaseModel
{
    
    static $table_num = '';//默认情况下是要哪一张表里面的数据
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        if(!static::$table_num){
            return 'yii_error_log';
        }else{
            $num = static::$table_num;
            return "yii_error_log_{$num}";
        }
    }    
    



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'post' => 'Post',
            'remark' => 'Remark',
            'type' => 'Type',
            'add_time' => 'Add Time',
        ];
    }
    
    public static function addData($type,$remark = '')
    {
        $model = new static;
        $data['url'] = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $data['post'] = json_encode($_POST);
        $data['remark'] = $remark;
        $data['type'] = $type;
        $data['add_date'] = date('Y-m-d H:i:s');
        $model->attributes = $data;
        $model->save(false);
    }
}
