<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%express}}".
 *
 * @property integer $id
 * @property string $e_name
 * @property string $e_state
 * @property string $e_code
 * @property string $e_letter
 * @property string $e_order
 * @property string $e_url
 */
class ExpressModel extends \common\components\baseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%express}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_state', 'e_order'], 'string'],
            [['e_name', 'e_code'], 'string', 'max' => 50],
            [['e_letter'], 'string', 'max' => 1],
            [['e_url'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'e_name' => 'E Name',
            'e_state' => 'E State',
            'e_code' => 'E Code',
            'e_letter' => 'E Letter',
            'e_order' => 'E Order',
            'e_url' => 'E Url',
        ];
    }
    
    
    public static  function showwuliu($e_code,$shipping_code)
    {
        //$e_code = $_GET['e_code'];
        //$shipping_code = $_GET['shipping_code'];
        $cacheKey = "getExpressDetail-{$e_code}{$shipping_code}";
        $expressDetail = yii::$app->cache->get($cacheKey);
        if (!empty($expressDetail)){
            return $expressDetail;
        }
        
        $cUrl = new \common\components\Curl();
        $url = 'https://poll.kuaidi100.com/poll/query.do';
        
//        $e_code = "jiayunmeiwuliu";
//        $shipping_code = "1117701807";
        $arr = [
            'com' => $e_code,
            'num' => $shipping_code,
        ];
        $customer = "105C65A3AFF13C564A08ED29EE9BB155";
        $param_d = json_encode($arr);
        $kk = "byNqVleu2794";
        $param = [
            'customer' => $customer,
            'param' => $param_d,
        ];
        
        $param['sign'] = strtoupper(md5($param_d . $kk . $customer));
        //echo $param['sign'];exit;
        $datas = $cUrl->post($url, $param);
        
        
        print_r($datas);exit;
        
        $dd =  isset($datas['data']) ? $datas['data'] : false;
        if($dd){
            yii::$app->cache->set($cacheKey,$dd,3600*4);
            return $dd;            
        }else{
            return false;
        }
        
        
//        $url.=http_build_query($param);
//        $contentByUrl = $cUrl->get($url, $_SERVER['HTTP_USER_AGENT'], '', 'http://www.kuaidi100.com/');
//        $content = json_decode($contentByUrl, true);
//        if ($content['status'] !== '200' || empty($content)) {
//            \common\models\ExpressModel::$error = $content['message'];
//            return false;
//            //$this->ajaxReturn(['status' => 0, 'msg' => $content['message']]);
//        }
//        $output = $content;
//        if (is_array($content['data']) && !empty($content['data'])) {
//            foreach ($content['data'] as $k => $v) {
//                if (!empty($v['time'])) {
//                    $output .= ' <li>' . $v['time'] . '&nbsp;&nbsp;' . $v['context'] . '</li>';
//                }
//            }
//        }
        

        //$this->ajaxReturn(['status' => 1, 'content' => $output]);        
    }    
    
    
    
    
    
    public static  function showwuliu_bak($e_code,$shipping_code)
    {
        //$e_code = $_GET['e_code'];
        //$shipping_code = $_GET['shipping_code'];
        $cacheKey = "getExpressDetail-{$e_code}{$shipping_code}";
        $expressDetail = yii::$app->cache->get($cacheKey);
        if (!empty($expressDetail)){
            return $expressDetail;
        }
        
        $cUrl = new \common\components\Curl();
        $url = 'http://www.kuaidi100.com/query?';
        $param = [
            'type' => $e_code,
            'postid' => $shipping_code,
            'id' => 1,
            'valicode' => '',
            'temp' => rand(1,9999),
        ];
        $url.=http_build_query($param);
        $contentByUrl = $cUrl->get($url, $_SERVER['HTTP_USER_AGENT'], '', 'http://www.kuaidi100.com/');
        $content = json_decode($contentByUrl, true);
        if ($content['status'] !== '200' || empty($content)) {
            \common\models\ExpressModel::$error = $content['message'];
            return false;
            //$this->ajaxReturn(['status' => 0, 'msg' => $content['message']]);
        }
        $output = $content;
//        if (is_array($content['data']) && !empty($content['data'])) {
//            foreach ($content['data'] as $k => $v) {
//                if (!empty($v['time'])) {
//                    $output .= ' <li>' . $v['time'] . '&nbsp;&nbsp;' . $v['context'] . '</li>';
//                }
//            }
//        }
        
        yii::$app->cache->set($cacheKey,$output,600);
        return $output;
        //$this->ajaxReturn(['status' => 1, 'content' => $output]);        
    }
    
//    0：在途，即货物处于运输过程中；
//    1：揽件，货物已由快递公司揽收并且产生了第一条跟踪信息；
//    2：疑难，货物寄送过程出了问题；
//    3：签收，收件人已签收；
//    4：退签，即货物由于用户拒签、超区等原因退回，而且发件人已经签收；
//    5：派件，即快递正在进行同城派件；
//    6：退回，货物正处于退回发件人的途中；
//    该状态还在不断完善中，若您有更多的参数需求，欢迎发邮件至
    public static  function getStateHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '在途',
                '1' => '揽件',
                '2' => '疑难',
                '3' => '已签收',
                '4' => '退签',
                '5' => '正在派件',
                '6' => '已退回',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }
}
