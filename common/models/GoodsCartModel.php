<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_cart}}".
 *
 * @property integer $id
 * @property string $uid
 * @property integer $type
 * @property string $keys_id
 * @property integer $num
 * @property string $money
 */
class GoodsCartModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_cart}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'type' => 'Type',
            'goods_id' => 'Keys ID',
            'num' => 'Num',
            'money' => 'Money',
            'goods_name' => 'goods_name',
        ];
    }
    
    //验证过后加入购物车，商品  具体的商品信息   如果是套件商品，请$goods_info中加入goods_list 的json数据过来
    //加入成功则返回true
    //加入失败则返回false
    //$goods_info
    //没有登录的话 则 $_COOKIE['cart_goods_num'] 加入到这个cookie里面去
    //如果有登录的话
    public function addCart($goods_info,$quantity,$other = '')
    {
        if (yii::$app->controller->user->isLogin()) {//有登录
            static::addOrUpdate($goods_info,$quantity,'',$other);
            return $this->getNum();
        } else {
            $cart_goods_num = empty($_COOKIE['cart_goods_num']) ? '' : $_COOKIE['cart_goods_num'];
            $cart_goods_num = json_decode($cart_goods_num, true);
            if(!$cart_goods_num){
                $cart_goods_num = [];
            }
            
            
            if(!empty($goods_info['shop_list'])){//这肯定是从套装那边直接过来的
                foreach($goods_info['shop_list'] as $shop_detail){
                    $goods_id = $shop_detail['goods_id'];
                    $quantity = $shop_detail['quantity'];
                    if(isset($cart_goods_num['goods_id'])){
                        $cart_goods_num['goods_id'] = $cart_goods_num['goods_id'] + $quantity;
                    }else{
                        $cart_goods_num['goods_id'] = $quantity;
                    }
                }
                if($other){
                    foreach($other as $val){
                        $goods_id = $val;
                        $quantity = 1;
                        if(isset($cart_goods_num['goods_id'])){
                            $cart_goods_num['goods_id'] = $cart_goods_num['goods_id'] + $quantity;
                        }else{
                            $cart_goods_num['goods_id'] = $quantity;
                        }                    
                    }
                }
            }else{//购买的是单个的商品
                if(isset($cart_goods_num[$goods_info['id']])){
                    $cart_goods_num[$goods_info['id']] = $cart_goods_num[$goods_info['id']] + $quantity;
                }else{
                    $cart_goods_num[$goods_info['id']] = $quantity;
                }            
            }            
            
            $count = count($cart_goods_num);
            $cart_goods_num = json_encode($cart_goods_num);
            setcookie('cart_goods_num', $cart_goods_num, time()+386400, '/');  
            return $count;
        }
    }
    //登录回调后的购物车
    public static function afterLoginCallback($member_info)
    {
        $cart_goods_num = empty($_COOKIE['cart_goods_num']) ? '' : $_COOKIE['cart_goods_num'];
        $cart_goods_num = json_decode($cart_goods_num, true);
        if(!$cart_goods_num){
            return 0;
        }else{
            foreach($cart_goods_num as $goods_id => $num){
                $goods_info = GoodsModel::findOne($goods_id);
                self::addOrUpdate($goods_info, $num,$member_info['uid']);   
            }
        }
        setcookie('cart_goods_num', '', 1, '/');          
    }
    
    
    
    protected static function _addOrUpdate($goods_id,$quantity,$uid)
    {
        $cart_model = static::find()->where(['goods_id' => $goods_id,'uid' => $uid])->limit(1)->one();
        $goods_model = GoodsModel::findOne($goods_id);
        if(!$goods_model){//找不到这个商品
            return false;
        }
        $max_quantity = $goods_model['num'];
        if(!$max_quantity){//已经没有库存了
            return false;
        }
        
        
        if($cart_model){//购物车里面还有没有这个商品
            $quantity = $quantity + $cart_model['num'];
            if($quantity > $max_quantity){
                $quantity = $max_quantity;
            }
            $cart_model->num = $quantity;
            $cart_model->save(false);
        }else{//购物车里面本来就有这个商品
            $cart_model = new static();
            if($quantity > $max_quantity){
                $quantity = $max_quantity;
            }
            $cart_model->uid = $uid;
            $cart_model->goods_id = $goods_id;
            $cart_model->num = $quantity;
            $cart_model->money = $goods_model['price'];
            $cart_model->goods_name = $goods_model['name'];
            $cart_model->type = $goods_model['type'];//1 正常商品   2附加商品
            $cart_model->admin_id = $goods_model['admin_id'];
            $cart_model->save(false);
        }
        return true;
    }

        //有可能是内部商城的，有可能是其它的$goods_info
    protected static function addOrUpdate($goods_info,$quantity,$uid = '',$other='')
    {
        if(!is_array($other)){
            $other = json_decode($other,true);
        }
        
        $uid === '' ?  $uid =  yii::$app->controller->user->isLogin() : $uid;
        if(!empty($goods_info['shop_list'])){//这肯定是从套装那边直接过来的
            foreach($goods_info['shop_list'] as $shop_detail){
                $goods_id = $shop_detail['goods_id'];
                $quantity = $shop_detail['quantity'];
                static::_addOrUpdate($goods_id, $quantity, $uid);
            }
            if($other){
                foreach($other as $val){
                    $goods_id = $val;
                    $quantity = 1;
                    static::_addOrUpdate($goods_id, $quantity, $uid);                    
                }
            }
        }else{//购买的是单个的商品
            static::_addOrUpdate($goods_info['id'], $quantity, $uid);  
        }
        
        
        return true;
        
//        
//        $model = self::find()->where(['type' => 1,'uid' => $uid,'goods_id' => $goods_info['id']])->limit(1)->one();
//        if(!$model || !empty($goods_info['shop_list'])){//没有存储
//            $model = new self();
//            $model['uid'] = $uid;
//            $model['goods_id'] = $goods_info['id'];
//            $model['money'] = $goods_info['price'];
//            $model['goods_name'] = $goods_info['name'];
//            $model['num'] = $quantity;
//            $model['other_data'] = $other;
//            if(isset($goods_info['shop_list'])){//内部商城的
//                $model['type'] = 2;
//                $model['shop_list'] = $goods_info['shop_list'];
//            }
//        }else{          
//            $model['num'] = $model['num'] + $quantity;
//            if($model['num'] > $goods_info['num']){
//                $model['num'] =  $goods_info['num'];
//            }
//            $model['money'] = $goods_info['price'];
//            $model['goods_name'] = $goods_info['name'];
//            $model['other_data'] = $other;
//        }
//        return $model->save();
    }
    //查看购物车的数量
    //返回具体的数量
    public static function getNum()
    {
        if ($uid = yii::$app->controller->user->isLogin()) {//有登录
            $count = self::find()->where(['uid' => $uid])->count();
            return $count;
        } else {
            
            $cart_goods_num = empty($_COOKIE['cart_goods_num']) ? '' : $_COOKIE['cart_goods_num'];
            $cart_goods_num = json_decode($cart_goods_num, true);
            if(!$cart_goods_num){
                return 0;
            }else{
                return count($cart_goods_num);
            }      
        }        
    }
    //购物车总价格
    public static function getCountMoney()
    {
        $uid = yii::$app->controller->user->isLogin();
        return static::find()->where(['uid' => $uid])->sum('money');
    }
    
    
    /**
     * 得到所购买的id和数量
     * 从购物车提交过来的数据
     * 商品ID   对应  数量
     */
    public static function parseItems($cart_id)
    {
        //存放所购商品ID和数量组成的键值对
        $buy_items = array();
        if (is_array($cart_id)) {
            foreach ($cart_id as $value) {
                if (preg_match_all('/^(\d{1,10})\|(\d{1,6})$/', $value, $match)) {
                    $buy_items[$match[1][0]] = $match[2][0];
                }
            }
        }
        return $buy_items;
    }    
    
    
    public static function buy($cart_id, $ifcart, $invalid_cart = [])
    {
        $goods_list = [];
        //删除无效的购物车的数据
        if ($ifcart) {
            $uid = yii::$app->user->isLogin();
            if (is_array($invalid_cart)) {
                self::deleteAll(['uid' => $uid,'id' => $invalid_cart]);
            }else{
                if(is_numeric($invalid_cart)){
                    self::deleteAll(['uid' => $uid,'id' => $invalid_cart]);
                }else{
                    self::deleteAll(['uid' => $uid,'id' => ['in',$invalid_cart]]);
                }
            }
        } 
        
        //获取我们所需要的商品
        if($ifcart){//从购物车过来的数据
            $cartlist = static::parseItems($cart_id);//ID列表加上  购买的数量    cart_id   buy_num
            $goods_ids = array_keys($cartlist);//商品ID
            //$datas = GoodsCartModel::find()->where(['id' => $cart_id_list])->asArray()->indexBy('goods_id')->all();       
            //$goods_ids = array_keys($datas);//购物车的ID列表        
            
            $goods_list = GoodsModel::find()->where(['id' => $goods_ids])->asArray()->all();
            
            foreach($goods_list as $key => $value){
                $buy_num = $cartlist[$value['id']];
                $goods_list[$key]['buy_num'] = $buy_num;
            }
            
        }else{//直接点击购买  变量数组的形式统一来购买
//            if(isset($cart_id[0])){
//                $cart_id = $cart_id[0];
//            }
            if(!is_array($cart_id)){
                $goods_more = explode(',', $cart_id);
            }else{
                $goods_more = $cart_id;
            }
            
            $goods_list = [];
            foreach($goods_more as $key => $value){
                $goods = explode('|', $value);
                $goods_id = $goods[0];
                $buy_num = $goods[1];
                $goods_detail = GoodsModel::findOne($goods_id);
                $goods_detail = $goods_detail->attributes;
                $goods_detail['buy_num'] = $buy_num;
                $goods_list[] = $goods_detail;             
            }
            
            
        }
        //计算总金额与折扣的金额
        return $goods_list;
    }
 
}
