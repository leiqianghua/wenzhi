<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_label_data}}".
 *
 * @property string $id
 * @property string $label_id
 * @property string $goods_id
 * @property string $add_time
 */
class GoodsLabelDataModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_label_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label_id', 'goods_id', 'add_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label_id' => 'Label ID',
            'goods_id' => 'Goods ID',
            'add_time' => 'Add Time',
        ];
    }
}
