<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_label_data_package}}".
 *
 * @property string $id
 * @property string $label_id
 * @property string $goods_id
 * @property string $add_time
 */
class GoodsLabelDataPackageModel extends \common\components\baseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_label_data_package}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label_id' => 'Label ID',
            'goods_id' => 'Goods ID',
            'add_time' => 'Add Time',
        ];
    }
}
