<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_class}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $status
 * @property integer $sorting
 * @property string $add_time
 * @property integer $label
 * @property string $title
 */
class GoodsLabelModel extends \common\models\BaseCategory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_label}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'sorting', 'add_time', 'label'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => '父ID',
            'status' => '状态',
            'sorting' => '排序',
            'add_time' => '添加时间',
            'label' => '层级',
            'title' => '名称',
        ];
    }
    
    /*
     * 多标签的获取条件的字段  可以获取到  gorupby having orderby in等条件
     * $condition  拿到的条件
     * $field 标签的条件  哪些个的标签的字段
     * $count_field 通过标签取的具体的哪个字段  比如通过label_id这个字段  取 goods_id这个字段   这个的值就是goods_id
     * $value 标签的值   /string|array  标签的具体的值。
     */
    public static function getLabelCondition(&$condition,$field,$count_field,$value)
    {
        if(is_array($value)){
            $num = count($value);
            $value = implode(',', $value);
        }else{
            $arr_value = explode(',', $value);
            $num = count($arr_value);
        }
        
        $condition[] = "{$field} in ({$value})";
        $condition['groupBy'] = $count_field;
        $condition['having'] = "count({$count_field}) >= {$num}";
        $condition['orderBy'] = "{$count_field} desc";        
    }
}
