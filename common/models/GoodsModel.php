<?php

namespace common\models;

use Yii;
use Exception;
/**
 * This is the model class for table "{{%goods}}".
 *
 * @property string $id
 * @property string $name
 * @property string $price
 * @property string $discount_1
 * @property string $discount_2
 * @property string $discount_3
 * @property string $add_time
 * @property integer $sorting
 * @property string $cid
 * @property string $img
 * @property string $img_list
 * @property string $details
 * @property integer $status
 */
class GoodsModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '商品名称',
            'price' => '价格',
            'discount_0' => '出厂价',
            'discount_1' => '事业合伙人',
            'discount_2' => '品牌合伙人',
            'discount_3' => '销售合伙人',
            'discount_4' => '普通会员',
            'add_time' => '添加时间',
            'weight' => '重量',
            'sorting' => '排序',
            'cid' => '分类ID',
            'img' => '主图',
            'img_list' => '图片',
            'details' => '详情',
            'status' => '状态',
            'num' => '库存',
            'type' => '商品类型',
        ];
    }
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '正常',
                '0' => '下架',
                '2' => '等待审核',
                '3' => '拒绝',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }
    //显示的HTML
    public static  function getTypeHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '正常商品',
                '2' => '附加商品',
                '3' => '不在列表展示只在套餐选择商品',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }
    //如果score为空 则是普通商品  如果为1则是积分要大于0的商品  其它则是 要积分大于0且价格为0的商品
    public static function baseGetDatas($condition = array(), $is_cache = false, $cache_time = 600, $is_getsql = false)
    {
        if(!isset($condition['type'])){
            $condition['type'] = 1;
        }else{
            if(empty($condition['type'])){
                unset($condition['type']);
            }
        }
        $is_score = \common\components\GuestInfo::getParam("is_score", 0);
        $condition['is_score'] = isset($condition['is_score']) ? $condition['is_score'] : $is_score;
        if(empty($condition['is_score'])){
            $condition['score'] = 0;
        }else if($condition['is_score'] == 1){
            $condition[] = "score > 0";
        }else{
            $condition[] = "score > 0 and price = 0";
        }      
        $condition['status'] = 1;
        return parent::baseGetDatas($condition, $is_cache, $cache_time, $is_getsql);
    }


    public static function getOrderd($query,$orderd)
    {
        $order = '';
        switch ($orderd) {
            case 1:
                $order = 'add_time  desc'; //时间最新
                break;
            
            case 2:
                $order = 'add_time ASC';//时间最早
                break;
            
            case 3:
                $order = 'price asc';//价格从小到大
                break;
            
            case 4:
                $order = 'price DESC';//价格从大到小
                break;   
            case 5:
                $order = 'sorting asc,sale_num DESC';//综合排序
                break;                                                                                            
            default:
                $order = 'sorting asc,sale_num desc';//默认排序
                break;  
        }
        $query->orderBy($order);
    }
    
    public static function mine_attributesStr($spec,$class = '')
    {
        if($spec){
            if(is_string($spec)){
                $spec = json_decode($spec,true);
            }
            $html = '<p class="'.$class.'">';
            foreach($spec as $key => $value){
                $html .= "<span>{$value['key']}：{$value['value']}</span>";
            }
            $html .= '</p>';
            return $html;
        }         
    }
    
    
    //根据其POST的数据获取我们想要的数据
    public static function getDataForPost()
    {
        $_POST = array_merge($_POST,$_GET);
        empty($_POST['ifcart']) ? $_POST['ifcart'] = 0 : '';//是否来源购物车
        empty($_POST['invalid_cart']) ? $_POST['invalid_cart'] = [] : '';//有无无效商品
        empty($_POST['goods_id']) ? $_POST['goods_id'] = 0 : '';//商品ID
        if(empty($_POST['goods_id'])){
            if(isset($_POST['id'])){
                $_POST['goods_id'] = $_POST['id'];
            }
        }
        empty($_POST['num']) ? $_POST['num'] = 0 : '';//购物数量
        empty($_POST['shop_list']) ? $_POST['shop_list'] = '' : '';//购物的东西 多渠道
        empty($_POST['cart_list']) ? $_POST['cart_list'] = '' : '';//直接来源购物车
        empty($_POST['type']) ? $_POST['type'] = '1' : '';//直接来源购物车
        
        if(is_string($_POST['invalid_cart'])){
            $_POST['invalid_cart'] = json_decode($_POST['invalid_cart'],true);
        }        
        if(is_string($_POST['cart_list'])){
            $_POST['cart_list'] = json_decode($_POST['cart_list'],true);
        }
        
        
        $datas = [];
        
        if($_POST['ifcart']){//来源于购物车，找到所有的商品列表
            if(!empty($_POST['invalid_cart'])){
                \common\models\GoodsCartModel::deleteAll(['id' => $_POST['invalid_cart']]);
            }
                        
            $putong = [];
            foreach($_POST['cart_list'] as $key => $value){
                $model = \common\models\GoodsCartModel::findOne($value);
                if(!$model){
                    continue;
                }
                $goods_model = GoodsModel::findOne($model['goods_id'])->attributes;
                $goods_model['buy_num'] = $model['num'];
                $datas[$model['admin_id']]['data'][] = $goods_model;
               
            }
        }else{//不是来源于购物车
            if($_POST['type'] == 2){//内部商城
                $shop_list = explode(',', $_POST['shop_list']);
                foreach($shop_list as $k => $v){
                    $vd = explode('|', $v);
                    if(count($vd) != 2){
                        continue;
                    }
                    
                    $goods_model = GoodsModel::findOne($vd[0])->attributes;
                    $goods_model['buy_num'] = $vd[1];
                    $datas[$goods_model['admin_id']]['data'][] = $goods_model;
                    
                    if($_POST['fujia']){
                        $fujia = explode(',', $_POST['fujia']);
                        foreach($fujia as $k => $v){
                            $goods_model = GoodsModel::findOne($v)->attributes;
                            $goods_model['buy_num'] = 1;
                            $datas[$goods_model['admin_id']]['data'][] = $goods_model;                            
                        }
                    }
                }
                
            }else{
                $lishi = GoodsModel::findOne($_POST['goods_id'])->attributes;
                $lishi['buy_num'] = $_POST['num'];
                $datas[$lishi['admin_id']]['data'][] = $lishi;
            }
        }
        
        
        

        if($_POST['ifcart']){
            $d = GoodsCartModel::find()->where(['id' => $_POST['cart_list']])->asArray()->limit(1)->one();
            if(!$d){
                return false;
            }
        } 
        
        return $datas;
    }
    
    /*
     * 级别是2以上的  统统不需要运费
     * 返回是一个数组    总的运费   加上各个供应商的运费  admin  =>  运费
     */
    public static function calcFreight($address_id,$datas = [],$uid = '')
    {
        if(empty($address_id)){
            return 0;
        }
        $uid === '' ? $uid = \yii::$app->user->uid : '';
        $member = MemberModel::findOne($uid);
        $label = $member['label'];
        if($label <= 2){//加盟商往上是不需要运费的  加盟商，战略加盟商价，出厂  不需要运费
            return 0;
        }
        if(empty($datas)){
            $datas = static::getDataForPost();
        }
        
        
        $address_model = MemberAddressModel::findOne($address_id);
        if(!$address_model){
            return 0;
        }
        $province_id = $address_model['province_id'];
        $city_id = $address_model['city_id'];
        //首先看有无精确到市的
        $data = \common\models\ElseFreightModel::find()->where(['city_id' => $city_id])->limit(1)->asArray()->one();
        if(!$data){
            $data = \common\models\ElseFreightModel::find()->where(['province_id' => $province_id])->limit(1)->asArray()->one();
        }
        if(!$data){
            $data = \common\models\ElseFreightModel::find()->where(['province_id' => 0])->limit(1)->asArray()->one();
        }
        //拿到了运费模板后  各个的计算
        $all_total = 0; //所有的加起来的总的运费
        $result = [];//各个供应商的运费
        foreach($datas as $admin_id => $values){
            //$data 为模板
            //算总共的重量
            $weight = 0;
            foreach($values['data'] as $v){
                $weight += $v['weight'] * $v['buy_num'];//这个供应商总共的重量
            }
            if($weight <= $data['first_weight']){
                $all_total += $data['first_cose'];
                $result[$admin_id] = $data['first_cose'];
                continue;
            }

            $has_weight = $weight - $data['first_weight'];
            if(empty($data['next_weight'])){
                $xun_weight = 0;
            }else{
                $xun_weight = ceil($has_weight/$data['next_weight']) * $data['next_cose'];
            }

            $total = $xun_weight + $data['first_cose'];                
            $all_total += $total;
            $result[$admin_id] = $total;
            continue;          
        }
        return [$all_total,$result];
    }
    /**
     * 获取到的数据是   总的原价   总的折扣价   总的积分    各个供应商的价格 原价  折扣价  积分  键值为  admin_id
     * @param type $datas
     * @param type $uid
     * @return type
     */
    public static function calcTotalMoney($datas = [],$uid = '')
    {
        $uid === '' ? $uid = \yii::$app->controller->user->uid : '';
        if(empty($datas)){
            $datas = $this->getDataForPost();
        }
        $member = MemberModel::findOne($uid);
        $label = $member['label'];
        
        $total_money = 0;//总的原价
        $total_money_zhe = 0;//总的折扣价
        $total_score = 0;//总的积分
        //这里到时候会扣除用户的多笔订单一起来结算的
        $result = [];
        
        foreach($datas as $admin_id => $data){
            $result[$admin_id]['total_money'] = $result[$admin_id]['total_score'] = $result[$admin_id]['total_money_zhe'] = 0;
            foreach($data['data'] as $d){
                $total_money += $d['price'] * $d['buy_num'];
                $total_score += $d['score'] * $d['buy_num'];
                $result[$admin_id]['total_money'] +=  $d['price'] * $d['buy_num']; 
                $result[$admin_id]['total_score'] +=  $d['score'] * $d['buy_num'];    
                if($label == 0){//出厂价
                    $total_money_zhe += $d['buy_num'] * $d['discount_0'];
                    $result[$admin_id]['total_money_zhe'] +=  $d['buy_num'] * $d['discount_0'];   
                }else if($label == 1){
                    $total_money_zhe += $d['buy_num'] * $d['discount_1'];
                    $result[$admin_id]['total_money_zhe'] +=  $d['buy_num'] * $d['discount_1'];   
                }else if($label == 2){
                    $total_money_zhe += $d['buy_num'] * $d['discount_2'];
                    $result[$admin_id]['total_money_zhe'] +=  $d['buy_num'] * $d['discount_2'];   
                }else if($label == 3){
                    $total_money_zhe += $d['buy_num'] * $d['discount_3'];
                    $result[$admin_id]['total_money_zhe'] +=  $d['buy_num'] * $d['discount_3'];   
                }else{
                    $total_money_zhe += $d['buy_num'] * $d['discount_4'];
                    $result[$admin_id]['total_money_zhe'] +=  $d['buy_num'] * $d['discount_4'];   
                }
            }     
        }
        //商品总金额  商品折扣金额  附加费金额
        return [$total_money,$total_money_zhe,$total_score,$result];
    }
    
    
    
    
    
    public static function buy2Verification($datas)
    {
        //print_r($datas);exit;
        $shop = [];
        foreach($datas as $data){
            foreach($data['data'] as $d){
                if($d['num'] < $d['buy_num']){
                    throw new Exception($d['name'] . '商品库存不足');
                }
            }
        }        
    }    
    
    public static function makePaySn($member_id)
    {
        return mt_rand(10, 99)
                . sprintf('%010d', time() - 946656000)
                . sprintf('%03d', (float) microtime() * 1000)
                . sprintf('%03d', (int) $member_id % 1000);
    }    
    
    //返回订单模型
    public static function creataOrder($address_id)
    {
        $post = $_POST;
        try {
            $cache_key = "creataOrder_" . \yii::$app->user->uid;
            $transaction = \yii::$app->db->beginTransaction();
            $state = \common\components\Tool::checkLock($cache_key);
            if(!$state){
                return false;
            }
            
            $datas = GoodsModel::getDataForPost();//获取来源的数据
            static::buy2Verification($datas);//验证  是不是有购买失败的东西，库存不足什么的
            
            
            $total_money = static::calcTotalMoney($datas);
            $freight = static::calcFreight($address_id, $datas);//运费
            $buy_money = $freight + $total_money[1];
            
            //然后购买，减库存，创建订单
            
            //验证通过后，开始创建订单,订单创建好了后要把相应的数量减少
            //创建订单，创建订单商品，减少库存
            $data = [];
            $data['order_sn'] = static::makePaySn(\yii::$app->user->uid);
            $data['uid'] = \yii::$app->user->uid;
            $data['username'] = \yii::$app->user->username;
            $data['goods_amount'] = $total_money[0];
            $data['goods_amount_discount'] = $total_money[1];
            $data['order_amount'] = $buy_money;
            $data['freight'] = $freight;
            $data['update_time'] = time();
            //这一笔订单的收货信息
            $address_arr = \common\models\MemberAddressModel::getOne($address_id);
            $data['address'] = $address_arr['address_info'] . $address_arr['address'];
            $data['phone'] = $address_arr['iphone'];
            $data['user_truename'] = $address_arr['username'];
            
            
            $order_model = new OrderModel();
            $order_model->attributes = $data;
            $order_model->save(false);
            
            
            //减少相应的库存，与增加相应的销量
            $shop = [];
            foreach($datas as $data){
                foreach($data['data'] as $d){
                    $shop[$d['id']]['num'] = $d['num'];
                    if(empty($shop[$d['id']]['buy_num'])){
                        $shop[$d['id']]['buy_num'] = $d['buy_num'];
                    }else{
                        $shop[$d['id']]['buy_num'] += $d['buy_num'];
                    }
                }
            }        
            foreach($shop as $shop_id => $data){
                $express = new \yii\db\Expression("num-{$data['buy_num']}");
                $express1 = new \yii\db\Expression("sale_num+{$data['buy_num']}");
                $state = static::updateAll(['num' => $express,'sale_num' => $express1], ['id' => $shop_id]);
                if(!$state){
                    throw new Exception('商品库存不足');
                }
            }            
            
            
            //创建相关的订单商品   再创建订单商品   再创建订单套件商品
            foreach($datas as $data){
                if($data['gid'] == -1){//这里的都是普通的商品
                    foreach($data['data'] as $k => $v){
                        $d = [];
                        $d['order_id'] = $order_model->order_id;
                        $d['goods_id'] = $v['id'];
                        $d['type'] = 1;
                        $d['name'] = $v['name'];
                        $d['img'] = $v['img'];
                        $d['uid'] = \yii::$app->user->uid;
                        $d['username'] = \yii::$app->user->username;
                        $d['money'] = $v['price']* $v['buy_num'];
                        $label = \yii::$app->user->label;
                        if($label == 0){//出厂价
                            $money_zhe = $v['buy_num'] * $v['discount_0'];
                        }else if($label == 1){
                            $money_zhe = $v['buy_num'] * $v['discount_1'];
                        }else if($label == 2){
                            $money_zhe = $v['buy_num'] * $v['discount_2'];
                        }else if($label == 3){
                            $money_zhe = $v['buy_num'] * $v['discount_3'];
                        }else{
                            $money_zhe = $v['buy_num'] * $v['buy_num'];
                        }                        
                        $d['discount_money'] = $money_zhe;
                        $d['num'] = $v['buy_num'];
                        $order_shop_model = new OrderGoodsModel();
                        $order_shop_model->attributes = $d;
                        $order_shop_model->save(false);
                    }
                }else{//这里的是内部商城
                    $model = GoodsPackageModel::findOne($data['gid']);
                    $d = [];
                    $d['order_id'] = $order_model->order_id;
                    $d['goods_id'] = $data['gid'];
                    $d['type'] = 2;
                    $d['name'] = $model['name'];
                    $d['uid'] = \yii::$app->user->uid;
                    $d['username'] = \yii::$app->user->username;
                    $d['money'] = 0;                  
                    $d['discount_money'] = 0;
                    $d['num'] = 1;   
                    $ddd = [];
                    foreach($data['data'] as $ll){
                        $lihsi = [];
                        $lihsi['goods_id'] = $ll['id'];
                        $lihsi['buy_num'] = $ll['buy_num'];
                        $ddd[] = $lihsi;
                    }
                    $d['shop_list'] = json_encode($ddd);
                    $order_shop_model = new OrderGoodsModel();
                    $order_shop_model->attributes = $d;
                    $order_shop_model->save(false);                    
                }
            }

            //删除购物车数据
            //删除购物车中的商品
            if ($_POST['ifcart']) {
                GoodsCartModel::deleteAll(['id' => $_POST['cart_list']]);
            }
            
            
            
            $transaction->commit();
            \common\components\Tool::deleteLock($cache_key);
            return $order_model;//订单模型
            
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            $transaction->rollBack();
            \common\components\Tool::deleteLock($cache_key);
            return false;
        }
    }       
    
    
    
    //新增或者是修改后的操作
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($_POST){
            try {
                $transaction = Yii::$app->db->beginTransaction();//开启事务   
                if(!empty($_POST['label'])){//把旧的删除掉，然后留下新的
                    GoodsLabelDataModel::deleteAll(['goods_id' => $this->id]);
                    $time = time();
                    foreach($_POST['label'] as $key => $value){
                        $model = new GoodsLabelDataModel();
                        $model->label_id = $value;
                        $model->goods_id = $this->id;
                        $model->save(false);
                    }
                }else{
                    GoodsLabelDataModel::deleteAll(['goods_id' => $this->id]);
                }       
                $transaction->commit();//提交事务                
            } catch (\Exception $exc) {
                $transaction->rollBack();//释放事务
            }

        }
        return true;
    }
    
//    public function afterDelete() {
//        parent::afterDelete();
//        GoodsLabelDataModel::deleteAll(['goods_id' => $this->id]);
//        return true;
//    }
//    
//    public static function afterDeleteAll($condition = '', $params = array()) {
//        if(!empty($condition['id'])){
//            GoodsLabelDataModel::deleteAll(['goods_id' => $condition['id']]);
//        }
//        return true;
//    }
    
    //获取标签
    public function getLabels()
    {
        $data = GoodsLabelDataModel::find()->asArray()->where(['goods_id' => $this->id])->all();
        return $data;
    }
    
    //获取不同尺寸图片
    //$img 图片的地址  可以带http://  $width尺寸 目前支持 60 120 160 240 320
    public static function getShopImg($img,$width)
    {
        if(strpos($img,"https://") === false){
            return \yii::$app->params['file_url'] . "/{$img}@!shop{$width}";
        }else{
            if(strpos($img,\yii::$app->params['file_url']) === false){
                $img = substr($img,strpos($img,'.com/')+5);
                return \yii::$app->params['file_url'] . "/{$img}@!shop{$width}";
            }else{
                return "{$img}@!shop{$width}";
            }
        }
    }
    
    //初始为20件的库存
    public function beforeSave($insert) {
        $state = parent::beforeSave($insert);
//        
//        if($insert){
//            $this->num = 20;
//        }
        
        //$this->getHesetImg("img");

        
        return $state;
    }    

}
