<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_package}}".
 *
 * @property string $id
 * @property string $shop_list
 * @property integer $add_time
 * @property integer $sorting
 * @property string $img
 * @property string $img_list
 * @property string $detail
 * @property string $mine_attributes
 */
class GoodsPackageModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_package}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_list', 'img', 'img_list', 'details', 'mine_attributes'], 'required'],
            [['shop_list', 'img_list', 'details', 'mine_attributes'], 'string'],
            [['add_time', 'sorting'], 'integer'],
            [['img'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shop_list' => '商品组合',
            'add_time' => '添加时间',
            'sorting' => '排序',
            'img' => '主图',
            'img_list' => '多图列表',
            'details' => '详情',
            'mine_attributes' => '属性',
            'status' => '状态',
            'shop_fuwu' => '附加商品',
        ];
    }
    
    
    //找相关的商品 $condition条件  is_upset 是否随机打乱  is_cache 是否缓存  cache_time 缓存时间
    public static function getVarData($condition = [], $is_upset = false, $is_cache = false, $cache_time = 600)
    {
        if(!empty($condition['no'])){
            $datas = $condition;
        }else if(!empty($condition['no_get'])){
            $datas = array_merge($_POST,$condition);
        }else if(!empty($condition['no_post'])){
            $datas = array_merge($_GET,$condition);
        }else{
            $datas = array_merge($_POST,$_GET,$condition);
        }
        
        
        $query = self::find();
        
        //条件
        $where = [];
        $columns = static::getTableSchema()->columns;
        foreach($datas as $attribute => $value){
            $filter = [];
            if(is_numeric($attribute)){
                $where[$attribute] = $value;
                continue;
            }
            if(empty($columns[$attribute])){
                continue;
            }
            $where[$attribute] = $value;
        }        
        $query->where($where);
        //echo $query->createCommand()->getRawSql();exit;
        
        //排序
        $datas['order'] = empty($datas['order']) ? '' : $datas['order'];
        static::getOrderd($query, $datas['order']);
        
        //分页
        $page = empty($datas['page']) ? 1 : $datas['page'];
        $page < 1 ? $page = 1 : '';
        
        $pagesize = empty($datas['pagesize']) ? 20 : $datas['pagesize'];
        $pagesize < 1 ? $pagesize = 1 : '';
        
        $offset = ($page - 1) * $pagesize;
        
        
        
        
        //取数据模式
        $gettype = empty($datas['gettype']) ? 0 : $datas['gettype'];
        
        switch ($gettype) {
            case 0://只取数据
                $query->limit($pagesize)->offset($offset);
                $list = $query->asArray()->all();

                break;
            case 1://取总数+分页
                $total = $query->count();
                $query->limit($pagesize)->offset($offset);

                break;
            case 2://取总数+数据+分页
                $total = $query->count();
                $query->limit($pagesize)->offset($offset);
                break;

            default:
                break;
        }
        return $list;
    }
    
    public static function getOrderd($query,$orderd)
    {
        $order = '';
        switch ($orderd) {
            //时间最新
            case 1:
                $order = 'add_time  DESC';
                break;
            //时间最早
            case 2:
                $order = 'add_time ASC';
                break;
            case 3:
            case 4:                                                                          
            default:
                $order = 'sorting asc';//默认排序
                break;  
        }
        $query->orderBy($order);
    }   
    
    
    //新增或者是修改后的操作
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($_POST){
            try {
                $transaction = Yii::$app->db->beginTransaction();//开启事务   
                if(!empty($_POST['label'])){//把旧的删除掉，然后留下新的
                    GoodsLabelDataPackageModel::deleteAll(['goods_id' => $this->id]);
                    $time = time();
                    foreach($_POST['label'] as $key => $value){
                        $model = new GoodsLabelDataPackageModel();
                        $model->label_id = $value;
                        $model->goods_id = $this->id;
                        $model->save(false);
                    }
                }else{
                    GoodsLabelDataPackageModel::deleteAll(['goods_id' => $this->id]);
                }       
                $transaction->commit();//提交事务                
            } catch (\Exception $exc) {
                $transaction->rollBack();//释放事务
            }

        }
        return true;
    }
    
    public function afterDelete() {
        parent::afterDelete();
        GoodsLabelDataPackageModel::deleteAll(['goods_id' => $this->id]);
        return true;
    }
    
    public static function afterDeleteAll($condition = '', $params = array()) {
        if(!empty($condition['id'])){
            GoodsLabelDataPackageModel::deleteAll(['goods_id' => $condition['id']]);
        }
        return true;
    }    
    
    //获取标签
    public function getLabels()
    {
        $data = GoodsLabelDataPackageModel::find()->asArray()->where(['goods_id' => $this->id])->all();
        return $data;
    }    
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '正常',
                '0' => '下架',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }    
    
}
