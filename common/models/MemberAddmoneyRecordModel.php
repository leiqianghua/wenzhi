<?php

namespace common\models;

use Yii;
use common\components\Tool;
use Exception;
/**
 * This is the model class for table "{{%member_addmoney_record}}".
 *
 * @property string $uid
 * @property string $code
 * @property string $money
 * @property string $pay_type
 * @property string $status
 * @property integer $add_time
 * @property string $scookies
 * @property integer $consume_type
 * @property integer $red_id
 * @property string $trade_no
 */
class MemberAddmoneyRecordModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_addmoney_record}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => '用户ID',
            'code' => '订单号',
            'money' => '金额',
            'pay_type' => '支付渠道',
            'status' => 'Status',
            'add_time' => '支付时间',
            'scookies' => 'Scookies',
            'consume_type' => '支付类型',
            'red_ids' => '搭配使用优惠券',
            'trade_no' => '第三方交易号',
        ];
    }
    
    //充值购买金额后回调
    // $code 订单号  
    // $trade_no第三方交易号
    // $pay_type 交易的方式  充值的途径 支付宝 或者 微信等
    public static function addMoneyCallback($code,$trade_no,$pay_type = 1)
    {
        $key = "addMoneyCallback_{$code}";
        try {
            if(Tool::checkLock($key)){//拿到了锁
                $transaction = Yii::$app->db->beginTransaction();//开启事务            
                $model = static::findOne($code);
                if(!$model){
                    throw new Exception('找不到相关数据');
                }
                if($model['status'] == 1){
                    throw new Exception('已经处理过');
                }
                
                $model->status = 1;
                $model->trade_no = $trade_no;
                $model->pay_type = $pay_type;
                $model->save(false);
                
                $state = MemberMoneyRecordModel::changeMoney(1, $model['money'], 2, $model['code'], $model['uid']);
                if(!$state){
                    throw new Exception(MemberMoneyRecordModel::$error);
                }
                
                //充值的回调
                $state = \common\components\CallbackOperation::afterRecharge($model);
                if(!$state){
                    throw new Exception(\common\components\CallbackOperation::$error);
                }   
                
                $transaction->commit();//提交事务
                Tool::deleteLock($key);
                return $model;
            }else{
                return false;
            }            
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            Tool::deleteLock($key);
            $transaction->rollBack();//释放事务
            return false;
        }
    }
    
    /*
     * 添加充值订单
     */
    public static function rechargePay($total_fee,$pay_type,$userid = '',$order_id_qian = "C_R")
    {
        $userid = empty($userid) ? \yii::$app->controller->user->uid : $userid;
        $user = MemberModel::find()->where(['uid' => $userid])->one();
        $res['status'] = 0;
        if ($user) {
            $out_trade_no =  self::pay_get_dingdan_code($order_id_qian);
            if(empty($out_trade_no)||$total_fee<=0)
            {
                static::$error = '请求订单信息无效';
                return false;
            }
            $model = new MemberAddmoneyRecordModel;
            $model->code=$out_trade_no;
            $model->uid=$userid;
            $model->money = $total_fee;
            $model->pay_type =$pay_type;
            $model->add_time = time();
            $model->consume_type=1;
            $model->save();
            return $model;
        }else {
            static::$error = "用户未登录";
            return false;
        }
    }
    
    /*
     * 生成订单号
     */
    protected static function pay_get_dingdan_code($dingdanzhui=''){
	return $dingdanzhui.time().substr(microtime(),2,6).rand(0,9);
    }
    
    /*充值记录  */
    public static function get_record()
    {
        $res['status']=0;
        $controller=\yii::$app->controller;
        $page=$controller->params->page;
        $uid=$controller->user->uid;
        if(!$uid){
            $res['info']='请先登录';
            return $res;
        }
        $page=$page?(int)$page-1:0;
        $pageSize=10;
        $offset=$page*$pageSize;
        $res['data']=self::find()
        //money`,`time`,`pay_type`,`status` FROM `go_member_addmoney_record`
        ->select(['FORMAT(money,0) as money','add_time','pay_type','IF(status=1,\'已兑换\',\'\') as status'])
        ->where(['uid'=>$uid,'status'=>1,'consume_type'=>3,'pay_type'=>'充值卡兑换'])
        ->orderBy('add_time desc')
        ->offset($offset)
        ->limit($pageSize)
        ->asArray()
        ->all();
        $res['status']=1;
        return $res;
    }
    /*
     * 查询用户订单
     */
    public static function getorderstudy($param=null)
    {
        $res['status']=0;
        $res['info']='';
        $code = trim($param['code']);
        if(isset($code)==false)
        {
            return $res;
        }
        $model = static::find()->where(['code'=>$code])->asArray()->one();
        if(!$model)
        {
           return $res;
        }
        if($model['status']==1)
        {
            $res['status']=1;
            if($model['consume_type']==1)
            {
                $res['info']='充值成功';
            }
            else if($model['consume_type']==2)
            {
                $res['info']='支付成功';
            }
        }
        else
        {
            $res['info']='请稍后再试';
        }
        return $res;
    }
    
    //显示的HTML
    public static  function getConsume_typeHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '普通充值',
                '2' => '消费充值',
                '3' => '购买优惠券充值',
        ];
        if($mix_data !== false){
                return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
                return $arr_data;
        }
    }    
}
