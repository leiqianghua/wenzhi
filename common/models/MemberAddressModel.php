<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_address}}".
 *
 * @property string $id
 * @property string $uid
 * @property string $username
 * @property string $area_id
 * @property string $city_id
 * @property string $province_id
 * @property string $address
 * @property string $iphone
 * @property integer $is_default
 */
class MemberAddressModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_address}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'area_id', 'city_id', 'province_id', 'is_default'], 'integer'],
            [['username', 'address', 'iphone'], 'required'],
            [['username'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 255],
            [['iphone'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'username' => 'Username',
            'area_id' => 'Area ID',
            'city_id' => 'City ID',
            'province_id' => 'Province ID',
            'address' => 'Address',
            'iphone' => 'Iphone',
            'is_default' => 'Is Default',
        ];
    }
}
