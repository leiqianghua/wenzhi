<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_apply}}".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $username
 * @property string $reason
 * @property integer $label
 * @property integer $add_time
 * @property integer $status
 */
class MemberApplyModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_apply}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户ID',
            'username' => '用户名',
            'reason' => '申请理由',
            'label' => '级别',
            'add_time' => '申请时间',
            'status' => '处理状态',
            'rejects_case' => '拒绝理由',
        ];
    }
    
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '未处理',
                '1' => '已成功处理',
                '2' => '已拒绝',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }
    
    //申请级别了后的通知。
    public  function afterApply()
    {
        //public function buy_notice($params,$my_url = '',$template_id = 'r9N_wVBHrEr4wW8_ZUZBmxxkkHX2P6lk8fMENSio6ck')
        $first = "申请更高级别通知";
        $keyword1 = $this->username;
        $keyword2 = date('Y-m-d H:i:s',$this->add_time);
        $remark = "申请更高级别:" . MemberModel::getLabelHtmlAll($this->label);
        $params = [
            "first" => [
                "value" => $first,
                "color" => "#fffff",
            ],
            "keyword1" => [
                "value" => $keyword1,
                "color" => "#fffff",
            ],
            "keyword2" => [
                "value" => $keyword2,
                "color" => "#fffff",
            ],
            "remark" => [
                "value" => $remark,
                "color" => "#fffff",
            ],
        ];
        $url =  "http://www.gubatoo.com" . \yii::$app->controller->to(['/wap/member/applylabel']);
        MemberModel::notice_user($params, $url,"-n106F-Grdww2M2q1hdMrUdwu1U7xs34WBFa7foPUsQ");
    }    
    
}
