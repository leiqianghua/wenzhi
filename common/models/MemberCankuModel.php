<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_canku}}".
 *
 * @property string $id
 * @property string $uid
 * @property string $title
 * @property integer $add_time
 */
class MemberCankuModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_canku}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'add_time'], 'integer'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户ID',
            'title' => '仓库名称',
            'add_time' => '添加时间',
        ];
    }
    
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $query = $dataProvider->query;
        if(!empty($_GET['money_change'])){
            switch ($_GET['money_change']) {
                case 1:
                    $query->andWhere("money > 0");

                    break;
                case 2:
                    $query->andWhere("money < 0");

                    break;

                default:
                    break;
            }
        }
        
        if (!empty($_GET['start_add_time'])) {
            $time = strtotime($_GET['start_add_time']);
            $query->andWhere("add_time>={$time}");
        }
        if (!empty($_GET['end_add_time'])) {
            $time = strtotime($_GET['end_add_time']);
            $query->andWhere("add_time<{$time}");
        }        
        return $dataProvider;
    }    
}
