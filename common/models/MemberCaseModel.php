<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_case}}".
 *
 * @property integer $id
 * @property string $province_id
 * @property string $city_id
 * @property string $area_id
 * @property string $address_info
 * @property string $address
 * @property string $iphone
 * @property string $username
 * @property string $winxin
 * @property string $money
 * @property string $realname
 * @property string $describe
 * @property integer $uid
 * @property integer $add_time
 * @property integer $status
 * @property string $imgs
 * @property string $score
 * @property string $ok_du
 * @property integer $type
 * @property integer $scene
 * @property integer $styles
 */
class MemberCaseModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_case}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'winxin', 'money', 'realname','iphone', 'province_id', 'city_id', 'area_id', 'uid', 'type', 'scene', 'styles'], 'required'],
            [['id', 'province_id', 'city_id', 'area_id', 'uid', 'add_time', 'status', 'score', 'ok_du', 'type', 'scene', 'styles'], 'integer'],
            [['describe', 'imgs'], 'string'],
            [['address_info'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 200],
            [['iphone'], 'string', 'max' => 11],
            [[ 'winxin', 'money', 'realname'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'province_id' => '省',
            'city_id' => '市',
            'area_id' => '区',
            'address_info' => '地址',
            'address' => '具体地址',
            'iphone' => '电话',
            'username' => '名称',
            'winxin' => '微信',
            'money' => '造景金额',
            'realname' => '造景师姓名',
            'describe' => '其它描述',
            'uid' => '会员ID',
            'add_time' => '上传时间',
            'status' => '状态',
            'imgs' => '多图',
            'score' => '所送积分',
            'ok_du' => '满意度分数',
            'type' => '客户类型',
            'scene' => '场景',
            'styles' => '风格',
            'title' => '案例库名称',
            'img' => '主图',
        ];
    }
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '未审核',
                '1' => '已审核(回访问且填写满意度)',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }        
    //显示的HTML  居家、商业、办公、其他
    public static  function getTypeHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '居家',
                '2' => '商业',
                '3' => '办公',
                '4' => '其他',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }           
    //场景 入户、玄关、楼梯间、阳台、立面、转角、橱窗、前台、立柱
    public static  function getSceneHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '入户',
                '2' => '玄关',
                '3' => '楼梯间',
                '4' => '阳台',
                '5' => '立面',
                '6' => '转角',
                '7' => '橱窗',
                '8' => '前台',
                '9' => '立柱',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }   
    //风格、中式复古、日式禅意、美式田园、欧式奢华、现代简约
    public static  function getStylesHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '中式复古',
                '2' => '日式禅意',
                '3' => '美式田园',
                '4' => '欧式奢华',
                '5' => '现代简约',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    } 
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if($insert){
            $first = "用户案例库上传成功通知";
            $keyword1 = $this->username;
            $keyword2 = date('Y-m-d H:i:s',$this->add_time);
            $remark = "案例名称:" . $this->title;
            $params = [
                "first" => [
                    "value" => $first,
                    "color" => "#fffff",
                ],
                "keyword1" => [
                    "value" => $keyword1,
                    "color" => "#fffff",
                ],
                "keyword2" => [
                    "value" => $keyword2,
                    "color" => "#fffff",
                ],
                "remark" => [
                    "value" => $remark,
                    "color" => "#fffff",
                ],
            ];   
            $url =  "http://www.gubatoo.com" . \yii::$app->controller->to(['/wap/member_case/detail','id' => $this->id]);
            MemberModel::notice_user($params, $url,"-n106F-Grdww2M2q1hdMrUdwu1U7xs34WBFa7foPUsQ");            
        }
    }
    
}
