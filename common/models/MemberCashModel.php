<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_cash}}".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $status
 * @property integer $money
 * @property string $account
 * @property string $type
 * @property string $name
 * @property string $remark
 * @property integer $add_time
 */
class MemberCashModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_cash}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户ID',
            'status' => '状态',
            'money' => '提现金额',
            'account' => '提现账号',
            'type' => '提现方式',
            'name' => '提现申请名',
            'remark' => '备注',
            'add_time' => '提现时间',
        ];
    }
    
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '未处理',
                '1' => '已处理',
                '2' => '用户已取消',
                '3' => '已拒绝',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }    
}
