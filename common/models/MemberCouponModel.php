<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_coupon}}".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $is_overlay
 * @property integer $money
 * @property integer $end_time
 * @property integer $must_money
 * @property string $goods_ids
 * @property integer $is_tejia
 * @property integer $is_use
 * @property integer $add_time
 * @property string $remark_relation
 */
class MemberCouponModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_coupon}}';
    }

    /**
     * @inheritdoc
     */
//    public function rules()
//    {
//        return [
//            [['uid', 'is_overlay', 'money', 'end_time', 'must_money', 'is_tejia', 'is_use', 'add_time'], 'integer'],
//            [['goods_ids'], 'string', 'max' => 500],
//            [['remark_relation'], 'string', 'max' => 200],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户ID',
            'is_overlay' => '是否可以叠加',
            'money' => '金额',
            'end_time' => '有效期结束时间',
            'must_money' => '满额',
            'goods_ids' => '必须是这些商品IDS',
            'is_tejia' => '是否允许特价',
            'is_use' => '是否使用',
            'add_time' => '添加时间',
            'remark_relation' => '关联商品',
        ];
    }
    
    //获取用户可用的优惠券
    //$datas  商品相关的东西
    //$uid 用户
    public static function getUseCoupon($datas,$total_money,$uid)
    {
        $condition['uid'] = $uid;//指定这个用户的
        $condition['is_use'] = 0;//没有使用的
        $condition[] = "invalid_time > " . TIMESTAMP;//没有失效的
        //$condition[] = "must_money = 0 or must_money <= {$total_money}";//达到满额的
        $condition['pagesize'] = 9999;
        $coupons = static::baseGetDatas($condition, false);
        //再过滤一些数据
        if($coupons){
            //获取可以使得的优惠券
            $goods = [];//购买的所有的商品的集合
            foreach($datas as $data){
                foreach($data['data'] as $d){
                    $goods[] = $d['id'];
                }
            }
            
            //对其继续过滤
            foreach($coupons as $key => $coupon){
                if($coupon['goods_ids']){//只有这些商品才可以使用
                    $arr_goods = explode(',', $coupon['goods_ids']);//所有的商品
                    foreach($goods as $good){
                        if(!in_array($good, $arr_goods)){
                            unset($coupons[$key]);
                            break;
                        }
                    }
                }
            }
        }
        
        return $coupons;
    }
    
    //显示的HTML
    public static  function getIs_overlayHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '不可叠加',
                '1' => '可叠加',
        ];
        if($mix_data !== false){
                return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : '';
        }else{
                return $arr_data;
        }
    }     
    //显示的HTML
    public static  function getIs_useHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '未使用',
                '1' => '已使用',
        ];
        if($mix_data !== false){
                return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : '';
        }else{
                return $arr_data;
        }
    }     
}
