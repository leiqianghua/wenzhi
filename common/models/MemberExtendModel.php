<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_extend}}".
 *
 * @property integer $uid
 * @property string $corporate_name
 * @property string $real_username
 * @property string $position
 * @property string $real_iphone
 * @property string $real_address
 * @property integer $real_add_time
 */
class MemberExtendModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_extend}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'corporate_name' => '公司名称',
            'real_username' => '姓名',
            'position' => '职位',
            'real_iphone' => '直实地址',
            'real_address' => 'Real Address',
            'real_add_time' => 'Real Add Time',
        ];
    }
}
