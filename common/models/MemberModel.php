<?php

namespace common\models;

use Yii;
use Exception;
/**
 * This is the model class for table "{{%member}}".
 *
 * @property string $uid
 * @property string $username
 * @property string $iphone
 * @property string $add_time
 * @property string $last_login_time
 * @property integer $status
 * @property string $password
 * @property string $reg_key
 * @property integer $type
 * @property string $img
 */
class MemberModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['username', 'iphone', 'password', 'reg_key', 'img'], 'required'],
//            [['add_time', 'last_login_time', 'status', 'type'], 'integer'],
//            [['username'], 'string', 'max' => 20],
//            [['iphone'], 'string', 'max' => 11],
//            [['password'], 'string', 'max' => 32],
//            [['reg_key'], 'string', 'max' => 50],
//            [['img'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => '会员ID',
            'username' => '用户名',
            'iphone' => '手机',
            'add_time' => '添加时间',
            'last_login_time' => '最后登录时间',
            'status' => '状态',
            'password' => '密码',
            'reg_key' => '第三方Key',
            'type' => '注册渠道',
            'img' => '头像',
            'parent_id' => '上级用户ID',
            'label' => '级别',
            'exten_code' => '邀请码',
            'address_info' => '地址',
            'address' => '详细地址',
            'money' => '余额',
            'score' => '积分',
            'kaidang_bili' => '开单比例',
        ];
    }
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '无效用户',
                '1' => '正常用户',
                '2' => '冻结用户',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }          
    //显示的HTML
    public static  function getTypeHtml($mix_data = false)
    { 
        $arr_data = [
                'phone' => '手机注册',
                'weixin' => '微信注册',
                'qq' => 'QQ注册',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }            
    //会员级别
    public static  function getLabelHtml($mix_data = false)
    { 
        $arr_data = [
                //'0' => '供应商',
               // '1' => '战略加盟商',
                '2' => '品牌合伙人',
                '3' => '销售合伙人',
                '4' => '普通会员',
        ];
        if($mix_data !== false){
                if(!isset($arr_data[$mix_data])){
                    return $mix_data;
                }
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }
    //会员级别
    public static  function getLabelHtmlAll($mix_data = false)
    { 
        $arr_data = [
                '0' => '总部供应商',
                '1' => '事业合伙人',
                '2' => '品牌合伙人',
                '3' => '销售合伙人',
                '4' => '普通会员',
        ];
        if($mix_data !== false){
                if(!isset($arr_data[$mix_data])){
                    return $mix_data;
                }
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }    
    public function getParent_name()
    {
        if($this->parent_id == 0){
            return '无上级';
        }
        $data = static::findOne($this->parent_id);
        return $data['username'];
    }
    
    //$uid 为要顶替的uid
    //$t_uid 为被顶替的用户ID
    //我要替换成谁
    public static function dinti($t_uid,$uid = '')
    {
        try {
            $transaction = static::getDb()->beginTransaction();
            $uid = empty($uid) ? \yii::$app->controller->user->islogin() : $uid;
            $t_model = static::findOne($t_uid);
            $model = static::findOne($uid);
            if(!$t_model || !$model){
                throw new Exception('找不到需要被顶替的会员或找不到相关会员');
            }
            
            $parent_id = $model->parent_id;     //我的上级
            static::updateAll(['parent_id' => $parent_id], ['parent_id' => $model->uid]);//它的下级的全部换过来      
            static::updateAll(['parent_id' => $model->uid], ['parent_id' => $t_model->uid]);//别的下级的全部换过来   
            
            //顶替了后，被顶替的人变成不确定级别了
            //顶替的人则成为了新的级别
            //原来被替换的人级别变成不确定
            $model->label = $t_model->label;
            $model->parent_id = $t_model->parent_id;
            $model->save(false);//自己级别改变            
            
            //被替换的人变成不确定的级别
            $t_model->label = 4;//不确定级别
            $t_model->parent_id = 0;//无上级
            $t_model->save(false);
            

           
            $transaction->commit();
            
            return true;
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            $transaction->rollBack();
            return false;
        }
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        
        if(!$insert){
            $model = static::findOne($this->uid);
            if($model->label != $this->label){//级别不相等,,改级别
                $m  = static::find()->where(['and', ['parent_id' => $this->uid],"label <= $this->label"])->limit(1)->one();//找我的下级
                if($m){
                    $this->addError('label', '你改成比用户下级的会员的级别还低了');
                    return false;
                }
                $m  = static::find()->where(['and', ['uid' => $this->parent_id],"label >= {$this->label}"])->limit(1)->one();//找我的上级
                //echo static::find()->where(['and', ['uid' => $this->parent_id],"label >= {$this->label}"])->limit(1)->createcommand()->getrawsql();exit;
                if($m){
                    $this->addError('label', '你改成比上级会员的级别还高了');
                    return false;
                }
            }
            
            if($this->parent_id > 0 && $this->parent_id != $model->parent_id){
                $m = static::findOne($this->parent_id);
                if(!$m){
                    $this->addError('parent_id', '找不到此上级会员');
                    return false;
                }
                if($m['label'] >= $this->label){
                    $this->addError('parent_id', '上级会员级别比你设置的级别低');
                    return false;
                }                
            }
            
            //修改了密码
            if(!empty($_POST['password'])){
                $this->password = \yii::$app->controller->user->createPassword($_POST['password']);
            }else{
                unset($this->password);
            }
            
            if(!empty($_POST['iphone'])){
                if($_POST['iphone'] != $this->iphone){
                    $data = static::find()->where(['iphone' => $_POST['iphone']])->exists();
                    if($data){
                        $this->addError('iphone', '已经存在此手机号码');
                        return false;
                    }
                }
            }
            
        }
        
       
        
        return true;
    }
    
    //获取此用户的所有的上级
    public  static function getParents($id)
    {
    	$result[] = $id;
        $datas = static::findOne($id);
        $parent = $datas['parent_id'];//当前栏目的父ID
        if($parent != 0){
            $result = array_merge($result,static::getParents($parent));
        }
        return $result;
    }
    

    
    public  function afterDelete()
    {
        parent::afterDelete();
        MemberOauthDataModel::deleteAll(['uid' => $this->uid]);
    }
    
    public static function afterDeleteAll($condition = '', $params = array())
    {
        parent::afterDeleteAll($condition, $params);
        if(!empty($condition['uid'])){
            MemberOauthDataModel::deleteAll(['uid' => $condition['uid']]);
        }
        return true;
    }
    
    /**
     * 
     * @param type $data  通知内容
     * @param type $url  通知跳转的链接
     * @param type $is_has_parant  是否是要通知上级
     * @param type $is_has_system  是否是要通知系统
     */
    public static function notice_user($data,$url = '',$template_id = '',$is_has_parant = true,$is_has_system = true,$is_mine = false,$other_user= [])
    {
        if($is_has_system){
            $notice_uid = \common\models\ConfigModel::getListForKey("goods_buy_notice");
            if($notice_uid){
                $uids = explode(',', $notice_uid);
                foreach($uids as $uid){
                    $obj = new \common\components\TemplateMessage();
                    $obj->uid = $uid;//固定写死
                    $obj->buy_notice($data,$url,$template_id);                
                }
            }            
        }
        
        //额外的用户
        if($other_user){
            foreach($other_user as $uid){
                $obj = new \common\components\TemplateMessage();
                $obj->uid = $uid;//固定写死
                $obj->buy_notice($data,$url,$template_id);     
                
                
                if($is_has_parant){
                    $m = \common\models\MemberModel::getOne($uid);
                    if($m && $m['parent_id']){
                        $obj = new \common\components\TemplateMessage();
                        $obj->uid = $m['parent_id'];//固定写死
                        $obj->buy_notice($data,$url,$template_id);            
                    }            
                }                 
            }          
        }
        
        if($is_has_parant){
            $uid = \yii::$app->controller->user->isLogin();
            if($uid){
                $m = \common\models\MemberModel::getOne($uid);
                if($m && $m['parent_id']){
                    $obj = new \common\components\TemplateMessage();
                    $obj->uid = $m['parent_id'];//固定写死
                    $obj->buy_notice($data,$url,$template_id);            
                }                
            }            
        } 
        
        //是否是要通知本人
        if($is_mine){
            $uid = \yii::$app->controller->user->isLogin();
            if($uid){
                $obj = new \common\components\TemplateMessage();
                $obj->uid = $uid;
                $obj->buy_notice($data,$url,$template_id);                
            }            
        }        
        
    } 
    
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $query = $dataProvider->query;
        if (!empty($_GET['is_gongyin'])) {
            $query->andWhere("admin_id > 0");
        } 
        
//        if (!empty($_GET['is_shen'])) {
//            $query->andWhere("status=2");
//        }
        return $dataProvider;
    }
    
    /**
     * 微信通知消息
     * @param type $first_title   最顶部标题名称
     * @param type $product_title   产品名称
     * @param type $price  价格
     * @param type $date  时间
     * @param type $remark  备注信息
     * @param type $url  跳转URL
     * @param type $is_has_parant  是否是要通知上级   这个上级表示 ,当前登录用户的上级
     * @param type $is_has_system 是否是要通知系统管理员
     * @param type $is_mine  是否是要通知 自己   表示的是当前登录的用户
     * @param type $other_user  通知其它相关的会员  uid数组
     */
    public static function buyOkNotice($first_title,$product_title,$price,$date,$remark,$url = "",$is_has_parant = false,$is_has_system = true,$is_mine = true,$other_user = [])
    {
        $data = [
            "first" => [
                "value" => $first_title,
                "color" => "#fffff",
            ],
            "product" => [
                "value" => $product_title,
                "color" => "#fffff",
            ],
            "price" => [
                "value" => $price,
                "color" => "#fffff",
            ],
            "time" => [
                'value' => $date,
                "color" => "#fffff",
            ],
            "remark" => [
                "value" => $remark,
                "color" => "#fffff",
            ],
        ];    
        \common\models\MemberModel::notice_user($data, $url, "r9N_wVBHrEr4wW8_ZUZBmxxkkHX2P6lk8fMENSio6ck", $is_has_parant, $is_has_system, $is_mine,$other_user);               
    }
    
    //$can_uid 的上级用户是否是  $uid
    public static function isParent($uid,$can_uid)
    {
        if($uid == $can_uid){
            return true;
        }
        while (true){
            $user = MemberModel::getOne($can_uid);
            if(!$user || $user['parent_id']==0){
                return false;
            }
            if($user['parent_id'] != $uid){
                $can_uid = $user['parent_id'];
                continue;
            }else{
                return true;
            }
        } 
    }
    
    //把可以设置的所有的上级给拿出来
    public static function getAllCanSetParent($uid)
    {
        $biao = \common\models\hy_designerModel::createDesigner($uid, 0);
        $max_levels = $biao['max_levels'];//只要比这个小的就可以
        if(!$biao['num_levels']){
            return ['0' => "无上级"]; 
        }
        foreach($biao['num_levels'] as $type => $hide_shenfen){
            if($hide_shenfen == $max_levels){
                break;
            }
        }
        //我的级别  跟  类型 给过去了     求出可以设置的lev_id 来
        $cat_level_id = hy_studio_levelModel::getOkTypeForType($max_levels, $type);
        $condition = [];
        $condition['studio_id'] = \common\models\hy_designerModel::getOne($uid, "studio_id");//工作室一样
        $condition['studio_level_id'] = $cat_level_id;//要是管理员的
        $condition['noget'] = true;
        $datas = hy_designer_levelModel::baseGetDatas($condition);
        if($datas){
            $result = [];
            foreach($datas as $value){
                $result[$value['uid']] = MemberModel::getOne($value['uid'], "username");
            }
            return $result;
        }else{
            return ['0' => "无上级"];
        }  
    }
    
    public static function findAllParent($uid)
    {
        $result = [];
        
        while (true){
            $parent_id = static::findParent($uid);
            if($parent_id){
                $result[] = $parent_id;
                $uid = $parent_id;                
            }else{
                break;
            }

        }
        

        
        return $result;
    }
    
    public static function findParent($uid){
        $member = MemberModel::getOne($uid, "parent_id");
        if($member){
           return $member['uid'];
        }else{
            return false;
        }   
    }    
}
