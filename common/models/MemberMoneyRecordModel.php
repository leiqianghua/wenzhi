<?php

namespace common\models;

use Yii;
use common\components\Tool;
use Exception;
/**
 * This is the model class for table "{{%member_money_record}}".
 * 用户金额使用变化记录
 * @property integer $id
 * @property string $uid
 * @property string $add_time
 * @property string $type
 * @property integer $money
 * @property integer $balance
 * @property string $relation_data
 */
class MemberMoneyRecordModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_money_record}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户ID',
            'add_time' => '添加时间',
            'type' => '变化类型',
            'money' => '变化金额',
            'balance' => '变化后用户余额',
            'relation_data' => '关联数据',
        ];
    }
    
    /*
     * $uid 用户ID
     * $type 变化 的类型
     * $money 变化的金额
     * $balance 用户余额
     * $relation_data 关联的数据
     */
    public static function addRecord($uid,$type,$money,$balance,$relation_data)
    {
        $data = [];
        $data['uid'] = $uid;
        $data['type'] = $type;
        $data['money'] = $money;
        $data['balance'] = $balance;
        $data['relation_data'] = $relation_data;
        $model = new self();
        $model->attributes = $data;
        $model->save(false);
        return true;
    }
    
    //变化金额  会改变两个表的记录  用户金额表   金额变化表
    //$change_type 1为用户增加金额   2为用户减少金额  
    //$money 增加或减少的金额数量
    //$type 是哪个类型来变化金额的  2支付充值
    //$relation_data 关联的相关的东西，或者是注释
    //$uid 用户的ID
    //成功 则返回 金额变化表，失败则返回false;
    public static function changeMoney($change_type,$money,$type,$relation_data,$uid = '')
    {
        try {
            $uid = $uid === '' ? \yii::$app->controller->user->islogin() : $uid;
            
            $transaction = yii::$app->db->beginTransaction();
            Tool::addLock("changeMoney_{$uid}");
            $member_model = MemberModel::findOne($uid);
            if(!$member_model){
                throw new Exception('找不到用户数据');
            }           
            
            //给用户增加减金额
            if($change_type == 1){
                $money_user = $member_model->money + $money;
            }else{
                $money_user = $member_model->money - $money;
                $money = -$money;
            }
            if($money_user < 0){
                throw new Exception('扣除金额失败,用户余额不足');
            }
            $member_model->updateCounters(['money' => $money]);
            
            //增加金额变化记录
            $data = [];
            $data['uid'] = $uid;
            $data['type'] = $type;
            $data['money'] = $money;
            $data['balance'] = $member_model->money;
            $data['relation_data'] = $relation_data;
            $model = new static;
            $model->attributes = $data;
            $state = $model->save();
            if(!$state){
                throw new Exception($model->getOneError());
            }
            
            Tool::deleteLock("changeMoney_{$uid}");    
            $transaction->commit();//释放事务
            return $model->id;
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            echo static::$error;exit;
            Tool::deleteLock("changeMoney_{$uid}");      
            $transaction->rollBack();//释放事务
            return false;
        }
    }
    
    
    //显示的HTML
    public static  function getTypeHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '购买商品扣款',
                '2' => '购买商品充值',
                '3' => '普通充值',
        ];
        if($mix_data !== false){
                return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
                return $arr_data;
        }
    }    
}
