<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_oauth_data}}".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $oauth_key
 * @property string $oauth_type
 * @property integer $add_time
 */
class MemberOauthDataModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_oauth_data}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'oauth_key' => 'Oauth Key',
            'oauth_type' => 'Oauth Type',
            'add_time' => 'Add Time',
        ];
    }
    
    //绑定功能
    public static function band($type,$key,$uid,$username,$img)
    {
        $condition = [
            'oauth_key' => $key,
            'oauth_type' => $type,
        ];
        $m = static::findOne($condition);
        if($m){
            return false;
        }
        $model = new static();
        $model->uid = $uid;
        $model->oauth_key = $key;
        $model->oauth_type = $type;
        $model->username = $username;
        $model->img = $img;
        return $model->save();
    }
    //解绑功能
    public static function unBand($type,$key,$uid)
    {
        $condition = [
            'uid' => $uid,
            'oauth_key' => $key,
            'oauth_type' => $type,
        ];
        return static::deleteAll($condition);
    }
}
