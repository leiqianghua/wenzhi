<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use Exception;

class MemberOauthQQModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_oauth_qq}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'access_token' => 'access_token',
            'openid' => 'openid',
            'add_time' => '添加时间',
            'uid' => '用户ID',
        ];
    }
    //操作
    // 查找相关的openid 然后再登录，登录如果没有数据的话则是进行其它的操作，如果有的话则是直接登录
    // $access_token 是保存的相关的数据  必定会有一条记录在这里面的，
    //如果找到了登录的相关的信息后则直接登录，如果找不到绑定的用户则直接返回false
    public static function operation(&$access_token)
    {
        try {
            $obj = new \common\components\oauth\QQ();//创建联合第三方对象
            $result = $obj->getAccess_token();
            if(!$result){
                throw new Exception($obj->error);
            }else{
                $access_token = $obj->session_array['access_token'];
            }
            
            //获取openid
            $model = static::findOne($obj->session_array['access_token']);
            if($model){//直接已经有数据的话则直接登录
                //如果已经是登录状态的话，则绑定这个到现在登录的用户上去
                if($uid = \yii::$app->controller->user->isLogin()){
                    //绑定并且跳转到其绑定的界面
                    //如果已经被绑定则提示，如果没有则绑定
                    
                    $state = MemberOauthDataModel::band('qq', $model['openid'], $uid,$model['username'],$model['img']);
                    if(!$state){
                        \yii::$app->controller->error('已经绑定','/wap/member_oauth/index');
                    }else{
                        \yii::$app->controller->success('成功绑定','/wap/member_oauth/index');
                    }
                }
                $datas = [
                    'third_party' => 1,
                    'type' => 'qq',
                    'reg_key' => $model['openid'],
                ];
                return yii::$app->user->login($datas);
            }
            
            $state = $obj->getOpenID();
            if(!$state){//如果找不到则首次重新刷新获取access_token
                $obj->updateAccess_token();
                $state = $obj->getOpenID();
            }
            if(!$state){
                throw new Exception($obj->error);
            }
            
            $model = static::find()->where(['openid' => $obj->session_array['openid']])->limit(1)->one();
            if($model){//找得到联合登录数据
                $model->access_token = $obj->session_array['access_token'];
                $model->save(false);      
                if($uid = \yii::$app->controller->user->isLogin()){
                    //绑定并且跳转到其绑定的界面
                    $state = MemberOauthDataModel::band('qq', $model['openid'], $uid,$model['username'],$model['img']);
                    if(!$state){
                        \yii::$app->controller->error('已经绑定','/wap/member_oauth/index');
                    }else{
                        \yii::$app->controller->success('成功绑定','/wap/member_oauth/index');
                    }
                }                
                $datas = [
                    'third_party' => 1,
                    'type' => 'qq',
                    'reg_key' => $model['openid'],
                ];
                $state = yii::$app->user->login($datas);                
                return $state;                
            }else{//说明没有存储这个用户相关的oauth信息
                
                //保存用户信息
                //保存联合登录信息
                //登录
                $userinfo = $obj->getuserInfo();//获取到了用户的相关的数据
                if(!$userinfo){
                    throw new Exception('获取不到用户相关数据');
                }
                
                $d = [];
                $d['access_token'] = $obj->session_array['access_token'];
                $d['openid'] = $obj->session_array['openid'];
                $d['username'] = $userinfo['nickname'];
                $d['img'] = empty($userinfo['figureurl_2']) ? $userinfo['figureurl_1'] : $userinfo['figureurl_2'];
                $model = new self();
                $model->attributes = $d;
                $model->save(false);         
                
                if($uid = \yii::$app->controller->user->isLogin()){
                    //绑定并且跳转到其绑定的界面
                    $state = MemberOauthDataModel::band('qq', $obj->session_array['openid'], $uid,$model['username'],$model['img']);
                    if(!$state){
                        \yii::$app->controller->error('已经绑定','/wap/member_oauth/index');
                    }else{
                        \yii::$app->controller->success('成功绑定','/wap/member_oauth/index');
                    }
                }                

                $datas = [
                    'third_party' => 1,
                    'type' => 'qq',
                    'reg_key' => $obj->session_array['openid'],
                    'img' => empty($userinfo['figureurl_2']) ? $userinfo['figureurl_1'] : $userinfo['figureurl_2'],//把头像也一起传送过去注册
                    'username' => $userinfo['nickname'],
                ];
                return  yii::$app->user->login($datas);
            }
            return true;
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            return false;
        }
    }    
    //操作
    public static function operation_bak()
    {
        try {
            $obj = new \common\components\oauth\QQ();//创建联合第三方对象
            $result = $obj->getAccess_token();
            if(!$result){
                throw new Exception($obj->error);
            }
            
            $model = static::find()->where(['access_token' => $obj->session_array['access_token']])->asArray()->limit(1)->one();
            if($model){//直接已经有数据的话则直接登录
                $datas = [
                    'third_party' => 1,
                    'type' => 'qq',
                    'reg_key' => $model['openid'],
                ];
                yii::$app->user->login($datas);
                return true;
            }
            
            $state = $obj->getOpenID();
            if(!$state){//如果找不到则首次重新刷新获取access_token
                $obj->updateAccess_token();
                $state = $obj->getOpenID();
            }
            if(!$state){
                throw new Exception($obj->error);
            }
            
            $model = static::find()->where(['openid' => $obj->session_array['openid']])->limit(1)->one();
            if($model){//找得到联合登录数据
                $datas = [
                    'third_party' => 1,
                    'type' => 'qq',
                    'reg_key' => $model['openid'],
                ];
                yii::$app->user->login($datas);                
                $model->access_token = $obj->session_array['access_token'];
                $model->save(false);
                return true;                
            }else{//说明没有存储这个用户相关的oauth信息
                //获取用户的UID
                $member_model = MemberModel::find()->where(['reg_key' => $model['openid'], 'type' => 'qq'])->limit(1)->one();
                if($member_model){//用户表里面已经有此用户的数据
                    //保存本数据 并登录
                    $uid = $member_model['uid'];
                    $d = [];
                    $d['uid'] = $uid;
                    $d['access_token'] = $obj->session_array['access_token'];
                    $d['openid'] = $obj->session_array['openid'];
                    $model = new self();
                    $model->attributes = $d;
                    $model->save(false);
                    
                    $datas = [
                        'third_party' => 1,
                        'type' => 'qq',
                        'reg_key' => $obj->session_array['openid'],
                    ];
                    yii::$app->user->login($datas);
                    return true;                    
                }else{//用户表里面也没有保存用户相关的数据
                    //保存用户信息
                    //保存联合登录信息
                    //登录
                    $userinfo = $obj->getuserInfo();//获取到了用户的相关的数据
                    if(!$userinfo){
                        throw new Exception('获取不到用户相关数据');
                    }
                    
                    $datas = [
                        'third_party' => 1,
                        'type' => 'qq',
                        'reg_key' => $obj->session_array['openid'],
                        'img' => empty($userinfo['figureurl_2']) ? $userinfo['figureurl_1'] : $userinfo['figureurl_2'],//把头像也一起传送过去注册
                        'username' => $userinfo['nickname'],
                    ];
                    $userinfo = yii::$app->user->login($datas);
                    if(!$userinfo){
                        throw new Exception('登录失败');
                    }else{
                        $d = [];
                        $d['uid'] = $userinfo['uid'];
                        $d['access_token'] = $obj->session_array['access_token'];
                        $d['openid'] = $obj->session_array['openid'];
                        $model = new self();
                        $model->attributes = $d;
                        $model->save(false);                        
                    }
                }
            }
            return true;
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            return false;
        }
    }
}
