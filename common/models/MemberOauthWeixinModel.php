<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\oauth\WeixinPublicNumber;
use common\components\Curl;
use Exception;

class MemberOauthWeixinModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_oauth_weixin}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'access_token' => 'access_token',
            'unionid' => 'unionid',
            'openid' => 'openid',
            'add_time' => '添加时间',
            'uid' => '用户ID',
        ];
    }
    
    //操作
    public static function operation(&$access_token)
    {
        try {
            $obj = new \common\components\oauth\Weixin();//创建联合第三方对象
            $result = $obj->getAccess_token();
            if(!$result){
                throw new Exception($obj->error);
            }
            $access_token = $obj->session_array['access_token'];
            $model = static::find()->where(['unionid' => $obj->session_array['unionid']])->limit(1)->one();
            if($model){//找得到联合登录数据
                $model->access_token = $access_token;
                $model->save(false);       
                
                if($uid = \yii::$app->controller->user->isLogin()){
                    //绑定并且跳转到其绑定的界面
                    $state = MemberOauthDataModel::band('weixin', $model['unionid'], $uid,$model['username'],$model['img']);
                    if(!$state){
                        \yii::$app->controller->error('已经绑定','/wap/member_oauth/index');
                    }else{
                        \yii::$app->controller->success('成功绑定','/wap/member_oauth/index');
                    }
                }                       
                
                $datas = [
                    'third_party' => 1,
                    'type' => 'weixin',
                    'reg_key' => $model['unionid'],
                ];
                return yii::$app->user->login($datas);                
            }else{//说明没有存储这个用户相关的oauth信息
                //保存用户信息
                //保存联合登录信息
                //登录
                $userinfo = $obj->getUserinfo();//获取到了用户的相关的数据
                if(!$userinfo){
                    $this->updateAccess_token();
                    $userinfo = $obj->getUserinfo();
                }
                if(!$userinfo){
                    throw new Exception('获取不到用户相关数据');
                }
                
                
                $d = [];
                $d['access_token'] = $obj->session_array['access_token'];
                $d['openid'] = $obj->session_array['openid'];
                $d['unionid'] = $obj->session_array['unionid'];
                $d['username'] = $userinfo['nickname'];
                $d['img'] = $userinfo['headimgurl'];
                $model = new self();
                $model->attributes = $d;
                $model->save(false);                 
                
                if($uid = \yii::$app->controller->user->isLogin()){
                    //绑定并且跳转到其绑定的界面
                    $state = MemberOauthDataModel::band('weixin', $obj->session_array['unionid'], $uid,$model['username'],$model['img']);
                    if(!$state){
                        \yii::$app->controller->error('已经绑定','/wap/member_oauth/index');
                    }else{
                        \yii::$app->controller->success('成功绑定','/wap/member_oauth/index');
                    }
                }                       

                $datas = [
                    'third_party' => 1,
                    'type' => 'weixin',
                    'reg_key' => $obj->session_array['unionid'],
                    'img' => $userinfo['headimgurl'],
                    'username' => $userinfo['nickname'],
                ];
                return yii::$app->user->login($datas);
            }
            return true;
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            return false;
        }
    }
    
    //公众号的操作
    
    public static function operationForPublicNumber(&$access_token)
    {
        try {
            
//            $obj = new WeixinPublicNumber();
//            $url = $obj->getCodeUrl(); 
//            $curl = new Curl();
//            $result = $curl->get($url);
//            
//            $code = $_GET['code'];
//            $state = $_GET['state'];            
            
            
            $obj = new WeixinPublicNumber();//创建联合第三方对象
            $result = $obj->getAccess_token();
            if(!isset($obj->session_array['unionid'])){
                $obj->session_array['unionid'] = $obj->session_array['openid'];
            }            
            //\common\models\ErrorLogModel::addLog(json_encode($obj->session_array), 'getAccess_token');//增加记录
            if(!$result){
                throw new Exception($obj->error);
            }
            $access_token = $obj->session_array['access_token'];
            $model = static::find()->where(['openid' => $obj->session_array['openid']])->limit(1)->one();//由于微信公众号这一步骤得不到unionid 所以只能通过 openid来获取相应用户
            if($model){//找得到联合登录数据
                $model->access_token = $access_token;
                $model->save(false);          
                if($uid = \yii::$app->controller->user->isLogin()){
                    //绑定并且跳转到其绑定的界面
                    $state = MemberOauthDataModel::band('weixin', $model['unionid'], $uid,$model['username'],$model['img']);
                    if(!$state){
                        \yii::$app->controller->error('已经绑定','/wap/member_oauth/index');
                    }else{
                        \yii::$app->controller->success('成功绑定','/wap/member_oauth/index');
                    }
                }                                
                $datas = [
                    'third_party' => 1,
                    'type' => 'weixin',
                    'reg_key' => $model['unionid'],
                ];
                return yii::$app->controller->user->login($datas);    
            }else{
                //说明没有存储这个用户相关的oauth信息
                //保存用户信息
                //保存联合登录信息
                //登录
                $userinfo = $obj->getUserinfo();//获取到了用户的相关的数据
                if(!$userinfo){
                    $this->updateAccess_token();
                    //\common\models\ErrorLogModel::addLog(json_encode($obj->session_array), 'updateAccess_token');//增加记录
                    $userinfo = $obj->getUserinfo();
                    //\common\models\ErrorLogModel::addLog(json_encode($userinfo), 'getUserinfo');//增加记录
                }
                if(!$userinfo){
                    throw new Exception('获取不到用户相关数据');
                }
                $model = static::find()->where(['unionid' => $obj->session_array['unionid']])->limit(1)->one();
                if(!$model){
                    $model = new self();
                }

                $d = [];
                $d['access_token'] = $obj->session_array['access_token'];
                $d['openid'] = $obj->session_array['openid'];
                $d['unionid'] = $obj->session_array['unionid'];
                $d['username'] = $userinfo['nickname'];
                $d['img'] = $userinfo['headimgurl'];
                $model->attributes = $d;
                $model->save(false);                    
                        
                if($uid = \yii::$app->controller->user->isLogin()){
                    //绑定并且跳转到其绑定的界面
                    $state = MemberOauthDataModel::band('weixin', $obj->session_array['unionid'], $uid,$model['username'],$model['img']);
                    if(!$state){
                        \yii::$app->controller->error('已经绑定','/wap/member_oauth/index');
                    }else{
                        \yii::$app->controller->success('成功绑定','/wap/member_oauth/index');
                    }
                }                             
                $datas = [
                    'third_party' => 1,
                    'type' => 'weixin',
                    'reg_key' => $obj->session_array['unionid'],
                    'img' => $userinfo['headimgurl'],
                    'username' => $userinfo['nickname'],
                ];
                return yii::$app->user->login($datas);
            }
            return true;
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            \common\components\TmpLog::addData("微信公众号", static::$error, "");
            return false;
        }        
    }
   
}
