<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%member_weixin_exten_code}}".
 *
 * @property string $open_id
 * @property string $ticket
 * @property integer $add_time
 */
class MemberWeixinExtenCodeModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_weixin_exten_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['open_id'], 'required'],
            [['add_time'], 'integer'],
            [['open_id', 'ticket'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'open_id' => 'Open ID',
            'ticket' => 'Ticket',
            'add_time' => 'Add Time',
        ];
    }
}
