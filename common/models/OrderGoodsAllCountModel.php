<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_goods_all_count}}".
 *
 * @property string $id
 * @property string $order_id
 * @property string $goods_id
 * @property string $name
 * @property string $img
 * @property string $uid
 * @property string $username
 * @property string $money
 * @property string $discount_money
 * @property string $num
 * @property string $add_time
 * @property string $fujia
 */
class OrderGoodsAllCountModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_goods_all_count}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'goods_id', 'uid', 'num', 'add_time'], 'integer'],
            [['money', 'discount_money'], 'number'],
            [['fujia'], 'string'],
            [['name', 'img', 'username'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'goods_id' => 'Goods ID',
            'name' => 'Name',
            'img' => 'Img',
            'uid' => 'Uid',
            'username' => 'Username',
            'money' => 'Money',
            'discount_money' => 'Discount Money',
            'num' => 'Num',
            'add_time' => 'Add Time',
            'fujia' => 'Fujia',
        ];
    }
}
