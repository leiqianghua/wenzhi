<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_goods}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $keys_id
 * @property integer $keys_type
 * @property string $parts_data
 * @property string $uid
 * @property string $username
 * @property string $money
 * @property string $discount_money
 * @property integer $add_time
 */
class OrderGoodsModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_goods}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'uid', 'add_time'], 'integer'],
            [['money', 'discount_money'], 'number'],
            [['username'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => '订单ID',
            'goods_id' => '商品ID',
            'type' => '订单商品类型',
            'uid' => '用户ID',
            'username' => '用户名',
            'money' => '金额',
            'discount_money' => '折扣金额',
            'add_time' => '添加时间',
            'num' => '购买件数',
            'name' => '商品名称',
        ];
    }
    
    public static function getdataForOrderID($order_id)
    {
        $datas = static::find()->where(['order_id' => $order_id])->asArray()->all();
        $result = [];
        foreach($datas as $data){
            if($data['type'] == 1){
                $data['buy_num'] = $data['num'];
                $data['price'] = GoodsModel::getOne($data['goods_id'],'price');
                $result['-1']['name'] = '单件';
                $result['-1']['type'] = '1';
                $result['-1']['data'][] = $data;
                
            }else{
                $shop_list = json_decode($data['shop_list'], true);
                if($shop_list){
                    $result[$data['goods_id']] = $data;
                    foreach($shop_list as $value){
                        $result[$data['goods_id']]['name'] = $data['name'];
                        $d = GoodsModel::getOne($value['goods_id']);
                        $d['goods_id'] = $d['id'];
                        $result[$data['goods_id']]['data'][] = array_merge($d, $value);
                    }                    
                }

            }
        }
        return $result;
    }
    
    public static  function getTypeHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '单品',
                '2' => '内部商城',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }    
}
