<?php

namespace common\models;

use Yii;
use Exception;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_id
 * @property string $order_sn
 * @property string $uid
 * @property string $username
 * @property string $add_time
 * @property string $payment_code
 * @property string $payment_time
 * @property string $finish_time
 * @property string $goods_amount
 * @property string $order_amount
 * @property integer $order_state
 * @property string $update_time
 * @property string $trade_sn
 * @property integer $callback_state
 */
class OrderModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_sn', 'uid', 'username'], 'required'],
            [['uid', 'add_time', 'payment_time', 'finish_time', 'order_state', 'callback_state'], 'integer'],
            [['goods_amount', 'order_amount'], 'number'],
            [['update_time'], 'safe'],
            [['username', 'trade_sn'], 'string', 'max' => 50],
            [['payment_code'], 'string', 'max' => 10],
            [['order_sn'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => '订单ID',
            'order_sn' => '订单号',
            'uid' => '用户ID',
            'username' => '用户名',
            'add_time' => '添加时间',
            'payment_code' => 'Payment Code',
            'payment_time' => '支付时间',
            'finish_time' => '完成时间',
            'goods_amount' => 'Goods Amount',
            'order_amount' => '订单价格',
            'order_state' => '状态',
            'update_time' => 'Update Time',
            'trade_sn' => '第三方单号',
            'callback_state' => 'Callback State',
            'shipping_code' => '快递单号',
            'fujia' => '附加费用',
            'red_ids' => '优惠券ID',
        ];
    }
    
    //订单状态
    public static  function getOrder_stateHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '未付款',
                '1' => '已付款',
                '2' => '已发货',
                '3' => '已完成',
                '4' => '已关闭',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }  
    
    public static function checkPay($order_id)
    {
        try {
            $order = static::find()->where(['order_sn' => $order_id])->limit(1)->one();
            if(!$order){
                throw new Exception('找不到该订单');
            }
            if($order['order_state'] != 0){
                throw new Exception('该订单已经处理');
            }
            return $order;
        } catch (\Exception $exc) {
            
            return false;
        }
    }
    
    public  static function getStateGood($model)
    {
        $url_array = [];
        switch ($model['order_state']) {
            case 0://未付款   可以取消订单
                $url_array = [
                    [
                        'url' => \yii::$app->controller->to(['cancel','id' => $model['order_id']]),
                        'title' => '取消订单',                                                    
                    ],
                ];
                break;

            case 1://已付款，可以提醒发货
                $url_array = [
                    [
                        'url' => \yii::$app->controller->to(['remind','id' => $model['order_id']]),
                        'title' => '提醒卖家发货',                                                    
                    ],
                ];
                break;

            case 2://已发货，可以确认收货，查看物流信息
                $url_array = [
                    [
                        'url' => \yii::$app->controller->to(['confirm','id' => $model['order_id']]),
                        'title' => '确认收货',                                                    
                    ],
                    [
                        'url' => \yii::$app->controller->to(['show_logistics','id' => $model['order_id']]),
                        'title' => '查看物流',                                                    
                        'noajax' => true,                                               
                    ],
                ];
                break;
            case 3://已完成，啥都不能操作
                $url_array = [
                    [
                        'url' => \yii::$app->controller->to(['show_logistics','id' => $model['order_id']]),
                        'title' => '查看物流',                                                          
                        'noajax' => true,                                                      
                    ],                                
                ];
                break;
            case 4://已关闭
                $url_array = [

                ];
                break;
            default:
                break;
        }
        $url_array[] = [
            'url' => \yii::$app->controller->to(['detail','id' => $model['order_id']]),
            'title' => '订单详情',      
            'noajax' => true,
        ];                    
        foreach($url_array as $d){
            if(!empty($d['noajax'])){
                echo "<a class=''  href='{$d['url']}'>{$d['title']}</a>";
            }else{
                echo "<a class='ajax_request'  href='{$d['url']}'>{$d['title']}</a>";
            }

        }        
    }
    
    
    function getOrder_goods(){
    	return $this->hasMany(OrderGoodsModel::className(), ['order_id'=>'order_id']);
    } 
    

    //对于我们搜索的时候先把这个我们要一起连接的先搜索出来。表
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->with("order_goods");
        return $dataProvider;
    }
}
