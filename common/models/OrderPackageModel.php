<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_package}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $package_id
 * @property string $name
 * @property string $uid
 * @property string $username
 * @property string $money
 * @property string $discount_money
 * @property integer $num
 * @property integer $add_time
 */
class OrderPackageModel extends \common\components\baseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_package}}';
    }

    /**
     * @inheritdoc
     */
//    public function rules()
//    {
//        return [
//            [['order_id', 'package_id', 'uid', 'num', 'add_time'], 'integer'],
//            [['name', 'username'], 'required'],
//            [['money', 'discount_money'], 'number'],
//            [['name', 'username'], 'string', 'max' => 100],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'package_id' => 'Package ID',
            'name' => 'Name',
            'uid' => 'Uid',
            'username' => 'Username',
            'money' => 'Money',
            'discount_money' => 'Discount Money',
            'num' => 'Num',
            'add_time' => 'Add Time',
        ];
    }
}
