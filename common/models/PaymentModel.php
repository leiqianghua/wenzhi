<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property integer $id
 * @property string $payment_code
 * @property string $payment_name
 * @property string $payment_config
 * @property integer $payment_state
 * @property string $img
 */
class PaymentModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_code', 'payment_name', 'payment_config', 'img'], 'required'],
            [['id', 'payment_state'], 'integer'],
            [['payment_config'], 'string'],
            [['payment_code', 'payment_name'], 'string', 'max' => 10],
            [['img'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_code' => 'Payment Code',
            'payment_name' => 'Payment Name',
            'payment_config' => 'Payment Config',
            'payment_state' => 'Payment State',
            'img' => 'Img',
        ];
    }
}
