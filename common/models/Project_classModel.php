<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_class}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $status
 * @property integer $sorting
 * @property string $add_time
 * @property integer $label
 * @property string $title
 */
class Project_classModel extends \common\models\BaseCategory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'sorting', 'add_time', 'label'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => '父ID',
            'status' => '状态',
            'sorting' => '排序',
            'add_time' => '添加时间',
            'label' => '层级',
            'title' => '名称',
            'img' => '图片',
        ];
    }
    
    public function beforeSave($insert) {
        $state = parent::beforeSave($insert);
        
        $this->getHesetImg("img");
        
        return $state;
    }
}
