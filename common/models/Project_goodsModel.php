<?php

namespace common\models;

use Yii;
use Exception;
/**
 * This is the model class for table "{{%goods}}".
 *
 * @property string $id
 * @property string $name
 * @property string $price
 * @property string $discount_1
 * @property string $discount_2
 * @property string $discount_3
 * @property string $add_time
 * @property integer $sorting
 * @property string $cid
 * @property string $img
 * @property string $img_list
 * @property string $details
 * @property integer $status
 */
class Project_goodsModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_goods}}';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '商品名称',
            'price' => '市场价格',
            'discount_0' => '出厂价格',
            'discount_1' => '事业合伙人',
            'discount_2' => '品牌合伙人',
            'discount_3' => '销售合伙人',
            'discount_4' => '普通会员',
            'add_time' => '添加时间',
            'weight' => '重量',
            'sorting' => '排序',
            'cid' => '分类ID',
            'img' => '主图',
            'img_list' => '图片',
            'details' => '详情',
            'status' => '状态',
            'num' => '库存',
            'type' => '商品类型',
        ];
    }
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '正常',
                '0' => '下架',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }



    public static function getOrderd($query,$orderd)
    {
        $order = '';
        switch ($orderd) {
            case 1:
                $order = 'add_time  desc'; //时间最新
                break;
            
            case 2:
                $order = 'add_time ASC';//时间最早
                break;
            
            case 3:
                $order = 'price asc';//价格从小到大
                break;
            
            case 4:
                $order = 'price DESC';//价格从大到小
                break;   
            case 5:
                $order = 'sorting asc,sale_num DESC';//综合排序
                break;                                                                                            
            default:
                $order = 'sorting asc,sale_num desc';//默认排序
                break;  
        }
        $query->orderBy($order);
    }

 
    
  
    
    //获取不同尺寸图片
    //$img 图片的地址  可以带http://  $width尺寸 目前支持 60 120 160 240 320
    public static function getShopImg($img,$width)
    {
        if(strpos($img,"http://") === false){
            return \yii::$app->params['file_url'] . "/{$img}@!shop{$width}";
        }else{
            if(strpos($img,\yii::$app->params['file_url']) === false){
                $img = substr($img,strpos($img,'.com/')+5);
                return \yii::$app->params['file_url'] . "/{$img}@!shop{$width}";
            }else{
                return "{$img}@!shop{$width}";
            }
        }
    }    
    function getCate()
    {
    	return $this->hasOne(Project_classModel::className(), ['id'=>'cid']);
    }     
    

    //对于我们搜索的时候先把这个我们要一起连接的先搜索出来。表
    public function search($params)
    {
        $dataProvider = parent::search($params);
        if($my_cid = \common\components\GuestInfo::getParam("my_cid")){//把这个及其所有的它的下级找出来
            $cids = Project_classModel::find()->where(['parent_id' => $my_cid])->select("id")->column();
            //print_r($cids);exit;
            if(!$cids){
                $cids = [];
            }
            $cids[] = $my_cid;
            $dataProvider->query->andWhere(['cid' => $cids]);
        }
        $dataProvider->query->with("cate");
        return $dataProvider;
    }   
    
 

}
