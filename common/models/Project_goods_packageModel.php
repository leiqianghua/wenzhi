<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_package}}".
 *
 * @property string $id
 * @property string $shop_list
 * @property integer $add_time
 * @property integer $sorting
 * @property string $img
 * @property string $img_list
 * @property string $detail
 * @property string $mine_attributes
 */
class Project_goods_packageModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_goods_package}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shop_list' => '商品组合',
            'add_time' => '添加时间',
            'sorting' => '排序',
            'img' => '主图',
            'img_list' => '多图列表',
            'details' => '详情',
            'mine_attributes' => '属性',
            'status' => '状态',
            'shop_fuwu' => '附加商品',
            'name' => '标题',
            'price' => '总价',
            'shop_fuwu' => '附加商品',
            'shop_fuwu' => '附加商品',
        ];
    }
    

    public static function getOrderd($query,$orderd)
    {
        $order = '';
        switch ($orderd) {
            //时间最新
            case 1:
                $order = 'add_time  DESC';
                break;
            //时间最早
            case 2:
                $order = 'add_time ASC';
                break;
            case 3:
            case 4:                                                                          
            default:
                $order = 'sorting asc';//默认排序
                break;  
        }
        $query->orderBy($order);
    }   
    
    
    function getCate()
    {
    	return $this->hasOne(Project_classModel::className(), ['id'=>'cid']);
    }     
    

    //对于我们搜索的时候先把这个我们要一起连接的先搜索出来。表
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->with("cate");
        return $dataProvider;
    }   
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '正常',
                '0' => '下架',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }    
    
}
