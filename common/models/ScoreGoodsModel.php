<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%score_goods}}".
 *
 * @property string $id
 * @property string $name
 * @property string $add_time
 * @property integer $sorting
 * @property integer $status
 * @property string $sale_num
 * @property string $num
 */
class ScoreGoodsModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%score_goods}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_time', 'sorting', 'status', 'sale_num', 'num'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'add_time' => 'Add Time',
            'sorting' => 'Sorting',
            'status' => 'Status',
            'sale_num' => 'Sale Num',
            'num' => 'Num',
        ];
    }
    
    //显示的HTML
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '1' => '上架',
                '0' => '下架',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }     
}
