<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%score_order}}".
 *
 * @property integer $order_id
 * @property string $order_sn
 * @property string $uid
 * @property string $username
 * @property string $add_time
 * @property string $address
 * @property string $phone
 * @property string $user_truename
 * @property string $shipping_code
 * @property string $e_code
 * @property integer $order_state
 */
class ScoreOrderModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%score_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'add_time', 'order_state'], 'integer'],
            [['order_sn'], 'string', 'max' => 200],
            [['username', 'user_truename', 'shipping_code', 'e_code'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 500],
            [['phone'], 'string', 'max' => 11],
            [['order_sn'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'order_sn' => 'Order Sn',
            'uid' => 'Uid',
            'username' => 'Username',
            'add_time' => 'Add Time',
            'address' => 'Address',
            'phone' => 'Phone',
            'user_truename' => 'User Truename',
            'shipping_code' => 'Shipping Code',
            'e_code' => 'E Code',
            'order_state' => 'Order State',
        ];
    }
    
    //显示的HTML
    public static  function getOrder_stateHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '未付款',
                '1' => '已付款',
                '2' => '已发货',
                '3' => '已完成',
                '4' => '已关闭',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }        
}
