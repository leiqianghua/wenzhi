<?php

namespace common\models;

use Yii;
use common\components\Tool;
use Exception;
/**
 * This is the model class for table "{{%score_detail}}".
 *
 * @property integer $id
 * @property string $uid
 * @property integer $score_type
 * @property string $add_time
 * @property string $score
 */
class Score_detailModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%score_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid','add_time', 'score'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '会员id',
            'score_type' => '积分类型',
            'add_time' => '添加时间',
            'score' => '积分',
        ];
    }
    
    //$change_type 1为用户增加积分   2为用户减少积分  
    //$score 增加或减少的积分 数量
    //$type 是哪个类型来变化积分 的
    //$relation_data 关联的相关的东西，或者是注释
    //$uid 用户的ID
    //成功 则返回 金额变化表，失败则返回false;
    public static function changeScore($change_type,$score,$type,$relation_data,$uid = '')
    {
        try {
            $uid = $uid === '' ? \yii::$app->controller->user->islogin() : $uid;
            
            $transaction = yii::$app->db->beginTransaction();
            Tool::addLock("changeScore_{$uid}");            
            
            $member_model = MemberModel::findOne($uid);
            if(!$member_model){
                throw new Exception('找不到用户数据');
            }        

            
            //给用户增加减积分
            if($change_type == 1){
                $money_user = $member_model->score + $score;
            }else{
                $money_user = $member_model->score - $score;
                $score = -$score;
            }
            if($money_user < 0){
                throw new Exception('扣除积分失败,用户积分不足');
            }
            $member_model->updateCounters(['score' => $score]);
            
            //增加积分变化记录
            $data = [];
            $data['uid'] = $uid;
            $data['score_type'] = $type;
            $data['score'] = $score;
            $data['remark'] = $relation_data;
            $model = new static;
            $model->attributes = $data;
            $state = $model->save();
            if(!$state){
                throw new Exception($model->getOneError());
            }
            
            Tool::deleteLock("changeScore_{$uid}");    
            $transaction->commit();//释放事务
            return $model->id;
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            Tool::deleteLock("changeScore_{$uid}");      
            $transaction->rollBack();//释放事务
            return false;
        }
    }
    
    //显示的HTML
    public static  function getScore_typeHtml($mix_data = false)
    { 
        $arr_data = [
                '购买商品' => '购买商品',
                '案例上传' => '案例上传',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }      
}
