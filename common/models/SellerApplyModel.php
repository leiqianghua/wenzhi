<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%seller_apply}}".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $company_address
 * @property string $company_name
 * @property string $company_iphone
 * @property string $company_corporation
 * @property string $registered_capital
 * @property string $main_products
 * @property string $company_profile
 * @property string $business_license_img
 * @property string $card_id_img
 * @property integer $type_1
 * @property integer $type_2
 * @property integer $status
 * @property string $add_time
 */
class SellerApplyModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%seller_apply}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['business_license_img', 'card_id_img','uid', 'type_1', 'type_2','company_address', 'company_name', 'company_iphone', 'company_corporation', 'registered_capital', 'main_products', 'company_profile'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户ID',
            'username' => '用户名',
            'company_address' => '公司地址',
            'company_name' => '公司名称',
            'company_iphone' => '公司电话',
            'company_corporation' => '法人代表',
            'registered_capital' => '注册资本',
            'main_products' => '主营产品',
            'company_profile' => '公司简介',
            'business_license_img' => '公司营业执照',
            'card_id_img' => '法人身份证',
            'type_1' => '供应商类型',
            'type_2' => '供应商类型',
            'status' => '状态',
            'add_time' => '申请时间',
            'address_info' => '供应商地址',
        ];
    }
    
    public static  function getType_1Html($mix_data = false)
    { 
        $arr_data = [
                '1' => '地方供应商',
                '2' => '战略供应商',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }        
    public static  function getType_2Html($mix_data = false)
    { 
        $arr_data = [
                '1' => '产品供应商',
                '2' => '服务供应商',
                '3' => '品牌供应商',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }         
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '未审核',
                '1' => '审核通过',
                '2' => '审核没有通过',
        ];
        if($mix_data !== false){
            if(isset($arr_data[$mix_data])){
                return $arr_data[$mix_data];
            }else{
                return $mix_data;
            }
        }else{
                return $arr_data;
        }
    }
    
    public function afterSave($insert, $changedAttributes) {
        $state = parent::afterSave($insert, $changedAttributes);
        if(!$insert){//修改
            if($this->status == 1){//已经同意啦
                //看是否是有其会员在这里面进行相关的登录的。 为其创建账号  然后做相应处理。
                $member = MemberModel::findOne($this->uid);
                if(!$member['type_1']){//为其创建相应的修改
                    
                    //在后台为其增加相应的账号。
                    $model = new AdminMember();
                    $model->username = $this->createUsername($this);
                    $model->password = $member->password;
                    $model->role_id = 4;
                    $model->save(false);
                    
                    //修改会员
                    $member->type_1 = $this->type_1;
                    $member->type_2 = $this->type_2;
                    $member->admin_id = $model->uid;
                    $member->save(false);
                }
            }
        }
    }
    
    public function createUsername($member)
    {
        $username = $member['username'];
        $is_data = AdminMember::find()->where(['username' => $username])->exists();
        if(!$is_data){
            return $username;
        }
        while(true){
            $rander = rand(1, 9999999);
            $name = $username . "_" . $rander;
            $is_data = AdminMember::find()->where(['username' => $name])->exists();
            if(!$is_data){
                return $name;
            }            
        }
    }
}
