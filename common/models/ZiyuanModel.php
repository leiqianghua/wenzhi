<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yii_ziyuan".
 *
 * @property integer $id
 * @property string $url
 * @property integer $add_time
 * @property integer $download_num
 */
class ZiyuanModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_ziyuan';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => '路径',
            'add_time' => '添加时间',
            'download_num' => 'Download Num',
            'name' => '文件名',
            'sorting' => '排序',
            'status' => '状态',
            'cid' => '文件夹',
            'label' => '权限级别',
        ];
    }
    public static  function getStatusHtml($mix_data = false)
    { 
        $arr_data = [
                '0' => '下架',
                '1' => '上架',
        ];
        if($mix_data !== false){
                return $arr_data[$mix_data];
        }else{
                return $arr_data;
        }
    }    
}
