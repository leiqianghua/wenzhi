<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_article".
 *
 * @property integer $id
 * @property string $title
 * @property string $synopsis
 * @property string $img
 * @property string $content
 * @property integer $add_time
 * @property integer $status
 */
class hy_articleModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['content'], 'string'],
//            [['add_time', 'status'], 'integer'],
//            [['title'], 'string', 'max' => 50],
//            [['synopsis', 'img'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'synopsis' => '简介',
            'img' => '图',
            'content' => '内容',
            'add_time' => '添加时间',
            'status' => '状态',
        ];
    }
    
    public static  function getStatusHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '上架',
            '0' => '下架',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
}
