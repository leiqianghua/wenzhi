<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_case".
 *
 * @property integer $id
 * @property integer $zaojia
 * @property integer $fenge
 * @property integer $type
 * @property integer $add_time
 * @property string $title
 * @property string $title1
 * @property string $label
 * @property string $img
 * @property integer $sorting
 * @property integer $show_num
 * @property string $city
 * @property integer $designer_id
 * @property string $designer_name
 * @property integer $studio_id
 * @property string $studio_name
 * @property string $content
 * @property integer $status
 */
class hy_caseModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_case';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['zaojia', 'fenge', 'type', 'add_time', 'sorting', 'show_num', 'designer_id', 'studio_id', 'status'], 'integer'],
//            [['content'], 'string'],
//            [['title', 'title1'], 'string', 'max' => 100],
//            [['label'], 'string', 'max' => 1000],
//            [['img'], 'string', 'max' => 300],
//            [['city', 'designer_name', 'studio_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'zaojia' => '造价',
            'fenge' => '风格',
            'type' => '类型',
            'add_time' => '添加时间',
            'title' => '标题',
            'title1' => '副标题',
            'label' => '标签',
            'img' => '主图',
            'sorting' => '排序',
            'show_num' => '浏览量',
            'city' => '城市',
            'designer_id' => '设计/造景师ID',
            'designer_name' => '设计/造景师名字',
            'studio_id' => '团队ID',
            'studio_name' => '工作室名称',
            'content' => '具体内容多图',
            'status' => '状态',
            'content_imgs' => '列表图',
            'use_type' => '创建用户类别',
        ];
    }
    

    
    public static  function getzaojiaHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '5万以下',
            '2' => '5-10万',
            '3' => '10-20万',
            '4' => '20-30万',
            '5' => '30-50万',
            '6' => '50-100万',
            '7' => '100万以上',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }   
    
    public static  function getfengeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '混搭',
            '2' => '自然',
            '3' => '现代',
            '4' => '英式',
            '5' => '法式',
            '6' => '日式',
            '7' => '地中海',
            '8' => '新中式',
            '9' => '美式',
            '10' => '东南式',
            '11' => '中式',
            '12' => '其它',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }     
    
    public static  function gettypeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '大花园(参考面积：200㎡以上)',
            '2' => '露台屋顶(参考面积：20-500㎡)',
            '3' => '下沉中庭(参考面积：120-200㎡)',
            '4' => '阳台(参考面积：3-50㎡)',
            '5' => '小庭院(参考面积：20-200㎡)',
            '6' => '商业花园',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }     
    
    public static  function gettype_zaojinHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '1大花园(参考面积：200㎡以上)',
            '2' => '露台屋顶(参考面积：20-500㎡)',
            '3' => '下沉中庭(参考面积：120-200㎡)',
            '4' => '阳台(参考面积：3-50㎡)',
            '5' => '小庭院(参考面积：20-200㎡)',
            '6' => '商业花园',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
    
    public static  function getStatusHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '显示',
            '0' => '隐藏',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
    
    public static  function getuse_typeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '设计师',
            '2' => '造景师',
            '0' => '未知',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
    
//    public function beforeSave($insert) {
//        $state = parent::beforeSave($insert);
//        
//        if($insert){
//            if($this->designer_id){//有指定是谁的
//                $this->use_type = \backend\models\AdminMenuModel::getOne($this->designer_id, "role_id");
//            }else{//没有指定是谁来上传的
//                $this->use_type = \yii::$app->controller->user->role_id;
//                $this->designer_id = \yii::$app->controller->user->uid;
//            }
//        }
//        
//        return $state;
//    }
    

    //对于我们搜索的时候先把这个我们要一起连接的先搜索出来。表
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->with("designer");
        
        
        if(!\yii::$app->controller->user->isSuper()){//不是超管  则要去找下它底下的所有的底下会员
            $uids = hy_designerModel::findAllXiaji(\yii::$app->controller->user->frontend_uid);
            //print_r($uids);exit;
            $uids[] = \yii::$app->controller->user->frontend_uid;
            $dataProvider->query->andWhere(['designer_id' => $uids]);
        }       
        
        
        return $dataProvider;
    }
    
    function getDesigner(){
    	return $this->hasOne(hy_designerModel::className(), ['uid'=>'designer_id']);
    }
    
    

    public static function getOrderd($query,$orderd)
    {
        $order = '';
        switch ($orderd) {
            case 1:
                $order = 'add_time  desc'; //时间最新
                break;
            
            case 2:
                $order = 'sorting desc,show_num desc';//最热  点击率越高越前面
                break;                                                                                          
            default:
                $order = 'add_time  desc';//默认排序
                break;  
        }
        $query->orderBy($order);
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if($_POST){
            hy_designerModel::updateAll(['case_update_time' => time()], ['uid' => $this->designer_id]);
        }
    }
    
    
}
