<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_category".
 *
 * @property integer $cateid
 * @property integer $parent_id
 * @property integer $status
 * @property string $name
 * @property string $catdir
 * @property string $info
 * @property integer $sorting
 * @property string $add_time
 */
class hy_categoryModel extends BaseCategory
{
    static $type;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%hy_category}}';
    }
    
    


    //获取所有的排好序的菜单  键值为   主键的值
    //  1 => []  2 => []  二维数组
    public static function getListForType($and_where = [])
    {
        if(!static::$type){
            return [];
        }
//        static $all;
//        if($all){
//            return $all;
//        }
        $datas = static::find()->orderBy("parent_id asc,sorting desc")->asArray()->where(['type' => static::$type])->andWhere($and_where)->all();
       // print_r($datas);exit;
        $contents = [];
        foreach($datas as $data){
            $contents[$data['id']] = $data;
        }
        return $all = $contents;
    }    
    
    /**
     * 获取所有的数据  一直往下的排
     * $layer定义了第几层  如果是第1层则获取所有第一层的数据
     * @param  boolean $layer [description]
     * @return [type]            [description]
     */
    public static function  getListMenuForType($layer = 1,$and_where=[])
    {
        $cache = [];
        $datas = static::getListForType($and_where);
        foreach ($datas as $id => $data) {
            if($data['parent_id'] == 0){
                $cache[$id] = static::getListForID($id, $layer, 1);
            }
        }
        return $cache;       
    }       
    

    
    //把相应的菜单信息展示好，按相应的格式一个个的来展示
    // 输出后，然后按其二维数据来展示相关的所有的数据
    public static function showMenuForType()
    {
        $list = static::getListMenuForType(false);
        $result = [];
        foreach($list as $key => $value){
            $d = static::getShowMenu($value);
            $result = array_merge($result,$d);
        }
        return $result;
    } 
    
    
    //获取所有的key value对应的这个菜单
    public static function getMenuKeyValueForType($insert_parent = true)
    {
        $datas = static::showMenuForType();
        if($insert_parent){
            $result[0] = '请选择';
        }else{
            $result = [];
        }
        
        foreach($datas as $key => $value){
            $v = $value['title'];
            $html = '';
            for($i = 1;$i < $value['Layer'];$i++){
                $html .= "├ ";
            }
            $result[$value['id']] = $html . $v;
        }
        return $result;
    }     
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['parent_id', 'status', 'sorting', 'add_time'], 'integer'],
//            [['title','img'], 'string', 'max' => 255],
//            [['catdir'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cateid' => '分类id',
            'parent_id' => '父类id',
            'status' => '是否显示',
            'title' => '栏目名称',
            'catdir' => '英文名',
            'info' => '介绍',
            'sorting' => '排序',
        	'label'=>'在第几层',
            'add_time' => '添加时间',
        	'img'=>'分类大图',
        	'type'=>'分类类型',
        ];
    } 
    
    /*
     * 获取商品分类  */
    public static  function getParentidHtml($type)
    {
    	$arr=self::find()->select(['id','title'])->andWhere(['type' => $type])->asArray()->all();
    	$arr_data[0]='≡ 作为一级栏目 ≡';
    	if($arr){
    		foreach ($arr as $v){
    			$id=$v['id'];
    			$arr_data[$id]=$v['title'];
    		}
    	}
    	
    	if($arr_data){
    		return $arr_data;
    	}
    }
    
    /*
     * 是否开启
     * */
    public static  function getStatusHtml($mix_data = false)
    {
    	$arr_data = [
    			'0' => '关闭',
    			'1' => '开启',
    	];
    	if($mix_data !== false){
    		return $arr_data[$mix_data];
    	}else{
    		return $arr_data;
    	}
    }
    
    /*
     * 是否开启
     * */
    public static  function getTypeHtml($mix_data = false)
    {
    	$arr_data = [
            '1' => '业务类',
            '2' => '设计类',
            '3' => '施工类',
            '4' => '增减项类',
    	];
    	if($mix_data !== false){
    		return $arr_data[$mix_data];
    	}else{
    		return $arr_data;
    	}
    }
    
    
    public static function getDataForJs()
    {
        //header("Content-type: text/html; charset=utf-8");
        $datas = static::getListMenu(false);
        //print_r($datas);exit;
        $result = [
            1 => [
                "id" => -1,
                'title' => "业务",
                'Layer' => 0,
                'data' => [],
            ],
            2 => [
                "id" => -2,
                'title' => "设计",
                'Layer' => 0,
                'data' => [],
            ],
            3 => [
                "id" => -3,
                'title' => "施工",
                'Layer' => 0,
                'data' => [],
            ],
            4 => [
                "id" => -4,
                'title' => "增减项",
                'Layer' => 0,
                'data' => [],
            ],
        ];
        //print_r($datas);
        foreach ($datas as $key => $value){
            $result[$value['type']]['data'][] = $value;
        }
        //print_r($result);exit;
        
        $dd = static::getDataForJsDetail($result);
        return $dd;
       // print_r($dd);exit;
    }
    
    
    public static function getDataForJsDetail($datas)
    {
        $result = [];
        foreach($datas as $key => $values){
            $arr = [];
            
            $arr['name'] = $values['title'];
            $arr['id'] = $values['id'];
            if(empty($values['id']) || $values['id'] < 0){
                //$arr['url'] = "create?type={$key}";
                
                $arr['nocheck'] = true;
            }else{
                //$arr['url'] = "update?id={$values['id']}";
                $arr['class'] = "layer_open_url";
            }
            
            if($values['data']){
                $arr['open'] = true;
                $arr['children'] = static::getDataForJsDetail($values['data']);
            }else if($values['Layer'] <= 1){
                $arr['isParent'] = true;
                

            }
            
            switch ($values['Layer']) {
                case 0:
//                    $arr['iconOpen'] = "/zTree_v3/css/zTreeStyle/img/diy/1_open.png";
//                    $arr['iconClose'] = "/zTree_v3/css/zTreeStyle/img/diy/1_close.png";

                    break;
                case 1:
                    $arr['iconOpen'] = "/zTree_v3/css/zTreeStyle/img/diy/1_open.png";
                    $arr['iconClose'] = "/zTree_v3/css/zTreeStyle/img/diy/1_close.png";

                    break;
                case 2:
                    $arr['icon'] = "/zTree_v3/css/zTreeStyle/img/diy/3.png";

                    break;
                case 3:
                    $arr['icon'] = "/zTree_v3/css/zTreeStyle/img/diy/2.png";

                    break;

                default:
                    $arr['icon'] = "/zTree_v3/css/zTreeStyle/img/diy/2.png";
                    break;
            }
            
            //$arr['checked'] = true;
            $result[] = $arr;
        }
        return $result;        
    }


    public static function all_update($datas)
    {
        $params = [
            "data" => $datas,
        ];
        $class_name = "\\" . __CLASS__;
        $method = "all_update";
        $cache_key = "{$class_name}_{$method}";
        $operation = [$class_name,"tit_all_update"];
        $result = \common\components\Tool::operation_method($cache_key, $operation, $params);     
        if($result['status']){
            return $result['info'];
        }else{
            static::$error = $result['info'];
            return false;
        }        
    }
    
    
    /**
     * id 购买哪一类
     * uid 哪个用户购买
     * buy_num  购买件数
     * @param type $params
     */
    public static function tit_all_update($params)
    {
        set_time_limit(0);
        $datas = $params['data'];
        //print_r($datas);exit;
        static::deleteAll();
        
        foreach($datas as $key => $value){
            if(!empty($value['data'])){
                static::savedata($value['data'],1,$key+1,0);
            }            
        }
        return true;
    }
    
    public static function savedata($data,$label,$type,$parent_id)
    {
        
        if(empty($data)){
            return true;
        }
        foreach($data as $kkk => $vvv){
            if(!empty($vvv['name'])){
                $model = new hy_categoryModel();
                $datass = [];
                $datass['parent_id'] = $parent_id;
                $datass['status'] = 1;
                $datass['label'] = $label;
                $datass['title'] = $vvv['name'];
                $datass['type'] = $type;
                $model->attributes = $datass;
                $model->save(false);
                if(!empty($vvv['data'])){
                    static::savedata($vvv['data'],$label+1,$type,$model->id);
                }
            }else{
                continue;
            }            
        }
    }
    
}
