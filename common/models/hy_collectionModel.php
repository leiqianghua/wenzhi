<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_collection".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $designer_id
 * @property integer $add_time
 */
class hy_collectionModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['uid', 'designer_id', 'add_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '会员ID',
            'designer_id' => '案例ID',
            'add_time' => '添加时间',
        ];
    }
    
    

    //对于我们搜索的时候先把这个我们要一起连接的先搜索出来。表
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->with("designer");
        $dataProvider->query->with("user");
        return $dataProvider;
    }
    
    function getCase(){
    	return $this->hasOne(hy_caseModel::className(), ['uid'=>'case_id']);
    }    
    function getUser(){
    	return $this->hasOne(hy_designerModel::className(), ['uid'=>'uid']);
    }
    
    
    public static function getDataForUid($uid,$case_id,$is_cache = true,$expire = 86400)
    {
        $cache_obj = \yii::$app->cache;
        $class_name = get_class(new static());
        $key = $class_name . "_getOne_{$uid}_{$case_id}";
        
        if($cache_obj->get($key) && $is_cache){
            return true;
        }
        
        $model = static::findOne(['case_id' => $case_id,'uid' => $uid]);
        if(!$model){
            $cache_obj->delete($key);
            return false;
        }else{
            $cache_obj->set($key,1,$expire);
            return true;
        }      
    }
    
    public function afterSave($insert, $changedAttributes) {
        $data = parent::afterSave($insert, $changedAttributes);
        $this->getDataForUid($this->uid, $this->case_id, false);
        return $data;
    }
    
    
    public static function deleteAll($condition = '', $params = [])
    {
        $state = static::beforeDeleteAll($condition,$params);
        if(!$state){
            return false;
        }
        
        if(!empty($condition['id'])){
            $delete_data = static::find()->where(['id' => $condition['id']])->asArray()->all();
        }        
        
        $parent = parent::deleteAll($condition,$params);
        static::afterDeleteAll($condition,$params);
        if(!empty($delete_data)){
            foreach($delete_data as $k => $v){
                static::getDataForUid($v['uid'], $v['case_id'], false);
            }
        }
        return $parent;
    }     
}
