<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_designer".
 *
 * @property integer $uid
 * @property string $img
 * @property string $company
 * @property integer $work_year
 * @property integer $design_fee_min
 * @property integer $design_fee_max
 * @property integer $design_fee_type
 * @property integer $celiang_fee_min
 * @property integer $celiang_fee_max
 * @property integer $celiang_fee_type
 * @property string $city
 * @property integer $studio_id
 * @property string $studio_name
 * @property integer $gold_type
 * @property integer $add_time
 */
class hy_designerModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_designer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['uid'], 'required'],
//            [['uid', 'work_year', 'design_fee_min', 'design_fee_max', 'design_fee_type', 'celiang_fee_min', 'celiang_fee_max', 'celiang_fee_type', 'studio_id', 'gold_type', 'add_time'], 'integer'],
//            [['img', 'company'], 'string', 'max' => 200],
//            [['city', 'studio_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'img' => '头像',
            'company' => '公司名称',
            'work_year' => '从业年限',
            'design_fee_min' => '最小设计费',
            'design_fee_max' => '最大设计费',
            'design_fee_type' => '设计费类型',
            'celiang_fee_min' => '最小测量费',
            'celiang_fee_max' => '最大测量费',
            'celiang_fee_type' => '测量费类型',
            'city' => '所属城市',
            'studio_id' => '所属工作室ID',
            'studio_name' => '所属工作室名称',
            'gold_type' => '标签类型',
            'linian' => '理念',
            'fenge_d' => '擅长风格',
            'jinyan' => '个人介绍',
            'zizhi' => '获得资质',
            'xinshui' => '工作年资',
            'zhuangye' => '专业',
            'biye' => '毕业院校',
            'email' => 'email',
            'jiguan' => '籍贯',
            'sexy' => '性别',
            'nickname' => '姓名',
        ];
    }
    
    
    public static  function getdesign_fee_typeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '50元以下/㎡',
            '2' => '50-100元/㎡',
            '3' => '100-150/㎡',
            '4' => '150-200/㎡',
            '5' => '200-300/㎡',
            '6' => '300元以上/㎡',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }        
    
    
    public static  function getsexyHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '男',
            '2' => '女',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }        
    public static  function getceliang_fee_typeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '免费测量',
            '2' => '200元以下/套',
            '3' => '200-500/套',
            '4' => '500-1000/套',
            '5' => '1000-3000/套',
            '6' => '3000元以上/套',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }      
    public static  function getgold_typeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '花园设计师',
            '2' => '金牌设计师',
            '3' => '资深设计师',
            '4' => '设计总监',
            '5' => '设计专家',
            
            
            '101' => '施工经理',
            '102' => 'Aqua生态水景',
            '103' => '水景工程师',
            '104' => '硬铺师傅',
            '105' => '泳池工程师',
            '106' => '浇灌工程师',
            '107' => '造雾工程师',
            '108' => '锦鲤池工程师',
            '109' => '假山师傅',
            '110' => '花园木匠',
            '111' => '植物搭配师',
            '112' => '景观风水师',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
    //找到这个人的所有的相关的数据  所属工作室  身份  身份标签 等这些数据   都是用前台账号来存储的。
    /*

Array
(
    [admin_user] => Array
        (
            [uid] => 1
            [username] => 雷强华
        )

    [designer_user] => Array
        (
            [uid] => 1
            [nickname] => abcd
            [img] => 
        )

    [levels] => Array 有哪些身份
        (
            [1] => fsdsd  身份ID  加  身份名称
            [3] => abcd   身份ID  加  身份名称
        )

    [max_levels] => 1   最高级的身份是在哪一层  （数字越低身份越高）
    [num_levels] => Array
        (
            [1] => 1   类型    对应  最高身份   我这个类型 的最高身份是什么
            [3] => 2    我这个类型 的最高身份是什么    3类型是设计师类型   其最高身份是设计总监职位
        )

)
     */
    public static function createDesigner($uid,$is_admin = true)
    {
        $result = [];
        if($is_admin){
            $result['admin_user'] = \backend\models\AdminMemberModel::getOne($uid);
        }else{
            $result['admin_user'] = \backend\models\AdminMemberModel::find()->where(['frontend_uid' => $uid])->asArray()->limit(1)->one();
        }
        if(empty($result['admin_user'])){
            static::$error = "找不到此用户数据";
            return false;
        }
        $result['designer_user'] = hy_designerModel::getOne($result['admin_user']['frontend_uid']);//前台的员工账号数据
        
        //有哪些身份
        $levels = \common\models\hy_designer_levelModel::find()->asArray()->limit(999)->where(['uid' => $result['admin_user']['frontend_uid']])->all();//这个用户有哪些身份
        
        $all_levels_ids = [];
        foreach($levels as $level){//这个用户有哪些身份
            $result['levels'][$level['studio_level_id']] = $level['studio_level_name'];
            $all_levels_ids[] = $level['studio_level_id'];
        }
        
        //身份有哪些类型 这个类型最高身份是什么   总的最高身份是什么
        
        $all = hy_studio_levelModel::baseGetDatas(['noget' => true,'pagesize' => 999,'id' => $all_levels_ids]);
        $result['max_levels'] = 999;
        $result['num_levels'] = [];
        foreach($all as $k => $v){
            $result['num_levels'][$v['type']] = $v['level'];
            if($v['level'] < $result['max_levels']){
                $result['max_levels'] = $v['level'];
            }
        }
        
        return $result;
    }
    
    public static function findAllParent($uid)
    {
        $result = [];
        
        while (true){
            $parent_id = static::findParent($uid);
            if($parent_id){
                $result[] = $parent_id;
                $uid = $parent_id;                
            }else{
                break;
            }

        }
        

        
        return $result;
    }
    
    public static function findAllXiaji($uid)
    {
        $result = [];
        
        while (true){
            $parent_id = static::findXiaji($uid);
            if($parent_id){
                $result = array_merge($result, $parent_id);
                $uid = $parent_id;                
            }else{
                break;
            }

        }
        return $result;
    } 
    
    //一级
    public static function findXiaji($uid)
    {
        return hy_designerModel::find()->where(['parentuid' => $uid])->select("uid")->column();
    }
    
    public static function findParent($uid){
        $member = static::getOne($uid, "parentuid");
        if($member){
           return $member['uid'];
        }else{
            return false;
        }   
    }
    
    //$can_uid 的上级用户是否是  $uid
    public static function isParent($uid,$can_uid)
    {
        if($uid == $can_uid){//本身就是自己的那就是允许操作
            return true;
        }
        while (true){
            $user = static::getOne($can_uid);
            if(!$user || $user['parentuid']==0){
                return false;
            }
            if($user['parentuid'] != $uid){
                $can_uid = $user['parentuid'];
                continue;
            }else{
                return true;
            }
        } 
    }
    
    //$mine_uid 为后台的UID
    //$you_uid 为前台的UID
    public static function is_shenfen($mine_uid,$you_uid)
    {
        $role_id = \backend\models\AdminMemberModel::getOne($mine_uid, "role_id");
        if($role_id == 1){
            return true;
        }
        //设置某一个人的上级
        $you_data = \common\models\hy_designerModel::createDesigner($you_uid, 0);
        $mine_data = \common\models\hy_designerModel::createDesigner($mine_uid, 1);
        if($mine_data['max_levels'] >= $you_data['max_levels']){//身份比设置的人低或同级则不允许设置
            return false;
        }else{
            return true;
        }
    }
    
    function getDesigner_level(){
    	return $this->hasMany(hy_designer_levelModel::className(), ['uid'=>'uid']);
    }     
    
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->with("designer_level");
        return $dataProvider;
    }
    //只要哪个身份的
    /*
        管理  1              1
        业务 2 6 10      2
        设计 3 7 11     3
        施工 4 8 12    4
        养护 5 9 13     5
        财务 14          6
     */
    public static function getAllForHtml($studio_level_arr)
    {
        $condition = [];
        $condition['noget'] = true;
        $condition['pagesize'] = 99999;
        $condition['alias'] = "main";
        
        $condition['leftJoin'] = ["table" => "yii_hy_designer_level l","on" => "main.uid = l.uid"];
        
        $studio_level_arr = implode(",", $studio_level_arr);
        $condition[] = "l.studio_level_id in({$studio_level_arr})";
        
        $condition['select'] = "main.uid uid,nickname,studio_level_name,studio_name";
        
        $datas = hy_designerModel::baseGetDatas($condition);
        $result = [];
        foreach($datas as $key => $value){
            $result[$value['uid']] = $value['studio_level_name'] . "--" . $value['nickname'] . "--工作室：" . $value['studio_name'];
        }
        return $result;
    }
}
