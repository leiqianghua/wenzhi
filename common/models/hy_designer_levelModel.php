<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_designer_level".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $username
 * @property integer $studio_level_id
 * @property integer $add_time
 */
class hy_designer_levelModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_designer_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'studio_level_id', 'add_time'], 'integer'],
            [['username'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'username' => 'Username',
            'studio_level_id' => 'Studio Level ID',
            'add_time' => 'Add Time',
        ];
    }
}
