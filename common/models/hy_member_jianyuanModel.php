<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_member_jianyuan".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $name
 * @property integer $sex
 * @property string $address_1
 * @property string $address_2
 * @property string $address_3
 * @property string $address_detail
 * @property integer $demand_type
 * @property integer $acreage
 * @property integer $space_type
 * @property string $element
 * @property string $style_type
 * @property integer $budget
 * @property integer $design_fee
 * @property integer $measure_fee
 * @property integer $complete_time
 * @property string $remark
 * @property string $add_time
 * @property integer $status
 */
class hy_member_jianyuanModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_jianyuan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['uid', 'sex', 'demand_type', 'acreage', 'space_type', 'budget', 'design_fee', 'measure_fee', 'complete_time', 'status'], 'integer'],
//            [['element'], 'string'],
//            [['name'], 'string', 'max' => 50],
//            [['address_1', 'address_2', 'address_3'], 'string', 'max' => 20],
//            [['address_detail'], 'string', 'max' => 200],
//            [['style_type', 'remark', 'add_time'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => '会员ID',
            'name' => '名字',
            'sex' => '性别',
            'address_1' => '省',
            'address_2' => '市',
            'address_3' => '县',
            'address_detail' => '具体地址',
            'demand_type' => '花园需求',
            'acreage' => '面积',
            'space_type' => '空间类型',
            'element' => '元素种类',
            'style_type' => '风格',
            'budget' => '预算',
            'design_fee' => '设计费',
            'measure_fee' => '测量费',
            'complete_time' => '完成周期',
            'remark' => '备注',
            'add_time' => '添加时间',
            'status' => '状态',
        ];
    }    
    public static  function getdemand_typeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '新建花园',
            '2' => '改造花园',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }      
    public static  function getacreageHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '20㎡以下',
            '2' => '20-50㎡',
            '3' => '50-100㎡',
            '4' => '100-300㎡',
            '5' => '300-500㎡',
            '6' => '500㎡以上',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }        
    //预算
    public static  function getbudgetHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '5万以下',
            '2' => '5-10万',
            '3' => '10-20万',
            '4' => '20-30万',
            '5' => '30-50万',
            '6' => '50-100万',
            '7' => '100万以上',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }           
    //设计费
    public static  function getdesign_feeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '50以下㎡',
            '2' => '80以下㎡',
            '3' => '150以下㎡',
            '4' => '300以下㎡',
            '5' => '500以下㎡',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }              
    //设计费
    public static  function getmeasure_feeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '收费',
            '2' => '免费',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }               
    //完成周期
    public static  function getcomplete_timeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '1个月内',
            '2' => '2个月内',
            '3' => '3个月内',
            '4' => '半年内',
            '5' => '1年内',
            '6' => '无限期',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }  
    //状态
//    public static  function getStatusHtml($mix_data = false)
//    {
//        $arr_data = [
//            '0' => '隐藏',
//            '1' => '显示',
//        ];
//        if($mix_data !== false){
//            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
//        }else{
//            return $arr_data;
//        }
//    }  
    //状态
    public static  function getsexHtml($mix_data = false)
    {
        $arr_data = [
            '0' => '未知',
            '1' => '男',
            '2' => '女',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
    public static  function getstyle_typeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '混搭',
            '2' => '自然',
            '4' => '其它',
            '3' => '现代',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
    
    
    function getspace_type(){
    	return $this->hasOne(hy_space_typeModel::className(), ['id'=>'space_type']);
    }     
    function getelement(){
    	return $this->hasOne(hy_space_type_detailModel::className(), ['id'=>'element']);
    }    
    function getRequest(){
    	return $this->hasOne(hy_member_requestModel::className(), ['uid'=>'uid']);
    }
    
    //对于我们搜索的时候先把这个我们要一起连接的先搜索出来。表
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->with("space_type");
        $dataProvider->query->with("element");
        $dataProvider->query->with("request");
        return $dataProvider;
    }    
}
