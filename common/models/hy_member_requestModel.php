<?php

namespace common\models;

use Yii;
use Exception;
/**
 * This is the model class for table "go_h_member_request".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $designer_id
 * @property integer $status
 * @property string $remark
 * @property integer $add_time
 */
class hy_member_requestModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'designer_id', 'status', 'add_time'], 'integer'],
            [['remark'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户ID',
            'designer_id' => '设计师ID',
            'status' => '状态',
            'remark' => '备注',
            'add_time' => '添加时间',
            'address' => '地址',
            'speed' => '进度',
            'money' => '项目金额',
            'cannel_reason' => '取消理由',
            'other_reason' => '其它理由',
            'design_img' => '设计图',
            'update_time' => '记录最后修改时间',
            'service_uid' => '分配的业务员',
            'speed_2_time' => '设计阶段开始时间',
            'speed_3_time' => '施工阶段开始时间',
            'speed_4_time' => '结束时间',
        ];
    }
    
    public static  function getStatusHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '服务中',
            '2' => '已关闭',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }    
    
    public static  function getcannel_reasonHtml($mix_data = false)
    {
        $arr_data = [
            '时间推迟' => '时间推迟',
            '财务问题' => '财务问题',
            '计划变动' => '计划变动',
            '物业问题' => '物业问题',
            '家人原因' => '家人原因',
            '其他原因' => '其他原因',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
    
    

    //对于我们搜索的时候先把这个我们要一起连接的先搜索出来。表
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->with("user");
        
        if(\yii::$app->controller->user->isSuper()){
            return $dataProvider;
        }else{
            $mine_uid = \yii::$app->controller->user->frontend_uid;
            $dataProvider->query->leftJoin("yii_hy_member_request_use use","use.request_id=main.id");
            $dataProvider->query->andWhere("main.service_uid={$mine_uid} or use.uid={$mine_uid}");
            return $dataProvider;
        }
    }
    
    function getUser(){
    	return $this->hasOne(MemberModel::className(), ['uid'=>'uid']);
    }     
    
    function getDesigner(){
    	return $this->hasOne(hy_designerModel::className(), ['uid'=>'designer_id']);
    }
    
    public static  function getSpeedHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '服务阶段',
            '2' => '方案阶段',
            '3' => '施工阶段',
            '4' => '完成',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
    
    
    public static function operationRequest($id,$operation_data,$operation_uid)
    {
        try {
            $request_id = \common\components\GuestInfo::getParam("id");
            $request = hy_member_requestModel::findOne($request_id);
            if(!$request){
                throw new Exception("找不到相关的项目需求");
            } 
            
            if($request['status'] != 1){
                throw new Exception("项目已经取消");
            }
            
            //完成工程进度
            if(!empty($operation_data['speed'])){
                if($operation_data['speed'] == 3){//设计到施工
                    if($request['speed'] != 2){
                        throw new Exception("操作失败");
                    }else{
                        
                        //查询是否是所有的工作都已经完成了呢
                        $is_data = hy_member_request_dataModel::find()->where(["over_time" => 0,"request_id" => $id,"type" => 2])->limit(1)->one();
                        if(!$is_data){
                            throw new Exception("还有未完成的工作,不允许完成此阶段");
                        }
                        
                        
                        $request->speed = 3;
                        $request->speed_3_time = time();
                    }
                }else if($operation_data['speed'] == 4){//施工到完成
                    if($request['speed'] != 3){
                        throw new Exception("操作失败");
                    }else{
                        
                        //查询是否是所有的工作都已经完成了呢
                        $is_data = hy_member_request_dataModel::find()->where(["over_time" => 0,"request_id" => $id,"type" => 3])->limit(1)->one();
                        if(!$is_data){
                            throw new Exception("还有未完成的工作,不允许完成此阶段");
                        }
                        $request->speed = 4;
                        $request->speed_4_time = time();
                    }                    
                }
                $request->save(false);
                return true;
            }
            
            //设置金额
//            if(!empty($operation_data['money'])){
//                $request->money = $operation_data['money'] + 0;
//                $request->save(false);
//                return true;
//            }
            
            //设置金额
            if(!empty($operation_data['money_1'])){
                $request->money_1 = $operation_data['money_1'] + 0;
                $request->money_2 = $operation_data['money_2'] + 0;
                $request->save(false);
                return true;
            }
            
            //取消项目
            if(!empty($operation_data['status']) && $operation_data['status'] == 2){
                $request->status = 2;
                $request->save(false);
                return true;
            }
            
            //设置成这个业务员了
            if(!empty($operation_data['service_uid'])){
                
                //如果是管理员才可以
                $uu = hy_designerModel::createDesigner(\yii::$app->controller->user->uid, false);
                if($uu['max_levels'] != 1){
                    throw new Exception("只允许管理员设置");
                }
                
                
                $user = hy_designerModel::findOne($operation_data['service_uid']);
                if(!$user){
                    throw new Exception("找不到此员工信息");
                }
                $request->service_uid = $operation_data['service_uid'];
                $request->save(false);
                return true;                
            }
            
        } catch (Exception $exc) {
            static::$error = $exc->getMessage();
            return false;
        }
    }
    
    //分配给哪些业务员操作的一个功能
    public static function canFenPeiUser($id)
    {
        $request = hy_member_requestModel::findOne($id);
        $studio_id = \common\models\hy_designerModel::getOne($request['service_uid'], "studio_id");//团队名称

        $condition = [];
        if($studio_id){
            $condition[] = "designer.studio_id = {$studio_id}" ;
        }
        $condition['noget'] = true;
        $condition['pagesize'] = 9999;
        $condition['alias'] = "designer";
        $condition['leftJoin'] = [
            'table' => "yii_hy_designer_level level",
            "on" => "level.uid=designer.uid",
        ];
        $condition[] = "level.studio_level_id in(2,6,10)";
        $datas = \common\models\hy_designerModel::baseGetDatas($condition);
        $user = [];
        foreach($datas as $key => $value){
            $arr = [];
            $arr['value'] = $value['uid'];
            $arr['label'] = $value['nickname'];
            //$user[$value['uid']] = $value['nickname'];
            $user[] = $arr;
        }

        return $user;
    }
    
    
    
    //分配给哪些业务员操作的一个功能
    public static function canDoUser($request_id)
    {
        $studio_id = hy_designerModel::getOne(hy_member_requestModel::getOne($request_id, "service_uid"), "studio_id");
        
        $datas = \common\models\hy_member_request_useModel::getBaseKeyValue("uid", "username", ['request_id' => $request_id]);//本身有谁了
        
        $uids_key = array_keys($datas);
        $str_uids = implode(",", $uids_key);
        
        $condition = [];
        $condition['noget'] = true;
        $condition['studio_id'] = $studio_id;
        if($str_uids){
            $condition[] = "uid not in({$str_uids})";
        }
        
        $condition['select'] = "uid,nickname";
        $user = hy_designerModel::baseGetDatas($condition);
        return $user;
    }    
    
    
    
     
}
