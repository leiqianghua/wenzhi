<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yii_hy_member_request_curing".
 *
 * @property integer $id
 * @property integer $request_id
 * @property string $title
 * @property integer $add_time
 * @property integer $notice_day_num
 * @property integer $is_need
 * @property integer $next_notice_time
 * @property integer $is_notice_mine
 * @property integer $next_notice_mine_time
 */
class hy_member_request_curingModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request_curing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => '项目ID',
            'title' => '标题',
            'add_time' => '添加时间',
            'notice_day_num' => '通知一次养护时间（单元天）',
            'is_need' => '用户是否有选此需求',
            'next_notice_time' => '下一次通知的时间',
            'is_notice_mine' => '通知工作人员隔的天数',
            'next_notice_mine_time' => '下一次通知工作人员时间',
        ];
    }
}
