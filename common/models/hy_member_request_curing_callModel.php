<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yii_hy_member_request_curing_call".
 *
 * @property integer $id
 * @property integer $request_id
 * @property string $curing_ids
 * @property integer $add_time
 * @property integer $work_uid
 * @property integer $is_docking
 */
class hy_member_request_curing_callModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request_curing_call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'add_time', 'work_uid', 'is_docking'], 'integer'],
            [['curing_ids'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => '需求ID',
            'curing_ids' => '请求项',
            'add_time' => '添加时间',
            'work_uid' => '安排对接的UID',
            'is_docking' => '是否有对接',
        ];
    }
}
