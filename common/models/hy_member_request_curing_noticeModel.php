<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yii_hy_member_request_curing_notice".
 *
 * @property integer $id
 * @property integer $request_id
 * @property string $curing_ids
 * @property integer $add_time
 * @property integer $notice_type
 */
class hy_member_request_curing_noticeModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request_curing_notice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'add_time', 'notice_type'], 'integer'],
            [['curing_ids'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => '项目ID',
            'curing_ids' => '通知养护项',
            'add_time' => '添加时间',
            'notice_type' => '通知端',
        ];
    }
    
    public static  function getnotice_typeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '通知业主',
            '2' => '通知工作人员',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }
}
