<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yii_hy_member_request_curing_record".
 *
 * @property integer $id
 * @property integer $curing_ids
 * @property integer $request_id
 * @property string $images
 * @property string $title
 * @property integer $add_time
 * @property integer $work_uid
 */
class hy_member_request_curing_recordModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request_curing_record';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['curing_ids', 'request_id', 'add_time', 'work_uid'], 'integer'],
            [['images'], 'string'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'curing_ids' => '项目类',
            'request_id' => '需求ID',
            'images' => '图片组',
            'title' => '标题',
            'add_time' => '添加时间',
            'work_uid' => '创建用户ID',
        ];
    }
}
