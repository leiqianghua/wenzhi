<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_member_request_construct_data".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $title
 * @property integer $add_time
 * @property integer $sel_uid
 * @property integer $request_id
 */
class hy_member_request_dataModel extends \common\models\BaseCategory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'add_time', 'sel_uid', 'request_id'], 'integer'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => '类型ID',
            'title' => '标题名称',
            'add_time' => '添加时间',
            'sel_uid' => '创建用户ID',
            'request_id' => '需求ID',
        ];
    }
    
    public static function add_Field(&$datas)
    {
        $datas['sel_nickname'] = \common\models\hy_designerModel::getOne($datas['sel_uid'], "nickname");
        $datas['type_name'] = \common\models\hy_categoryModel::getTypeHtml($datas['type']);
    }


    
    public function beforeSave($insert)
    {
        return true;
    }
    
    public function afterSave($insert, $changedAttributes) {
        if(\common\components\GuestInfo::getParam("shezhi_d") == 1){
            if($this->parent_id){
                $is_data = hy_member_request_dataModel::find()->where(['parent_id' => $this->parent_id,"request_id" => $this->request_id,"over_time" => 0])->limit(1)->one();
                if(!$is_data){
                    $model = hy_member_request_dataModel::find()->where(['type_id' => $this->parent_id,"request_id" => $this->request_id])->limit(1)->one();
                    if($model){
                        $model->over_time = time();
                        $model->save(false);
                    }
                }
            }
            return parent::afterSave($insert, $changedAttributes);
        }else{
            return parent::afterSave($insert, $changedAttributes);
        }
    }
}
