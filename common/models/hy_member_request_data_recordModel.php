<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_member_request_construct_data_record".
 *
 * @property integer $id
 * @property integer $data_id
 * @property integer $do_uid
 * @property string $images
 * @property string $title
 * @property integer $add_time
 */
class hy_member_request_data_recordModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request_data_record';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_id', 'do_uid', 'add_time'], 'integer'],
            [['images'], 'string'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_id' => '类型ID',
            'do_uid' => '上传的UID',
            'images' => '图',
            'title' => '标题',
            'add_time' => '上传的时间',
        ];
    }
}
