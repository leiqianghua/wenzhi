<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_member_request_pay_record".
 *
 * @property string $ordresn
 * @property integer $re_id
 * @property integer $title
 * @property integer $money
 * @property integer $status
 * @property integer $add_time
 * @property integer $update_time
 * @property string $pay_type
 * @property string $scookies
 * @property string $consume_type
 * @property string $trade_no
 */
class hy_member_request_pay_recordModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request_pay_record';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordresn'], 'required'],
            [['scookies'], 'string'],
            [['ordresn', 'pay_type', 'consume_type', 'trade_no'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ordresn' => '订单',
            're_id' => '哪一个事项需求',
            'title' => '付款事项',
            'money' => '金额',
            'status' => '状态',
            'add_time' => '添加时间',
            'update_time' => '修改时间',
            'pay_type' => '充值的途径',
            'scookies' => '充值相关信息',
            'consume_type' => '充值类型',
            'trade_no' => '第三号交易号',
            'pay_mine_type' => '付款针对类型',
            'relation_id' => '关联ID',
            'is_underline_payment' => "是否线下支付",
            'underline_payment_mark' => "线下支付备注",
            'underline_payment_img' => "线下支付凭证图",
        ];
    }
    
    
    /**
     * 创建充值订单
     * @param type $userid  哪个用户
     * @param type $money   充值多少钱
     * @param type $scookies 充值的其它的附带的数据  一般充值后可能还会使用到
     * @param type $consume_type  充值的类型  比如 普通充值  购买充值等此类型
     * @param type $red_ids  充值的其它的优惠是否是有使用  附带的相关的信息
     * @return string
     */
    public static function rechargePay($money,$title,$request_id,$consume_type = 1, $pay_mine_type = '',$scookies = '') 
    {
        $data = [];
        $data['ordresn'] = \common\components\Tool::createOrderID('PY');//订单号
        $data['request_id'] = $request_id;
        $data['pay_uid'] = hy_member_requestModel::getOne($request_id, "uid");
        $data['title'] = $title;
        $data['money'] = $money;
        $data['consume_type'] = $consume_type;
        $data['status'] = 0;
        $data['pay_type'] = "";//充值途径 回调的时候才知道 具体的去处理
        $data['scookies'] = $scookies;
        $data['consume_type'] = $consume_type;//充值类型
        $data['trade_no'] = "";//第三方交易号
        $data['pay_mine_type'] = $pay_mine_type;//第三方交易号
        $model = new hy_member_request_pay_recordModel();
        $model->attributes = $data;
        $model->save(false);
        
        
        return $model;
    } 
    
    public static  function getStatusHtml($mix_data = false)
    {
        $arr_data = [
            '0' => '未付款',
            '1' => '已付款',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }   
    
    public static  function getis_underline_paymentHtml($mix_data = false)
    {
        $arr_data = [
            '0' => '未确认支付',
            '1' => '线下支付正在核实',
            '2' => '已完成线下支付',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }    
    
    public static  function getpay_mine_typeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '设计',
            '2' => '施工',
            //'3' => '尾款',
            '4' => '增减项',
            '5' => '其它',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }   
}
