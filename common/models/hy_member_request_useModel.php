<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_member_request_use".
 *
 * @property integer $id
 * @property integer $request_id
 * @property integer $uid
 * @property string $username
 * @property integer $add_time
 */
class hy_member_request_useModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_member_request_use';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'uid', 'add_time'], 'integer'],
            [['username'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => '需求ID',
            'uid' => '用户ID',
            'username' => '用户名',
            'add_time' => '添加时间',
        ];
    }
}
