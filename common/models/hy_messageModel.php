<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_message".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $title
 * @property string $content
 * @property integer $add_time
 */
class hy_messageModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['uid', 'add_time'], 'integer'],
//            [['title'], 'string', 'max' => 50],
//            [['content'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'title' => '标题',
            'content' => '内容',
            'add_time' => '添加时间',
        ];
    }
    //增加相关的数据  什么情况下会给用户发消息的时候，则调用这个发消息的来。
    public static function addMessage($uid,$title,$content)
    {
        $model = new hy_messageModel();
        $datas = [];
        $datas['uid'] = $uid;
        $datas['title'] = $title;
        $datas['content'] = $content;
        $model->attributes = $datas;
        $model->save(false);
        return $model;
    }
    
    
}
