<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_space_type".
 *
 * @property integer $id
 * @property string $title
 * @property string $english
 * @property string $img
 * @property integer $add_time
 * @property integer $sorting
 */
class hy_space_typeModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_space_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['add_time', 'sorting'], 'integer'],
//            [['title', 'english'], 'string', 'max' => 50],
//            [['img'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'english' => '英文',
            'img' => '图',
            'add_time' => '添加时间',
            'sorting' => '排序',
        ];
    }
}
