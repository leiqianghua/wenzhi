<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_space_type_detail".
 *
 * @property integer $id
 * @property string $img
 * @property string $title
 * @property integer $add_time
 * @property integer $sorting
 * @property integer $type_id
 */
class hy_space_type_detailModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_space_type_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['add_time', 'sorting', 'type_id'], 'integer'],
//            [['img'], 'string', 'max' => 200],
//            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => '图',
            'sel_img' => '选中图',
            'title' => '标题',
            'add_time' => '添加时间',
            'sorting' => '排序',
            'type_id' => '空间类型',
        ];
    }
}
