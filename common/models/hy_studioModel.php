<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_studio".
 *
 * @property integer $id
 * @property string $name
 * @property integer $add_time
 * @property integer $founder_uid
 * @property string $founder_name
 * @property string $introduction
 * @property string $img
 * @property string $city
 */
class hy_studioModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_studio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['add_time', 'founder_uid'], 'integer'],
//            [['name', 'founder_name', 'city'], 'string', 'max' => 50],
//            [['introduction'], 'string', 'max' => 1000],
//            [['img'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '名称',
            'add_time' => '添加时间',
            'founder_uid' => '创始ID',
            'founder_name' => '创始人',
            'introduction' => '创始人',
            'img' => '主图',
            'city' => '所属城市',
        ];
    }
}
