<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_h_studio_level".
 *
 * @property integer $id
 * @property string $title
 * @property integer $level
 * @property integer $type
 * @property integer $add_time
 */
class hy_studio_levelModel extends \common\components\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_hy_studio_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'level', 'type', 'add_time'], 'integer'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '名称',
            'level' => '层级',
            'type' => '类型',
            'add_time' => '添加时间',
        ];
    }
    
    /**
     * 求出你上级允许的h_studio_level ID有哪些
     * @param type $type  你的类型是什么
     * @param type $level 你的身份是什么
     * @return type
     */
    public static function getOkTypeForType($level,$type = "")
    {
        switch ($level) {
            case 1:
                return [];
            case 2:
                return [1];
            case 3:
                if($type == 2){
                    return [1,2];
                }else if($type == 3){
                    return [1,3];
                }else if($type == 4){
                    return [1,4];
                }else if($type == 5){
                    return [1,5];
                }
            case 4:
                if($type == 2){
                    return [1,2,6];
                }else if($type == 3){
                    return [1,3,7];
                }else if($type == 4){
                    return [1,4,8];
                }else if($type == 5){
                    return [1,5,9];
                }
        }
    }
    

    
    public static  function getTypeHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '管理员',
            '2' => '业务',
            '3' => '设计',
            '4' => '施工',
            '5' => '养护',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }        
    public static  function getlevelHtml($mix_data = false)
    {
        $arr_data = [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
        ];
        if($mix_data !== false){
            return isset($arr_data[$mix_data]) ? $arr_data[$mix_data] : $mix_data;
        }else{
            return $arr_data;
        }
    }    
}
