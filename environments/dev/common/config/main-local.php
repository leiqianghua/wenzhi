<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=118.190.175.80;dbname=jiaju',
            'username' => 'root',
            'password' => '631222',
            'charset' => 'utf8',
            'tablePrefix' => 'yii_',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'redis' => [
            'class' => 'common\components\RedisYii2',
            'host' => '127.0.0.1',
            'port' => '6379',
        ],
        'cache' => [
            'class' => 'yii\caching\DummyCache',
//            'servers' => [
//                [
//                    'host' => '127.0.0.1',
//                    'port' => 11211,
//                    'weight' => 100,
//                ],
//            ],
        ],
    ],
];
