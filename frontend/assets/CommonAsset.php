<?php
/**
 * 错误页面 与  正确页面 会用到
 */
namespace backend\assets;
class CommonAsset extends \yii\web\AssetBundle
{
    public $basePath = "@webroot/common";
    public $baseUrl = "@web/common";
    
    public $css = [
        'css/style.css',
    ];  
    
    public $js = [
        
    ];
}

