<?php
//各应用都需要继承的控制器基类。  包括各增删改查基类控制器
namespace frontend\components;


use yii;
//use common\components\BaseController;
class BaseController extends \common\components\BaseController 
{
    public function init()
    {
        //存储相关的code
        $exten_code = \common\components\GuestInfo::getParam('exten_code', '');
        if($exten_code){
            setcookie('exten_code', $exten_code, time()+86400, '/');   
        }
        
        if (\common\components\GuestInfo::isMobileRequest()) {
            $url = $this->to('/wap/index/index');
            header("Location: {$url}");
            exit;
        }        
        //是否是手机的访问，如果是的话有些页面则跳到相应的其它的页面
        parent::init();
    }
}