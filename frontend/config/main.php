<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => '古笆免商城',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'hy/public/hy',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'class' => 'frontend\modules\wap\components\Member',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'public/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
    ],
    'params' => $params,
    
    'modules' => [
        'wap' => [
            'class' => 'frontend\modules\wap\WapModule',
            'components' => [
                'user' => [
                    'class' => 'frontend\modules\wap\components\Member',
                ],               
            ]
        ],
        'hy' => [
            'class' => 'frontend\modules\hy\HyModule',
            'components' => [
                'user' => [
                    'class' => 'frontend\modules\hy\components\Member',
                ],               
            ]
        ],
    ],  
];
