<?php

namespace frontend\modules\wap\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/hy';
    public $baseUrl = '@web/hy';
    public $css = [
        'common.css',
    ];
    public $js = [
        'js/layer.m/layer.m.js',
        'js/main.js',
    ];
//    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//    ];
}
