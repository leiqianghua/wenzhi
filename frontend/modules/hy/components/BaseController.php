<?php
//各应用都需要继承的控制器基类。  包括各增删改查基类控制器
namespace frontend\modules\hy\components;


use yii;
//use common\components\BaseController;
class BaseController extends \common\components\BaseController 
{
    public $title;
    public function init()
    {
        parent::init();
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:OPTIONS, GET, POST'); // 允许option，get，post请求
        header('Access-Control-Allow-Headers:x-requested-with'); // 允许x-requested-with请求头    
        header('Access-Control-Allow-Headers:token');
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'options') {
            exit;
        }        
        $this->layout = false;
        $this->user = yii::$app->modules['hy']->user;
        $this->user->isLogin();
    }
    
    public function ajaxReturn($arr = [])
    {
        if(is_string($arr)){
            $info = $arr;
            $arr = ['msg' => $info];
        }
        if(!isset($arr['code'])){
            $arr['code'] = 1;
        }
            
        exit(json_encode($arr));  
    }    
    
    //便捷的跳转数据
    public function success($arr = [], $url = '', $is_ajax = true)
    {
        $is_ajax = true;
        
        if(is_string($arr)){
            $info = $arr;
            $arr = ['msg' => $info];
        }
        $data = ['code' => 1, 'msg' => '处理成功'];
//        $url == '' ? $url = Yii::$app->request->getReferrer() : $url;
//        empty($url) ? $url = \yii\helpers\Url::to(['index/index']) : '';
//        $data['url'] = $url;
        $data = array_merge($data,$arr);
        if($is_ajax){
            exit(json_encode($data));  
        }else{
            empty($data['wait_second']) ? $data['wait_second'] = 3 : '';
            $content = $this->renderPartial("@app/views/common/success", $data);
            exit($content);
        }      
    }      
    //便捷的跳转数据
    public function error($arr = [],  $url = '',$is_ajax = true)
    {
        if(yii::$app->request->isAjax){
            $is_ajax = true;
        }
        
        if(is_string($arr) || empty($arr)){
            $info = empty($arr) ? '处理失败' : $arr;
            $arr = ['msg' => $info];
        }
        $data = ['code' => 0, 'msg' => '处理失败'];
        if($url){
            $data['url'] = $this->to($url);
        }else{
            $data['url'] = '';
        }
        
        
        $data = array_merge($data,$arr);
        if($is_ajax){
            exit(json_encode($data));  
        }else{
            empty($data['wait_second']) ? $data['wait_second'] = 3000 : '';
            $content = $this->renderPartial("@app/views/common/error", $data);
            exit($content);
        }   
    }
}
