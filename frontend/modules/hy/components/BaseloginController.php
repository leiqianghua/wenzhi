<?php
//各应用都需要继承的控制器基类。  包括各增删改查基类控制器
namespace frontend\modules\hy\components;


use yii;
//use common\components\BaseController;
class BaseloginController extends BaseController 
{
    public function init()
    {
        //return true;
        parent::init();
        if(!$this->user->isLogin()){
            $this->error(['code' => -1,"msg" => "您未登录",'data' => ['url' => "/wap/public/login?type_new=hy"]]);
        }        
    }
}