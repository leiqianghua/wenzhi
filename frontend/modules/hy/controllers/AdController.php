<?php

namespace frontend\modules\hy\controllers;
class AdController extends \frontend\modules\hy\components\BaseloginController
{
    public function actionIndex()
    {
        $post_alias = \common\components\GuestInfo::getParam("type");
        $datas = \common\models\AdListWapModel::getList($post_alias, false);
        
        
        foreach($datas as $key => $value){
            $datas[$key] = $value;
        }
        
        $this->success(['data' => $datas]);   
    }
}
