<?php

namespace frontend\modules\hy\controllers;
class ArticleController extends \frontend\modules\hy\components\BaseloginController
{
    public function actionIndex()
    {
        $condition['status'] = 1;
        $datas = \common\models\hy_articleModel::baseGetDatas($condition);
        
                
        foreach($datas as $key => $value){
            $datas[$key] = $value;
        }
        
        $this->success(['data' => $datas]);  
    }
    
    public function actionDetail()
    {
        $data = \common\models\hy_articleModel::getOne(\common\components\GuestInfo::getParam("id"));
        if(!$data){
            $this->error("找不到相关的数据");
        }
        
        
        $this->success(['data' => $data]);
    }
}
