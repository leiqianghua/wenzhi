<?php

namespace frontend\modules\hy\controllers;
class CaseController extends \frontend\modules\hy\components\BaseloginController
{
    public function actionIndex()
    {
        $condition['status'] = 1;
        $condition['orderBy'] = "sorting desc,id desc";
        $datas = \common\models\hy_caseModel::baseGetDatas($condition);
        
        
        foreach($datas as $key => $value){
            $datas[$key]['content_imgs'] = json_decode($value['content_imgs'],true);
            $datas[$key]['zaojia_name'] = \common\models\hy_caseModel::getzaojiaHtml($value['zaojia']);
            $datas[$key]['fenge_name'] = \common\models\hy_caseModel::getfengeHtml($value['fenge']);
            $datas[$key]['type_name'] = \common\models\hy_caseModel::gettypeHtml($value['type']);
        }
        
        $this->success(['data' => $datas]);
    }
    
    public function actionDetail()
    {
        $data = \common\models\hy_caseModel::getOne(\common\components\GuestInfo::getParam("id"));
        if(!$data){
            $this->error("找不到相关的数据");
        }
        
        $data['content_imgs'] = json_decode($data['content_imgs'],true);
        $data['zaojia_name'] = \common\models\hy_caseModel::getzaojiaHtml($data['zaojia']);
        $data['fenge_name'] = \common\models\hy_caseModel::getfengeHtml($data['fenge']);
        $data['type_name'] = \common\models\hy_caseModel::gettypeHtml($data['type']);        
        $is_data = \common\models\hy_collectionModel::findOne(['case_id' => \common\components\GuestInfo::getParam("id"),"uid" => $this->user->uid]);
        if($is_data){
            $data['is_connect'] = 1;
        }else{
            $data['is_connect'] = 0;
        }        
        
        $this->success(['data' => $data]);
    }
}
