<?php

namespace frontend\modules\hy\controllers;
class CollectionController extends \frontend\modules\hy\components\BaseloginController
{
    public function actionIndex()
    {
        $condition = [];
        $condition['uid'] = $this->user->uid;
        $result = \common\models\hy_collectionModel::baseGetDatas($condition);
        
        
        $case_arr = [];
        foreach($result as $key => $value){
            $case_arr[] = $value['case_id'];
        }
        
        $condition = [];
        $condition['noget'] = true;
        $condition['pagesize'] = 99999;
        $condition['id'] = $case_arr;
        $result = \common\models\hy_caseModel::baseGetDatas($condition);
        
        $datas = [];
        foreach($result as $key => $value){
            $datas[$key] = $value;
            
            $datas[$key]['content_imgs'] = json_decode($value['content_imgs'],true);
            $datas[$key]['zaojia_name'] = \common\models\hy_caseModel::getzaojiaHtml($value['zaojia']);
            $datas[$key]['fenge_name'] = \common\models\hy_caseModel::getfengeHtml($value['fenge']);
            $datas[$key]['type_name'] = \common\models\hy_caseModel::gettypeHtml($value['type']);            
        }        
        
        
        $this->success(['data' => $datas]);            
    }
    
    
    public function actionUpdate()
    {
        $id = \common\components\GuestInfo::getParam("id");
        $status = \common\components\GuestInfo::getParam("status");
        
        if($status == 1){//要收藏
            $model = \common\models\hy_collectionModel::findOne(['uid' => $this->user->uid,"case_id" => $id]);
            if($model){
                $this->error("已经收藏过");
            }else{
                $m = new \common\models\hy_collectionModel();
                $m->uid = $this->user->uid;
                $m->case_id = $id;
                $m->save(false);
                $this->success("收藏成功");
            }
        }else{//要取消收藏
            $model = \common\models\hy_collectionModel::findOne(['uid' => $this->user->uid,"case_id" => $id]);
            if($model){
                $model->delete();
                $this->success("取消成功");
            }else{
                $this->error("未收藏");
            }            
        }
    }
}
