<?php

namespace frontend\modules\hy\controllers;
class MemberController extends \frontend\modules\hy\components\BaseloginController
{
    public function actionDetail()
    {
        $data = $this->user->getMember();
        unset($data['password']);
        $data_1 = \common\models\hy_designerModel::getOne($this->user->uid);
        if($data_1){
            $data = array_merge($data, $data_1);
        }
        
        //各种相关的数量需要提供的. 收藏数量和消息数量
        //养护列表
        //正在呼叫数量
        
//            $num_1 = 0;//跟进中
//            $num_2 = 0;//设计中
//            $num_3 = 0;//施工中
//            $num_4 = 0;//请款中
//            $num_5 = 0;//正在呼叫        
        
        
        
        $collect = \common\models\hy_collectionModel::find()->where(['uid' => $this->user->uid])->count();
        $message = \common\models\hy_messageModel::find()->where(['uid' => $this->user->uid,'status' => 0])->count();
        
        
        
        if($data_1){
            
            $user_detail = \common\models\hy_designerModel::createDesigner($this->user->uid, false);
            $level_keys = array_keys($user_detail['levels']);
            $data['role_id'] = implode(',', $level_keys);
        }else{
            $data['role_id'] = "0";
        }        
        
        
        //你是属于哪个角色？？？？？  但是某一个人可能是会有多个角色的，我们那边是如何的来分的呢
        if($data_1){//是员工
            
            $num_1 = \common\models\hy_member_requestModel::find()->where(['service_uid' => $this->user->uid,'speed' => 1])->count();//还在服务阶段的此人跟进的
            
            
            if(!empty($user_detail['num_levels'][3])){//是设计师
                $uid = $this->user->uid;
                $condition = [];
                $condition['noget'] = true;
                $condition['gettype'] = 0;
                $condition['status'] = 1;
                $condition['alias'] = "main";
                $condition[] = "speed = 2";//设计中
                $condition['leftJoin'] = ['table' => "yii_hy_member_request_use use","on" => "use.uid = main.uid"];
                $condition[] = "use.uid={$uid}";
                $project_num = \common\models\hy_member_requestModel::baseGetDatas($condition);
                $num_2 = (string)count($project_num);                
            }else{
                $num_2 = "0";
            }
            
            
            if(!empty($user_detail['num_levels'][4])){//是造景师
                $uid = $this->user->uid;
                $condition = [];
                $condition['noget'] = true;
                $condition['gettype'] = 0;
                $condition['status'] = 1;
                $condition['alias'] = "main";
                $condition[] = "speed = 3";//施工中
                $condition['leftJoin'] = ['table' => "yii_hy_member_request_use use","on" => "use.uid = main.uid"];
                $condition[] = "use.uid={$uid}";
                $project_num = \common\models\hy_member_requestModel::baseGetDatas($condition);
                $num_3 = (string)count($project_num);                
            }else{
                $num_3 = "0";
            } 
            
            
            //请款中  说明需要有多少要打款的
            if(!empty($user_detail['num_levels'][6])){//是财务
                $condition = [];
                $condition['gettype'] = 0;
                $condition['alias'] = "main";
                $condition['leftJoin'] = ['table' => "yii_hy_member_request_use u","on" => "u.request_id = main.request_id"];

                $condition[] = "main.status=0 and is_underline_payment=1 and u.uid={$uid}";

                $caiwu_num = \common\models\hy_member_request_pay_recordModel::baseGetDatas($condition);    
                $num_4 = (string)count($caiwu_num);                          
            }else{
                $num_4 = "0";
            } 
            

            
            $condition = [];
            $condition['work_uid'] = $this->user->uid;
            $condition['is_docking'] = 0;
            $condition['gettype'] = 0;
            $int_yanghu_num = \common\models\hy_member_request_curing_callModel::baseGetDatas($condition);     
            $num_5 = (string)count($int_yanghu_num);
            


            
        }else{//是普通会员
            $num_1 = "0";//跟进中
            $num_2 = "0";//设计中
            $num_3 = "0";//施工中
            $num_4 = "0";//请款中
            $num_5 = "0";//正在呼叫
            $xiangmu = \common\models\hy_member_requestModel::find()->where(['uid' => $this->user->uid])->orderBy("id desc")->limit(1)->one();
            if($xiangmu){
                $xiangmu = $xiangmu->attributes;
            }
        }
        
        $data['num'] = [
            'collect' => $collect,
            'message' => $message,
            'num_1' => $num_1,
            'num_2' => $num_2,
            'num_3' => $num_3,
            'num_4' => $num_4,
            'num_5' => $num_5,
        ];
        if(empty($xiangmu)){
            $xiangmu = [];
        }
        $data['mine_xiangmu'] = $xiangmu;
     
        
        $this->success(['data' => $data]);
    }
}
