<?php

namespace frontend\modules\hy\controllers;
class Member_jianyuanController extends \frontend\modules\hy\components\BaseloginController
{
    //用户创建需求
    public function actionCreate_or_update()
    {
        $obj = new \common\components\newfs\CreateUseRquest();
        $obj->uid = $this->user->uid;
        
        //$d = \common\models\hy_designerModel::getOne($this->user->uid);
        
        
        $obj->params = $_POST;
        $state = $obj->run();
        if($state){
            if($state == 2){
                $this->success("你的需求已经创建,请等待工作人员与您联系");
            }else{
                $this->success("处理成功");
            }
            
        }else{
            $this->error($obj->error);
        }
    }
    
    
    protected function _get_data($data)
    {
        $result = [];
        foreach($data as $key => $value){
            $result[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $result;
    }




    //查看用户需求
    public function actionShow()
    {
        
        $config = [
            "sex" => $this->_get_data(\common\models\hy_member_jianyuanModel::getsexHtml()),
            "demand_type" => $this->_get_data(\common\models\hy_member_jianyuanModel::getdemand_typeHtml()),
            "acreage" => $this->_get_data(\common\models\hy_member_jianyuanModel::getacreageHtml()),
            "space_type" => $this->_get_data(\common\models\hy_space_typeModel::find()->limit(9999)->asArray()->all()),
            "element" => $this->_get_data(\common\models\hy_space_type_detailModel::find()->limit(99999)->asArray()->all()),
            "style_type" => $this->_get_data(\common\models\hy_member_jianyuanModel::getstyle_typeHtml()),
            "budget" => $this->_get_data(\common\models\hy_member_jianyuanModel::getbudgetHtml()),
            "design_fee" => $this->_get_data(\common\models\hy_member_jianyuanModel::getdesign_feeHtml()),
            "measure_fee" => $this->_get_data(\common\models\hy_member_jianyuanModel::getmeasure_feeHtml()),
            "complete_time" => $this->_get_data(\common\models\hy_member_jianyuanModel::getcomplete_timeHtml()),
        ];
        
        $model = \common\models\hy_member_jianyuanModel::findOne($this->user->uid);
        if(!$model){
            $data = [];
        }else{
            $data = $model->attributes;
        }
        
        $this->success(['data' => ['data' => $data,'config' => $config]]);
    }
    
    
    
    //针对需求进行相应的修改 进入某个阶段  speed
    public function actionUpdate_request()
    {
        $operation_data = array_merge($_GET, $_POST);//具体的操作
        
        $state = \common\models\hy_member_requestModel::operationRequest(\common\components\GuestInfo::getParam("id"), $operation_data, $this->user->uid);
        if($state){
            $this->success("处理成功");
        }else{
            $this->error(\common\models\hy_member_requestModel::$error);
        }
    }
    
    
    //这个项目我可以指定的人有哪些
    public function actionCan_feipai_user()
    {
        $user = \common\models\hy_member_requestModel::canFenPeiUser(\common\components\GuestInfo::getParam("request_id"));
        $this->success(['data' => $user]);
    }
    
    
    //这个项目可以拉哪些人进来
    public function actionCan_do_user()
    {
        $user = \common\models\hy_member_requestModel::canDoUser(\common\components\GuestInfo::getParam("request_id"));
        $this->success(['data' => $user]);
    }   
    
    
    //拉人
    public function actionUser_operation()
    {
        
        $uid = \common\components\GuestInfo::getParam("uid");
        $request_id = \common\components\GuestInfo::getParam("request_id");    
        $type = \common\components\GuestInfo::getParam("type");  
        
        if($type == 2){
            if($this->user->uid != \common\models\hy_member_requestModel::getOne($request_id, "service_uid")){
            
                $mine = \common\models\hy_designerModel::createDesigner($this->user->uid, false);

                if($mine['max_levels'] != 1){
                    $this->error("不允许删除");
                }                
            }
        }
        
        $obj = new \common\components\newfs\CreateUpdateRequest_use();
        if(!is_array($uid)){
            $kid = $uid;
            $uid = [];
            $uid[] = $kid;
        }
        $obj->uids = $uid;
        $obj->type = $type;
        $obj->request_id = $request_id;
        $state = $obj->run();
        if(!$state){
            $this->error($obj->error);
        }else{
            $this->success("处理成功");
        }        
    }
    
    public function actionGet_user()
    {
        $condition['request_id'] = \common\components\GuestInfo::getParam("request_id");
        $datas = \common\models\hy_member_request_useModel::baseGetDatas($condition);
        
                
        foreach($datas as $key => $value){
            $datas[$key] = $value;
        }
        
        $this->success(['data' => $datas]);    
    }
    
    
    //获取用户的相关的需求
    public function actionUse_request()
    {
        $condition = [];
        $condition['pagesize'] = 20;
        $condition['alias'] = "main";
        
        $mine_uid = $this->user->uid;
        
        $hy_designer = \common\models\hy_designerModel::getOne($this->user->uid);
        if(!$hy_designer){
            $condition['uid'] = $this->user->uid;
        }else{
            $condition['leftJoin'] = [
                "table" => "yii_hy_member_request_use use",
                "on" => "use.request_id=main.id",
            ];
            $condition[] = "main.service_uid={$mine_uid} or use.uid={$mine_uid}";
        }
        $datas = \common\models\hy_member_requestModel::baseGetDatas($condition);
                
        foreach($datas as $key => $value){
            $datas[$key] = $value;
            $datas[$key]['service_name'] = \common\models\hy_designerModel::getOne($value['service_uid'], "nickname");
            $datas[$key]['username'] = \common\models\MemberModel::getOne($value['uid'], "username");
        }
        
        $this->success(['data' => $datas]);  
    }    
    
    
    //获取某一个用户的需求
    public function actionUse_request_detail()
    {
        $request = \common\models\hy_member_requestModel::findOne(\common\components\GuestInfo::getParam("id"));
        if(!$request){
            $this->error("找不到相关的数据");
        }else{
            $request = $request->attributes;
        }
        $request['service_name'] = \common\models\hy_designerModel::getOne($request['service_uid'], "nickname");
        
        $jianyuan = \common\models\hy_member_jianyuanModel::findOne($request['uid']);
        if(!$jianyuan){
            $this->error("找不到相关的数据");
        }else{
            $jianyuan = $jianyuan->attributes;
        }
        
        
        $config = [
            "sex" => $this->_get_data(\common\models\hy_member_jianyuanModel::getsexHtml()),
            "demand_type" => $this->_get_data(\common\models\hy_member_jianyuanModel::getdemand_typeHtml()),
            "acreage" => $this->_get_data(\common\models\hy_member_jianyuanModel::getacreageHtml()),
            "space_type" => $this->_get_data(\common\models\hy_space_typeModel::find()->limit(9999)->asArray()->all()),
            "element" => $this->_get_data(\common\models\hy_space_type_detailModel::find()->limit(99999)->asArray()->all()),
            "style_type" => $this->_get_data(\common\models\hy_member_jianyuanModel::getstyle_typeHtml()),
            "budget" => $this->_get_data(\common\models\hy_member_jianyuanModel::getbudgetHtml()),
            "design_fee" => $this->_get_data(\common\models\hy_member_jianyuanModel::getdesign_feeHtml()),
            "measure_fee" => $this->_get_data(\common\models\hy_member_jianyuanModel::getmeasure_feeHtml()),
            "complete_time" => $this->_get_data(\common\models\hy_member_jianyuanModel::getcomplete_timeHtml()),
        ];        
        
        
        
        
        $this->success(['data' => ['request' => $request,"jianyuan" => $jianyuan,"config" => $config]]);  
    }
    
    
}
