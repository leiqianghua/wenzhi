<?php

namespace frontend\modules\hy\controllers;
class Member_request_dataController extends \frontend\modules\hy\components\BaseloginController
{
    //总共有这些
    public function actionIndex()
    {
        $condition['pagesize'] = 99999;
        $datas = \common\models\hy_member_request_dataModel::baseGetDatas($condition);
               
        
        $result = [];
        foreach($datas as $key => $value){
            \common\models\hy_member_request_dataModel::add_Field($datas[$key]);
            $result[$value['type_id']] = $datas[$key];
        }
        
        $datas = \common\models\hy_member_request_dataModel::getListMenuForDatas($result);
        
        $datas = \common\models\hy_member_request_dataModel::getListForArr($datas);
        
        $this->success(['data' => $datas]);  
    }
    //所有可以加进来的分类
    public function actionAll_can_create_index()
    {
        $type = \common\components\GuestInfo::getParam("type");//找到有哪些
        $request_id = \common\components\GuestInfo::getParam("request_id");//哪个需求
        
        $condition = [];
        $condition['pagesize'] = 99999;
        $condition['page'] = 1;
        $condition['request_id'] = $request_id;
        $condition['type'] = $type;
        $datas = \common\models\hy_member_request_dataModel::baseGetDatas($condition);//有这些 选择了这些了
        $ids = [];
        foreach($datas as $data){
            //看一下是否是最后一层 底下没人
            $ids[] = $data['type_id'];
        }
        if($ids){
            //找出其所有的这个底下的子元素
            $condition = [];
            $condition['pagesize'] = 99999;
            $condition['page'] = 1;
            $condition['parent_id'] = $ids;//有父的
            $ddds = \common\models\hy_categoryModel::baseGetDatas($condition);//有这些 选择了这些了  
            if($ddds){
                $oo_ids = [];
                foreach($ddds as $kkk => $vvv){
                    if(!in_array($vvv['parent_id'], $oo_ids)){
                        $oo_ids[] = $vvv['parent_id'];
                    }
                }
                foreach($ids as $k => $v){
                    if(in_array($v, $oo_ids)){
                        unset($ids[$k]);
                    }
                }
            }
        }
        
        if($ids){
            $str_id = implode(",", $ids);
            $and_where = "id not in({$str_id})";
        }else{
            $and_where = [];
        }
        
        $condition = [];
        $condition['pagesize'] = 99999;
        $condition['page'] = 1;
        $condition[] = $and_where;
        $condition['type'] = $type;
        $datas = \common\models\hy_categoryModel::baseGetDatas($condition);//有这些 选择了这些了      
        $result = [];
        foreach($datas as $key => $value){
            //\common\models\hy_member_request_dataModel::add_Field($datas[$key]);
            $result[$value['id']] = $datas[$key];
        }    
        
        
        $datas = \common\models\hy_categoryModel::getListMenuForDatas($result);
        //print_r($and_where);exit;
//        \common\models\hy_categoryModel::$type = $type;
//        $datas = \common\models\hy_categoryModel::getListMenuForType(false, $and_where);
        $datas = $this->get_data($datas);
        //print_r($datas);exit;
        
        
        return $this->success(['data' => $datas]);
    }
    
    
    
    private function get_data($datas)
    {
        $result = [];
        $num = 0;
        foreach ($datas as $key => $value){
            $result[$num]['open'] = true;
            $result[$num]['id'] = $value['id'];
            $result[$num]['name'] = $value['title'];
            if(!empty($value['data'])){
                $result[$num]['children'] = $this->get_data($value['data']);
            }else{
                $result[$num]['children'] = [];
            }
            $num++;
        }
        return $result;
    }


    //增加
    public function actionAdd_data()
    {
        $type_id = \common\components\GuestInfo::getParam("type_id");
        $request_id = \common\components\GuestInfo::getParam("request_id");
        if(!$type_id){
            $this->error("请选择增加工作流程");
        }
        if(is_string($type_id)){
            $type_id = json_decode($type_id,true);
        }
        foreach ($type_id as $key => $value){
            
            $is_data = \common\models\hy_member_request_dataModel::find()->where(['request_id' => $request_id,"type_id" => $value])->limit(1)->one();
            if($is_data){
                continue;
            }
            
            $type_detail = \common\models\hy_categoryModel::getOne($value);
            
            $obj = new \common\models\hy_member_request_dataModel();
            $obj->type_id = $value;
            $obj->title = $type_detail['title'];
            $obj->sel_uid = $this->user->uid;
            $obj->request_id = $request_id;
            $obj->type = $type_detail['type'];
            $obj->parent_id = $type_detail['parent_id'];
            $obj->sorting = $type_detail['sorting'];
            $obj->label = $type_detail['label'];
            $obj->status = $type_detail['status'];
            $obj->img = $type_detail['img'];
            $obj->save(false);
        }
        $this->success("处理成功");
    }
    
    public function actionDelete()
    {
        $model = new \common\models\hy_member_request_dataModel();    
        return $this->baseDelete(get_class($model));
    }
    
    //设置其完成
    public function actionUpdate()
    {
        $model = new \common\models\hy_member_request_dataModel();   
        
        $d = \common\models\hy_member_request_data_recordModel::find()->where(['data_id' => \common\components\GuestInfo::getParam("id")])->limit(1)->one();
        if(!$d){
            $this->error("请先上传相关的工作流程记录");
        }
        $_POST['ok'] = true; 
        $_POST['over_time'] = time();
        $_POST['shezhi_d'] = 1;
        return $this->baseUpdate(get_class($model));
    }
}
