<?php

namespace frontend\modules\hy\controllers;
class Member_request_data_recordController extends \frontend\modules\hy\components\BaseloginController
{
    //查看相关的记录
    public function actionIndex()
    {
        $condition['pagesize'] = 99999;
        $datas = \common\models\hy_member_request_data_recordModel::baseGetDatas($condition);
        
                
        foreach($datas as $key => $value){
            $datas[$key] = $value;
            $datas[$key]['do_nickname'] = \common\models\hy_designerModel::getOne($value['do_uid'], "nickname");
            $datas[$key]['images'] = json_decode($value['images'],true);
            $datas[$key]['type_name'] = \common\models\hy_categoryModel::getTypeHtml($value['type']);
        }
        
        $this->success(['data' => $datas]);          
    }
    
    
    //查看相关的记录
    public function actionCreate()
    {
        $type = \common\models\hy_member_request_dataModel::getOne(\common\components\GuestInfo::getParam("data_id"), "type");
        if(!$type){
            $this->error("找不到此相关数据");
        }
        $model_class = new \common\models\hy_member_request_data_recordModel();
        
        $model_class->do_uid = $this->user->uid;
        $model_class->type = $type;
        $_POST['ok'] = true;
        return $this->baseCreate($model_class);      
    }
}
