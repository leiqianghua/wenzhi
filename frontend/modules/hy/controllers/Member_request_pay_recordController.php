<?php

namespace frontend\modules\hy\controllers;
class Member_request_pay_recordController extends \frontend\modules\hy\components\BaseloginController
{
    //确认其支付上的东西
    public function actionIndex()
    {
        $condition['pagesize'] = 99999;
        $condition['orderBy'] = "status asc,add_time desc";
        $datas = \common\models\hy_member_request_pay_recordModel::baseGetDatas($condition);
        
                
        foreach($datas as $key => $value){
            $datas[$key] = $value;
            $datas[$key]['pay_username'] = \common\models\MemberModel::getOne($value['pay_uid'], "username");
        }
        
        
        $config = [
            'status' => \common\models\hy_member_request_pay_recordModel::getStatusHtml(),
            'is_underline_payment' => \common\models\hy_member_request_pay_recordModel::getis_underline_paymentHtml(),
            'pay_mine_type' => \common\models\hy_member_request_pay_recordModel::getpay_mine_typeHtml(),
        ];
        
        $this->success(['data' => $datas,'config' => $config]);          
    }
    
    public function actionDetail()
    {
        $datas = \common\models\hy_member_request_pay_recordModel::findOne(\common\components\GuestInfo::getParam("order_sn"));
        if(!$datas){
            $this->error("找不到相关数据");
        }else{
            $datas = $datas->attributes;
        }
        $datas['pay_username'] = \common\models\MemberModel::getOne($datas['pay_uid'], "username");
                

        $config = [
            'status' => \common\models\hy_member_request_pay_recordModel::getStatusHtml(),
            'is_underline_payment' => \common\models\hy_member_request_pay_recordModel::getis_underline_paymentHtml(),
            'pay_mine_type' => \common\models\hy_member_request_pay_recordModel::getpay_mine_typeHtml(),
        ];
        
        $this->success(['data' => $datas,'config' => $config]);          
    }
    
    
    
    //创建一个需要你支付的东西了.
    public function actionCreate()
    {
        $money = \common\components\GuestInfo::getParam("money");
        $title = \common\components\GuestInfo::getParam("title");//付款事项
        $request_id = \common\components\GuestInfo::getParam("request_id");
        $scookies = \common\components\GuestInfo::getParam("scookies");
        $pay_mine_type = \common\components\GuestInfo::getParam("pay_mine_type");
        $model = \common\models\hy_member_request_pay_recordModel::rechargePay($money, $title, $request_id, 1, $pay_mine_type, $scookies);
        if($model){
            $this->success(['data' => ['order' => $model['ordresn']]]);
        }else{
            $this->error("付款出错");
        }
    }
    
    //走线下，然后用户上传截图
    public function actionUpdate_use()
    {
        $ordersn = \common\components\GuestInfo::getParam("ordresn");
        $model = \common\models\hy_member_request_pay_recordModel::findOne($ordersn);
        if(!$model){
            $this->error("找不到数据");
        }
        if($model['pay_uid'] != $this->user->uid){
            $this->error("不是你的支付");
        }
        if($model['status']){
            $this->error("已经确认了的支付");
        }
        if($model['is_underline_payment'] != 0){
            $this->error("线下支付状态异常");
        }
//        if(!$model || ($model['pay_uid'] != $this->user->uid) || $model['status'] || $model['is_underline_payment'] != 0){
//            $this->error("状态出错");
//        }
        $model->is_underline_payment = 1;
        $model->underline_payment_mark = \common\components\GuestInfo::getParam("underline_payment_mark");
        
//        if($model->underline_payment_img){
//            $img = json_decode($model->underline_payment_img,true);
//        }
//        if(empty($img)){
//            $img = [];
//        }
        
        $i = \common\components\GuestInfo::getParam("underline_payment_img");
        if(is_string($i)){
            $i = json_decode($i,true);
        }
        
        if(!$i){
            $i = [];
        }
        //$img = array_merge($i,$img);
        
        $model->underline_payment_img = json_encode($i);
        $model->save(false);
        $this->success("提交成功等待核实");
    }
    
    //走线下，然后用户上传截图
    public function actionUpdate_cai()
    {
        $ordersn = \common\components\GuestInfo::getParam("ordresn");
        $model = \common\models\hy_member_request_pay_recordModel::findOne($ordersn);
        if(!$model  || $model['status'] || $model['is_underline_payment'] != 1){
            $this->error("状态出错");
        }
        $model->is_underline_payment = \common\components\GuestInfo::getParam("is_underline_payment");
        if($model->is_underline_payment == 2){
            $model->status = 1;
        }
        $model->save(false);
        $this->success("处理成功");
    }
}
