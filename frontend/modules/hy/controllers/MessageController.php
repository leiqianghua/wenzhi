<?php

namespace frontend\modules\hy\controllers;
class MessageController extends \frontend\modules\hy\components\BaseloginController
{
    public function actionIndex()
    {
        $condition['uid'] = $this->user->uid;
        $condition['orderBy'] = "status asc";
        $datas = \common\models\hy_messageModel::baseGetDatas($condition);
        
        
        foreach($datas as $key => $value){
            $datas[$key] = $value;
        }
        
        $this->success(['data' => $datas]);  
        
    }
    
    public function actionDetail()
    {
        $data = \common\models\hy_messageModel::getOne(\common\components\GuestInfo::getParam("id"));
        if(!$data){
            $this->error("找不到相关的数据");
        }
        
        
        $this->success(['data' => $data]);
    }
    
    public function actionUpdate()
    {
        $ids = \common\components\GuestInfo::getParam("id");
        $data = \common\models\hy_messageModel::updateAll(['status' => 1], ['id' => $ids]);
        if(!$data){
            $this->error("标记失败");
        }else{
            $this->success("标记成功");
        }
    }
}
