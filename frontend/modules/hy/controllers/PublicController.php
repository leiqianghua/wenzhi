<?php

namespace frontend\modules\hy\controllers;
class PublicController extends \frontend\modules\hy\components\BaseController
{
    public function actionHy()
    {
        return $this->redirect("/hy");
    }
    
    
    
    public function actionLogin()
    {
        if($this->user->isLogin()){
            $this->error("已经登录");
        }else{//告诉其登录地址让它跳转
            $this->success(['data' => ['url' => "/wap/public/login?type_new=hy"]]);//注册的时候告诉其地址让它跳
        }     
    }
    
    public function actionRegister()
    {
        if($this->user->isLogin()){
            $this->error("已经登录");
        }else{
            $this->success(['data' => ['url' => "/wap/public/register?type_new=hy"]]);//注册的时候告诉其地址让它跳
        }
    }
    
    public function actionLayout()
    {
        $this->user->layout();
        $this->success("退出成功");
    }
    //相关的配置信息
    public function actionConfig()
    {
        $key_list = \common\models\ConfigModel::find()->where(['type' => "forentend"])->asArray()->select("varname")->column();
        $datas = \common\models\ConfigModel::getNewDatas($key_list);
        
        $datas['design_fee_type'] = $this->_get_data(\common\models\hy_designerModel::getdesign_fee_typeHtml());
        $datas['celiang_fee_type'] = $this->_get_data(\common\models\hy_designerModel::getceliang_fee_typeHtml());
        $datas['gold_type'] = $this->_get_data(\common\models\hy_designerModel::getgold_typeHtml());

        
        
        $this->success(['data' => $datas]);
    }
    
    
    protected function _get_data($datas)
    {
        $result = [];
        foreach($datas as $key => $value){
            $drr = [];
            $drr['label'] = $key;
            $drr['value'] = $value;
            $result = $drr;
        }
        return $result;
    }
    

    public function actionUploadfile()
    {
        if($_FILES){
            $this->user->isLogin();
            $dir = \common\components\GuestInfo::getParam('path', 'common/upload');
            $obj = new \common\components\UploadFile();
            $file = $obj->upload($dir);  
            $this->success(['data' => $file['url']]);
        }
        $res ['status'] = 0;
        $string_img = \common\components\GuestInfo::getParam ( 'base64Img' );
        
        $num = stripos($string_img,"base64,") + 7;
        $postShareimg = substr ( $string_img, $num );        
        //$postShareimg = substr ( $string_img, 23 );
        // $string_img=explode(';',$string_img);
        //echo $postShareimg;exit;
        if ($postShareimg) {
            $uploadurl = date("Ymd") . '/user/load';
            do {
                $randName = rand ( 1000000000, 9999999999 ) . '.jpeg';
                // 相对路径
                $fileName = $uploadurl . "/" . $randName;
            } while ( file_exists ( $fileName ) );
            $oss = new \common\components\UploadFile ();
            //file_put_contents('test1.jpeg',base64_decode ( $postShareimg ));die;
            $oss_res = $oss->saveObject ( $fileName, base64_decode ( $postShareimg ) );
            
            if(!$oss_res){
                $this->error($oss->error);
            }
            $imgFileName = \yii::$app->params ['file_url'] . "/" . $fileName ;
            $this->success(['data' => $imgFileName]);
        }else{
            $this->error("请传base64数据");
        }
    }
}
