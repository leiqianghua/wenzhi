<?php

namespace frontend\modules\hy\controllers;
class StaffController extends \frontend\modules\hy\components\BaseloginController
{
    public function actionDesign_index()
    {
        $condition['status'] = 1;
        $condition['orderBy'] = "d.case_update_time desc,d.update_time desc,d.add_time desc";
        $condition['alias'] = "d";
        $condition['leftJoin'] = [
            'table' => "yii_hy_designer_level level",
            "on" => "d.uid = level.uid",
        ];
        $condition[] = "level.studio_level_id in(3,7,11)";//4 8 12
        
        $result = \common\models\hy_designerModel::baseGetDatas($condition);
        
        
        foreach($result as $key => $value){//每个人三个案例带上来
            $condition = [];
            $condition['designer_id'] = $value['uid'];
            $condition['status'] = 1;
            $condition['orderBy'] = "sorting desc";
            $condition['pagesize'] = 3;
            $condition['noget'] = true;
            $d = \common\models\hy_caseModel::baseGetDatas($condition);
            
            foreach ($d as $kl => $v){
                $d[$kl]['content_imgs'] = json_decode($v['content_imgs'],true);
            }
            $result[$key]['case'] = $d;
            
            $arr = [];
            $gold_type = json_decode($value['gold_type'],true);
            if($gold_type){
                $arr = [];
                foreach($gold_type as $k => $v){
                    $arr[] = \common\models\hy_designerModel::getgold_typeHtml($v);
                }
            }
            
            $result[$key]['gold_type'] = $gold_type;
            $result[$key]['gold_type_html'] = $arr;
            //$result[$key]['content_imgs'] = json_decode($value['content_imgs'],true);
        }
        
        $this->success(['data' => $result]);
    }
    
    public function actionDesign_detail()
    {
        $data = \common\models\hy_designerModel::getOne(\common\components\GuestInfo::getParam("id"));
        if(!$data){
            $this->error("找不到相关的数据");
        }
            
        $arr = [];
        $gold_type = json_decode($data['gold_type'],true);
        if($gold_type){
            $arr = [];
            foreach($gold_type as $k => $v){
                $arr[] = \common\models\hy_designerModel::getgold_typeHtml($v);
            }
        }

        $data['gold_type'] = $gold_type;
        $data['gold_type_html'] = $arr;
        
        
        $this->success(['data' => $data]);
    }
    
    
    public function actionZaojin_index()
    {
        $condition['status'] = 1;
        $condition['orderBy'] = "d.case_update_time desc,d.update_time desc,d.add_time desc";
        $condition['alias'] = "d";
        $condition['leftJoin'] = [
            'table' => "yii_hy_designer_level level",
            "on" => "d.uid = level.uid",
        ];
        $condition[] = "level.studio_level_id in(4,8,12)";//4 8 12
        
        $result = \common\models\hy_designerModel::baseGetDatas($condition);
        
        //echo $result;exit;
        $datas = [];
        foreach($result as $key => $value){//每个人三个案例带上来
            $condition = [];
            $condition['designer_id'] = $value['uid'];
            $condition['status'] = 1;
            $condition['orderBy'] = "sorting desc";
            $condition['pagesize'] = 3;
            $condition['noget'] = true;
            $d = \common\models\hy_caseModel::baseGetDatas($condition);
            $result[$key]['case'] = $d;
            
            $arr = [];
            $gold_type = json_decode($value['gold_type'],true);
            if($gold_type){
                $arr = [];
                foreach($gold_type as $k => $v){
                    $arr[] = \common\models\hy_designerModel::getgold_typeHtml($v);
                }
            }
            
            $result[$key]['gold_type'] = $gold_type;
            $result[$key]['gold_type_html'] = $arr;
            //$datas[$key]['content_imgs'] = json_decode($value['content_imgs'],true);
        }
        
        $this->success(['data' => $result]);
    }
    
    public function actionZaojin_detail()
    {
        $data = \common\models\hy_designerModel::getOne(\common\components\GuestInfo::getParam("id"));
        if(!$data){
            $this->error("找不到相关的数据");
        }
        
        $arr = [];
        $gold_type = json_decode($data['gold_type'],true);
        if($gold_type){
            $arr = [];
            foreach($gold_type as $k => $v){
                $arr[] = \common\models\hy_designerModel::getgold_typeHtml($v);
            }
        }

        $data['gold_type'] = $gold_type;
        $data['gold_type_html'] = $arr;        
        
        $this->success(['data' => $data]);
    }
}
