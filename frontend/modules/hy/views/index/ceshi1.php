<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" href="/hy/weui2/css/weui.css"/>
<link rel="stylesheet" href="/hy/weui2/css/weuix.min.css"/>
<script src="/hy/weui2/js/zepto.min.js"></script>
<script src="/hy/weui2/js/zepto.weui.js"></script>
<script src="/hy/weui2/js/swipe.js"></script>

<link rel="stylesheet" href="/hy/css/common.css"/>
<link rel="stylesheet" href="/hy/css/style.css"/>

<meta content="古笆兔花园" name="keywords">
<meta content="古笆兔花园" name="description">
<title>古笆兔花园</title>
</head>
<body>
    <div class="hy_user_select">
        <div>全部人</div>
        <div class="flex user_select_detail select"><span>真不世</span><img src="/hy/images/down.png"/></div>
        <div class="flex user_select_detail"><span>真不世</span><img src="/hy/images/down.png"/></div>
        <div class="flex user_select_detail"><span>真不世</span><img src="/hy/images/down.png"/></div>
    </div>
    
    <div class="content">
        <div class="hy_content_list">
            
            <div  class="hy_content hy_list_user">
                <div class="hy_content_item1">
                    <img class="logo" src="/hy/images/down.png"/>
                    <div class="flex hy_detail_show">
                        <div>
                            <div class="hy_detail_fotter_1">
                                <span class="name_show">雷强华</span> <img src="/hy/images/work_area.png"> <span class="name_show_2">南京</span>
                            </div>
                            <div class="hy_detail_fotter_2">
                                <span>从年限制：7年</span>  <span>案例：3个</span>
                            </div>  
                            <div class="hy_detail_fotter_3">
                                <span>设计费：3533</span>
                            </div>    
                            <div class="hy_detail_fotter_3">
                                <span>测量费：3533</span>
                            </div>             
                        </div>
                        <div class="show_pai show_pai1">金牌设计</div>                        
                    </div>
                </div>
                <div class="hy_content_item2">
                    <img class="show_img" src="http://huanimage.gubatoo.com/20181121/h_case/f744199085780430.png">
                    <div class="no_img">
                        <img src="/hy/images/noUp.png">
                        <span>未上传</span>
                    </div>
                    <div class="no_img">
                        <img src="/hy/images/noUp.png">
                        <span>未上传</span>
                    </div>
                </div>
            </div>
            <div  class="hy_content hy_list_user">
                <div class="hy_content_item1">
                    <img class="logo" src="https://wx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEIxLOskwsPxACaD98Ev4vEUKFvm64jmZf2sLjNNLgK2CAQAqNZLq3wdZ5PbO8hDRfvCvBpsL8TSGg/132"/>
                    <div class="flex hy_detail_show">
                        <div>
                            <div class="hy_detail_fotter_1">
                                <span class="name_show">雷强华</span> <img src="/hy/images/work_area.png"> <span class="name_show_2">南京</span>
                            </div>
                            <div class="hy_detail_fotter_2">
                                <span>从年限制：7年</span>  <span>案例：3个</span>
                            </div>  
                            <div class="hy_detail_fotter_3">
                                <span>设计费：3533</span>
                            </div>    
                            <div class="hy_detail_fotter_3">
                                <span>测量费：3533</span>
                            </div>             
                        </div>
                        <div class="show_pai show_pai1">金牌设计</div>                        
                    </div>
                </div>
                <div class="hy_content_item2">
                    <img class="show_img" src="http://huanimage.gubatoo.com/20181121/h_case/f744199085780430.png">
                    <div class="no_img">
                        <img src="/hy/images/noUp.png">
                        <span>未上传</span>
                    </div>
                    <div class="no_img">
                        <img src="/hy/images/noUp.png">
                        <span>未上传</span>
                    </div>
                </div>
            </div>
            
            
        </div>
        
        
        <div class="hy_content no_content">
            没有更多内容了
        </div>
    </div>
</body>


