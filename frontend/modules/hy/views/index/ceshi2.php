<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" href="/hy/weui2/css/weui.css"/>
<link rel="stylesheet" href="/hy/weui2/css/weuix.min.css"/>
<script src="/hy/weui2/js/zepto.min.js"></script>
<script src="/hy/weui2/js/zepto.weui.js"></script>
<script src="/hy/weui2/js/swipe.js"></script>

<link rel="stylesheet" href="/hy/css/common.css"/>
<link rel="stylesheet" href="/hy/css/style.css"/>

<meta content="古笆兔花园" name="keywords">
<meta content="古笆兔花园" name="description">
<title>设计师详情页面</title>
</head>
<body>
    <div class="content_show">
        <div class="hy_content">
            <div class="user_detail_head">
                <div class="user_detail_head1">高海派</div>
                <div class="user_detail_head2"><img src="/hy/images/work_area.png"/><span>厦门市</span></div>
            </div>


            <div class="user_detail_show_item">
                <div class="user_detail_show_item1">
                    <div class="show_item1_detail"><img class="" src="/hy/images/work.png"/><span>你真好</span></div>
                    <div class="show_item1_detail"><img class="" src="/hy/images/design_fee_ico.png"/><span>你真好</span></div>
                    <div class="show_item1_detail"><img class="" src="/hy/images/survey_fee_ico.png"/><span>你真好</span></div>
                    <div class="show_item1_detail"><span>从业年限：3年</span></div>
                </div>
                <div class="user_detail_show_item2"><img class="hy_detail_logo" src="/hy/images/default.png"/></div>
            </div>


        </div>

        <div style="margin-bottom: 6.4vw"  class="hy_content">
            <h3>设计案例</h3>
        </div>


        <div class="anli_list">
            <div class="hy_content">
                <div class="anli_item">厦门私家花园</div>
                <div class="anli_item anli_list_1">
                    <div><am class="case-item-start">#</am><span>100--200万</span></div>
                    <div class="hy_detail_fotter_1">
                                    <img src="/hy/images/work_area.png"> <span class="name_show_2">南京</span>
                    </div>
                </div>
                <div class="anli_item show_imgs">
                    <img class="img_data" src="http://huanimage.gubatoo.com/20181121/h_case/4e42562929780334.jpg"/>
                    <img class="img_data_show" src="/hy/images/no-collect.png"/>
                </div>
            </div>
        </div>


        <div class="anli_list">
            <div class="hy_content">
                <div class="anli_item">厦门私家花园</div>
                <div class="anli_item anli_list_1">
                    <div><am class="case-item-start">#</am><span>100--200万</span></div>
                    <div class="hy_detail_fotter_1">
                                    <img src="/hy/images/work_area.png"> <span class="name_show_2">南京</span>
                    </div>
                </div>
                <div class="anli_item show_imgs">
                    <img class="img_data" src="http://huanimage.gubatoo.com/20181121/h_case/4e42562929780334.jpg"/>
                    <img class="img_data_show" src="/hy/images/no-collect.png"/>
                </div>
            </div>
        </div>



            <div class="hy_content no_content">
                没有更多内容了
            </div>
        
    </div>

    
    <div class="select_jian"><button>请他建园</button></div>
    
</body>


