<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" href="/hy/weui2/css/weui.css"/>
<link rel="stylesheet" href="/hy/weui2/css/weuix.min.css"/>
<script src="/hy/weui2/js/zepto.min.js"></script>
<script src="/hy/weui2/js/zepto.weui.js"></script>
<script src="/hy/weui2/js/swipe.js"></script>

<link rel="stylesheet" href="/hy/css/common.css"/>
<link rel="stylesheet" href="/hy/css/style.css"/>

<meta content="古笆兔花园" name="keywords">
<meta content="古笆兔花园" name="description">
<title>文章列表</title>
</head>
<body>
    <div class="weui-news">
        <ul class="weui-news-list">
            <li class="weui-news-item">
                <div class="weui-news-inners">
                        <div class="weui-news-title">中国改革开放的成功始终在于坚持走自己的路</div>
                        <div class="weui-news-text">
                            <p class="weui-news-p">新中国成立以来，特别是改革开放40年来，党和人民立足国情、立足实际大力度推进社会主义现代化建设，中国经济建设健康持续发展，取得了世界瞩目的成就。当然，在中国改革开放的进程中，总是伴有杂音的，也有一些错误的论调。比如，有的认为，“改革开放的力度和成效还远远不够，只有按照西方模式推进才是真正的改革”。事实胜于雄辩，中国的改革开放，“既不走封闭僵化的老路，也不走改旗易帜的邪路”，而是坚持走正路，走自己的路，建设有中国特色的社会主义。党和人民立足国情、立足实际推进社会主义现代化建设，中国经济建设持续健康发展，取得了世界瞩目的成就。</p>
                        </div>
                    <div class="weui-news-info">
                        <div class="weui-news-infoitem">
<!--                            <img src="../images/user.png" class="weui-news-round">-->
                            <span>微信小程序</span>
                        </div>
                        <div class="weui-news-infoitem">1条评论</div>
                    </div>
                </div>

            </li>

            <li class="weui-news-item">
                <div class="weui-news-inners">
                        <div class="weui-news-title">中国改革开放的成功始终在于坚持走自己的路</div>
                        <div class="weui-news-text">
                            <p class="weui-news-p">新中国成立以来，特别是改革开放40年来，党和人民立足国情、立足实际大力度推进社会主义现代化建设，中国经济建设健康持续发展，取得了世界瞩目的成就。当然，在中国改革开放的进程中，总是伴有杂音的，也有一些错误的论调。比如，有的认为，“改革开放的力度和成效还远远不够，只有按照西方模式推进才是真正的改革”。事实胜于雄辩，中国的改革开放，“既不走封闭僵化的老路，也不走改旗易帜的邪路”，而是坚持走正路，走自己的路，建设有中国特色的社会主义。党和人民立足国情、立足实际推进社会主义现代化建设，中国经济建设持续健康发展，取得了世界瞩目的成就。</p>
                        </div>
                    <div class="weui-news-info">
                        <div class="weui-news-infoitem">
<!--                            <img src="../images/user.png" class="weui-news-round">-->
                            <span>微信小程序</span>
                        </div>
                        <div class="weui-news-infoitem">1条评论</div>
                    </div>
                </div>

            </li>
        </ul>
    </div>    
</body>


