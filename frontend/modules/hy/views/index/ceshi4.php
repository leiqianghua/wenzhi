<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" href="/hy/weui2/css/weui.css"/>
<link rel="stylesheet" href="/hy/weui2/css/weuix.min.css"/>
<script src="/hy/weui2/js/zepto.min.js"></script>
<script src="/hy/weui2/js/zepto.weui.js"></script>
<script src="/hy/weui2/js/swipe.js"></script>

<link rel="stylesheet" href="/hy/css/common.css"/>
<link rel="stylesheet" href="/hy/css/style.css"/>

<meta content="古笆兔花园" name="keywords">
<meta content="古笆兔花园" name="description">
<title>文章列表</title>
</head>
<body>
    <div class="weui-content">
        <div class="weui-c-inner">
            <div class="weui-c-content">
                 <h2 class="weui-c-title">Flutter是谷歌的移动UI框架，可以快速在iOS和Android上构建高质量的原生用户界面</h2>
                <div class="weui-c-meta">
                    <span class="weui-c-nickname"><a href="javascript:;">Yoby开发者</a></span>
                    <em class="weui-c-nickname">2018-10-10 10:10</em>
                </div>
                <div class="weui-c-article">
                    内容
                </div>
            </div>
             <div class="weui-c-tools">
                 <a href="javascript:;">古</a>
                 <div class="weui-c-readnum">阅读<span id="readnum">10000+</span></div>
                 <div class="weui-c-like">
                    <i class="icon"></i>
                     <span id="likenum">1000</span>
                 </div>
             </div>
        </div>

    </div> 
</body>


