<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" href="/weui2/css/weui.css"/>
<link rel="stylesheet" href="/weui2/css/weuix.min.css"/>
<script src="/weui2/js/zepto.min.js"></script>
<script src="/weui2/js/swipe.js"></script>


<meta content="古笆兔花园" name="keywords">
<meta content="古笆兔花园" name="description">
<title>古笆兔花园</title>
</head>
<body>

    
    <style>
        .calc_height{
            width:50vw;
        }
        .calc_height img{
            height: 100%;
            width: 100%;
            object-fit:cover;
        }
    </style>    
    
    
    
<div class="container page-bg">
    <div class="page-bd-15">
        





        <div style="height:50vw;display:flex">
            
            <div class="calc_height" style="background: red;">
                <div style="height: 100%;max-height:100%" class="slide" id="slide11">
                    <ul>
                        <li>
                            <a href="javascript:;">
                                <img src='http://huanimage.gubatoo.com/20181012/ad_wap_img/6286299924325657.jpg' alt="">
                            </a>
                            <div class="slide-desc">白日依山尽黄河入海流鱼群千里马更上高楼处</div>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <img src='/images/logo.png' alt="">
                            </a>
                            <div class="slide-desc">东风无力百花残</div>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <img src='/images/logo.png' alt="">
                            </a>
                            <div class="slide-desc">千金易得一将难求</div>
                        </li>
                    </ul>
                    <div class="dot">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>                  
            </div>
            
            
            
            <div class="calc_height" style="background: yellow;width: 40vw"></div>



<!--             <div style="height: 100%;padding-right: 5px" class="weui-flex__item">
                <div style="height: 100%;max-height:100%" class="slide" id="slide11">
                    <ul>
                        <li>
                            <a href="javascript:;">
                                <img src='/images/logo.png' alt="">
                            </a>
                            <div class="slide-desc">白日依山尽黄河入海流鱼群千里马更上高楼处</div>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <img src='/images/logo.png' alt="">
                            </a>
                            <div class="slide-desc">东风无力百花残</div>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <img src='/images/logo.png' alt="">
                            </a>
                            <div class="slide-desc">千金易得一将难求</div>
                        </li>
                    </ul>
                    <div class="dot">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>            
            </div>
            
            <div  style="height: 150px;padding-left: 5px;" class="weui-flex__item">
                <img style="height: 75px;width: 100%" src="/images/default.jpg" />
                <img style="height: 70px;width: 100%" src="/images/default.jpg" />
            </div> -->
        </div> 
        
        
        
        <h3>最新案例</h3>
        
        
        







        
    </div>  
</div>
    
    

    
    
    
</body>


<script>
    $(function(){
        $('#slide11').swipeSlide({
            autoSwipe:true,//自动切换默认是
            speed:3000,//速度默认4000
            continuousScroll:true,//默认否
            transitionType:'cubic-bezier(0.22, 0.69, 0.72, 0.88)',//过渡动画linear/ease/ease-in/ease-out/ease-in-out/cubic-bezier
            lazyLoad:true,//懒加载默认否
            firstCallback : function(i,sum,me){
                me.find('.dot').children().first().addClass('cur');
            },
            callback : function(i,sum,me){
                me.find('.dot').children().eq(i).addClass('cur').siblings().removeClass('cur');
            }
        });  
        
        
        var mySwiper = new Swiper ('.swiper-container', {
           //loop: true, // 循环模式选项
            slidesPerView: 3,
            centeredSlides: true,
            paginationClickable: true,
            spaceBetween: 30,
            grabCursor: true
        })         
    })
</script>

</html>