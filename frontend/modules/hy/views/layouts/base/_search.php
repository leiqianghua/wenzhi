<form id="search_submit" action="<?= $this->context->to(['search/index']);  ?>">
<header class="header_wrapper">

<div class="header_bg"></div>
        <a class="up_level header_btn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="user_center header_btn" href="javascript:void(0);" onclick="$('#search_submit').submit();">搜索</a>
        <a class="back_btn header_btn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
        <div class="search_wrapper">
            <div class="search_box">
                <input type="text" name="keyword" value="<?= empty($_GET['keyword']) ? '' : $_GET['keyword']; ?>" class="search_input" placeholder="搜索您想要的商品">
                <a class="search_btn" href="javascript:$('#search_submit').submit();"><i class="iconfont">&#xe606;</i></a>
            </div>
        </div>
</header>        
</form>     
<div class="hot_search">
    <h1>热门搜索</h1>
    <div class="hot_words">
        <a href="<?= $this->context->to(['search/index','keyword' => '家装']);  ?>">家装</a>
        <a href="<?= $this->context->to(['search/index','keyword' => '装饰']);  ?>">装饰</a>
        <a href="<?= $this->context->to(['search/index','keyword' => '室内装饰']);  ?>">室内装饰</a>
    </div>
    
    <?php  $datas = \common\models\GoodsLabelModel::getListMenu(2);  ?>
    <?php foreach($datas as $data){  ?>
            <h1><?= $data['title']  ?></h1>
            <div class="hot_words">
                <?php foreach($data['data'] as $k => $v){  ?>      
                    <a href="<?= $this->context->to(['label/index','label_id' => $v['id']])  ?>"><?= $v['title']  ?></a>
                <?php }  ?>
            </div>
    <?php }  ?>      
</div>  


<script type="text/javascript">
$(function(){
    //搜索列表
    var searchList = new searchListFun();
    searchList.focus();
    searchList.back();
})
//搜索列表
var searchListFun = function(){
    var _height=$(window).height();
    this.focus=function(){
        $('.search_input').focusin(function(){
            $('.header_bg').addClass('search_bg');
            $(".zh_container").css({"height":_height});
            $('.hot_search,.back_btn').show();
            $('.up_level').hide();
        })
    }
    this.back=function(){
        $('.back_btn').on('click',function(){
            $('.header_bg').removeClass('search_bg');
            $('.zh_container').css({"height":'auto'});
            $('.hot_search,.back_btn').hide();
            $('.up_level').show();
        })
    }
}
</script>