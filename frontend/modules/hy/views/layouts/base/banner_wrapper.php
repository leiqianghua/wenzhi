<section class="banner_wrapper">
    <div class="swiper-container index_banner">
        <div class="swiper-wrapper">
            <?php  foreach($flash as $key => $value){ ?>
                <div class="swiper-slide">
                    <a  href="<?php echo $value['url'];  ?>"><img src="<?php echo $value['img'];  ?>"/></a>
                </div>
            <?php }  ?>                     

        </div>
        <div class="swiper-pagination banner_pagination"></div>
    </div>
</section>