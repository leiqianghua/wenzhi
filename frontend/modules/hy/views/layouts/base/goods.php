 <?php foreach($goods as $key => $good){  ?>
    <li>
        <a class="goods_item" href="<?= $this->context->to(['goods/index','gid' => $good['id']])  ?>">
            <span class="item_img">
                <img  class="item_pic" src="<?= \common\models\GoodsModel::getShopImg($good['img'], 240)  ?>">
            </span>
            <span class="detail_pirce item_info">
                <span class="item_name"><?= $good['name']  ?></span>
                <span class="item_price">
                    <i>¥</i><?= (int)($good['discount_4'])  ?>
                    <?php  if($good['discount_4'] < $good['price']){   ?>
                        <small>¥<em id="goods_marketprice"><?= $good['price'] ?></em></small>
                    <?php }  ?>
                    <?php  if($good['score']){ ?>
                    &nbsp;积分:<?php echo $good['score']; ?>
                    <?php }  ?>
                </span>
                <span class="item_num">已售<em><?= $good['sale_num']  ?></em>件&nbsp<span common_id='<?= $good['id'] ?>' class="add_cart  item_top"><i class="iconfont top_icon">&#xe608;</i></span></span>
                
            </span>
        </a>
    </li>    
<?php }  ?>