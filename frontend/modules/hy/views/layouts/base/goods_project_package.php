 <?php foreach($goods as $key => $good){  ?>
    <li>
        <a class="goods_item" href="<?= $this->context->to(['goods/index_project','gid' => $good['id']])  ?>">
            <span class="item_img">
                <img  class="item_pic" src="<?= \common\models\GoodsModel::getShopImg($good['img'], 240)  ?>">
            </span>
            <span class="detail_pirce item_info">
                <span class="item_name"><?= $good['name']  ?></span>
                <span class="item_price">
                    <i>¥</i><?= (int)($good['price'])  ?>
                        <small>¥<em id="goods_marketprice"><?= $good['price'] ?></em></small>
                    
                </span>
<!--                <span class="item_num">已售<em><?= $good['sale_num']  ?></em>件</span>-->
            </span>
        </a>
    </li>    
<?php }  ?>
