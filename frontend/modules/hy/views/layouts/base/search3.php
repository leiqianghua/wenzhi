<!-- 分类页搜索弹窗 -->
<div id="classSearch_Wrapper">
    <div class="classSearch_cover"><span></span></div>
    <div class="classSearch_box">
        <form id="search_submit" action="<?= $this->context->to(['search/index']);  ?>">
        <header class="header_wrapper">
            
            <div class="header_bg search_bg"></div>
            <a class="classSearch_backBtn header_btn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
        <a class=" header_btn_i" href="javascript:void(0);"><?= $GLOBALS['city_name'] ?></a>
            <a class="user_center header_btn" href="javascript:void(0);" onclick="$('#search_submit').submit();">搜索</a>
            <div class="search_wrapper">
                <div class="search_box">
                    <input type="text" name="keyword" value="<?= empty($_GET['keyword']) ? '' : $_GET['keyword']; ?>" class="search_input" placeholder="搜索您想要的商品">
                    <a class="search_btn" href="javascript:$('#search_submit').submit();"><i class="iconfont">&#xe606;</i></a>
                </div>
            </div>
        </header>
        </form>
        <div class="hot_search class_hot_word">
            <h1>热门搜索</h1>
            <div class="hot_words">
                <a href="<?= $this->context->to(['search/index','keyword' => '家装']);  ?>">家装</a>
                <a href="<?= $this->context->to(['search/index','keyword' => '装饰']);  ?>">装饰</a>
                <a href="<?= $this->context->to(['search/index','keyword' => '室内装饰']);  ?>">室内装饰</a>
            </div>
    
    <?php  $datas = \common\models\GoodsLabelModel::getListMenu(2);  ?>
    <?php foreach($datas as $data){  ?>
            <h1><?= $data['title']  ?></h1>
            <div class="label_list hot_words">
                <?php foreach($data['data'] as $k => $v){  ?>      
                    <a href="javascript:void(0)" data="<?= $v['id'] ?>"><?= $v['title']  ?></a>
                <?php }  ?>
            </div>
    <?php }  ?>      
    <div class="account_opt">
         <input class="up_btn select_ok"  value="确 认">
     </div>          
        </div>

      
        
    </div>
</div>

<script>
    $(function(){
        $(".label_list  a").click(function(e){
            e.preventDefault();
            if($(this).hasClass('select_label')){
                $(this).removeClass('select_label');
            }else{
                $(this).addClass('select_label');
            }
            return false;
        })
        
        $(".select_ok").click(function(){
            var objs = $(".select_label");
            var length = objs.length;
            var label = [];
            for(var i = 0;i< length;i++){
                label[i] = objs.eq(i).attr("data");
            }
            if(label.length == 0){
                alert("请至少选择一个标签");
            }
            var str_label = label.join(',');
            var url = "<?= $this->context->to(["label/index"]) ?>/?label_ids="+str_label;
            window.location.href=url;
        })
    })
</script>
<script type="text/javascript">
$(function(){
    //搜索列表
    var classSearch = new classSearchFun();
    classSearch.classSearchBtn();
    classSearch.classSearchBack();
})

//分类页 - 搜索
var classSearchFun = function(){
    this.classSearchBtn=function(){
        $('.class_search').on('click',function(){
            $('#classSearch_Wrapper').addClass('classSearch_show');
            $('.classSearch_cover').fadeIn();
            $('.search_input').focus();
            $('body').css({overflow:'hidden'})
        })
    };
    this.classSearchBack=function(){
        $('.classSearch_backBtn').on('click',function(){
            $('#classSearch_Wrapper').removeClass('classSearch_show');
            $('.classSearch_cover').fadeOut();
            $('.search_input').blur();
            $('body').css({overflow:'auto'})
        })
    }
}
</script>