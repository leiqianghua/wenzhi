            
<header class="header_wrapper fixed_header change_red">
<!--    <div><img width="100px" height="100px" src="/images/logo.png" /></div>-->
    <form id="search_index" action="<?= $this->context->to(['search/index']);  ?>">
        <div class="header_bg"></div>
        <a class="header_btn" href="<?=  $this->context->to(['index/category']) ?>"><i class="iconfont">&#xe609;</i></a>
        
        <a class=" header_btn_i" href="javascript:void(0);"><?= $GLOBALS['city_name'] ?></a>
        <a class="user_center header_btn" href="<?= $this->context->to(['member/index']);  ?>"><i class="iconfont">&#xe607;</i></a>
        <a class="search_unit header_btn" href="javascript:void(0);" onclick="$('#search_index').submit();">搜索</a>
        <a class="back_btn header_btn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
        <div class="search_wrapper">
            <div class="search_box">
                <input type="text" name="keyword" value="<?= empty($_GET['keyword']) ? '' : $_GET['keyword']; ?>" class="search_input" placeholder="搜索您想要的商品">
                <a class="search_btn" href="javascript:void(0);" onclick="$('#search_index').submit();"><i class="iconfont">&#xe606;</i></a>
            </div>
        </div>        
    </form>
</header>

<div class="hot_search">
    <h1>热门搜索</h1>
    <div class="hot_words">
        <a href="<?= $this->context->to(['search/index','keyword' => '家装']);  ?>">家装</a>
        <a href="<?= $this->context->to(['search/index','keyword' => '装饰']);  ?>">装饰</a>
        <a href="<?= $this->context->to(['search/index','keyword' => '室内装饰']);  ?>">室内装饰</a>
    </div>
    
    
    <?php  $datas = \common\models\GoodsLabelModel::getListMenu(2);  ?>
    <?php foreach($datas as $data){  ?>
            <h1><?= $data['title']  ?></h1>
            <div class="label_list hot_words">
                <?php foreach($data['data'] as $k => $v){  ?>      
                    <a href="javascript:void(0)" data="<?= $v['id'] ?>"><?= $v['title']  ?></a>
                <?php }  ?>
            </div>
    <?php }  ?>     
    
           <div class="account_opt">
                <input class="up_btn select_ok"  value="确 认">
            </div>      
</div>


<script>
    $(function(){
        $(".label_list  a").click(function(e){
            e.preventDefault();
            if($(this).hasClass('select_label')){
                $(this).removeClass('select_label');
            }else{
                $(this).addClass('select_label');
            }
            return false;
        })
        
        $(".select_ok").click(function(){
            var objs = $(".select_label");
            var length = objs.length;
            var label = [];
            for(var i = 0;i< length;i++){
                label[i] = objs.eq(i).attr("data");
            }
            if(label.length == 0){
                alert("请至少选择一个标签");
            }
            var str_label = label.join(',');
            var url = "<?= $this->context->to(["label/index"]) ?>/?label_ids="+str_label;
            window.location.href=url;
        })
    })
</script>


<script>
$(function(){
    //搜索
    var searchEvent = new searchFun();
    searchEvent.focus();
    searchEvent.back();
    readyData();
    
    function readyData()
    {
        var html = $("#menu_tab").html();
        if(!html){
            $.post('<?= $this->context->to(['ajaxgetcid']);  ?>',{},function(data){
                if(data.status){
                    $("#menu_tab").html(data.content);   
                }
            },'json');
        }else{
            $('#menu_tab').addClass('show_menu');
            $('.tab_cover').fadeIn();             
        }         
    }
    
    
    $(".category_menu").click(function(){
        readyData();        
    })
})

//首页搜索
var searchFun = function(){
    var _height=$(window).height();
    this.focus=function(){
        $('.search_input').focusin(function(){
            $('.header_bg').addClass('search_bg');
            $(".zh_container").css({"height":_height});
            $('.hot_search,.back_btn').show();
            $('.category_menu').hide();
            $('.user_center').hide();
            $('.search_unit').show();
        })
    }
    this.back=function(){
        $('.back_btn').on('click',function(){
            $('.header_bg').removeClass('search_bg');
            $('.zh_container').css({"height":'auto'});
            $('.hot_search,.back_btn').hide();
            $('.category_menu').show();
            $('.user_center').show();
            $('.search_unit').hide();
            $('.header_wrapper').addClass('change_red');
        })
    }
}
</script>