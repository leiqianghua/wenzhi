<script>
changeAddress();
function changeAddress(){
   var _height=$(window).height();
   var _height2 = _height - 45;   // 选择地区高度
   $(document).on('click', '.select_tigger', function() {   //选择地区
        $('#areaWrapper').addClass('area_show');
        $('.area_cover').fadeIn();
        $("#areaList").css({"height":_height2})
    })
    $(document).on('click',".area_backBtn",function(){
        $('#areaWrapper').removeClass('area_show');
        $('.area_cover').fadeOut();
        $('#areaList').css({"height":'auto'})
    })
} 
</script>


<div id="areaWrapper">
    <div class="area_cover"><span></span></div>
    <div class="area_box">
        <header class="page_header">
            <div class="page_headerBg"></div>
            <a class="area_backBtn page_headerBtn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
            <div class="page_title"><h1>商品选择</h1></div>
        </header>
        <section id="areaList" class="area_list">
<!--            展示具体内容-->
        <?php  require __DIR__ . '/show_goods_list.php';  ?>
        </section>
    </div>
</div> 