<?php

namespace frontend\modules\wap\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AddressAsset extends AssetBundle
{
    public $basePath = '@webroot/resource';
    public $baseUrl = '@web/resource';
    public $css = [
//        'css/style.css',
//        'js/Validform_v5.3.2/css/style.css',
    ];
    public $js = [
        'js/area_array.js',
        'js/selectArea.js',
    ];
    public $depends = [
        'frontend\modules\wap\assets\AppAsset',
    ];
}
