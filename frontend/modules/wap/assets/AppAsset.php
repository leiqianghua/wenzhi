<?php

namespace frontend\modules\wap\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/resource';
    public $baseUrl = '@web/resource';
    public $css = [
        'css/style.css',
    ];
    public $js = [
        'js/layer.m/layer.m.js',
        'js/main.js',
    ];
//    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//    ];
}
