<?php

namespace frontend\modules\wap\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FormAsset extends AssetBundle
{
    public $basePath = '@webroot/resource';
    public $baseUrl = '@web/resource';
    public $css = [
        'js/Validform_v5.3.2/css/style.css',
        'bootstrap-3.3.5/css/bootstrap.min.css',
    ];
    public $js = [
        'js/Validform_v5.3.2/js/Validform_v5.3.2_min.js',
        'bootstrap-3.3.5/js/bootstrap.min.js',
    ];
    public $depends = [
        'frontend\modules\wap\assets\AppAsset',
    ];
}
