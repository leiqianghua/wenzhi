<?php

namespace frontend\modules\wap\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SwiperAsset extends AssetBundle
{
    public $basePath = '@webroot/resource';
    public $baseUrl = '@web/resource';
    public $css = [
        'css/swiper.min.css',
    ];
    public $js = [
        'js/swiper-3.2.5.jquery.min.js',
    ];
//    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//    ];
}
