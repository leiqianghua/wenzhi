<?php
//各应用都需要继承的控制器基类。  包括各增删改查基类控制器
namespace frontend\modules\wap\components;


use yii;
//use common\components\BaseController;
class BaseController extends \common\components\BaseController 
{
    public $title;
    public function init()
    {
        parent::init();
        $this->layout = 'main';
        $this->user = yii::$app->modules['wap']->user;
        $this->user->isLogin();
        //$data = \common\components\GuestInfo::getDataForIp();
        if(!empty($data['city'])){
            $GLOBALS['city_name'] = $data['city'];
        }else{
            $GLOBALS['city_name'] = "全国";
        }
    }
    
    public function template($layout)
    {
        return \yii::getAlias('@frontend/modules/wap/views/') . $layout . '.php';
    }
    
    protected function queryCart()
    {
        if ($_COOKIE('cart_goods_num') != null && intval($_COOKIE('cart_goods_num')) >= 0) {
            $cart_num = intval($_COOKIE('cart_goods_num'));
        } else {
            //已登录状态，存入数据库,未登录时，优先存入缓存，否则存入COOKIE
            if ($GLOBALS['control']->member->uid) {
                $save_type = 'db';
            } else {
                $save_type = C('cache.type') != 'file' ? 'cache' : 'cookie';
            }
            $cart_num = Model('cart')->getCartNum($save_type, array('buyer_id' => $GLOBALS['control']->member->uid));
        }
        Tpl::output('cart_goods_num', $cart_num);
    }
    
    //获取基础的数据
    public function baseGetDataForList($datas,$base_list,$title = '',$params = [])
    {
        $this->title = $title;
        $this->baseGetDataForListAJAX($datas,$base_list,$params);        
        $this->assign('datas',$datas);
        $this->assign($params);
        return $this->render();        
    }
    
    private function baseGetDataForListAJAX($datas,$base_list,$params = [])
    {
        //是否是ajax的数据
        if(!yii::$app->request->isAjax){
            return;
        } 
        $this->assign($params);
        if($datas){
            $content = $this->renderPartial($base_list,['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }           
    }
    
    public function go_hy()
    {
        $this->user->updateToken();
        $arr = $this->user->getMember();
        

//        $m = \common\models\AdminMember::findOne(['frontend_uid' => $this->user->uid]);
//        if($m){
//            $role = \common\models\hy_designer_levelModel::getBaseKeyValue("studio_level_id", "studio_level_name", ['uid' => $this->user->uid]);
//            $arr['role_id'] = json_encode($role);
//        }else{
//            $arr['role_id'] = "";
//        }        
        $arr[0] = '/hy/#/pages/login/wx_trust';
        \common\components\TmpLog::addData("dddddd", $arr);
        if(isset($arr['password'])){
            unset($arr['password']);
        }
        return $this->redirect($arr);//这里走一个中间页TODO        
    }
    
    public function go_hy_geturl()
    {
        $this->user->updateToken();
        $arr = $this->user->getMember();
        
//        $m = \common\models\AdminMember::findOne(['frontend_uid' => $this->user->uid]);
//        if($m){
//            $role = \common\models\hy_designer_levelModel::getBaseKeyValue("studio_level_id", "studio_level_name", ['uid' => $this->user->uid]);
//            $arr['role_id'] = $role;
//        }else{
//            $arr['role_id'] = [];
//        }         
        
        $arr[0] = '/hy/#/pages/login/wx_trust'; 
        \common\components\TmpLog::addData("ddddddss", $arr);
        if(isset($arr['password'])){
            unset($arr['password']);
        }
        $url = $this->to($arr);
        $this->success('登录成功',$url);
    }
}
