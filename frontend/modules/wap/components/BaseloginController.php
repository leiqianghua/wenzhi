<?php
//各应用都需要继承的控制器基类。  包括各增删改查基类控制器
namespace frontend\modules\wap\components;


use yii;
//use common\components\BaseController;
class BaseloginController extends BaseController 
{
    public function init()
    {
        parent::init();
        if(!$this->user->isLogin()){
            //这个页面是需要登录的，这个时候我记住这个页面，等下次的时候我再把其恢复过来看是否是可以的》
            $url = Yii::$app->request->getHostInfo().Yii::$app->request->url;
            \common\components\GuestInfo::setData($url);
            //echo \common\components\GuestInfo::getData();exit;
            $this->success("跳到登录",'/wap/public/login');
            //return $this->redirect('/wap/public/login');
        }        
    }
}