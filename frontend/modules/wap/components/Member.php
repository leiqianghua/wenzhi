<?php
/*
 * WAP会员   通过网页来注册的  微信或者PC都是适用
 */
namespace frontend\modules\wap\components;
use Exception;
use common\components\Encrypt;
use common\models\MemberModel;
use common\components\BaseMember;
class Member extends BaseMember
{
    public $mine_cookie = 'new_mall_user';//普通的key信息,保存的cookie信息   
    public $str_model;
    
    public function init()
    {
        parent::init();
        $this->str_model = "\common\models\MemberModel";//对应的会员数据模型的对象。
    }
    
    // third_party 0 or 1 是否是第三方登录
    //其它
    public function login($datas)
    {
        //try {
            if(empty($datas) || !is_array($datas)){
                throw new Exception('数据传送不能为空');
            }  
            if(isset($datas['third_party']) && $datas['third_party'] == 1){//第三方登录
                return $this->loginforSanfan($datas);
            }else{//非第三方登录
                return $this->loginforUser($datas);
            }
        //} catch (Exception $exc) {
        //    $this->error = $exc->getMessage();
        //    return false;
        //}
    }
    
    //非第三方登录
    //$datas 参数 参考login方法
    // type  username phone   默认为 phone
    // password  必填
    // name 必填  结合 type 确定是手机号，用户名 或其它
    protected function loginforUser($datas)
    {
        $str_model   = $this->str_model;
        try {
            if(empty($datas['password']) || empty($datas['name'])){
                throw new Exception('数据参数非法');
            }
            $datas['login_type'] = empty($datas['login_type']) ? 'phone' : $datas['login_type'];
            switch ($datas['login_type']) {
                case 'username'://通过用户名来登录
                    $member_info = $str_model::find()->asArray()->where(['username' => $datas['name']])->limit(1)->one();
                    break;
                default:
                    $member_info = $str_model::find()->asArray()->where(['iphone' => $datas['name']])->limit(1)->one();
                    break;
            }
            if(empty($member_info)){
                throw new Exception('找不到用户');
            }
            if($member_info['password'] != $this->createPassword($datas['password'])){
                throw new Exception('密码错误');
            }
            if(!$member_info['status']){
                throw new Exception('账号已被冻结');
            }            
            return $this->afterLogin($member_info,$datas);//登录进去后
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    
    //确认用户可以登录后的操作       或者需要让某一个用户登录的时候可以用到
    //$member_info 登录获取到的用户数据
    //$datas 用户传进来数据
    protected function afterLogin($member_info,$datas)
    {
        try {
            $str_model   = $this->str_model;
            if($member_info['uid']){
                $str_model::updateAll(['last_login_time' => TIMESTAMP],['uid' => $member_info['uid']]);
            }            
            //创建相关的cookie信息
            $this->addCookie($member_info['uid']);
            $this->uid = $member_info['uid'];
            $this->member_info = $member_info;
            
            \common\models\GoodsCartModel::afterLoginCallback($member_info,[]);//购物车的回调 
            
            return $member_info;
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    
    protected function addCookie($uid)
    {
        $value = Encrypt::authcode($uid, '');//加密
        setcookie($this->mine_cookie, $value, time()+86400, '/');         
    }
    
    //如果有绑定某一个用户，则直接登录这个用户，如果没有的话则让其进行绑定用户
    protected function loginforSanfan($datas)
    {
        $str_model   = $this->str_model;
        try {
            $datas['type'] = empty($datas['type']) ? 'weixin' : $datas['type'];
            if(empty($datas['reg_key'])){
                throw new Exception('参数有误');
            }
            
            //是否有绑定某一个用户
            $oauth_model = \common\models\MemberOauthDataModel::findOne(['oauth_key' => $datas['reg_key'],'oauth_type' => $datas['type']]);
            if($oauth_model){//有绑定某一个用户
                //直接让其登录这个用户
                $member_info = MemberModel::getOne($oauth_model['uid']);
                //找不到这个用户，那么就删除掉这一条记录，然后返回false
                if(!$member_info){
                    $oauth_model->delete();
                    return false;
                }
                return $this->afterLogin($member_info,[]);//登录进去后
            }else{//没有绑定用户
                //跳转让其绑定某一个用户，然后完成绑定
                return false;
            }
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    
    
    
    //第三方的登录方法
    // type  qq sina weixin wx_number 等 默认 weixin
    // reg_key  必填  第三方的标识
    //其它相关
    protected function loginforSanfan_bak($datas)
    {
        $str_model   = $this->str_model;
        try {
            $datas['type'] = empty($datas['type']) ? 'weixin' : $datas['type'];
            if(empty($datas['reg_key'])){
                throw new Exception('参数有误');
            }

            switch ($datas['type']) {
                case 'weixin':
                case 'qq':
                case 'sina':
                case 'wx_number':
                    $member_info = $str_model::find()->asArray()->where(['reg_key' => $datas['reg_key'], 'type' => $datas['type']])->limit(1)->one();
                    break;

                default:
                    break;
            }
            if(empty($member_info)){ //找不到用户的话则创建相关的用户
                $member_info = $this->registerSanfan($datas);//注册用户
            }
            
            if(!$member_info['status']){
                throw new Exception('账号已被冻结');
            }   
            return $this->afterLogin($member_info,$datas);//登录进去后
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }
    //注册第三方用户的登录
    protected function registerSanfan($datas)
    {
        $member_model = new MemberModel();
        $member_model->loadDefaultValues();
        $member_model->attributes = $datas;
        $member_model['exten_code'] = $this->createExtenCode();
        $member_model['username'] = empty($datas['username']) ? $datas['type']  . $member_model['exten_code'] : $datas['username'];
        $member_model['add_time'] = $member_model['last_login_time'] = TIMESTAMP;
        
        $is_state = $member_model->save();
        if(!$is_state){
            throw new Exception($member_model->getOneError());
        }

        //保存额外的数据
//        $member_extend = [];
//        $member_extend['uid'] = $member_model['uid'];
//        $member_extend['channel'] = 'wap';
//        $member_extend_model = new \common\models\Member_extendModel(); 
//        $member_extend_model->attributes = $member_extend;
//        $member_extend_model->save(false);
        $this->registerAfter($member_model);//注册好了后的回调 
        return $member_model->attributes;
    }
    
    //创建推广ID
    public function createExtenCode()
    {
        while(true){
            $extenCode=rand(1000000000,9999999990);
            $is_state = MemberModel::find()->where(['exten_code' => $extenCode])->exists(); 
            if(!$is_state){
                return $extenCode;
            }
        }  
    }    
    
    
    //用户注册
    //iphone password 两个选项    手机号与密码
    //code 选填
    public function register($datas)
    {
        try {
            /***********************   验证     ************************/
            if(empty($datas['iphone']) || empty($datas['password'])){
                throw new Exception('手机号或密码不能为空');
            }
            
            if(strlen($datas['password'])<6 || strlen($datas['password'])>16){
                throw new Exception('密码小于6位或大于16位！');
            }
            if(preg_replace('/[a-zA-Z0-9]*/',"",$datas['password'])!=''){
                throw new Exception('密码只能使用英文和数字！');
            }
            //是否存在此手机号
            $has_iphone = MemberModel::find()->where(['iphone' => $datas['iphone']])->exists();
            if($has_iphone){
                throw new Exception('该手机已被使用！');
            }
            //是否是存在验证码的验证
//            if(isset($_POST['code'])){
//                $sns = new \common\components\SnsVerification;
//                $state = $sns->verification($_POST['code'], $_POST['iphone']);    
//                if(!$state){
//                    throw new Exception('手机短信验证码不正确！');
//                }
//            }
            
            
            
            /***********************   验证完成     ************************/
            
            $member_data = [];
            $member_data['username'] = empty($datas['username']) ? $datas['iphone'] : $datas['username'];
            $member_data['img'] = empty($datas['img']) ? '' : $datas['img'];
            $member_data['iphone'] = $datas['iphone'];
            $member_data['type'] = 'phone';
            $member_data['exten_code'] = $this->createExtenCode();
            $member_data['add_time'] = TIMESTAMP;
            $member_data['last_login_time'] = TIMESTAMP;
            $member_data['password'] = $this->createPassword($datas['password']);
            $member_model = new \common\models\MemberModel();
            $member_model->attributes = $member_data;
            $is_state = $member_model->save();
            if(!$is_state){
                throw new Exception($member_model->getOneError());
            }
            
            $this->registerAfter($member_model);//注册好了后的回调 
            return $this->afterLogin($member_model->attributes, []);
            
        } catch (Exception $exc) {
            $this->error = $exc->getMessage();
            return false;
        }
    }

    //注册回调
    public function registerAfter($member_model)
    {
        //看是否有推广码
        if(!empty($_COOKIE['exten_code'])){
            $exten_code = \common\components\Encrypt::authcode($_COOKIE['exten_code']);
            if($exten_code){
                $model = MemberModel::find()->where(['exten_code' => $exten_code])->limit(1)->one();
                if($model){
                    $member_model->parent_id = $model->uid;
                    //$member_model->label = $model->label + 1;
                    $member_model->label = 4;
                    $member_model->save(false);
                    //print_r($member_model);exit;
                }
            }
        }
        return true;
    }
    
    public function isLogin()
    {
        if(empty($_COOKIE[$this->mine_cookie])){
            return false;
        }
        $uid = Encrypt::authcode($_COOKIE[$this->mine_cookie], 'DECODE');//解密
        if(!$uid){
            return false;
        }
        $this->uid = $uid;
        return $uid;
    }
    
    //退出
    public function logout()
    {
        if(!$this->uid){
            return false;
        }
        setcookie($this->mine_cookie, '', 1,'/');
        return true;
    } 
    
    
    //获取用户数据
    public function getMember()
    {
        try {
            if(!$this->uid){
                throw new Exception('用户没有登录');
            }
            if($this->member_info){
                return $this->member_info;
            }
            $member = MemberModel::findOne($this->uid);
            if(!$member){
                $this->logout();
                throw new Exception('找不到相关用户');
            }
            $this->member_info = $member->attributes;;
            return $this->member_info;
        } catch (Exception $exc) {
            return [];
        }
    }
    
    
    //加密操作
    public function createPassword($password)
    {
        return md5($password);
    }    
    
    
    
    /** 
     * google api 二维码生成【QRcode可以存储最多4296个字母数字类型的任意文本，具体可以查看二维码数据格式】 
     * @param string $chl 二维码包含的信息，可以是数字、字符、二进制信息、汉字。 
     不能混合数据类型，数据必须经过UTF-8 URL-encoded 
     * @param int $widhtHeight 生成二维码的尺寸设置 
     * @param string $EC_level 可选纠错级别，QR码支持四个等级纠错，用来恢复丢失的、读错的、模糊的、数据。 
     * L-默认：可以识别已损失的7%的数据 
     * M-可以识别已损失15%的数据 
     * Q-可以识别已损失25%的数据 
     * H-可以识别已损失30%的数据 
     * @param int $margin 生成的二维码离图片边框的距离 
     */
    function generateQRfromGoogle($chl,$widhtHeight ='150',$EC_level='L',$margin='0') 
    { 
     $chl = urlencode($chl); 
     echo '<img src="http://chart.apis.google.com/chart?chs='.$widhtHeight.'x'.$widhtHeight.' 
     &cht=qr&chld='.$EC_level.'|'.$margin.'&chl='.$chl.'" alt="QR code" widhtHeight="'.$widhtHeight.'
     " widhtHeight="'.$widhtHeight.'"/>'; 
    } 
    
    public function updateToken()
    {
        if(!$this->uid){
            return true;
        }
        $this->getMember();
        $str_model = $this->str_model;
        $access_token = md5(time() . rand(1, 9999999));
        $str_model::updateAll(['last_login_time' => TIMESTAMP,"access_token" => $access_token,"express_time" => time()+86400*30],['uid' => $this->uid]);
        
        $m = \common\models\AdminMember::findOne(['frontend_uid' => $this->uid]);
        if($m){
            $role = \common\models\hy_designer_levelModel::getBaseKeyValue("studio_level_id", "studio_level_name", ['uid' => $this->uid]);
            $keys = array_keys($role);
            $this->member_info['role_id'] = implode(',', $keys);
        }else{
            $this->member_info['role_id'] = "0";
        }        
        
        $this->member_info['access_token'] = $access_token;
        return true;
    }
    

}