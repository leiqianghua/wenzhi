<?php
//回调的一些处理
namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use yii;
use common\components\pay\run\Alipaywap;
use common\components\pay\run\Alipay;
/**
 * Default controller for the `wap` module
 */
class AliwappayController extends BaseController
{
//    public function init()
//    {
//        parent::init();
//        //支付宝相关的处理文件引入
//        $path = yii::getAlias('@common/components/pay/alipayapi.php');
//        if (!file_exists($path)) {
//            $this->error('接口出错');
//        }
//        require_once($path);        
//    }
    

    //异步通知
    public function actionNotify()
    {
        
        
        
        $alipay = new aliwappay();
        $alipay->alipayNotify();
    }

    //返回
    public function actionReturn()
    {
        $alipay = new aliwappay();
        //取得返回地址
        $url = Unit::getCookie('wappay_return_url');
        if ($url) {
            Unit::removeCookie('wappay_return_url');
        } else {
            $url = Unit::U('index/index');
        }
        if ($alipay->alipayReturn() === true) {
            redirect($url);
        } else {
            redirect(Unit::U('index/index'));
        }
    }    
}
