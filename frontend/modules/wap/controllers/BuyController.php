<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\ElseCarouselModel;
use common\models\GoodsModel;
use common\components\GuestInfo;
use yii;
use common\models\GoodsClassModel;
use common\models\GoodsCartModel;
use common\models\MemberAddressModel;
use common\models\OrderModel;
use common\components\pay\run\wxpay\jsapi\Jsapi;
/**
 * Default controller for the `wap` module
 */

class BuyController extends BaseController {

    public function init()
    {
        parent::init();
        $this->layout = false;
//        $uid = yii::$app->user->isLogin();
//        print_r($_SERVER);exit; 
//        if(!$uid && $this->action->id != "payok"){
//            $this->error('你未登录');
//        }          
    }

    
    //微信支付页面
    public function _weixin()
    {
        $order_id = $_GET['order_id'];//订单号
        setcookie('order_id', $order_id, time()+86400,'/');
        $pay_url = '/weixin/index_weixin_wap.php';
        @header('Location: ' . $pay_url, true, 302);
        exit;
        return $this->redirect($pay_url);
    }      
    
    
    //微信支付页面
    public function actionWeixin()
    {
        $order_id = $_COOKIE['order_id'];
        if(!$order_id){
            $this->error('参数错误');
        }
        
        $model = OrderModel::find()->where(['order_sn' => $order_id])->limit(1)->one();
        if(!$model){
            $this->error('找不到相关的信息');
        }
        
        $data = [];
        $data['body'] = '古笆兔商城商品';
        $data['out_trade_no'] = $order_id;
        $data['total_fee'] = $model['order_amount'] * 100;//总金额,腾讯默认支付金额单位为【分】
        $data['ming_return_url'] = '/wap/buy/payok';
        $obj = new Jsapi($data);
        $js = $obj->getJs();
        $this->assign('money',$model['order_amount']);
        $this->assign('js',$js);
        echo $this->render();
        exit;
        
        
        //根据其规则确定好所需要展示的内容与JS。
        $inc_file = BASE_ROOT_PATH . "/api/pay/wxpay/wxpay.php";
        if (!file_exists($inc_file)) {
            echo "接口错误，无法完成支付！";
        }
        require_once($inc_file);
        
        
        //任何一个支付所必须传的参数
        $data = [];
        $data['member_id'] = $this->member->member_id;
        $data['pay_amount'] = $result['pay_amount'];
        $data['subject'] = '微信支付';
        $data['order_type'] = 'product_buy';
        $data['pay_sn'] = $pay_sn;
        $payment_api = new wxpay($data);//传送支付订单信息
        $openId = $payment_api->GetOpenid();
        //获取支付参数
        $jsApiParameters = $payment_api->GetParameter($openId);
        if ( empty($jsApiParameters) ) {
            echo "空数据!";exit;
        }
        
        
        $return_url = Unit::U('buy/pay_ok', ['pay_sn' => $pay_sn]);
        Tpl::output('return_url', $return_url);
        Tpl::output('jsApiParameters', $jsApiParameters);
        $this->assign($result);
        Tpl::showpage('weixin');         
    }    
    
    
    function actionPayok()
    {
        $this->success('恭喜你支付成功',$this->to('/wap/index/index'));
    }
    
    
    //除了微信支付正常的其它的界面
    //选择支付方式页面
    //支付页面
    public function actionPay()
    {
        //看是否有这笔订单
        if(\common\components\GuestInfo::isWeixinRequest()){
            $this->_weixin();
            exit;
        }
        $pay_sn = $_GET['order_id'];
        $order = OrderModel::checkPay($pay_sn);
        if(!$order){
            $this->error('请求出错');
        }
        
        $this->assign('order',$order);
        $this->assign('pay_sn',$pay_sn);
        return $this->render();
    }    
    
    
    /*
     *  POST的数据
     * is_cart 是否是购物车
     * invalid_cart 失效的ID列表
     * cart_list  我们选中的这个ID列表   购物车过来的时候才会有
     * goods_id  商品ID    直接购物的
     * shop_list  直接购物才会有   多个的
     * $num 购物的数量   
     */ 
    public function actionBuy_step1()
    {
        $datas = GoodsModel::getDataForPost();//获取来源的数据
        if(!$datas){
            $this->redirect($this->to(['cart/index']));
        }
        $uid = yii::$app->user->uid;
        $address_info = MemberAddressModel::find()->where(['uid' => $uid])->orderBy('is_default desc')->asArray()->all();
        //选中的收货地址设定
        if($address_info){
            $address_info[0]['is_default'] = 1;
        }
        $this->assign('datas',$datas);
        //print_r($datas);exit;
        $this->assign('ifcart',$_POST['ifcart']);        
        $this->assign('address_info', $address_info);
        
        $total_money = GoodsModel::calcTotalMoney($datas);
        $this->assign('total_money', $total_money);//总价 是一个数组，包括折扣价与原价
        
        $address_id = empty($address_info[0]['id']) ? 0 : $address_info[0]['id'];
        
        $freight = GoodsModel::calcFreight($address_id,$datas);
        $this->assign('freight', $freight);//运费
        
        exit($this->render());
        //Tpl::showpage('buy_step1');
    }
    //计算运费
    public function actionCalcfreight()
    {
        $address_id = $_POST['address_id'];
        $_POST = json_decode($_POST['mine_post'],true);
        $freight = GoodsModel::calcFreight($address_id);
        $this->ajaxReturn(['content' => $freight]);
    } 

    /**
     * 生成订单     中转页  或者跳到支付页面，或者跳到支付成功页面
     * [提交订单]
     * 增加了  [coupon] => Array ( [0] => 2 [1] => 3 ) )  使用了哪些的优惠券的一个东西。。。。。
     */
    public function actionBuy_step2()
    {
        $coupon = empty($_POST['coupon']) ? '' : $_POST['coupon'];
        $config = [
            'detail_data' => $_POST['mine_post'],
            'type' => 2,
            'uid' => $this->user->uid,
            'red_ids' => $coupon,
            'address_id' => $_POST['address_id'],
        ];
        \common\components\TmpLog::addData("user", $config);
        $obj = new \common\components\buy\GoodsBuy($config);
        $code = $obj->buy();
        if(!$code){
            $this->error($obj->error);
        }
        if(strpos($code, "C_S") === false){//这里是直接已经成功了
            $this->success("支付成功","/wap/member_order/index");
        }else{//这里则是需要跳转到第三方去支付
            $this->redirect(['new_pay/index','order_id' => $code]);
        }
    }
    
    //除了微信支付正常的其它的界面
    //选择支付方式页面
    //支付页面
    public function payOp()
    {
        if(Model('buy')->is_weixin()){
            $this->_weixin();
            exit;
        }
        $pay_sn = unit::I('pay_sn');
        $buy_model = Model('buy');
        $result = $buy_model->pay($pay_sn);
        if(!$result){
            $this->error($buy_model->getOneError(),Unit::U('member_order/index'));
        }
        if($result['pay_amount_online'] <= 0){
            redirect(Unit::U('buy/pay_ok', ['pay_sn' => $pay_sn, 'pay_amount' => $result['pay_amount']]));
        }
        $this->assign($result);
        $this->assign('pay_sn',$pay_sn);
        Tpl::output('buy_step', '3');
        Tpl::showpage('buy_step2');        
    }

    



    /**
     * 预存款充值下单时支付页面
     */
    public function pd_payOp()
    {
        $pay_sn = $_GET['pay_sn'];
        if (!preg_match('/^\d{18}$/', $pay_sn)) {
            showMessage(Language::get('para_error'), Unit::U('predeposit/index'), 'html', 'error');
        }

        //查询支付单信息
        $model_order = Model('predeposit');
        $pd_info = $model_order->getPdRechargeInfo(array('pdr_sn' => $pay_sn, 'pdr_member_id' => $_SESSION['member_id']));
        if (empty($pd_info)) {
            showMessage("请选择充值方式!", '', 'html', 'error');
        }
        if (intval($pd_info['pdr_payment_state'])) {
            showMessage('您的订单已经支付，请勿重复支付', Unit::U('predeposit/index'), 'html', 'error');
        }
        Tpl::output('pdr_info', $pd_info);

        //显示支付接口列表
        $model_payment = Model('payment');
        $condition = array();
        $condition['payment_code'] = array('not in', array('offline', 'predeposit'));
        $condition['payment_state'] = 1;
        $payment_list = $model_payment->getPaymentList($condition);
        Tpl::output('payment_list', $payment_list);

        //标识 购买流程执行第几步
        Tpl::output('buy_step', '3');
        Tpl::setLayout('member_layout');
        Tpl::showpage('predeposit_pay');
    }

    /**
     * 支付成功页面
     */
    public function pay_okOp()
    {
        $pay_sn = unit::I('pay_sn');
        $buy_model = Model('buy');
        $result = $buy_model->payOk($pay_sn);
        if(!$result){
            showMessage($buy_model->getOneError(), Unit::U('member_order/index'), 'html', 'error');
        }
        
        $pay_amount = Unit::I('pay_amount', '', 'floatval');
        $result['pay_info']['pay_amount'] = ncPriceFormat($pay_amount);
        $this->assign($result);
        Tpl::output('buy_step', '4');
        Tpl::showpage('buy_step3');
    }

    /**
     * 加载买家收货地址
     *
     */
    public function load_addrOp()
    {
        $model_addr = Model('address');
        $member_id = Model('member')->getCookieUid();
        $id = unit::I('id', 'intval');
        //如果传入ID，先删除再查询
        if (!empty($id) && $id > 0) {
            $model_addr->delAddress(array('address_id' => $id, 'member_id' => $member_id));
        }
        $list = $model_addr->getAddressList(array('member_id' => $member_id));
        Tpl::output('address_list', $list);
        Tpl::showpage('buy_address.load', 'null_layout');
    }

    /**
     * 选择不同地区时，异步处理并返回每个店铺总运费以及本地区是否能使用货到付款
     * 如果店铺统一设置了满免运费规则，则运费模板无效
     * 如果店铺未设置满免规则，且使用运费模板，按运费模板计算，如果其中有商品使用相同的运费模板，则两种商品数量相加后再应用该运费模板计算（即作为一种商品算运费）
     * 如果未找到运费模板，按免运费处理
     * 如果没有使用运费模板，商品运费按快递价格计算，运费不随购买数量增加
     */
    public function actionChange_addr()
    {
        $this->ajaxReturn(['state' => 'fail']);
    }

    //获取单条地址信息
    public function get_addressOp()
    {
        $model_addr = Model('address');
        $addr_id = Unit::I('addr_id', 'intval');
        $addr_info = $model_addr->getAddressInfo(array('member_id' => unit::session('member_id'), 'address_id' => $addr_id));
        if (!empty($addr_info)) {
            $addr_info['status'] = true;
            exit(json_encode($addr_info));
        } else {
            exit();
        }
    }

    //设置默认收货地址
    public function set_addr_defaultOp()
    {
        $model_addr = Model('address');
        $addr_id = Unit::I('addr_id', 'intval');
        $result = $model_addr->editAddress(array('is_default' => 1), array('member_id' => unit::session('member_id'), 'address_id' => $addr_id));
        if ($result) {
            exit(json_encode(array('status' => true, 'msg' => '设置默认地址成功')));
        }
        exit(json_encode(array('status' => false, 'msg' => '设置默认地址失败')));
    }

    //删除收货地址
    public function del_addrOp()
    {
        $model_addr = Model('address');
        $addr_id = Unit::I('addr_id', 'intval');
        $result = $model_addr->delAddress(array('address_id' => $addr_id));
        if ($result) {
            exit(json_encode(array('status' => true, 'msg' => '删除地址成功')));
        }
        exit(json_encode(array('status' => true, 'msg' => '删除地址失败')));
    }

    private function _validate_address_data()
    {
        $data = array();
        $data['member_id'] = Unit::session('member_id');
        $data['true_name'] = $_POST['true_name'];
        $data['area_id'] = intval($_POST['area_id']);
        $data['city_id'] = intval($_POST['city_id']);
        $data['area_info'] = $_POST['area_info'];
        $data['address'] = $_POST['address'];
        $data['tel_phone'] = $_POST['tel_phone'];
        $data['mob_phone'] = $_POST['mob_phone'];
        $data['is_default'] = intval($_POST['is_default']);
        return $data;
    }

    //修改收货地址
    public function update_addrOp()
    {
        $addr_id = Unit::I('addr_id', 0, 'intval');
        $model_addr = Model('address');

        if (chksubmit() && $addr_id !== 0) {
            $data = $this->_validate_address_data();
            //只允许一个默认地址, 先修改所有的地址为  非默认状态
            $result_id = $model_addr->editAddress($data, array('member_id' => $data['member_id'], 'address_id' => $addr_id));

            if ($result_id !== false) {
                exit(json_encode(array('status' => true, 'addr_id' => $addr_id, 'city_id' => $data['city_id'], 'is_update' => true, 'is_default' => (bool) $data['is_default'])));
            } else {
                $error = $model_addr->getError();
                empty($error) ? $error = '保存失败' : '';
                exit(json_encode(array('status' => false, 'msg' => $error)));
            }
        }
        $this->ajaxReturn(['status' => false, 'msg' => '提交数据为空']);
    }

    /**
     * 添加新的收货地址
     *
     */
    public function actionAdd_addr()
    {
        if ($_POST) {
            //$data = $this->_validate_address_data();
            $data = $_POST;
            $data['uid'] = yii::$app->user->uid;
            //只允许一个默认地址, 先修改所有的地址为  非默认状态
            if ($data['is_default'] === 1) {
                MemberAddressModel::updateAll(['is_default' => 0], ['uid' => $data['uid']]);
            }
            $model = new MemberAddressModel();
            $model->attributes = $data;
            $insert_id = $model->save(false);
            if ($insert_id) {
                $this->ajaxReturn(['status' => 1, 'address_id' => $model->id]);
            } else {
                $this->ajaxReturn(['status' => 0, 'info' => '添加新地址失败']);
            }
        }
        $this->ajaxReturn(['status' => 0, 'info' => '提交数据为空']);
    }


    /**
     * AJAX验证登录密码
     * 改成支付密码填写
     */
    public function check_pd_pwdOp()
    {
        if (empty($_GET['password']))
            exit('0');
        $model_member = Model('member');
        if (processClass::islock('pay_valite')) {
            echo '3'; //超过最大登陆次数
            exit;
        }
        $result = $model_member->verifyPaypwd(trim($_GET['password']));
        if ($result === false) {
            processClass::addprocess('pay_valite');
            echo '0';
            exit;
        } else {
            processClass::clear('pay_valite');
            echo '1';
            exit;
        }
    }
}
