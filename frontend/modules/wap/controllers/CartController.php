<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\ElseCarouselModel;
use common\models\GoodsModel;
use common\components\GuestInfo;
use yii;
use common\models\GoodsClassModel;
use common\models\GoodsCartModel;
/**
 * Default controller for the `wap` module
 */
class CartController extends BaseController
{
    public function init()
    {
        parent::init();
        $this->layout = false;
//        $uid = yii::$app->user->isLogin();
//        if(!$uid){
//            $this->error('你未登录');
//        }
    }


    public function actionDelete_fujia()
    {
        $model = GoodsCartModel::findOne($_GET['id']);
        if(!$model){
            $this->error("找不到数据");
        }
        $other_data = json_decode($model->other_data, true);
        if(!$other_data){
            $this->error("找不到相关附加费");
        } 
        if(empty($_GET['fujia_id'])){
            $this->error("找不到数据");
        }
        foreach($other_data as $key => $data){
            if($data == $_GET['fujia_id']){
                unset($other_data[$key]);
            }
        }
        
        if(empty($other_data)){
            $model->other_data = "";
        }else{
            $model->other_data = json_encode($other_data);
        }
        $model->save(false);
        $this->success("处理成功");
    }
    
    
    
    /**
     * 购物车首页
     */
    public function actionIndex()
    {
        $uid = yii::$app->user->isLogin();
        if(!$uid){
            $this->redirect(['public/login']);
        }       
        
        $result = [];
        $datas = GoodsCartModel::find()->where(['uid' => $this->user->uid])->asArray()->all();
        foreach($datas as $data){
            $result[$data['admin_id']]['data'][] = $data;
            if(!$data['admin_id']){
                $result[$data['admin_id']]['admin_name'] = "古笆兔自营";
            }else{
                $result[$data['admin_id']]['admin_name'] = \common\models\MemberModel::getOne($data['admin_id'], 'username');
            }
        }
        
        $this->assign("carts", $result);
        if($result){
            return $this->render();
        }else{
            return $this->render('empty');
        }        
    }

    /**
     * 购物车删除单个商品，未登录前使用goods_id，此时cart_id可能为0，登录后使用cart_id
     */
    public function actionDel()
    {
        
        $uid = yii::$app->user->isLogin();
        if(!$uid){
            $this->redirect(['public/login']);
        }        
        
        $cart_id = intval($_GET['cart_id']);
        
        if(!$cart_id){
            $thia->error('参数错误');
        }
        
        $model = GoodsCartModel::findOne($cart_id);
        if(!$model){
            $thia->error('找不到数据');
        }
        
        if($model['uid'] != $this->user->isLogin()){
            $thia->error('删除出错');
        }
        
        $model->delete();
        
        $data = [];
        $data['state'] = 1;
        $data['quantity'] = GoodsCartModel::getNum();
        $data['amount'] = GoodsCartModel::getCountMoney();
        $this->ajaxReturn($data);
    }
    public function actionDelall()
    {
        
        $uid = yii::$app->user->isLogin();
        if(!$uid){
            $this->redirect(['public/login']);
        }
        if(empty($_POST['data'])){
            $this->error('参数错误');
        }
        
        $num = GoodsCartModel::deleteAll(['uid' => $uid,"id" => $_POST['data']]);
        $this->success("删除成功");
    }    
    
    /**
     * 加入购物车，登录后存入购物车表
     * 登录前，如果开启缓存，存入缓存，否则存入COOKIE，由于COOKIE长度限制，最多保存5个商品
     * 未登录不能将优惠内部商城加入购物车，登录前保存的信息以goods_id为下标
     *
     */
    public function actionAdd()
    {        
        if(!empty($_GET['datas']) && is_array($_GET['datas'])){
            foreach($_GET['datas'] as $key => $value){
                $goods_id = intval($value['goods_id']);
                $quantity = intval($value['quantity']);    
                $model = new GoodsCartModel();
                $goods_info = GoodsModel::findOne($goods_id);
                $result = $model->addCart($goods_info,$quantity);                
            }
            $num = $result;
            $result = [];
            $result['num'] = $num;
            $result['status'] = 1;
            $this->ajaxReturn($result);            
        }
        //商品加入购物车(默认)
        if(!empty($_GET['shop_list'])){
            $goods_info = \common\models\GoodsPackageModel::findOne($_GET['goods_id']);
            if(!$goods_info){
                $this->error("数据异常");
            }
            $goods_info = $goods_info->attributes;
            $goods_info['shop_list'] = $_GET['shop_list'];     
            $model = new GoodsCartModel();
            if(empty($_GET['other'])){
                $_GET['other'] = '';
            }
            $result = $model->addCart($goods_info,1,$_GET['other']);         
        }else{
            if($common_id = GuestInfo::getParam("common_id")){
                $mm = GoodsModel::find()->where(['common_id' => $common_id])->limit(1)->one();
                if($mm){
                    $_GET['goods_id'] = $mm['id'];
                }
            }
            $goods_id = intval(GuestInfo::getParam("goods_id"));
            $quantity = intval(GuestInfo::getParam("quantity"));            
            $model = new GoodsCartModel();
            $goods_info = GoodsModel::findOne($goods_id);
            $result = $model->addCart($goods_info,$quantity);            
        }
        

        if(!$result){
            $this->error($model->getOneError());
        }else{
            $num = $result;
            $result = [];
            $result['num'] = $num;
            $result['status'] = 1;
            $this->ajaxReturn($result);
        }
    }
    
    public function actionUpdate()
    {
        //商品加入购物车(默认)
        $cart_id = intval($_GET['cart_id']);
        $quantity = intval($_GET['quantity']);     
        
        $model = GoodsCartModel::findOne($cart_id);
        $goods_model = GoodsModel::findOne($model['goods_id']);
        if($goods_model->num < $quantity){
            $this->error('库存不足');
        }
        if(!$goods_model->status){
            $this->error('商品已经下架');
        }
        
        GoodsCartModel::updateAll(['num' => $quantity], ['id' => $cart_id,'uid' => $this->user->uid]);
        $this->success('处理成功');
    }
    
    public function actionUpdate_package()
    {
        $data = explode(',', $_GET['cart_id']);
        if(count($data) > 1){
            $pack_id = $data['0'];
            $goods_id = $data['1'];
            $quantity = $_GET['quantity'];
        }else{
            return $this->actionUpdate();
        }     
        $d = \common\models\GoodsCartModel::findOne($pack_id);
        if(!$d){
            throw new Exception('找不到商品');
        }
        $shop = json_decode($d->shop_list,true);
        foreach($shop as $key => $value){
            if($shop[$key]['goods_id'] == $goods_id){
                $shop[$key]['quantity'] = $quantity;
            }
        }
        $shop = json_encode($shop);
        $d->shop_list = $shop;
        $state = $d->save(false);
        if($state){
            $this->success('修改成功');
        }else{
            $this->error('修改失败');
        }
    }
    
    public function actionDel_package()
    {
        return $this->actionDel();
        $data = explode(',', $_GET['cart_id']);
        if(count($data) > 1){
            $pack_id = $data['0'];
            $goods_id = $data['1'];
        }
        $d = \common\models\GoodsCartModel::findOne($pack_id);
        if(!$d){
            throw new Exception('找不到商品');
        }
        $shop = json_decode($d->shop_list,true);
        foreach($shop as $key => $value){
            if($shop[$key]['goods_id'] == $goods_id){
                unset($shop[$key]);
            }
        }
        if(empty($shop)){
            $d->delete();
            $this->success('修改成功');
        }
        $shop = json_encode($shop);
        
        $d->shop_list = $shop;
        $state = $d->save(false);
        if($state){
            $this->success('修改成功');
        }else{
            $this->error('修改失败');
        }
    }    
}
