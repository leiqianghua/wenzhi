<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\ElseCarouselModel;
use common\models\GoodsModel;
use common\components\GuestInfo;
use yii;
use common\models\GoodsClassModel;
use common\models\GoodsCartModel;
use common\models\GoodsPackageModel;
use common\components\TmpLog;
use common\components\Tool;
use Exception;
/**
 * Default controller for the `wap` module
 */
class CronController extends BaseController
{
    public function init()
    {
        parent::init();
        set_time_limit(0);
    }
    
    public function baseTit($is_lock,$is_transaction,$check_lock,$cache_key,$callback = 'tit',$params = [])
    {
        try {
            //锁
            if($is_lock){
                if($check_lock){
                    Tool::addLock($cache_key);//增加锁had
                }else{
                    $state = Tool::checkLock($cache_key);
                    if(!$state){//没有拿到锁则直接不处理 返回true
                        throw new Exception('没有拿到锁');
                        //return true;
                    }
                }
            }
            //事务
            if($is_transaction){
                $transaction = Yii::$app->db->beginTransaction();//开启事务      
            }
                       

            //TODO
            if(empty($params)){
                $result = $this->$callback();//处理具体的逻辑的操作
            }else{
                $result = $this->$callback($params);//处理具体的逻辑的操作
            }
            
            if(!$result){
                throw new Exception($this->error);
            }
            
            if($is_lock){
                Tool::deleteLock($cache_key);//释放锁
            }
            if($is_transaction){
                $transaction->commit();//提交事务
            }            
            
            return $result;
        } catch (Exception $exc) {
            if($is_lock){
                Tool::deleteLock($cache_key);//释放锁
            }
            if(!empty($transaction)){
                $transaction->rollBack();//释放事务          
            }            
            
            static::$error = $exc->getMessage();
            return false;
        }
    }    
    
    public function tit()
    {
        $time = TIMESTAMP - 1800;
        $condition = [
            'order_state' => 0,
            "add_time<$time",
        ];
        $datas = \common\models\OrderModel::baseGetDatas($condition,false);
        foreach($datas as $data){
            $goods = \common\models\OrderGoodsModel::baseGetDatas(['order_id' => $data['order_id']]);
            foreach($goods as $good){
                if($good['type'] == 1){
                    $express = new \yii\db\Expression("num+{$good['num']}");
                    GoodsModel::updateAll(['num' => $express], ['id' => $good['goods_id']]);
                }else{
                    $d = json_decode($good['shop_list'],true);
                    foreach($d as $k => $v){
                        $express = new \yii\db\Expression("num+{$v['buy_num']}");
                        GoodsModel::updateAll(['num' => $express], ['id' => $v['goods_id']]);
                    }                
                }
                \common\models\OrderModel::updateAll(['order_state' => 4], ['order_id' => $data['order_id']]);                
            }
        }
    }
    
    public function actionIndex()
    {
        $this->tit();
        exit;
        $state = $this->baseTit(true, true, false, "cron_actionIndex", "tit");
        if($state){
            echo $state;
        }else{
            echo static::$error;
        }
    }
    
    //自动收货
    public function actionShouhuo()
    {
        $time = TIMESTAMP - 86400*10;
        $condition = [
            'order_state' => 2,
            "add_time<$time",
        ];
        $datas = \common\models\OrderModel::baseGetDatas($condition,false);
        print_r($datas);exit;
        foreach($datas as $data){
            \common\models\OrderModel::updateAll(['order_state' => 3], ['order_id' => $data['order_id']]);
        }        
    }
}
