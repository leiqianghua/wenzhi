<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\ElseCarouselModel;
use common\models\GoodsModel;
use common\components\GuestInfo;
use yii;
use common\models\GoodsClassModel;
use common\models\GoodsCartModel;
use common\models\GoodsPackageModel;
/**
 * Default controller for the `wap` module
 */
class Else_kaidanController extends BaseController
{
    
    public $model_class;
    public function init()
    {
        parent::init();
        $this->model_class = new \common\models\ElseKaidanModel();
        $this->layout = "address";
    }    
    
    //需要参数ID。代表的是哪一个的支付。
    public function actionPay()
    {
        $model = \common\models\ElseKaidanModel::findOne(GuestInfo::getParam("id"));
        if(!($model['last_setp_is_confirm'] || !$model['step_has_ok'])){
            $this->error("已经付款或者找不到付款信息");
        }
        
        $arr = explode(',', $model['step_pay_num']); $pay_money = $arr[$model->step_has_ok]/100*$model->zhe_price;
        if($model['order_id']){
            $m = \common\models\MemberAddmoneyRecordModel::findOne($model['order_id']);
            if(!$m['status'] && $m['money'] == $pay_money){
                $is_data = true;
            }else{
                $is_data = false;
            }
        }else{
            $is_data = false;
        }
        if($is_data){
            $ordermodel = \common\models\MemberAddmoneyRecordModel::findOne($model['order_id']);
        }else{
            $ordermodel = \common\models\MemberAddmoneyRecordModel::rechargePay($pay_money, 1, $model['uid'], "C_K");
            if($ordermodel){
                $model->order_id = $ordermodel['code'];
                $beizhu = json_decode($model['beizhu'],true);
                if(!$beizhu){
                    $beizhu = [];
                }
                $step = $model->step_has_ok + 1;
                $beizhu[$step] = $ordermodel['code'];
                $model->beizhu = json_encode($beizhu);
                $model->save(false);                
            }else{
                $this->error("找不到订单信息");
            }
        }
        $this->redirect(['new_pay/index','order_id' => $ordermodel['code']]);
    }
    
    
    public function actionShow()
    {
        error_reporting(0);
        $model = \common\models\ElseKaidanModel::findOne(GuestInfo::getParam("id"));
        if(!$model){
            $this->error("已经付款或者找不到付款信息");
        }
        $this->assign("model", $model);
        return $this->render("show_type");//这个是工程开单
//        if($model['type']){
//            return $this->render("show_type");//这个是工程开单
//        }else{
//            return $this->render();
//        }
        
    }    
    
    public function actionIndex()
    {
        if(!$this->user->isLogin()){
            $this->error("请先登录");
        }
        $this->model_class->uid = $this->user->uid;
        return $this->baseIndex($this->model_class);
    }
    
    public function actionCreate()
    {
        if(!$this->user->isLogin()){
            $this->error("请先登录");
        }
        $is_card = GuestInfo::getParam("is_card");
        if($is_card){
            $content = [];
            $all_data = GoodsCartModel::findAll(['uid' => $this->user->uid]);
            foreach($all_data as $key => $value){
                $good = GoodsModel::findOne($value['goods_id']);
                if(!$good){
                    continue;
                }
                $d = [];
                $d['img'] = $good['img'];
                $d['id'] = $good['id'];
                $d['common_id'] = $good['common_id'];
                $d['name'] = $good['name'];
                $d['price'] = $good['price'];
                $d['buy_num'] = $value['num'];
                $content['content_1'][] = $d;
            }
            $this->model_class->content = json_encode($content);
        }
        if($_POST){
            $_POST['step_pay_num'] = implode(',', GuestInfo::getParam("step_pay_num"));
        }
        $this->model_class->uid = $this->user->uid;
        return $this->baseCreate($this->model_class);
    }    

    
    public function actionUpdate()
    {
        if(!$this->user->isLogin()){
            $this->error("请先登录");
        }
        if($_POST){
            $_POST['step_pay_num'] = implode(',', GuestInfo::getParam("step_pay_num"));
        }
        return $this->baseUpdate(get_class($this->model_class));
    }
    
    public function actionDelete()
    {
        if(!$this->user->isLogin()){
            $this->error("请先登录");
        }
        return $this->baseDelete(get_class($this->model_class));
    }
    
    public function actionAjax_select_goods()
    {
        $datas = GoodsModel::baseGetDatas(['id' => $_POST['shop_ids']]);
        foreach($datas as $k => $v){
            $datas[$k]['buy_num'] = 1;
        }
        $_POST['shop_ids'] = $datas;
        
        $content = $this->renderPartial('_goods',$_POST);            
        $this->ajaxReturn(['content' => $content, 'status' => true]);        
    }
    
    //工程类开单列表
    public function actionList_package()
    {
        $this->title = "工程开单类列表";
        //开哪些类目的单
        $condition = [];
        $condition['status'] = 1;
        $condition['parent_id'] = 0;
        $condition['orderBy'] = "sorting desc";
        $condition['pagesize'] = 999;
        $condition['noget'] = true;
        $datas = \common\models\Project_classModel::baseGetDatas($condition);
        $this->assign("datas", $datas);
        return $this->render();
    }
    
    
    //创建开单
    public function actionCreate_package()
    {
        if(!$_POST){
            $goods_id = GuestInfo::getParam("goods_id");
            if($goods_id){
                $model = \common\models\Project_goods_packageModel::findOne($goods_id);
                $_GET['type'] = $model['cid'];
                $dd = [];
                $dd['content_3'] = json_decode($model['shop_list'],true);
                $this->model_class->content = json_encode($dd);
            } 
            
        

        
            $this->model_class->type = \common\components\GuestInfo::getParam("type");
            $this->title = \common\models\Project_classModel::getOne($this->model_class->type, "title");
        
            //这个分类的所有的子分类拿出来

            $query = \common\models\Project_classModel::find()->where(['parent_id' => $this->model_class->type])->limit(10000);
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $query,
            ]);
            $dataProvider->pagination->pageSize = 10000;
            $dataProvider->setTotalCount(10000);

            $this->assign("dataProvider", $dataProvider);            
        }else{
            $_POST['step_pay_num'] = implode(',', GuestInfo::getParam("step_pay_num"));
        }
        $this->user->isLogin();
        $this->model_class->uid = $this->user->uid;
        return $this->baseCreate($this->model_class);
    }    
    //修改开单
    public function actionUpdate_package()
    {
        $m = \common\models\ElseKaidanModel::findOne(\common\components\GuestInfo::getParam("id"));
        if(!$_POST){
            
            $_GET['type'] = $m['type'];
            $this->title = \common\models\Project_classModel::getOne($_GET['type'], "title");
            //这个分类的所有的子分类拿出来

            $query = \common\models\Project_classModel::find()->where(['parent_id' => $m['type']])->limit(10000);
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $query,
            ]);
            $dataProvider->pagination->pageSize = 10000;
            $dataProvider->setTotalCount(10000);

            $this->assign("dataProvider", $dataProvider);             
        }else{
            $_POST['step_pay_num'] = implode(',', GuestInfo::getParam("step_pay_num"));
        }

        
        
        return $this->baseUpdate($m);
    }
    
    
    public function actionSelect_index()
    {
        //把商品列出来 选择这些商品
        $condition = [];
        $condition['pagesize'] = 99999;
        $condition['status'] = 1;
        $condition['cid'] = GuestInfo::getParam("cid");
        $datas = \common\models\Project_goodsModel::baseGetDatas($condition);
        $content = $this->renderPartial("select_index", ['goods' => $datas]);
        $this->success(['content' => $content]);
    }        
    public function actionAjax_select_index()
    {
        //把商品列出来 选择这些商品
        $condition = [];
        $condition['pagesize'] = 99999;
        $condition['status'] = 1;
        $condition['cid'] = GuestInfo::getParam("cid");
        $datas = \common\models\Project_goodsModel::baseGetDatas($condition);
        $content = $this->renderPartial('/layouts/base/goods_select',['goods' => $datas]);   
        $this->success(['content' => $content]);
    }
    
    public function actionAjax_select_goods_project()
    {
        error_reporting(0);
        //$shop_ids  //$has_select_id    $num  $name_show

        $condition['id'] = $_POST['shop_ids'];
        $shop_ids = \common\models\Project_goodsModel::baseGetDatas($condition);
        foreach($shop_ids as $key => $value){
            $shop_ids[$key]['num'] = 1;
        }
        $_POST['shop_ids'] = $shop_ids;
        $_POST['num'] = GuestInfo::getParam("last_key",false);
        $_POST['name_show'] = GuestInfo::getParam("name_show");
        
        $content = $this->renderPartial('_shop_select',$_POST);            
        $this->ajaxReturn(['content' => $content, 'status' => true]);        
    } 
    
    public function actionPay_chuli()
    {
        //用户上传图表示支付OK的。
        $model = \common\models\ElseKaidanModel::findOne(GuestInfo::getParam("id"));
        \common\components\TmpLog::addData("uuccbb", $model->attributes);
//        if(!(!$model['step_has_ok'] || $model['last_setp_is_confirm'])){//才是OK的
//            $this->error("状态出错,已经处理等待结果");
//        }
        
        
        //不能操作是一个什么状态
        if($model['step_has_ok'] && $model->last_setp_is_confirm == 0){
            $this->error("状态出错");
        }
        if($model['step_has_ok'] == $model['step'] && $model['last_setp_is_confirm']){   
            $this->error("已经完成");
        }     
        
        $model->user_upload_img = json_encode(GuestInfo::getParam("user_upload_img"));
        $model->step_has_ok = $model->step_has_ok + 1;
        $model->last_setp_is_confirm = 0;
        $model->save(false);
        
        \common\components\TmpLog::addData("uuccbb", $model->id);
        $model->payAfterNotice();
        
        $this->success("处理成功");
    }
    
    public function actionIs_pay()
    {
        $uid = $this->user->isLogin();
        $system_user = \common\models\ConfigModel::getOne("goods_buy_notice", "value");
        $system_user = explode(",", $system_user);
        if(!in_array($uid, $system_user)){
            $this->error("用户信息出错");
        }
        
        $model = \common\models\ElseKaidanModel::findOne(\common\components\GuestInfo::getParam("id"));
        if(!$model){
            $this->error("找不到相关数据");
        }
        if($model['last_setp_is_confirm']){
            $this->error("上一次的已经处理了");
        }
        $model->last_setp_is_confirm = 1;
        
        
        $model->save(false);
        //通知开单的人。
        $model->shenAfterNotice();
        $model->jiesuan();
        
        
        
        $this->success("处理成功");      
    }
}
