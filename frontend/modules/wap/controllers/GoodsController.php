<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\ElseCarouselModel;
use common\models\GoodsModel;
use common\components\GuestInfo;
use yii;
use common\models\GoodsClassModel;
use common\models\GoodsCartModel;
use common\models\GoodsPackageModel;
use common\models\ChaGoodsCommonModel;
use common\models\ChaGoodsClassModel;
use common\models\ChaGoodsModel;
/**
 * Default controller for the `wap` module
 */
class GoodsController extends BaseController
{

    public function actionIndex()
    {
        $this->layout = false;
        $this->assign('cart_goods_num', GoodsCartModel::getNum());//多一个显示购物车有多少件商品的功能
        $gid = GuestInfo::getParam('gid', 0);//common_id
        $goods_id = GuestInfo::getParam('goods_id', 0);//具体的详情的goods_id
        $datas = \common\models\GoodsCommonModel::findOne($gid);
        if(!$datas){
            $this->error('找不到商品');
        }
        $result_data = [];
        $result = [];
        $result['data_common'] = $datas->attributes;
        $result['data_common']['spec_value'] = json_decode($result['data_common']['spec_value'], true);
        $result['data_common']['spec_1'] = json_decode($result['data_common']['spec_1'], true);
        $result['data_common']['spec_2'] = json_decode($result['data_common']['spec_2'], true);
        $result['data_common']['spec_3'] = json_decode($result['data_common']['spec_3'], true);
        $goods_list = GoodsModel::baseGetDatas(['noget' => true,'pagesize' => 999,'common_id' => $gid]);
        if(!$goods_list){
            $this->error("找不到商品");
        }
        foreach($goods_list as $k => $v){
            $result['data_goods_list'][$v['id']] = $v;
            $result['data_goods_list'][$v['id']]['spec_value'] = json_decode($v['spec_value'], true);
            $result['data_goods_list'][$v['id']]['spec_1'] = json_decode($v['spec_1'], true);
            $result['data_goods_list'][$v['id']]['spec_2'] = json_decode($v['spec_2'], true);
            $result['data_goods_list'][$v['id']]['spec_3'] = json_decode($v['spec_3'], true);
            
            if($v['num']){
                $result_data[$v['spec_1_num']][$v['spec_2_num']][$v['spec_3_num']] = $v['id'];
            }
            
        }
        //print_r($result);exit;
        $this->assign('result_data', $result_data);//包括商品  //具体的商品
        $this->assign('goods_id', $goods_id);//包括商品  //具体的商品
        \common\components\TmpLog::addData("aa", $result);
        \common\components\TmpLog::addData("bb", $result_data);
        $this->assign('datas', $result);//包括商品  //具体的商品

        return $this->render();
    }

    public function actionCha_index()
    {
        $this->layout = false;
        $this->assign('cart_goods_num', GoodsCartModel::getNum());//多一个显示购物车有多少件商品的功能
        $gid = GuestInfo::getParam('gid', 0);//common_id
        $goods_id = GuestInfo::getParam('goods_id', 0);//具体的详情的goods_id
        $datas = \common\models\ChaGoodsCommonModel::findOne($gid);
        if(!$datas){
            $this->error('找不到商品');
        }
        $result_data = [];
        $result = [];
        $result['data_common'] = $datas->attributes;
        $result['data_common']['spec_value'] = json_decode($result['data_common']['spec_value'], true);
        $result['data_common']['spec_1'] = json_decode($result['data_common']['spec_1'], true);
        $result['data_common']['spec_2'] = json_decode($result['data_common']['spec_2'], true);
        $result['data_common']['spec_3'] = json_decode($result['data_common']['spec_3'], true);
        $goods_list = ChaGoodsModel::baseGetDatas(['noget' => true,'pagesize' => 999,'common_id' => $gid]);
        if(!$goods_list){
            $this->error("找不到商品");
        }
        foreach($goods_list as $k => $v){
            $result['data_goods_list'][$v['id']] = $v;
            $result['data_goods_list'][$v['id']]['spec_value'] = json_decode($v['spec_value'], true);
            $result['data_goods_list'][$v['id']]['spec_1'] = json_decode($v['spec_1'], true);
            $result['data_goods_list'][$v['id']]['spec_2'] = json_decode($v['spec_2'], true);
            $result['data_goods_list'][$v['id']]['spec_3'] = json_decode($v['spec_3'], true);
            
            if($v['num']){
                $result_data[$v['spec_1_num']][$v['spec_2_num']][$v['spec_3_num']] = $v['id'];
            }
            
        }
        //print_r($result);exit;
        $this->assign('result_data', $result_data);//包括商品  //具体的商品
        $this->assign('goods_id', $goods_id);//包括商品  //具体的商品
        \common\components\TmpLog::addData("aa", $result);
        \common\components\TmpLog::addData("bb", $result_data);
        $this->assign('datas', $result);//包括商品  //具体的商品

        return $this->render();
    }
    
    
    //详情页面
    public function actionIndex_project()
    {
        $this->layout = false;
        //$this->title = "套装";
        $result = \common\models\Project_goods_packageModel::findOne(GuestInfo::getParam("gid"));
        $this->assign('datas', $result->attributes);//包括商品  //具体的商品
        


        $condition = [];
        $condition['noget'] = true;
        $condition['parent_id'] = $result['cid'];
        $condition['pagesize'] = 9999;
        $goods_class = \common\models\Project_classModel::baseGetDatas($condition);
        $this->assign("goods_class", $goods_class);
        
        
        
        
        
        return $this->render();
    }
    
    /**
     * AJAX取得相关数据
     * 商品评论
     */
    public function actionComments()
    {
        exit('暂时没有评论！');
        $goods_commonid = intval($_GET['goods_commonid']);
        $model = Model('evaluate_goods');
        $result = $model->getCommonForGoods_commonidType($goods_commonid, $_GET['type'], 20);
        Tpl::output('goodsevallist', $result);
        Tpl::output('show_page', $model->showpage('5'));      
        $this->renderNew();
    }
    
    public function actionGoods_select()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        $pagesize = 99999;
        $goods = GoodsModel::baseGetDatas(['pagesize' => $pagesize,'type' => false,"num > 0"]);
        if($goods){
            $content = $this->renderPartial('/goods_package/_goods_select',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false,'info' => "找不到商品"]);
            exit;
        }        
    }
    
    //选择商品  手机端那boot的展示给拿出来
    public function actionSelect_goods()
    {
        $this->layout = "address";
        $model = new GoodsModel();
        $model->status = 1;
        return $this->baseIndex(new GoodsModel());      
    }
}
