<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\ElseCarouselModel;
use common\models\GoodsModel;
use common\components\GuestInfo;
use yii;
use common\models\GoodsClassModel;
use common\models\GoodsCartModel;
use common\models\GoodsPackageModel;
use GoodsPackageClassModel;
/**
 * Default controller for the `wap` module
 */
class Goods_packageController extends BaseController
{
    public function init()
    {
        parent::init();
        if(!$this->user->isLogin() || $this->user->label >= 4){
            $this->error('你没有权限访问');
        }
        $this->layout = false;
    }

    //商品套件详情页
    public function actionDetail()
    {
        $this->layout = false;
        $this->assign('cart_goods_num', GoodsCartModel::getNum());//多一个显示购物车有多少件商品的功能
        $gid = GuestInfo::getParam('gid', 0);
        $datas = GoodsPackageModel::findOne($gid);
        $datas = $datas->attributes;
        if(!$datas){
            $this->error('找不到商品');
        }

        $this->assign('datas', $datas);

        return $this->render();        
    }
    
    /**
     * AJAX取得相关数据评论
     * 商品评论
     */
    public function actionComments()
    {
        $gid = $_GET['gid'];
        $condition = [
            'goods_id' => $gid,
            'type' => 2,
            "bvaluate != ''",
        ];
        $goods = \common\models\OrderGoodsModel::baseGetDatas($condition);
        if(!$goods){
            exit("占无评论");
        }
        $this->assign('goods',$goods);
        return $this->render();
    }
    //列表页
    public function actionList()
    {
        $this->ajaxgetgoods();
        $cid = GuestInfo::getParam('cid', 0);
//        if(!$cid){
//            $this->error('请求出错');
//        }
        
        $_GET['pagesize'] = 10;
        $this->assign($_GET);          
        $goods = GoodsPackageModel::baseGetDatas(['pagesize' => 10,'status' => 1]);//取总数与数据
        
        //分类名称
        if($cid){
            $class_name = \common\models\GoodsPackageClassModel::getOne($cid,'title');
            $this->assign('class_name', $class_name);      
        }else{
            $this->assign('class_name', '快速景观');      
        }
        
        $this->assign('goods',$goods);
        return $this->render();        
    }
    
    //下拉载入条件符合条件的商品商品，，，，固定是10件
    private function ajaxgetgoods()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        $goods = GoodsPackageModel::baseGetDatas(['pagesize' => 10,'status' => 1]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods_package',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
    }
    
    public function actionAjaxdetail()
    {
        $goods = GoodsModel::getOne($_POST['id']);
        if(!$goods){
            $this->error('找不到数据');
        }
        $content = $this->renderPartial('/layouts/base/shop_select',['goods' => $goods]);     
        $this->ajaxReturn(['content' => $content, 'status' => true]);
    }
}
