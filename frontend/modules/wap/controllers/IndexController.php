<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\ElseCarouselModel;
use common\models\GoodsModel;
use common\components\GuestInfo;
use yii;
use common\models\GoodsClassModel;
use common\models\GoodsPackageModel;
use common\models\GoodsCommonModel;
use common\models\ChaGoodsCommonModel;
use common\models\ChaGoodsClassModel;
use common\models\ChaGoodsModel;
/**
 * Default controller for the `wap` module
 */
class IndexController extends BaseController
{
    public function actionIndex()
    {
        $this->layout = 'index';
        $this->ajaxgetgoods();
        //轮播商品
        $flash = ElseCarouselModel::getDataForType(1);
        $this->assign('flash',$flash);
        
        
        //今日最新商品
        $goods = \common\models\GoodsCommonModel::baseGetDatas(['order' => 5,'pagesize' => 10]);
        //找你的这个城市的这个商品
        $city_id = GuestInfo::getIDForName($GLOBALS['city_name']);//市ID;
        //$city_id = "123";
        if($city_id){
            $goods_city = GoodsCommonModel::baseGetDatas(['order' => 5,'pagesize' => 10,"city_id" => $city_id]);
            $this->assign('goods_city',$goods_city);
            $this->assign('city_id',$city_id);
        }
        $this->assign('goods',$goods);
        
        return $this->render();
    }
    
    //ajax显示特定商品      用于首页的
    public function actionAjaxindexgoods()
    {
        $type = GuestInfo::getParam('type', 1);  
        switch ($type) {
            case 1:
                $_GET['order'] = 5;

                break;
            case 2:
                $_GET['order'] = 1;

                break;
            case 3:
                if(!empty($_POST['order'])){
                    $_GET['order'] = $_POST['order'];
                }else{
                    $_GET['order'] = 3;
                }
                break;
            case 10: //内部商城
                $this->ajaxgetgoodspackage();
                exit;
                break;
            case 4://取到我们可以要的商品来放上去。
                $this->ajaxgetgoods_project();
                exit;
                break;

            default:
                break;
        }
        $this->ajaxgetgoods();
    }    
    
    public function actionGoods()
    {
        $this->layout = false;
        $this->ajaxgetgoods();
        $cid = GuestInfo::getParam('cid', 0);
        if(!$cid && !isset($_GET['is_score'])){
            $this->error('请求出错');
        }
        
        $_GET['pagesize'] = 10;
        $this->assign($_GET);          
        $goods = GoodsCommonModel::baseGetDatas(['pagesize' => 10]);//取总数与数据
        //分类名称
        $class_name = GoodsClassModel::getOne($cid,'title');
        $this->assign('class_name', $class_name);      
        $this->assign('goods',$goods);
        return $this->render();
    }  
    
    
  
    
    public function actionCha_goods()
    {
        $this->layout = false;
        $this->ajaxchagetgoods();
        $cid = GuestInfo::getParam('cid', 0);
        $cid = 1;
        $_GET['cid'] = 1;
        if(!$cid && !isset($_GET['is_score'])){
            $this->error('请求出错');
        }
        
        $_GET['pagesize'] = 10;
        $this->assign($_GET);          
        $goods = ChaGoodsCommonModel::baseGetDatas(['pagesize' => 10]);//取总数与数据
        //分类名称
        $class_name = ChaGoodsClassModel::getOne($cid,'title');
        $this->assign('class_name', $class_name);      
        $this->assign('goods',$goods);
        return $this->render();
    }    
    //下拉载入条件符合条件的商品商品，，，，固定是10件
    private function ajaxchagetgoods()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        $goods = ChaGoodsCommonModel::baseGetDatas(['pagesize' => 10]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/cha_goods',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
    }          
    
    public function actionGoods_select()
    {
        if(empty($_POST['cid']) && isset($_POST['cid'])){
            unset($_POST['cid']);
        }
        $goods = GoodsModel::baseGetDatas(['pagesize' => 10]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods_select',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
    }    
    
    
    //下拉载入条件符合条件的商品商品，，，，固定是10件
    private function ajaxgetgoods()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        $goods = GoodsCommonModel::baseGetDatas(['pagesize' => 10]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
    }    
    
    //或者内部商城
    private function ajaxgetgoodspackage()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        $goods = GoodsPackageModel::baseGetDatas(['pagesize' => 10]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods_package',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }        
    }      
    
    
    //ajax获取所要用的分类
    public function actionAjaxgetcid()
    {
        $this->layout = false;
        //获取所有的大分类
        $datas = GoodsClassModel::getListMenu();
        $this->assign('datas', $datas);
        $content = $this->render();
          
        $this->ajaxReturn(['status' => true, 'content' => $content]);
    }
    
    //下拉载入推荐商品
    public function actionAjax_recommand()
    {
        $this->ajaxgetgoods();
    } 
    
    //下拉载入推荐商品
    public function actionAjax_recommand_project()
    {
        $this->ajaxgetgoods();
    } 
    
    
    public function actionDifan_goods()
    {
        $this->layout = false;
        $this->ajax_difan_goods();

        //找你的这个城市的这个商品
        $city_id = GuestInfo::getParam("city_id");
        if(!$city_id){
            $city_id = GuestInfo::getIDForName($GLOBALS['city_name']);//市ID;
        }
        if($city_id){
            $goods = GoodsCommonModel::baseGetDatas(['order' => 5,'pagesize' => 10,"city_id" => $city_id]);
        }else{
            $this->error("找不到所在城市");
        }       
        $city_name = GuestInfo::getNameForId($city_id);
        $this->assign("city_name",$city_name);    
        $this->assign('goods',$goods);
        $_GET['pagesize'] = 10;
        $this->assign($_GET);          
        return $this->render();
    }  
    
    //下拉载入条件符合条件的商品商品，，，，固定是10件
    private function ajax_difan_goods()
    {
        if(!yii::$app->request->isAjax){
            return;
        }

        //找你的这个城市的这个商品
        $city_id = GuestInfo::getParam("city_id");
        if(!$city_id){
            $city_id = GuestInfo::getIDForName($GLOBALS['city_name']);//市ID;
        }
        if($city_id){
            $goods = GoodsCommonModel::baseGetDatas(['order' => 5,'pagesize' => 10,"city_id" => $city_id]);
        }else{
            $this->error("找不到所在城市");
        }        
        
        
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
    } 

    //纯分类页面
    public function actionCategory()
    {
        $datas = GoodsClassModel::getListMenu(2);
        $this->title = "古笆兔分类";
        $this->assign("datas", $datas);
        
        
        
        $jinguang = \common\models\Project_classModel::getListMenu(2);
        $this->assign("datas_g", $jinguang);
        
//        $jinguang = \common\models\Project_classModel::getListMenu(2);
//        $this->assign("jinguang", $jinguang);
        return $this->render();
    }
    //纯分类页面
    public function actionCategory_gongcheng()
    {
        //$datas = GoodsClassModel::getListMenu(2);
        $this->title = "古笆兔分类";
        //$this->assign("datas", $datas);
        
        $jinguang = \common\models\Project_classModel::getListMenu(2);
        $this->assign("datas", $jinguang);
        return $this->render();
    }
    
    public function actionCeshi()
    {
        //$this->layout = false;
        return $this->render();
    }
    

    
    public function actionProject()
    {
        $this->layout = false;
        $this->ajaxgetgoods_project();
        $cid = GuestInfo::getParam('cid', 0);
        
        $_GET['pagesize'] = 10;
        $this->assign($_GET);          
        $goods = \common\models\Project_goods_packageModel::baseGetDatas(['pagesize' => 10]);//取总数与数据
        //分类名称
        $class_name = \common\models\Project_classModel::getOne($cid,'title');
        $this->assign('class_name', $class_name);      
        $this->assign('goods',$goods);
        return $this->render();        
    }    
    private function ajaxgetgoods_project()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        $goods = \common\models\Project_goods_packageModel::baseGetDatas(['pagesize' => 10]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods_project_package',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }        
    }    
}
