<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\GoodsModel;
use common\models\GoodsPackageModel;
use yii;
/**
 * 搜索页面  keyword关键字
 */
class Label_packageController extends BaseController
{
    
    public function init()
    {
        parent::init();
        $this->layout = false;
    }
    
    public function actionIndex()
    {
        $this->ajax();
        if(empty($_GET['label_ids'])){
            $this->error('请输入你要查找的标签');
        }
        //$name = ['like',"%{$_GET['keyword']}%"];
        
        
        //获取具体的值
        \common\models\GoodsLabelModel::getLabelCondition($condition, "label_id", "goods_id", $_GET['label_ids']);
        $condition['pagesize'] = 10;
        $goods = \common\models\GoodsLabelDataPackageModel::baseGetDatas($condition);
        

        $goods_id = [];
        foreach($goods as $good){
            $goods_id[] = $good['goods_id'];
        }
        $goods = \common\models\GoodsPackageModel::baseGetDatas(['page' => 1, 'id' => $goods_id]);
        
        
        $label_name = "内部商城标签检索";
        
        $this->assign('goods',$goods);
        $this->assign('search_data',$label_name);
        if(empty($goods)){
            return $this->render('empty');
        }else{
            return $this->render();
        }        
    }
    
    
    public function ajax()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        
        //获取具体的值
        \common\models\GoodsLabelModel::getLabelCondition($condition, "label_id", "goods_id", $_POST['label_ids']);
        $condition['pagesize'] = 10;
        $goods = \common\models\GoodsLabelDataPackageModel::baseGetDatas($condition);
        
        $goods_id = [];
        foreach($goods as $good){
            $goods_id[] = $good['goods_id'];
        }
        $goods = \common\models\GoodsPackageModel::baseGetDatas(['page' => 1, 'id' => $goods_id]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods_package',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
    }     
    
    
    
    public function actionIndex_bak()
    {
        $this->ajax();
        if(empty($_GET['label_id'])){
            $this->error('请输入你要查找的标签');
        }
        //$name = ['like',"%{$_GET['keyword']}%"];
        
        $goods = \common\models\GoodsLabelDataPackageModel::baseGetDatas(['label_id' => $_GET['label_id'],'pagesize' => 10]);
        $goods_id = [];
        foreach($goods as $good){
            $goods_id[] = $good['goods_id'];
        }
        $goods = \common\models\GoodsPackageModel::baseGetDatas(['page' => 1, 'id' => $goods_id]);
        $label_name = \common\models\GoodsLabelModel::getOne($_GET['label_id'], 'title');
        
        $this->assign('goods',$goods);
        $this->assign('search_data',$label_name);
        if(empty($goods)){
            return $this->render('empty');
        }else{
            return $this->render();
        }        
    }
    
    
    public function ajax_bak()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        $goods = \common\models\GoodsLabelDataPackageModel::baseGetDatas(['label_id' => $_POST['label_id'],'pagesize' => 10]);
        $goods_id = [];
        foreach($goods as $good){
            $goods_id[] = $good['goods_id'];
        }
        $goods = \common\models\GoodsPackageModel::baseGetDatas(['page' => 1, 'id' => $goods_id]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods_package',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
    }  
}

