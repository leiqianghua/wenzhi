<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseloginController;
use common\models\MemberAddressModel;
use common\models\MemberModel;
use yii;
/**
 * Default controller for the `wap` module
 */
class MemberController extends BaseloginController
{
    //个人中心首页
    public function actionIndex()
    {
        $this->title = '个人中心';
        $this->assign('member',$this->user->getMember());
        return $this->render();
    }
    
    
    public function actionUpdate_password()
    {
        $model = MemberModel::findOne($this->user->uid);
        if($_POST){
            if(!empty($_POST['password'])){//有输入新密码
                if($this->user->password){
                    if($this->user->createPassword($_POST['password']) != $this->user->password){
                        $this->error('旧密码输入不正确,请重新输入');
                    }                    
                }
            }
            
            if(!empty($_POST['new_password']) || !empty($_POST['new_password_wd'])){
                if($_POST['new_password'] != $_POST['new_password_wd']){
                    $this->error('新密码不能为空或两次密码输入不一致');
                }      
                if(strlen($_POST['new_password']) < 6){
                    $this->error('密码长度最长6位');
                }                
            }

            
            $_POST['password'] = $_POST['new_password'];            
            
            $model->save(false);

            
            $this->success('修改成功');
        }
        $this->title = '修改密码';
        $this->layout = "address";
        $this->assign('model',$model);
        return $this->render();           
    }
    
    public function actionUpdate()
    {
        $model = MemberModel::findOne($this->user->uid);
        $model_extend = \common\models\MemberExtendModel::findOne($this->user->uid);
        if(!$model_extend){
            $model_extend = new \common\models\MemberExtendModel();
        }
        if($_POST){


            
            if(!empty($_POST['iphone'])){
                if($_POST['iphone'] != $this->user->iphone){
                    $model_user = MemberModel::findOne(['iphone' => $_POST['iphone']]);
                    if($model_user){
                        $this->error('已经存在此手机号码');
                    }
                }
            }
            
            
            $model->attributes = $_POST;
            $model->save(false);
            
            $model_extend->attributes = $_POST;
            $model_extend->uid = $model->uid;
            $model_extend->real_add_time = time();
            $model_extend->save(false);
            
            $this->success('修改成功');
        }
        $this->title = '修改资料';
        $this->layout = "address";
        $this->assign('model',$model);
        $this->assign('model_extend',$model_extend);
        return $this->render();        
    }
    
    //我的会员
    public function actionMyuser()
    {
        $this->title = '我的会员';
        $this->ajaxgetData();
        $condition = [];
        $condition[] = ['parent_id' => $this->user->uid];
        $condition['pagesize'] = 20;
        $datas = MemberModel::baseGetDatas($condition, false);
        
        $this->assign('datas',$datas);
        
        return $this->render();
    }
    
    //ajax获取会员信息
    private function ajaxgetData()
    {
        if(!yii::$app->request->isAjax){
            return;
        } 
        $condition = [];
        $condition[] = ['parent_id' => $this->user->uid];
        $condition['pagesize'] = 20;
        $datas = MemberModel::baseGetDatas($condition, false);
        if($datas){
            $content = $this->renderPartial('base_list',['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }        
    }
    
    public function actionUpdate_label()
    {
        if($_POST['label'] <= $this->user->label){
            $this->error('级别设置的不正确');
        }
        
        $m  = MemberModel::find()->where(['and', ['parent_id' => $_POST['uid']],"label <= {$_POST['label']}"])->limit(1)->one();//找我的下级
        if($m){
            $this->error('此会员的下级会员比这会员级别更高了');
        }        
        
        $state = MemberModel::updateAll(['label' => $_POST['label']], ['uid' => $_POST['uid'],'parent_id' => $this->user->uid]);
        if($state){
            $this->success('成功设置');
        }else{
            $this->error('修改错误');
        }
    }
    
    //申请级别
    public function actionApply()
    {
        $model = \common\models\MemberApplyModel::findOne(['uid' => $this->user->uid,'status' => 0]);
        if($model){
            $this->error("存在未处理的申请，请不要重复申请");
        }
        if($_POST){
            $model = new \common\models\MemberApplyModel();
            $model->attributes = $_POST;
            $model->uid = $this->user->uid;
            $model->username = $this->user->username;
            $model->status = 0;
            
            $model->label = $this->user->label - 1;
            $state = $model->save(false);
            if($state){
                $model->afterApply();//通知申请成功
                //通知功能。
                
                $this->success('申请成功',"/member/index");
            }else{
                $this->error('申请失败');
            }
        }else{
            
            $member_extend = \common\models\MemberExtendModel::getOne($this->user->uid);
            if(!$member_extend){
                return $this->render("no_apply");
            }   
            
            foreach($member_extend as $key => $value){
                if(!$value){
                    return $this->render("no_apply");
                }
            }
            
            
            //找可以申请的级别
            $labels = MemberModel::getLabelHtml();
            krsort($labels);
            $my_labels = [];
            $my_label = $this->user->label;
            foreach($labels as $key => $value){
                if($my_label > $key){
                    $my_labels[] = $value;
                    break;
                }else{
                    continue;
                }
            }
            $this->assign('labels',$my_labels);
            return $this->render();
        }
    }
    
    
    
    public function actionZiyuan($cid = 0)
    {
        $condition = [
            'pagesize' => 999,
            'parent_id' => $cid,
            "orderBy" => "sorting desc,id desc",
        ];
        $datas = \common\models\ZiyuanClassModel::baseGetDatas($condition);
        
        $condition = [
            "cid" => $cid,
            'pagesize' => 999,
            "status" => 1,
            "orderBy" => "sorting desc,id desc",
        ];
        $ziyuan = \common\models\ZiyuanModel::baseGetDatas($condition);
        
        $is_weixin = \common\components\GuestInfo::isWeixinRequest();
        $is_weixin = $is_weixin ? 1 : 0;
        $this->assign('is_weixin', $is_weixin);        
        
        
        $this->assign("datas",$datas);//当前文件夹
        $this->assign("ziyuan",$ziyuan);//当前中的文件
        return $this->render();
    }
    
    private function ajaxZiyuan()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        

        $condition = [];
        $condition['pagesize'] = 1000;
        $condition['status'] = 1;
        $condition['orderBy'] = "sorting desc,add_time desc";
        $datas = \common\models\ZiyuanModel::baseGetDatas($condition, false);
        if($datas){
            $content = $this->renderPartial('_list',['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }         
    }
    public function actionZiyuandownload()
    {
        $ziyuan = \common\models\ZiyuanModel::getOne($_GET['id']);
        if(!$ziyuan){
            $this->error('找不到资源');
        }
        if($this->user->label > $ziyuan['label']){
            $this->error('你没有权限下载');
        }else{
            $this->redirect($ziyuan['url']);
        }
    }
    
    //申请会员
    public function actionApplylabel()
    {
        $goods_buy_notice = \common\models\ConfigModel::getOne("goods_buy_notice", "value");
        $goods_buy_notice_arr = explode(',', $goods_buy_notice);
        if(in_array($this->user->uid, $goods_buy_notice_arr)){//所有的正在审核的都列出来 。
            $condition = [
                "alias" => "apply",
                "innerJoin" => ["table" => "yii_member member","on" => "apply.uid=member.uid"],
                //"member.parent_id={$this->user->uid}",
                "apply.status = 0",        
            ];            
        }else{
            $condition = [
                "alias" => "apply",
                "innerJoin" => ["table" => "yii_member member","on" => "apply.uid=member.uid"],
                "member.parent_id={$this->user->uid}",
                "apply.status = 0",        
            ];            
        }
        $datas = \common\models\MemberApplyModel::baseGetDatas($condition);
        
        return $this->baseGetDataForList($datas, "_applylabel", "级别申请");
    }
    
    public function actionApplyd()
    {
        $model = \common\models\MemberApplyModel::findOne($_GET['id']);
        if(!$model || $model['status'] != 0){
            $this->error('找不到相关数据');
        }
        $member = MemberModel::getOne($model['uid']);
        if(!$member || $member['parent_id'] != $this->user->uid){
            $this->error('不是你的会员');
        }
        
        
        $status = \common\components\GuestInfo::getParam("status", 2);
        
        if($status == 1){//同意
            $member_model = \common\models\MemberModel::findOne($model['uid']);
            if(!$member_model){
                $model->status = 2;
                $model->save();                    
                $this->error('找不到此会员信息,已被拒绝申请');
            }else{
                $member_model->label = $model['label'];
                $state = $member_model->save();
                if($state){
                    $model->status = 1;
                    $model->save();
                    $this->success('处理成功');                        
                }else{
                    $this->error($member_model->getOneError());                          
                }
            }
        }else{//不同意
            $model->attributes = $_POST;
            $model->status = 2;
            $model->save();
            $this->success('处理成功');
        }        

    }
    //我的推广链接与二维码
    public function actionMyma()
    {
        $url  = \common\components\weixin\Erweima::getPermanent($this->user->uid);
        $excode_url = \yii::$app->request->getHostInfo() . "?exten_code={$this->user->exten_code}";
        $this->assign('url', $url);
        $this->assign('excode_url', $excode_url);
        return $this->render();
    }
    
    public function actionCeshi()
    {
        return $this->render();
    }
    
    public function actionApply_seller()
    {
        // 已经是了就不需要再申请了 其它的不需要再申请了  有正在审核的也不需要审核了
        if($this->user->type_1){
            $this->error("你已经申请成功了");
        }
        $model = \common\models\SellerApplyModel::find()->where(["uid" => $this->user->uid])->orderBy("add_time desc")->one();
        if($model){
            if(!$model['status']){
                $this->error("你还有未审核的待处理");
            }
            if($model['add_time'] > (time() - 3600)){
                $this->error("可审核时间还未到");
            }
            
        }
        
        $model = new \common\models\SellerApplyModel();
        $model->uid = $this->user->uid;
        $model->username = $this->user->username;
        if($_POST){
            $model->attributes = $_POST;
//            $obj = new \common\components\UploadFile();
//            \common\components\TmpLog::addData("upload", $_FILES);
//            $files = $obj->upload("common/upload");
//            \common\components\TmpLog::addData("upload_data", $files);
//            $model->business_license_img = empty($files['business_license_img']['url']) ? '' : $files['business_license_img']['url'];
//            $model->card_id_img = empty($files['card_id_img']['url']) ? '' : $files['card_id_img']['url'];
            if($model->save(true)){
                $this->success("处理成功,请等待审核","index");
            }else{
                $this->error($model->getOneError());
            }
        }else{
            $this->title = "申请供应商";
            $this->layout = "address";
            $this->assign("model",$model);
            return $this->render();
        }
    }
    
    public function actionShow_admin()
    {
        $model = \common\models\AdminMember::findOne($this->user->admin_id);
        $this->assign('model',$model);
        $this->title = "查看后台管理账号";
        return $this->render();
    }    
    public function actionCase()
    {
        $this->title = '案例库';
        return $this->render();
    }
    
    
    public function actionAjax_update_beizhu()
    {
        $member = MemberModel::findOne(\common\components\GuestInfo::getParam("uid"));
        if($member['parent_id'] == $this->user->uid){
            $member->beizhu = \common\components\GuestInfo::getParam("beizhu");
            $member->save(false);
            $this->success("修改成功");
        }else{
            $this->error("数据异常");
        }
    }
    
    public function actionDetail()
    {
        $member = MemberModel::findOne(\common\components\GuestInfo::getParam("uid"));
        if($member['parent_id'] == $this->user->uid){
            $this->layout = "address";
            $this->assign("model", $member);
            return $this->render();
        }else{
            $this->error("数据异常");
        }        
    }

}
