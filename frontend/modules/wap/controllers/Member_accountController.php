<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseloginController;
use common\models\MemberAddressModel;
use common\models\MemberModel;
use yii;
/**
 * Default controller for the `wap` module
 */
class Member_accountController extends BaseloginController
{
    
    public function actionMycase()
    {
        $this->title = '账户管理';
        $this->assign('member',$this->user->getMember());
        return $this->render();
    }
    
    //个人中心首页
    public function actionIndex()
    {
        $this->title = '账户管理';
        $this->assign('member',$this->user->getMember());
        return $this->render();
    }
    
    //资金变化情况
    public function actionMoney_change()
    {
        $this->layout = 'address';
        $this->title = '账户资金交易记录';
        $this->ajaxgetMoney_change();
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 10;
        $condition['orderBy'] = "add_time desc";
        $datas = \common\models\MemberMoneyRecordModel::baseGetDatas($condition, false);
        $this->assign('datas',$datas);
        
        return $this->render();        
    }
    
    //ajax获取会员信息
    private function ajaxgetMoney_change()
    {
        if(!yii::$app->request->isAjax){
            return;
        } 
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 10;
        $condition['orderBy'] = "add_time desc";
        $datas = \common\models\MemberMoneyRecordModel::baseGetDatas($condition, false);
        if($datas){
            $content = $this->renderPartial('_money_change',['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }        
    }    
    
    //我的金额
    public function actionMoneydetail()
    {
        $this->title = '我的钱包';
        $this->assign('member',$this->user->getMember());
        return $this->render();        
    }
    
    //提现记录
    public function actionCash_list()
    {
        $this->title = '提现记录';
        $this->ajaxgetCash_list();
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 10;
        $condition['orderBy'] = "add_time desc";
        $datas = \common\models\MemberCashModel::baseGetDatas($condition, false);
        
        $this->assign('datas',$datas);
        
        return $this->render();             
    }
    
    //ajax获取会员信息
    private function ajaxgetCash_list()
    {
        if(!yii::$app->request->isAjax){
            return;
        } 
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 10;
        $condition['orderBy'] = "add_time desc";
        $datas = \common\models\MemberCashModel::baseGetDatas($condition, false);
        if($datas){
            $content = $this->renderPartial('_cash_list',['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }        
    }        
    
    //申请提现
    public function actionCash_insert()
    {
//        if($this->user->money <= 0){
//            $this->error('余额不足,不能提现');
//        }
        if($_POST){
            //金额是否足够
            if($_POST['money'] > $this->user->money){
                $this->error('你的余额不足');
            }
            if($_POST['money'] < 500){
                $this->error('提现金额不满足条件');
            }            
            $m = \common\models\MemberCashModel::find()->where(['uid' => $this->user->uid,'status' => 0])->exists();
            if($m){
                $this->error('存在未处理的提现申请,请等待处理完再提现');
            }
            $model = new \common\models\MemberCashModel();
            $model->attributes = $_POST;
            $model->uid = $this->user->uid;
            $model->save(false);
            $this->success('申请成功,请等待处理',$this->to(['cash_list']));
        }else{
            $this->title = '余额提现';
            return $this->render();         
        }
    }
    //取消提现
    public function actionCash_channel()
    {
        $model = \common\models\MemberCashModel::findOne($_GET['id']);
        if(!$model){
            $this->error('找不到记录');
        }
        if($model->uid != $this->user->uid){
            $this->error('参数有误');
        }
        if($model->status != 0){
            $this->error('参数有误,已经处理了，不能取消');
        }
        $model->status = 2;
        $model->save(false);
        $this->success('取消成功',$this->to(['cash_list']));
    }
    
    
    public function actionRechange()
    {
        if(!$_POST){
            $this->title = '充值';
            return $this->render();
        }
        //创建充值订单，拿到了订单号后直接跳转。
        $money = (int)\common\components\GuestInfo::getParam("money", 0);
        $mix_model = \common\models\MemberAddmoneyRecordModel::rechargePay($money, "充值");
        if($mix_model){
            $this->redirect(['new_pay/index','order_id' => $mix_model['code']]);
        }else{
            $this->error(\common\models\MemberAddmoneyRecordModel::$error);
        }
    }
}
