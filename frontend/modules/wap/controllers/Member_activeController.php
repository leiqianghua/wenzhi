<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseloginController;
use common\models\MemberAddressModel;
use common\models\MemberModel;
use yii;
/**
 * Default controller for the `wap` module
 */
class Member_activeController extends BaseloginController
{
    //活动首页
    public function actionIndex()
    {
        $this->title = "活动列表";
        $label = $this->user->label;
        $time = time();
        $condition[] = "start_time <= {$time} and end_time > {$time}";//时间范围  还没有结束
        $condition[] = "max_num > use_num";//还没有出售完
        $condition['pagesize'] = 1000;
        $datas = \common\models\ActivityCouponModel::baseGetDatas($condition);
        
        //过滤掉一些内容
        foreach($datas as $key => $data){
            if($data['label']){
                $label_list = json_decode($data['label'], true);
                if(!in_array($label, $label_list)){
                    unset($datas[$key]);
                }
            }
        }
        $this->assign('datas',$datas);
        return $this->render();
    }
    
    //用户进行购买
    public function actionPay()
    {
        $buy_num = \common\components\GuestInfo::getParam('buy_num');//这个是购买的件数
        $id = \common\components\GuestInfo::getParam('id');//购买的活动
        $config = [
            'type' => 1,
            'detail_data' => [
                'id' => $id,
                'buy_num' => $buy_num,
            ],
        ];
        $obj = new \common\components\buy\ActiveBuy($config);
        $code = $obj->buy();
        if(!$code){
            $this->error($obj->error);
        }
        if(strpos($code, "C_A") === false){//这里是直接已经成功了
            $this->success("支付成功");
        }else{//这里则是需要跳转到第三方去支付
            $this->redirect(['new_pay/index','order_id' => $code]);
        }
    }
    
    public function actionMy()
    {
        $this->title = '我的优惠券';
        $this->ajaxgetMy();
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 10;
        $condition['orderBy'] = "is_use asc,id desc";
        $datas = \common\models\MemberCouponModel::baseGetDatas($condition, false);
        
        $this->assign('datas',$datas);
        
        return $this->render();        
    }
    
    //ajax获取会员信息
    private function ajaxgetMy()
    {
        if(!yii::$app->request->isAjax){
            return;
        } 
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 10;
        $condition['orderBy'] = "is_use asc,id desc";
        $datas = \common\models\MemberCouponModel::baseGetDatas($condition, false);
        //$datas = \common\models\MemberCouponModel::baseGetDatas($condition, false,false,true);
        //echo $datas;exit;
        if($datas){
            $content = $this->renderPartial('_my',['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }        
    }       
    
    
}
