<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseloginController;
use common\models\MemberAddressModel;
/**
 * Default controller for the `wap` module
 */
class Member_addressController extends BaseloginController
{
    public function init()
    {
        parent::init();
        $this->layout = 'address';
    }
    //地址列表
    public function actionIndex()
    {
        $this->layout = 'main';
        $this->title = '收货地址';
        $datas = MemberAddressModel::find()->where(['uid' => $this->user->uid])->orderBy('is_default desc')->asArray()->all();
        $this->assign('address',$datas);
        return $this->render();
    }
    
    //修改
    public function actionUpdate()
    {
        return $this->baseUpdate('\common\models\MemberAddressModel');
    }
    //添加
    public function actionCreate()
    {
        $model = new \common\models\MemberAddressModel();
        $model->uid = $this->user->uid;
        return $this->baseCreate($model);
    }
    //删除
    public function actionDelete()
    {
        return $this->baseDelete('\common\models\MemberAddressModel');
    }
}
