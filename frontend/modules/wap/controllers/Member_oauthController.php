<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseloginController;
use common\models\MemberAddressModel;
use yii;
/**
 * Default controller for the `wap` module
 */
class Member_oauthController extends BaseloginController
{
    public function actionIndex()
    {
        $this->ajaxgetData();
        $this->title = '三方账号';
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 30;        
        $condition['orderBy'] = "add_time desc";        
        $datas = \common\models\MemberOauthDataModel::baseGetDatas($condition);
        $this->assign('datas', $datas);
        return $this->render();
    }
    
    //ajax获取会员信息
    private function ajaxgetData()
    {
        if(!yii::$app->request->isAjax){
            return;
        } 
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 30;    
        $condition['orderBy'] = "add_time desc";        
        $datas = \common\models\MemberOauthDataModel::baseGetDatas($condition);
        if($datas){
            $content = $this->renderPartial('base_list',['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }        
    }
    
    //添加绑定
    public function actionCreate()
    {
        if(!empty($_GET['type'])){
            if($_GET['type'] == 'qq'){
                $this->redirect(['/wap/oauth/qq']);
            }else{
                
                $this->redirect(['/wap/oauth/weixin_public_number']);
            }
        }else{
            $this->error('参错错误');
        }
    }
    
    
    
    
    //删除绑定
    public function actionDelete()
    {
        return $this->baseDelete('\common\models\MemberOauthDataModel');
    }
}
