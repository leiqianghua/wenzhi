<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseloginController;
use common\models\MemberAddressModel;
use common\models\MemberModel;
use common\models\OrderModel;
use common\models\OrderGoodsModel;
use common\models\OrderPackageModel;
use common\models\GoodsModel;
use yii;
/**
 * Default controller for the `wap` module
 */
class Member_orderController extends \frontend\modules\wap\components\BaseController
{
    public function init()
    {
        parent::init();
        if(!$this->user->isLogin()){
            
            if($_SERVER['REDIRECT_URL'] == "/wap/member_order/detail"){
                return true;
            }
            
            //这个页面是需要登录的，这个时候我记住这个页面，等下次的时候我再把其恢复过来看是否是可以的》
            $url = Yii::$app->request->getHostInfo().Yii::$app->request->url;
            \common\components\GuestInfo::setData($url);
            //echo \common\components\GuestInfo::getData();exit;
            return $this->redirect('/wap/public/login');
        }        
    }    
    
    //订单
    public function actionIndex()
    {
        $this->ajaxgetData();
        $this->title = '订单管理';
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 10;        
        $condition['orderBy'] = "order_state asc,order_id desc";        
        $datas = OrderModel::baseGetDatas($condition);
        $this->assign('datas', $datas);
        return $this->render();
    }
    
    //ajax获取会员信息
    private function ajaxgetData()
    {
        if(!yii::$app->request->isAjax){
            return;
        } 
        $condition = [];
        $condition[] = ['uid' => $this->user->uid];
        $condition['pagesize'] = 10;    
        $condition['orderBy'] = "order_state asc,order_id desc";    
        $datas = OrderModel::baseGetDatas($condition);
        if($datas){
            $content = $this->renderPartial('base_list',['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }        
    }
    
    //订单详情页 在这里面需要有一个评价的东西
    public function actionDetail()
    {
        $model = OrderModel::findOne($_GET['id']);
        $this->assign('model',$model);
        return $this->render();
    }
    //取消订单
    public function actionCancel()
    {
        $model = OrderModel::findOne($_GET['id']);
        if($model['order_state'] == 0){
            $model['order_state'] = 4;
            $model->save(false);
            
            $goods = \common\models\OrderGoodsModel::baseGetDatas(['order_id' => $_GET['id']]);
            foreach($goods as $good){
                if($good['type'] == 1){
                    $express = new \yii\db\Expression("num+{$good['num']}");
                    GoodsModel::updateAll(['num' => $express], ['id' => $good['goods_id']]);
                }else{
                    $d = json_decode($good['shop_list'],true);
                    foreach($d as $k => $v){
                        $express = new \yii\db\Expression("num+{$v['buy_num']}");
                        GoodsModel::updateAll(['num' => $express], ['id' => $v['goods_id']]);
                    }                
                }
                \common\models\OrderModel::updateAll(['order_state' => 4], ['order_id' => $_GET['id']]);                
            }            
            //把其给转过来
            $this->success('取消成功');
        }else{
            $this->error('此状态下不允许取消');
        }
    }
    //提醒发货
    public function actionRemind()
    {
        $model = OrderModel::findOne($_GET['id']);
        $this->success('提醒成功');
    }
    //确认收货
    public function actionConfirm()
    {
        $model = OrderModel::findOne($_GET['id']);
        if($model['order_state'] == 2){
            $model['order_state'] = 3;
            $model['finish_time'] = time();
            $model->save(false);
            $this->success('确认成功');            
        }else{
            $this->error('此状态下不允许确认');
        }
    }
    //查看物流
    public function actionShow_logistics()
    {
        header("Content-type: text/html; charset=utf-8");
        $model = OrderModel::findOne($_GET['id']);
        $this->assign('order',$model);
        $data = \common\models\ExpressModel::showwuliu($model['e_code'], $model['shipping_code']);
        if($data){
            $this->assign('content',$data);
        }else{
            $this->assign('content','');
            $this->assign('error',\common\models\ExpressModel::$error);
        }
        
        return $this->render();
    }
    //评价
    public function actionBvaluate()
    {
        $order_goods_model = OrderGoodsModel::findOne($_GET['id']);
        if($_POST){
            $order_goods_model->attributes = $_POST;
            if($order_goods_model->save()){
                $this->success('评价成功',$this->to(['bvaluate_show','id' => $_GET['id']]));
            }else{
                $this->error($order_goods_model->getOneError());
            }
        }else{
            $this->title = '评价';
            $this->assign('model', $order_goods_model);
            $this->assign('goods', \common\models\GoodsPackageModel::getOne($order_goods_model['goods_id']));
            return $this->render();
        }
    }
    
    public function actionBvaluate_show()
    {
        $order_goods_model = OrderGoodsModel::findOne($_GET['id']);
        $this->assign('imgs',  json_decode($order_goods_model['bvaluate'], true));
        return $this->render();
    }
}
