<?php

namespace frontend\modules\wap\controllers;

use yii;
use frontend\modules\wap\components\BaseController;
use common\components\pay\run\wxpay\jsapi\Jsapi;
use common\components\pay\run\Alipaywap;

/**
 * Default controller for the `wap` module
 */
class New_payController extends BaseController
{
    
    public function actionPay_alipaywap()
    {
        //取得订单相关东西
        $order_id = \common\components\GuestInfo::getParam('order_id');
        $model = \common\models\MemberAddmoneyRecordModel::findOne($order_id);
        if(!$model){
            $this->error('找不到相关的信息');
        }
        $data = [
            'out_trade_no' => $order_id,
            'subject' => '古笆兔商城',
            'total_fee' => $model['money'],
            'body' => '古笆兔商城',
            'show_url' => $this->to(['index/index']),
        ];
        $obj = new Alipaywap($data);
        $html = $obj->getHmtl();
        echo $html;
        exit;
    }    
    
    
    //支付页面
    public function actionIndex()
    {
        $this->layout = false;
        //跳转到支付界面
        if(\common\components\GuestInfo::isWeixinRequest()){//微信 的话直接跳到微信的入口 
            $this->_weixin();
            exit;
        }                
        
        $order_id = \common\components\GuestInfo::getParam('order_id');
        $model = \common\models\MemberAddmoneyRecordModel::findOne($order_id);
        if(!$model){
            $this->error('找不到订单数据');
        }
        if($model['status'] != 0){
            $this->error('订单已经处理');
        }
        $this->assign('order',$model);
        return $this->render();        
        
    }
    
    
    //微信支付页面临时跳转
    public function _weixin()
    {
        $order_id = $_GET['order_id'];//订单号
        setcookie('order_id', $order_id, time()+86400,'/');
        $pay_url = '/weixin/index_weixin_wap.php';
        @header('Location: ' . $pay_url, true, 302);
        exit;
        return $this->redirect($pay_url);
    }      
    
    //微信支付页面
    public function actionWeixin()
    {
        $this->layout = false;
        $order_id = $_COOKIE['order_id'];
        if(!$order_id){
            $this->error('参数错误');
        }
        
        $model = \common\models\MemberAddmoneyRecordModel::findOne($order_id);

        $data = [];
        $data['body'] = '古笆兔商城';
        $data['out_trade_no'] = $order_id;
        $data['total_fee'] = $model['money'] * 100;//总金额,腾讯默认支付金额单位为【分】
        $data['ming_return_url'] = '/wap/buy/payok';
        $obj = new Jsapi($data);
        $js = $obj->getJs();
        $this->assign('money',$model['money']);
        $this->assign('js',$js);
        echo $this->render();
        exit;        
    }    
    
    
    function actionPayok()
    {
        $this->success('恭喜你支付成功',$this->to('/wap/index/index'));
    }    
    
}
