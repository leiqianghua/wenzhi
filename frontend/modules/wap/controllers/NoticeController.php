<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use yii;
/**
 * Default controller for the `wap` module
 */
class NoticeController extends \frontend\modules\wap\components\BaseloginController
{
    public function init()
    {
        $state = parent::init();
        $this->layout = "address";
        return $state;
    }
    //通知支付  然后点击页面会进入到这个页面上来的   这里面 有  系统管理员  跟 开单人员  会有机会进来
    public function actionKaidang()
    {
        $id = \common\components\GuestInfo::getParam("id");
        $model = \common\models\ElseKaidanModel::findOne($id);
        if(!$model){
            $this->error("找不到开单记录");
        }
        //给你一个审核通过的东西让你来审核
        $this->assign("model", $model);
        return $this->render();
    }    
}
