<?php

/**
 * 联合登录
 */
namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\components\oauth\QQ;
use common\components\oauth\Weixin;
use common\components\oauth\WeixinPublicNumber;

class OauthController extends BaseController
{
    public function init()
    {
        parent::init();
        $this->layout = false;
    }
    //qq的第三方登录链接跳转
    public function actionQq()
    {
        $obj = new QQ();
        $url = $obj->getCodeUrl();
        return $this->redirect($url);
        //header("location: " . $url);         
    }
    //QQ的回调
    public function actionCallback_qq()
    {
        if(\common\models\MemberOauthQQModel::operation($access_token)){//有绑定且已经成功
            $this->redirect(['index/index']);
        }else{
            $this->layout = 'main';
            $this->title = '账号绑定';
            //让其进行相关的绑定
            $this->assign('type','qq');
            $this->assign('token',$access_token);
            return $this->render('band');
            //$this->error(\common\models\MemberOauthQQModel::$error);
        }         
    }
    
    
    
    public function error($arr = array(), $url = '', $is_ajax = false) {
        if(\common\components\GuestInfo::getParam("type_new") == "hy"){
            return $this->go_hy();
        }
        parent::error($arr, $url, $is_ajax);
    }
    
    public function success($arr = array(), $url = '', $is_ajax = false) {
        if(\common\components\GuestInfo::getParam("type_new") == "hy"){

            return $this->go_hy();          
        }        
        parent::success($arr, $url, $is_ajax);
    }
    
    public function actionBand()
    {
        //验证
        if(empty($_POST['type']) || empty($_POST['token'])){
            $this->error('数据提交错误');
        }
        
        if($_POST['type'] == 'qq'){//QQ
            $model = \common\models\MemberOauthQQModel::findOne($_POST['token']);
        }else{//微信
            $model = \common\models\MemberOauthWeixinModel::findOne($_POST['token']);
        }
        if(!$model){
            $this->error('找不到相关的绑定信息');
        }        
        
        if(empty($_POST['iphone']) || empty($_POST['password'])){
            $this->error('数据提交错误');
        }
        
        //找是否有这个用户如果有就直接登录，如果没有则注册
        $member_model = \common\models\MemberModel::findOne(['iphone' => $_POST['iphone']]);
        if($member_model){
            //第一步先注册账号，第二步进行相关的绑定
            $_POST['username'] = $model['username'];
            $_POST['img'] = $model['img'];
            $d = [];
            $d['third_party'] = 0;
            $d['password'] = $_POST['password'];
            $d['name'] = $_POST['iphone'];
            $member_info = $this->user->login($d);            
        }else{
            //第一步先注册账号，第二步进行相关的绑定
            $_POST['username'] = $model['username'];
            $_POST['img'] = $model['img'];
            $member_info = $this->user->register($_POST);            
        }

        if(!$member_info){
            $this->error($this->user->error);
        }
        //第二步进行绑定
        $arr_model = $model->attributes;
        $openid = empty($arr_model['unionid']) ? $arr_model['openid'] : $arr_model['unionid'];
        $state = \common\models\MemberOauthDataModel::band($_POST['type'], $openid, $member_info['uid'],$arr_model['username'],$arr_model['img']);
        
        //注册的时候我这里来确认你的关系，是否是我的这个推广的。
        $model_member = \common\models\MemberModel::findOne($member_info['uid']);
        \common\components\TmpLog::addData("我的微信", $model->attributes);
        if(empty($model['parent_id'])){
            if(\common\components\GuestInfo::isWeixinRequest()){//如果是微信的浏览器
                if(!empty($model['unionid'])){
                    $d = \common\models\MemberWeixinExtenCodeModel::findOne($model['openid']);
                    if($d){
                        $member = \common\models\MemberModel::find()->where(['ticket' => $d['ticket']])->limit(1)->one();
                        if($member){
                            $parent_id = $member->uid;
                            \common\models\MemberModel::updateAll(['parent_id' => $parent_id],['uid' => $model_member['uid']]);
                            
                            
                            $data = [
                                'first' => ['value' => "新增下级会员通知"],
                                'keyword1' => ['value' => $model_member['username']],
                                'keyword2' => ['value' => date("Y-m-d H:i:s", $model_member['add_time'])],
                                'remark' => ['value' => "祝您生活愉快"],
                            ];
                            \common\models\MemberModel::notice_user($data, "", "Xj728_LM3B3_V0IWJXkTssm646TwU8jyixM1RaXmuJY", false, false, false, [$parent_id]);
                            
                            
                            //$model_member->parent_id = $member->uid;
                            //$s = $model_member->save(false);    
                        }
                    }                    
                }
            }            
        }        
        
        
        $this->success('提交成功','/wap/index/index');
        //$this->redirect(['index/index']);
        

        
        

        
    }
    
    //pc微信
    public function actionWeixin()
    {
        $obj = new Weixin();
        $url = $obj->getCodeUrl();
        return $this->redirect($url);
        //header("location: " . $url);   
    }
    
    //微信的回调
    public function actionCallback_weixin()
    {
        if(\common\models\MemberOauthWeixinModel::operation($access_token)){
            $this->redirect(['index/index']);
        }else{
            $this->layout = 'main';
            $this->title = '账号绑定';
            //让其进行相关的绑定
            $this->assign('type','weixin');
            $this->assign('token',$access_token);
            return $this->render('band');
            //$this->error(\common\models\MemberOauthQQModel::$error);
        }         
    }
    
    
    //微信公众号
    public function actionWeixin_public_number()
    {
        if(\common\components\GuestInfo::getParam("type_new") == "hy"){
            \common\components\TmpLog::addData("absdf", 'tttsdsd');
            return $this->actionWeixin_public_number_vue();
        }
        $obj = new WeixinPublicNumber();
        $url = $obj->getCodeUrl();
        return $this->redirect($url);
        //header("location: " . $url);   
    } 
    
    
    //微信公众号
    public function actionWeixin_public_number_vue()
    {
        $obj = new WeixinPublicNumber();
        $url = $obj->getCodeUrlVue();
        return $this->redirect($url);
        //header("location: " . $url);   
    }
    
    //微信公众号的回调
    public function actionNumber()
    {
        

        
        //\common\models\ErrorLogModel::addLog(json_encode($_GET), 'actionNumber');//增加记录
        if(\common\models\MemberOauthWeixinModel::operationForPublicNumber($access_token)){
            
            if(!empty($_GET['state']) && substr($_GET['state'], 0, 3) == "vue"){

                return $this->go_hy();
            }            
            
            
            if(!empty($_GET['state'])){
                if(md5(\common\components\GuestInfo::getIpForLvs()) == $_GET['state']){
                    return $this->redirect(['index/index']);
                }else{
                    return $this->redirect([$_GET['state']]);
                } 
            }else{
                return $this->redirect(['index/index']);
            }
        }else{
            if(!empty($_GET['state']) && substr($_GET['state'], 0, 3) == "vue"){

                $_GET['type_new'] = "hy";
            }              
            $this->layout = 'main';
            $this->title = '账号绑定';
            //让其进行相关的绑定
            $this->assign('type','weixin');
            $this->assign('token',$access_token);
            return $this->render('band');
            //$this->error(\common\models\MemberOauthQQModel::$error);
        }         
    }
    
    
}
