<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use yii;
use common\components\pay\run\Alipaywap;
use common\components\pay\run\Alipay;
use common\models\OrderModel;
/**
 * Default controller for the `wap` module
 */
class PaymentController extends BaseController
{
    public function actionIndex_wap()
    {
        //取得订单相关东西
        $_GET['pay_sn'] = $_POST['pay_sn'];
        $model = OrderModel::find()->where(['order_sn' => $_GET['pay_sn']])->limit(1)->one();
        if(!$model){
            $this->error('找不到相关的信息');
        }
        $data = [
            'out_trade_no' => $_GET['pay_sn'],
            'subject' => '古笆兔商城商品',
            'total_fee' => $model->order_amount,
            'body' => '古笆兔商城商品',
            'show_url' => $this->to(['index/index']),
        ];
        $obj = new Alipaywap($data);
        $html = $obj->getHmtl();
        echo $html;
        exit;
    }
    
    public function actionIndex()
    {
        //取得订单相关东西
        $_GET['pay_sn'] = $_POST['pay_sn'];
        $model = OrderModel::find()->where(['order_sn' => $_GET['pay_sn']])->limit(1)->one();
        if(!$model){
            $this->error('找不到相关的信息');
        }
        $data = [
            'out_trade_no' => $_GET['pay_sn'],
            'subject' => '古笆兔商城商品',
            'total_fee' => $model->order_amount,
            'body' => '古笆兔商城商品',
        ];
        $obj = new Alipay($data);
        $html = $obj->getHmtl();
        echo $html;
        exit;
    }       
}
