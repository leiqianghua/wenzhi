<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;

/**
 * 登录注册等相关的功能
 */
class PublicController extends BaseController
{
    //登录
    public function actionLogin()
    {
        $this->title = '登录';
        if($this->user->isLogin()){
            if(\common\components\GuestInfo::getParam("type_new") == "hy"){
                
                return $this->go_hy();
            }else{
                return $this->redirect(['index/index']);
            }
            
        }
        //我们看是否是微信的浏览器
        if(\common\components\GuestInfo::isWeixinRequest()){//如果是微信的浏览器
            return $this->redirect(['oauth/weixin_public_number',"type_new" => \common\components\GuestInfo::getParam("type_new")]);
        }
        
        if($_POST){
            $state = $this->user->login($_POST);
            if($state){
                $url = \common\components\GuestInfo::getData();
                if(!$url){
                    $url = $this->to(['index/index']);
                }
                if(\common\components\GuestInfo::getParam("type_new") == "hy"){
                    return $this->go_hy_geturl();
                }else{
                    $this->success('登录成功',$url);
                }
                
            }else{
                $this->error($this->user->error);
            }
        }else{
            return $this->render();
        }
    }
    //注册
    public function actionRegister()
    {
        if($this->user->isLogin()){
            if(\common\components\GuestInfo::getParam("type_new") == "hy"){
                return $this->go_hy();
            }else{
                return $this->redirect(['index/index']);
            }
        }
        //我们看是否是微信的浏览器
        if(\common\components\GuestInfo::isWeixinRequest()){//如果是微信的浏览器
            return $this->redirect(['oauth/weixin_public_number',"type_new" => \common\components\GuestInfo::getParam("type_new")]);
        }        
        if($_POST){           
            $state = $this->user->register($_POST);
            if($state){
                if(\common\components\GuestInfo::getParam("type_new") == "hy"){
                    return $this->go_hy_geturl();
                }else{
                    $this->success('注册成功',$this->to(['index/index']));
                }                
            }else{
                $this->error($this->user->error);
            }
        }else{
            return $this->render();
        }        
    }
    //找回密码
    public function actionFind_password()
    {
        return $this->render();
    }
    
    public function actionLogout()
    {
        $this->user->logout();
        $this->redirect(['index/index']);
    }
    
    public function actionShow_weixin()
    {
        $key = \common\components\GuestInfo::getParam("key");
        $content = \common\models\ConfigModel::getWeixinContentForKey($key);
        if(empty($content['Description'])){
            $this->error("找不到相关消息!");
        }   
        $this->assign("content", $content['Description']);
        return $this->render();
    }
}

