<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use common\models\GoodsModel;
use common\models\GoodsCommonModel;
use yii;
/**
 * 搜索页面  keyword关键字
 */
class SearchController extends BaseController
{
    
    public function init()
    {
        parent::init();
        $this->layout = false;
    }
    public function actionIndex()
    {
        if(empty($_GET['keyword'])){
            $this->error('请输入你要搜索的内容');
        }
        //$name = ['like',"%{$_GET['keyword']}%"];
        $this->ajax();
        
        //$goods = GoodsModel::getVarData(['order' => 1,'pagesize' => 10,'name' => $_GET['keyword']]);
        $goods = GoodsCommonModel::baseGetDatas(['status' => 1,'order' => 1,'pagesize' => 10,"name like '%{$_GET['keyword']}%'"]);
        $this->assign('search_data',$_GET['keyword']);
        $this->assign('goods',$goods);
        if(empty($goods)){
            return $this->render('empty');
        }else{
            return $this->render();
        }        
    }
    
    
    public function ajax()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        $name = ['like',"%{$_GET['keyword']}%"];
        $goods = GoodsCommonModel::baseGetDatas(['status' => 1,'pagesize' => 10,'name' => $name]);
        if($goods){
            $content = $this->renderPartial('/layouts/base/goods',['goods' => $goods]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
    }  
}

