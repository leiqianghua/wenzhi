<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseController;
use yii;
/**
 * //微信通知的链接
 * Array
(
    [ToUserName] => gh_28c7437fa60d
    [FromUserName] => ou7OgwjWEKI4WfAAV1s424RdEMDU
    [CreateTime] => 1479227283
    [MsgType] => event
    [Event] => CLICK
    [EventKey] => key4
) 
 */
class WeixinController extends BaseController
{
    public function actionIndex()
    {
        
        $obj = new \common\components\weixin\Yanzheng();
        //$obj->valid();     
        
        return $obj->responseMsg();
    }
    
    
    
    public function actionCreatemenu()
    {
        $menu = [
            "button" => [
                [
                    'name' => "古笆免",
                    'sub_button' => [
                        [
                            'type' => "click",
                            'name' => "公司简介",
                            'key' => "key1",
                        ],
                        [
                            'type' => "click",
                            'name' => "发展历程",
                            'key' => "key2",
                        ],
                        [
                            'type' => "click",
                            'name' => "企业优势",
                            'key' => "key3",
                        ],
                        [
                            'type' => "click",
                            'name' => "客服咨询",
                            'key' => "key4",
                        ],
                        [
                            'type' => "view",
                            'name' => "申请加盟",
                            'url' => "https://sojump.com/jq/10351496.aspx",
                        ],
                    ],
                ],
                [
                    'name' => "商城",
                    'type' => "view",
                    'url' => "http://www.gubatoo.com/wap/public/login",
                ],                
            ],
        ];
        $state = \common\components\weixin\Menu::create($menu);
        print_r($state);exit;
    }
}
