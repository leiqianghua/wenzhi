<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>
<form method="post" id="order_form" name="order_form" action="<?= $this->context->to(['buy_step2'])  ?>">


<?php  foreach($_POST as $key => $value){  ?>    
    <?php if(is_array($value)){ $value = json_encode($value); }  ?>
    <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
<?php }  ?>
    
    
<input type="hidden" id="mine_post" name="mine_post" value='<?= json_encode($_POST) ?>'/>


    
<input type="hidden" id="address_id" name="address_id" value="<?php echo empty($address_info[0]['id']) ? '' : $address_info[0]['id']; ?>" >



<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <div class="page_title"><h1>确认订单</h1></div>
    </header>
<!--    收货地址-->
    <section class="buy_address">
        <?php if(empty($address_info[0])){  ?>
            <div class="buy_address_has" style="display:none">
                <i class="iconfont location">&#xe610;</i>
                <i class="iconfont arrow">&#xe612;</i>
                <p>收货人：<em id="addressName"></em> <span class="phone" id="addressPhone"></span></p>
                <p>收货地址：<em id="addressPosi"></em></p>
            </div>
            <div class="buy_address_none" >
                <a href="javascript:void(0)" id="addressNone"><i class="iconfont add">&#xe608;</i>请添加收货地址</a>
            </div>        
        <?php }else{  ?>
            <div class="buy_address_has">
                <i class="iconfont location">&#xe610;</i>
                <i class="iconfont arrow">&#xe612;</i>
                <p>收货人：<em id="addressName"><?= $address_info[0]['username'] ?></em> <span class="phone" id="addressPhone"><?= $address_info[0]['iphone'] ?></span></p>
                <p>收货地址：<em id="addressPosi"><?= $address_info[0]['address_info'] . $address_info[0]['address'] ?></em></p>
            </div>
            <div class="buy_address_none" style="display:none;">
                <a href="javascript:void(0)" id="addressNone"><i class="iconfont add">&#xe608;</i>请添加收货地址</a>
            </div>        
        <?php }  ?>

    </section>


<section class="buy_main">
        <?php  foreach($datas as $admin_id => $data){ ?>
            <div data-id="<?= $admin_id ?>" class="buy_list">
<!--                前面头部-->
                <div class="buy_hd">
                    <div class="eachShop_name">

                        <a href="javascript(0)" target="_blank">
                            <?php if($admin_id == 0){ $admin_name = "古笆兔出品"; }else{ $admin_name = \common\models\MemberModel::getOne($admin_id, "username"); }  ?>
                            <i class="iconfont"></i><?= $admin_name ?>
                        </a>
                    </div>                    
                </div>
                <div class="buy_bd">
                    <?php foreach($data['data'] as $k => $v){ ?>
                        <div class="buy_item">
                            <div class="buy_detail">
                                <div class="buy_img">
                                    <a href="/wap/goods/index?gid=<?= $v['common_id'] ?>&goods_id=<?= $v['id'] ?>">
                                        <img src="<?= $v['img'] ?>">
                                    </a>
                                </div>
                                <div class="buy_info">
                                    <a href="/wap/goods/index?gid=<?= $v['common_id'] ?>&goods_id=<?= $v['id'] ?>">
                                        <h3><?= $v['name'] ?></h3>
                                        <?php echo \common\models\GoodsModel::mine_attributesStr($v['mine_attributes'])  ?>
                                    </a>
                                    <div class="eachGoods_price"><i>¥</i><em class="td_price"><?= $v['price'] ?></em></div>
                                </div>
                                <span class="eachGoodsNum">×<em class="buy_td_num"><?= $v['buy_num'] ?></em></span>
                            </div>
                        </div>                        
                    <?php  } ?> 
                </div>
                <div class="pay_for_list">
                    <div class="pay_col">
                        <label>商品总金额</label>
                        <span>¥<em id="allGoodsTotal_<?= $admin_id ?>"><?= $total_money[3][$admin_id]['total_money'] ?></em></span>
                    </div>
                    <div class="pay_col">
                        <label><?= \common\models\MemberModel::getLabelHtmlAll($this->context->user->label) ?>折扣金额</label>
                        <span>¥<em id="allGoodsTotal_zhe_<?= $admin_id ?>"><?= $total_money[3][$admin_id]['total_money_zhe'] ?></em></span>
                    </div>          
                    <div class="pay_col">
                        <label>运费</label>
                        <span>¥<em class="admin_id_allFreightTotal" id="allFreightTotal_<?= $admin_id ?>"><?= isset($freight[1][$admin_id]) ? $freight[1][$admin_id] : 0;  ?></em></span>
                    </div>   
                    <?php  if(!empty($total_money[3][$admin_id]['total_score'])){  ?>
                        <div class="pay_col">
                            <label>消耗积分</label>
                            <span><em id="total_score_<?= $admin_id ?>"><?= $total_money[3][$admin_id]['total_score'];  ?></em></span>
                        </div> 
                        <div class="pay_col">
                            <label>账户积分: <?= $this->context->user->score;  ?></label>
                        </div>                  
                    <?php }  ?>            
                </div>
            </div>    
        <?php  } ?>
        <div class="pay_for_list">

            <div class="pay_col">
                <label>账户可用余额为 <?= $this->context->user->money;  ?></label>
            </div>            
            <div  class="pay_col">
                <label style="cursor:pointer;">使用优惠卡</label>
                <input type="checkbox" name="" id="use_coupon"/>
            </div>
            <ul style="display: none" id="coupon_list">
                <?php   $coupons = common\models\MemberCouponModel::getUseCoupon($datas, $total_money[1], $this->context->user->uid);  ?>
                <?php if($coupons) { ?>
                    <?php  foreach($coupons as $coupon){  ?>

                        <li data="<?= $coupon['id'] ?>" is_overlay="<?= $coupon['is_overlay'] ?>">
                            <span><?= $coupon['money'] ?> 元优惠券</span>
                            <span><?php  if($coupon['is_overlay']){ echo '可叠加'; }else{ echo '不可叠加'; }  ?></span>
                            <span><?php  if($coupon['must_money']){ echo "满{$coupon['must_money']}元使用"; }else{ echo '直接使用'; }  ?></span>
                            <input  <?php  if($total_money[1] < $coupon['must_money']){ echo "disabled"; } ?>  is_overlay="<?php if(!empty($coupon['is_overlay'])){ echo 1; }else{ echo 0; } ?>" type="checkbox" name="coupon[]" value="<?= $coupon['id'] ?>" class="select_coupon" />
                        </li>                    
                    <?php }  ?>
                <?php }else{  ?>
                    <li>你还没有可使用的优惠券</li>
                <?php }  ?>
            </ul>
        </div>
    </section>

    <script>
        $(function(){
            $("#use_coupon").click(function(){
                if($(this)[0].checked){//选择了
                    $("#coupon_list").show();
                    
                }else{
                    $("#coupon_list").hide();
                    var length_has_overlay = $(".select_coupon:checked").length;
                    for(var i=0;i<length_has_overlay;i++){
                        $(".select_coupon:checked").eq(i)[0].checked = false;
                    }
                }
            })
            //选择优惠券
            $(".select_coupon").click(function(){
                if($(this)[0].checked){//选择了
                    var length_has_overlay = $(".select_coupon:checked[is_overlay='1']").length;
                    var length_no_overlay = $(".select_coupon:checked[is_overlay='0']").length;
                    if(length_has_overlay && length_no_overlay){
                        $(this)[0].checked = false;
                    }
                    if(length_no_overlay > 1){
                        $(this)[0].checked = false;
                    }
                }else{
                    
                }
            })
            
        })
    </script>

<div class="buy_opt_box">
        <div class="all_prize">
            <input type="hidden" value="<?= $total_money[1]  ?>" id="allGoodsTotal_zhe" />
            <span>总计：</span>
            <i>¥</i>
            <em id="all_total_money"><?= $total_money[1] + $freight[0];  ?></em>
        </div>
        <a href="javascript:;" class="buy_btn" id="submitOrder">提交订单</a>
    </div>


</div>
</form>

    
    
<!-- 新增地址 -->
<div id="addressWrapper">
    <form id="creatAddrForm" action="<?php echo $this->context->to(['buy/add_addr']) ?>"  method="post">
    <div class="address_cover"><span></span></div>
    <div class="address_box">
        <header class="page_header">
            <div class="page_headerBg"></div>
            <a class="address_backBtn page_headerBtn" href="javascript::void(0);"><i class="iconfont">&#xe613;</i></a>
            <div class="page_title"><h1>新增地址</h1></div>
        </header>
        <section id="add_form" class="add_form">
            <div class="row">
                <input id="add_name" placeholder="请输入收货人姓名" type="text" name="username" maxlength="20" nullmsg="请输入收货人姓名" datatype="*">
            </div>
            <div class="row">
                <input id="add_phone" placeholder="请输入收货人手机号码" type="text" name="iphone" maxlength="11" errormsg="请输入正确的手机号码" nullmsg="请输入手机号码" datatype="m">
            </div>
            <div class="row">
                <input id="add_area_info" placeholder="请选择地区" type="text" value="" errormsg="请选择完整地区" nullmsg="请选择地区" datatype="wCity" name="address_info">
                <input id="add_area_id" type="hidden" name="area_id" value="37">
                <input id="add_city_id" type="hidden" name="city_id" value="36">
                <input id="province_id" type="hidden" name="province_id" value="36">
                <i class="iconfont arrow">&#xe612;</i>
            </div>
            <div class="row">
                <input id="add_address" placeholder="请输入详细地址" type="text" name="address" datatype="*" maxlength="100"></input>
            </div>
            <div class="row">
                <input id="is_default"  type="hidden" name="is_default"  ></input>
                <i class="ck_icon" id="switchDefault" ischeck="0"></i>
                <div class="default_txt">设为默认地址</div>
            </div>
        </section>
        <div class="add_opt_box">
            <a id="saveAdd_btn" href="javascript:void(0);">保存地址</a>
        </div>
    </div>
    </form>
</div>

<!-- 地址区域列表 -->
<div id="areaWrapper">
    <div class="area_cover"><span></span></div>
    <div class="area_box">
        <header class="page_header">
            <div class="page_headerBg"></div>
            <a class="area_backBtn page_headerBtn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
            <div class="page_title"><h1>配送至</h1></div>
        </header>
        <section id="areaList" class="area_list"></section>
    </div>
</div>

<!-- 地址列表 -->
<div id="listWrapper">
    <div class="list_cover"><span></span></div>
    <div class="list_box">
        <header class="page_header">
            <div class="page_headerBg"></div>
            <a class="list_backBtn page_headerBtn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
            <div class="page_title"><h1>地址列表</h1></div>
        </header>
        <section id="listAddr" class="list_Addr">
            <ul>
                <?php $num = 0; foreach($address_info as $key => $value){ $num++; ?>
                    <li class="<?php if($num == 1){ echo 'active'; } ?>">
                        <div class="item_list" data-city-id="<?= $value['city_id']  ?>" data-address-id="<?= $value['id']  ?>">
                            <i class="iconfont location">&#xe610;</i>
                            <i class="iconfont arrow">&#xe612;</i>
                            <p>收货人：<span class="select_name"><?= $value['username']  ?></span><span class="select_phone"><?= $value['iphone']  ?></span></p>
                            <p>收货地址：<span class="select_address"><?= $value['address_info'] . $value['address']  ?></span></p>
                        </div>
                    </li>                
                <?php }  ?>
            </ul>
        </section>
        <div class="add_opt_box">
            <a id="addNew_btn" href="javascript:void(0);">新增收货地址</a>
        </div>
    </div>
</div>

<?php require $this->context->template('layouts/base/resource'); ?>   
<script>
// 更改地址，获取运费
var changeAddr = "<?= $this->context->to('buy/change_addr') ?>";
var freightHash = "0";
</script>

<script type="text/javascript" src="/resource/js/m.goods_pay.js"></script>
<script type="text/javascript" src="/resource/js/layer.m/layer.m.js"></script>
<script type="text/javascript" src="/resource/js/Validform_v5.3.2/js/Validform_v5.3.2_min.js"></script>
<script src="/resource/js/area_array.js"></script>
<script src="/resource/js/selectArea.js"></script>
</body>
</html>