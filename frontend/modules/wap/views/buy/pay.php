<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <div class="page_title"><h1>付款</h1></div>
    </header>

    <form action="<?php echo $this->context->to(['payment/index_wap']) ?>" method="post" id="buy_form">
        <input type="hidden" name="pay_sn" value="<?php echo $pay_sn; ?>">
        <input type="hidden" id="payment_code" name="payment_code" value="aliwappay">
    </form>    


    <section class="payGoods_ing">
        <p>您需支付<em><?php echo $order['order_amount']; ?></em>元</p>

        <div class="pay_way_img">
            <a class="active" payment_code="aliwappay" href="javascript:;">
                <img src="/resource/images/alipay.png">
            </a>
        </div>

        <a id="next_button" class="pay_submit" href="javascript:;">确 定</a>
    </section>
</div>
<?php require $this->context->template('layouts/base/resource'); ?>    
<script type="text/javascript" src="/resource/js/layer.m/layer.m.js"></script>
<script type="text/javascript" src="/resource/js/Validform_v5.3.2/js/Validform_v5.3.2_min.js"></script>
<script>
$(function() {
    $('.pay_way_img').children('a').on('click', function() {
        $('#payment_code').val($(this).attr('payment_code'));
        $(this).addClass('active').siblings().removeClass('active');
    });
    $('#next_button').on('click', function() {
        var payCode = $('#payment_code').val();
        if (payCode == '' || payCode == undefined) {
            layer.open({
                className: 'alertMsg',
                content: '请选择支付方式！',
                time:2
            });
            return false;
        }
        $('#buy_form').submit();
    });
});
</script>
</body>
</html>