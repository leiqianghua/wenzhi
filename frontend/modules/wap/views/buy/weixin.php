<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <div class="page_title"><h1>付款</h1></div>
    </header>

    <section class="payGoods_ing">
        <p>您需支付<em><?php echo $money; ?></em>元</p>

        <div class="pay_way_img">
            <a class="active" payment_code="wxpay" href="javascript:;">
                <img src="/resource/images/wxpay.png">
            </a>
        </div>

        <a onclick="callpay()" id="next_button" class="pay_submit" href="javascript:;">确 定</a>
    </section>
</div>
    <?=  $js;  ?>
</body>
</html>