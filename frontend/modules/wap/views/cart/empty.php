<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>
<form id="form_buy" name="form_buy" method="post" action="">
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="page_homeBtn page_headerBtn" href="<?= $this->context->to(['index/index']) ?>"><i class="iconfont">&#xe615;</i></a>
        <div class="page_title"><h1>购物车</h1></div>
    </header>

    <section class="cart_main">
        <div class="cart_noGoods">
            <div class="cart_emptyImg"></div>
            <p>购物车空空如也，快去逛一逛吧~</p>
            <a class="cart_back" href="<?= $this->context->to(['index/index']) ?>">回到首页</a>
        </div>
    </section>
</div>
</form>
</body>
</html>