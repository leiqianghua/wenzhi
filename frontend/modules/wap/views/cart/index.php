<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>

<form id="form_buy" name="form_buy" method="post" action="<?= $this->context->to(['buy/buy_step1'])  ?>">
    <input type="hidden" name="ifcart" value="1"/>
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="page_homeBtn page_headerBtn" href="<?= $this->context->to(['index/index']) ?>"><i class="iconfont">&#xe615;</i></a>
        <div class="page_title"><h1>购物车</h1></div>
    </header>

    <section class="cart_main">        
        <?php  foreach($carts as $admin_id => $datas){  ?>
        
            <div data="<?= $admin_id ?>" class="taojian cart_list">
                <div class="cart_hd">
                    <i class="ck_icon" chinaType="checkShopAll" isCheck="0"></i>
                    <div class="eachShop_name"><i class="iconfont">&#xe60b;</i><?= $datas['admin_name']  ?></div>                    
                </div>
                <div class="cart_bd">
                    <?php  foreach($datas['data'] as $cart){  ?>
                    <?php  
                        //print_r($cart);exit;
                        $cart['goods_detail'] = \common\models\GoodsModel::getOne($cart['goods_id']);
                        $cart['money'] = $cart['goods_detail']['price'];
                        $cart['num'] = $cart['num'];
                        $cart['goods_name'] = $cart['goods_detail']['name'];
                        $cart['id'] = $cart['id'];
                    ?>
                        <div data="<?= $cart['id'] ?>" class="cart_item">
                            <?php 
                                if($cart['goods_detail']['num'] <= 0 || $cart['goods_detail']['status'] == 0){
                                    $html = " ck_icon ck_disabled";
                                    $isCheck = 0;
                                }else{
                                    $html = "list_selected ck_icon ck_selected";
                                    $isCheck = 1;                                
                                }
                            ?>
                            <i class="<?= $html ?>" isCheck="<?= $isCheck ?>"></i>
                            <input type="hidden" class="eachGoodsTotal" value="<?= $cart['money']*$cart['num'] ?>">
                            <div class="cart_detail">
                                <div class="cart_img">
                                    <a href="/wap/goods/index?gid=<?= $cart['goods_detail']['id']  ?>">
                                        <img src="<?= $cart['goods_detail']['img']  ?>">
                                        <?php  if($isCheck == 0){ ?>
                                        <span class="time_out">已失效</span>
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="cart_info">
                                    <a href="/wap/goods/index?gid=<?= $cart['goods_detail']['id']  ?>">
                                        <h3><?= $cart['goods_name'] ?></h3>
    <!--                                    <p><span>颜色：黄色</span><span>规格：5.4米</span></p>-->
                                    </a>
                                    <div class="item_amout">
                                        <span class="minus">-</span>
                                        <input type="text" cart_id="<?= $cart['id']  ?>" max_num="<?= $cart['goods_detail']['num']  ?>" value="<?= $cart['num']  ?>">
                                        <span class="plus">+</span>
                                    </div>
                                    <div class="eachGoods_price"><i>¥</i><em class="td_price"><?= $cart['goods_detail']['price']  ?></em></div>
                                </div>
                                <a class="cart_del" onclick="drop_cart_item('<?= $cart['id']  ?>');" href="javascript:void(0);"><i class="iconfont">&#xe61a;</i></a>
                            </div>
                        </div>                  
                    <?php  }  ?>
                    
    
          
                    

                </div>
            </div>        
        <?php }  ?>
        
        <div style="height:4em"  class="cart_item">

            <div style="height:4em"   class="cart_detail">
                <a style="right:10px;" class="cart_del cart_del_all" onclick="drop_cart_all();" href="javascript:void(0);">批量删除<i class="iconfont"></i></a>
                <a style="left:10px;position: absolute;bottom: 1em" class=""  href="<?= $this->context->to(['else_kaidan/create','is_card' => 1]) ?>">添加开单</a>
            </div>
        </div>                
    </section>
</div>

<div class="cart_wrap">
    <div class="c_selectAll">
        <i class="ck_icon" id="checkBoxAllBottom" isCheck="0"></i>全选
    </div>
    <div class="cart_fsum">
        <span class="txt">合计：</span>
        <strong><i>¥</i><em id="allTotalBottom">0.00</em></strong>
    </div>
    <div class="cart_action">
        <a id="next_submit" href="javascript:;">去结算</a>
    </div>
</div>
</form>    
    
    
    
<?php require $this->context->template('layouts/base/resource'); ?>    
<script type="text/javascript" src="/resource/js/layer.m/layer.m.js"></script>
<script type="text/javascript" src="/resource/js/m.goods_cart.js"></script>
</body>
</html>