 <?php foreach($datas as $key => $data){  ?>
        <div class="each_order">
            <div class="order_hd">
                <span><?= $data['username']  ?></span>&nbsp&nbsp
                <span><?= date('Y-m-d',$data['add_time'])  ?></span>
                <select data="<?= $data['uid'] ?>" style="margin-top:5px;" class="order_state" name="label">
                    <?php  $d = \common\models\MemberModel::getLabelHtmlAll();  ?>
                    <?php foreach($d as $k => $v){  ?>
                        <?php  if($k <= \yii::$app->controller->user->label){ continue; }  ?>
                        <option <?php  if($k == $data['label']){ echo 'selected'; }  ?> value="<?= $k  ?>"><?= $v ?></option>
                    <?php }  ?>
                </select>
                
            </div>
        </div>      
<?php }  ?>

<script>
    $(function(){
        $(".order_state").change(function(){
            var value = $(this).val();
            var obj = {};
            obj.uid = $(this).attr('data');
            obj.label = value;
            
            requestajax('<?= \yii::$app->controller->to(['update_label']) ?>',obj);
        })
    })
</script>