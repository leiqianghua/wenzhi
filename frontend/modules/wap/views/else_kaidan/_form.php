<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
?>
<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    <?= $form->field($model, 'title')->textInput()->label("标题"); ?>
    
    

<!--    展示这些商品-->
<div><button type="button" class="select_tigger">选择商品</button></div>
</br>
<div id="show_datas">
    <?php  $shop_ids = json_decode($model->content,true)['content_1'];  ?>
    <?php include '_goods.php';  ?>
</div>


<!--自定义添加商品-->
<div><button type="button" class="">自定义商品</button></div>
<?php 
    $content_2 = json_decode($model->content,true)['content_2'];
    echo  \common\components\Tool::getDataForViewList("content[content_2]", "/else_kaidan/_content", $content_2, "");
?>
    
    
    
    <?= $form->field($model, 'price')->textInput()->label("原价"); ?>
<div class="form-group">
<div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="elsekaidanmodel-zhe_price">参考出厂价</label></div>
    <div  style="" class="col-xs-6 col-sm-4">
        <button type="button" id="show_or_hide_chu">展示</button>&nbsp&nbsp<span style="display: none" id="chuchang">0</span>
    </div>
</div>
    <?= $form->field($model, 'zhe_price')->textInput()->label("折扣价"); ?>

    <?= $form->field($model, 'step')->dropDownList(['1' => "1",'2' => "2",'3' => "3"]) ?>

<div class="form-group field-elsekaidanmodel-step_pay_num">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label" for="elsekaidanmodel-step_pay_num">百分比</label>
    </div>
    <div class="all_step_list col-xs-6 col-sm-4">
<!--        <input type="text" id="elsekaidanmodel-step_pay_num" class="form-control" name="ElseKaidanModel[step_pay_num]">-->
    </div>
</div>

    <?= $form->field($model, 'customer_name') ?>  
    <?= $form->field($model, 'customer_iphone') ?>
    <?= $form->field($model, 'customer_address')->textarea() ?>

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php include __DIR__ . "/../layouts/base/tan_goods.php" ?>

<script>
    $(function(){
        function change_input()
        {
            var step = $("#elsekaidanmodel-step").val();
            var length = $(".all_step_list").find("input").length;
            if(length == step){
                return true;
            }
            var html = "";
            if(step == 1){
                html = "<input type='text' readonly='readonly'  value='100' class='form-control' name='step_pay_num["+i+"]'>";
                $(".all_step_list").html(html); 
                return true;
            }
            for(var i = 1;i <= step;i++){
                html += "<input type='text'  class='form-control' name='step_pay_num["+i+"]'>";
            }
            $(".all_step_list").html(html);
        }
        change_input();
        $("#elsekaidanmodel-step").change(function(){
            change_input();
        })
    })
</script>

<script>
        function contains(arr, obj) {
          var i = arr.length;
          while (i--) {
            if (arr[i] === obj) {
              return true;
            }
          }
          return false;
        }   
        
     var has_data = "<?=  $model->price ?>";
     if(!has_data){
         calcPrice();   
     }        
        
     $(document).on('click',".delete_all_detail",function(){
         $(this).parent().remove();
         calcPrice();
     })
     
     $("#show_or_hide_chu").click(function(){
         var display = $("#chuchang").css("display");
         if(display == "none"){
             $("#chuchang").show();
             $(this).html("隐藏");
         }else{
             $("#chuchang").hide();
             $(this).html("展示");
         }
     })
     
     
     function calcPrice()
     {
        var obj = $(".all_detail_goods");
        var length = obj.length;
        var price = 0;
        var discount_0 = 0;
        for(var i = 0;i<length;i++){
            if(parseInt(obj.eq(i).find(".detail_num").val())){
                console.log('tttt');
                price += parseInt(obj.eq(i).find(".detail_price").val()) * parseInt(obj.eq(i).find(".detail_num").val());
                discount_0 += parseInt(obj.eq(i).find(".detail_price").attr('discount_0')) * parseInt(obj.eq(i).find(".detail_num").val());
            }
        }
        
        var obj = $(".mine_detail_price");
        var length = obj.length;
        for(var i = 0;i<length;i++){
            if(parseInt(obj.eq(i).val())){
                price += parseInt(obj.eq(i).val()) * parseInt(obj.eq(i).parent().next().find(".mine_detail_num").val());
                discount_0 += parseInt(obj.eq(i).val()) * parseInt(obj.eq(i).parent().next().find(".mine_detail_num").val());
            }
        }        
        
        $("#elsekaidanmodel-price").val(price);
        $("#elsekaidanmodel-zhe_price").val(price);
        $("#chuchang").html(discount_0);        
     }
     
     $(document).on('keyup',".detail_num",function(){
         calcPrice();
     })     
     $(document).on('keyup',".mine_detail_price",function(){
         calcPrice();
     })     
     $(document).on('keyup',".mine_detail_num",function(){
         calcPrice();
     })
        
    //选择了然后返回后,的处理方式。  你选择的东西要告诉我,
    $(document).on('click',".area_backBtn",function(){
        var obj = $(".select_goods_checkbox:checked");
        var length = obj.length;
        var shopids = [];//我们要选择的这些。
        for(var i = 0;i<length;i++){
            shopids[i] = obj.eq(i).val();
        }
        //选择的商品在这个shopids里面。然后我这里选择的我要过滤掉一些我这里选择过的。
        var obj = $(".all_detail_goods");
        var length_has = obj.length;
        var shopids_has = [];//选择了的这个shop_id
        for(var i = 0;i<length_has;i++){
            shopids_has[i] = obj.eq(i).attr("data");
        }
        
        var result_shopids = [];
        var num_arr = 0;
        for(var i = 0;i<length;i++){
            if(!contains(shopids_has,shopids[i])){
                result_shopids[num_arr] = shopids[i];
                num_arr++;
            }
        } 
        
        if(!result_shopids){
            return true;
        }
        
        
        var param_obj = {shop_ids:result_shopids};
        if($(".all_detail_goods").length){
            var last_key = $(".all_detail_goods").last().attr("key");
            param_obj.last_key = last_key;
        }
        
        $.post("/wap/else_kaidan/ajax_select_goods",param_obj,function(data){
            if(data.status){
                $("#show_datas").append(data.content);
            }
            calcPrice();
        },"json");
        
    })    
</script>