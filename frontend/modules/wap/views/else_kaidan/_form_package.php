<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
error_reporting(0);
/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
$GLOBALS['my_data'] = $model['content'];
?>

<div class="admin-role-model-form">

    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>

    
    <?=    \kartik\grid\GridView::widget([
        'export' => false,
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '',   //固定不变
        'responsiveWrap' => false,
        'columns' => [
            
            "title",
            
            [
                'class'=>'kartik\grid\ExpandRowColumn',
                'detailAnimationDuration' => 200,
                'width'=>'70px',
                'expandIcon' => '<a class="glyphicon glyphicon-expand " data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="全部展开"></a>',
                'collapseIcon' => '<a class="glyphicon glyphicon-collapse-down" data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="收合"></a>',
                //'header' => '<a class="show_detail" data-toggle="tooltip" title="" href="javascript:void(0);" data-original-title="全部展开"></a>',
                'value'=>function ($model, $key, $index, $column) {
                    return \kartik\grid\GridView::ROW_COLLAPSED;
                },
                'detail'=>function ($model, $key, $index, $column) {
                    $model['label'] = $GLOBALS['my_data'];
                    return Yii::$app->controller->renderPartial('/else_kaidan/_good_detail', ['model'=>$model]);
                },
                'headerOptions'=>['class'=>'kartik-sheet-style'], 
                'expandOneOnly'=>true,
                'detailRowCssClass' => 'success',
            ],  
            [
               'value' => function($model, $key, $index, $column){
                    //这里面总共是多少的价格
                    return "<span class='total_money' data='{$model['id']}'>0</span>";
               },
               'label' => '小计', 
               'format' => 'raw',                      
            ],  
 
        ],
    ]); ?>    
        
    
    
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'price') ?>
<div class="form-group">
<div class="col-xs-3 col-sm-2 text-right"><label class="control-label" for="elsekaidanmodel-zhe_price">参考出厂价</label></div>
    <div  style="" class="col-xs-6 col-sm-4">
        <button type="button" id="show_or_hide_chu">展示</button>&nbsp&nbsp<span style="display: none" id="chuchang">0</span>
    </div>
</div>
    <?= $form->field($model, 'zhe_price') ?>
    <?= $form->field($model, 'type')->hiddenInput()->label(""); ?>
    
    
    <?= $form->field($model, 'step')->dropDownList(['1' => "1",'2' => "2",'3' => "3"]) ?>

<div class="form-group field-elsekaidanmodel-step_pay_num">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label" for="elsekaidanmodel-step_pay_num">百分比</label>
    </div>
    <div class="all_step_list col-xs-6 col-sm-4">
<!--        <input type="text" id="elsekaidanmodel-step_pay_num" class="form-control" name="ElseKaidanModel[step_pay_num]">-->
    </div>
</div>
    

    <?= $form->field($model, 'customer_name') ?>  
    <?= $form->field($model, 'customer_iphone') ?>
    <?= $form->field($model, 'customer_address')->textarea() ?>
    

 


    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>



<div id="areaWrapper">
    <div class="area_cover"><span></span></div>
    <div class="area_box">
        <header class="page_header">
            <div class="page_headerBg"></div>
            <a class="area_backBtn page_headerBtn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
            <div class="page_title"><h1>商品选择</h1></div>
        </header>
        <section id="areaList" class="area_list">

        </section>
    </div>
</div> 

<script>
    $(function(){
        function change_input()
        {
            var step = $("#elsekaidanmodel-step").val();
            var length = $(".all_step_list").find("input").length;
            if(length == step){
                return true;
            }
            var html = "";
            if(step == 1){
                html = "<input type='text' readonly='readonly'  value='100' class='form-control' name='step_pay_num["+i+"]'>";
                $(".all_step_list").html(html); 
                return true;
            }
            for(var i = 1;i <= step;i++){
                html += "<input type='text'  class='form-control' name='step_pay_num["+i+"]'>";
            }
            $(".all_step_list").html(html);
        }
        change_input();
        $("#elsekaidanmodel-step").change(function(){
            change_input();
        })
    })
</script>

<script>
    
    SELECT_ZI_CID = false;
    
    
    $(function(){
        //选择商品
        var _height=$(window).height();
        var _height2 = _height - 45;   // 选择地区高度        
        
        $(document).on("click",".delete_select",function(){
            $(this).parent().parent().remove();
        })
        
        $(".select_goods").click(function(){
            var cid  = $(this).attr("data");
            SELECT_ZI_CID = cid;
            //把找到的内容全部放到areaList里面去.
            $.get("select_index",{cid:cid},function(data){
                if(data.status){
                    $("#areaList").html(data.content);
                    $('#areaWrapper').addClass('area_show');
                    $('.area_cover').fadeIn();
                    $("#areaList").css({"height":_height2})                    
                }
            },"json");
        })
        
        $(document).on('click',".area_backBtn",function(){
            $('#areaWrapper').removeClass('area_show');
            $('.area_cover').fadeOut();
            $('#areaList').css({"height":'auto'})
        }) 
        
        function contains(arr, obj) {
          var i = arr.length;
          while (i--) {
            if (arr[i] === obj) {
              return true;
            }
          }
          return false;
        }   
     
     var has_data = "<?=  $model->price ?>";
     if(!has_data){
         calcPrice();   
     }
     calcPrice(true);
     function calcPrice(is_calc)
     {
        var obj = $(".num_show");
        var length = obj.length;
        var price = 0;
        var discount_0 = 0;
        var oo_xiaoji = {};
        var parent_id = null;
        for(var i = 0;i<length;i++){
            if(parseInt(obj.eq(i).val())){
                price += parseInt(obj.eq(i).val()) * parseInt(obj.eq(i).attr("price"));
                discount_0 += parseInt(obj.eq(i).val()) * parseInt(obj.eq(i).attr("discount_0"));
                parent_id = obj.eq(i).parents(".skip-export").eq(0).attr("data-key");
                if(typeof(oo_xiaoji[parent_id]) == "undefined"){
                    oo_xiaoji[parent_id] = parseInt(obj.eq(i).val()) * parseInt(obj.eq(i).attr("price"));
                }else{
                    oo_xiaoji[parent_id] += parseInt(obj.eq(i).val()) * parseInt(obj.eq(i).attr("price"));
                }
            }
        }
        console.log(is_calc);
        if(is_calc){
            for(dd in oo_xiaoji){
                $(".total_money[data='"+dd+"']").html(oo_xiaoji[dd]);
            }
            return true;
        }
        $("#elsekaidanmodel-price").val(price);
        $("#elsekaidanmodel-zhe_price").val(price);
        $("#chuchang").html(discount_0);
        for(dd in oo_xiaoji){
            $(".total_money[data='"+dd+"']").html(oo_xiaoji[dd]);
        }
     }
     
     $(document).on('keyup',".num_show",function(){
         calcPrice();
     })       
     $(document).on('click',".delete_select",function(){
         calcPrice();
     })    
        
     $("#show_or_hide_chu").click(function(){
         var display = $("#chuchang").css("display");
         if(display == "none"){
             $("#chuchang").show();
             $(this).html("隐藏");
         }else{
             $("#chuchang").hide();
             $(this).html("展示");
         }
     })        
        
        //选择好了后然后拿过来使用
    //选择了然后返回后,的处理方式。  你选择的东西要告诉我,
        $(document).on('click',".area_backBtn",function(){
            var obj = $(".select_goods_checkbox:checked");
            var length = obj.length;
            var shopids = [];//我们要选择的这些。
            for(var i = 0;i<length;i++){
                shopids[i] = obj.eq(i).val();
            }
            //选择的商品在这个shopids里面。然后我这里选择的我要过滤掉一些我这里选择过的。
            var obj = $("#goods_list_select_ok_" + SELECT_ZI_CID).find("tr[key]");
            var length_has = obj.length;
            var shopids_has = [];//选择了的这个shop_id
            for(var i = 0;i<length_has;i++){
                shopids_has[i] = obj.eq(i).attr("data");
            }

            var result_shopids = [];
            var num_arr = 0;
            for(var i = 0;i<length;i++){
                if(!contains(shopids_has,shopids[i])){
                    result_shopids[num_arr] = shopids[i];
                    num_arr++;
                }
            } 
            if(!result_shopids){
                return true;
            }


            var param_obj = {shop_ids:result_shopids};
            param_obj.name_show = "content[content_3]["+SELECT_ZI_CID+"]";
            if($("#goods_list_select_ok_" + SELECT_ZI_CID).find("tr[key]").length){
                var last_key = $("#goods_list_select_ok_" + SELECT_ZI_CID).find("tr[key]").last().attr("key");
                //alert(last_key);
                param_obj.last_key = last_key;
            }

            $.post("/wap/else_kaidan/ajax_select_goods_project",param_obj,function(data){
                if(data.status){
                    $("#goods_list_select_ok_" + SELECT_ZI_CID).append(data.content);
                }
                calcPrice();
            },"json");
        
        })        
    })
</script>