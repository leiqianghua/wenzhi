<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

    <h1>开单列表</h1>

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline J_ajaxForm'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'status')->dropDownList($object::getStatusHtml(),['prompt' => '全部'])->label("状态") ?>
            <?= $form->field($model, 'type')->dropDownList($object::getTypeHtml(),['prompt' => '全部'])->label("类型") ?>
            

            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            
            <div class="form-group">
                <a href="create">
                    <?= Html::button('添加开单', ['class' => 'btn']) ?>
                </a>
            </div>            
            <?php ActiveForm::end(); ?>

        </div>
    </div>    

<div style="display:none" id="dddd">
    <form class="layermmain">
        <input type="text" name="bbbbbb" value="ccc" />
    </form>    
</div>


<script>
    var DIALOG_CONTENT = $("#dddd").html();
</script>    
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '总共{totalCount}条数据',   //固定不变
        'columns' => [
            'title',//直接展示
//            'customer_name',//直接展示
//            'customer_iphone',//直接展示
            'zhe_price',//直接展示
            //'price',//直接展示
            ['attribute' => 'add_time','format' => ['date', 'yyyy-MM-dd']],//时间展示
            [
                'attribute' => 'status',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::getStatusHtml($model->status);
                }
            ],
            [
                'attribute' => 'type',
                'value' => function($model){
                    $d = get_class($model);
                    return $d::gettypeHtml($model->type);
                }
            ],
            [
                'label' => "付款链接",
                'value' => function($model){
                    $html = "http://www.gubatoo.com/wap/else_kaidan/show?id={$model['id']}";
                    $html = "<a class='fuzhi' href='javascript:void(0)' data='{$html}'>复制</a>";
                    return $html;
                },
               'format' => 'raw',  
            ],
            [
               'value' => function($model, $key, $index, $column){
                    if($model->type){
                        $html = "<a href='".\yii::$app->controller->to(['update_package','id' => $model->id])."'  >编辑</a>";
                    }else{
                        $html = "<a href='".\yii::$app->controller->to(['update','id' => $model->id])."'  >编辑</a>";
                    }
                    
                    $html .= "</br>";
                    $html .= "<a href='".\yii::$app->controller->to(['show','id' => $model->id])."'  >详情</a>";
                    $html .= "</br>";
                    $html .= "<a href='".\yii::$app->controller->to(['delete','id' => $model->id])."' class='ajax_request' >删除</a>";
                    return $html;
               },
               'label' => '管理', 
               'format' => 'raw',                      
            ],  
        ],
    ]); ?>

</div>

<script type="text/javascript">
    
    $(".fuzhi").click(function(){
        var  text = $(this).attr("data");
        copyTextToClipboard(text);
    })
    
    
    var messageDOM = document.getElementById('messageDOM');

    // 实现一个简单的委托，简化绑定事件代码
    document.addEventListener('click', function (event) {
      var copyBtn = event.srcElement;
      if (copyBtn.className !== 'copyBtn')
        return ;
      var sourceDOM = document.getElementById(copyBtn.getAttribute('source-element'));
      var text = getElementText(sourceDOM);
      if (copyTextToClipboard(text)) {
        messageDOM.innerText = '复制成功！';
      } else {
        messageDOM.innerText = '复制失败！';
      }
      document.body.removeChild(textArea);
      setTimeout(function () {
        messageDOM.innerText = '';
      }, 2000);
    }, false);

    function getElementText(element) {
      var nodeName = element.nodeName,
          text = '';
      switch(nodeName) {
        case 'SELECT':
        case 'INPUT':
        case 'TEXTAREA':
          text = element.value;
        break;
        default:
          text = element.innerText;
      }
      return text;
    };

    function copyTextToClipboard(text) {
      var textArea = document.createElement('textarea');
      textArea.style.position = 'fixed';
      textArea.style.top = '-9999px';
      textArea.style.left = '-9999px';
      textArea.style.zIndex = -9999;
      textArea.value = text;
      textArea.setAttribute('readonly', '');
      document.body.appendChild(textArea)

      textArea.select();
      textArea.setSelectionRange(0, textArea.value.length);

      var flag = false;
      try {
        flag = document.execCommand('copy');
      } catch (err) {

      }
      return flag;
    }
</script>

