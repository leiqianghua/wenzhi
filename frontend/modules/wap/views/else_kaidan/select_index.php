<div class="zh_container">
    <header class="header_wrapper">
            <div class="page_title">
                <h2>
                    选择商品
                </h2>
            </div> 
        
    </header>
    


    <section style='height: auto' class="goods_container clear" id="goods_container">
        <ul style='height: auto' id="goods_list" class="clear">
            <?php require $this->context->template('layouts/base/goods_select') ?>
        </ul>
    </section>

    <?php require $this->context->template('layouts/base/msg_more'); ?>
    
    <button class="area_backBtn btn-block btn-primary btn btn-lg">确定</button>

</div>




<?php require $this->context->template('layouts/base/backtotop'); ?>    


    
    
    <script>
        
        
        $(".area_backBtn").click(function(){
            var obj = $(".select_goods_checkbox:checked");
            var length = obj.length;
            var shopids = [];
            for(var i = 0;i<length;i++){
                shopids[i] = obj.eq(i).val();
            }
            
        })     
        
//        $(".my_ok").click(function(){
//            $(".area_backBtn").click();
//        })
        

        
        
        //下拉加载更多
        $(function(){
            $(".msg_more").click(function(){
                jiaziaMore('<?= $this->context->to(['else_kaidan/ajax_select_index']) ?>');
            })
            
            $(window).on('scroll', function(){
                var he = $(document).scrollTop() / $(document).height();
                if (he > 0.20) {
                    jiaziaMore('<?= $this->context->to(['else_kaidan/ajax_select_index']) ?>');
                }
            });                
        })
    </script>  
