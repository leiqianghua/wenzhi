<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $content = json_decode($model->content,true);
error_reporting(0)  ?>
<table class="table table-striped table-bordered detail-view">
    <tbody>
        <tr>
            <th>标题</th>
            <td><?= $model['title']  ?></td>
        </tr>
        <tr>
            <th>客户名称</th>
            <td><?= $model['customer_name']  ?></td>
        </tr>
        <tr>
            <th>客户电话</th>
            <td><?= $model['customer_iphone']  ?></td>
        </tr>
        <tr>
            <th>客户地址</th>
            <td><?= $model['customer_address']  ?></td>
        </tr>
        
        <?php if(!empty($content['content_1'])){ foreach($content['content_1'] as $data){  ?>
            <tr>
                <th ><a href="/wap/goods/index?gid=<?=  $data['common_id'] ?>"><?= $data['name'] ?></a></th>
                <th><img class="show_img" style="width: 60px;" src="<?= $data['img'] ?>" />
                    </br>
                    <?= "数量:{$data['buy_num']},单价{$data['price']}"  ?>
                </th>
                <td></td>
            </tr>        
        
        <?php }}  ?>
            
            
        <?php if(!empty($content['content_2'])){ foreach($content['content_2'] as $data){  ?>
            <tr>
                <th><?= $data['title'] ?></th>
                <th> <img class="show_img" style="width: 60px;" src="<?= $data['duoqi_icon'] ?>" />
                    </br>
                    <?= "数量:{$data['num']},单价{$data['price']}"  ?>
                </th>
            </tr>        
        
        <?php }}  ?>            
        
        <tr>
            <th>原价</th>
            <td><?= $model['price']  ?></td>
        </tr>
        <tr>
            <th>折扣价</th>
            <td><?= $model['zhe_price']  ?></td>
        </tr>
    </tbody>
</table>


<?php if(!$model['status']){   ?>
<div class="form-group">
    <a href="/wap/else_kaidan/pay?id=<?= $model['id'] ?>">
        <button type="button" class="J_ajax_submit_btn btn btn-success">付款</button>  
    </a>
</div>

<?php }else{  ?>

<h2>已付款</h2>

<?php } ?>

