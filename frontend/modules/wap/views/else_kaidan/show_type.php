<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $content = json_decode($model->content,true);
error_reporting(0)  ?>
<table class="table table-striped table-bordered detail-view">
    <tbody>
        <tr>
            <th>标题</th>
            <td><?= $model['title']  ?></td>
        </tr>
        
        <?php if(!empty($content['content_1'])){ foreach($content['content_1'] as $data){  ?>
            <tr>
                <th ><a href="/wap/goods/index?gid=<?=  $data['common_id'] ?>"><?= $data['name'] ?></a></th>
                <th><img class="show_img" style="width: 60px;" src="<?= $data['img'] ?>" />
                    </br>
                    <?= "数量:{$data['buy_num']},单价{$data['price']}"  ?>
                </th>
                <td></td>
            </tr>        
        
        <?php }}  ?>
            
            
        <?php if(!empty($content['content_2'])){ foreach($content['content_2'] as $data){  ?>
            <tr>
                <th><?= $data['title'] ?></th>
                <th> <img class="show_img" style="width: 60px;" src="<?= $data['duoqi_icon'] ?>" />
                    </br>
                    <?= "数量:{$data['num']},单价{$data['price']}"  ?>
                </th>
            </tr>        
        
        <?php }}  ?>      
        <tr>
            <th>客户名称</th>
            <td><?= $model['customer_name']  ?></td>
        </tr>
        <tr>
            <th>客户电话</th>
            <td><?= $model['customer_iphone']  ?></td>
        </tr>
        <tr>
            <th>客户地址</th>
            <td><?= $model['customer_address']  ?></td>
        </tr>      
        
        <tr>
            <th>原价</th>
            <td><?= $model['price']  ?></td>
        </tr>
        <tr>
            <th>折扣价</th>
            <td><?= $model['zhe_price']  ?></td>
        </tr>
        <tr>
            <th>分几次付款</th>
            <td><?= $model['step']  ?></td>
        </tr>
        <tr>
            <th>每次付款比例</th>
            <td><?= $model['step_pay_num']  ?></td>
        </tr>
        <tr>
            <th>已付款次数</th>
            <td><?= $model['step_has_ok']  ?></td>
        </tr>
    </tbody>
</table>


<?php  
    if($model['step_has_ok'] == $model['step'] && $model['last_setp_is_confirm']){//已经完成了  完成状态
        echo "<h2>已完成付款</h2>";
    }else if($model['last_setp_is_confirm'] || !$model['step_has_ok']){//用户一步都没有完成  或者 最后一次的用户付款已经确认了
?>
<h2>需付款<?php   $arr = explode(',', $model['step_pay_num']); echo $arr[$model->step_has_ok]/100*$model->zhe_price  ?></h2>
    <div class="form-group">
        <a href="/wap/else_kaidan/pay?id=<?= $model['id'] ?>">
            <button type="button" class="J_ajax_submit_btn btn btn-success">在线付款</button>  
        </a>
    </div>
    
    
<div>
    <div style="text-align: center"><button type="button" class="btn-block btn-primary btn btn-lg">线下支付方式</button></div>
    <?php echo \common\models\ConfigModel::getOne("pay_config","value");  ?>
</div>



    <div class="form-group">
        <?php
            $form = ActiveForm::begin([
                    'options' => ['class' => 'J_ajaxForm form-horizontal'],
                    'enableClientScript' => false,
                    'action' => "pay_chuli?id=" . \common\components\GuestInfo::getParam("id"),
                    'fieldConfig' => [
                        'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                    ]        
            ]);
        ?>

            <?php  $date = date('Ymd'); ?>
            <?php  //$model->user_upload_img = json_decode($model->user_upload_img,true) ?>
            <?php 
                $operation = [];
                $operation['button'] = [
                    'name' => "上传支付截图",
                    'class' => "btn-block btn-primary btn-info btn btn-lg",
                ];
            ?>
            <?= Form::getFileContent($form,$model,'user_upload_img','user_upload_img', $date . '/user_upload_img','',$operation,'',true); ?>


         <div class="form-group">
            <div class='col-xs-3 col-sm-4 text-right'>
            <?= Html::submitButton($model->isNewRecord ? '提交' : '提交', ['class' => $model->isNewRecord ? 'J_ajax_submit_btn btn btn-success' : 'J_ajax_submit_btn btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>       
        
    </div>

    


<?php
    }else{//等待着确认
        echo "<h2>请等待管理员确认付款</h2>";
        
    }
?>


<style>
    
</style>