<?php
$goodInfo = $datas['data_common'];
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head_1'); ?>
</head>
<body>
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="page_homeBtn page_headerBtn" href="<?= $this->context->to(['index/cha_goods']) ?>"><i class="iconfont">&#xe615;</i></a>
        <div class="page_title"><h1>商品详情</h1></div>
    </header>
    <section class="page_scroll">
        <div class="swiper-container page_scrollPIc">
            <div class="swiper-wrapper">
                <?php  $imgs = json_decode($goodInfo['img_list'], true);
                        if(empty($imgs)){ $imgs = []; }
                ?>
                <?php foreach ($imgs as $key => $image): ?>
                    <div class="swiper-slide"><img src="<?= $image ?>" alt=""></div>
                <?php endforeach; ?>                
            </div>
            <div class="swiper-pagination page_scrollPag"></div>
        </div>
    </section>
    <section class="page_detail">
        <?php 
        if($goods_id && !empty($datas['data_goods_list'][$goods_id])){ 
            $price_detail = $datas['data_goods_list'][$goods_id];
            
        } else{
            $price_detail = $goodInfo;
        }
        ?>
        
        <h1><?= $price_detail['name'] ?></h1>
        <div class="detail_pirce">
            <span><i>¥</i><em id="goods_price"><?= $price_detail['discount_4'] ?></em></span><small>¥<em id="goods_marketprice"><?= $price_detail['price'] ?></em></small>
            <?php  if($price_detail['score']){ ?>
            &nbsp;<span><i>积分:</i><em id=""><?= $price_detail['score'] ?></em></span>
            <?php }  ?>            
<!--            <span><i>¥</i><em id="goods_price"><?= $price_detail['price'] ?></em></span><small>¥<em id="goods_marketprice">5</em></small>-->
            <div class="detail_num">已售<em><?= $price_detail['sale_num'] ?></em>件</div>
        </div>

    </section>

    <section class="sku_box detail_guige">
        
        <?php  $yes_click_true = $goodInfo['status'] && $price_detail['num'];?>
        
        <?php  
        $spec = $goodInfo['spec_1'];
        if(!empty($spec['spec_name'])){     
        ?>
        
            <dl class="spec_num">
                <dt><?php echo $spec['spec_name'] ?>：</dt>
                <dd class="spec <?php  if(empty($hasimg)){ echo 'ify_yes'; }else{ echo 'ify_no'; }  ?>" >
                    <?php $num_key= 0; foreach($spec['data'] as $spec_id => $value){ 
                        $is_c = isset($price_detail['spec_1_num']) && $price_detail['spec_1_num'] == $num_key;
                    ?>
                        <!--//选中的规格$datas['data_detail']['spec'][$spec_id]-->
                        <span class="<?php if($yes_click_true){  echo 'yes_click'; }  ?> <?php if($is_c){ echo 'active'; } ?>" data-spec_num ="1" data-param="<?php echo $num_key;  ?>" ><?php echo $value['spec_name'];  ?></span>
                    <?php $num_key++; } ?>                            
                </dd>                        
            </dl>        
        
        <?php }  ?>

        
        
        <?php  
        $spec = $goodInfo['spec_2'];
        if(!empty($spec['spec_name'])){     
        ?>
        
            <dl class="spec_num">
                <dt><?php echo $spec['spec_name'] ?>：</dt>
                <dd class="spec <?php  if(empty($hasimg)){ echo 'ify_yes'; }else{ echo 'ify_no'; }  ?>" >
                    <?php $num_key= 0; foreach($spec['data'] as $spec_id => $value){ 
                        $is_c = isset($price_detail['spec_2_num']) && $price_detail['spec_2_num'] == $num_key;
                    ?>
                        <!--//选中的规格$datas['data_detail']['spec'][$spec_id]-->
                        <span class="<?php if($yes_click_true){  echo 'yes_click'; }  ?> <?php if($is_c){ echo 'active'; } ?>" data-spec_num ="2" data-param="<?php echo $num_key;  ?>" ><?php echo $value['spec_name'];  ?></span>
                    <?php $num_key++; } ?>                            
                </dd>                        
            </dl>        
        
        <?php }  ?>        
        
        
        <?php  
        $spec = $goodInfo['spec_3'];
        if(!empty($spec['spec_name'])){     
        ?>
        
            <dl class="spec_num">
                <dt><?php echo $spec['spec_name'] ?>：</dt>
                <dd class="spec <?php  if(empty($hasimg)){ echo 'ify_yes'; }else{ echo 'ify_no'; }  ?>" >
                    <?php $num_key= 0; foreach($spec['data'] as $spec_id => $value){ 
                        $is_c = isset($price_detail['spec_3_num']) && $price_detail['spec_3_num'] == $num_key;
                    ?>
                        <!--//选中的规格$datas['data_detail']['spec'][$spec_id]-->
                        <span class="<?php if($yes_click_true){  echo 'yes_click'; }  ?> <?php if($is_c){ echo 'active'; } ?>" data-spec_num ="3" data-param="<?php echo $num_key;  ?>" ><?php echo $value['spec_name'];  ?></span>
                    <?php $num_key++; } ?>                            
                </dd>                        
            </dl>        
        
        <?php }  ?>        
        
        
        
        <!--库存展示-->
        <dl>
            <dt>数量：</dt>
            <dd>
                <div class="input_num" id="buyNum" data-storage="<?= $price_detail['num'] ?>">
                    <i class="iconfont minus">-</i>
                    <input type="text" value="1" id="goods_num" name="goods_num">
                    <i class="iconfont add">+</i>                    
                </div>
                <div class="item_stock">库存<em id="goods_storage"><?= $price_detail['num'] ?></em>件</div>
            </dd>
        </dl>        
        
        <dl>
            <dt>承诺：</dt>
            <dd>
                <ul class="pz_box">
                    <li class="pz_1"><i class="pz_icon"></i>正品保证</li>
                    <li class="pz_2"><i class="pz_icon"></i>闪电发货</li>
                    <li class="pz_3"><i class="pz_icon"></i>售后保障</li>
                </ul>
            </dd>
        </dl>
    </section>



    <section class="detail_wrapper">

        <div class="detail_container" id="detail_container">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-no-swiping">
                    <div class="instroduce_wrapper">
                        <h3>商品规格</h3>
                        <?php $mine_attributes = json_decode($price_detail['mine_attributes'], true);if(!$mine_attributes){ $mine_attributes = []; }  ?>
                        <div class="instroduce_guige">
                            <?php foreach ($mine_attributes as $key => $val){  ?>
                                <dl><dt><?= $val['key'] ?></dt><dd><?= $val['value'] ?></dd></dl>                            
                            <?php }  ?>
                        </div>                        

                        <h3>图片详情</h3>
                        <div class="instroduce_img">
                            <?= $price_detail['details'];  ?>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </section>
</div>

<div class="buy_wrap">
    <form>
        <a class="detail_kefu cell" href="mqqwpa://im/chat?chat_type=wpa&uin=405747275&version=1&src_type=web&web_src=oicqzone.com" target="_blank">客服<i class="iconfont">&#xe626;</i></a>
<!--        <a id="collect" class="collect_act cell active" href="javascript:;">收藏<i class="iconfont">&#xe619;</i></a>-->
        <div class="buy_action">
            <?php
            /* 商品未上架的不显示 */
            if ($goodInfo['status'] == 1):
            ?>
            <a href="javascript:;" class="btn_buy  <?php if ($price_detail['num']) echo 'buynow_submit'; ?> <?php echo $price_detail['num'] < 1 ? 'buy_no' : 'buy_no'; ?>"   title="立即购买">立即购买</a>
            <a href="javascript:;" class="btn_add_car <?php echo $price_detail['num'] < 1 ? 'buy_no' : 'buy_no'; ?>"  title="加入购物车">加入购物车</a>
            <?php else: ?>
                <a href="javascript:;" class="btn_buy buy_no buy_sold_out">商品已下架</a>
            <?php endif; ?>            
            
        </div>
    </form>
</div>

<?php require $this->context->template('layouts/base/backtotop'); ?>   

    <form id="buynow_form" method="post" action="<?= $this->context->to(['buy/buy_step1'])  ?>">
        <input id="num" name="num" type="hidden"/>
        <input id="goods_id" name="goods_id" type="hidden"/>
        <input id="ifcart" name="ifcart" value="0" type="hidden"/>
        <input id="type" name="type" value="1" type="hidden"/>
    </form>    
    
<?php require $this->context->template('layouts/base/resource'); ?>   

<script src="/resource/js/goods.js"></script>    
<script>
    //系统数据
    var DATAS = <?php  echo json_encode($datas); ?>   
    var result_data = <?php  echo json_encode($result_data); ?>   
    var IS_LOGIN = '<?php echo \yii::$app->controller->user->isLogin();  ?>';
    var STORE_SELF = '0';
    var GOODS_DEFAULT_PIC = '<div class="mod_fly_cart" id="flyCart"><img src="/images/default.jpg"></div>';
    //收藏地址
    var COLLECT_URL = ''; 

    //运费
    var CALC_URL = '0';
    
    var IMG_URL = '';
    
    var GOODS_ID = '<?php echo $goods_id; ?>';
    //始发地商品ID
    var ZH_SENT = '';
</script>
<script>
$(function(){
    //详情页-图片轮播
    var detailScroll = new Swiper('.page_scrollPIc', {
        pagination: '.page_scrollPag',
        paginationClickable: true,
        autoplay : 4000
    })

    //详情页-商品介绍
    var detailInstroduce = new Swiper('#detail_container',{
        // onInit: function(swiper){
        //     swiper.container[0].style.height=swiper.slides[swiper.activeIndex].offsetHeight+'px';
        // },
        onSlideChangeStart: function(swiper){
            $(".goods_tab .active").removeClass('active');
            $(".goods_tab a").eq(swiper.activeIndex).addClass('active');
            $('#detail_container').find('.swiper-slide').height(0);
            swiper.slides[swiper.activeIndex].style.height='auto';
        }
    })
    $(".detail_tab a").on('touchstart mousedown',function(e){
        e.preventDefault();
        $(".detail_tab .active").removeClass('active');
        $(this).addClass('active');
        detailInstroduce.slideTo( $(this).index() );
    });

    //商品详情页滚动跟随
    var objTab= $('.detail_tab').offset();
    if(objTab != 'undefined'){
        var objTop = objTab.top;
        $(window).scroll(function () {
            if ($(this).scrollTop() > objTop) {
                $('.detail_tab').addClass('fix_filter');
            } else {
                $('.detail_tab').removeClass('fix_filter');
            }
        });
        if ($(window).scrollTop() > objTop) {
            $('.detail_tab').addClass('fix_filter');
        }else{
            $('.detail_tab').removeClass('fix_filter');
        }
    }

})
</script>
<script type="text/javascript" src="/resource/js/layer.m/layer.m.js"></script>
</body>
</html>