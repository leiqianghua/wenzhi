<?php
$datas['num'] = 99999;
$goodInfo = $datas;
$GLOBALS['my_data'] = $datas['shop_list'];
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
    
    <style>
        .btn{
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;    
        }
    </style>    
    
</head>
<body>
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="page_homeBtn page_headerBtn" href="<?= $this->context->to(['index/index']) ?>"><i class="iconfont">&#xe615;</i></a>
        <div class="page_title"><h1><?= $datas['name']  ?></h1></div>
    </header>
    <section class="page_scroll">
        <div class="swiper-container page_scrollPIc">
            <div class="swiper-wrapper">
                <?php  $imgs = json_decode($datas['img_list'], true) ?>
                <?php foreach ($imgs as $key => $image): ?>
                <div class="swiper-slide"><img src="<?= common\models\GoodsModel::getShopImg($image, 640) ?>" alt=""></div>
                <?php endforeach; ?>                
            </div>
            <div class="swiper-pagination page_scrollPag"></div>
        </div>
    </section>
    <section class="page_detail">
        <h1><?= $goodInfo['name'] ?></h1>

        

    </section>

    <section class="sku_box detail_guige mine_box">
        

        
        <!--库存展示-->
        <?php $shoplist = json_decode($datas['shop_list'], true);  ?>
        
        <?php foreach($goods_class as $keys => $values){  ?>
            <h3><?= $goods_class['title']  ?></h3>
            <?php  if(!empty($shoplist[$values['id']])){  ?>
                <?php foreach($shoplist[$values['id']] as $key => $value){  ?>
                    <dl  class="content_list">
                            <dt><a href="javasript:void(0)"><img src="<?= $value['img']  ?>"></a></dt>
                        <dd>
                            <div class="item_stock">数量<em class="goods_storage"><?= $value['num'] ?></em>件</div>
                        </dd>
                        <dd>
                            <div class="goods_detail_name item_stock">
                                <?= $value['name'] ?>        </div>
                        </dd>
                    </dl>                    
            
                <?php }  ?>
            
            <?php }  ?>
        
        <?php }  ?>


        
        
        
        

        <div id="goods_detail">
        <div class="detail_pirce all_money">
           <span>商品总计：<i>¥</i><em id="goods_price"><?= $datas['price'] ?></em></span>
<!--           <small>¥<em id="goods_marketprice">5</em></small>-->
<!--            <div class="detail_num">已售<em><?= $datas['sale_num'] ?></em>件</div>-->
        </div>        

        </div>
       
         
        
    
        
        <dl>
            <dt>承诺：</dt>
            <dd>
                <ul class="pz_box">
                    <li class="pz_1"><i class="pz_icon"></i>正品保证</li>
                    <li class="pz_2"><i class="pz_icon"></i>闪电发货</li>
                    <li class="pz_3"><i class="pz_icon"></i>售后保障</li>
                </ul>
            </dd>
        </dl>
    </section>

    <section class="detail_wrapper">
        <div class="detail_tab">
            <a class="active" href="javascript:void(0);">商品介绍</a>
            <a id="guest_goods" href="javascript:void(0);">猜你喜欢</a>
            <a id="cid_goods" href="javascript:void(0);">同类商品</a>
        </div>
        <div class="detail_container" id="detail_container">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-no-swiping">
                    <div class="instroduce_wrapper">
                        <h3>图片详情</h3>
                        <div class="instroduce_img">
                            <?= $datas['details'];  ?>
                        </div>
                    </div>
                </div>
                
                


                <div class="swiper-slide swiper-no-swiping">
                    
                    <div class="guest_goods goods_container clear">
                        <ul id="goods_list" class="clear"></ul>
                        <?php require $this->context->template('layouts/base/msg_more'); ?>
                        <script>
                            $(function(){
                                if(!$("#goods_list").html()){
                                    GOODS_DATA_CONDITION.page = 0;
                                    jiaziaMore('<?= $this->context->to(['index/ajax_recommand_project'])  ?>');
                                }
                                $(".guest_goods .msg_more").click(function(){
                                    jiaziaMore('<?= $this->context->to(['index/ajax_recommand_project'])  ?>');
                                })
                            })
                        </script>
                        
                    </div>   
                    
                </div>
                
                
                <div class="swiper-slide swiper-no-swiping">
                    
                    <div class="cid_goods goods_container clear">
                        <ul id="goods_list_cid" class="clear"></ul>
                        <?php require $this->context->template('layouts/base/msg_more'); ?>
                        <script>
                            $(function(){
                                if(!$("#goods_list_cid").html()){
                                    GOODS_DATA_CONDITION.page = 0;
                                    jiaziaMore('<?= $this->context->to(['index/project','cid' => $datas['cid']])  ?>',"#goods_list_cid");
                                }
                                $(".cid_goods .msg_more").click(function(){
                                    jiaziaMore('<?= $this->context->to(['index/project','cid' => $datas['cid']])  ?>',"#goods_list_cid");
                                })
                            })
                        </script>
                        
                    </div>   
                    
                </div>                
            </div>
        </div>
    </section>    

</div>

<?php  if(\yii::$app->controller->user->isLogin() && \yii::$app->controller->user->label <= 2){   ?>
    <div class="buy_wrap">
        <form>
            <a class="detail_kefu cell" href="mqqwpa://im/chat?chat_type=wpa&uin=385612297&version=1&src_type=web&web_src=oicqzone.com" target="_blank">客服<i class="iconfont">&#xe626;</i></a>
    <!--        <a id="collect" class="collect_act cell active" href="javascript:;">收藏<i class="iconfont">&#xe619;</i></a>-->
            <a class="cart_act cell" href="<?= $this->context->to(['cart/index']) ?>">购物车<i class="iconfont">&#xe604;</i><span class="cart_num"><?= $cart_goods_num;  ?></span></a>
            <div class="buy_action">

                <a style="width:100%" href="/wap/else_kaidan/create_package?goods_id=<?=  $datas['id'] ?>" class="btn_buy"   title="进行开单">进行开单</a>

            </div>
        </form>
    </div>    
<?php }  ?>


<?php require $this->context->template('layouts/base/backtotop'); ?>   

 
    
<?php require $this->context->template('layouts/base/resource'); ?>   


<script>
$(function(){
    //详情页-图片轮播
    var detailScroll = new Swiper('.page_scrollPIc', {
        pagination: '.page_scrollPag',
        paginationClickable: true,
        autoplay : 4000
    })

    //详情页-商品介绍
    var detailInstroduce = new Swiper('#detail_container',{
        // onInit: function(swiper){
        //     swiper.container[0].style.height=swiper.slides[swiper.activeIndex].offsetHeight+'px';
        // },
        onSlideChangeStart: function(swiper){
            $(".goods_tab .active").removeClass('active');
            $(".goods_tab a").eq(swiper.activeIndex).addClass('active');
            $('#detail_container').find('.swiper-slide').height(0);
            swiper.slides[swiper.activeIndex].style.height='auto';
        }
    })
    $(".detail_tab a").on('touchstart mousedown',function(e){
        e.preventDefault();
        $(".detail_tab .active").removeClass('active');
        $(this).addClass('active');
        detailInstroduce.slideTo( $(this).index() );
    });

    //商品详情页滚动跟随


})
</script>
<script type="text/javascript" src="/resource/js/layer.m/layer.m.js"></script>





</body>
</html>