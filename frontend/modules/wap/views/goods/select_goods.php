<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

?>
<div class="admin-role-model-index">

<!--    搜索-->
    <div class="row">
        <div class="alert alert-info alert-other">
            <?php $form =  ActiveForm::begin([
                'encodeErrorSummary' => false,
                'enableClientScript' => false,
                'fieldConfig' => ['errorOptions' => []],
                'method' => 'get',
                'action' => 'index',//这时可能会有变化
                'options' => ['class' => 'form-inline'],
            ]); ?>
            
<!--            这里列出来的与表单中的是完全一样的展现           -->
            <!--时间插件如何来展现呢-->
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'cid')->dropDownList(common\models\GoodsClassModel::getMenuKeyValue(),['prompt' => '全部'])->label("分类") ?>
            
            
            <div class="form-group">
                <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        
 <div>
    <button type="button" id="is_ok" class="btn btn-info">确认</button>
</div>       
        
    </div>    

 
    
<!--    列表-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  //固定不变
        'summary' => '',   //固定不变
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'], //全选
            'id',//直接展示
            [
                 'attribute' => 'name',
                 'format' => 'raw',    
                 'value' => function ($data) {
                    $url = \yii::$app->params['mine_url']['frontend_url'] . "/wap/goods/index?gid={$data->id}";
                    $html = "<a target='_blank' href='{$url}' class='' >{$data['name']}</a>";
                    return $html;
                 },
             ],                
            [
                 'attribute' => 'img',
                 'format' => 'html',    
                 'value' => function ($data) {
                     return Html::img($data['img'],
                         ['width' => '70px']);
                 },
             ],             
        ],
    ]); ?>


</div>



<script>
    //给父页面传值
    $('#is_ok').on('click', function(){
        //选择了哪些东西拿到具体的数据来.
        var obj = $("[name='selection[]']:checked");
        var select_ids = [];
        var length = obj.length;
        if(!length){
            layer.open({
                content:"请选择相关商品"
            });
        }
        for(var i = 0;i<length;i++){
            select_ids[i] = obj.eq(i).val();
        }
        
        
    });
   
</script>