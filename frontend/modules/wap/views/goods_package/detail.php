<?php
$datas['num'] = 99999;
$goodInfo = $datas;
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
    
    <style>
        .btn{
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;    
        }
    </style>    
    
</head>
<body>
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="page_homeBtn page_headerBtn" href="<?= $this->context->to(['index/index']) ?>"><i class="iconfont">&#xe615;</i></a>
        <div class="page_title"><h1>内部商城详情</h1></div>
    </header>
    <section class="page_scroll">
        <div class="swiper-container page_scrollPIc">
            <div class="swiper-wrapper">
                <?php  $imgs = json_decode($datas['img_list'], true) ?>
                <?php foreach ($imgs as $key => $image): ?>
                <div class="swiper-slide"><img src="<?= common\models\GoodsModel::getShopImg($image, 640) ?>" alt=""></div>
                <?php endforeach; ?>                
            </div>
            <div class="swiper-pagination page_scrollPag"></div>
        </div>
    </section>
    <section class="page_detail">
        <h1><?= $goodInfo['name'] ?></h1>

        

    </section>

    <section class="sku_box detail_guige mine_box">
        
        <?php  //$yes_click_true = ($goodInfo['goods_state'] == 1 && $goodInfo['goods_verify'] == 1);?>
        <!--规格展示-->
        <?php $todo['spec_value']=[]; foreach($todo['spec_value'] as $spec => $list){ ?>
            <?php 
                $s = array_keys($list);
                $k = $s[0];
                if(empty($datas['data_images'][$k])){
                    $hasimg = false;
                }else{
                    $hasimg = true;
                }
            ?>
            <dl class="spec_num">
                <dt><?php echo $goodInfo['spec_name'][$spec] ?>：</dt>
                <dd class="spec <?php  if($hasimg){ echo 'ify_yes'; }else{ echo 'ify_no'; }  ?>" >
                    <?php $num = 0; foreach($list as $spec_id => $value){ $num++; ?>
                        <!--//选中的规格$datas['data_detail']['spec'][$spec_id]-->
                        <span class="<?php if($yes_click_true){  echo 'yes_click'; }  ?> <?php if(!empty($datas['data_detail']['spec'][$spec_id])){ echo 'active'; } ?>" data-param="<?php echo $spec_id;  ?>" ><?php echo $value;  ?></span>
                    <?php } ?>                            
                </dd>                        
            </dl>
        <?php } ?>
        
        <!--库存展示-->
        <?php  $shoplist = json_decode($datas['shop_list'], true);  ?>
<!--        所有的分类-->
        <?php  $goods_class = \common\models\GoodsClassModel::getMenuKeyValue() ?>
        <select id="category_goods" style="height:34px;color:#5bc0de">
            <?php foreach($goods_class as $k => $v){  ?>
                <option value="<?=  $k ?>"><?=  $v ?></option>
            <?php }  ?>
        </select>
        

        <button type="button" class="add_element_mine_attributes btn btn-info">增加</button>
        
        <div id="goods_detail">
        <?php  foreach($shoplist as $key => $value){  ?>
            <?= $this->render('/layouts/base/shop_select',['goods' => $value]) ?>
        <?php }  ?>     
        <div class="detail_pirce all_money">
           <span>商品总计：<i>¥</i><em id="goods_price"><?= $datas['price'] ?></em></span>
<!--           <small>¥<em id="goods_marketprice">5</em></small>-->
            <div class="detail_num">已售<em><?= $datas['sale_num'] ?></em>件</div>
        </div>        
           
       
            
        <?php  $fujia = common\models\GoodsModel::baseGetDatas(['pagesize' => 9999,'type' => 2,'status' => 1,'id' => explode(',', $goodInfo['shop_fuwu'])])  ?>   
        <?php $shop_fuwu = explode(',', $goodInfo['shop_fuwu']); ?>
        <?php  foreach($fujia as $key => $value){  ?>
            <dl data="<?= $value['id'] ?>" money="<?= $value['price'] ?>" class="fujia">
                <dt><img src="<?= $value['img'] ?>"></dt>
                <dd>
                    <div class="goods_detail_name item_stock">
                        <font><?= $value['name'] ?></font> &nbsp;&nbsp; <font style="color:red">¥<?= $value['price'] ?></font> 
                        
                        <input type="checkbox" <?php  if(in_array($value['id'], $shop_fuwu)){ echo "checked"; }  ?>  data="<?= $value['id'] ?>" value="<?= $value['price'] ?>" class="fujia_num" name="fujia_num">
                    
                    </div>
                </dd>                
            </dl>             
        <?php }  ?>            
            

        <div class="detail_pirce">
           <span>总计：<i>¥</i><em id="goods_price_total"><?= $datas['price'] ?></em></span>

        </div>                        
            
        </div>
       
         
        
    
        
        <dl>
            <dt>承诺：</dt>
            <dd>
                <ul class="pz_box">
                    <li class="pz_1"><i class="pz_icon"></i>正品保证</li>
                    <li class="pz_2"><i class="pz_icon"></i>闪电发货</li>
                    <li class="pz_3"><i class="pz_icon"></i>售后保障</li>
                </ul>
            </dd>
        </dl>
    </section>

    

    <section class="detail_wrapper">
        <div class="detail_tab">
            <a class="active" href="javascript:void(0);">商品介绍</a>
            <a href="javascript:void(0);">买家秀</a>
            <a id="guest_goods" href="javascript:void(0);">猜你喜欢</a>
        </div>
        <div class="detail_container" id="detail_container">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-no-swiping">
                    <div class="instroduce_wrapper">
                        <h3>商品规格</h3>
                        <?php $mine_attributes = json_decode($datas['mine_attributes'], true);if(!$mine_attributes){ $mine_attributes = []; }  ?>
                        <div class="instroduce_guige">
                            <?php foreach ($mine_attributes as $key => $val){  ?>
                                <dl><dt><?= $val['key'] ?></dt><dd><?= $val['value'] ?></dd></dl>                            
                            <?php }  ?>
                        </div>                        

                        <h3>图片详情</h3>
                        <div class="instroduce_img">
                            <?= $datas['details'];  ?>
                        </div>
                    </div>
                </div>
                
                
                <div class="swiper-slide swiper-no-swiping">
                    <div  class="evaluation_wrapper">
                        <ul id="comments"></ul>
                    </div>
                </div>
                <script>
                    //评价
                    
                    var commentUrl = '<?= $this->context->to(['goods_package/comments', 'gid' => $goodInfo['id']]); ?>';
                    $("#comments").load(commentUrl, function() {

                    });                    
                </script>

                <div class="swiper-slide swiper-no-swiping">
                    
                    <div class="goods_container clear">
                        <ul id="goods_list" class="clear"></ul>
                        <?php require $this->context->template('layouts/base/msg_more'); ?>
                        <script>
                            $(function(){
                                if(!$("#goods_list").html()){
                                    GOODS_DATA_CONDITION.page = 0;
                                    jiaziaMore('<?= $this->context->to(['index/ajax_recommand'])  ?>');
                                }
                                $(".msg_more").click(function(){
                                    jiaziaMore('<?= $this->context->to(['index/ajax_recommand'])  ?>');
                                })
                            })
                        </script>
                        
                    </div>   
                    
                </div>
            </div>
        </div>
    </section>
</div>

<div class="buy_wrap">
    <form>
        <a class="detail_kefu cell" href="mqqwpa://im/chat?chat_type=wpa&uin=385612297&version=1&src_type=web&web_src=oicqzone.com" target="_blank">客服<i class="iconfont">&#xe626;</i></a>
<!--        <a id="collect" class="collect_act cell active" href="javascript:;">收藏<i class="iconfont">&#xe619;</i></a>-->
        <a class="cart_act cell" href="<?= $this->context->to(['cart/index']) ?>">购物车<i class="iconfont">&#xe604;</i><span class="cart_num"><?= $cart_goods_num;  ?></span></a>
        <div class="buy_action">

            <a href="javascript:;" class="btn_buy  <?php if ($datas['num']) echo 'buynow_submit_list'; ?> <?php echo $datas['num'] < 1 || $datas['status'] == 0 ? 'buy_no' : ''; ?>"   title="立即购买">立即购买</a>
            <a href="javascript:;" class="btn_add_car <?php echo $datas['num'] < 1 || $datas['status'] == 0 ? 'buy_no' : 'addcart_submit_list'; ?>"  title="加入购物车">加入购物车</a>
           
            
        </div>
    </form>
</div>

<?php require $this->context->template('layouts/base/backtotop'); ?>   

    <form id="buynow_form" method="get" action="<?= $this->context->to(['buy/buy_step1'])  ?>">
        <input id="shop_list" name="shop_list" type="hidden"/>
        <input id="goods_id" name="goods_id" value="<?= $_GET['gid'] ?>" type="hidden"/>
        <input id="ifcart" name="ifcart" value="0" type="hidden"/>
        <input id="type" name="type" value="2" type="hidden"/>

        
        <input id="fujia" name="fujia" value="" type="hidden"/>
    </form>    
    
<?php require $this->context->template('layouts/base/resource'); ?>   

<script src="/resource/js/goods.js"></script>    
<script src="/resource/js/goods_package.js"></script>   
<script>
    //系统数据
    var DATAS = <?php  echo json_encode($datas); ?>   
    var IS_LOGIN = '<?php echo \yii::$app->controller->user->isLogin();  ?>';
    var STORE_SELF = '0';
    var GOODS_DEFAULT_PIC = '<div class="mod_fly_cart" id="flyCart"><img src="/images/default.jpg"></div>';
    //收藏地址
    var COLLECT_URL = ''; 

    //运费
    var CALC_URL = '0';
    
    var IMG_URL = '';
    
    var GOODS_ID = '<?php echo $_GET['gid']; ?>';
    //始发地商品ID
    var ZH_SENT = '';
</script>
<script>
$(function(){
    //详情页-图片轮播
    var detailScroll = new Swiper('.page_scrollPIc', {
        pagination: '.page_scrollPag',
        paginationClickable: true,
        autoplay : 4000
    })

    //详情页-商品介绍
    var detailInstroduce = new Swiper('#detail_container',{
        // onInit: function(swiper){
        //     swiper.container[0].style.height=swiper.slides[swiper.activeIndex].offsetHeight+'px';
        // },
        onSlideChangeStart: function(swiper){
            $(".goods_tab .active").removeClass('active');
            $(".goods_tab a").eq(swiper.activeIndex).addClass('active');
            $('#detail_container').find('.swiper-slide').height(0);
            swiper.slides[swiper.activeIndex].style.height='auto';
        }
    })
    $(".detail_tab a").on('touchstart mousedown',function(e){
        e.preventDefault();
        $(".detail_tab .active").removeClass('active');
        $(this).addClass('active');
        detailInstroduce.slideTo( $(this).index() );
    });

    //商品详情页滚动跟随
    var objTab= $('.detail_tab').offset();
    if(objTab != 'undefined'){
        var objTop = objTab.top;
        $(window).scroll(function () {
            if ($(this).scrollTop() > objTop) {
                $('.detail_tab').addClass('fix_filter');
            } else {
                $('.detail_tab').removeClass('fix_filter');
            }
        });
        if ($(window).scrollTop() > objTop) {
            $('.detail_tab').addClass('fix_filter');
        }else{
            $('.detail_tab').removeClass('fix_filter');
        }
    }

})
</script>
<script type="text/javascript" src="/resource/js/layer.m/layer.m.js"></script>





</body>
</html>