<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>
<div class="zh_container">
    <header class="header_wrapper">
        <div class="header_bg"></div>
        <a class="up_level header_btn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="class_search page_headerBtn" href="javascript:void(0);">
            <i class="iconfont">&#xe606;</i>
        </a>
        
        <?php if(!empty($class_name)){  ?>
            <div class="page_title">
                <h2>
                    <?php error_reporting(0);  $datas = \common\models\GoodsPackageClassModel::getMenuKeyValue(false);  ?>
                    <?php echo \common\components\Tool::getSelectData($datas, "package_class", ['class' => "form-control",'id' => "select_class"], true, $_GET['cid'],"内部商城")  ?>
                </h2>
            </div>        
        <?php } ?>
        
    </header>
    


<!--    排序相关功能-->
<?php 
$url =  $this->context->to(['goods_package/list']); 
require $this->context->template('layouts/base/order');
?> 

    <section class="goods_container clear" id="goods_container">
        <ul id="goods_list" class="clear">
            <?php require $this->context->template('layouts/base/goods_package') ?>
        </ul>
    </section>

    <?php require $this->context->template('layouts/base/msg_more'); ?>

    <?php require_once $this->context->template('layouts/base/footer'); ?>
</div>


<?php require $this->context->template('layouts/base/search4'); ?>


<?php require $this->context->template('layouts/base/backtotop'); ?>    

<?php require $this->context->template('layouts/base/resource'); ?>   

    
    
    <script>
        //下拉加载更多
        $(function(){
            $(".msg_more").click(function(){
                jiaziaMore('<?= $this->context->to(['goods_package/list']) ?>');
            })
            
            $(window).on('scroll', function(){
                var he = $(document).scrollTop() / $(document).height();
                if (he > 0.20) {
                    jiaziaMore('<?= $this->context->to(['goods_package/list']) ?>');
                }
            });                
        })
        
        
        $(function(){
            $("#select_class").change(function(){
                var class_id = $(this).val();
                var url = "/wap/goods_package/list?cid=" + class_id;
                window.location.href=url;
            })
        })
    </script>    
    
</body>
</html>