<div class="tab_cover">
    <span></span>
    <p>点击此处返回<i class="iconfont hide_icon">&#xe60a;</i></p>
</div>
<div class="tab_container">
    <div class="tab_header">
        <h1>全部分类</h1>
    </div>
    <div class="tab_content">
        <ul>
            <?php foreach($datas as $data){  ?>
                <li>
                    <a href="<?= $this->context->to(['goods','cid' => $data['id']])  ?>">
                        <?php  
                            if($data['img']){
                                $img = $data['img'];
                            }else{
                                $img = "/images/default.jpg";
                            }
                                
                        ?>
                       <img src="<?= $img  ?>">
                        <span class="tab_name"><?= $data['title']; ?></span>
                    </a>
                </li>
            <?php }  ?>
        </ul>
    </div>
</div>
<script>
    $(document).on('click','.tab_cover',function(){
        $('#menu_tab').removeClass('show_menu');
        $(this).fadeOut();
    })      
</script>