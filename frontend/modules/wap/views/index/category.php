<style>
/*----------------- category.goods.html -------------------*/
.radius {margin: 10px 4px;padding: 10px;background: #fff;border: 1px solid #dedede;border-radius: 5px;-webkit-border-radius: 5px;-moz-border-radius: 5px;}
.goods_image_wrap{background: #efefef;position: relative;width: 100%;overflow: inherit;}
.goods_image_wrap .left{width:30%;}

.goods_image{width:100%;padding:0.5em;overflow-x: scroll;line-height: 2em;border-bottom: 1px solid #e0e0e0;border-right: 1px solid #e0e0e0;background-color:#efefef }
.goods_image::-webkit-scrollbar{
        display: none
    }
.goods_image.first{background:#fff;}
.goods_image.first a{color:red}
.goods_image h3{text-align: center;font-weight: normal;font-size:0.5em}
.goods_image h3 a {color:#000}
.goods_image .child{position: absolute;width:70%;right: -1.1em;top:0;background: #fff;display: none}
.goods_image.active{background:red;border-right: 1px solid red;}
.goods_image.active a{color:white}
.goods_image.active .child{display: block;height: 100%}
.goods_image .child .child_wrap{}
.goods_image .child .child_wrap a{display: inline-table;width: 30%;padding: 10px 0;text-align: center;color: #546280;font-size:12px;}
.goods_image .child .child_wrap a img{width:60px;height: 60px}   
.zh_container{
    overflow: inherit;
}
</style>
<div class="goods_image_wrap clearfix">
<div class="left">   
    
    
    
    <?php $nn = 0; foreach($datas as $key => $value){ $nn++;  ?>
    
    
        
        <div  class="goods_image first <?php if($nn == 1){ echo 'active'; }  ?>">
            <h3><a href="javascript:void(0)"><?php echo $value['title']  ?></a></h3>
            <div class="child">
                <div class="child_wrap">
                    <?php $num = 0; foreach($value['data'] as $data){ $num++; ?>
                        <a href="/wap/index/goods?cid=<?= $data['id'] ?>">
                            <p><img src="<?= $data['img'] ?>"></p>
                            <p><?= $data['title'] ?></p>
                        </a>                     
                    <?php }  ?>
                </div>
            </div>
        </div>      

    
    
    <?php  }  ?>
    
        
<!--        <div class="goods_image first ">
            <h3><a href="javascript:void(0)">景观分类</a></h3>
            <div class="child">
                <div class="child_wrap">

                </div>
            </div>
        </div>       

    
    
        <div class="goods_image first ">
            <h3><a href="javascript:void(0)">【花园梦工场】</a></h3>
            <div class="child">
                <div class="child_wrap">
                    即将上线，敬请期待．．．．．．
                </div>
            </div>
        </div>      
    
        <div class="goods_image first ">
            <h3><a href="javascript:void(0)">【商城活动】</a></h3>
            <div class="child">
                <div class="child_wrap">
                    占无活动
                </div>
            </div>-->
        </div>      
 
</div>

</div>

<script>
    $(".goods_image").click(function(){
        $(".goods_image").removeClass("active");
        $(this).addClass("active");
        
        $(".item").hide();
        if($(this).parent().hasClass("item")){
            $(this).parent().show();
        }else{
            $(this).next(".item").show();
        }
    })
</script>