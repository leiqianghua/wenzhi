<style>
/*----------------- category.goods.html -------------------*/
.radius {margin: 10px 4px;padding: 10px;background: #fff;border: 1px solid #dedede;border-radius: 5px;-webkit-border-radius: 5px;-moz-border-radius: 5px;}
.goods_image_wrap{background: #efefef;position: relative;width: 100%;overflow: hidden;}
.goods_image_wrap .left{width:30%;}

.goods_image{width:100%;padding:0.5em;overflow-x: scroll;line-height: 2em;border-bottom: 1px solid #e0e0e0;border-right: 1px solid #e0e0e0;background-color:#efefef }
.goods_image::-webkit-scrollbar{
        display: none
    }
.goods_image.first{background:#fff;}
.goods_image.first a{color:red}
.goods_image h3{text-align: center;font-weight: normal;font-size:0.5em}
.goods_image h3 a {color:#000}
.goods_image .child{position: absolute;width:70%;right: -1.1em;top:0;background: #fff;display: none}
.goods_image.active{background:red;border-right: 1px solid red;}
.goods_image.active a{color:white}
.goods_image.active .child{display: block;height: 100%}
.goods_image .child .child_wrap{}
.goods_image .child .child_wrap a{display: inline-table;width: 30%;padding: 10px 0;text-align: center;color: #546280;font-size:12px;}
.goods_image .child .child_wrap a img{width:60px;height: 60px}    
</style>
<div class="goods_image_wrap clearfix">
<div class="left">
        
        <div class="goods_image first active">
            <h3><a href="javascript:void(0)">全部分类</a></h3>
            <div class="child">
                <div class="child_wrap">
                    <?php $num = 0; foreach($datas as $data){ $num++; ?>
                        <a href="/wap/index/project?cid=<?= $data['id'] ?>">
                            <p><img src="<?= $data['img'] ?>"></p>
                            <p><?= $data['title'] ?></p>
                        </a>                     
                    <?php }  ?>
                </div>
            </div>
        </div>   
        
        
    
        <?php $num = 0; foreach($datas as $data){ $num++; ?>
    
            <div class="goods_image">
                <h3><a href="javascript:void(0)"><?= $data['title'] ?></a></h3>
                <div class="child">
                    <div class="child_wrap">
                        <?php $num_detail = 0; foreach($data['data'] as $data_detail){ $num_detail++; ?>
                            <a href="/wap/index/project?cid=<?= $data_detail['id'] ?>">
                                <p><img src="<?= $data_detail['img'] ?>"></p>
                                <p><?= $data_detail['title'] ?></p>
                            </a>                     
                        <?php }  ?>
                    </div>
                </div>
            </div>             
      
        <?php }  ?>    
</div>

</div>

<script>
    $(".goods_image").click(function(){
        $(".goods_image").removeClass("active");
        $(this).addClass("active");
    })
</script>