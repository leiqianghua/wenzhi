<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head_1'); ?>
</head>
<body>
<div class="zh_container">
    <header class="header_wrapper">
        <div class="header_bg"></div>
        <a class="up_level header_btn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="class_search page_headerBtn" href="javascript:void(0);">
            <i class="iconfont">&#xe606;</i>
        </a>
        <div class="page_title">
            <h2><?= $class_name ?></h2>
        </div>
    </header>
    


<!--    排序相关功能-->
<?php 
$url =  $this->context->to(['index/cha_goods']); 
require $this->context->template('layouts/base/order');
?> 

    <section class="goods_container clear" id="goods_container">
        <ul id="goods_list" class="clear">
            <?php require $this->context->template('layouts/base/cha_goods') ?>
        </ul>
    </section>

    <?php require $this->context->template('layouts/base/msg_more'); ?>

    <?php require_once $this->context->template('layouts/base/footer'); ?>
</div>


<?php //require $this->context->template('layouts/base/search3'); ?>


<?php require $this->context->template('layouts/base/backtotop'); ?>    

<?php require $this->context->template('layouts/base/resource'); ?>   

    
    
    <script>
        //下拉加载更多
        $(function(){
            $(".msg_more").click(function(){
                jiaziaMore('<?= $this->context->to(['index/cha_goods']) ?>');
            })
            
            $(window).on('scroll', function(){
                var he = $(document).scrollTop() / $(document).height();
                if (he > 0.20) {
                    jiaziaMore('<?= $this->context->to(['index/cha_goods']) ?>');
                }
            });                
        })
    </script>    
    
</body>
</html>