
<?=  $this->render('/layouts/base/banner_wrapper',['flash' => $flash]);  ?>
<?=  $this->render('/layouts/base/index_catg');  ?>
<?=  $this->render('/layouts/base/index_news');  ?>

<section class="goods_wrapper">
    <div class="goods_tab">
        <?php
            if($this->context->user->uid && $this->context->user->label < 4){
                $is_taozhuang = 1;
            }else{
                $is_taozhuang = 0;
            }
        ?>
        
        
        <a <?php  if($is_taozhuang){ echo 'style="width:20%"'; } ?> data = "1" class="active" href="javascript:void(0);"><i class="iconfont tejia">&#xe60f;</i>综合</a>
        <a  <?php  if($is_taozhuang){ echo 'style="width:20%"'; } ?> data = "2" href="javascript:void(0);"><i class="iconfont jingxuan">&#xe60d;</i>最新</a>
        <a  <?php  if($is_taozhuang){ echo 'style="width:20%"'; } ?> data = "4" href="javascript:void(0);"><i class="iconfont taozhuang">&#xe60d;</i>工程套装</a>
        <a  <?php  if($is_taozhuang){ echo 'style="width:20%"'; } ?> class="sort" data-sort="1" order="3" data = "3" href="javascript:void(0);"><i class="iconfont xinpin">&#xe60e;</i>价格<i class="iconfont arrow arrow_hv"></i></a>
        <?php if($is_taozhuang){  ?>
            <a  <?php  if($is_taozhuang){ echo 'style="width:20%"'; } ?> data = "10" href="javascript:void(0);">内部商城</a>
        <?php } ?>
    </div>
    <div class="goods_container" id="goods_container">
        <div class="swiper-wrapper">

         
            
            
            
            <div class="swiper-slide swiper-no-swiping clear">
                
                <?php  if(!empty($goods_city)){  ?>
                <h3 style="margin-top:10px;padding-left:2%;padding-right:2%;position: relative">地方商品 <a style="position:absolute;right:2%;" href="/wap/index/difan_goods?city_id=<?= $city_id ?>">更多</a></h3>
                    <ul>
                        <?= $this->render('/layouts/base/goods',['goods' => $goods_city]) ?>
                    </ul>
                <?php }  ?>
                                
                <ul  data = "1" class="ajaxDate">
                    <?= $this->render('/layouts/base/goods',['goods' => $goods]) ?>
                </ul> 
            </div>
            <div  class="swiper-slide swiper-no-swiping clear">
                <ul data = "2" class="ajaxDate"></ul> 
            </div>
            <div  class="swiper-slide swiper-no-swiping clear">
                <ul data = "4" class="ajaxDate"></ul> 
            </div>
            <div class="swiper-slide swiper-no-swiping clear">
                <ul data = "3" class="ajaxDate"></ul> 
            </div>
            <div class="swiper-slide swiper-no-swiping clear">
                <ul data = "10" class="ajaxDate"></ul> 
            </div>
        </div>
    </div>
</section>

<?= $this->render('/layouts/base/msg_more'); ?>

<script>
    
    
 
    
    
    
    
    
    
//首页产品
$(function(){
    
    //商品列表 && 搜索结果页 筛选
//    $(document).on('click',".goods_tab .sort.active",function(){
//        console.log(2);
//        var sortVal = $(this).attr('data-sort');
//        if(sortVal == 1){
//            $(this).attr('order', 3);
//            $(this).find('i.arrow').addClass('arrow_hv');
//            $(this).attr('data-sort','0');
//        }else{
//            $(this).attr('order', 4);
//            $(this).find('i').removeClass('arrow_hv');
//            $(this).attr('data-sort','1');
//        }
//        
//        PAGE_COMMON[3] = 0;
//        PAGE_COMMON[4] = 0;
//        GOODS_NO[3] = true;
//        GOODS_NO[4] = true;
//        
//        var obj = $(".ajaxDate[data='3']");
//        obj.html('');
//        ajaxGetGoods();
//        
//    })        
    
    
    
    
    var PAGE_COMMON = [];
    PAGE_COMMON[1] = 1;//目前今日特价在哪一页
    PAGE_COMMON[2] = 0;//目前本周精选在哪一页
    PAGE_COMMON[3] = 0;//目前新品上市在哪一页
    PAGE_COMMON[4] = 0;//目前新品上市在哪一页
    PAGE_COMMON[10] = 0;//目前新品上市在哪一页
    var TYPE_COMMON = 1;//目前是在哪个类型下面。。。。默认是在1
    
    var GOODS_NO = [];
    GOODS_NO[1] = true;//是否还有商品
    GOODS_NO[2] = true;//是否还有商品
    GOODS_NO[3] = true;//是否还有商品
    GOODS_NO[4] = true;//是否还有商品
    GOODS_NO[10] = true;//是否还有商品

    var tabsSwiper = new Swiper('#goods_container',{
        speed:500,
        onSlideChangeStart: function(swiper){
            $(".goods_tab .active").removeClass('active');
            $(".goods_tab a").eq(tabsSwiper.activeIndex).addClass('active');
            $('#goods_container').find('.swiper-slide').height(0);
            swiper.slides[swiper.activeIndex].style.height='auto';
        }
    })
    
    //banner
    var swiper1 = new Swiper('.index_banner', {
        pagination: '.banner_pagination',
        paginationClickable: true,
        autoplay : 4000
    })
    
    $(".goods_tab a").on('touchstart mousedown',function(e){
        e.preventDefault();
        var obj_this = $(this);
        $(".goods_tab .active").removeClass('active');
        obj_this.addClass('active');            
        TYPE_COMMON = obj_this.attr('data');//哪一个类型的
        
        if(TYPE_COMMON == 3){
            var sortVal = $(this).attr('data-sort');
            if(sortVal == 1){
                $(this).attr('order', 3);
                $(this).find('i.arrow').addClass('arrow_hv');
                $(this).attr('data-sort','0');
            }else{
                $(this).attr('order', 4);
                $(this).find('i').removeClass('arrow_hv');
                $(this).attr('data-sort','1');
            }

            PAGE_COMMON[3] = 0;
            PAGE_COMMON[4] = 0;
            GOODS_NO[3] = true;
            GOODS_NO[4] = true;

            var obj = $(".ajaxDate[data='3']");
            obj.html('');
            ajaxGetGoods();            
        }else{
            var obj = $(".ajaxDate[data='"+TYPE_COMMON+"']");
            if(!obj.html()){
                ajaxGetGoods();         
            }            
        }

        tabsSwiper.slideTo( obj_this.index() );
    })

    //点击加载更多
    $(".msg_more").click(function(e){
        zhixin();
    })

    //下拉加载更多
    loadGoods(zhixin);
    function zhixin(){
        IS_OK = false;
        if(TYPE_COMMON == 3){
            var order = $('.goods_tab [data="3"]').attr('order');
            var type_c = order;
        }else{
            var type_c = TYPE_COMMON;
        }
        if(GOODS_NO[type_c]){//还存在商品
            ajaxGetGoods();       
        }else{
            $(".msg_more_txt").html('已经没有了');
            return false;//已经不存在商品了
        } 
    }
    //ajax加载商品
    function ajaxGetGoods(){
        
        if(TYPE_COMMON == 3){
            
            var order = $('.goods_tab [data="3"]').attr('order');
            var page = PAGE_COMMON[order] + 1;
            var obj = {type:TYPE_COMMON,page:page,order:order};
        }else{
            var page = PAGE_COMMON[TYPE_COMMON] + 1;
            var obj = {type:TYPE_COMMON,page:page};
        }
        $.post('<?= $this->context->to(['index/ajaxindexgoods']) ?>',obj,function(data){
            if(data.status){
                $(".ajaxDate[data='"+TYPE_COMMON+"']").append(data.content);
                if(TYPE_COMMON == 3){
                    var order = $('.goods_tab [data="3"]').attr('order');
                    PAGE_COMMON[order]++;
                }else{
                    PAGE_COMMON[TYPE_COMMON]++;
                }                
                
            }else{//认为没有商品了
                if(TYPE_COMMON == 3){
                    var order = $('.goods_tab [data="3"]').attr('order');
                    GOODS_NO[order] = false;
                }else{
                    GOODS_NO[TYPE_COMMON] = false;
                }                  
            }
            IS_OK = true;
        },'json');                 
    }
})
</script>