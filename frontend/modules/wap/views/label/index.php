<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>
<div class="zh_container">

    <!--    排序相关功能-->
    <?php 
//    $url =  $this->context->to(['search/index']); 
//    require $this->context->template('layouts/base/order');
    ?>    

    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class="page_homeBtn page_headerBtn" href="<?= $this->context->to(['index/index']) ?>"><i class="iconfont">&#xe615;</i></a>
        <div class="page_title"><h1><?= $search_data ?></h1></div>
    </header>    
    

    <section class="goods_container clear" id="goods_container">
        <ul id="goods_list" class="clear">
            <?php require $this->context->template('layouts/base/goods') ?>
        </ul> 
    </section>
    
    <?php require $this->context->template('layouts/base/msg_more'); ?>
    <?php require $this->context->template('layouts/base/footer'); ?>
</div>

    
<?php require $this->context->template('layouts/base/backtotop'); ?>
<?php require $this->context->template('layouts/base/resource'); ?>       

</body>

<script>
    $(function(){
        function searchLoadMore(){
            jiaziaMore('<?= $this->context->to(['label/index']) ?>');
        }
        //点击加载更多
        $(".msg_more").click(function(){
            searchLoadMore()
        })
        //下拉加载更多数据
        loadGoods(searchLoadMore);
    })
</script>  
</html>