<?php
use frontend\modules\wap\assets\AppAsset;
use frontend\modules\wap\assets\AddressAsset;
AppAsset::register($this);
AddressAsset::register($this);
\frontend\modules\wap\assets\FormAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta content="古笆兔商城" name="keywords">
<meta content="古笆兔商城" name="description">
<script src="/resource/js/jquery.js"></script>
<link href="/resource/bootstrap-3.3.5/css/bootstrap.min.css" rel="stylesheet">
<script src="/resource/bootstrap-3.3.5/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="/resource/js/zooming.js"></script>  
<title>古笆兔</title>
<?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <div class="page_title"><h1><?php echo empty($this->context->title) ? '古笆兔商城' : $this->context->title  ?></h1></div>
        <a class="page_homeBtn page_headerBtn" href="<?= $this->context->to(['index/index']) ?>"><i class="iconfont">&#xe615;</i></a>
    </header>

    <?= $content ?>
</div>
    
    
<!-- 地址区域列表 -->
<div id="areaWrapper">
    <div class="area_cover"><span></span></div>
    <div class="area_box">
        <header class="page_header">
            <div class="page_headerBg"></div>
            <a class="area_backBtn page_headerBtn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
            <div class="page_title"><h1>配送至</h1></div>
        </header>
        <section id="areaList" class="area_list"></section>
    </div>
</div>    
    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>