 <?php foreach($articles as $key => $article){  ?>
<li>
    <a class="schoolBox" href="<?= Unit::U('school/detail',['id' => $article['article_id']])  ?>">
        <?php if(!$article['img1']){ $article['img1'] = M_RESOURCE_URL . '/images/no_pic.jpg'; }  ?>
        <img class="school_img" src="<?=  $article['img1']  ?>">
        <h2><?= $article['article_title'] ?></h2>
        <p>
         <?php  echo mb_substr(str_replace(array("\r\n",'&nbsp;'), '', strip_tags($article['article_content'])), 0, 10,  'utf-8') ?>...   
        </p>
        <span class="school_info">
            <span class="school_time"><?= date('Y-m-d', $article['article_time']) ?></span>
            <span class="school_view"><i class="iconfont view">&#xe625;</i> <?= $article['click'] ?></span>
        </span>
    </a>
</li>
<?php }  ?>