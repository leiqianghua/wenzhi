 <?php foreach($goods as $key => $good){  ?>
    <li>
        <a class="goods_item" href="<?= $this->context->to(['goods_package/detail','gid' => $good['id']])  ?>">
            <span class="item_img">
                <img  class="item_pic" src="<?= \common\models\GoodsModel::getShopImg($good['img'], 240)  ?>">
            </span>
            <span class="newgoods item_info">
                <span class="item_name"><?= $good['name']  ?></span>
                <?php  $price_begin = $good['price'];$price_end = $good['price'] + $good['price']/10;  ?>
                <span class="item_price"><i>¥</i><?= number_format($price_begin,2) . "-" . number_format($price_end,2) ?></span>
<!--                <span class="item_num">已售<em><?= $good['sale_num']  ?></em>件</span>-->
            </span>
        </a>
    </li>    
<?php }  ?>
