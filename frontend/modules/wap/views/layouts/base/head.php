<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta name="x5-fullscreen" content="true">
<meta name="x5-page-mode" content="app">
<title>古笆兔</title>

<link href="/resource/css/swiper.min.css" rel="stylesheet">
<link href="/resource/css/style.css" rel="stylesheet">

<script type="text/javascript" src="/resource/js/jquery.js"></script>   
<script type="text/javascript" src="/resource/js/zooming.js"></script>  
