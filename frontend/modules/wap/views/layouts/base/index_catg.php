
<section class="index_catg clear">
    <ul>
        <li>
            <a class="item" href="<?=  $this->context->to(['index/category']) ?>"><div class="img item1"><i class="iconfont">&#xe601;</i></div><div class="text">类目</div></a>
            <a class="item" href="<?= $this->context->to(['member_order/index'])  ?>"><div class="img item2"><i class="iconfont">&#xe605;</i></div><div class="text">全部订单</div></a>
            <a class="item" href="<?= $this->context->to(['cart/index'])  ?>"><div class="img item3"><i class="iconfont">&#xe604;</i></div><div class="text">购物车</div></a>
            <a class="item" href="<?= $this->context->to(['member_order/index','order_state' => 2])  ?>"><div class="img item4"><i class="iconfont">&#xe603;</i></div><div class="text">待收货</div></a>
            
            <?php if(\yii::$app->controller->user->label && \yii::$app->controller->user->label < 4){  ?>
                <a class="item" href="<?= $this->context->to(['else_kaidan/list_package'])  ?>"><div class="img item5"><i class="iconfont">&#xe625;</i></div><div class="text">报价器</div></a>
            <?php  } ?>
            
            
            <a class="item" href="<?= $this->context->to(['member/index'])  ?>"><div class="img item6"><i class="iconfont">&#xe607;</i></div><div class="text">个人中心</div></a>
            <a class="item" href="<?= $this->context->to(['member_address/index'])  ?>"><div class="img item7"><i class="iconfont">&#xe600;</i></div><div class="text">我的地址</div></a>
            <a class="item" href="<?= $this->context->to(['index/goods','is_score' => 1])  ?>"><div class="img item7"><i class="iconfont">&#xe600;</i></div><div class="text">积分商品</div></a>
            <a class="item" href="<?= $this->context->to(['member_case/index'])  ?>"><div class="img item7"><i class="iconfont">&#xe600;</i></div><div class="text">案例库</div></a>
        </li>
    </ul>
</section>