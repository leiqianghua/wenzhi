    <section class="msg_more">
        <span class="l_line"></span>
        <span class="msg_more_txt">下拉加载更多</span>
        <span class="r_line"></span>
    </section>
    <script>
        var HAS_DATA = true;//是否还有数据需要加载
        var GOODS_DATA_CONDITION = {};//加载更多的条件 一般带有order,page,cid等这些条件
        <?php if(!empty($_GET)){   ?>
            GOODS_DATA_CONDITION = <?= json_encode($_GET);  ?>//各个的条件。。。。。
        <?php }  ?>
        
        delete GOODS_DATA_CONDITION.act;
        delete GOODS_DATA_CONDITION.op;
        GOODS_DATA_CONDITION.page = 1;
        var OBJ_GOODS = $("#goods_list");
        
        //加载更多商品
//        obj为需要插入数据的元素
        function jiaziaMore(post_url,obj_goods)
        {
            IS_OK = false;   
            if(!HAS_DATA){
                return false;
            }
            //加载商品
            GOODS_DATA_CONDITION.page++;
            $.post(post_url,GOODS_DATA_CONDITION,function(data){
                if(data.status && data.content){
                    if(obj_goods){
                        $(obj_goods).append(data.content);
                    }else{
                        OBJ_GOODS.append(data.content);
                    }
                    
                }else{
                    GOODS_DATA_CONDITION.page--;
                    HAS_DATA = false;
                };
                IS_OK = true;                 
            },'json');                 
        }
    </script>