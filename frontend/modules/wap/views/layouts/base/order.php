    <section class="page_filter" id="page_filter">
        <a order="0" class="sort active" href="javascript:void(0);">默认</a>
        <a order="1" class="sort" href="javascript:void(0);">时间</a>
        <a order="4" class="sort last" id="priceSort" data-sort="1" href="javascript:void(0);" order="">价格<i class="iconfont arrow">&#xe613;</i></a></a>
    </section>

    <script>
        $(function(){
            //排序相关功能
            $("#page_filter a").click(function(){
                GOODS_DATA_CONDITION.page = 1;//分页默认变为从第一页开始
                GOODS_DATA_CONDITION.order = $(this).attr('order');
                var obj = $(this);

                $.post('<?= $url ?>',GOODS_DATA_CONDITION,function(data){
                    if(data.status && data.content){
                        $("#goods_list").html(data.content);
                        if(GOODS_DATA_CONDITION.order == 3){
                            obj.attr('order',4);
                        }
                        if(GOODS_DATA_CONDITION.order == 4){
                            obj.attr('order',3);
                        }
                    }
                },'json');
            })
        })
    </script>