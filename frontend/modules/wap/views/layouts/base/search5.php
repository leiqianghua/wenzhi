<form id="search_submit" action="<?= $this->context->to(['search_package/index']);  ?>">
<header class="header_wrapper">

<div class="header_bg"></div>
        <a class="up_level header_btn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <a class=" header_btn_i" href="javascript:void(0);"><?= $GLOBALS['city_name'] ?></a>
        <a class="user_center header_btn" href="javascript:void(0);" onclick="$('#search_submit').submit();">搜索</a>
        <a class="back_btn header_btn" href="javascript:void(0);"><i class="iconfont">&#xe613;</i></a>
        <div class="search_wrapper">
            <div class="search_box">
                <input type="text" name="keyword" value="<?= empty($_GET['keyword']) ? '' : $_GET['keyword']; ?>" class="search_input" placeholder="搜索您想要的商品">
                <a class="search_btn" href="javascript:$('#search_submit').submit();"><i class="iconfont">&#xe606;</i></a>
            </div>
        </div>
</header>        
</form>     
<div class="hot_search">
    <h1>热门搜索</h1>
    <div class="hot_words">
        <a href="<?= $this->context->to(['search_package/index','keyword' => '家装']);  ?>">家装</a>
        <a href="<?= $this->context->to(['search_package/index','keyword' => '装饰']);  ?>">装饰</a>
        <a href="<?= $this->context->to(['search_package/index','keyword' => '室内装饰']);  ?>">室内装饰</a>
    </div>
    
    <?php  $datas = \common\models\GoodsLabelModel::getListMenu(2);  ?>
    <?php foreach($datas as $data){  ?>
            <h1><?= $data['title']  ?></h1>
            <div class="label_list hot_words">
                <?php foreach($data['data'] as $k => $v){  ?>      
                    <a href="javascript:void(0)" data="<?= $v['id'] ?>"><?= $v['title']  ?></a>
                <?php }  ?>
            </div>
    <?php }  ?>     
            
    <div class="account_opt">
         <input class="up_btn select_ok"  value="确 认">
     </div>       
</div>  

<script>
    $(function(){
        $(".label_list  a").click(function(e){
            e.preventDefault();
            if($(this).hasClass('select_label')){
                $(this).removeClass('select_label');
            }else{
                $(this).addClass('select_label');
            }
            return false;
        })
        
        $(".select_ok").click(function(){
            var objs = $(".select_label");
            var length = objs.length;
            var label = [];
            for(var i = 0;i< length;i++){
                label[i] = objs.eq(i).attr("data");
            }
            if(label.length == 0){
                alert("请至少选择一个标签");
            }
            var str_label = label.join(',');
            var url = "<?= $this->context->to(["label_package/index"]) ?>/?label_ids="+str_label;
            window.location.href=url;
        })
    })
</script>

<script type="text/javascript">
$(function(){
    //搜索列表
    var searchList = new searchListFun();
    searchList.focus();
    searchList.back();
})
//搜索列表
var searchListFun = function(){
    var _height=$(window).height();
    this.focus=function(){
        $('.search_input').focusin(function(){
            $('.header_bg').addClass('search_bg');
            $(".zh_container").css({"height":_height});
            $('.hot_search,.back_btn').show();
            $('.up_level').hide();
        })
    }
    this.back=function(){
        $('.back_btn').on('click',function(){
            $('.header_bg').removeClass('search_bg');
            $('.zh_container').css({"height":'auto'});
            $('.hot_search,.back_btn').hide();
            $('.up_level').show();
        })
    }
}
</script>