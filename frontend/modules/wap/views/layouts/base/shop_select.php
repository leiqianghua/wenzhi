<?php  
    
    if(!empty($goods['price'])){
        $shop_detail = $goods;
        $goods['num'] = 1;
    }else{
        $shop_detail = \common\models\GoodsModel::findOne($goods['id']); 
    }
    
//    if(!$shop_detail || !$shop_detail['num'] || !$shop_detail['status'] < 0){
//        return;
//    }
    if(!$shop_detail){
        return;
    }
?>
<dl data="<?= $shop_detail['id'] ?>" class="content_list">
    <?php  $gid = \common\models\GoodsModel::getOne($shop_detail['id'], "common_id");   ?>
    <dt><a href="<?= $this->context->to(['goods/index','gid' => $gid])  ?>"><img src="<?= $shop_detail['img'] ?>" /></a></dt>
    <dd>
        <div class="input_num buyNum" id="" data-id="<?= $shop_detail['id']  ?>" data-price="<?= $shop_detail['price']  ?>" data-storage="<?= $shop_detail['num']  ?>">
            <i class="iconfont minus">-</i>
<!--            <input type="text" value="<?= $goods['num'] >= $shop_detail['num'] ? $shop_detail['num'] : $goods['num']  ?>" class="goods_num" name="goods_num">-->
            <input type="text" value="<?= $goods['num']  ?>" class="goods_num" name="goods_num">
            <i class="iconfont add">+</i>                    
        </div>
        <div class="item_stock">剩余<em class="goods_storage"><?= $shop_detail['num']  ?></em>件</div>
    </dd>
    <dd>
        <div class="goods_detail_name item_stock">
            <?= $shop_detail['name']  ?>
        </div>
    </dd>
    <a class="cart_del"  href="javascript:void(0);"><i class="iconfont"></i></a>
</dl>  