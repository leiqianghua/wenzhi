<?php
use frontend\modules\wap\assets\AppAsset;
use frontend\modules\wap\assets\SwiperAsset;
AppAsset::register($this);
SwiperAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="zh-CN">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta name="x5-fullscreen" content="true">
<meta name="x5-page-mode" content="app">
<title>古笆兔</title>
<script src="/resource/js/jquery.js"></script>

<?php $this->head() ?>
    </head>
    <?php $this->beginBody() ?>
    
    <body>
        <div class="zh_container">
            
            <?=  $this->render('/layouts/base/search_index');  ?>

            <?=  $content ?>
            
            
            <?=  $this->render('/layouts/base/footer');  ?>
            
            


        </div>
        <div id="menu_tab"></div>    
        <div id="backtotop" class="backtotop">
    <div class="backTopBg"></div>
    <i class="iconfont top_icon">&#xe611;</i>
</div>    
        
  

    </body>
    <?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>

