<?php
use frontend\modules\wap\assets\AppAsset;
AppAsset::register($this);

//$action_array = [
//    'update',
//    'create',
//    'login',
//    'register',
//    'cash_insert',
//    'callback_qq',
//    'callback_weixin',
//    'apply',
//];
//if(in_array($this->context->action->id, $action_array)){
//    \frontend\modules\wap\assets\FormAsset::register($this);
//}


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<script src="/resource/js/jquery.js"></script>
<meta content="古笆兔商城" name="keywords">
<meta content="古笆兔商城" name="description">




<title>古笆兔</title>
<?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
<div class="zh_container">
    <header class="page_header">
        <div class="page_headerBg"></div>
        <a class="page_backBtn page_headerBtn" href="javascript:history.go(-1);"><i class="iconfont">&#xe613;</i></a>
        <div class="page_title"><h1><?php echo empty($this->context->title) ? '古笆兔商城' : $this->context->title  ?></h1></div>
        <a class="page_homeBtn page_headerBtn" href="<?= $this->context->to(['index/index']) ?>"><i class="iconfont">&#xe615;</i></a>
    </header>

    <?= $content ?>
</div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>