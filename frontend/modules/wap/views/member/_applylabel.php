
        <?php foreach($datas as $key => $data){  ?>
            <tr>
                <td><?=  $data['username'] ?></td>
                <td><?= date('Y-m-d H:i:s', $data['add_time']) ?></td>
                <td><?= \common\models\MemberModel::getLabelHtmlAll($data['label']) ?></td>
                <td class="hidden_td"><?=  $data['reason'] ?></td>
                <td>
                    <a class="ajax_request" href="<?= $this->context->to(['applyd','id' => $data['id'],'status' => 1]) ?>">同意</a>
                    <a class="ajax_request"href="<?= $this->context->to(['applyd','id' => $data['id'],'status' => 2]) ?>">拒绝</a>
                </td>
            </tr>            
        <?php }  ?>



<script>
    $(function(){
        $(".order_state").change(function(){
            var value = $(this).val();
            var obj = {};
            obj.uid = $(this).attr('data');
            obj.label = value;
            
            requestajax('<?= \yii::$app->controller->to(['update_label']) ?>',obj);
        })
    })
</script>