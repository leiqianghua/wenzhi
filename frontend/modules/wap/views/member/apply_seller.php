<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;
?>

<?php  \frontend\modules\wap\assets\FormAsset::register($this); ?>
<section class="memberWrapper">
<?php
    $form = ActiveForm::begin([
            'options' => ["style" => "margin-top:15px;",'class' => 'new_update_list J_ajaxForm form-horizontal','id' => 'verifyForm'],
            'enableClientScript' => false,
            'fieldConfig' => [
                'template' => "<div style='padding-top: 7px;' class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-9 col-sm-6'>{input}</div>",
            ]        
    ]);
?>
<?= $form->field($model, 'address_info')->textInput(['name' => "address_info","id" => "address_info","placeholder" => "请选择地区"])->label("所在区域");  ?>         
<input id="province_id" type="hidden" name="province_id" value="<?= $model->province_id ?>">
<input id="city_id" type="hidden" name="city_id" value="<?= $model->city_id ?>">
<input id="area_id" type="hidden" name="area_id" value="<?= $model->area_id ?>">
               

<?= $form->field($model, 'company_address')->textInput() ?>
<?= $form->field($model, 'company_name')->textInput() ?>
<?= $form->field($model, 'company_iphone')->textInput() ?>
<?= $form->field($model, 'company_corporation')->textInput() ?>
<?= $form->field($model, 'registered_capital')->textInput() ?>
<?= $form->field($model, 'main_products')->textInput() ?>
<?= $form->field($model, 'company_profile')->textarea() ?>
            
  
    <?php  //$date = date('Ymd'); ?>
    
        
<?php echo $form->field($model, 'type_1')->radioList(common\models\SellerApplyModel::getType_1Html());  ?>   
<?php echo $form->field($model, 'type_2')->radioList(common\models\SellerApplyModel::getType_2Html());  ?>        
    
    <?php  $date = date('Ymd'); ?>
    <?= Form::getFileContent($form,$model,'business_license_img','business_license_img', $date . '/business_license_img'); ?>
    <?= Form::getFileContent($form,$model,'card_id_img','card_id_img', $date . '/card_id_img'); ?>
    

<?php //echo $form->field($model, 'business_license_img')->fileInput(['name' => "business_license_img"]) ?>    
<?php //echo  $form->field($model, 'card_id_img')->fileInput(['name' => "card_id_img"]) ?>   
    
    
            <div  class="account_opt">
                <input style="width:60%" class="up_btn" type="submit" value="提 交">
            </div>   
    
    
<?php ActiveForm::end(); ?>           
            
            

</section>  


<script>   
    $(function(){
        $('#address_info').zh_region();
    })
</script>
