    <?php  if(!$datas){  ?>
        <section class="order_none">
            <p>你还没有申请级别的记录噢 !</p>
            <p>去逛一逛吧~ </p>
            <a href="<?=  $this->context->to(['member/index'])  ?>">回到首页</a>
        </section>    
    <?php }else{  ?>

        <script src="/resource/bootstrap-3.3.5/js/bootstrap.min.js"></script>
        <link href="/resource/bootstrap-3.3.5/css/bootstrap.min.css" rel="stylesheet">    
        <section>
        <table class="member_table_list table">
            <thead>
                <th>用户名</th>
                <th>申请时间</th>
                <th>申请级别</th>
                <th>申请理由</th>
                <th>处理</th>
            </thead>
            <tbody id="goods_list">            
                    <?= $this->render('_applylabel',['datas' => $datas]) ?>    
            </tbody>
        </table>        
        </section>    
        <?php require $this->context->template('layouts/base/msg_more'); ?>
    <?php  }  ?>

    
    <script>
        //下拉加载更多
        $(function(){
            $(".msg_more").click(function(){
                jiaziaMore('<?= $this->context->to(['applylabel']) ?>');
            })
            
            $(window).on('scroll', function(){
                var he = $(document).scrollTop() / $(document).height();
                if (he > 0.20) {
                    jiaziaMore('<?= $this->context->to(['applylabel']) ?>');
                }
            });                
        })
    </script>     
</body>
</html>