 <?php foreach($datas as $key => $data){  ?>
        <div class="each_order">
            <div class="order_hd">
                
                <input type="text" data="<?= $data['uid'] ?>" name="beizhu" class="beizhu control-input" value="<?php  if(!empty($data['beizhu'])){ echo $data['beizhu']; }else{ echo $data['username']; }  ?>"/>
                &nbsp&nbsp
                <span><?= date('Y-m-d',$data['add_time'])  ?></span>
                <select data="<?= $data['uid'] ?>" style="margin-top:5px;" class="order_state" name="label">
                    <?php  $d = \common\models\MemberModel::getLabelHtmlAll();  ?>
                    <?php foreach($d as $k => $v){  ?>
                        <?php  if($k <= \yii::$app->controller->user->label){ continue; }  ?>
                        <option <?php  if($k == $data['label']){ echo 'selected'; }  ?> value="<?= $k  ?>"><?= $v ?></option>
                    <?php }  ?>
                </select>
                <a href="/wap/member/detail?uid=<?= $data['uid'] ?>">详情</a>
            </div>
        </div>      
<?php }  ?>

<script>
    $(function(){
        $(".order_state").change(function(){
            var value = $(this).val();
            var obj = {};
            obj.uid = $(this).attr('data');
            obj.label = value;
            
            requestajax('<?= \yii::$app->controller->to(['update_label']) ?>',obj);
        })
        
        $(".beizhu").keyup(function(e){
            var uid = $(this).attr("data");
            var str = $(this).val();
            $.post("/wap/member/ajax_update_beizhu",{uid:uid,beizhu:str},function(data){},"json")
        })
    })
</script>