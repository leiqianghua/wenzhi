<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>
    <?=    yii\widgets\DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uid',//直接展示 
            'username',//直接展示 
            'iphone',//直接展示     
            'money',//直接展示             
            'score',//直接展示               
            'address',//直接展示                 
            'address_info',//直接展示    
        ],
    ]) ?>
