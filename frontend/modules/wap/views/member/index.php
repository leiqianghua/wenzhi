    <section class="member_banner">
        <div class="default_head"><img src="<?php if($member['img']){ echo $member['img']; }else{ echo '/resource/images/member/default_head.jpg'; }  ?> "></div>
        <div class="member_phone"><?= $member['username'] ?></div>
    </section>
    <section class="membner_home1 clear">
        <div class="order_col">
            <div class="m_icon m_1"><i class="iconfont">&#xe605;</i></div>
            <span class="txt1">全部订单</span>
            <span class="txt2"><a href="<?= $this->context->to(['member_order/index']);  ?>">查看全部订单</a></span>
            <i class="iconfont m_arrow">&#xe612;</i>
        </div>
        <a class="item" href="<?= $this->context->to(['member_order/index','order_state' => 0]);  ?>">
            <div class="state"></div><p>待付款</p>
        </a>
        <a class="item" href="<?= $this->context->to(['member_order/index','order_state' => 1]);  ?>"><div class="state"></div><p>待发货</p></a>
        <a class="item" href="<?= $this->context->to(['member_order/index','order_state' => 2]);  ?>"><div class="state"></div><p>待收货</p></a>
        <a class="item" href="<?= $this->context->to(['member_order/index','order_state' => 3]);  ?>"><div class="state"></div><p>已完成</p></a>
<!--        <a class="item" href=""><div class="state"><i class="iconfont">&#xe618;</i><em>3</em></div><p>已完成</p></a>-->
    </section>


<section class="index_catg clear">
    <ul>
        <li>
            <a class="item category_menu" href="update"><div class="img item1"><i class="iconfont"></i></div><div class="text">修改资料</div></a>
            <a class="item category_menu" href="update_password"><div class="img item1"><i class="iconfont"></i></div><div class="text">修改密码</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['member_oauth/index']);  ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">关联第三方账号</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['cart/index']);  ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">我的购物车</div></a>
            <?php if($member['label'] < 4){ ?>     
                        <a class="item category_menu" href="<?= $this->context->to(['member_account/index']);  ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">我的账户</div></a>            
            <?php } ?>  
                        
            <a class="item category_menu" href="<?= $this->context->to(['member/apply']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">申请更高级别</div></a>
            
            
            <?php if(!($member['type_1'] || $member['label'] < 2)){ ?>
                <a class="item category_menu" href="<?= $this->context->to(['member/apply_seller']);  ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">申请成为供应商</div></a>

            
            <?php }else if($member['type_1']){ ?>   

            <a class="item category_menu" href="<?= $this->context->to(['member/show_admin']);  ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">查看管理后台账号</div></a>
                
            <?php }  ?>              
            
            
            
            <a class="item category_menu" href="<?= $this->context->to(['member/applylabel']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">会员级别申请</div></a>
            
            
            <?php if($member['label'] < 4){ ?>          
            <a class="item category_menu" href="<?= $this->context->to(['member/myuser']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">会员管理</div></a>
            
            <?php } ?>            
            
            
            <a class="item category_menu" href="<?= $this->context->to(['member_address/index']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">我的收货地址</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['myma'])  ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">推广</div></a>
            
                <?php if($member['label'] < 4){ ?>          
                    <a class="item category_menu" href="<?= $this->context->to(['member/ziyuan']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">资源下载</div></a>
            
            <?php } ?>        
            
                    
            <a class="item category_menu" href="http://wpa.qq.com/msgrd?v=3&uin=405747275&site=qq&menu=yes"><div class="img item1"><i class="iconfont"></i></div><div class="text">客服QQ</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['member/case']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">案例库</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['public/logout']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">退出登录</div></a>
      
                        
        </li>
    </ul>
</section>




<script>

    
    
    
        window._bd_share_config = {
                common : {
                        bdText : '古笆兔商城',	
                        bdDesc : '古笆兔商城',	
                        bdUrl : 'www.gubatoo.com', 	
                        bdPic : '古笆兔商城'
                },
                share : [{
                        "bdSize" : 16
                }],
                slide : [{	   
                        bdImg : 0,
                        bdPos : "right",
                        bdTop : 100
                }],
                image : [{
                        viewType : 'list',
                        viewPos : 'top',
                        viewColor : 'black',
                        viewSize : '16',
                        viewList : ['qzone','tsina','huaban','tqq','renren']
                }],
                selectShare : [{
                        "bdselectMiniList" : ['qzone','tqq','kaixin001','bdxc','tqf']
                }]
        }
        with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];
</script>

