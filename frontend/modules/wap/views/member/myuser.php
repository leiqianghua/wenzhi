    <?php  if(!$datas){  ?>
        <section class="order_none">
            <p>你还没有发展会员噢 !</p>
            <p>去逛一逛吧~ </p>
            <a href="<?=  $this->context->to(['member/index'])  ?>">回到首页</a>
        </section>    
    <?php }else{  ?>
    
        <section id="goods_list"  class="refund_list">
            <?= $this->render('base_list',['datas' => $datas]) ?>    
        </section>    
        <?php require $this->context->template('layouts/base/msg_more'); ?>
    <?php  }  ?>

    
    <script>
        //下拉加载更多
        $(function(){
            $(".msg_more").click(function(){
                jiaziaMore('<?= $this->context->to(['myuser']) ?>');
            })
            
            $(window).on('scroll', function(){
                var he = $(document).scrollTop() / $(document).height();
                if (he > 0.20) {
                    jiaziaMore('<?= $this->context->to(['myuser']) ?>');
                }
            });                
        })
    </script>     
    
    
</body>
</html>