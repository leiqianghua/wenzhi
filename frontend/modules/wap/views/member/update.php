<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="wap_form">
    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    
    
    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'iphone') ?>
    
    <?= $form->field($model, 'address_info')->textInput(["class" => "form-control form_address_info",'name' => "address_info","id" => "address_info","placeholder" => "请选择地区"]);  ?>         
    <input id="province_id" type="hidden" name="province_id" value="<?= $model->province_id ?>">
    <input id="city_id" type="hidden" name="city_id" value="<?= $model->city_id ?>">
    <input id="area_id" type="hidden" name="area_id" value="<?= $model->area_id ?>">        
    
    <?= $form->field($model, 'address') ?>
    <?= $form->field($model_extend, 'corporate_name') ?>
    <?= $form->field($model_extend, 'real_username') ?>
    <?= $form->field($model_extend, 'position') ?>
    
    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div> 
    
    
    <?php ActiveForm::end(); ?>
    
</div>



<script>   
    $(function(){
        $('#address_info').zh_region();
        form_valid();//验证
    })
</script>

