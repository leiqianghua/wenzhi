<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wap_form">
    <?php
        $form = ActiveForm::begin([
                'options' => ['class' => 'J_ajaxForm form-horizontal'],
                'enableClientScript' => false,
                'fieldConfig' => [
                    'template' => "<div class='col-xs-3 col-sm-2 text-right'>{label}</div><div class='col-xs-6 col-sm-4'>{input}</div><div style='padding-top:7px;' class='col-xs-3 col-sm-2'>{hint}</div>",
                ]        
        ]);
    ?>
    


    
<div class="form-group">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label">原密码</label>
    </div>
    <div class="col-xs-6 col-sm-4">
        <input type="password" id="password" class="form-control" name="password" value="">
    </div>
    <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
        
    </div>
</div>     
<div class="form-group">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label">新密码</label>
    </div>
    <div class="col-xs-6 col-sm-4">
        <input type="password" id="password" class="form-control" name="new_password" value="">
    </div>
    <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
        
    </div>
</div>     
<div class="form-group">
    <div class="col-xs-3 col-sm-2 text-right">
        <label class="control-label">确认新密码</label>
    </div>
    <div class="col-xs-6 col-sm-4">
        <input type="password" id="password" class="form-control" name="new_password_wd" value="">
    </div>
    <div style="padding-top:7px;" class="col-xs-3 col-sm-2">
        
    </div>
</div> 
    
    
    

    <div class="form-group">
        <div class='col-xs-3 col-sm-4 text-right'>
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>    
    <?php ActiveForm::end(); ?>
    
</div>



<script>   
    $(function(){
        form_valid();//验证
    })
</script>
