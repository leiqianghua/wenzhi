<?php  \frontend\modules\wap\assets\FormAsset::register($this); ?>
<section class="memberWrapper">
        <form id="verifyForm" method="post" action="">
        <div class="balance_drawing">
            <div class="balance_col">
                <label>提现金额：</label>
                <input type="text" name="money" maxlength="9" nullmsg="请输入提现金额" errormsg="请输入正确金额" datatype="/^(([1-9]\d{0,9}))(\.\d{1,2})?$/">
            </div>
            <div class="balance_col">
                <label>提现类型：</label>
                <select class="form-control" name="type">
                    <option value="支付宝">支付宝</option>
                    <option value="微信">微信</option>
                    <option value="银行">银行提现</option>
                </select>
            </div>
            <div class="balance_col">
                <label>账号：</label>
                <input name="account" type="text" nullmsg="请输入账号" datatype="*">
            </div>
            <div class="balance_col">
                <label>收款人姓名：</label>
                <input name="name" type="text" nullmsg="请输入收款人姓名" datatype="*">
            </div>
            <div class="balance_col">
                <label>备注：</label>
                <input name="remark" type="text" nullmsg="请输入相关备注">
            </div>
            <div class="balance_col balance_tips">
<!--                <label>手续费：</label>0.00-->
                <p>提现金额必须大于500元</p>
                <p>银行提现请请备注好是什么银行</p>
            </div>
            <div class="balance_opt">
                <input class="post_btn" type="submit" value="提 交">
            </div>
        </div>
        </form>
    </section>  


<script>
    $(function(){
        form_valid();//验证
    })
</script>    