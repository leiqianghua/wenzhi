<?php  if(!$datas){  ?>
        <section class="order_none">
            <p>记录为空 !</p>
            <p>去逛一逛吧~ </p>
            <a href="<?= $this->context->to(['member/index']) ?>">回到首页</a>
        </section>    
    <?php }else{  ?>
    
        <section id="goods_list"  class="refund_list">
            <?= $this->render('_cash_list',['datas' => $datas]) ?>    
        </section>    
    <?php  }  ?>

    <?php require $this->context->template('layouts/base/msg_more'); ?>
    <script>
        //下拉加载更多
        $(function(){
            $(".msg_more").click(function(){
                jiaziaMore('<?= $this->context->to(['cash_list']) ?>');
            })
            
            $(window).on('scroll', function(){
                var he = $(document).scrollTop() / $(document).height();
                if (he > 0.20) {
                    jiaziaMore('<?= $this->context->to(['cash_list']) ?>');
                }
            });                
        })
    </script>     
    