    <section class="member_list">
        <ul>
            <li style="text-align:center">
                余额：<?= $this->context->user->money ?>&nbsp;&nbsp;
                积分：<?= $this->context->user->score ?>
            </li>
        </ul>
    </section>



<section class="index_catg clear">
    <ul>
        <li>
            <a class="item category_menu" href="<?= $this->context->to(['mycase']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">我的资金</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['member_score/index']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">积分明细</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['money_change']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">资金变化情况</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['cash_list']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">提现列表</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['member_active/index']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">活动列表</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['member_active/my']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">我的优惠券</div></a>
            <a class="item category_menu" href="<?= $this->context->to(['else_kaidan/index']) ?>"><div class="img item1"><i class="iconfont"></i></div><div class="text">开单</div></a>

        </li>
    </ul>
</section>