<?php  if(!$datas){  ?>
        <section class="order_none">
            <p>记录为空 !</p>
            <p>去逛一逛吧~ </p>
            <a href="<?= $this->context->to(['member/index']) ?>">回到首页</a>
        </section>    
    <?php }else{  ?>
    
        <section   class="refund_list">
 
        <table class="table">
            <thead>
                <tr>
                    <th>类型</th>
                    <th>时间</th>
                    <th>变化金额</th>
                </tr>
            <thead>
            <tbody id="goods_list">
                <?= $this->render('_money_change',['datas' => $datas]) ?>        
            </tbody>
        </table>            
            
            
            
        </section>    
    <?php  }  ?>

    <?php require $this->context->template('layouts/base/msg_more'); ?>
    <script>
        //下拉加载更多
        $(function(){
            $(".msg_more").click(function(){
                jiaziaMore('<?= $this->context->to(['money_change']) ?>');
            })
            
            $(window).on('scroll', function(){
                var he = $(document).scrollTop() / $(document).height();
                if (he > 0.20) {
                    jiaziaMore('<?= $this->context->to(['money_change']) ?>');
                }
            });                
        })
    </script>     
    