<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta content="中华钓鱼网" name="keywords">
<meta content="中华钓鱼网" name="description">
<link href="../../resource/css/swiper.min.css" rel="stylesheet">
<link href="../../resource/css/style.css" rel="stylesheet">
<title>中华钓鱼网-触屏版</title>
</head>
<body>
<div class="zh_container">
    <section class="memberWrapper">
        <div class="my_wallet">
            <p>您当前账户余额为</p>
            <p><em><?= $member['money']  ?> 元</em></p>
            <div class="my_money_opt">
                <a href="<?= $this->context->to(["rechange"])  ?>">充 值</a>
                <a href="<?= $this->context->to(["cash_insert"])  ?>">提 现</a>
            </div>
        </div>
<!--        <div class="my_money_tips">
            <h3>什么是余额？</h3>
            <p>余额是您在中华钓鱼网账户中的货币消费形式。如账户内有款项，可以在下单时直接勾选使用，抵消部分或全部费用。余额在购物时充当现金使用。</p>
        </div>-->
    </section>  
</div>
<script type="text/javascript" src="../../resource/js/jquery.js"></script>
<script type="text/javascript" src="../../resource/js/swiper-3.2.5.jquery.min.js"></script>
<script type="text/javascript" src="../../resource/js/main.js"></script>
<script type="text/javascript" src="../../resource/js/layer.m/layer.m.js"></script>
</body>
</html>