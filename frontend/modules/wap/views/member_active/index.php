    <section class="order_list">
        <?php foreach($datas as $data){  ?>
            <div data="<?= $data['id'] ?>" class="each_order">
                <div class="order_hd">
                    <i class="iconfont shop_icon">&#xe60b;</i>
                    <span><?= $data['title'] ?></span>
                </div>
                <a href="#">
                <div style="min-height:14em;height:auto;padding: 1em 1em 0 11em" class="order_bd">
                    <img class="order_img" src="<?=  $data['img'] ?>">
                    <span style="position:absolute;left:1em;top:9em;">总量剩余：<?= $data['max_num'] - $data['use_num'] ?></span>                    
                    <span style=position:absolute;left:1em;top:11em;">结束时间：</span>              
                    <span style=color:red;position:absolute;left:1em;top:13em;"><?= date('Y-m-d H:i',$data['invalid_time']) ?></span>
                    
                    
                    
                    <?php  $contents = json_decode($data['content'], true);  ?>
                    
                    <?php $num = 0; foreach($contents as $key => $content){ $num++; ?>
                        <p class="msg">
                            <span>
                                券<?= $num ?>：
                                金额：<?= $content['money'] ?>,
                                <?php if(!empty($content['is_overlay'])){ echo '可叠加,'; }else{ echo '不可叠加,'; }  ?>
                                <?php if(!empty($content[' must_money'])){ echo "满{$content[' must_money']}使用,"; }else{ echo ''; }  ?>
                                <?php if(!empty($content['is_tejia'])){ echo '特价商品不可用,'; }else{ echo ''; }  ?>
                                <?php if(!empty($content['goods_ids'])){ echo "只允许 {$content['goods_ids']},ID使用,"; }else{ echo ''; }  ?>
                            </span></p>
                    
                    <?php }  ?>
                    

                    <p class="info"><em>¥<?=  $data['money'] ?></em><span>×1</span></p>
                </div>
                </a>

                <div class="order_opt">
                    <a class="buy_detail" data="<?= $data['id'] ?>" data_has_num="<?= $data['max_num'] - $data['use_num'] ?>" href="javascript:void()">购买</a>
                    <input type="text"  class="form-control buy_detail_num"  placeholder="输入购买数量" value="1">
                </div>
            </div>        
        
        <?php }  ?>
    </section>


<script>
    $(function(){
        $(".buy_detail").click(function(){
            var id  = $(this).attr('data');//购买的ID
            var data_has_num  = parseInt($(this).attr('data_has_num'));//还剩余多少 
            var buy_num  = parseInt($(this).parent().find('.buy_detail_num').val());//你购买的件数
            
            
            if(!buy_num){
                alert('购买件数有误');
                return false;
            }
            
            if(buy_num > data_has_num){
                alert('库存不足');
                return false;
            }
            
            //跳 转到支付界面进行支付 产生相关的订单界面
            window.location.href="/wap/member_active/pay?id="+id+"&buy_num="+buy_num;
        })
    })
</script>