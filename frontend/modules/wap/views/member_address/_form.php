<?php  \frontend\modules\wap\assets\FormAsset::register($this); ?>

<form id="verifyForm" action=""  method="post">


    <section id="verifyForm" class="add_form">
        <div class="row">
            <input id="username"  value="<?= $model->username ?>"placeholder="请输入收货人姓名" type="text" name="username" maxlength="20" nullmsg="请输入收货人姓名" datatype="*">
        </div>
        <div class="row">
            <input id="iphone"  value="<?= $model->iphone ?>"placeholder="请输入收货人手机号码" type="text" name="iphone" maxlength="11" errormsg="请输入正确的手机号码" nullmsg="请输入手机号码" datatype="m">
        </div>
        <div class="row">
            <input id="address_info"  name="address_info" placeholder="请选择地区" type="text"  value="<?= $model->address_info ?>" errormsg="请选择完整地区" nullmsg="请选择地区" datatype="*">
            <input id="province_id" type="hidden" name="province_id" value="<?= $model->province_id ?>">
            <input id="city_id" type="hidden" name="city_id" value="<?= $model->city_id ?>">
            <input id="area_id" type="hidden" name="area_id" value="<?= $model->area_id ?>">
            <i class="iconfont arrow">&#xe612;</i>
        </div>
        <div class="row">
            <input id="address"  value="<?= $model->address ?>" placeholder="请输入详细地址" type="text" name="address" datatype="*" maxlength="100"></input>
        </div>
        <div class="row">
            <input id="is_default" type="hidden" name="is_default" value="<?= $model->is_default ?>">
            <i class="ck_icon <?php if($model->is_default){ echo 'ck_select'; }  ?>" id="switchDefault" ischeck="0"></i>
            <div class="default_txt">设为默认地址</div>
        </div>
        <?php if($this->context->action->id == 'update'){  ?>
        <div class="row addr_del">
            <a class="ajax_request" href="<?= $this->context->to(['delete','id' => $model->id])  ?>"><i class="iconfont">&#xe61a;</i>删除地址</a>
        </div>
        <?php }  ?>
        <div class="row">
        </div>        
    </section>
    <div class="add_opt_box">
        <input type="submit" id="saveAdd_btn" value="保存地址">
    </div>
    </form>

<script type="text/javascript">
$(function(){
   $('#address_info').zh_region();
   changeAddress();
   
   
   form_valid();//验证
   
   
//收货地址管理
function changeAddress(){
   var _height=$(window).height();
   var _height2 = _height - 45;   // 选择地区高度
   $(document).on('click', '.areaEdit', function() {   //选择地区
        $('#areaWrapper').addClass('area_show');
        $('.area_cover').fadeIn();
        $("#areaList").css({"height":_height2})
    })
    $('.area_backBtn').on('click',function(){
        $('#areaWrapper').removeClass('area_show');
        $('.area_cover').fadeOut();
        $('#areaList').css({"height":'auto'})
    })

    // 设为默认地址
    $('#switchDefault').on('click', function() {
        if ($(this).hasClass('ck_select')) {
            $("#is_default").val(0);
            $(this).removeClass('ck_select').next().val(0);
        } else {
            $("#is_default").val(1);
            $(this).addClass('ck_select').next().val(1);
        }
    })
}   
   
})
</script>