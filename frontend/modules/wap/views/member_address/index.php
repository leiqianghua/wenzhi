    <?php  if($address){ ?>
        <section class="list_Addr">
                <ul>
                    <?php foreach($address as $key => $value){  ?>
                        <li class="<?php if($value['is_default']){ echo 'active'; }  ?>">
                            <a href="<?= $this->context->to(['update','id' => $value['id']]) ?>">
                            <div class="item_list">
                                <i class="iconfont location">&#xe610;</i>
                                <i class="iconfont arrow">&#xe612;</i>
                                <p>收货人：<span class="select_name"><?= $value['username']  ?></span><span class="select_phone"><?= $value['iphone']  ?></span></p>
                                <p>收货地址：<span class="select_address"><?= $value['address_info'] . " " . $value['address'] ?></span></p>
                            </div>
                            </a>
                        </li>                    
                    <?php }  ?>
                </ul>
        </section>
    <?php }  ?>



    <?php  if(!$address){ ?>
        <section class="addr_none">
            <div class="addr_noneImg"></div>
            <p>您还没有添加收货地址!</p>
            <a href="<?= $this->context->to(['create'])  ?>">添加新地址</a>
        </section>
    <?php }  ?>
