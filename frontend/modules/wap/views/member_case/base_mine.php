 <?php foreach($datas as $key => $data){  ?>

    <!--订单类的包含了商品图片-->
    <div class="each_order">
        <div class="order_hd">
            <span><?= $data['title'] ?></span>
            <span class="order_state"><?= \common\models\MemberCaseModel::getStatusHtml($data['status']) ?></span>
        </div>

        <div class="order_bd">
            <img class="order_img" src="<?= \common\models\MemberCaseModel::getStatusHtml($data['img']) ?>">
            <h2>造景师姓名：<?= $data['realname'] ?>。电话：<?= $data['iphone'] ?></h2>
            <p class="msg">
                <span><?= \common\models\MemberCaseModel::gettypeHtml($data['type']) ?></span>
                <span><?= \common\models\MemberCaseModel::getsceneHtml($data['scene']) ?></span>
                <span><?= \common\models\MemberCaseModel::getstylesHtml($data['styles']) ?></span>
            </p>
    <!--    <p class="back">退款金额：<em>¥ 158.00</em></p>-->
            <p class="info"><em>¥<?= $data['money'] ?></em></p>
        </div>


        <div class="order_opt">
            <a href="<?=  $this->context->to(["detail","id" => $data['id']]) ?>">查看详情</a>
        </div>
    </div>

<?php }  ?>
