

<ul style="margin-left: 15px;"class="member_list">
    <li>    
        <label>名称:</label>
        <?= $model['title'] ?>
    </li>
    <li>    
        <label>造景师姓名:</label>
        <?= $model['realname'] ?>
    </li>
    <li>    
        <label>造景师电话:</label>
        <?= $model['iphone'] ?>
    </li>
    <li>    
        <label>微信:</label>
        <?= $model['winxin'] ?>
    </li>
    <li>    
        <label>造景金额:</label>
        <?= $model['money'] ?>
    </li>
    <li>    
        <label>场景:</label>
        <?= common\models\MemberCaseModel::getSceneHtml($model['scene']) ?>
    </li>
    <li>    
        <label>客户类型:</label>
        <?= common\models\MemberCaseModel::getTypeHtml($model['type']) ?>
    </li>
    <li>    
        <label>风格:</label>
        <?= common\models\MemberCaseModel::getStylesHtml($model['styles']) ?>
    </li>
</ul>


<?php  $imgs = json_decode($model['imgs'],true);  ?>
<ul>
     <?php foreach($imgs as $key => $data){  ?>
        <li style="margin-top: 10px;">
            <img style="width: 100%;" src="<?= $data  ?>"/>
        </li>    
    <?php }  ?>
</ul>
