    <?php  if(!$datas){  ?>
        <section class="order_none">
            <p>还没有相关案例 !</p>
            <p>去逛一逛吧~ </p>
            <a href="<?=  $this->context->to(['create'])  ?>">上传案例</a>
        </section>    
    <?php }else{  ?>
    
        <section id="goods_list">
            <?= $this->render('base_index',['datas' => $datas]) ?>    
        </section>    
        <?php require $this->context->template('layouts/base/msg_more'); ?>
    <?php  }  ?>

    
    <script>
        //下拉加载更多
        $(function(){
            list_data('<?= $this->context->to(['index']) ?>');
        })
    </script>     
    
    
</body>
</html>