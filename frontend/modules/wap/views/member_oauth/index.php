<div style="margin:15px;">
    <a href="<?= $this->context->to(['create','type' => 'weixin'])  ?>">绑定微信</a>
    <a href="<?= $this->context->to(['create','type' => 'qq'])  ?>">绑定QQ</a>
</div>
<div id="goods_list">
    <?php if($datas){  ?>
        <?= $this->render('base_list',['datas' => $datas]) ?>    
         <?php require $this->context->template('layouts/base/msg_more'); ?>
    <?php  }else{ ?>
        <section class="order_none">
            <p>您没有绑定第三方账号 !</p>
            <p>去逛一逛吧~ </p>
            <a href="<?=  $this->context->to(['member/index'])  ?>">回到首页</a>
        </section>    
    
    <?php }  ?>

</div>


    <script>
        //下拉加载更多
        $(function(){
            $(".msg_more").click(function(){
                jiaziaMore('<?= $this->context->to(['index']) ?>');
            })
            
            $(window).on('scroll', function(){
                var he = $(document).scrollTop() / $(document).height();
                if (he > 0.20) {
                    jiaziaMore('<?= $this->context->to(['index']) ?>');
                }
            });                
        })
    </script>  
</body>
</html>