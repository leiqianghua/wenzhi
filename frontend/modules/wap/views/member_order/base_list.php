<?php 
    use common\models\OrderModel;
    use common\models\OrderGoodsModel;
    use common\models\GoodsModel;
?>

 <?php foreach($datas as $key => $data){  ?>
    <section class="order_list">
        <div class="each_order">
            <div class="order_hd">
                <i class="iconfont shop_icon">&#xe60b;</i>
                <span>订单号：<?= $data['order_sn'] ?></span>
                <span class="order_state"><?php echo OrderModel::getOrder_stateHtml($data['order_state'])  ?></span>
            </div>
                <?php  
                    //获取订单商品
                    $order_goods = OrderGoodsModel::getdataForOrderID($data['order_id']);
                    $total_num = 0;
                ?>    

                <?php  foreach($order_goods as $key => $ordergood){  ?>
                    <div class="taozhuang">
                        <div class="order_hd">
                            <?php if($ordergood['type'] == 2 && $data['order_state'] == 3){  ?>
                                <?php if($ordergood['bvaluate']){  ?>
                                    <a style="color:red" href="<?= $this->context->to(['bvaluate_show','id' => $ordergood['id']]) ?>">查看评价</a>
                                <?php  }else{ ?>
                                    <a style="color:red" href="<?= $this->context->to(['bvaluate','id' => $ordergood['id']]) ?>">评价</a>
                                <?php }  ?>
                                
                            <?php }  ?>
                            
                        </div>    
                        
                        <?php foreach($ordergood['data'] as $k => $v){  ?>
                            <?php  
                                if($ordergood['type'] == 2){
                                    $url = $this->context->to(['goods_package/detail','gid' => $v['goods_id']]);
                                }else{
                                    $url = $this->context->to(['goods/index','gid' => $v['goods_id']]);
                                }
                                $url = $this->context->to(['goods/index','gid' => $v['goods_id']]);
                            ?>
                            <a target="_blank" href="<?= $url;  ?>">
                            <div class="order_bd">
                                <img class="order_img" src="<?=  $v['img'] ?>">
                                <h2><?=  $v['name'] ?></h2>
                                <p class="info"><em>¥<?=  $v['discount_money']/$v['buy_num'] ?></em><span>×<?= $v['buy_num'] ?></span></p>
                            </div>
                            </a>                        
                        
                        <?php }  ?>              
                    </div>        
                <?php }  ?>    
            </a>
            <div class="order_ft">
                 折扣价合计：<em>¥ <?= $data['order_amount']  ?></em>（含运费 ¥<?= $data['freight']  ?>）
            </div>
            <?php  if($data['uid'] == $this->context->user->uid){  ?>
            <div class="order_opt">
                <?php OrderModel::getStateGood($data) ?>
            </div>
            <?php }  ?>
        </div>
    </section>
<?php }  ?>
