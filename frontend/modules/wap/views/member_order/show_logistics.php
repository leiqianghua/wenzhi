<?php if($content){  ?>
    <section class="member_logis">
            <i class="iconfont car">&#xe603;</i>
            <p>物流状态：<em><?= common\models\ExpressModel::getStateHtml($content['state']) ?></em></p>
            <p>订单编号：<?= $order['order_sn'] ?></p>
            <p>快递单号：<?= $order['shipping_code'] ?></p>
            <p>物流公司：<?php  $d = common\models\ExpressModel::find()->where(['e_code' => $order['e_code']])->asArray()->select('e_name')->limit(1)->one(); echo $d['e_name']   ?></p>
    </section>
    <section class="member_logisList">
        <h2>物流跟踪</h2>
        <div class="logisList_box">
            <ul>
                <?php foreach($content['data'] as $key => $value){  ?>
                    <li class="<?php if($key == 0){ echo 'active'; }  ?>">
                        <i class="circle"></i>
                        <p><?= $value['context']  ?></p>
                        <p><?= $value['time']  ?></p>
                        <div></div>
                    </li>            
                <?php }  ?>
            </ul>
            <div class="row_line"></div>
        </div>
    </section>
<?php  }else{ ?>
    <section class="refund_progess">
        <p class="info"><?= $error;  ?></p>
    </section>
<?php } ?>

