<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin_roleModel */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $content = json_decode($model->content,true);
error_reporting(0)  ?>
<table class="table table-striped table-bordered detail-view">
    <tbody>
        <tr>
            <th>标题</th>
            <td><?= $model['title']  ?></td>
        </tr>
        
        <?php if(!empty($content['content_1'])){ foreach($content['content_1'] as $data){  ?>
            <tr>
                <th ><a href="/wap/goods/index?gid=<?=  $data['common_id'] ?>"><?= $data['name'] ?></a></th>
                <th><img class="show_img" style="width: 60px;" src="<?= $data['img'] ?>" />
                    </br>
                    <?= "数量:{$data['buy_num']},单价{$data['price']}"  ?>
                </th>
                <td></td>
            </tr>        
        
        <?php }}  ?>
            
            
        <?php if(!empty($content['content_2'])){ foreach($content['content_2'] as $data){  ?>
            <tr>
                <th><?= $data['title'] ?></th>
                <th> <img class="show_img" style="width: 60px;" src="<?= $data['duoqi_icon'] ?>" />
                    </br>
                    <?= "数量:{$data['num']},单价{$data['price']}"  ?>
                </th>
            </tr>        
        
        <?php }}  ?>            
        
        <tr>
            <th>原价</th>
            <td><?= $model['price']  ?></td>
        </tr>
        <tr>
            <th>折扣价</th>
            <td><?= $model['zhe_price']  ?></td>
        </tr>
        <tr>
            <th>分几次付款</th>
            <td><?= $model['step']  ?></td>
        </tr>
        <tr>
            <th>每次付款比例</th>
            <td><?= $model['step_pay_num']  ?></td>
        </tr>
        <tr>
            <th>已付款次数</th>
            <td><?= $model['step_has_ok']  ?></td>
        </tr>
        <tr>
            <th>付款截图</th>
            <td>
                <?php  
                $img = json_decode($model->user_upload_img,true); 
                $img = array_values($img);
                $count = count($img) - 1;
                echo "<img style='width:100%' src='{$img[$count]}'/>";
                ?>
            </td>
        </tr>
    </tbody>
</table>


<?php  
    if($model['step_has_ok'] == $model['step'] && $model['last_setp_is_confirm']){//已经完成了  完成状态
        echo "<h2>已完成付款</h2>";
    }else if($model['last_setp_is_confirm'] || !$model['step_has_ok']){//用户一步都没有完成  或者 最后一次的用户付款已经确认了
?>
<h2>需付款<?php   $arr = explode(',', $model['step_pay_num']); echo $arr[$model->step_has_ok]/100*$model->zhe_price  ?></h2>



<?php
    }else{//等待着确认
        $uid = \yii::$app->controller->user->uid;
        $system_user = \common\models\ConfigModel::getOne("goods_buy_notice", "value");
        $system_user = explode(",", $system_user);
        if(in_array($uid, $system_user)){
            echo "<button><a href='/wap/else_kaidan/is_pay?id={$model['id']}'>确认到款</a></button>";
        }else{
            echo "<h2>请等待管理员确认付款</h2>";
        } 
    }
?>


