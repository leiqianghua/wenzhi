    <section class="memberWrapper">
        <form id="verifyForm" method="post" action="/wap/oauth/band">
            <input type="hidden" name="type_new" value="<?= \common\components\GuestInfo::getParam("type_new") ?>">
        <input type="hidden" name="type" value="<?=  $type ?>"/>   
        <input type="hidden" name="token" value="<?=  $token ?>"/>   
        <div class="balance_drawing">
            <div class="balance_col">
                <label>手机号码：</label>
                <input type="text" name="iphone" maxlength="11" nullmsg="请输入手机号码" errormsg="请输入手机号码" datatype="/^1[3|4|5|7|8][0-9]{9}$/">
            </div>
            <div class="balance_col">
                <input type="password" name="password" placeholder="请输入密码" nullmsg="请输入密码" errormsg="密码6到16位" value="" datatype="*6-16" maxlength="20" id="password"  >
            </div>
            <div class="balance_col">
                <input type="password" name="rspassword"  placeholder="请再次输入密码" nullmsg="请再次输入密码" errormsg="两次密码不一样" value="" datatype="*" recheck="password" maxlength="20" id="rspassword" >
            </div>
            <div class="balance_opt">
                <input class="post_btn" type="submit" value="提 交">
            </div>
        </div>
        </form>
    </section>  


<script>
    $(function(){
        form_valid();//验证
    })
</script>    