<?php  \frontend\modules\wap\assets\FormAsset::register($this); ?>

<form id="login_form" method="post" action="">
    <input type="hidden" name="type_new" value="<?= \common\components\GuestInfo::getParam("type_new") ?>">
        <section class="member_login">
            <div class="login_col">
                <label>手 机：</label>
                <input type="text" placeholder="请输入手机号" nullmsg="请输入手机号！" maxlength="30" datatype="*" name="name" id="name">
            </div>
            <div class="login_col">
                <label>密 码：</label>
                <input type="password" placeholder="请输入密码" maxlength="30" nullmsg="请填写密码！" datatype="*" name="password" autocomplete="off"  id="password">
            </div>
        </section>

        <section class="member_loginOpt">
            <input class="submit_btn" type="submit" name="loginSubmit" value="登 录">
            <div class="member_optBar">
                <a href="<?=  $this->context->to('register') ?>">免费注册</a>
<!--                <a href="<?=  $this->context->to('find_password') ?>">找回密码</a>-->
            </div>
            <div class="other_login">
                <i class="line"></i>
                <span>其它登录方式</span>
            </div>
            <div class="login_way">
                <a class="way_qq" href="<?=  $this->context->to('/wap/oauth/qq') ?>"><i class="iconfont">&#xe61e;</i></a>
<!--                <a class="way_weixin" href="<?=  $this->context->to('/wap/oauth/weixin') ?>"><i class="iconfont">&#xe61f;</i></a>-->
<!--                <a class="way_weibo" href="#"><i class="iconfont">&#xe621;</i></a>-->
            </div>
        </section>
    </form>
<footer class="footer" style="color:black">

                <div class="copyright">Copyright &copy; 2021 gubatoo.com 古笆兔版权所有 <a href="https://beian.miit.gov.cn">闽ICP备19024784号-2</a></div>
</footer>
<script>
    window.onload = function(){
        form_valid(false);//验证
    }
</script>