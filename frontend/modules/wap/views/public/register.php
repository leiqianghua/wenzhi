<?php  \frontend\modules\wap\assets\FormAsset::register($this); ?>

<form id="register_form" method="post" action="">
    <input type="hidden" name="type_new" value="<?= \common\components\GuestInfo::getParam("type_new") ?>">
    <section class="member_register">
        <div class="register_col">
            <input type="text" placeholder="请输入手机号码" name="iphone" id="iphone" maxlength="11" datatype="ph">
        </div>
<!--        <div class="register_col">
            <input type="text" placeholder="请输入验证码" nullmsg="请输入验证码" errormsg="请输入6位数验证码" maxlength="6" datatype="n6-6" name="code" id="code">
            <a id="button_phone" class="button_phone" href="javascript:void(0);">获取短信验证码</a>
        </div>-->
        <div class="register_col">
            <input type="password" name="password" placeholder="请输入密码" nullmsg="请输入密码" errormsg="密码6到16位" value="" datatype="*6-16" maxlength="20" id="password"  >
        </div>
        <div class="register_col">
            <input type="password" name="rspassword"  placeholder="请再次输入密码" nullmsg="请再次输入密码" errormsg="两次密码不一样" value="" datatype="*" recheck="password" maxlength="20" id="rspassword" >
        </div>
    </section>

    <section class="member_loginOpt">
<!--        <p class="txt1">注册即视为同意<a href="#">《古笆兔网使用协议》</a></p>-->
        <input type="submit" value="注 册" id="Submit" class="submit_btn" >
<!--        <p class="txt2">注：没收到短信？请检查是否被您的手机软件屏蔽，或者点击获取验证码重新发送</p>-->
    </section>
</form>

<script>
    window.onload = function(){
        send_code('#button_phone');  //验证码
        var valid = form_valid();//验证登录
        $.Datatype.u1=/^[^:%,'\*\"\s\<\>\&]+$/;
        $.Datatype.ph=/^1[3|4|5|7|8][0-9]{9}$/;
        $.Datatype.stringLength=function(gets){
                if(getStringLength(gets)>24||getStringLength(gets)<6){
                    return "用户名的长度为6-18个字符";
                }
            };
        $.Datatype.isPhoneNum=function(gets){
                var reg=/^1[3|4|5|7|8][0-9]{9}$/;
                if(reg.test(gets)){
                    return "用户名不能为手机号！";  
                }
            };
        $.Datatype.isEmall=function(gets){
                var reg=/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                if(reg.test(gets)){
                    return "用户名不能为邮箱！";  
                }
            };
            
        function getStringLength(str){
            var slength=0;
            for(i=0;i<str.length;i++){
                if ((str.charCodeAt(i)>=0) && (str.charCodeAt(i)<=255))
                        slength=slength+1;
                else
                        slength=slength+3;
            }   
            return slength;
        }
        
    };
</script>