<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>
<div class="zh_container">
    <?php require $this->context->template('layouts/base/search5'); ?>
    

    
        <section class="search_noGoods">
            <p>很抱歉，未找到相关商品。您可以：</p>
            <p class="txt">1. 重新输入关键字搜索</p>
            <p class="txt">2. 返回<a href="<?= $this->context->to(['index/index']) ?>">首页</a>浏览热卖商品</p>
            <div class="search_noTitle">
                <i class="line"></i>
                <span>为您推荐</span>
            </div>
        </section>    

    


    <section class="goods_container clear" id="goods_container">
<!--        推荐-->
        <?php require $this->context->template('layouts/base/recommend') ?>
    </section>

    <?php require $this->context->template('layouts/base/msg_more'); ?>
    

    <?php require $this->context->template('layouts/base/footer'); ?>
</div>



<?php require $this->context->template('layouts/base/backtotop'); ?>
<?php require $this->context->template('layouts/base/resource'); ?>
    
      
</body>
</html>