<!DOCTYPE html>
<html lang="zh-CN">
<head>
<?php require $this->context->template('layouts/base/head'); ?>
</head>
<body>
<div class="zh_container">
    <?php require $this->context->template('layouts/base/search5'); ?>

    <!--    排序相关功能-->
    <?php 
//    $url =  $this->context->to(['search/index']); 
//    require $this->context->template('layouts/base/order');
    ?>    

    
    

    <section class="goods_container clear" id="goods_container">
        <ul id="goods_list" class="clear">
            <?php require $this->context->template('layouts/base/goods_package') ?>
        </ul> 
    </section>
    
    <?php require $this->context->template('layouts/base/msg_more'); ?>
    <?php require $this->context->template('layouts/base/footer'); ?>
</div>

    
<?php require $this->context->template('layouts/base/backtotop'); ?>
<?php require $this->context->template('layouts/base/resource'); ?>       

</body>

<script>
    $(function(){
        function searchLoadMore(){
            jiaziaMore('<?= $this->context->to(['search_package/index']) ?>');
        }
        //点击加载更多
        $(".msg_more").click(function(){
            searchLoadMore()
        })
        //下拉加载更多数据
        loadGoods(searchLoadMore);
    })
</script>  
</html>