<?php 
    \backend\assets\CommonAsset::register($this);
?>
<?php $this->beginPage();  ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo  yii::$app->name ?></title>
		<?php $this->head();  ?>
	</head>
        <?php $this->beginBody();  ?>
	<body>
		<div class="tij">
		   <div class="tij_img"><img src="/common/images/error.png"></div>
		   <div class="tij_text">
		   	<p><?= $info  ?></p>
		   	<p><a href="/">返回首页</a></p>
		   	<p><a href="javascript:history.go(-1)">返回上一页</a></p>
		   </div>	
		</div>
	</body>
        <?php $this->endBody();  ?>
</html>
<?php $this->endPage();  ?>

<script language="javascript">
if('<?php echo $url;?>'){
    setTimeout(function(){
        location.href = '<?php echo $url;?>';
    },<?php echo $wait_second;?>);             
}     
</script>


