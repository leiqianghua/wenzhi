

//获取对象的个数
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var URL = window.location.href;


//修改浏览器地址
function updateUrl(arg,arg_val)
{
    var stateObject = {};
    var title = "w";
    var newUrl = changeURLArg(URL,arg,arg_val);
    history.pushState(stateObject,title,newUrl);    
}


//通过URL与参数与参数值设置 URL链接
function changeURLArg(url,arg,arg_val){
    if(!arg_val){
        return delQueStr(url,arg);
    }
    var pattern=arg+'=([^&]*)'; 
    var replaceText=arg+'='+arg_val; 
    if(url.match(pattern)){ 
        var tmp='/('+ arg+'=)([^&]*)/gi'; 
        tmp=url.replace(eval(tmp),replaceText); 
        return tmp; 
    }else{ 
        if(url.match('[\?]')){ 
            return url+'&'+replaceText; 
        }else{ 
            return url+'?'+replaceText; 
        } 
    } 
    return url+'\n'+arg+'\n'+arg_val; 
}
//删除URL中的某个参数
function delQueStr(url, ref) {
     var str = "";
     if (url.indexOf('?') != -1) {
         str = url.substr(url.indexOf('?') + 1);
     }
     else {
         return url;
     }
     var arr = "";
     var returnurl = "";
     var setparam = "";
     if (str.indexOf('&') != -1) {
         arr = str.split('&');
         for (i in arr) {
             if (arr[i].split('=')[0] != ref) {
                 returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
             }
         }
         return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
     }
     else {
         arr = str.split('=');
         if (arr[0] == ref) {
             return url.substr(0, url.indexOf('?'));
         }
         else {
             return url;
         }
     }
 }


/**
* 商品详情页js
**/
$(function(){
    if($(".buy_no").length <= 0){
        checkCanClick();     
    }
    
    //规格点击事件
    $(document).on('click','.yes_click',function(){
        $(".btn_buy").addClass('buynow_submit');
        $(".btn_buy").removeClass('buy_no');
        $(".btn_add_car").addClass('addcart_submit');
        $(".btn_add_car").removeClass('buy_no');
        $(".buy_no_tip").remove();
        
        
        var obj = $(this);
        if(obj.hasClass('active')){
            obj.removeClass('active');
        }else{
            $(obj).parent('dd').find("[data-param]").removeClass('active');
            $(obj).addClass('active');                
        }
        
        checkCanClick();
        createGoods_id();
    }) 
    
    if($(".yes_click").length == 1){
        $(".yes_click").click();
    }
    
    
    function isOk(key1,key2,key3)
    {
        for(var key_1 in result_data){
            for(var key_2 in result_data[key_1]){
                for(var key_3 in result_data[key_1][key_2]){
                    if(!(key1 === false || key1 == key_1)){//等于三个都是要通过才可以
                        continue;
                    }
                    if(!(key2 ===false || key2 == key_2)){//等于三个都是要通过才可以
                        continue;
                    }
                    if(!(key3 === false || key3 == key_3)){//等于三个都是要通过才可以
                        continue;
                    }
                    return true;
                }                 
            }            
        }
        return false;
    }    
    //给每一个来确认其是否是可以点击的要不要带上click的class  选择了的class   然后拥有的这个属性的集合。
    function checkCanClick()
    {
        var obj_select = $(".yes_click.active");//已经选择了的obj
        var length = obj_select.length;
        var spec_select = {};
        
        for(var i = 0;i<length;i++){
            var spec_num = obj_select.eq(i).attr("data-spec_num");
            var spec_key = obj_select.eq(i).attr("data-param");
            spec_select[spec_num] = spec_key;
        }

            //console.log(key_1);console.log(key_2);console.log(key_3);       
        
        
        var spec_click = $(".spec").find('span[data-param]:not(.active)');//没有选择的obj
        var length = spec_click.length;
        for(var i = 0;i<length;i++){//计算每一个的是不是要加click让它可以点击
            var obj = spec_click.eq(i);
            var spec_num = obj.attr("data-spec_num");
            var spec_key = obj.attr("data-param");
            var key_1 = key_2 = key_3 = false;//三个需要带的参数。
            //console.log(spec_num);
            //console.log(spec_key);
            for(var num in spec_select){
                if(num == 1){
                    key_1 = spec_select[num];
                }else if(num == 2){
                    key_2 = spec_select[num];
                }else{
                    key_3 = spec_select[num];
                }                
            }             
            //console.log(key_1);console.log(key_2);console.log(key_3);  
            
            if(spec_num == 1){
                key_1 = spec_key;
            }else if(spec_num == 2){
                key_2 = spec_key;
            }else{
                key_3 = spec_key;
            }
            if(isOk(key_1,key_2,key_3)){
                obj.addClass("yes_click");
            }else{
                obj.removeClass("yes_click");               
            }
            
        }
    }
    

    
    
    //规格点击事件
    $(document).on('click','.yes_click_bakbak',function(){
        $(".btn_buy").addClass('buynow_submit');
        $(".btn_buy").removeClass('buy_no');
        $(".btn_add_car").addClass('addcart_submit');
        $(".btn_add_car").removeClass('buy_no');
        $(".buy_no_tip").remove();
        
        
        var obj = $(this);
        if(obj.hasClass('active')){
            obj.removeClass('active');
        }else{
            $(obj).parent('dd').find("[data-param]").removeClass('active');
            $(obj).addClass('active');                
        }
        var spec_id = obj.attr('data-param');
        var is_img = obj.parent('dd').hasClass('ify_yes');
        if(is_img){
            img_change(spec_id);
        }
        createspec();                 
        createGoods_id();
    })
    
    //通过点击的规格增加商品ID的选择
    function createGoods_id()
    {
        var obj = $('.yes_click.active');
        var length = obj.length;
        var length_sku_box = $(".sku_box > .spec_num").length;
        if(length != length_sku_box){
            GOODS_ID = '';
            updateUrl('goods_id', '');
            return false;
        }
        
        
        var key_1 = key_2 = key_3 = 0;
        var spec = [];
        for(var i = 0;i<length;i++){
            var spec_num = obj.eq(i).attr("data-spec_num");
            var spec_key = obj.eq(i).attr("data-param");
            if(spec_num == 1){
                key_1 = spec_key;
            }else if(spec_num == 2){
                key_2 = spec_key;
            }else{
                key_3 = spec_key;
            }            
        }
        GOODS_ID = result_data[key_1][key_2][key_3];
        
        
        
        
        //GOODS_ID = goods_id;
        //$(".tb_act").show();
        var data = DATAS.data_goods_list[GOODS_ID];
        console.log(data);
        $("#goods_storage").html(data.num);
        $("#goods_price").html(data.discount_4);
        $("#goods_marketprice").html(data.price);
        
        updateUrl('goods_id', GOODS_ID);
        
        return true;
    }

    //通过规格ID改变图片
    function img_change(spec_id)
    {
        if(typeof(DATAS.data_images[spec_id]) == 'object'){
            var img = DATAS.data_images[spec_id].goods_image;
            var cur_img = IMG_URL + img + "@!shop360";
            var r_big_img = IMG_URL + img + "@!shop700";
            $(".cur_img").attr('src',cur_img);
            $(".r_big_img img").attr('src',r_big_img);
        }
        return true;
    }


    
    //通过商品ID列表明判断是否有库存 数组
    function catGoods_storage(obj_goods_list)
    {
        for(var i = 0;i<obj_goods_list.length;i++){
            if(parseInt(DATAS.data_goods[obj_goods_list[i]].goods_storage) > 0){
                return true;
            }
        }
        return false;
    }
    
    //取对象的key变成数组。
    function getObjkey(obj)
    {
        var arr = [];
        var num = 0;
        for(var i in obj){
            arr[num] = i;
            num++;
        }
        return arr;
    }
    
    
    //相等的值的个数
    //spec_detail 是具体的某一个规格ID，，你要求是否是要点击的
    //spec 是选中了的规格列表
    //good_spec 是指某一个商品的规格列表
    //求出这个商品包含了spec_detail这个规格ID的情况下  然后有几个是包含在了spec中的
    function getNumEn(spec_detail,spec,good_spec)
    {
        if(!isInArray(good_spec, spec_detail)){
            return 0;
        }       
        var n = 0;
        for(var i = 0; i<good_spec.length;i++){
            if(isInArray(spec, good_spec[i])){
                n++;
            }                
        }
        return n;
    }
    //遍历数组模拟indexOf
    function isInArray(good_spec, spec_detail){
        for(var i=0; i<good_spec.length; i++){
            if(good_spec[i] == spec_detail){
                return true;
            }
        }
        return false;
    }

	//立即购买
	$('a.buynow_submit').click(function(){
        if(IS_LOGIN == '' || IS_LOGIN == '0'){//未登录
            window.location.href='/wap/public/login';
            return false;
        }
        if(STORE_SELF == '1'){
            layer.open({
                className: 'alertMsg',
                content: '不能购买自己店铺的商品',
                time:2
            });
            return false;                  
        }
        if(!GOODS_ID || GOODS_ID == '0'){//还没有选到商品ID  
            layer.open({
                className: 'alertMsg',
                content: '规格没选全',
                time:2
            });
            return false;                    
        }else{
            var goods_num = parseInt($("#goods_num").val());//购买的件数
            var goods_storage = parseInt($("#goods_storage").html());//库存
            if(goods_num > goods_storage){
                layer.open({
                    className: 'alertMsg',
                    content: '库存不足',
                    time:2
                });
                return false;   
            }
        }
        //购买
        buynow(GOODS_ID, goods_num);
	});

	//加入购物车
	$('.addcart_submit').on('click',function(){
        var thisObj=$(this);
        if(!GOODS_ID || GOODS_ID == '0'){//还没有选到商品ID   
            layer.open({
                className: 'alertMsg',
                content: '规格没选全',
                time:2
            });
            return false;                    
        }else{
            var goods_num = parseInt($("#goods_num").val());//购买的件数
            var goods_storage = parseInt($("#goods_storage").html());//库存
            if(goods_num > goods_storage){
                layer.open({
                    className: 'alertMsg',
                    content: '库存不足',
                    time:2
                });
                return false;   
            }
        }
        var url = '/wap/cart/add';
        var goods_id=GOODS_ID;
        $.getJSON(url, {'goods_id': goods_id, 'quantity': goods_num}, function(data) {
            if (data !== null) {
                //layer.closeAll();
                if (data.status) {
                    layer.open({
                        className: 'alertMsg',
                        content: '添加成功',
                        time:2
                    });
                    $(".cart_num").html(data.num);
                } else {
                    layer.open({
                        className: 'alertMsg',
                        content: data.msg,
                        time:2
                    });
                }
            }
        });
	}) 

	//点击收藏商品
	$("#collect").click(function(){ 
        $.post(COLLECT_URL, {'fav_id': DATAS.data_common.goods_commonid}, function(data) {
            if(data.status){
                layer.open({
                    className: 'alertMsg',
                    content: '收藏成功',
                    time:2
                });                
                $("#collect").addClass('active');
            }else{
                if(IS_LOGIN){
                    layer.open({
                        className: 'alertMsg',
                        content: data.msg,
                        time:2
                    });                     
                }else{
                    window.location.href='/m/index.php?act=login&op=index';
                }
            }
        },'json');
	})

	//商品数量操作
    var cartOpt = new cartNum();
    cartOpt.num_input();
    cartOpt.num_add();
    cartOpt.minus();
})

// 商品详情页 数量加减
var cartNum = function(){
    var buyNumObj=$('#buyNum');
    var buyNumInput=buyNumObj.children('input');
    var buyNumAdd=buyNumObj.children('.add');
    var buyNumMinus=buyNumObj.children('.minus');
    this.num_input = function(){
        buyNumInput.on('keyup',function(){
            var inputNum=parseInt($(this).val());
            var buyMaxNum = parseInt($("#goods_storage").html());
            if(isNaN(inputNum)||inputNum<=0){
                inputNum=1;
            }else{
                if(inputNum>buyMaxNum){
                    inputNum=buyMaxNum;
                    layer.open({
                        className: 'alertMsg',
                        content: '超出数量范围~',
                        time:2
                    });
                }
            }
            $(this).val(inputNum);
        })
    };
    this.num_add = function(){
        buyNumAdd.on('click',function(){
            var num=parseInt(buyNumInput.val());
            var buyMaxNum = parseInt($("#goods_storage").html());
            if(num>=buyMaxNum){
                layer.open({
                    className: 'alertMsg',
                    content: '亲，宝贝不能购买更多哦~',
                    time:2
                });
               return
            };
            buyNumInput.val(num+1);
        })
    };
    this.minus = function(){
        buyNumMinus.on('click',function(){
            var num=parseInt(buyNumInput.val());
            if(num<=1){
                layer.open({
                    className: 'alertMsg',
                    content: '受不了，宝贝不能再减少了哦~',
                    time:2
                });
                return
            };
            buyNumInput.val(num-1);
        })
    }
}

// 立即购买  num(商品数量)
function buynow(goods_id, num) {
    if (num < 1 || num == undefined) {
        layer.open({
            className: 'alertMsg',
            content: '宝贝库存不足,请选择别的产品~',
            time:2
        });
        return;
    }
    
    $("#num").val(num);
    $("#goods_id").val(goods_id);
    $("#buynow_form").submit();
}
