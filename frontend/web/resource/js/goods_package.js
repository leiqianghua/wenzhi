$(function(){


$(".fujia_num").click(function(){
    calePrice();
})



$(document).on('click',".cart_del",function(){
    $(this).parent().remove();
    calePrice();
})

$("#category_goods").change(function(){
    var cid = $(this).val();
    if(cid == 0){
        $("#goods_list_select").remove();
    }else{
        $.post("/wap/goods/goods_select",{"cid":cid},function(data){
            if(data.status){
                $("#goods_list_select").remove();
                $("#category_goods").after(data.content);
            }else{
                alert(data.info);
            }
        },'json');
    }
})

$(".add_element_mine_attributes").click(function(){
    var value=$("#goods_list_select").val();
    var length = $(".content_list[data='"+value+"']").length;
    if(length){
        layer.open({
            time:1,
            content:'你已经添加,请不要重复添加',
            type:0,
        });
        return true;
    }
    
    $.post("/wap/goods_package/ajaxdetail", {'id': value}, function(data) {
        if(data.status){
            $(".all_money").before(data.content);
            calePrice();
                //商品数量操作
            var cartOpt = new cartNum();
            cartOpt.num_input();
            cartOpt.num_add();
            cartOpt.minus();            
        }else{
            layer.open({
                time:1,
                content:data.info,
                type:0
            });  
        }
    },'json');    
    
})



// 商品详情页 数量加减
var cartNum = function(){
    var buyNumObj=$('.buyNum');
    var buyNumInput=buyNumObj.children('input');
    var buyNumAdd=buyNumObj.children('.add');
    var buyNumMinus=buyNumObj.children('.minus');
    this.num_input = function(){
        buyNumInput.on('keyup',function(){
            var inputNum=parseInt($(this).val());
            var buyMaxNum = parseInt($(this).parent().attr('data-storage'));
            if(isNaN(inputNum)||inputNum<=0){
                inputNum=1;
            }else{
                if(inputNum>buyMaxNum){
                    inputNum=buyMaxNum;
                    layer.open({
                        className: 'alertMsg',
                        content: '超出数量范围~',
                        time:2
                    });
                }
            }
            $(this).val(inputNum);
            calePrice();
        })
    };
    this.num_add = function(){
        buyNumAdd.on('click',function(){
            var num=parseInt($(this).prev('input').val());
            var buyMaxNum = parseInt($(this).parent().attr('data-storage'));
            if(num>=buyMaxNum){
                layer.open({
                    className: 'alertMsg',
                    content: '亲，宝贝不能购买更多哦~',
                    time:2
                });
               return
            };
            $(this).prev('input').val(num+1);
            calePrice();
        })
    };
    this.minus = function(){
        buyNumMinus.on('click',function(){
            var num=parseInt($(this).next('input').val());
            if(num<=1){
                layer.open({
                    className: 'alertMsg',
                    content: '受不了，宝贝不能再减少了哦~',
                    time:2
                });
                return
            };
            $(this).next('input').val(num-1);
            calePrice();
        })
    }
}    

var cartOpt = new cartNum();
cartOpt.num_input();
cartOpt.num_add();
cartOpt.minus();

calePrice();
function calePrice()
{
    var objs = $(".mine_box .content_list");
    var length = objs.length;
    var money = 0;
    for(var i = 0;i<length;i++){
        var price = objs.eq(i).find('.buyNum').attr('data-price');
        var num = objs.eq(i).find('input').val();
        var money_total = parseFloat(price) * parseFloat(num);
        //console.log(money_total);
        money += money_total;
    }
    
    //增加附加的
    var objs = $(".mine_box .fujia_num:checked");
    var length = objs.length;
    var fujia_money = 0;
    for(var i = 0;i<length;i++){
        fujia_money += parseInt(objs.eq(i).attr("value"));
    }    
    
    var shangpin_money = money;
    money = money + fujia_money;
    
    $("#goods_price").html(shangpin_money.toFixed(2));
    $("#goods_price_total").html(money.toFixed(2));
}

$("#shangmen,#zhiwu").click(function(){
    calePrice();
})

//加入购物车
addCart();
function addCart()
{
    //加入购物车
    $('.addcart_submit_list').on('click',function(){
        var thisObj=$(this);


        var state = buy_v_yan();
        if(!state){
            return false;
        }

        //数组 goods_id  goods_num
        var objs = $(".mine_box .content_list");
        var length = objs.length;
        var mine_data = [];
        for(var i = 0;i<length;i++){
            mine_data[i] = {};
            mine_data[i].goods_id = objs.eq(i).find('.buyNum').attr('data-id');
            mine_data[i].quantity = objs.eq(i).find('input').val();
        }
        
        
        //增加附加的
        var other_obj = {};
        var objs = $(".mine_box .fujia_num:checked");
        var length = objs.length;
        for(var i = 0;i<length;i++){
            other_obj[i] = objs.eq(i).attr("data");
        } 
        
        
        var url = '/wap/cart/add';
        $.getJSON(url, {shop_list:mine_data,goods_id:GOODS_ID,other:other_obj}, function(data) {
            if (data !== null) {
                //layer.closeAll();
                if (data.status) {
                    layer.open({
                        className: 'alertMsg',
                        content: '添加成功',
                        time:2
                    });
                    $(".cart_num").html(data.num);
                } else {
                    layer.open({
                        className: 'alertMsg',
                        content: data.msg,
                        time:2
                    });
                }
            }
        });
    })     
}

function buy_v_yan()
{
    if(!GOODS_ID){//还没有选到商品ID    
        layer.open({
            className: 'alertMsg',
            content: '规格没选全',
            time:2
        });
        return false;                    
    }else{
        var goods_num = parseInt($("#goods_num").val());//购买的件数
        var goods_storage = parseInt($("#goods_storage").html());//库存
        var obj = $("dl.content_list");
        var length = obj.length;
        for(var i = 0;i<length;i++){
            var goods_num = parseInt(obj.eq(i).find('.goods_num').eq(0).val());//购买的件数
            var goods_storage = parseInt(obj.eq(i).find('.goods_storage').eq(0).html());//购买的件数
            if(goods_num > goods_storage){
                var goods_name = obj.eq(i).find('.goods_detail_name').html()
                layer.open({
                    className: 'alertMsg',
                    content: goods_name + '库存不足',
                    time:2
                });
                return false;   
            }
        }
    }
    return true;
}


Buy();
function Buy()
{
	$('a.buynow_submit_list').click(function(){
        if(IS_LOGIN == '' || IS_LOGIN == '0'){//未登录
            window.location.href='/wap/public/login';
            return false;
        }
        if(STORE_SELF == '1'){
            layer.open({
                className: 'alertMsg',
                content: '不能购买自己店铺的商品',
                time:2
            });
            return false;                  
        }


        var state = buy_v_yan();
        if(!state){
            return false;
        }

        //购买
        buynow(GOODS_ID, 1);
	});   
        
        
// 立即购买  num(商品数量)
function buynow(goods_list) {
    
    //数组 goods_id  goods_num
    var objs = $(".mine_box .content_list");
    var length = objs.length;
    var mine_data = [];
    var html = [];
    for(var i = 0;i<length;i++){
        mine_data[i] = {};
        mine_data[i].goods_id = objs.eq(i).find('.buyNum').attr('data-id');
        mine_data[i].quantity = objs.eq(i).find('input').val();
        html[i] = mine_data[i].goods_id + '|' + mine_data[i].quantity;
    }    
    var html_str = html.join(',');
    $("#shop_list").val(html_str);
    
    
    //增加附加的
    var objs = $(".mine_box .fujia_num:checked");
    var length = objs.length;
    var fujia_money = [];
    for(var i = 0;i<length;i++){
        fujia_money[i] =  parseInt(objs.eq(i).attr("data"));
    }     
    fujia_money = fujia_money.join(',');
    $("#fujia").val(fujia_money);    
    
    
    $("#buynow_form").submit();
}        
        
}

})
