$(function() {


    //点击收货地址
    changeAddress();

    //新增修改收货地址验证
    addrValid();

    //提交订单
    $('#submitOrder').on('click', function() {
        submitNext();
    });
});






//提交订单
function submitNext() {
    if ($('#address_id').val() == '' || $('.buy_address_has').length < 1) {
        layer.open({
            className: 'alertMsg',
            content: '请先设置收货地址！',
            time:2
        });
        return false;
    }
    if ($('#curCity_id').val() == '') {
        layer.open({
            className: 'alertMsg',
            content: '请选择收货地址！',
            time:2
        });
        return false;
    }

    
    $('#order_form').submit();
}

// 金额格式化
function number_format(num, ext){
    if(ext < 0){
        return num;
    }
    num = Number(num);
    if(isNaN(num)){
        num = 0;
    }
    var _str = num.toString();
    var _arr = _str.split('.');
    var _int = _arr[0];
    var _flt = _arr[1];
    if(_str.indexOf('.') == -1){
        /* 找不到小数点，则添加 */
        if(ext == 0){
            return _str;
        }
        var _tmp = '';
        for(var i = 0; i < ext; i++){
            _tmp += '0';
        }
        _str = _str + '.' + _tmp;
    }else{
        if(_flt.length == ext){
            return _str;
        }
        /* 找得到小数点，则截取 */
        if(_flt.length > ext){
            _str = _str.substr(0, _str.length - (_flt.length - ext));
            if(ext == 0){
                _str = _int;
            }
        }else{
            for(var i = 0; i < ext - _flt.length; i++){
                _str += '0';
            }
        }
    }
    return _str;
}


//切换收货地址
function check_address_id(){
    var city_id = $('#curCity_id').val();
    //切换地址，取消钱包支付
    $('#walletPay').find('.ck_icon').attr('ischeck',0).removeClass('ck_select');
    $('#walletPay').find('.prize_pay').html(0);
    $('#minusPrice').hide();
    $('#walletPay').find('.pay_password,.pay_tip').hide();
    getFreightPrice(city_id);
}


//点击收货地址
function changeAddress(){
    var _height=$(window).height();
    var _height1 = _height - 95;   // 新增地址高度
    var _height2 = _height - 45;   // 选择地区高度
    $('#addressNone').on('click',function(){   //第一次新增地址
        $('#addressWrapper').addClass('address_show');
        $('.address_cover').fadeIn();
        $("#add_form").css({"height":_height1});
        $(".zh_container").css({"height":_height})
    })
    $('.address_backBtn').on('click',function(){
        $('#addressWrapper').removeClass('address_show');
        $('.address_cover').fadeOut();
        $('#add_form').css({"height":'auto'});
        $(".zh_container").css({"height":'auto'})
    });

    $('#addNew_btn').on('click',function(){   //地址选择列表 - 新增地址
        $('#addressWrapper').addClass('address_show');
        $('.address_cover').fadeIn();
        $("#add_form").css({"height":_height1});
        $(".zh_container").css({"height":_height});
        $('#saveAdd_btn').addClass('list_creatBack_btn');
    });


    $(document).on('click', '.areaEdit', function() {   //选择地区
        $('#areaWrapper').addClass('area_show');
        $('.area_cover').fadeIn();
        $("#areaList").css({"height":_height2})
    })
    $('.area_backBtn').on('click',function(){
        $('#areaWrapper').removeClass('area_show');
        $('.area_cover').fadeOut();
        $('#areaList').css({"height":'auto'})
    })
    // 设为默认地址
//    $('#switchDefault').on('click', function() {
//        if ($(this).hasClass('ck_select')) {
//            $("#is_default").val(0);
//            $(this).removeClass('ck_select').attr('ischeck', 0);
//        } else {
//            $("#is_default").val(1);
//            $(this).addClass('ck_select').attr('ischeck', 1);
//        }
//    })

    $('.buy_address_has').on('click',function(){  //地址列表选择
        $('#listWrapper').addClass('list_show');
        $('.list_cover').fadeIn();
        $("#listAddr").css({"height":_height1});
        $(".zh_container").css({"height":_height})
    })
     $('.list_backBtn').on('click',function(){
        $('#listWrapper').removeClass('list_show');
        $('.list_cover').fadeOut();
        $('#listAddr').css({"height":'auto'});
        $(".zh_container").css({"height":'auto'})
    })
}

//新增&&修改收货地址 验证
function addrValid(){
    $('#add_area_info').zh_region();
    //表单操作
    var cForm = $("#creatAddrForm").Validform({
        tiptype: function(msg, o) {
            if (o.type == 3) {//只验证错误信息
                layer.open({
                    className: 'alertMsg',
                    content: msg,
                    time:2
                });
            }
        },
        datatype: {
            "wCity": /((\s)(.+)(\s))|(特别行政区\s)|(台湾省\s)/, //正则验证地区
        },
        tipSweep: true, //提交才验证
        callback: function(data) {
            if (data.status) { //成功添加新地址
                addAddressSuccess(data);
            } else {
                layer.open({
                    className: 'alertMsg',
                    content: data.msg,
                    time:2
                });
            }
            return false;
        }
    });
    //新增地址成功后功能
    var addAddressSuccess = function(data){
        $('.buy_address_none').hide();
        $('.buy_address_has').show();
        
        $('#addressName').html($("#add_name").val());
        $('#addressPhone').html($("#add_phone").val());
        $('#addressPosi').html($("#add_area_info").val() + $("#add_address").val());
        $('#curCity_id').val($("#add_city_id").val());       
        $('#curAddress_id').val(data.address_id);      
        $('#address_id').val(data.address_id);

        //赋值给地址列表
        var oMsg2 = $('#add_city_id').val();
        var oMsg3 = $('#add_name').val();
        var oMsg4 = $('#add_phone').val();
        var oMsg5 = $('#add_area_info').val() + $("#add_address").val();
        var objList = $("#listAddr").find("ul");
        var objMsg = "<li class='active'>"
                    +  "<div class='item_list' data-city-id='"+oMsg2+"' data-address-id='"+data.address_id+"'>"
                    +       "<i class='iconfont location'>&#xe610;</i>"
                    +       "<i class='iconfont arrow'>&#xe612;</i>"
                    +       "<p>收货人：<span class='select_name'>"+oMsg3+"</span><span class='select_phone'>"+oMsg4+"</span></p>"
                    +       "<p>收货地址：<span class='select_address'>"+oMsg5+"</span></p>"
                    +  "</div>";
                    +"</li>";
        objList.find('li').removeClass('active');
        objList.append(objMsg);

        //关闭所有的层
        $('#addressWrapper').removeClass('address_show');
        $('.address_cover').fadeOut();
        $('#add_form').css({"height":'auto'});
        $(".zh_container").css({"height":'auto'});
        $('.address_backBtn').removeClass('list_creatBack_btn');
        $('#listWrapper').removeClass('list_show');
        $('.list_cover').fadeOut();
        $('#listAddr').css({"height":'auto'});      
        
        //清空原来数值
        $("#add_name").val('');
        $("#add_phone").val('');
        $("#add_area_info").attr('value','');
        $("#is_default").val('');
        $("#add_address").val('');
        $("#switchDefault").attr('ischeck',0);
        $("#switchDefault").removeClass('ck_select');

        check_address_id(); //价格计算的功能           
    }
    
    $("#saveAdd_btn").click(function() {
        cForm.ajaxPost();
    });
}

// 地址列表选择
$(document).on('click', '#listAddr li', function() {
    var trueName = $(this).find('.select_name').html();
    var mobPhone = $(this).find('.select_phone').html();
    var modAddr = $(this).find('.select_address').html();
    var selCity = $(this).find('.item_list').attr('data-city-id');
    var selAddr = $(this).find('.item_list').attr('data-address-id');
    $('#listAddr').find('li').removeClass('active');
    $(this).addClass('active');
    $('#addressName').html(trueName);
    $('#addressPhone').html(mobPhone);
    $('#addressPosi').html(modAddr);
    $('#curAddress_id').val(selAddr);
    $('#address_id').val(selAddr);
    $('#curCity_id').val(selCity);
    //关闭弹出层
    $('#listWrapper').removeClass('list_show');
    $('.list_cover').fadeOut();
    $('#listAddr').css({"height":'auto'});
    $(".zh_container").css({"height":'auto'})
    check_address_id(); //改变地址，改变邮费TODO 
});

//计算价格的功能，或者是运费计算的功能
function check_address_id()
{
    var  mine_post = $("#mine_post").val();
    var  address_id = $("#address_id").val();
    
    $.post('/wap/buy/calcfreight',{mine_post:mine_post,address_id:address_id},function(data){
        if(data.status){
            if(data.content == 0){
                var total_f = 0;
                var length = $(".admin_id_allFreightTotal").length;
                for(var i=0; i< length;i++){
                    $(".admin_id_allFreightTotal").eq(0).html(0);
                }
            }else{
                for(var i in data.content[1]){
                   $("#allFreightTotal_" + i).html(data.content[1][i]);
                } 
                var total_f = data.content[0];
            }
            
            var all_total_money = parseInt(total_f) + parseFloat($("#allGoodsTotal_zhe").val())
            $("#all_total_money").html(all_total_money);
        }
    },'json');
}