//下拉加载更多数据
var IS_OK = true;  //是否可以继续加载数据
function loadGoods(callback){
    var range = 150;             //距下边界长度/单位px
    var totalheight = 0;
    $(window).scroll(function(){ 
        var srollPos = $(window).scrollTop();    //滚动条距顶部距离(页面超出窗口的高度)
        totalheight = parseFloat($(window).height()) + parseFloat(srollPos);
        if(($(document).height()-range) <= totalheight && IS_OK == true) {
            callback();
        }  
    }); 
}

//layer的弹窗提示
function layer_alert(info)
{
    layer.open({
        className: 'alertMsg',
        content: info,
        time:2
    });          
}


//表单的验证
function form_valid(is_alert)
{
    $('form').eq(0).Validform({
        tiptype: function(msg, o) { //有错误就会输出，一个是验证还没有提交前的，一个是验证ajax过来的数据的错误的数据信息
            if (o.type == 3) {//只验证错误信息,是没有经过服务器的本地的验证，表示如果验证失败的话会做的处理
                layer_alert(msg);
            }
        },
        tipSweep: true, //提交才验证
        ajaxPost: true, //ajax提交
        beforeSubmit: function() {
            layer.open({type: 2});//提交前的时候会输出这个东西，这是一个加载的一个按钮
        },
        callback: function(data) {//然后会返回这样的一个数据过来
            layer.closeAll();
//            if(is_alert){
//                layer_alert(data.info);          
//                return false;                    
//            }
            if (data.status && data.status != 500) {
                if(is_alert){
                    if(data.url){
                        window.location.href = data.url;                        
                    }else{
                        window.location.reload();
                    }                     
                }else{
                    layer.open({
                      content: data.info,
                      btn: ['确定'],
                      anim: 'up',
                      skin: 'footer',
                      yes: function(index){
                        if(data.url){
                            window.location.href = data.url;                        
                        }else{
                            window.location.reload();
                        } 
                      }
                    });                    
                }

            }else{
                layer_alert(data.info);          
                return false;                
            }
        }
    });   
}

//sel为点击的按钮，发送短信的功能
//var phone = $("#iphone").val();  必须含有带 iphone为ID的表单
function send_code(sel)
{
    
    var wait = 60;	//60秒才能发送一次
    if (getCookie("time") == null || getCookie("time") < 0){
        setCookie("time",wait,wait);
    }else if(getCookie("time") != wait){
        timeRef();
    } 
    
    //刷新按钮
    function timeRef(){
        waittime = getCookie("time");
        $(sel).attr('disabled','true');
        $(sel).html("重新发送(" + waittime + ")");
        waittime--;
        setCookie("time",waittime);
        setTimeout(function() {
            time()
        },1000);
    }
    
    //计时器
    function time() {
        if (getCookie("time") == null || getCookie("time") <= 0) {
            $(sel).removeAttr('disabled');
            //$(sel).attr('disabled','false');
            $(sel).html('获取短信验证码');
        } else {
            timeRef();
        }
    }    
    
    
    //js设置Cookie
    function setCookie(name,value,time)
    {
        var exp = new Date();
        exp.setTime(exp.getTime() + time * 1000);
        document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
    }
    //js获取Cookie
    function getCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");

        if(arr=document.cookie.match(reg))
            return (arr[2]);
        else
            return null;
    }    
    

    
    //手机号码一定是需要  ID  iphone的这个
    $(sel).click(function(){
        if($(this).hasClass('disabled')){
            return false;
        }
        var phone = $("#iphone").val();
        if(phone!='' && /^1[3|4|5|7|8][0-9]{9}$/.test(phone)){
            $.ajax({
                dataType:'json',
                data:{phone:phone}, 
                url:'/sns/send_message',
                success: function(data){
                    if (data.status) {
                        layer_alert(data.info);                       
                        setCookie("time",wait,wait);
                        time();
                    } else {
                        layer_alert(data.info);          
                    }
                } 
            }); 
        }else{
            layer_alert('请输入正确的手机号码');                       
        }
        return false;
    });    
}

//进行服务端请求 通过url 与请求的data_obj
function requestajax(url,data_obj)
{
    $.post(url, data_obj, function(data){
        if(((data.status === true || data.status === 1) && data.status != 500)){
            
            layer.open({
              content: data.info,
              btn: ['确定'],
              anim: 'up',
              skin: 'footer',
              yes: function(index){
                if(data.url){
                    window.location.href = data.url;                        
                }else{
                    window.location.reload();
                } 
              }
            });            
        }else{
            layer_alert(data.info);
        }
    },'json');    
}

//a标签如果 有带 .ajax_request  则会变成ajax请求链接并跳转
//请求前会先调用 excallback(obj),如果返回false则不处理，如果返回true则继续请求
//excallback 中会看是否有带callback属性，如果有的话则会去执行这个方法。
$(document).on('click','a.ajax_request',function(e){
    e.preventDefault();
    var msg = $(this).attr('msg');
    msg = !msg ? '确认此操作吗？' : msg;
    var obj = $(this);
    var url = obj.attr('href');
    
 
    
    layer.open({
        content: msg,
        btn: ['确认', '取消'],
        yes: function(){
            var data_obj = excallback(obj);
            if(data_obj === false){
                var error = window.ERROR == '' ? '请求出错' : window.ERROR;
                layer.open({
                    className: 'alertMsg',
                    content: error,
                    time:2
                }); 
                return false;
            }            
            requestajax(url,data_obj);
        }
    });      
})




//弹窗的操作增加了表单的一些元素的功能。。。
//a标签有带 ajax_layer的请求
//定义这个  DIALOG_CONTENT  或者带有content属性  来确定content
//弹出来的内容必须含有  layermmain这个class 这个class是form的class

$(document).on('click','a.ajax_layer',function(e)
{
    
    var FUNCTION_REQUEST = {};
    FUNCTION_REQUEST.findContentForLayer = function(obj)
    {     
        var content = typeof DIALOG_CONTENT == 'string' ? DIALOG_CONTENT : obj.attr('content');
        return content;
    }

    FUNCTION_REQUEST.ajaxLayerContent = function(content,data_obj,url)
    {
        layer.open({
            content:content,
            btn:['确认s','取消'],
            yes:function(index, layers){
                var obj_form = $(".layermmain");
                //获取其它的参数
                FUNCTION_REQUEST.addFormData(data_obj,obj_form);//增加元素的值，
                requestajax(url,data_obj);//然后再次的请求某个链接
            },
        });         
    }

    FUNCTION_REQUEST.addFormData = function (data_obj,form_obj)
    {
        var obj_from = form_obj.find('input,textarea,select');
        var length = obj_from.length;
        if(length){
            for(var i=0; i<length; i++){
                var name = obj_from.eq(i).attr('name');
                if(typeof data_obj[name] == 'undefined'){
                    data_obj[name] = form_obj.find('[name="'+name+'"]').val();
                }
            }
        }        
    }    
    
    
    
    
    e.preventDefault();
    var obj = $(this);
    //第一步执行callback确定对象
    var data_obj = excallback(obj);
    if(data_obj === false){
        var error = window.ERROR == '' ? '请求出错' : window.ERROR;
        layer.msg(error);
        return false;
    }        
    var url = obj.attr('href');
    
    var content = FUNCTION_REQUEST.findContentForLayer(obj);
    if(content){
        FUNCTION_REQUEST.ajaxLayerContent(content,data_obj,url);           
    }else{
        var u = obj.attr('url');
        if(!u){
            return false;
        }else{
            $.post(u,data_obj,function(data){
                if(data){
                    var content = data;
                    FUNCTION_REQUEST.ajaxLayerContent(content,data_obj,url);                                  
                }
            });
        }
    }
})

    //执行回调并返回相关对象
    function excallback(obj)
    {
        var callback = obj.attr('callback');
        if(callback){
            var data_obj = window[callback]();
            if(callback === false){
                return false;
            }
            return data_obj;
        }else{
            return {};
        }
    }
    
    
$(function(){
    //返回顶部 && 首页搜索框操作
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('#backtotop').show();
            $('.fixed_header').removeClass('change_red');
        } else {
            $('#backtotop').hide();
            $('.fixed_header').addClass('change_red');
        }
    });
    $('#backtotop').click(function () {
        $('body,html').animate({
            scrollTop: 0
        },  400);
        return false;
    });
    if ($(window).scrollTop() == 0) {
        $('#backtotop').hide();
        $('.fixed_header').addClass('change_red');
    }else{
        $('#backtotop').show();
        $('.fixed_header').removeClass('change_red');
    }

    //商品列表 && 搜索结果页 筛选
    var gfl = $('#page_filter').find('.sort');
    gfl.bind('click',function(){
        gfl.removeClass('active');
        $(this).addClass('active');
        var sortVal = $('#priceSort').attr('data-sort');
        if($(this).hasClass("last")){  //价格
            if(sortVal == 1){
                $(this).attr('order', 3);
                $('#priceSort').find('i').addClass('arrow_hv');
                $('#priceSort').attr('data-sort','0');
            }else{
                $(this).attr('order', 4);
                $('#priceSort').find('i').removeClass('arrow_hv');
                $('#priceSort').attr('data-sort','1');
            }
        }
    })

    //商品列表 && 搜索结果页 滚动跟随
    $(window).scroll(function () {
        if ($(this).scrollTop() > 44) {
            $('#page_filter').addClass('fix_filter');
        } else {
            $('#page_filter').removeClass('fix_filter');
        }
    });
    if ($(window).scrollTop() > 44) {
        $('#page_filter').addClass('fix_filter');
    }else{
        $('#page_filter').removeClass('fix_filter');
    }

})

function list_data(url)
{
    $(function(){
        $(".msg_more").click(function(){
            jiaziaMore(url);
        })

        $(window).on('scroll', function(){
            var he = $(document).scrollTop() / $(document).height();
            if (he > 0.20) {
                jiaziaMore(url);
            }
        });                
    })    
}

$(function(){
    $(document).on('click','.add_cart',function(e){
        e.preventDefault();
        var goods_id = $(this).attr("common_id");
        var data_obj = {common_id:goods_id,quantity:1};
        var url = "/wap/cart/add";
        $.post(url, data_obj, function(data){
            if(((data.status === true || data.status === 1) && data.status != 500)){

                layer_alert("添加成功");
            }else{
                layer_alert("添加失败");
            }
        },'json');       
    })
})