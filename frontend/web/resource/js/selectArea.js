/*
 * 地址联动选择
 * input不为空时出现编辑按钮，点击按钮进行联动选择
 *
 * 使用范例：
 * [html]
 * <input id="region" name="region" type="hidden" value="" >
 * [javascrpt]
 * $("#region").zh_region();
 *
 */
(function($) {
    var form_province_id = "#province_id";
    var form_city_id = "#city_id";
    var form_area_id = "#area_id";
    $.fn.zh_region = function(options) {
        var $region = $(this);
        var settings = $.extend({}, {area_id: 0, region_span_class: "areaEdit"}, options);
        return this.each(function() {
            var $inputArea = $(this);
            var $region_btn = $('<a class="' + settings.region_span_class + '" href="javascript:void(0);">编辑</a>');
            $inputArea.after($region_btn);
            $region_btn.on("click", function(){
                initArea($inputArea);
            });
        });

        function initArea($inputArea) {
            settings.$area = $('<ul></ul>');
            //$inputArea.after(settings.$area);
            $('#areaList').html(settings.$area);
            loadAreaArray(function() {
                loadArea(settings.$area, settings.area_id);
            });
        }

        function loadArea($area, area_id){
            if($area && nc_a[area_id].length > 0){
                var areas = [];
                areas = nc_a[area_id];
                for (i = 0; i <areas.length; i++){
                    $area.append("<li value='" + areas[i][0] + "'>" + areas[i][1] + "</li>");
                }
            }
            $area.find('li').on('click', function() {
                $(this).parents('ul').nextAll("ul").remove();
                var region_value = '';
                var area_id = '';  //area_id 代表省市县id 如：1 25 135
                $(this).parent().find('li').removeClass();
                $(this).addClass('selected');//给当前点击的li增加标示
                $('#areaList').find("ul").each(function() {
                    region_value += $(this).find("li.selected").text() + ' ';
                    area_id = $(this).find("li.selected").val();
                });
                $('#add_area_id').val(area_id);
                //获取城市ID
                if($(this).parent().html() == $('#areaList').find("ul").eq(1).html()){
                    $("#add_city_id").val($(this).attr('value'));
                    if($("#add_city_id").attr('changeCallback')){
                        eval($("#add_city_id").attr('changeCallback'));
                    }
                }
                $region.attr('value',region_value);
                $(this).parent().hide();
                var area_id = $(this).val();
                if(area_id > 0) {
                    if(nc_a[area_id] && nc_a[area_id].length > 0) {
                        var $newArea = $('<ul></ul>');
                        $(this).parents('ul').after($newArea);
                        loadArea($newArea, area_id);
                    }else{
                        $('#areaWrapper').removeClass('area_show'); //选择地区完毕
                        $('.area_cover').fadeOut();
                        $('#areaList').css({"height":'auto'});
                        $(form_province_id).val($('#areaList').find("ul").eq(0).find('li.selected').attr('value'));  
                        $(form_city_id).val($('#areaList').find("ul").eq(1).find('li.selected').attr('value'));  
                        $(form_area_id).val($('#areaList').find("ul").eq(2).find('li.selected').attr('value'));  
                    }
                }
            });
        }

        function loadAreaArray(callback) {
            if(typeof nc_a === 'undefined') {
                //取JS目录的地址
                var area_scripts_src = '';
                area_scripts_src = $("script[src*='jquery.js']").attr("src");
                area_scripts_src = area_scripts_src.replace('jquery.js', 'area_array.js');
                $.ajax({
                    url: area_scripts_src,
                    async: false,
                    dataType: "script"
                }).done(function(){
                    callback();
                });
            } else {
                callback();
            }
        }
    };
})(jQuery);



$(function(){

   changeAddress();
   

//收货地址管理
function changeAddress(){
   var _height=$(window).height();
   var _height2 = _height - 45;   // 选择地区高度
   $(document).on('click', '.areaEdit', function() {   //选择地区
        $('#areaWrapper').addClass('area_show');
        $('.area_cover').fadeIn();
        $("#areaList").css({"height":_height2})
    })
    $('.area_backBtn').on('click',function(){
        $('#areaWrapper').removeClass('area_show');
        $('.area_cover').fadeOut();
        $('#areaList').css({"height":'auto'})
    })

    // 设为默认地址
    $('#switchDefault').on('click', function() {
        if ($(this).hasClass('ck_select')) {
            $("#is_default").val(0);
            $(this).removeClass('ck_select').next().val(0);
        } else {
            $("#is_default").val(1);
            $(this).addClass('ck_select').next().val(1);
        }
    })
}   
   
})
