<?php

namespace frontend\modules\wap\controllers;

use frontend\modules\wap\components\BaseloginController;
use common\models\MemberAddressModel;
use common\models\MemberModel;
use yii;
/**
 * Default controller for the `wap` module
 */
class Member_caseController extends BaseloginController
{
    public function init() {
        parent::init();
        $this->title = "案例库";
    }

    
    //我的案例库
    public function actionMine()
    {
        $this->_ajaxMine();
        $condition = [
            'uid' => $this->user->uid,
        ];
        $datas = \common\models\MemberCaseModel::baseGetDatas($condition);
        $this->assign("datas",$datas);
        return $this->render();
    }
    
    //我的案例库
    private function _ajaxMine()
    {
        if(!yii::$app->request->isAjax){
            return;
        }
        //获取DATA的过程 开始
        $condition = [
            'uid' => $this->user->uid,
        ];
        $datas = \common\models\MemberCaseModel::baseGetDatas($condition);
        //获取DATA的过程 结束
        
        //转为数据的过程
        if($datas){
            $content = $this->renderPartial('base_mine',['datas' => $datas]);            
            $this->ajaxReturn(['content' => $content, 'status' => true]);
            exit;
        }else{
            $this->ajaxReturn(['status' => false]);
            exit;
        }
        //转为数据的过程结束
    }
    
}
