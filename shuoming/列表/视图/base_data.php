<!--订单类的包含了商品图片-->
<div class="each_order">
    <div class="order_hd">
        <i class="iconfont shop_icon">&#xe60b;</i>
        <span>中华钓鱼旗舰店</span>
        <span class="order_state">待发货</span>
    </div>

    <div class="order_bd">
        <img class="order_img" src="https://img.alicdn.com/imgextra/i1/515123038/TB2hpe8hVXXXXaVXXXXXXXXXXXX_!!515123038.jpg">
        <h2>佳钓尼-伽玛鲤3H限量版5.4米伽玛鲤超硬超轻钓鱼竿</h2>
        <p class="msg"><span>颜色：黄色</span><span>规格：5.4米</span></p>
<!--    <p class="back">退款金额：<em>¥ 158.00</em></p>-->
        <p class="info"><em>¥158.00</em><span>已售：1542件</span></p>
    </div>
    
    <div class="order_ft">
        共2件商品 合计：<em>¥ 898.00</em>（含运费 ¥0.00）
    </div>
    
    <div class="order_opt">
        <a href="#">取消收藏</a>
        <a href="#">立即购买</a>
    </div>
</div>




<!--<div  class="schoolItem">
 <ul>
                    <li>
                        <a class="schoolBox" href="#">
                            <img class="school_img" src="http://img.alicdn.com/imgextra/i3/TB1KxBTJpXXXXbnXFXXXXXXXXXX_!!0-item_pic.jpg_300x300.jpg">
                            <h2>自制玉米粒窝料的过程</h2>
                            <p>打碎的玉米粒加工一下作为窝料效果很好的尤的时候可以适量添加。下面介绍一些自制...</p>
                            <span class="school_info">
                                <span class="school_time">2015-12-12</span>
                                <span class="school_view"><i class="iconfont view"></i>1512</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a class="schoolBox" href="#">
                            <img class="school_img" src="http://img.alicdn.com/imgextra/i3/TB1KxBTJpXXXXbnXFXXXXXXXXXX_!!0-item_pic.jpg_300x300.jpg">
                            <h2>自制玉米粒窝料的过程</h2>
                            <p>打碎的玉米粒加工一下作为窝料效果很好的尤的时候可以适量添加。下面介绍一些自制...</p>
                            <span class="school_info">
                                <span class="school_time">2015-12-12</span>
                                <span class="school_view"><i class="iconfont view"></i>1512</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a class="schoolBox" href="#">
                            <img class="school_img" src="http://img.alicdn.com/imgextra/i3/TB1KxBTJpXXXXbnXFXXXXXXXXXX_!!0-item_pic.jpg_300x300.jpg">
                            <h2>自制玉米粒窝料的过程</h2>
                            <p>打碎的玉米粒加工一下作为窝料效果很好的尤的时候可以适量添加。下面介绍一些自制...</p>
                            <span class="school_info">
                                <span class="school_time">2015-12-12</span>
                                <span class="school_view"><i class="iconfont view"></i>1512</span>
                            </span>
                        </a>
                    </li>
                </ul>   
</div>-->
