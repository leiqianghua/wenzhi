    <?php  if(!$datas){  ?>
        <section class="order_none">
            <p>你还没有上传案例噢 !</p>
            <p>去逛一逛吧~ </p>
            <a href="<?=  $this->context->to(['create'])  ?>">上传案例</a>
        </section>    
    <?php }else{  ?>
    
        <section id="goods_list">
            <?= $this->render('base_mine',['datas' => $datas]) ?>    
        </section>    
        <?php require $this->context->template('layouts/base/msg_more'); ?>
    <?php  }  ?>

    
    <script>
        //下拉加载更多
        $(function(){
            list_data('<?= $this->context->to(['mine']) ?>');
        })
    </script>     
    
    
</body>
</html>