$(function(){
    //购物车全部选中（失效不选）
    var objChk = $('#form_buy').find('.ck_icon:not(.ck_disabled)');
    objChk.attr('ischeck',1).addClass('ck_select');

	all_total();

    //购物车数量操作
    var cartOpt = new cartNum();
    cartOpt.num_input();
    cartOpt.num_add();
    cartOpt.minus();

	// 全选
    $('#checkBoxAllBottom').on('click',function(){
        var obj = $('.cart_main').find('.ck_icon:not(.ck_disabled)');
        if($(this).attr('ischeck') == 0) {
            $(this).attr('ischeck',1).addClass('ck_select');
            obj.attr('ischeck',1).addClass('ck_select');
        } else {
            $(this).attr('ischeck',0).removeClass('ck_select');
            obj.attr('ischeck',0).removeClass('ck_select');
        }
        all_total();
    })
    // 单个店铺 全选
    $('i[chinatype="checkShopAll"]').on('click',function(){
        if ($(this).attr('ischeck') == 0) {
        	var b=0;
            $(this).attr('ischeck',1).addClass('ck_select');
            $(this).parent().next().find('.ck_icon:not(.ck_disabled)').attr('ischeck',1).addClass('ck_select');
            $('i[chinatype="checkShopAll"]:not(.ck_disabled)').each(function(){ //未选中，则不全选（失效商品忽略）
                if($(this).attr('ischeck') == 0) b+=1;
            })
	    	if(b<=0)$('#checkBoxAllBottom').attr('ischeck',1).addClass('ck_select');
        }else{
            $(this).attr('ischeck',0).removeClass('ck_select');
        	$('#checkBoxAllBottom').attr('ischeck',0).removeClass('ck_select');
            $(this).parent().next().find('.ck_icon').attr('ischeck',0).removeClass('ck_select');
        }
        all_total();
    });
    // 单个商品 选择
    $('.cart_item').find('.ck_icon:not(.ck_disabled)').on('click',function(){
    	if($(this).attr('ischeck') == 0){
    		var a=0,b=0;
            $(this).attr('ischeck',1).addClass('ck_select');
            $(this).parents('.cart_bd').find('.ck_icon:not(.ck_disabled)').each(function(){
                if($(this).attr('ischeck') == 0) a+=1;
            })
    		if(a<=0) $(this).parents('.cart_list').find('i[chinatype="checkShopAll"]').attr('ischeck',1).addClass('ck_select');
            $('i[chinatype="checkShopAll"]:not(.ck_disabled)').each(function(){ //未选中，则不全选（失效商品忽略）
                if($(this).attr('ischeck') == 0) b+=1;
            })
	    	if(b<=0)$('#checkBoxAllBottom').attr('ischeck',1).addClass('ck_select');
    	}else{
            $(this).attr('ischeck',0).removeClass('ck_select');
    		$(this).parents('.cart_list').find('i[chinatype="checkShopAll"]').attr('ischeck',0).removeClass('ck_select');
    		$('#checkBoxAllBottom').attr('ischeck',0).removeClass('ck_select');
    	}
    	all_total();
    })

    //结算提交
    $('#next_submit').on('click', function() {
        if ($('.cart_item').find('.ck_icon:not(.ck_disabled)').length == 0) {
            layer.msg('商品已失效，请返回重新购买！');
            return false;
        }
        
        var objPar = $('.cart_item').find('.ck_icon.ck_select').parent();
        var objInput = objPar.find('.item_amout').find('input');
        for(var i=0;i<objInput.length;i++){
            var maxNun = parseInt(objInput.eq(i).attr('max_num'));
            var curNun = parseInt(objInput.eq(i).val());
            if (curNun>maxNun){
                layer.open({
                    className: 'alertMsg',
                    content: '所选商品大于库存量，请更改数量',
                    time:2
                });
                return false;
            }
        }

        if ($('.cart_item').find('.ck_icon.ck_select').size() == 0) {
            layer.open({
                className: 'alertMsg',
                content: '请勾选您要结算的宝贝',
                time:2
            });
            return false;
        } else {
            addDataSubmit();//一些元素
            $('#form_buy').submit();
        }
    });
})

var cartNum = function(){
    // 商品数量增减
    var buyNumObj=$('.item_amout');
    var buyNumAdd=buyNumObj.children('.plus');
    var buyNumInput=buyNumObj.children('input');
    var buyNumMinus=buyNumObj.children('.minus');
    this.num_input = function(){
        buyNumInput.on('keyup',function(){
            var cartId=$(this).attr('cart_id');
            var prize_only=parseFloat($(this).parents('.cart_item').find('.td_price').html());
            var inputNum=parseInt($(this).val());
            if(isNaN(inputNum)||inputNum<=0){
                inputNum=1;
            }else{
                if(inputNum>parseInt($(this).attr('max_num'))){
                    inputNum=parseInt($(this).attr('max_num'));
                    $(this).parents('.cart_item').find('.cart_img').removeClass('show_low');//输到库存够时，隐藏提示
                    layer.open({
                        className: 'alertMsg',
                        content: '超出数量范围~',
                        time:2
                    });
                }
            }
            $(this).val(inputNum);
            change_quantity(cartId,inputNum);
            $(this).parents('.cart_item').find('.eachGoodsTotal').val((prize_only*inputNum).toFixed(2));
            all_total();
        })
    };
    this.num_add = function(){
        buyNumAdd.on('click',function(){
            var prize_only=parseFloat($(this).parents('.cart_item').find('.td_price').html());
            var buyNumInput=$(this).siblings('input');
            var cartId=buyNumInput.attr('cart_id');
            var num=parseInt(buyNumInput.val())+1;
            if(num>=parseInt(buyNumInput.attr('max_num'))+1){
                layer.open({
                    className: 'alertMsg',
                    content: '亲，宝贝不能购买更多哦~',
                    time:2
                });
                return;
            }
            change_quantity(cartId,num);
            buyNumInput.val(num);
            $(this).parents('.cart_item').find('.eachGoodsTotal').val((prize_only*num).toFixed(2));
            all_total();
        })
    };
    this.minus = function(){
        buyNumMinus.on('click',function(){
            var prize_only=parseFloat($(this).parents('.cart_item').find('.td_price').html());
            var buyNumInput=$(this).siblings('input');
            var cartId=buyNumInput.attr('cart_id');
            var num=parseInt(buyNumInput.val())-1;
            var max=parseInt(buyNumInput.attr('max_num'));
            //减到库存够时，隐藏提示
            if(num<=max){
                $(this).parents('.cart_item').find('.cart_img').removeClass('show_low');
            }
            if(num<=0){
                layer.open({
                    className: 'alertMsg',
                    content: '受不了，宝贝不能再减少了哦~',
                    time:2
                });
                return;
            };
            change_quantity(cartId,num);
            buyNumInput.val(num);
            $(this).parents('.cart_item').find('.eachGoodsTotal').val((prize_only*num).toFixed(2));
            all_total();
        }) 
    }
}

// 更改购物车数量
function change_quantity(cart_id,value){
    var subtotal = $('#item' + cart_id + '_subtotal');
    //暂存为局部变量，否则如果用户输入过快有可能造成前后值不一致的问题
    $.getJSON('index.php?act=cart&op=update&cart_id=' + cart_id + '&quantity=' + value, function(result){
        if(result.state == 'invalid'){
            layer.open({
                className: 'alertMsg',
                content: '宝贝已下架',
                time:2
            });
            return;
        }
        //库存不足时
        if(result.state == 'shortage'){
            layer.open({
                className: 'alertMsg',
                content: result.msg,
                time:2
            });
            return;
        }
        if(result.state == '') {
            layer.open({
                className: 'alertMsg',
                content: result.msg,
                time:2
            });
        }
    });
};

//删除购物车单个商品,
function drop_cart_item(cart_id) {
    layer.open({
        content: '确定要删除这个宝贝吗？',
        btn: ['确认', '取消'],
        shadeClose: false,
        yes: function(){
            $.getJSON('/m/cart/del/cart_id/' + cart_id + '.html' , function(result) {
                if (result.state) {
                    //删除成功
                    if (result.quantity == 0) {//判断购物车是否为空
                        window.location.reload();    //刷新
                    } else {
                        window.location.reload(); //暂时使用整个刷新
                        //删除单个产品 移除一些展示内容
                    }
                } else {
                    layer.open({
                        className: 'alertMsg',
                        content: result.msg,
                        time:2
                    });
                }
            });
        }
    });
}

// 购物车 统计总价格
function all_total() {
    obj = $('.cart_list');
    if(obj.length==0) return;
    var allTotal = 0; //购物车已选择商品的总价格
    var allGoodsNum=0;//购物车总件数
    obj.find('.cart_item').each(function(){
        var eachTotal = 0; //购物车每个店铺已选择商品的总价格
        $(this).find('.eachGoodsTotal').each(function(){
            var obj = $(this).siblings('.ck_icon').attr('ischeck');
            if(obj == 0){
                return;
            }
            eachTotal = eachTotal + parseFloat($(this).val());
            allGoodsNum+=1;
        });
        allTotal += eachTotal;
    });
    $('#allTotalBottom').html(parseFloat(allTotal).toFixed(2));
}

//提交的时候增加一些元素数据
function addDataSubmit()
{
    //获取所有满足条件的元素
    var obj = $(".cart_item").has('.ck_select');
    var length = obj.length;
    var html = '';
    for(var i = 0;i<length;i++){
        var cart_id = obj.eq(i).attr('data');
        var goods_num = obj.eq(i).find('.num_select').val();
        html += "<input name='cart_id[]' type='hidden' value='"+cart_id+"|"+goods_num+"'  />";
    }
    $("#form_buy").append(html);
}