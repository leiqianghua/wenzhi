$(function() {
    //总计 初始化
    calculationOrder();

    //第一次进页面地址包邮判断
    check_address_id();

    //点击收货地址
    changeAddress();

    //新增修改收货地址验证
    addrValid();

    //提交订单
    $('#submitOrder').on('click', function() {
        submitNext();
    });

    //使用钱包支付
    $('#walletPay').find('.ck_icon').on('click',function(){
        if($(this).attr('ischeck') == 0) {
            $(this).attr('ischeck',1).addClass('ck_select');
        } else {
            $(this).attr('ischeck',0).removeClass('ck_select');
        }
        walletPay($(this));
    })

    // 钱包支付 点击确定
    $('#walletPay').find('.pay_yes').on('click', function() {
        if($('#pd_password').val()==''){
            layer.open({
                className: 'alertMsg',
                content: '请输入支付密码！',
                time:2
            });
            return false;
        }
        $('#password_callback').val('');
        $.get("index.php?act=buy&op=check_pd_pwd", {'password': $('#pd_password').val()}, function(data) {
            if (data == '1') {
                $('#password_callback').val('1');
                $('#password').val($('#pd_password').val());
                $('.pay_password,.pay_tip').hide();
                layer.open({
                    className: 'alertMsg',
                    content: '验证成功',
                    time:2
                });
                return;
            }
            if (data == '3') {
                $('#password').val('');
                layer.open({
                    className: 'alertMsg',
                    content: '输入密码错误超过3次,请稍后输入!',
                    time:2
                });
            } else {
                $('#password').val('');
                layer.open({
                    className: 'alertMsg',
                    content: '密码错误,如果您暂未设置请到会员中心设置!',
                    time:2
                });
            }
        });
    });
});

//使用钱包支付js
function walletPay(obj){
    if(obj.length<=0) return;
    var payMax=parseFloat($('.payMax').html()).toFixed(2);
    var allNum=parseFloat($('#allTotalBottom').html()).toFixed(2);
    if(obj.attr('ischeck') == '1'){ 
        if(payMax-allNum>0){
            $('.prize_pay').html(allNum);
            $('#allTotalBottom').html('0.00');
        }else{
            $('.prize_pay').html(payMax);
            $('#allTotalBottom').html((allNum-payMax).toFixed(2));
        }
        $('#minusPrice,.pay_password,.pay_tip').show();
        $('#password').val('');
        $('#password_callback').val('');
    }else{
        var payMaxVal=parseFloat(number_format(obj.next().find('.prize_pay').html(),2));
        var backPrice=payMaxVal+parseFloat(allNum);
        $('#password_callback').val('');
        $('#password').val('');
        $('#allTotalBottom').html(number_format(backPrice,2));
        $('#minusPrice,.pay_password,.pay_tip').hide();
    }
}

//计算每个店铺商品总计(不含运费) 不展示,回调
function eachShopGoodsTotal() {
    var array = [];
    var indexPrizeAll = 0;
    var all = 0; //每个店铺小计(不含运费)
    $('.buy_list').each(function() {
        $(this).find('.buy_td_sum').each(function() {
            all += parseFloat($(this).val());
            indexPrizeAll += parseFloat($(this).val())
        });
        array.push(indexPrizeAll);
        indexPrizeAll = 0;
    });
    array.push(all);
    return array;
}

//计算商品总金额 和总运费
function calculationOrder() {
    var allGoodsTotal = 0,allFreight = 0,allTotal = 0;
    var tempTotal = eachShopGoodsTotal();
    $('.eachShopTotal').each(function(index){
        store_id = $(this).attr('store_id');
        if ($('#eachStoreFreight_'+store_id).length > 0) {
            allFreight += parseFloat($('#eachStoreFreight_'+store_id).val()); //计算总运费
        }
        $('#allFreightTotal').html(number_format(allFreight,2));
        allGoodsTotal += tempTotal[index]; //输出商品总金额
        allTotal = allFreight + allGoodsTotal;
    });
    $('#allGoodsTotal').html(number_format(allGoodsTotal,2)); //商品总金额
    $('#allTotalBottom').html(number_format(allTotal,2)); //总计
}



//提交订单
function submitNext() {
    if ($('#address_id').val() == '' || $('.buy_address_has').length < 1) {
        layer.open({
            className: 'alertMsg',
            content: '请先设置收货地址！',
            time:2
        });
        return false;
    }
    if ($('#curCity_id').val() == '') {
        layer.open({
            className: 'alertMsg',
            content: '请选择收货地址！',
            time:2
        });
        return false;
    }
    if ($('i[name="pd_pay"]').attr('ischeck') == '1' && $('#password_callback').val() != '1') {
        layer.open({
            className: 'alertMsg',
            content: '使用钱包金额支付，需输入支付密码！',
            time:2
        });
        return false;
    }
    
    
    
    $('#order_form').submit();
}

// 金额格式化
function number_format(num, ext){
    if(ext < 0){
        return num;
    }
    num = Number(num);
    if(isNaN(num)){
        num = 0;
    }
    var _str = num.toString();
    var _arr = _str.split('.');
    var _int = _arr[0];
    var _flt = _arr[1];
    if(_str.indexOf('.') == -1){
        /* 找不到小数点，则添加 */
        if(ext == 0){
            return _str;
        }
        var _tmp = '';
        for(var i = 0; i < ext; i++){
            _tmp += '0';
        }
        _str = _str + '.' + _tmp;
    }else{
        if(_flt.length == ext){
            return _str;
        }
        /* 找得到小数点，则截取 */
        if(_flt.length > ext){
            _str = _str.substr(0, _str.length - (_flt.length - ext));
            if(ext == 0){
                _str = _int;
            }
        }else{
            for(var i = 0; i < ext - _flt.length; i++){
                _str += '0';
            }
        }
    }
    return _str;
}


//切换收货地址
function check_address_id(){
    var city_id = $('#curCity_id').val();
    //切换地址，取消钱包支付
    $('#walletPay').find('.ck_icon').attr('ischeck',0).removeClass('ck_select');
    $('#walletPay').find('.prize_pay').html(0);
    $('#minusPrice').hide();
    $('#walletPay').find('.pay_password,.pay_tip').hide();
    getFreightPrice(city_id);
}

// ajax获取每个店铺运费 city_id计算运费
function getFreightPrice(city_id){
    $('#curCity_id').val('');
    $.post(changeAddr, {city_id: city_id, 'freight_hash': freightHash}, function(data) {
        if(data) {
            $('#curCity_id').val(city_id);
            var content = data.content;
            var allFreight = 0;
            for (var i in content) {
                $("#eachStoreFreight_" + i).val(number_format(content[i],2));
                allFreight = allFreight + parseFloat(content[i]);
            }
            calculationOrder();
        }
    }, "json");
}

//点击收货地址
function changeAddress(){
    var _height=$(window).height();
    var _height1 = _height - 95;   // 新增地址高度
    var _height2 = _height - 45;   // 选择地区高度
    $('#addressNone').on('click',function(){   //第一次新增地址
        $('#addressWrapper').addClass('address_show');
        $('.address_cover').fadeIn();
        $("#add_form").css({"height":_height1});
        $(".zh_container").css({"height":_height})
    })
    $('.address_backBtn').on('click',function(){
        $('#addressWrapper').removeClass('address_show');
        $('.address_cover').fadeOut();
        $('#add_form').css({"height":'auto'});
        $(".zh_container").css({"height":'auto'})
    });

    $('#addNew_btn').on('click',function(){   //地址选择列表 - 新增地址
        $('#addressWrapper').addClass('address_show');
        $('.address_cover').fadeIn();
        $("#add_form").css({"height":_height1});
        $(".zh_container").css({"height":_height});
        $('#saveAdd_btn').addClass('list_creatBack_btn');
    });


    $(document).on('click', '.areaEdit', function() {   //选择地区
        $('#areaWrapper').addClass('area_show');
        $('.area_cover').fadeIn();
        $("#areaList").css({"height":_height2})
    })
    $('.area_backBtn').on('click',function(){
        $('#areaWrapper').removeClass('area_show');
        $('.area_cover').fadeOut();
        $('#areaList').css({"height":'auto'})
    })
    // 设为默认地址
    $('#switchDefault').on('click', function() {
        if ($(this).hasClass('ck_select')) {
            $("#is_default").val(0);
            $(this).removeClass('ck_select').attr('ischeck', 0);
        } else {
            $("#is_default").val(1);
            $(this).addClass('ck_select').attr('ischeck', 1);
        }
    })

    $('.buy_address_has').on('click',function(){  //地址列表选择
        $('#listWrapper').addClass('list_show');
        $('.list_cover').fadeIn();
        $("#listAddr").css({"height":_height1});
        $(".zh_container").css({"height":_height})
    })
     $('.list_backBtn').on('click',function(){
        $('#listWrapper').removeClass('list_show');
        $('.list_cover').fadeOut();
        $('#listAddr').css({"height":'auto'});
        $(".zh_container").css({"height":'auto'})
    })
}

//新增&&修改收货地址 验证
function addrValid(){
    $('#add_area_info').zh_region();
    //表单操作
    var cForm = $("#creatAddrForm").Validform({
        tiptype: function(msg, o) {
            if (o.type == 3) {//只验证错误信息
                layer.open({
                    className: 'alertMsg',
                    content: msg,
                    time:2
                });
            }
        },
        datatype: {
            "wCity": /((\s)(.+)(\s))|(特别行政区\s)|(台湾省\s)/, //正则验证地区
        },
        tipSweep: true, //提交才验证
        callback: function(data) {
            if (data.status) { //成功添加新地址
                addAddressSuccess(data);
            } else {
                layer.open({
                    className: 'alertMsg',
                    content: data.msg,
                    time:2
                });
            }
            return false;
        }
    });
    //新增地址成功后功能
    var addAddressSuccess = function(data){
        $('.buy_address_none').hide();
        $('.buy_address_has').show();
        
        $('#addressName').html($("#add_name").val());
        $('#addressPhone').html($("#add_phone").val());
        $('#addressPosi').html($("#add_area_info").val() + $("#add_address").val());
        $('#curCity_id').val($("#add_city_id").val());       
        $('#curAddress_id').val(data.address_id);

        //赋值给地址列表
        var oMsg2 = $('#add_city_id').val();
        var oMsg3 = $('#add_name').val();
        var oMsg4 = $('#add_phone').val();
        var oMsg5 = $('#add_area_info').val() + $("#add_address").val();
        var objList = $("#listAddr").find("ul");
        var objMsg = "<li class='active'>"
                    +  "<div class='item_list' data-city-id='"+oMsg2+"' data-address-id='"+data.address_id+"'>"
                    +       "<i class='iconfont location'>&#xe610;</i>"
                    +       "<i class='iconfont arrow'>&#xe612;</i>"
                    +       "<p>收货人：<span class='select_name'>"+oMsg3+"</span><span class='select_phone'>"+oMsg4+"</span></p>"
                    +       "<p>收货地址：<span class='select_address'>"+oMsg5+"</span></p>"
                    +  "</div>";
                    +"</li>";
        objList.find('li').removeClass('active');
        objList.append(objMsg);

        //关闭所有的层
        $('#addressWrapper').removeClass('address_show');
        $('.address_cover').fadeOut();
        $('#add_form').css({"height":'auto'});
        $(".zh_container").css({"height":'auto'});
        $('.address_backBtn').removeClass('list_creatBack_btn');
        $('#listWrapper').removeClass('list_show');
        $('.list_cover').fadeOut();
        $('#listAddr').css({"height":'auto'});      
        
        //清空原来数值
        $("#add_name").val('');
        $("#add_phone").val('');
        $("#add_area_info").attr('value','');
        $("#is_default").val('');
        $("#add_address").val('');
        $("#switchDefault").attr('ischeck',0);
        $("#switchDefault").removeClass('ck_select');

        check_address_id(); //运费计算           
    }
    
    $("#saveAdd_btn").click(function() {
        cForm.ajaxPost();
    });
}

// 地址列表选择
$(document).on('click', '#listAddr li', function() {
    var trueName = $(this).find('.select_name').html();
    var mobPhone = $(this).find('.select_phone').html();
    var modAddr = $(this).find('.select_address').html();
    var selCity = $(this).find('.item_list').attr('data-city-id');
    var selAddr = $(this).find('.item_list').attr('data-address-id');
    $('#listAddr').find('li').removeClass('active');
    $(this).addClass('active');
    $('#addressName').html(trueName);
    $('#addressPhone').html(mobPhone);
    $('#addressPosi').html(modAddr);
    $('#curAddress_id').val(selAddr);
    $('#curCity_id').val(selCity);
    check_address_id(); //改变地址，改变邮费
    //关闭弹出层
    $('#listWrapper').removeClass('list_show');
    $('.list_cover').fadeOut();
    $('#listAddr').css({"height":'auto'});
    $(".zh_container").css({"height":'auto'})
});