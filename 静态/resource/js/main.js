$(function(){
    //首页新闻
    var $linksCarous = $('.news_sider');
    if ($linksCarous.find('li').length > 1) {
        setInterval(function() {
            $linksCarous.stop().animate({marginTop: '-48px'}, 500, function() {
                $(this).css('marginTop', 0).find('li:first').appendTo(this);
            })
        }, 5000);
    }
    //返回顶部 && 首页搜索框操作
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('#backtotop').show();
            $('.fixed_header').removeClass('change_red');
        } else {
            $('#backtotop').hide();
            $('.fixed_header').addClass('change_red');
        }
    });
    $('#backtotop').click(function () {
        $('body,html').animate({
            scrollTop: 0
        },  400);
        return false;
    });
    if ($(window).scrollTop() == 0) {
        $('#backtotop').hide();
        $('.fixed_header').addClass('change_red');
    }else{
        $('#backtotop').show();
        $('.fixed_header').removeClass('change_red');
    }

    //商品列表 && 搜索结果页 筛选
    var gfl = $('#page_filter').find('.sort');
    gfl.bind('click',function(){
        gfl.removeClass('active');
        $(this).addClass('active');
        var sortVal = $('#priceSort').attr('data-sort');
        if($(this).hasClass("last")){  //价格
            if(sortVal == 1){
                $(this).attr('order', 3);
                $('#priceSort').find('i').addClass('arrow_hv');
                $('#priceSort').attr('data-sort','0');
            }else{
                $(this).attr('order', 4);
                $('#priceSort').find('i').removeClass('arrow_hv');
                $('#priceSort').attr('data-sort','1');
            }
        }
    })

    //商品列表 && 搜索结果页 滚动跟随
    $(window).scroll(function () {
        if ($(this).scrollTop() > 44) {
            $('#page_filter').addClass('fix_filter');
        } else {
            $('#page_filter').removeClass('fix_filter');
        }
    });
    if ($(window).scrollTop() > 44) {
        $('#page_filter').addClass('fix_filter');
    }else{
        $('#page_filter').removeClass('fix_filter');
    }
})


//表单ajax提交
function ajaxFun(){
    var showmsg=function(msg){
        layer.open({
            className: 'alertMsg',
            content: msg,
            time:2
        });
    }
    $("#verifyForm").Validform({
        tiptype: function(msg, o) {
            if (o.type == 3) {//只验证错误信息
                showmsg(msg);
            }
        },
        callback: function(data) {
            if ((data.status === true || data.status === 1)) {
                window.location.href = data.url;
            } else {
                layer.open({
                    className: 'alertMsg',
                    content: data.info,
                    time:2
                });
            }
            return false;
        },
        tipSweep:true,
        ajaxPost:true
    });
}

//下拉加载更多数据
var IS_OK = true;  //是否可以继续加载数据
function loadGoods(callback){
    var range = 150;             //距下边界长度/单位px
    var totalheight = 0;
    $(window).scroll(function(){ 
        var srollPos = $(window).scrollTop();    //滚动条距顶部距离(页面超出窗口的高度)
        totalheight = parseFloat($(window).height()) + parseFloat(srollPos);
        if(($(document).height()-range) <= totalheight && IS_OK == true) {
            callback();
        }  
    }); 
}