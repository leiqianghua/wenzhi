$(function(){
    ajaxFun();
})

//表单ajax提交
function ajaxFun(){
    var showmsg=function(msg){
        layer.open({
            className: 'alertMsg',
            content: msg,
            time:2
        });
    }
    $("#verifyForm").Validform({
        tiptype: function(msg, o) {
            if (o.type == 3) {//只验证错误信息
                showmsg(msg);
            }
        },
        datatype: {
            "wCity": /((\s)(.+)(\s))|(特别行政区\s)|(台湾省\s)/, //正则验证地区
            "wCash": /^(([0-9]|([1-9][0-9]{0,9}))((\.[0-9]{1,2})?))$/, //金额
            "wMin":function(gets){
                if(gets>1){
                    return true;
                }
                return false;
            },
        },
        callback: function(data) {
            if ((data.status === true || data.status === 1)) {
                window.location.href = data.url;
            } else {
                layer.open({
                    className: 'alertMsg',
                    content: data.info,
                    time:2
                });
            }
            return false;
        },
        tipSweep:true,
        ajaxPost:true
    });
}
//点击 获取短信验证码
function sentFun(URLCODE){
    $('#sentCode').click(function(){
        var phone = $('#mPhone').val();
        if (phone == '') {
            layer.open({
                className: 'alertMsg',
                content: '请输入您的手机号码',
                time:2
            });
            return;
        }
        re = /^1[34578][0-9]{9}$/;
        if (!re.test(phone)) {
            layer.open({
                className: 'alertMsg',
                content: '手机号格式不正确',
                time:2
            });
            return;
        };

        $.ajax({
            dataType: 'json',
            data: {phone: phone, type: 'check'},
            url: URLCODE,
            success: function(data) {
                if (data.status) {
                    layer.open({
                        className: 'alertMsg',
                        content: data.msg,
                        time:2
                    });
                    settime($('#sentCode'));
                } else {
                    layer.open({
                        className: 'alertMsg',
                        content: data.msg,
                        time:2
                    });
                }
            }
        });
    });
}

//3s 倒计时的秒数
var countFun = function(url){
    var time = 3; 
    this.Load=function(){
        for (var i = time; i >= 0; i--) {
            window.setTimeout('doUpdate('+i+','+url+')', (time - i) * 1000);
        }
    }
}
function doUpdate(num,url){
    document.getElementById('showSec').innerHTML = num;
    if (num == 0) {
        window.location = url;
    }
}

//发送验证码按钮   60s倒计时
var countdown = 60;
function settime(val) {
    if (countdown == 0) {
        val.attr("disabled", false);
        val.html("获取短信验证码");
        countdown = 60;
        return true;
    } else {
        val.attr("disabled", true);
        val.html("重新发送(" + countdown + ")")
        countdown--;
    }
    setTimeout(function() {
        settime(val)
    }, 1000)
}

$(document).on('click','a.ajax_request',function(e){
    e.preventDefault();
    var msg = $(this).attr('msg');
    msg = !msg ? '确认此操作吗？' : msg;
    var obj = $(this);
    var url = obj.attr('href');
    
    layer.open({
        content: msg,
        btn: ['确认', '取消'],
        yes: function(){
            var data_obj = excallback(obj);
            if(data_obj === false){
                var error = window.ERROR == '' ? '请求出错' : window.ERROR;
                layer.open({
                    className: 'alertMsg',
                    content: error,
                    time:2
                }); 
                return false;
            }            
            requestajax(url,data_obj);
        }
    });      
})
//执行回调并返回相关对象
function excallback(obj)
{
    var callback = obj.attr('callback');
    if(callback){
        var data_obj = window[callback]();
        if(callback === false){
            return false;
        }
        return data_obj;
    }else{
        return {};
    }
}
//进行服务端请求
function requestajax(url,data_obj)
{
    $.post(url, data_obj, function(data){
        if((data.status === true || data.status === 1)){
            if(data.url){
                window.location.href = data.url;                        
            }else{
                window.location.reload();
            }                
        }else{
            layer.open({
                className: 'alertMsg',
                content: data.msg,
                time:2
            });            
        }
    },'json');    
}

//收货地址管理
function changeAddress(){
   var _height=$(window).height();
   var _height2 = _height - 45;   // 选择地区高度
   $(document).on('click', '.areaEdit', function() {   //选择地区
        $('#areaWrapper').addClass('area_show');
        $('.area_cover').fadeIn();
        $("#areaList").css({"height":_height2})
    })
    $('.area_backBtn').on('click',function(){
        $('#areaWrapper').removeClass('area_show');
        $('.area_cover').fadeOut();
        $('#areaList').css({"height":'auto'})
    })

    // 设为默认地址
    $('#switchDefault').on('click', function() {
        if ($(this).hasClass('ck_select')) {
            $("#is_default").val(0);
            $(this).removeClass('ck_select').next().val(0);
        } else {
            $("#is_default").val(1);
            $(this).addClass('ck_select').next().val(1);
        }
    })
}

$(document).on('click','a.ajax_layer',function(e){
    e.preventDefault();
    var obj = $(this);
    //第一步执行callback确定对象
    var data_obj = excallback(obj);
    if(data_obj === false){
        var error = window.ERROR == '' ? '请求出错' : window.ERROR;
        layer.msg(error);
        return false;
    }        
    var url = obj.attr('href');

    var content = findContentForLayer(obj);
    if(content){
        ajaxLayerContent(content,data_obj,url);           
    }else{
        var u = obj.attr('url');
        if(!u){
            return false;
        }else{
            $.post(u,data_obj,function(data){
                if(data){
                    var content = data;
                    ajaxLayerContent(content,data_obj,url);                                  
                }
            });
        }
    }
})

function findContentForLayer(obj)
{     
    var content = typeof DIALOG_CONTENT == 'string' ? DIALOG_CONTENT : obj.attr('content');
    return content;
}

function ajaxLayerContent(content,data_obj,url)
{
    layer.open({
        content:content,
        btn:['确认','取消'],
        yes:function(index, layers){
            var obj_form = $(".layermmain");
            //获取其它的参数
            addFormData(data_obj,obj_form);//增加元素的值，
            requestajax(url,data_obj);//然后再次的请求某个链接
        },
    });         
}

function addFormData(data_obj,form_obj)
{
    var obj_from = form_obj.find('input,textarea,select');
    var length = obj_from.length;
    if(length){
        for(var i=0; i<length; i++){
            var name = obj_from.eq(i).attr('name');
            if(typeof data_obj[name] == 'undefined'){
                data_obj[name] = form_obj.find('[name="'+name+'"]').val();
            }
        }
    }        
}    